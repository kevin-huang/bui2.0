/*
 * 配置文件
 * @Author: kevin.huang
 * @Date: 2018-07-21 09:10:46 
 * @Last Modified by: kevin.huang
 * @Last Modified time: 2019-04-19 11:29:29
 * Copyright (c): kevin.huang Released under MIT License
 */
(function ( global, factory) {
    "use strict";
      if ( typeof module === "object" && typeof module.exports === "object" ) {
          module.exports = global.document ?
              factory( global, true ) :
              function( w ) {
                  if ( !w.document ) {
                      throw new Error( "Bui requires a window with a document" );
                  }
                  return factory( w );
              };
      } else {
            factory( global );
      }  
  })( typeof window !== "undefined" ? window : this, function( window, noGlobal ) {
    "use strict";    
    var $B = window["$B"] ? window["$B"] : {};
    window["$B"] = $B;
    $B["config"] = {      
        copy:'复制',
        recoverCopy:'回到JSON树', 
        nextPage: '下一页',
        prevPage: '前一页',
        firstPage: '第一页',
        lastPage: '最后一页',
        pageSum: '共{total}条记录，每页显示',
        pageSumSuffix: '条记录',
        go2page: '跳转至',
        go2pageSuffix: '页',
        requestError: '对不起，请求错误！',
        error: '对不起，系统出现未知错误： ',
        permission: '对不起，您没有权限！',
        busy: '处理中......',
        loading: '正在加载中......',
        processing: '正在处理中......',
        loadingBackground:'#0A00FC',
        confirmTitle: '请您确认',
        buttonOkText: '确认',
        buttonCancleText: '取消',
        errorTitle: '错误提示',
        warnTitle: '警告提示',
        successTitle: '成功提示',
        messageTitle: '提示信息',
        file404:'无法找到需要下载的文件！',
        noData: '无数据',
        expException: '导出异常，请稍后再试！',
        uploadException: '文件上传出现异常',
        uploadTimeout: '文件上传超时！',
        uploadClearConfirm: '删除或者清空文件？',
        uploadClear: '清空文件',
        uploadRemove: '删除文件',
        uploadLable: '选择文件',
        uploadAccept: '只能上传【<accept>】类型的文件！',
        uploadNotEmpty: '请选择文件！',
        uploadAddFile: '添加文件',
        clearFile: '清空文件',
        uploading: '正在上传中......',
        uploadFail: '上传失败！',
        oprColName: '操作',
        closeLable: '关闭',
        comboxPlaceholder: '请选择',
        dataIsExist: '输入值已经存在!',
        tabLimit:'当前打开标签已经超过[x]个，请选关闭其他标签！',
        onlyGetOneData: '只能选择一条数据进行编辑！',
        need2CheckedData:'请您选择需要更新的数据！',
        need2CheckForDel:"请选择需要删除的数据！",
        confirmDelete:"您真的要删除数据吗？",
        htmlLoadError:'远程请求错误，请确保地址可以访问！',
        crossError:'由于浏览器安全机制，不支持本地文件请求访问，需要部署到服务器(iis、tomcat)上再浏览，或者使用Firefox浏览器进行打开',
        calendar:{
            year:'年',
            month:'月',           
            clear:'清空',
            now:'今天',
            close:'关闭',
            monthArray : [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
            weekArray : ['日', '一', '二', '三', '四', '五', '六'],
            error:'输入是时间格式错误！',
            minTip:'输入时间不能小于[ x ]',
            maxTip:'输入时间不能大于[ x ]',
            error:'输入值不符合格式要求！'
        },
        tabMenu: {
            closeLeft: '关闭左侧菜单',
            closeRight: '关闭右侧菜单',
            closeOthers: '关闭其他菜单',
            closeAll: '关闭所有菜单',
            reload: '刷新当前菜单'
        },
        curdDatagridOpts:{
            fillParent: false,
            splitColLine: 'k_datagrid_td_all_line',
            pgposition: 'bottom',
            idField:"id"
        },
        curdOpts: {           
            add_action: "add",
            del_action: "delete",
            query_action: "query",
            get_action: "get",
            update_action: "update",
            page_action: "page"
        },
        curdWinDefOpts: {
            maxminable: false, //可变化小大
            collapseable: false //上下收缩
        },
        valid: {
            message: {
                require: '该项必填',
                wchar: '必须为字母、数字、下划线',
                enchar: '必须填写字母类型的字符',
                chchar: '必须填写汉字',
                remote: '该字段已经存在',
                email: '必须输入正确的邮件',
                phone: '请输入正确的手机号码',
                telphone: '请输入正确的座机号码',
                number: '必须输入数值文字',
                digits: '必须输入整形数值',
                password:'最少6位包含大小写字母，特殊字符',
                range: '输入值必须在{1}至{2}范围内',
                minlength: '输入文字长度必须大于{1}',
                maxlength: '输入文字长度必须小于{1}',
                regex: '输入数据不符合格式要求',
                url: '请输入合法的url地址',
                idCardNo:'请输入合法的身份证号码'
            },
            regex: {
                wchar: /^\w+$/, //只能是字母、数字、下划线
                enchar: /^[a-zA-Z]+$/, //必须为英文字符
                chchar: /^[u4E00-u9FA5]+$/, //必须为中文字符
                email: /^([a-zA-Z0-9_-])+.([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(.[a-zA-Z0-9_-])+/, //必须符合邮件格式
                telphone: /^(\(\d{3,4}\)|\d{3,4}-)?\d{7,8}$/, //必须符合坐机电话号码格式
                phone: /^(0|86|17951)?(13[0-9]|15[012356789]|17[678]|18[0-9]|14[57])[0-9]{8}$/, //必须符合手机号码格式
                number: /^-?\d+(.?\d+)?$/, //必须为数值
                password:/^.*(?=.{6,})(?=.*\d)(?=.*[A-Z])(?=.*[a-z])(?=.*[!@#$%^&*? ]).*$/, //密码强度要求 最少6位，包括至少1个大写字母，1个小写字母，1个数字，1个特殊字符
                digits: /^[0-9][0-9]*$/, //必须为整形数值
                url: /((http(s)?|ftp):\/\/)?([\w-]+\.)+[\w-]+(\/[\w- .\/?%&=]*)?(:\d+)?/,
                idCardNo:/^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$|^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}([0-9]|X)$/
            }
        }, 
        sucessIconColor:'#3BB208',
        errorIconColor:'#F7171C',
        alertIconColor:'#F0F000',       
        toolbarOpts: {
            style: 'min', //工具栏按钮样式
            color: '#64B2E5',
            iconColor: '#FFFFFF',
            fontColor: '#FFFFFF'
        },
        rowBtnColor:'#939393',//行按钮颜色
        scrollBarStyle:{ //myscrollbar 的自定义样式
                "background-color": '#03ADE5',
                opacity: 0.6
        }
    };
    return $B;
});