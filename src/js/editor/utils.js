/**
 * 定义富文本的工具类
 * by kevin.huang
 * ****/
var UTILS = {
    stopDefaultctxMenu: function () {
        if (typeof UTILS.CTXMENUTIMER !== "undefined") {
            return;
        }
        let $body = $B.getBody();
        let fn = function () {
            return false;
        };
        $B.DomUtils.addListener($body, "edit.contextmenu", fn);
        UTILS.CTXMENUTIMER = setTimeout(function () {
            UTILS.CTXMENUTIMER = undefined;
            $B.DomUtils.removeListener($body, "edit.contextmenu", fn);
        }, 500);
    },
    getDPI: function () {
        if (this.DPI) {
            return this.DPI;
        }
        if (window.screen.deviceXDPI != undefined) {
            this.DPI = window.screen.deviceXDPI;
        } else {
            let tmpNode = document.createElement("DIV");
            tmpNode.style.cssText = "width:1in;height:1in;position:absolute;left:0px;top:0px;z-index:99;visibility:hidden";
            document.body.appendChild(tmpNode);
            this.DPI = parseInt(tmpNode.offsetWidth);
            tmpNode.parentNode.removeChild(tmpNode);
        }
        this.DPI;
    },
     /*** 96dpi：1英寸=96 1cm=37.795276  一英寸=0.0254m=2.54cm 
     * ***/
    px2cm:function(px){
        let dpi = this.getDPI();
        let tmp = dpi.div(2.54);
        let res = px.div(tmp);
        return res;
    },
    cm2px:function(cm){
        let dpi = this.getDPI();
        let tmp = dpi.div(2.54);
        let res = cm.mul(tmp);
        return res;
    },
    /**
    * 获取一个段落元素
    * ***/
    createPEl: function (notCHild) {
        let id = $B.generateDateUUID();
        let $e = $B.DomUtils.createEl('<div class="p_element" id="' + id + '" style="' + editCfg.pElementCss + '"></div>');
        if (!notCHild) {
            let $s = this.createSpanEl();
            $B.DomUtils.append($e, $s);
        }
        return $e;
    },
    createSpanEl: function () {
        let id = $B.generateDateUUID();
        let $e = $B.DomUtils.createEl('<span class="p_span"  id="' + id + '"  style="' + editCfg.spanCss + '">\u200B</span>');
        return $e;
    },
    getPelDefCss: function () {
        var cssMap = {
            "pbackgorund": undefined,
            "lhrate": undefined,
            "indent": undefined,
            "line-height": undefined,       //行高
            "padding-left": undefined,       //左边距
            "padding-right": undefined,      //右边距
            "padding-top": undefined,         //上边距
            "padding-bottom": undefined,       //下边距
            "border-top": undefined,         //上边框
            "border-right": undefined,         //上边框
            "border-bottom": undefined,         //上边框
            "border-left": undefined,       //上边框
            "text-align": undefined,               //对齐
            "border-bottom": undefined,
            "border-left": undefined,
            "border-right": undefined,
            "border-top": undefined
        };
        return cssMap;
    },
    getSpanDefcss: function () {
        var cssMap = {
            "font-weight": undefined, //加粗
            "font-style": undefined,     //斜体
            "text-decoration": undefined,    //underline下划线，line-through中划线
            "vertical-align": undefined,      //上标super ；下标sub
            "font-family": undefined,        //字体
            "font-size": undefined,          //字体大小
            "letter-spacing": undefined,    //字体间距  
            "color": undefined,              //字体颜色
            "background-color": undefined   //背景色
        };
        return cssMap;
    },
    removeAfterEls: function (firstp) {
        let nextp = firstp.nextSibling;
        while (nextp) {
            let tmpp = nextp.nextSibling;
            $B.DomUtils.remove(nextp);
            nextp = tmpp;
        }
    },
    /**
 * 获取行内最大字体
 * ***/
    getMaxFontSize: function ($p, max) {
        var childs = $p.children;
        for (let i = 0; i < childs.length; i++) {
            let child = childs[i];
            let obj = $B.style2cssObj(child);
            if (obj["font-size"]) {
                let v = parseFloat(obj["font-size"]);
                if (!max) {
                    max = v;
                } else {
                    if (v > max) {
                        max = v;
                    }
                }
            }
            let nv = this.getMaxFontSize(child, max);
            if (nv && nv > max) {
                max = nv;
            }
        }
        return max;
    },
    /**
    * 获取段落的行高比，及最大字体
    */
    getLineHightData: function ($p) {
        let max = this.getMaxFontSize($p);
        let lineHeight = $B.DomUtils.css($p, "line-height");
        let r = lineHeight / max;
        return {
            lh: lineHeight,
            max: max,
            r: r
        };
    },
    getFirstTextEL: function ($el) {
        let txt = $el.innerText.replaceAll("\u200B", "");
        if (txt.length > 0) {
            return $el;
        }
        if ($el.nextSibling) {
            return this.getFirstTextEL($el.nextSibling);
        }
        return $el;
    },
    setBorderMap: function (map, borderObj) {
        if (typeof map.size === "undefined") {
            map.size = borderObj.size;
        } else if (borderObj.size !== map.size) {
            map.size = undefined;
        }
        if (typeof map.style === "undefined") {
            map.style = borderObj.style;
        } else if (borderObj.style !== map.style) {
            map.style = undefined;
        }
        if (typeof map.color === "undefined") {
            map.color = borderObj.color;
        } else if (borderObj.color !== map.color) {
            map.color = undefined;
        }
    },
    setBorderCss: function ($el, borderCss) {
        let cssObj = $B.style2cssObj($el);
        let keys = Object.keys(cssObj);
        for (let i = 0; i < keys.length; i++) {
            let key = keys[i];
            if (key.indexOf("border") > -1) {
                delete cssObj[key];
            }
        }
        $B.extendObjectFn(cssObj, borderCss);
        let style = $B.cssObj2string(cssObj);
        $B.DomUtils.attribute($el, { style: style });
    },
    /**
     * 边框样式转 边框对象
     * * */
    border2obj: function (border, not2int) {
        let r = {
            size: undefined,
            style: undefined,
            color: undefined
        };
        if (border) {
            border = border.replace(/,\s+/g, ",");
            let tmp = border.split(/\s+/g);
            r.size = not2int ? tmp[0] : parseInt(tmp[0].replace("px", ""));
            r.style = tmp[1];
            r.color = tmp[2].replaceAll(",", ", ");
        }
        return r;
    },
    getPelCss: function ($el, skipBorder) {
        let lData = this.getLineHightData($el);
        let lineHight = lData.lh;
        let paddingLeft = $B.DomUtils.css($el, "padding-left");
        let paddingRight = $B.DomUtils.css($el, "padding-right");
        let paddingTop = $B.DomUtils.css($el, "padding-top");
        let paddingBottom = $B.DomUtils.css($el, "padding-bottom");
        let textAlign = $B.DomUtils.css($el, "text-align");
        let textIndent = $B.DomUtils.css($el, "text-indent");
        let indent = parseFloat(textIndent.replace("px", ""));
        let $fc = this.getFirstTextEL($el.firstChild);
        let fs = parseFloat($B.DomUtils.css($fc, "font-size").replace("px", ""));
        let char = indent / fs;
        let ret = {
            "pbackgorund": $B.DomUtils.css($el, "background-color"),
            "line-height": lineHight,
            "padding-left": paddingLeft,
            "padding-right": paddingRight,
            "padding-top": paddingTop,
            "padding-bottom": paddingBottom,
            "text-align": textAlign,
            "text-indent": textIndent,
            "lhrate": lData.r,
            "indent": char
        };
        if (!skipBorder) {
            let border = this.getBorder($el);
            ret["border-bottom"] = border["border-bottom"];
            ret["border-left"] = border["border-left"],
                ret["border-right"] = border["border-right"];
            ret["border-top"] = border["border-top"];
        }
        return ret;
    },
    getBorder: function ($el, exclude) {
        var bsize = $B.DomUtils.css($el, "border-top-width") + "px";
        var bstyle = $B.DomUtils.css($el, "border-top-style");
        var bcolor = $B.DomUtils.css($el, "border-top-color");
        var val = bsize + " " + bstyle + " " + bcolor;
        if (bsize === 0 || bstyle === "none") {
            val = undefined;
        }
        var ret = {};
        if (!exclude || exclude !== "top") {
            ret["border-top"] = val;
        }
        bsize = $B.DomUtils.css($el, "border-right-width") + "px";
        bstyle = $B.DomUtils.css($el, "border-right-style");
        bcolor = $B.DomUtils.css($el, "border-right-color");
        val = bsize + " " + bstyle + " " + bcolor;
        if (bsize === 0 || bstyle === "none") {
            val = undefined;
        }
        if (!exclude || exclude !== "right") {
            ret["border-right"] = val;
        }
        bsize = $B.DomUtils.css($el, "border-bottom-width") + "px";
        bstyle = $B.DomUtils.css($el, "border-bottom-style");
        bcolor = $B.DomUtils.css($el, "border-bottom-color");
        val = bsize + " " + bstyle + " " + bcolor;
        if (bsize === 0 || bstyle === "none") {
            val = undefined;
        }
        if (!exclude || exclude !== "bottom") {
            ret["border-bottom"] = val;
        }
        bsize = $B.DomUtils.css($el, "border-left-width") + "px";
        bstyle = $B.DomUtils.css($el, "border-left-style");
        bcolor = $B.DomUtils.css($el, "border-left-color");
        val = bsize + " " + bstyle + " " + bcolor;
        if (bsize === 0 || bstyle === "none") {
            val = undefined;
        }
        if (!exclude || exclude !== "left") {
            ret["border-left"] = val;
        }
        return ret;
    },
    setCssMapVal: function (cssMap, valObj, cssName) {
        if (typeof cssMap[cssName] === "undefined") {
            cssMap[cssName] = valObj[cssName];
        } else if (cssMap[cssName] !== null && cssMap[cssName] !== valObj[cssName]) {
            cssMap[cssName] = null;
        }
    },
    loopExtractCss: function ($el, cssMap, skipBorder) {
        if ($el.nodeName === "P") {
            this.extractPcss($el, cssMap, skipBorder);
            let childs = $el.children;
            for (let i = 0; i < childs.length; i++) {
                this.loopExtractCss(childs[i], cssMap, skipBorder);
            }
        } else if ($el.nodeName === "SPAN") {
            this.extractSpancss($el, cssMap);
        } else {
            let childs = $el.children;
            for (let i = 0; i < childs.length; i++) {
                this.loopExtractCss(childs[i], cssMap, skipBorder);
            }
        }
    },
    /**
    * 抽取段落样式
    * ***/
    extractPcss: function ($p, cssMap, skipBorder) {
        let v1 = this.getPelCss($p, skipBorder);
        this.setCssMapVal(cssMap, v1, "pbackgorund");//段落背景色        
        this.setCssMapVal(cssMap, v1, "text-align");//对齐
        this.setCssMapVal(cssMap, v1, "line-height");//行高
        this.setCssMapVal(cssMap, v1, "padding-left");//左边距
        this.setCssMapVal(cssMap, v1, "padding-right");//右边距
        this.setCssMapVal(cssMap, v1, "padding-top");//上边距
        this.setCssMapVal(cssMap, v1, "padding-bottom");//下边距 
        this.setCssMapVal(cssMap, v1, "text-indent");//缩进     
        this.setCssMapVal(cssMap, v1, "lhrate");//行高比例      
        this.setCssMapVal(cssMap, v1, "indent");//缩进字符 
        if (!skipBorder) {
            this.setCssMapVal(cssMap, v1, "border-top");//上边框
            this.setCssMapVal(cssMap, v1, "border-right");//上边框
            this.setCssMapVal(cssMap, v1, "border-bottom");//上边框
            this.setCssMapVal(cssMap, v1, "border-left");//左边框 
        }
    },
    /**
    * 抽取SPAN样式
    * ***/
    extractSpancss: function ($el, cssMap) {
        let v1 = this.getSpanCss($el);
        this.setCssMapVal(cssMap, v1, "font-weight");//加粗
        this.setCssMapVal(cssMap, v1, "font-style");//斜体
        this.setCssMapVal(cssMap, v1, "text-decoration");//underline下划线，line-through中划线
        this.setCssMapVal(cssMap, v1, "vertical-align");//上标super ；下标sub
        this.setCssMapVal(cssMap, v1, "font-family");//字体
        this.setCssMapVal(cssMap, v1, "font-size");//字体大小
        this.setCssMapVal(cssMap, v1, "letter-spacing");//字体间距 
        this.setCssMapVal(cssMap, v1, "color"); //字体颜色
        this.setCssMapVal(cssMap, v1, "background-color"); //背景色        
    },
    getSpanCss: function ($el) {
        let txtDesc = $B.DomUtils.css($el, "text-decoration");
        if (txtDesc.indexOf("none") >= 0) {
            txtDesc = undefined;
        }
        let ret = {
            "font-weight": $B.DomUtils.css($el, "font-weight"),           //加粗
            "font-style": $B.DomUtils.css($el, "font-style"),            //斜体
            "text-decoration": txtDesc,       //underline下划线，line-through中划线
            "vertical-align": $B.DomUtils.css($el, "vertical-align"),        //上标super ；下标sub
            "font-family": $B.DomUtils.css($el, "font-family"),           //字体
            "font-size": $B.DomUtils.css($el, "font-size"),             //字体大小
            "letter-spacing": $B.DomUtils.css($el, "letter-spacing"),        //字体间距  
            "color": $B.DomUtils.css($el, "color"),                 //字体颜色           
            "background-color": $B.DomUtils.css($el, "background-color")       //背景色
        };
        return ret;
    },
    getPanelEl: function () {
        return $B.DomUtils.createEl('<div style="width: auto; display: none;" class="k_edit_tools_tip_wrapper k_edit_tools_drop_wrapper k_box_size  k_box_shadow"><div class="k_edit_tools_drop_caret"><i class="fa fa-up-dir"></i></div><div class="k_edit_tools_drop_item_wrap" tabindex="0" _focus="true"></div></div>');
    },
    createCommPanel: function (opts) {
        let onClosed = opts.onClosed;
        css = opts.css,
            title = opts.title;
        let $panel = this.getPanelEl();
        if (css) {
            $B.DomUtils.css($panel, css);
        }
        let $wap = $panel.lastChild;
        let $b = $B.DomUtils.append($wap, "<div  style='position:relative;width:100%;border-top:30px solid #fff' class='k_box_size'></div>");
        let $h = $B.DomUtils.append($wap, "<div id='_dragable' class='_common_drag k_box_size' style='position:absolute;width:100%;height:30px;top:0;left:0;border-bottom:1px solid #dfdbdb;text-align:left;line-height:26px;' class='k_box_size'>" + title + "<span style='position:absolute;top:5px;right:3px;cursor:pointer;' id='_title_close'><i class='fa  fa-cancel-2'></i></span></div>");
        if (opts.title === "") {
            $b.style["border-top"] = "10px solid #fff";
            $h.style["height"] = "10px";
            $h.firstChild.style["top"] = "-5px";
            $h.firstChild.style["fontSize"] = "12px";
        }
        $B.DomUtils.append(document.body, $panel);
        if (opts.onCreated) {
            opts.onCreated($b)
        };
        let dragOpt = {
            notClearRange: true,
            isProxy: false,
            handler: "#_dragable"
        };
        $B.draggable($panel, dragOpt);
        let $c = $B.DomUtils.findbyId($h, "_title_close");
        $B.DomUtils.click($c, (e) => {
            let $el = e.target.parentNode.parentNode.parentNode.parentNode;
            $el.style.display = "none";
            if (onClosed) { onClosed($el); }
        });
        return $panel;
    },
    showPanel: function ($panel, pos) {
        $panel.style.top = "-1000px";
        $panel.style.left = "-1000px";
        $panel.style.display = "block";
        let width = $B.DomUtils.outerWidth($panel);
        let height = $B.DomUtils.outerHeight($panel);
        let bw = $B.DomUtils.width(document.body);
        let bh = $B.DomUtils.height(document.body);
        let maxLeft = pos.left + width + 12;
        if (maxLeft > bw) {
            pos.left = bw - width - 12;
        }
        let maxTop = pos.top + height + 20;
        let $icon = $panel.firstChild;
        if (maxTop > bh) {
            pos.top = pos.top - height - 20;
            $panel.style.borderBottom = "2px solid rgb(66, 66, 66)";
            $panel.style.borderTop = "1px solid #dfdbdb";
            $icon.style.top = (height - 5) + "px";
            $icon.firstChild.style.transform = "rotateZ(180deg)";
        } else {
            $icon.style.top = "-11px";
            $icon.firstChild.style.transform = "rotateZ(0deg)";
            $panel.style.borderTop = "2px solid rgb(66, 66, 66)";
            $panel.style.borderBottom = "none";
        }
        $panel.style.top = pos.top + "px";
        $panel.style.left = pos.left + "px";
    },
    createChildCtxMenu: function (id, list, onClick) {
        let $wap = $B.DomUtils.createEl("<div id='" + id + "' class='k_box_size k_box_shadow k_edit_ctx_menu' style='position:absolute;background:#fff;display:none;z-index:2147483647'></div>");
        for (let i = 0; i < list.length; i++) {
            $B.DomUtils.append($wap, "<span class='_ctx_it' fn='" + list[i].fn + "'>" + list[i].label + "</span>");
        }
        $B.DomUtils.click($wap, (e) => {
            if ($B.DomUtils.hasClass(e.target, "_ctx_it")) {
                onClick.call(e.target, $B.DomUtils.attr(e.target, "fn"));
            }
        });
        $B.DomUtils.mouseleave($wap, (e) => {
            e.target.style.display = "none";
        });
        $B.DomUtils.append(document.body, $wap);
        return $wap;
    },
    showCtxMenu: function ($ctx, el) {
        //console.log("showCtxMenu");
        $ctx.style.top = "-1000px";
        $ctx.style.left = "-1000px";
        $ctx.style.display = "block";
        let ctxw = $B.DomUtils.outerWidth($ctx);
        let ctxh = $B.DomUtils.outerHeight($ctx);

        let ofs = $B.DomUtils.offset(el);
        let w = $B.DomUtils.outerWidth(el) + 5;

        ofs.left = ofs.left + w;
        let bw = $B.DomUtils.width(document.body);
        let bh = $B.DomUtils.height(document.body);
        if ((ofs.left + ctxw) > bw) {
            ofs.left = ofs.left - w - ctxw;
        }
        if ((ofs.top + ctxh) > bh) {
            let h = $B.DomUtils.outerHeight(el);
            ofs.top = ofs.top - ctxh + h;
        }
        $ctx.style.top = ofs.top + "px";
        $ctx.style.left = ofs.left + "px";
    },
    isHightLight: function ($btn) {
        if ($B.DomUtils.hasClass($btn, editCfg.hightLightClz)) {
            return true;
        } else {
            return false;
        }
    },
    addHightLight: function ($btn) {
        $B.DomUtils.addClass($btn, editCfg.hightLightClz);
    },
    removeHightLight: function ($btn) {
        $B.DomUtils.removeClass($btn, editCfg.hightLightClz);
    },
    getBoldCss: function ($evEl) {
        let css;
        if (this.isHightLight($evEl)) {
            css = "normal";
            this.removeHightLight($evEl);
        } else {
            css = "bold";
            this.addHightLight($evEl);
        }
        return css;
    },
    fontWeightConver: function (spanCss) {
        if (spanCss["font-weight"] === "400") {
            spanCss["font-weight"] = "normal";
        } else {
            spanCss["font-weight"] = "bold";
        }
    },
    getitalicCss: function ($evEl) {
        let css;
        if (this.isHightLight($evEl)) {
            css = "normal";
            this.removeHightLight($evEl);
        } else {
            css = "italic";
            this.addHightLight($evEl);
        }
        return css;
    },
    indentPFn: function (indent, $p) {
        if (indent !== 0) {
            let $f = UTILS.getFirstTextEL($p);
            let size = parseFloat($B.DomUtils.css($f, "font-size").replace("px", ""));
            indent = indent * size;
        }
        $p.style.textIndent = indent + "px";
    },
    isEmptyEl: function (el) {
        let txt = el.innerText.replaceAll("\u200b", "");
        return txt === "";
    },
    createCtxItem: function (ctxMenus, $b, onCLick) {
        for (let i = 0; i < ctxMenus.length; i++) {
            let c = ctxMenus[i];
            $B.Dom.append($b, '<div fn="' + c.fn + '"  tabindex="0"  class="k_edit_tools_drop_item item" _focus="true"><i style="padding-right:6px;" class="fa ' + c.icon + '"></i>' + c.label + '</div>');
        }
        $B.Dom.click($b, (e) => {
            let el = e.target;
            if (el.nodeName === "I") {
                el = el.parentNode;
            }
            if ($B.Dom.hasClass(el, "item")) {
                onCLick(el, e);
            }
        });
    },
    removeBr: function ($p) {
        let brArr = $B.Dom.children($p, "br");
        for (let i = 0; i < brArr.length; i++) {
            $p.removeChild(brArr[i]);
        }
    },
    appendIfrPanel: function ($wap, onLoad) {
        let $ifr = $B.DomUtils.createEl('<iframe class="panel_ifr" frameborder="0" style="width:100%;height:100%;" scrolling="no"></iframe>');
        $B.DomUtils.append($wap, $ifr);
        let doc = $ifr.contentWindow.document;
        let childs = document.head.children;
        let source = [];
        for (let i = 0; i < childs.length; i++) {
            let child = childs[i];
            if (child.nodeName === "LINK" || child.nodeName === "SCRIPT") {
                if ($B.Dom.hasClass(child, "edit_source")) {
                    if (child.nodeName === "LINK") {
                        source.push(child.cloneNode(true));
                    } else {
                        let s = doc.createElement('script');
                        s.type = 'text/javascript';
                        s.src = child.src;
                        source.push(s);
                    }
                }
            }
        }
        let loading = source.length;
        let onLoadFn = function () {
            loading--;
        };
        var timetemp = new Date().getTime();
        var $body = doc.body;
        var $head = doc.head;
        for (let i = 0; i < source.length; i++) {
            let tag = source[i];
            if (tag.nodeName === "LINK") {
                tag.href = tag.href.indexOf("?") > 0 ? (tag.href + "&y=" + timetemp) : (tag.href + "?y=" + timetemp);
            } else {
                tag.src = tag.src.indexOf("?") > 0 ? (tag.src + "&y=" + timetemp) : (tag.src + "?y=" + timetemp);
            }
            tag.onload = onLoadFn;
            $B.Dom.append($head, source[i]);
        }
        let ivt = 0;
        let ivtr = setInterval(() => {
            if (ivt >= 100) {
                if (!this.ifrsourcelding) {
                    this.ifrsourcelding = $B.busyTip({
                        mask: true,
                        closeable: false, //是否关闭
                        title: editCfg.pagesCfg.waiting
                    });
                }
            }
            if (loading === 0) {
                clearInterval(ivtr);
                if (this.ifrsourcelding) {
                    this.ifrsourcelding.close();
                    this.ifrsourcelding = undefined;
                }
                onLoad($body, doc, $head);
            }
            ivt++;
            if (ivt > 2000) {
                clearInterval(ivtr);
                if (this.ifrsourcelding) {
                    this.ifrsourcelding.close();
                    this.ifrsourcelding = undefined;
                }
                $B.error(editCfg.pagesCfg.sourceErr);
            }
        }, 50);
    },
    runInnerJS: function ($head, _callInner, _returnInner, js) {
        window._callInner = _callInner;
        window._returnInner = _returnInner;
        let exeScript = document.createElement('script');
        exeScript.type = 'text/javascript';
        exeScript.innerHTML = js;
        $head.appendChild(exeScript);
        window._returnInner = undefined;
        window._callInner = undefined;
    },
    makePropFormAccor: function ($body, title, icon) {
        let $h = $B.Dom.append($body, "<h5 class='k_edit_prp_accr_title k_box_size'><span class='k_prp_title_span'>" + title + "</span><i style='position:absolute;top:6px;right:2px;' class='fa fa-angle-up'></i></h5>");
        if (icon) {
            $B.Dom.prepend($h, "<i class='fa " + icon + "'></i>");
        }
        let $table = $B.Dom.append($body, "<div style='padding:10px 2px 2px 12px;'></div>");
        if (!UTILS.accorFormEvs) {
            UTILS.accorFormEvs = {
                click: (e) => {
                    let nextEl = e.catpurer.nextSibling;
                    $B.toggle(nextEl, 159, () => {
                        let deg = "0"
                        if (nextEl.style.display === "none") {
                            deg = "180";
                        }
                        $B.animate(e.catpurer.lastChild, { "rotateZ": deg + "deg" }, 160);
                    });
                }
            };
        }
        $B.Dom.bind($h, UTILS.accorFormEvs);
        return $table;
    },
    isFindedInSelect:function($sel,val){
        let opts = $sel.children;
        let isFinded = false;
        for(let i =0 ;i < opts.length ;i++){
            if(opts[i].value === val){
                isFinded = true;
                break;
            }
        } 
        return isFinded;
    }
};
$B["Utils"] = UTILS;