var defaultOpts = {
    data: null, //'数据'
    isTree: false,
    multiple: false, //是否多选
    readOnly: false, //是否只读
    inputImdSearch: false,//是否输入即触发搜索
    getIdAndTxt:false, 
    search: true,  //输入搜索功能，当存在url的时候，优先url请求，否则本地数据搜索
    default: { id: '', text: '请您选择'},//默认选项
    url: null, //请求数据的地址【如果有些请求参数是固定不变的，请去url中设置】
    valueModel:'parent', //parent包括父节点，显示模式 chlidId（id取最后一个但是显示包括父节点），child（id和显示都是最后一个元素）
    textField: 'text', //菜单名称字段，默认为text
    idField: 'id', //菜单id字段,默认为id
    pidField: "pid",
    onSelected: null, //function (data) { },//点击选择事件
    onloaded: null //加载完成事件function (data) { }
};
class Combox extends $B.BaseControl {
    constructor(elObj, opts) {
        super();
        this.opts = $B.extendObjectFn(true, {}, defaultOpts, opts);
        super.setElObj(elObj);
        this.$input = this.elObj;
        this.isArrayValue = this.opts.multiple || this.opts.readOnly;
        if (this.opts.readOnly) {
            $B.DomUtils.attribute(this.$input, { readonly: true });
        }
        let inputAttr = { "id": this.id + "_input", "autocomplete": "off" };
        if (this.opts.multiple) {
            inputAttr["multiple"] = true;
        }
        $B.DomUtils.attribute(this.$input, inputAttr);
        $B.DomUtils.addClass(this.elObj, "k_box_size k_combox_input");
        $B.DomUtils.css(this.elObj, { "padding-right": "18px" });
        this.srcHeight = $B.DomUtils.height(this.elObj);
        var top = (this.srcHeight - 14) / 2;
        var wrap = $B.DomUtils.createEl("<span id='" + this.id + "' class='k_combox_wrap'  outline='0' ></span>");
        
        //这里需要考虑元素被声明了位置的情况
        let posAttr = $B.DomUtils.css(this.elObj, "position");
        if (posAttr !== "static") {
            let cssinf = $B.DomUtils.domInfo(this.elObj);
            $B.DomUtils.css(wrap, { position: posAttr, top: cssinf.top, left: cssinf.left });
            this.elObj.style.position = "static";
        }
        $B.DomUtils.after(this.elObj, wrap);
        var classList = this.$input.classList;
        let wCls,wminWidth = $B.DomUtils.outerWidth(this.$input);
        if(classList){
            let rTest = /^form_input\d+$/; 
            for(let i =0 ;i < classList.length ;i++){
                let cls = classList[i];
                if(rTest.test(cls)){
                    wCls = cls;                   
                    break;
                }
            }            
        }
        $B.DomUtils.css(wrap,{"min-width":wminWidth+"px"});
        $B.DomUtils.remove(this.elObj);
        if(wCls){            
            $B.DomUtils.removeClass(this.$input,wCls);
            $B.DomUtils.css(this.$input,{width:'100%'});            
        }
        $B.DomUtils.append(wrap, this.elObj);
        this._bindInputEvent(this.elObj);
        super.setElObj(wrap);
        this.maxWidth = $B.DomUtils.innerWidth(this.elObj.parentNode) - 35;
        $B.DomUtils.attribute(this.$input, { "max_width": this.maxWidth, "src_width": $B.DomUtils.width(this.$input) });        
        if (this.isArrayValue) {
            if (!this.opts.readOnly) {
                this.maxWidth = this.maxWidth - 80;
            }
            $B.DomUtils.append(this.elObj, "<span maxwidth='" + this.maxWidth + "' style='position:absolute;top:0;left:0;display:block;max-width:" + this.maxWidth + "px;' class='k_combox_inner_wap clearfix'></span>");
        }
        this.srcWidth = $B.DomUtils.width(this.$input);
        $B.DomUtils.attribute(wrap, { "fix_width": this.srcWidth });
        if(this.opts.selectedValue){
            if(Array.isArray(this.opts.selectedValue)){
                this.$input.selectedValue = this.opts.selectedValue;
            }else{
                this.$input.selectedValue = [this.opts.selectedValue];
            }            
        }else{
            this.$input.selectedValue = [];
        }       
        this.$icon = $B.DomUtils.createEl("<i style='position:absolute;right:3px;top:" + top + "px' class='fa fa-down-open-big k_dropdown_list_el'></i>");
        $B.DomUtils.append(this.elObj, this.$icon);
        this.height = $B.DomUtils.height(this.elObj);
        let dataArray = [];
        if (this.opts.data) {
            dataArray = this.opts.data;
        } else {
            this.opts.data = dataArray;
        }
        this._createDropList(dataArray);
        if (dataArray.length === 0 && this.opts.url) {//发起请求
            this.request({}, (data) => {
                this.opts.data = data;
                let $inner = this.$dropEl.lastChild.firstChild.firstChild;
                if (this.opts.isTree) {
                    let _itlen = parseInt($B.DomUtils.attribute(this.$dropEl, "_itlen"));
                    let ret = this.dropOpts.onCreate(data, this.dropOpts, $inner, this.$input);
                    _itlen = _itlen + ret._itlen;
                    $B.DomUtils.attribute(this.$dropEl, { "_itlen": _itlen });
                } else {
                    $B._createDropListItems(data, this.dropOpts, $inner, this.$input, true);
                }
                this._renderReadonlyUI();
            });
        }
    }
    /*****
     * 树形事件
     * ******/
    _bindTreeEevents($inner) {
        var _this = this;
        if (this.hasBindedTreeEv) {
            return;
        }
        this.hasBindedTreeEv = true;
        $B.DomUtils.bind($inner, {
            click: function (e) {
                let isI = e.target.tagName === "I";
                if (isI || $B.DomUtils.hasClass(e.target, "_tree_prt")) { //父节点点击
                    let $i = e.target;
                    if (!isI) {
                        $i = $i.firstChild;
                    }
                    if ($B.DomUtils.hasClass($i, "fa-folder-empty")) {
                        $B.DomUtils.removeClass($i, "fa-folder-empty");
                        $B.DomUtils.addClass($i, "fa-folder-open-empty");
                        let ul = $i.parentNode.parentNode.parentNode.nextSibling;
                        $B.slideDown(ul, 180);
                        if (ul.children.length === 0) {
                            let pid = $B.DomUtils.attribute(ul.previousSibling, "dataid");
                            let deep = parseInt($B.DomUtils.attribute(ul.parentNode, "deep"));
                            let param = { pid: pid };
                            $B.DomUtils.removeClass($i, "fa-folder-open-empty");
                            $B.DomUtils.addClass($i, "fa-spin3 animate-spin");
                            let url = _this.opts.url;
                            let method = (url.indexOf(".data") > 0 || url.indexOf(".json") > 0) ? "GET" : "POST";
                            if (_this.opts.setParams) {
                                let pr = _this.opts.setParams(param);
                                if (pr) {
                                    param = $B.extendObjectFn(true, param, pr);
                                }
                            }
                            $B.request({
                                dataType: 'json',
                                url: url,
                                data: param,
                                type: method,
                                onErrorEval: true,
                                onReturn: function () {
                                    $B.DomUtils.addClass($i, "fa-folder-open-empty");
                                    $B.DomUtils.removeClass($i, "fa-spin3 animate-spin");
                                },
                                ok: function (message, data) {
                                    if (!data || data.length === 0) {
                                        $B.toolTip($i, ($B.config && $B.config.returnEmptyData) ? $B.config.returnEmptyData : 'the return is empty!', 2);
                                        return;
                                    }
                                    if (_this.opts.onloaded) {
                                        _this.opts.onloaded(data, param);
                                    }
                                    let $inner = _this.$dropEl.lastChild.firstChild.firstChild;
                                    let sH = $B.DomUtils.height($inner);
                                    let len = _this._createTreeUI(data, _this.dropOpts, ul, _this.$input, deep + 1); 
                                    let srlen = $B.DomUtils.attribute(_this.$dropEl,"_itlen");
                                    if(!srlen){
                                        srlen = 0;
                                    }
                                    len = len + parseInt(srlen);
                                    $B.DomUtils.attribute(_this.$dropEl,{"_itlen":len});
                                    setTimeout(() => {
                                        let eH = $B.DomUtils.height($inner);
                                        if(eH > sH){ //高度扩展
                                            let diff = eH - sH;
                                            let ofs = $B.DomUtils.offset(_this.$dropEl);
                                            let $b =  $B.getBody();
                                            let avH = $B.DomUtils.height($b) - ofs.top - sH;
                                            if(diff > avH){
                                                diff - avH - 20;
                                            }
                                            sH = sH + diff;
                                            $B.DomUtils.height(_this.$dropEl,sH);
                                        }
                                        $B.DomUtils.trigger($inner.parentNode, "myscrollabr.mouseenter");
                                    }, 200);
                                }
                            });
                        } else {
                            setTimeout(() => {
                                let $inner = _this.$dropEl.lastChild.firstChild.firstChild;
                                $B.DomUtils.trigger($inner.parentNode, "myscrollabr.mouseenter");
                            }, 200);
                        }
                    } else {
                        $B.DomUtils.addClass($i, "fa-folder-empty");
                        $B.DomUtils.removeClass($i, "fa-folder-open-empty");
                        let ul = $i.parentNode.parentNode.parentNode.nextSibling;
                        $B.slideUp(ul, 200);
                    }
                } else {
                    let el = e.target;
                    while (el) {
                        if ($B.DomUtils.hasClass(el, "k_dropdown_list_item")) {
                            break;
                        }
                        el = el.parentNode;
                    }
                    let dataid = $B.DomUtils.attribute(el, "dataid");
                    let $txt = $B.DomUtils.findByClass(el, "._combox_text")[0];
                    let data = { id: [dataid], text: [$txt.innerText] };
                    let isSelected = true;
                    if (_this.opts.multiple) { //如果是多选
                        if ($B.DomUtils.hasClass(el, "k_dropdown_item_selected")) {//执行取消
                            $B.DomUtils.removeClass(el, "k_dropdown_item_selected");
                            isSelected = false;
                        } else {
                            $B.DomUtils.addClass(el, "k_dropdown_item_selected");
                        }
                    } else {//单选   
                        isSelected = !$B.DomUtils.hasClass(el, "k_dropdown_item_selected");
                        _this._unSelectedSiblings($inner.firstChild.firstChild.firstChild.children);
                        if (isSelected) {
                            $B.DomUtils.addClass(el, "k_dropdown_item_selected");
                        }
                        _this.$dropEl.hideFn();//单选自动收起
                    }
                    if(_this.opts.valueModel !== "child"){
                        let li = el.parentNode;
                        _this._getParentDataPath(li, data);
                    }
                    if(_this.opts.valueModel === "childId"){
                        data.id = [data.id[data.id.length-1]];
                    }                   
                    if(data.id.length > 1){
                        data.id = data.id.join(";");
                    }else{
                        data.id = data.id.join("");
                    }
                    if(data.text.length > 1){
                        data.text = data.text.join("/");
                    }else{
                        data.text = data.text.join("");
                    }
                    
                    _this._onClick(data, isSelected);
                }
            }
        });
    }
    _getParentDataPath(li, data) {
        let deep = parseInt($B.DomUtils.attribute(li, "deep"));
        if (deep > 0) {
            var ul = li.parentNode;
            var $it = ul.previousSibling;
            let $txt = $B.DomUtils.findByClass($it, "._combox_text")[0];
            data.id.unshift($B.DomUtils.attribute($it, "dataid"));
            data.text.unshift($txt.innerText);
            this._getParentDataPath($it.parentNode, data);
        }
    }
    _unSelectedSiblings(listLi) {
        for (let i = 0; i < listLi.length; i++) {
            let li = listLi[i];
            $B.DomUtils.removeClass(li.firstChild, "k_dropdown_item_selected");
            let childUl = li.firstChild.nextSibling;
            if (childUl && childUl.children) {
                this._unSelectedSiblings(childUl.children);
            }
        }
    }
    /***
     * data = {id:text}
     * ***/
    _onClick(data, isSelected) {
        let _this = this;
        clearTimeout(_this.blurDefaultTimer);
        var oldValue = this.getCheckData();//_this.$input.selectedValue;
        let retVal, attr;
        if (_this.isArrayValue) { //多选或者只读
            if (isSelected) {
                if (!_this.opts.multiple) {
                    _this.$input.selectedValue = [];
                }
                _this.$input.selectedValue.push(data);
            } else {
                let newVals = [];
                let values = _this.$input.selectedValue;
                for (let i = 0; i < values.length; i++) {
                    if ((values[i].id + "") !== data.id) {
                        newVals.push(values[i]);
                    }
                }
                _this.$input.selectedValue = newVals;
            }
            attr = _this._renderReadonlyUI();
            if (!_this.opts.multiple) {
                retVal = true;
            }
        } else {
            _this.$input.value = data.text;
            _this.$input.selectedValue = [data];
            attr = _this._renderReadonlyUI();
            retVal = true;
        }
        let newData = this.getCheckData();
        this._notify(oldValue, newData);
        return retVal;
    }
    _notify(oldValue, data) {
        if (this.opts.onSelected) {
            clearTimeout(this.onSelectedTimer);
            this.onSelectedTimer = setTimeout(() => {
                this.opts.onSelected(data);
            }, 1);
        }
        if (this.opts.onChange) {
            clearTimeout(this.onChangeTimer);
            this.onChangeTimer = setTimeout(() => {
                this.opts.onChange(this.elObj.id, data, oldValue);
            }, 1);
        }
    }
    _loopTreeUl($ul, selectIdMap, selectedArr, txtPathArr, idPathArr) {
        let childs = $ul.children;
        for (let i = 0; i < childs.length; i++) {
            let childTxtArr, childIdArr;
            if (!idPathArr) {
                childIdArr = [];
            } else {
                childIdArr = Array.from(idPathArr);
            }
            if (!txtPathArr) {
                childTxtArr = [];
            } else {
                childTxtArr = Array.from(txtPathArr);
            }
            let $t = childs[i].firstChild;
            if ($B.DomUtils.hasClass($t, "k_dropdown_item_selected")) {
                $B.DomUtils.removeClass($t, "k_dropdown_item_selected");
            }
            let dataId = $B.DomUtils.attribute($t, "dataid");
            let $wap = $t.firstChild;
            let $txt = $B.DomUtils.children($wap, "._combox_text")[0];
            childTxtArr.push($txt.innerText);
            childIdArr.push(dataId);
            if (selectIdMap[dataId]) {
                selectedArr.push({ id: childIdArr.join(";"), text: childTxtArr.join("/") });
                $B.DomUtils.addClass($t, "k_dropdown_item_selected");
            }
            if ($t.nextSibling) {
                this._loopTreeUl($t.nextSibling, selectIdMap, selectedArr, childTxtArr, childIdArr);
            }
        }
    }
    setValue(args, notify){  
        if($B.isPlainObjectFn(args)){
            if(Array.isArray(args.id)){
                args = args.id;
            }
        }    
        this.setSelected(args,notify);
    }
    getCheckData(onlyId){
        if(typeof onlyId === "undefined"){
            onlyId = !this.opts.getIdAndTxt;
        }
        var values = this.$input.selectedValue ;
        var ret =[];
        for(let i =0 ; i < values.length ; i++){
            let id = values[i].id;
            let text = values[i].text;
            if(id.indexOf(";") > 0){
                id = id.split(";");
                id = id[id.length - 1];
                text = text.split("/");
                text = text[text.length - 1];
            }
            if(onlyId){
                ret.push(id);
            }else{
                ret.push({id:id,text:text});
            }
        }
        return ret;
    }
    /**
     *  args = [id1,id2,id3];
     *  notify: 是否触发通知
     * ***/
    setSelected(args, notify) {
        let idMap = {};
        let selectedArr = [];
        if (Array.isArray(args)) {
            for (let i = 0; i < args.length; i++) {
                idMap[args[i]] = true;
            }
        } else {
            idMap[args] = true;
        }
        if (this.opts.isTree) {
            this._loopTreeUl(this.$ul, idMap, selectedArr, undefined);
        } else {
            var $wap = this.$dropEl.lastChild.firstChild.firstChild;
            var childs = $wap.children;
            for (let i = 0; i < childs.length; i++) {
                let $t = childs[i];
                let dataid = $B.DomUtils.attribute($t, "dataid");
                if ($B.DomUtils.hasClass($t, "k_dropdown_item_selected")) {
                    $B.DomUtils.removeClass($t, "k_dropdown_item_selected");
                }
                if (idMap[dataid]) {
                    selectedArr.push({ id: dataid, text: $t.innerText });
                    $B.DomUtils.addClass($t, "k_dropdown_item_selected");
                }
            }
        }
        if(selectedArr.length > 0){
            let oldValue = this.getCheckData();
            this.$input.selectedValue = selectedArr;
            this._renderReadonlyUI();
            if (typeof notify === "undefined" || notify) {
                this._notify(oldValue, this.getCheckData());
            }
        }       
    }
    clearSelected(notify) {
        let oldValue = this.getCheckData();
        this.$input.selectedValue = [];
        this._renderReadonlyUI();
        if (typeof notify === "undefined" || notify) {
            this._notify(oldValue, this.getCheckData());
        }
    }
    _createDropList(data) {
        var _this = this;
        let ddOpts = {
            multiple: this.opts.multiple,
            textField: this.opts.textField,
            idField: this.opts.idField,
            motionless: true,
            onClick: function (data, target, isSelected) {
                return _this._onClick(data, isSelected);
            }
        };
        if (this.opts.isTree) {
            //拦截创建，树形列表
            function createFilterFn(items, opts, $inner, $input) { //树形列表的创建                
                let ul = $B.DomUtils.createEl("<ul class='k_combox_tree_ul'/>");
                _this.$ul = ul;
                _this.selectedLiArray = [];
                let len = _this._createTreeUI(items, opts, ul, $input, 0);
                $inner.appendChild(ul);
                _this._renderTreeSelectedUI();
                setTimeout(() => {
                    _this._bindTreeEevents($inner);
                    $B.DomUtils.trigger($inner.parentNode, "myscrollabr.mouseenter");
                }, 1);
                return { _itlen: len, defautlClick: false };
            };
            ddOpts.onCreate = createFilterFn;
        }
        this.$dropEl = $B.createDropList(this.elObj, data, ddOpts);
        this.dropOpts = ddOpts;
        this._renderReadonlyUI();
    }
    _renderTreeSelectedUI() {
        for (let i = 0; i < this.selectedLiArray.length; i++) {
            let li = this.selectedLiArray[i];
            let $it = li.firstChild;
            let id = $B.DomUtils.attribute($it, "dataid");
            let txt = $B.DomUtils.findByClass($it, "._combox_text")[0].innerText;
            let data = { id: [id], text: [txt] };
            this._getParentDataPath(li, data);
            data.id = data.id.join(";");
            data.text = data.text.join("/");
            this._onClick(data, true);
        }
        this.selectedLiArray = [];
    }
    _createTreeUI(items, opts, $ul, $input, deep) {
        let len = items.length;
        for (let i = 0; i < items.length; i++) {
            let _deep = deep;
            let it = items[i];
            let id = items[i][opts.idField]
            let li = $B.DomUtils.createEl("<li deep='" + deep + "'><div class='k_dropdown_list_item' dataid='" + id + "'><div class='k_combox_tree_item_wrap' style='display: inline-block;'></div></div></li>");
            let div = li.firstChild.firstChild;
            if (it.selected) {
                if (!this.opts.multiple) {
                    if (this.selectedLiArray.length === 0) {
                        $B.DomUtils.addClass(li.firstChild, "k_dropdown_item_selected");
                        this.selectedLiArray.push(li);
                    }
                } else {
                    $B.DomUtils.addClass(li.firstChild, "k_dropdown_item_selected");
                    this.selectedLiArray.push(li);
                }
            }
            while (_deep > 0) {
                $B.DomUtils.append(div, "<div class='_deep_helper'></div>");
                _deep--;
            }
            if (it.children) {
                let cls = "fa-folder-open-empty";
                if (it.children.length === 0) {
                    cls = "fa-folder-empty";
                }
                $B.DomUtils.append(div, "<div class='_deep_helper _tree_prt'><i class='fa " + cls + "'></i></div>");
            }
            $B.DomUtils.append(div, "<span class='_combox_text'>" + it[this.opts.textField] + "</span>");
            $B.DomUtils.append($ul, li);
            if (it.children) {
                let childUl = $B.DomUtils.createEl("<ul class='k_combox_tree_ul'></ul>");
                len = len + this._createTreeUI(it.children, opts, childUl, $input, deep + 1);
                $B.DomUtils.append(li, childUl);
            }
        }
        return len;
    }
    _renderReadonlyUI() {       
        let attr;
        let $wap = $B.DomUtils.children(this.elObj, ".k_combox_inner_wap")[0];
        let maxW = parseInt($B.DomUtils.attribute(this.$input, "max_width"));      
        let srcW = parseInt($B.DomUtils.attribute(this.$input, "src_width"));
        let maxWidth = maxW;
        if (this.isArrayValue) { //多选或者只读           
            $B.DomUtils.removeChilds($wap, ".k_combox_user_selected");
            let values = this.$input.selectedValue;
            this.$input.placeholder = "";
            this.$input.value =  "";
            if (values.length === 0) {
                this.$input.placeholder = this.opts.default.text;                
            }
            let idARR = [];
            let txtARR = [];
            let wapHeight = 0;
            let w = 0;           
            if(values.length === 0){
                this.$input.style.width = srcW + "px";
            }
            for (let i = 0; i < values.length; i++) {
                let data = values[i];                
                let it = $B.DomUtils.createEl("<span  style='' dataid='" + data.id + "' class='k_combox_user_selected k_box_size'>" + data.text + "<i class='fa fa-cancel-2'></i></span>");
                $B.DomUtils.append($wap, it);
                this._bindItemEvents(it);
                idARR.push(data.id);
                txtARR.push(data.text);
                let tmpH = $B.DomUtils.height($wap);
                if (wapHeight === 0) {
                    wapHeight = tmpH;
                }
                w = w + $B.DomUtils.outerWidth(it) + 14;
                //当高度发生变化
                if (wapHeight !== tmpH) {
                    if (w < maxWidth) {
                        $wap.style.width = w + "px";
                    }
                    wapHeight = $B.DomUtils.height($wap);
                    let wapW = $B.DomUtils.width($wap);
                    let tmp = wapW;
                    if (!this.opts.readOnly) {
                        tmp = wapW + 80;
                    }
                    if (tmp > maxW) {
                        wapW = maxW;
                    } else {
                        wapW = tmp;
                    }
                    if (wapW < srcW) {
                        wapW = srcW;
                    }
                    this.$input.style.width = wapW + "px";
                }else{
                    w =w + 5;
                    if(w > srcW){
                        if(w < maxWidth){
                            this.$input.style.width = w + "px";
                        }else{
                            this.$input.style.width = maxWidth + "px";
                        }
                    }else{
                        this.$input.style.width = srcW + "px";
                    }           
                }
            }
            attr = { "_id": idARR.join(","), "_text": txtARR.join(",") };
            $B.DomUtils.attribute(this.$input, attr);
            if (wapHeight === 0) {
                wapHeight = 28;
            }
            wapHeight = wapHeight + 4;
            let inputCss = { height: wapHeight, "line-height": wapHeight };
            if (wapHeight < 40) {
                if (!this.opts.readOnly) {
                    w = w - values.length * 4;
                    if (w < 6) {
                        w = 6;
                    }
                    inputCss["padding-left"] = w + "px";
                }
            }
            $B.DomUtils.css(this.$input, inputCss);          
            if (!this.opts.readOnly) {
                this.$input.focus();
            }
            //调整位置
            this.fitListPos();
        } else {
            attr = { "_id": "", "_text": "" };
            if (this.$input.selectedValue.length === 0) {
                this.$input.value = "";
                this.$input.placeholder = this.opts.default.text;
                this.$input.style.width = srcW + "px";
            } else {
                attr["_id"] = this.$input.selectedValue[0].id;
                attr["_text"] = this.$input.selectedValue[0].text;
                let w = $B.getCharWidth(attr["_text"]) + 20;
                this.$input.value = attr["_text"];
                if(w > srcW){
                    if(w < maxWidth){
                        this.$input.style.width = w + "px";
                    }else{
                        this.$input.style.width = maxWidth + "px";
                    }
                }else{
                    this.$input.style.width = srcW + "px";
                }     
            }
            $B.DomUtils.attribute(this.$input, attr);
        }        
        return attr;
    }
    fitListPos() {
        if(this.$dropEl && $B.DomUtils.css(this.$dropEl,"display") !== "none"){
            let $wap = $B.DomUtils.children(this.elObj, ".k_combox_inner_wap")[0];
            let wapHeight = $B.DomUtils.height($wap);
            if(wapHeight < 28){
                wapHeight = 28;
            }
            let ofs = $B.DomUtils.offset(this.elObj);
            let top = ofs.top + wapHeight + 12;
            this.$dropEl.style.top = top + "px";
        }        
    }
    /****
     * 删除选择事件
     * ******/
    _bindItemEvents($it) {
        if (!this.itemClick) {
            var _this = this;
            this.itemClick = function (e) {
                if (e.target.tagName === "I") {
                    let pt = e.target.parentNode;
                    let id = $B.DomUtils.attribute(pt, "dataid");
                    let values = _this.$input.selectedValue;
                    var newVals = [];
                    for (let i = 0; i < values.length; i++) {
                        if (values[i].id + '' !== id) {
                            if (_this.opts.isTree) {
                                let idArr = values[i].id.split(";");
                                newVals.push(idArr[idArr.length - 1]);
                            } else {
                                newVals.push(values[i].id);
                            }
                        }
                    }
                    if(newVals.length > 0){
                        _this.setSelected(newVals);
                    }else{
                        _this.clearSelected();
                    }                   
                    return false;
                }
            };
        }
        $B.DomUtils.click($it, this.itemClick);
    }
    _bindInputEvent($input) {
        var _this = this;
        this.inputEvents = {
            focus: function (e) {
                $B.DomUtils.addClass(_this.$icon, "k_combox_focus_icon");
            },
            blur: function (e) {
                $B.DomUtils.removeClass(_this.$icon, "k_combox_focus_icon");
                if (_this.isArrayValue) {
                    let $wap = $B.DomUtils.children(_this.elObj, ".k_combox_inner_wap");
                    let childs = $B.DomUtils.children($wap, ".k_combox_user_selected");
                    if (childs.length === 0) {
                        _this.blurDefaultTimer = setTimeout(() => {
                            _this.$input.selectedValue = [];
                            _this.$input.value = "";
                            _this.$input.placeholder = _this.opts.default.text;
                        }, 800);
                    }
                    if (!_this.opts.readOnly) {
                        _this.$input.value = "";
                    }
                } else {
                    if (this.value === "") {
                        _this.blurDefaultTimer = setTimeout(() => {
                            _this.$input.selectedValue = _this.opts.default;
                            _this.$input.value = "";
                            _this.$input.placeholder = _this.opts.default.text;
                        }, 800);
                    }
                }
            }
        };
        if (!this.opts.readOnly && this.opts.search) {
            this.inputEvents.input = function (e, imd) {
                if (imd) {
                    _this.userSearch($B.trimFn(this.value));
                } else if (_this.opts.inputImdSearch) {
                    clearTimeout(_this.userInputSearchTimer);
                    _this.userInputSearchTimer = setTimeout(() => {
                        _this.userSearch($B.trimFn(this.value));
                    }, 800);
                }
            };
            this.inputEvents.keydown = function (e) {
                if (e.keyCode === 13) {
                    _this.inputEvents.input.call(this, e, true);
                }
            };
        }
        $B.DomUtils.bind($input, this.inputEvents);
    }
    userSearch(text) {
        if (this.$dropEl) {
            this.$dropEl.showFn();
            text = text.toLowerCase();
            if (this.opts.url) {
                this.request({ keyword: text }, (data) => {
                    let $inner = this.$dropEl.lastChild.firstChild.firstChild;
                    if (this.opts.isTree) {
                        let _itlen = parseInt($B.DomUtils.attribute(this.$dropEl, "_itlen"));
                        let ret = this.dropOpts.onCreate(data, this.dropOpts, $inner, this.$input);
                        _itlen = _itlen + ret._itlen;
                        $B.DomUtils.attribute(this.$dropEl, { "_itlen": _itlen });
                    } else {
                        $B._createDropListItems(data, this.dropOpts, $inner, this.$input, true);
                    }
                    this._renderReadonlyUI();
                });
            } else { //本地搜索
                let $wrap = this.$dropEl.lastChild.firstChild.firstChild;
                if (this.opts.isTree) {
                    console.log("本地树搜索,待开发，，.......");
                } else {
                    for (let i = 0; i < $wrap.children.length; i++) {
                        let it = $wrap.children[i];
                        if (text === "" || it.textContent.toLowerCase().indexOf(text) >= 0) {
                            it.style.display = "block";
                        } else {
                            it.style.display = "none";
                        }
                    }
                }
            }
        }
    }
    request(param, callBack) {
        if (this.opts.setParams) {
            let pr = this.opts.setParams(param);
            if (pr) {
                param = $B.extendObjectFn(true, param, pr);
            }
        }
        let _this = this;
        if (!this.loading) {
            this.loading = $B.getLoadingEl(undefined, "#fff");
            var wrap = this.$dropEl.lastChild.firstChild.firstChild;
            wrap.innerHTML = "";
            $B.DomUtils.append(wrap, this.loading);
        }
        let url = this.opts.url;
        let method = (url.indexOf(".data") > 0 || url.indexOf(".json") > 0) ? "GET" : "POST";
        $B.request({
            dataType: 'json',
            url: url,
            data: param,
            type: method,
            onErrorEval: true,
            onReturn: function () {
                let $l = _this.loading;
                _this.loading = undefined;
                if ($l) {
                    try {
                        $B.removeLoading($l, () => {                            
                        });
                    } catch (ex) {
                        console.log(ex);
                    }
                }
            },
            ok: function (message, data) {
                if (!data || data.length === 0) {
                    data = [];
                }
                if (_this.opts.onloaded) {
                    _this.opts.onloaded(data, param);
                }
                callBack(data);
            }
        });
    }
    destroy(isForce) {
        this.$dropEl._clearAllFn();
        this.elObj.selectedValue = undefined;
        $B.DomUtils.remove(this.$dropEl);
        super.destroy();
    }
}
$B["Combox"] = Combox;