var defaultOpts = {
    title: '', //标题
    isTop: false,
    iconCls: null, //图标class，font-awesome字体图标
    iconColor: undefined,//图标颜色
    headerColor: undefined,//头部颜色
    toolbar: null, //工具栏对象参考工具栏组件配置说明，可以是创建函数
    toolbarStyle: undefined,//工具栏样式定义
    size: { width: 'auto', height: 'auto' },
    shadow: true, //是否需要阴影
    radius: undefined, //圆角px定义
    header: true, //是否显示头部
    zIndex: 2147483647,//层级
    content: null, //静态内容
    url: '',//请求地址
    dataType: 'html', //当为url请求时，html/json/iframe
    closeType: 'hide', //关闭类型 hide(隐藏，可重新show)/ destroy 直接从dom中删除    
    moveProxy: false, //是否代理移动方式
    draggableHandler: 'header', //拖动触发焦点
    triggerHide:false,
    closeable: false, //是否关闭
    draggable: false, //是否可以拖动
    expandable: false, //可左右收缩
    maxminable: false, //可变化小大
    collapseable: false, //上下收缩
    resizeable: false,//右下角拖拉大小
    onResized: null, //function (pr) { },//大小变化事件
    onLoaded: null, //function () { },//加载后
    onClose: null, //关闭前
    onClosed: null, //function () { },//关闭后
    onExpanded: null, // function (pr) { },//左右收缩后
    onCollapsed: null, // function (pr) { }//上下收缩后
    onCreated: null //function($content,$header){} panel创建完成事件
};

var footToolbarHtml = '<div class="k_panel_foot_toolbar k_box_size" ></div>';
var contentHtml = '<div class="k_panel_content k_box_size" ></div>';
var headerIconHtml = '<div style="padding-right:1px;height:100%;text-align:center;" class="k_panel_header_icon  btn"><i style="font-size:16px;line-height:1.5em;" class="fa {icon}">\u200B</i></div>';
var headerHtml = '<div style="height:30px" class="k_panel_header"><div style="width:100%;height:100%;" class="k_panel_header_content"><h1 class="k_box_size" style="white-space:nowrap;text-overflow:ellipsis;overflow:hidden;">{title}</h1><div class="k_panel_header_toolbar k_box_size"></div></div></div>';

class Panel extends $B.BaseControl {
    constructor(elObj, opts) {
        super();
        var _this = this,
            $h1,
            headerHeight = 0,
            headerTop = 0;
        super.setElObj(elObj);
        $B.DomUtils.addClass(this.elObj, "k_panel_main k_box_size");
        this.opts = $B.extendObjectFn(true, {}, defaultOpts, opts);
        if (this.opts.shadow) {
            $B.DomUtils.addClass(this.elObj, "k_box_shadow");
        }
        if (this.opts.radius) {
            $B.DomUtils.css(this.elObj, { "border-radius": this.opts.radius });
        }
        let paddingRgt = 0;
        if (this.opts.header) {
            if (this.opts.expandable) {
                paddingRgt = 20;
            }
            if (this.opts.closeable) {
                paddingRgt = paddingRgt + 20;
            }
            if (this.opts.maxminable) {
                paddingRgt = paddingRgt + 20;
            }
            if (this.opts.collapseable) {
                paddingRgt = paddingRgt + 20;
            }
            if (typeof this.opts.title === "string") {
                this.$header = $B.DomUtils.createEl(headerHtml.replace("{title}", this.opts.title));
                $h1 = $B.DomUtils.findBySelector(this.$header, "h1");
            } else {
                this.$header = $B.DomUtils.createEl(headerHtml.replace("{title}", ""));
                $h1 = $B.DomUtils.findBySelector(this.$header, "h1");
                $B.DomUtils.append($h1, this.opts.title);
            }
            $B.DomUtils.append(this.elObj, this.$header);
            headerHeight = $B.DomUtils.outerHeight(_this.$header);
            $B.DomUtils.css($h1, { "line-height": headerHeight + "px", "padding-right": paddingRgt + "px" });
        }
        this.$body = $B.DomUtils.createEl(contentHtml);
        var bodyCss = { "border-top": "solid #fff " + headerHeight + "px" };
        if (this.opts.bodyCss) {
            $B.extendObjectFn(bodyCss, this.opts.bodyCss);
        }
        $B.DomUtils.css(this.$body, bodyCss);
        $B.DomUtils.prepend(this.elObj, this.$body);
        //加入工具栏功能
        this.toolArray = [];
        var toolbar;
        if ($B.Toolbar) {/***依赖toobar组件***/
            let paddingBottom = "2px";
            if (typeof this.opts.toolbar === 'function') { //由外部创建工具栏
                toolbar = $B.DomUtils.createEl(footToolbarHtml);
                this.opts.toolbar.call(toolbar);
                if (this.$header) {
                    $B.DomUtils.before(this.$header, toolbar);
                } else {
                    $B.DomUtils.append(this.elObj, toolbar);
                }
            } else if (this.opts.toolbar) {
                toolbar = $B.DomUtils.createEl(footToolbarHtml);
                if (this.$header) {
                    $B.DomUtils.before(this.$header, toolbar);
                } else {
                    $B.DomUtils.append(this.elObj, toolbar);
                }
                var toolbarOpts;
                if (Array.isArray(this.opts.toolbar)) {
                    toolbarOpts = { buttons: this.opts.toolbar, "align": "center" };
                    if (this.opts.toolbarAlign) {
                        toolbarOpts.align = this.opts.toolbarAlign;
                    }
                } else {
                    toolbarOpts = this.opts.toolbar;
                }
                if (this.opts.toolbarStyle) {
                    $B.extendObjectFn(toolbarOpts, this.opts.toolbarStyle);
                }
                if(toolbarOpts.align && toolbarOpts.align === "100%"){
                    paddingBottom = "0px";
                }
                let toolbarIns = new $B.Toolbar(toolbar, toolbarOpts);
                this.toolArray.push(toolbarIns);
            }
            if (toolbar) {               
                let h = $B.DomUtils.outerHeight(toolbar);
                $B.DomUtils.css(toolbar, { "padding-bottom": paddingBottom });
                $B.DomUtils.css(this.$body, { "border-bottom": h + "px solid #fff" });
            }
        }
        this._$outerBody = this.$body;
        if ($B.myScrollbar && this.opts.dataType !== "iframe") {
            this.$body = $B.myScrollbar(this.$body, {});
            $B.DomUtils.css(this.$body, { "padding":"2px 4px"});
        } else {
            $B.DomUtils.css(this.$body, { "overflow": "auto" ,"padding":"2px 4px"});
        }
        var isUrl = true;
        if (this.opts.content !== null && this.opts.content !== "") {
            isUrl = false;
            $B.DomUtils.append(this.$body, this.opts.content);
        }
        $B.DomUtils.css(this.elObj, this.opts.size);
        if (this.opts.iconCls != null && this.opts.header) {
            let $icon = $B.DomUtils.createEl(headerIconHtml.replace("{icon}", this.opts.iconCls));
            $B.DomUtils.css($icon, { "line-height": headerHeight + "px" });
            $B.DomUtils.prepend($B.DomUtils.children(this.$header, ".k_panel_header_content"), $icon);
            $icon = $B.DomUtils.children($icon, "i");
            if (this.opts.iconColor) {
                $B.DomUtils.css($icon, { "color": this.opts.iconColor });
            }
        }
        let $toolbar;
        if (this.opts.header) {
            $toolbar = $B.DomUtils.children(this.$header, ".k_panel_header_content");
            $toolbar = $B.DomUtils.children($toolbar, ".k_panel_header_toolbar");
            if (this.opts.expandable) { //可左右收缩
                let $a = $B.DomUtils.createEl("<a style='line-height:" + headerHeight + "px' class='k_panel_tool_icon'><i class='fa fa-left-open-2'></i></a>");
                $B.DomUtils.append($toolbar, $a);
                $B.DomUtils.click($a, function () {
                    let $_t = $B.DomUtils.children(this, "i");
                    if ($B.DomUtils.hasClass($_t, "fa-left-open-2")) {
                        $B.DomUtils.addClass($_t, "fa-right-open-2");
                        $B.DomUtils.removeClass($_t, "fa-left-open-2");
                        var allEls = $B.DomUtils.children(_this.elObj);
                        $B.fadeOut(allEls, 100, () => {
                            _this.expandWidth = $B.DomUtils.css(_this.elObj, "width");
                            $B.animate(_this.elObj, {
                                width: 30
                            }, {
                                duration: 100,
                                complete: () => {
                                    $B.DomUtils.addClass(_this.elObj, "k_panel_body_retractile");
                                    let $e = $B.DomUtils.createEl("<a style='display:block;height:22px;width:100%;text-align:center;position:absolute;top:6px;cursor:pointer;' class='k_panel_tool_icon'><i class='fa fa-right-open-2'></i></a>");
                                    $B.DomUtils.append(_this.elObj, $e);
                                    $B.DomUtils.click($e, () => {
                                        $B.DomUtils.removeClass($_t, "fa-right-open-2");
                                        $B.DomUtils.addClass($_t, "fa-left-open-2");
                                        $B.animate(_this.elObj, {
                                            width: _this.expandWidth
                                        }, {
                                            duration: 100,
                                            complete: () => {
                                                $B.DomUtils.removeClass(_this.elObj, "k_panel_body_retractile");
                                                $B.DomUtils.remove($e);
                                                $B.fadeIn(allEls, 100, () => {
                                                    if (typeof _this.opts.onExpanded === 'function') {
                                                        setTimeout(() => {
                                                            _this.opts.onExpanded.call(_this.elObj, "max");
                                                        }, 1);
                                                    }
                                                });
                                            }
                                        });
                                        return false;
                                    });
                                    $B.DomUtils.click(_this.elObj, function () {
                                        $B.DomUtils.trigger($e, "click");
                                        $B.DomUtils.offEvents(_this.elObj, "click");
                                        return false;
                                    });
                                    if (typeof _this.opts.onExpanded === 'function') {
                                        setTimeout(() => {
                                            _this.opts.onExpanded.call(_this.elObj, "min");
                                        }, 1);
                                    }
                                }
                            });
                        });
                    }
                    return false;
                });
            }
            if (this.opts.maxminable) { //可变化小大                
                let $a = $B.DomUtils.createEl("<a style='line-height:" + headerHeight + "px' class='k_panel_tool_icon'><i class='fa fa-resize-full-2'></i></a>");
                $B.DomUtils.append($toolbar, $a);
                $B.DomUtils.click($a, function (e) {
                    let $i = $B.DomUtils.children(this, "i");
                    let pr;
                    $B.DomUtils.css(_this.elObj, { "position": "absolute" });
                    if ($B.DomUtils.hasClass($i, "fa-resize-full-2")) {
                        let srcWidth = $B.DomUtils.css(_this.elObj, "width");
                        let srcHeight = $B.DomUtils.css(_this.elObj, "height");
                        _this.srcSize = { width: srcWidth, height: srcHeight };
                        let $parent = _this.elObj.parentNode;
                        let zIndex = $B.DomUtils.css(_this.elObj, "zIndex");
                        _this.zIndex = zIndex;
                        $B.DomUtils.css(_this.elObj, { "z-index": _this.opts.zIndex });
                        let p_width = $B.DomUtils.width($parent);
                        let p_height = $B.DomUtils.height($parent);
                        pr = {
                            width: p_width,
                            height: p_height
                        };
                        if (_this.opts.draggable) {
                            pr["top"] = 0;
                            pr["left"] = 0;
                            let pos = $B.DomUtils.position(_this.elObj);
                            _this.srcPos = pos;
                        }
                        $B.animate(_this.elObj, pr, {
                            duration: 120, complete: () => {
                                if (typeof _this.opts.onResized === "function") {
                                    setTimeout(() => {
                                        _this.opts.onResized.call(_this.elObj, "max");
                                    }, 1);
                                }
                            }
                        });
                        $B.DomUtils.removeClass($i, "fa-resize-full-2");
                        $B.DomUtils.addClass($i, "fa-resize-normal");
                    } else {
                        pr = _this.srcSize;
                        if (_this.opts.draggable) {
                            let src_pos = _this.srcPos;
                            pr["top"] = src_pos.top;
                            pr["left"] = src_pos.left;
                        }
                        pr["z-index"] = _this.zIndex;
                        $B.animate(_this.elObj, pr, {
                            duration: 120, complete: () => {
                                if (typeof _this.opts.onResized === "function") {
                                    setTimeout(() => {
                                        _this.opts.onResized.call(_this.elObj, "min");
                                    }, 1);
                                }
                            }
                        });
                        $B.DomUtils.removeClass($i, "fa-resize-normal");
                        $B.DomUtils.addClass($i, "fa-resize-full-2");
                    }
                    return false;
                });
                //加入双击功能
                if (this.$header) {
                    $B.DomUtils.bind(this.$header, {
                        click: function () {
                            return false;
                        },
                        dblclick: function () {
                            $B.DomUtils.trigger($a, "click");
                            return false;
                        }
                    });
                }
            }
            if (this.opts.collapseable) { //上下收缩
                let $a = $B.DomUtils.createEl("<a style='line-height:" + headerHeight + "px' class='k_panel_tool_icon'><i class='fa fa-down-open-2'></i></a>");
                $B.DomUtils.append($toolbar, $a);
                $B.DomUtils.click($a, function () {
                    let $_t = $B.DomUtils.children(this, "i");
                    let allEls = $B.DomUtils.previousAll(_this.$header);
                    if ($B.DomUtils.hasClass($_t, "fa-down-open-2")) {
                        $B.fadeOut(allEls, 100, function () {
                            _this.lastHeight = $B.DomUtils.height(_this.elObj);
                            $B.animate(_this.elObj, {
                                height: headerHeight
                            }, { duration: 150 });
                            $B.DomUtils.removeClass($_t, "fa-down-open-2");
                            $B.DomUtils.addClass($_t, "fa-up-open-2");
                            if (typeof _this.opts.onCollapsed === "function") {
                                setTimeout(function () {
                                    _this.opts.onCollapsed.call(_this.elObj, "min");
                                }, 1);
                            }
                        });

                    } else {
                        $B.animate(_this.elObj, {
                            height: _this.lastHeight
                        }, {
                            duration: 150, complete: function () {
                                $B.DomUtils.removeClass($_t, "fa-up-open-2");
                                $B.DomUtils.addClass($_t, "fa-down-open-2");
                                $B.fadeIn(allEls, 100, function () {
                                    if (typeof _this.opts.onCollapsed === "function") {
                                        setTimeout(function () {
                                            _this.opts.onCollapsed.call(_this.elObj, "max");
                                        }, 1);
                                    }
                                });
                            }
                        });
                    }
                    return false;
                });
            }
        }
        if (this.opts.closeable) { //可关闭窗口
            let $a;
            if ($toolbar) {
                $a = $B.DomUtils.createEl("<a style='line-height:" + headerHeight + "px' class='k_panel_tool_icon'><i class='fa fa-cancel'></i></a>");
                $B.DomUtils.append($toolbar, $a);
            } else {
                $a = $B.DomUtils.createEl("<a style='display:block;position:absolute;top:0px;right:0px;width:14px;height:14px;z-index:2147483647;cursor:pointer;'><i style='color:#A0A2A2' class='fa fa-cancel-circled'></i></a>");
                $B.DomUtils.append(this.elObj, $a);
            }
            $B.DomUtils.click($a, function () {
                if (_this.opts.closeType === "hide") {
                    _this.close();
                } else {
                    _this.destroy();
                }
                return false;
            });
        }
        if (this.opts.draggable && $B.draggable) {
            var dragOpt = {
                isProxy: this.opts.moveProxy,
                onDragReady: this.opts.onDragReady, //鼠标按下时候准备拖动前，返回true则往下执行，返回false则停止
                onStartDrag: this.opts.onStartDrag, //开始拖动事件
                onDrag: this.opts.onDrag, //拖动中事件
                onStopDrag: this.opts.onStopDrag //拖动结束事件
            };
            dragOpt["onDragReady"] =  (state, e)=> {               
                if(this.opts.triggerHide){
                    let $body = $B.getBody();
                    $B.DomUtils.trigger($body,"mousedown");
                }
                if (e.target.nodeName === "I" && $B.DomUtils.hasClass(e.target.parentElement, "k_panel_tool_icon")) {
                    return false;
                }
            };
            if (this.opts.draggableHandler === 'header' && this.opts.header) {
                dragOpt["handler"] = this.$header;
            }
            $B.draggable(_this.elObj, dragOpt);
        }
        if (toolbar) {
            let _h = $B.DomUtils.outerHeight(toolbar);
            let borderCss = {
                "border-bottom-width": _h + "px",
                "border-bottom-style": "solid",
                "border-bottom-color": "#ffffff"
            };
            $B.DomUtils.css(this._$outerBody, borderCss);
        }
        if (typeof this.opts.onCreated === 'function') {
            this.opts.onCreated.call(this.elObj, this.$body, this.$header);
        }
        if (isUrl && this.opts.url !== null && this.opts.url !== "") {
            this.load({
                url: this.opts.url,
                dataType: this.opts.dataType
            });
        }
        if (this.opts.resizeable) {
            $B.DomUtils.css(this.elObj,{"overflow":"hidden"});
            let $el = $B.DomUtils.createEl("<div style='position:absolute;bottom:-3px;right:-3px;width:10px;height:20px;transform: rotate(45deg);z-index:2147483647;font-size:16px;cursor:pointer;'><i style='' class='fa fa-angle-double-right k_panel_resizeable_icon'></i></div>");
            $B.DomUtils.append(this.elObj, $el);
            $B.DomUtils.mousedown($el, function (e) {
                var size = $B.DomUtils.outerSize(_this.elObj);
                var h = size.height;
                var w = size.width;
                var ofs = $B.DomUtils.offset(_this.elObj);
                var $s = $B.DomUtils.createEl("<div style='position:absolute;width:" + w + "px;height:" + h + "px;top:" + ofs.top + "px;left:" + ofs.left + "px;z-index:2147483647;border:1px dashed rgb(12, 156, 229);'></div>");
                var $node = $B.DomUtils.createEl("<div style='position:absolute;bottom:0px;right:0px;width:10px;height:10px'></div>");
                $B.DomUtils.append($s, $node);
                $B.DomUtils.append($s, $node);
                $B.DomUtils.append($B.getBody(), $s);
                $B.draggable($node, {
                    onStartDrag: function (arg) {                       
                        let size = $B.DomUtils.outerSize(_this.elObj);
                        arg.state.elh = size.height;
                        arg.state.elw = size.width;
                    },
                    onDrag: function (args) {
                        var state = args.state;
                        var leftOffset = state._data.leftOffset;
                        var topOffset = state._data.topOffset;
                        $B.DomUtils.css($s, {
                            height: state.elh + topOffset,
                            width: state.elw + leftOffset
                        });
                    },
                    onStopDrag: function (args) {
                        $B.DomUtils.remove($s);
                        var state = args.state;
                        var leftOffset = state._data.leftOffset;
                        var topOffset = state._data.topOffset;
                        var newSize = {
                            height: state.elh + topOffset,
                            width: state.elw + leftOffset
                        };
                        _this.resize(newSize);
                    },
                    onMouseUp: function (args) {
                        $B.DomUtils.remove($s);
                    }
                });
                $B.DomUtils.trigger($node, "mousedown", e);
            });
        }
    }
    reload() {
        this.load();
    }
    /**用于加载数据
     args={
        url: null,//url地址
        dataType:'html/json/iframe'
    }**/
    load(args) {
        var url = this.opts.url;
        if ($B.isUrl(this.opts.content)) {
            url = this.opts.content;
        }
        var dataType = this.opts.dataType;
        var _this = this;
        if (args) {
            url = args.url;
            dataType = args.dataType;
        }
        var isFun = typeof this.opts.onLoaded === 'function';
        if (url.indexOf("?") > 0) {
            url = url + "&_t_=" + $B.generateMixed(5);
        } else {
            url = url + "?_t_=" + $B.generateMixed(5);
        }
        this.url = url;
        if(!this.loading){
            this.loading = $B.getLoadingEl();
            let loadEl = this.$body;
            if(dataType !== "iframe"){
                loadEl = this.$body.parentNode;
            }
            $B.DomUtils.append(loadEl, this.loading);
        }
        setTimeout( ()=> {
            if (dataType === "html") {
                $B.htmlLoad({
                    url: url,
                    success: function (res) {                        
                        _this.$body.innerHTML = res;           
                        if (isFun) {
                            setTimeout(()=>{
                                _this.opts.onLoaded.call(_this.$body, {});
                            },1);                           
                        }
                        $B.bindInputClear(_this.$body);                        
                    },
                    complete:function(){
                        let $l = _this.loading;
                        _this.loading = undefined;
                        try {
                            $B.removeLoading($l,()=>{                                   
                            });
                        } catch (ex) {
                        }
                    }
                },this.$body);
            } else if (dataType === "json") {
                let method = ( url.indexOf(".data") > 0 || url.indexOf(".json") > 0 ) ? "GET" : "POST";
                $B.request({
                    dataType: 'json',
                    url: url,
                    type:method,
                    onErrorEval:true,
                    ok: function (message, data) {
                        if (isFun) {
                            _this.opts.onLoaded.call(_this.$body, data);
                        }
                    },
                    final: function (res) {
                        let $l = _this.loading;
                        _this.loading = undefined;
                        try {
                            $B.removeLoading($l,()=>{                               
                            });
                        } catch (ex) {
                        }
                    },
                    fail:function(arg1,arg2,arg3){
                    }
                });
            } else {
                let goAppend = false;
                if (!_this.iframe) {
                    goAppend = true;
                    _this.iframe = $B.getIframeEl("k_panel_content_ifr");
                    var ifrId = $B.getUUID();
                    $B.DomUtils.attribute(_this.iframe, { "name": ifrId, "id": ifrId });
                    $B.DomUtils.onload(_this.iframe, () => {
                        var url = $B.DomUtils.attribute(_this.iframe, "src");
                        if (url !== "") {                           
                            let $l = _this.loading;
                            _this.loading = undefined;
                            try {
                                $B.removeLoading($l,()=>{                                   
                                });
                            } catch (ex) {
                            }
                            if (isFun) {
                                _this.opts.onLoaded.call(_this.$body, {});
                            }
                            var ua = window.navigator.userAgent;
                            var isFirefox = ua.indexOf("Firefox") !== -1;
                            try {
                                var ifrBody = this.contentDocument.body;
                                $B.DomUtils.append(ifrBody, "<span id='_window_ifr_id_' style='display:none'>" + ifrId + "</span>");
                                if (isFirefox) {
                                    // ifrBody.find("a").each(function () {
                                    //     var $a = $(this);
                                    //     if ($a.attr("href").toLowerCase().indexOf("javascript") > -1) {
                                    //         $a.attr("href", "#");
                                    //     }
                                    // });
                                }
                            } catch (ex) { }
                        }
                    });
                }
                _this.iframe.src = url;
                if (goAppend) {
                    $B.DomUtils.append(_this.$body, _this.iframe);
                }
            }
        }, 1);
    }
    /**
     * 更新静态内容 content
     * ***/
    updateContent(content) {
        $B.DomUtils.html(this.$body, content);
    }
    /**
     * args={title:'标题',iconCls:'图标样式'}/args=title
     ***/
    setTitle(args) {
        if (!this.opts.header) {
            return;
        }
        var div = $B.DomUtils.children(this.$header, "div");
        var h1 = $B.DomUtils.children(div, "h1");
        if (typeof args === 'string') {
            $B.DomUtils.html(h1, args);
        } else {
            if (args.title) {
                $B.DomUtils.html(h1, args.title);
            }
            if (args.iconCls) {
                var icon = $B.DomUtils.children(div, ".k_panel_header_icon");
                icon = $B.DomUtils.children(icon, "i");
                $B.DomUtils.attribute(icon, { "class": "fa " + args.iconCls });
            }
        }
    }
    _onAnimate(fn){        
        if (this.opts.position) {
            let hidepos = $B.DomUtils.outerHeight( this.elObj);
            let duration = { duration: 260, complete: fn };
            if (this.opts.position === "bottom") {
                $B.animate(this.elObj, { bottom: -hidepos },duration );
            } else if(this.opts.position === "top") {
                $B.animate(this.elObj, { top: -hidepos }, duration);
            }else{
                $B.fadeOut(this.elObj, 200, fn);
            }
        } else {
            $B.fadeOut(this.elObj, 200, fn);
        }
    }
    _canClose(){
        var goClose = true;
        if (typeof this.opts.onClose === 'function') {
            let r = this.opts.onClose.call(this.elObj);
            if(r !== undefined){
                goClose = r;
            }
        }
        return goClose;
    }
    /**关闭
     * ifForce:是否强制关闭，强制则忽略onClose的返回
     * **/
    close(isForce) {
        if (!this.opts) {
            return;
        }
        if (this.opts.closeType === "destroy") {
            this.destroy();
            return;
        }
        if(!isForce){
            if( !this._canClose()){
                return;
            }
        }
        var _this = this;
        this._onAnimate(function () {
            $B.DomUtils.hide(_this.elObj);
            _this._invokeClosedFn();
        });
    }
    /**显示**/
    show(callBack) {
        if (this.elObj) {
            $B.fadeIn(this.elObj, 200, callBack);
        }
    }
    hide() {
        this.close(true);
    }
    fadeOut(timeOut, callBack) {
        var m = 300;
        if (timeOut) {
            m = timeOut;
        }
        $B.fadeOut(this.elObj, 200, () => {
            this.close(true);
            if (callBack) {
                callBack();
            }
        });
    }
    _invokeClosedFn() {
        if (this.opts.dataType !== "iframe") {

        }
        if (typeof this.opts.onClosed === 'function') {
            try {
                this.opts.onClosed.call(this.elObj);
            } catch (e) {
                console.log(e);
            }
        }
    }
    /**
     * 查询元素
     * ***/
    find(id) {
        let el = $B.DomUtils.findbyId(this.$body,id);
        return el;
    }
    findByClass(cls){
        let res = $B.Dom.findByClass(this.$body,cls);
        return res;
    }
    /**
     * 销毁
     * **/
    destroy(isForce) {
        if (!this.opts) {
            return;
        }
        if(!isForce){
            if( !this._canClose()){
                return;
            }
        }
        var _this = this;        
        this._onAnimate(function () {
            _this._invokeClosedFn();
            for (var i = 0, len = _this.toolArray.length; i < len; ++i) {
                _this.toolArray[i].destroy();
            }
            _this._destroy();
        });        
    }
    _destroy() {
        super.destroy();
    }
    /*重设大小
     * args={width:,height:}
     * **/
    resize(args, callBack) {
        var rs = {};
        var src_size = {};
        if (args.width) {
            rs["width"] = args.width;
            src_size["width"] = args.width;
        } else {
            src_size["width"] = $B.DomUtils.width(this.elObj);
        }
        if (args.height) {
            rs["height"] = args.height;
            src_size["height"] = args.height;
        } else {
            src_size["height"] = $B.DomUtils.height(this.elObj);
        }
        this.srcSize = src_size;
        $B.animate(this.elObj, src_size, {
            duration: 100, complete: () => {
                this.onResized();
            }
        });
    }    
    onResized(params) {
        
    }
    /***获取大小***/
    getSize() {
        return {
            width: $B.DomUtils.outerWidth(this.elObj),
            height: $B.DomUtils.outerHeight(this.elObj)
        };
    }
    /**获取iframe**/
    getIfr() {
        if (this.opts.dataType !== 'iframe') {
            return;
        }
        return this.iframe;
    }
    setAttr(attr) {
        $B.DomUtils.attribute(this.elObj,attr);
    }
}
$B["Panel"] = Panel;