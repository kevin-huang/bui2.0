/**
 *注册富文本工具栏响应处理事件
 *封装富文本的修饰处理API函数
 *by kevin.huang 
 ***/
Keditor.extend({
    testFn: function () {
        console.log("api testFn", this);
    },
    saveFn: function () {
        let html = this.$input.innerHTML;
        console.log(html);       
    },
    paintBrushFn: function (params, $evEl) {        
        let r = this._getDomRange();
        this.$brushBtn = $evEl;
        if (r && !r.collapsed) {
            let ret = this.getSpanAndPCss();
            let spanCss = ret.spanCss;
            $B.confirm({
                okIcon: "fa-ok-circled",
                noIcon: "fa-cancel-circled",
                title: editCfg.label.brushTitle,
                content: editCfg.label.brushConfirm,
                okText: editCfg.label.yesTitle,
                noText: editCfg.label.noTitle,
                okFn: () => {
                    this.BrushingCSS = ret;    
                    let $p = this.getFocusEle(true);
                    let listObj = this.anyliscList($p);
                    if(listObj){
                        this.BrushingCSS.listObj = listObj;
                        this.BrushingCSS.srcp = $p;
                    }
                },
                noFn: () => {
                    this.BrushingCSS = { spanCss: spanCss };  
                    let $p = this.getFocusEle(true);
                    let listObj = this.anyliscList($p);
                    if(listObj){
                        this.BrushingCSS.listObj = listObj;
                        this.BrushingCSS.srcp = $p;
                    }                  
                },
                onClosed:()=>{                    
                    if(!this.BrushingCSS){
                        $B.Dom.removeClass(this.$brushBtn.firstChild, "k_edit_brush_actived");
                    }
                }
            });
            $B.Dom.addClass(this.$brushBtn.firstChild, "k_edit_brush_actived");
        } else {
            $B.alert(editCfg.label.availableBrush, 1.5);
        }
    },
    exeBrushing2el: function (params) {
        setTimeout(()=>{     
            this.makeUndoData();
            this.putUndoData();      
            let spanCss = params.spanCss;
            let pCss = params.pCss;
            this.getCssEls();           
            UTILS.fontWeightConver(spanCss);
            for(let i =0 ; i < this.cssEls.length ;i++){
                $B.Dom.css(this.cssEls[i],spanCss);
            }
            if(pCss){               
                let css = {
                    "border-bottom": pCss["border-bottom"],
                    "border-left":  pCss["border-left"],
                    "border-right":  pCss["border-right"],
                    "border-top":  pCss["border-top"],
                    "padding-bottom":  pCss["padding-bottom"],
                    "padding-left":  pCss["padding-left"],
                    "padding-right":  pCss["padding-right"],
                    "padding-top":  pCss["padding-top"],
                    "backgorund-color":  pCss["backgorund-color"],
                    "text-align":  pCss["text-align"]
                };
                for(let i =0 ; i < this.cssPEls.length ;i++){
                    $B.Dom.css(this.cssPEls[i],css);
                }
            }
            //是否是序列格式
            if(params.listObj){
                let $p = this.cssPEls[0];
                if($p !== params.srcp){
                    params.listObj.usePrs = true;
                    this.updateListSeq($p,params.listObj);
                }
                params.srcp = undefined;
                params.listObj = undefined;
            }
            setTimeout(()=>{
                this.reactiveTools();
            },1);
        },100);
    },
    clearFn: function () {
        $B.confirm(editCfg.label.clearConfirm, () => {
            this._clearFn();
        }, () => {
            this.rebuildRegion();
        }, () => {
            this.rebuildRegion();
        });
    },
    _clearFn:function(){
        this.makeUndoData("clearFn");//插入撤销重做
        this.putUndoData(1);
        let $p = this.$input.firstChild;
        let $n = $p.nextSibling;
        while ($n) {
            let $0 = $n.nextSibling;
            this.$input.removeChild($n);
            $n = $0;
        }
        $B.Dom.removeChilds($p);
        let $1 = UTILS.createSpanEl();
        $B.Dom.append($p,$1);         
        setTimeout(() => {
            this.move2end($1);
            setTimeout(() => {
                this.reactiveTools();
            }, 1);
        }, 1);
    },
    eraserFn: function () {
        this.activedLinePELS();
        let $w = $B.window({
            iconCls: "fa-help-1",
            expandable: false, //可左右收缩
            maxminable: false, //可变化小大
            collapseable: false, //上下收缩
            resizeable: false,
            width: 400,
            title: editCfg.label.clearTitle,
            onClosed: () => {
                this.unactivedLinePELS();
            },
            content: '<div><p style="line-height:28px;text-align:center;font-size:16px;">' + editCfg.label.clearTitle2 + '</p></div>',
            toolbar: {
                align: '100%',
                buttons: [
                    {
                        color: '#E8E8F5',
                        text: editCfg.label.clearSelect,
                        iconCls: 'fa-brush',
                        click: () => {
                            $w.close();
                            if (this.cssEls.length === 0) {
                                this.getCssEls();
                            }
                            if (this.cssEls.length > 0) {
                                for (let i = 0; i < this.cssEls.length; i++) {
                                    let $s = this.cssEls[i];
                                    $B.Dom.attr($s, { style: editCfg.spanCss });
                                }
                                setTimeout(() => {
                                    this.reactiveTools();
                                }, 1);
                            }
                        }
                    },
                    {
                        color: '#F2F2FC',
                        text: editCfg.label.clearPlist,
                        iconCls: 'fa-list-bullet',
                        click: () => {
                            $w.close();
                            if (this.cssEls.length === 0) {
                                this.getCssEls();
                            }
                            if (this.cssPEls.length > 0) {
                                for (let i = 0; i < this.cssPEls.length; i++) {
                                    let $p = this.cssPEls[i];
                                    $B.Dom.attr($p, { style: editCfg.pElementCss });
                                    if ($p.firstChild.nodeName === "TABLE") {
                                        this.tableIns.resetCss($p.firstChild);
                                    } else {
                                        let childs = $p.children;
                                        for (let j = 0; j < childs.length; j++) {
                                            let $s = childs[j];
                                            $B.Dom.attr($s, { style: editCfg.spanCss });
                                        }
                                    }
                                }
                                setTimeout(() => {
                                    this.reactiveTools();
                                }, 1);
                            }
                        }
                    }, {
                        color: '#E8E8F5',
                        text: editCfg.label.clearAll,
                        iconCls: 'fa-cancel',
                        click: () => {
                            $w.close();
                            let children = this.$input.children;
                            for (let i = 0; i < children.length; i++) {
                                let $p = children[i];
                                if ($B.Dom.hasClass($p, "p_element")) {
                                    $B.Dom.attr($p, { style: editCfg.pElementCss });
                                    if ($p.firstChild.nodeName === "TABLE") {
                                        this.tableIns.resetCss($p.firstChild);
                                    } else {
                                        let childs = $p.children;
                                        for (let j = 0; j < childs.length; j++) {
                                            let $s = childs[j];
                                            $B.Dom.attr($s, { style: editCfg.spanCss });
                                        }
                                    }
                                }
                            }
                            setTimeout(() => {
                                this.reactiveTools();
                            }, 1);
                        }
                    }
                ]
            }
        });
    },
    boldFn: function (params, $evEl) {
        let css = UTILS.getBoldCss($evEl);
        this.loopSpanCssEls((el) => {
            el.style.fontWeight = css;
        });
    },
    italicFn: function (params, $evEl) {
        let css = UTILS.getitalicCss($evEl);
        this.loopSpanCssEls((el) => {
            el.style.fontStyle = css;
        });
    },
    fontColorFn: function (params, $evEl) {
        this.loopSpanCssEls((el) => {
            el.style.color = params.color;
        });
        this.changeSelectionColor("color", params.color);
    },
    backgroundColorFn: function (params, $evEl) {
        this.loopSpanCssEls((el) => {
            el.style.backgroundColor = params.color;
            el.style.opacity = params.opacity;
        });
        this.changeSelectionColor("background-color", params.color);
    },
    fontSizeFn: function (params, $evEl) {
        let map = {};
        this.loopPelCssEls(($p, i) => {
            map[i] = UTILS.getLineHightData($p);
        });
        let v = parseFloat(params.replace("px", ""));
        this.loopSpanCssEls((el) => {
            el.style.fontSize = params;
        });
        //需要更新行高
        this.loopPelCssEls(($p, i) => {
            let maxFont = UTILS.getMaxFontSize($p);
            let newHeight = maxFont * map[i].r + "px";
            $B.DomUtils.css($p, { "line-height": newHeight });
        });
    },
    fontFamilyFn: function (params, $evEl) {
        //console.log("family",params);
        this.loopSpanCssEls((el) => {
            el.style.fontFamily = params;
        });
    },
    underlineFn: function (params, $evEl) {
        let css = "underline";
        this.textDecoration(css, $evEl);
    },
    strikethroughFn: function (params, $evEl) {
        let css = "line-through";
        this.textDecoration(css, $evEl);
    },
    textDecoration: function (css, $evEl) {
        if (UTILS.isHightLight($evEl)) {
            css = "none";
            UTILS.removeHightLight($evEl);
        } else {
            UTILS.addHightLight($evEl);
            if (css === "underline") {
                UTILS.removeHightLight($evEl.nextSibling);
            } else {
                UTILS.removeHightLight($evEl.previousSibling);
            }
        }
        this.loopSpanCssEls((el) => {
            el.style.textDecoration = css;
        });
    },
    superscriptFn: function (css, $evEl) {
        this.textVertical("super", $evEl);
    },
    subscriptFn: function (css, $evEl) {
        this.textVertical("sub", $evEl);
    },
    textVertical: function (css, $evEl) {
        let set = true;
        if (UTILS.isHightLight($evEl)) {
            UTILS.removeHightLight($evEl);
            set = false;
        } else {
            UTILS.addHightLight($evEl);
            if (css === "super") {
                UTILS.removeHightLight($evEl.nextSibling);
            } else {
                UTILS.removeHightLight($evEl.previousSibling);
            }
        }
        this.loopSpanCssEls((el) => {
            if (set) {
                el.style.verticalAlign = css;
            } else {
                let obj = $B.style2cssObj(el);
                delete obj["vertical-align"];
                let attr = $B.cssObj2string(obj);
                $B.DomUtils.attribute(el, { "style": attr });
            }
        });
    },
    fontTitleFn: function (params, $evEl) {
        this.loopPelCssEls(($p, i) => {
            this.loopPelSapn($p, ($s) => {
                $s.style.fontSize = params;
                if (params === editCfg.textFontSize) {
                    $s.style.fontWeight = "normal";
                } else {
                    $s.style.fontWeight = "bold";
                }
            });
        });
    },
    fontSpaceFn: function (params, $evEl) {
        //console.log(params,$evEl);
        if (params.forp) {
            this.loopPelCssEls(($p) => {
                this.loopPelSapn($p, ($s) => {
                    $s.style.letterSpacing = params["letter-spacing"];
                });
            });
        } else {
            this.loopSpanCssEls((el) => {
                el.style.letterSpacing = params["letter-spacing"];
            });
        }
    },
    pindentFn: function (params, $evEl) {
        this.loopPelCssEls(($p) => {
            let indent = params.indent;
            UTILS.indentPFn(indent, $p);
            // if(indent !== 0){
            //     let $f = UTILS.getFirstTextEL($p);
            //     let size = parseFloat($B.DomUtils.css($f,"font-size").replace("px",""));
            //     indent = params.indent * size;              
            // }
            // $p.style.textIndent = indent + "px";
        });
    },
    plineHeight: function (params, $text) {
        let v;
        if (params["line-height"]) {
            v = params["line-height"] + "px";
        }
        this.loopPelCssEls(($p) => {
            if (typeof v !== "undefined") {
                $p.style.lineHeight = v;
            } else if (params.lineR) {
                let s = UTILS.getMaxFontSize($p) * params.lineR;
                $p.style.lineHeight = s + "px";
                $text.value = s;
            }
        });
    },
    pborderFn: function (params, $evEl) {
        this.loopPelCssEls(($p) => {
            UTILS.setBorderCss($p, params);
        });
    },
    ppadding: function (params, $evEl) {
        this.loopPelCssEls(($p) => {
            $B.DomUtils.css($p, params);
        });
    },
    /**
     * 段落字体颜色
     * **/
    pcolorFn: function (params, $evEl) {
        this.loopPelCssEls(($p) => {
            this.loopPelSapn($p, ($s) => {
                $s.style.color = params.color;
            });
        });
        this.changeSelectionColor("color", params.color);
    },
    /**
     * 段落背景色
     * ***/
    pbackground: function (params, $evEl) {
        this.loopPelCssEls(($p) => {
            this.loopPelSapn($p, ($s) => {
                $s.style.backgroundColor = params.color;
            });
            $p.style.backgroundColor = params.color;
        });
        this.changeSelectionColor("background-color", params.color);
    },
    insertTableFn: function (params, $evEl) {
        console.log("insertTableFn", params);
        let row = params.row;
        let col = params.col;
        let $tab = Table.getTable(row, col);
        let $body = $tab.firstChild;
        for (let i = 0; i < row; ++i) {
            let tr = Table.createTR({
                "id": "row_" + i
            });
            for (let j = 0; j < col; ++j) {
                let td = Table.createTD({
                    "tabindex": "0",
                    "row": i,
                    "col": j
                });
                $B.DomUtils.append(tr, td);
            }
            $B.DomUtils.append($body, tr);
        }
        let $p = this.getFocusEle(true);
        if ($p) {
            let txt = $p.innerText.replaceAll("\u200b", "");
            if (txt !== "") {
                $p = this.insertNewPAfter($p);                
            }      
            this.move2end($p);
            //插入表格的撤销重做
            this.makePutUndoImd(true);
            let isInTd = false;
            let $prtd = $p.parentNode;//父单元格
            let helper = 0;
            while ($prtd && helper <= 6) {
                if ($prtd.nodeName === "TD") {
                    isInTd = true;
                    break;
                }
                $prtd = $prtd.parentNode;
                helper++;
            }
            $p.innerHTML = "";
            if (isInTd) {
                if (!this.tableIns.isEmptyTd($prtd)) {
                    $B.message({
                        mask: false,
                        content: '<span style="color:#9D1CF2">' + editCfg.label.insertTableMsg + '</span>',
                        timeout: 2,
                        position: 'top'
                    });
                    return;
                }
                let $pre = $p.previousSibling;
                while ($pre) {
                    $B.Dom.remove($pre);
                    $pre = $pre.nextSibling;
                }
                $pre = $p.nextSibling;
                while ($pre) {
                    $B.Dom.remove($pre);
                    $pre = $pre.nextSibling;
                }
                this.tableIns.clear4border($tab);
                let $tdWap = $prtd.firstChild;
                $B.DomUtils.css($tdWap, { "padding": "0px 0px" });
                $B.DomUtils.css($tdWap.firstChild, { width: '100%', height: '100%' });
                $B.DomUtils.css($p, { width: '100%', height: '100%', overflow: 'hidden', "padding": "0px 0px", "margin": "0px 0px" });
                //期待的高度,与当前嵌套单元格的高度比较
                let expHeight = row * 26 + 2;
                let tdHeight = $B.DomUtils.height($prtd);
                if (expHeight > tdHeight) { //高度不足，先撑开行高度
                    let diff = expHeight - tdHeight;
                    this.tableIns.updateRowHeightByDiff($prtd.parentNode, diff)
                }
            }
            $B.DomUtils.append($p, $tab);
            $p.contenteditable = false;
            $B.DomUtils.addClass($p, 'k_edit_p_relative');
            this.tableIns.bindEvents($tab);
            if (!isInTd && !$p.nextSibling) {
                this.insertNewPAfter($p);
            }
            if (isInTd) {
                //更新一次被插入行的高度
                this.tableIns.updateRowWnHFromDom($prtd.parentNode);
                this.tableIns.fit2parent($tab);
            } else {
                this.tableIns.setWnHFromDom($tab);
            }
            let td = $tab.firstChild.firstChild.firstChild;
            this.move2start(td.firstChild.firstChild);
            this.tableIns.setTarget($tab);
            this.tableIns.setClickTd(td);
            this.tableIns.hidePanel();
        }
    },
    alignLeftFn: function (params, $evEl) {
        this.alignFn("left");
        $B.Dom.addClass($evEl, "k_edit_tools_hlight");
        $B.Dom.removeClass($evEl.nextSibling, "k_edit_tools_hlight");
        $B.Dom.removeClass($evEl.nextSibling.nextSibling, "k_edit_tools_hlight");
    },
    alignCenterFn: function (params, $evEl) {
        this.alignFn("center");
        $B.Dom.addClass($evEl, "k_edit_tools_hlight");
        $B.Dom.removeClass($evEl.previousSibling, "k_edit_tools_hlight");
        $B.Dom.removeClass($evEl.nextSibling, "k_edit_tools_hlight");
    },
    alignRightFn: function (params, $evEl) {
        this.alignFn("right");
        $B.Dom.addClass($evEl, "k_edit_tools_hlight");
        $B.Dom.removeClass($evEl.previousSibling, "k_edit_tools_hlight");
        $B.Dom.removeClass($evEl.previousSibling.previousSibling, "k_edit_tools_hlight");
    },
    alignFn: function (css) {
        this.loopPelCssEls(($p) => {
            $p.style["textAlign"] = css;
        });
    },
    linkFn: function (params) {
        console.log("linkFN", params);
        let r = this._getDomRange();
        let href = params.addr;
        if (href.indexOf("http") !== 0) {
            href = "http://" + href;
        }
        let attr = "";
        if (params.isBlank) {
            attr = "target='_blank'";
        }
        let id = $B.generateDateUUID();
        if (r.collapsed) {
            if (r.startContainer.nodeType === 3) {
                let $s1 = r.startContainer.parentNode;
                let ret = this.splitWrapText($s1, r.startOffset, r.endOffset);
                $s1.style.display = "none";
                let targetEL = $s1;
                let linkEL;
                for (let i = 0; i < ret.length; i++) {
                    let el = ret[i];
                    $B.Dom.after(targetEL, el);
                    targetEL = el;
                    if ($B.Dom.hasClass(el, "_region_span")) {
                        $B.Dom.removeClass(el, "_region_span");
                        $B.Dom.addClass(el, "_hyper_link");
                        let style = $B.Dom.attr(el, "style");
                        el.innerHTML = "<a style='" + style + "' id='" + id + "' " + attr + " href='" + href + "'>" + params.name + "</a>";
                        linkEL = el;
                    }
                }
                $B.Dom.remove($s1);
                this.setRegionByEl(linkEL.firstChild, linkEL.firstChild);
                $B.Dom.attr(linkEL, { contenteditable: false });
                if (!linkEL.nextSibling) {
                    $B.Dom.after(linkEL, UTILS.createSpanEl());
                }
            } else {
                $B.alert(editCfg.label.availableInsert, 1.6);
            }
        } else {
            let el = this.cssEls[0];
            if (el.nodeName === "A") {
                el = el.parentNode;
            }
            let style = $B.Dom.attr(el, "style");
            let i = 1;
            let prter = el.parentNode;
            while (i < this.cssEls.length) {
                prter.removeChild(this.cssEls[i]);
                i++;
            }
            $B.Dom.attr(linkEL, { contenteditable: false });
            el.innerHTML = "<a style='" + style + "' id='" + id + "' " + attr + " href='" + href + "'>" + params.name + "</a>";
            this.setRegionByEl(el.firstChild, el.firstChild);
            if (!el.nextSibling) {
                $B.Dom.after(el, UTILS.createSpanEl());
            }
            if (!el.previousSibling) {
                $B.Dom.before(el, UTILS.createSpanEl());
            }
        }
    },
    pictureFn: function (params) {
        if (params.url !== "") {
            let r = this._getDomRange();
            if (r.collapsed && r.startContainer.nodeType === 3) {
                let position = params.pos;
                let padding = {
                    "padding-top": params.top + "px",
                    "padding-right": params.right + "px",
                    "padding-bottom": params.bottom + "px",
                    "padding-left": params.left + "px"
                };
                let $s1 = r.startContainer.parentNode;
                let $img = this.createImage(params);
                $B.Dom.css($img, padding);
                if (position === "center") {//无环绕独占一行
                    let $p = this._getPelByChild($s1);
                    if (!UTILS.isEmptyEl($p)) {
                        let $p1 = UTILS.createPEl();
                        $B.Dom.after($p, $p1);
                        $p = $p1;
                        if (!$p.nextSibling) {
                            $B.Dom.after($p, UTILS.createPEl());
                        }
                    }
                    $B.Dom.append($p.firstChild, $img);
                    $B.Dom.addClass($p.firstChild, "_img_link");
                    if (!$p.firstChild.nextSibling) {
                        let $nextSp = UTILS.createSpanEl();
                        $B.Dom.after($p.firstChild, $nextSp);
                    }
                    $B.Dom.attr($p.firstChild, { "contenteditable": false });
                    setTimeout(() => {
                        this.move2start($p.firstChild.nextSibling);
                    }, 1);
                } else {
                    let ret = this.splitWrapText($s1, r.startOffset, r.endOffset);
                    let imgEL;
                    if(Array.isArray(ret)){
                        $s1.style.display = "none";
                        let targetEL = $s1;                        
                        for (let i = 0; i < ret.length; i++) {
                            let el = ret[i];
                            $B.Dom.after(targetEL, el);
                            targetEL = el;
                            if ($B.Dom.hasClass(el, "_region_span")) {
                                $B.Dom.removeClass(el, "_region_span");
                                $B.Dom.addClass(el, "_img_link");
                                el.innerHTML = "";
                                imgEL = el;
                            }
                        }
                        $B.Dom.remove($s1);
                        $B.Dom.append(imgEL, $img);
                    }else{
                        imgEL = ret;
                        imgEL.innerHTML="";
                        $B.Dom.append(imgEL, $img);
                        $B.Dom.removeClass(imgEL, "_region_span");
                    }                   
                    if (!imgEL.nextSibling) {
                        let $nextSp = UTILS.createSpanEl();
                        $B.Dom.after(imgEL, $nextSp);
                    }
                    if (!imgEL.previousSibling) {
                        let $nextSp = UTILS.createSpanEl();
                        $B.Dom.before(imgEL, $nextSp);
                    }
                    $B.Dom.attr(imgEL, { "contenteditable": false });
                    setTimeout(() => {
                        this.move2start(imgEL.nextSibling);
                    }, 1);
                    if (position === "right") {
                        imgEL.style.float = "right";
                        let prt = imgEL.parentNode;
                        $B.Dom.remove(imgEL);
                        $B.Dom.append(prt, imgEL);
                    } else if (position === "left") {
                        imgEL.style.float = "left";
                        let prt = imgEL.parentNode;
                        $B.Dom.remove(imgEL);
                        $B.Dom.prepend(prt, imgEL);
                    }
                }
            } else {
                $B.alert(editCfg.label.availableInsert, 1.6);
            }
        }
    },
    pictureEditFn: function (params) {
        let css = {
            width: params.width + "px",
            height: params.height + "px",
            "padding-top": params.top + "px",
            "padding-right": params.right + "px",
            "padding-bottom": params.bottom + "px",
            "padding-left": params.left + "px"
        };
        $B.Dom.css(this.curImgTag, css);
        this.curImgTag = undefined;
    },
    imgBorderFn: function (css) {
        $B.Dom.css(this.curImgTag, css);
    },
    uploadFn: function (params) {
        if (params.url !== "") {
            let r = this._getDomRange();
            if (r.collapsed && r.startContainer.nodeType === 3) {
                let $s1 = r.startContainer.parentNode;
                let $img = this.createLinkA(params);
                let ret = this.splitWrapText($s1, r.startOffset, r.endOffset);
                let imgEL;
                if(Array.isArray(ret)){
                    $s1.style.display = "none";
                    let targetEL = $s1;                   
                    for (let i = 0; i < ret.length; i++) {
                        let el = ret[i];
                        $B.Dom.after(targetEL, el);
                        targetEL = el;
                        if ($B.Dom.hasClass(el, "_region_span")) {
                            $B.Dom.removeClass(el, "_region_span");
                            $B.Dom.addClass(el, "_hyper_link");
                            el.innerHTML = "";
                            imgEL = el;
                        }
                    }
                    $B.Dom.remove($s1);
                    $B.Dom.append(imgEL, $img);
                }else{
                    imgEL = ret;
                    imgEL.innerHTML="";
                    $B.Dom.append(imgEL, $img);
                    $B.Dom.removeClass(imgEL, "_region_span");
                }
                if (!imgEL.nextSibling) {
                    let $nextSp = UTILS.createSpanEl();
                    $B.Dom.after(imgEL, $nextSp);
                }
                if (!imgEL.previousSibling) {
                    let $nextSp = UTILS.createSpanEl();
                    $B.Dom.before(imgEL, $nextSp);
                }
                $B.Dom.attr(imgEL, { "contenteditable": false });
                setTimeout(() => {
                    this.move2start(imgEL.nextSibling);
                }, 1);
            } else {
                $B.alert(editCfg.label.availableInsert, 1.6);
            }
        }
    },
    focus:function(focusEnd){       
        if(focusEnd){
           this.move2end( this.$input.lastChild);
        }else{
            this.move2start( this.$input.firstChild);
        }
    },
    orderListFn:function(params){
        console.log(params);
        this.getFontIconBase64("#F7003E",30,"&#x26ab;");
    }, 
    unorderListFn:function(params){
        console.log(params);
    }
}); 