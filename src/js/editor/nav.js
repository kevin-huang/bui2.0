/**
 *注册封装 左侧导航面板，收缩功能，各个NavTab的创建
 *by kevin.huang
 * ***/
Keditor.extend({
    initNavs: function ($wap) {
        UTILS.appendIfrPanel($wap, ($body,$doc,$head) => {           
            let docName = "2022年中国农业银行合同管理平台项目研发计划.docx";
            $body.style.position = "relative";
            let $bwap = $B.Dom.createEl("<div id='k_edit_nav_body' class='k_box_size'></div>");
            let $bhad = $B.Dom.createEl("<div id='k_edit_nav_head' class='k_box_size'></div>");
            $B.Dom.append($bhad, "<div id='k_edit_nav_title'  class='k_box_size' style='overflow:hidden;'><h6>" + docName + "</h6></div>");
            $B.Dom.append($bhad, "<div id='k_edit_nav_logo'  class='k_box_size'></div>");
            this.createNavTabs($bwap,$doc,$head);
            $B.Dom.append($body, $bwap);
            $B.Dom.append($body, $bhad);
            this.bindNavHeadEvs($bhad);
            this.makeNavMMBtn();
        });
    },
    /**
     * 收缩功能
     * ***/
    makeNavMMBtn:function(){
       var cfg = editCfg.pagesCfg;
       let btn = $B.Dom.append(this.$mainWrap,"<div class='k_edtit_nav_mmbtn'><i style='position: absolute;top:12px;left:10px;color: #000;cursor:pointer;' class='fa fa-left-open-1'></i></div>");
       $B.mouseTip(btn, ()=>{
            if($B.Dom.hasClass(btn,"_opened")){
                return cfg.openSibar;
            }else{
                return cfg.closeSibar;
            }           
       }, undefined, "#9090C9");
       $B.Dom.click(btn,(e)=>{           
           let deg = "0";
           if($B.Dom.hasClass(e.catpurer,"_opened")){
                $B.Dom.removeClass(e.catpurer,"_opened");    
                let s = this.getFitSize();   
                $B.animate(this.$lbar, { "width": s.l + "px" }, { duration: 200 });
                $B.animate(this.$pageWap, { "border-left-width": s.l +"px" }, { duration: 200 });  
                $B.animate(this.toolIns.$wrap, { "border-left-width": s.l +"px" }, { duration: 200 }); 
                $B.animate(this.$bottomTool, { "border-left-width": s.l +"px" }, { duration: 200 }); 
                if(this.$sibarMasker){
                    this.$sibarMasker.parentNode.removeChild(this.$sibarMasker);
                }
                this.rightSibarClosed = false;
           }else{
                deg = "180"
                $B.Dom.addClass(e.catpurer,"_opened");
                $B.animate(this.$lbar, { "width": "31px" }, { duration: 200 });
                $B.animate(this.$pageWap, { "border-left-width": "31px" }, { duration: 200 });
                $B.animate(this.toolIns.$wrap, { "border-left-width":"31px" }, { duration: 200 }); 
                $B.animate(this.$bottomTool, { "border-left-width": "31px" }, { duration: 200 }); 
                if(!this.$sibarMasker){
                    this.$sibarMasker = $B.Dom.createEl("<div style='position:absolute;left:0;top:0;width:100%;height:100%;background:#DBD6D6;opacity:0.7;z-index:2;'></div>");
                }
                $B.Dom.append(this.$leftTabWap,this.$sibarMasker);
                this.rightSibarClosed = true;
           }
           $B.animate(e.catpurer.firstChild, { "rotateZ": deg+"deg" }, { duration: 200 ,complete:()=>{
               $B.Dom.trigger(btn,"mouseleave");
           }});           
       });
    },
    bindNavHeadEvs: function ($bhad) {
        this.bindTitleMouseTip($bhad.firstChild.firstChild);
    },
    bindTitleMouseTip: function ($bhad) {
        if (!this.titleMouseTipEvs) {
            var tipMonseEvents = {
                mouseenter: (e) => {
                    if (this.hideToolTiptimter) {
                        clearTimeout(this.hideToolTiptimter);
                        this.hideToolTiptimter = undefined;
                    }
                },
                mouseleave: (e) => {
                    if (this.toolTipIns) {
                        this.toolTipIns.close();
                        this.toolTipIns = undefined;
                    }
                }
            };
            this.titleMouseTipEvs = {
                mouseenter: (e) => {
                    if (this.toolTipIns) {
                        clearTimeout(this.hideToolTiptimter);
                        this.toolTipIns.close(true);
                        this.toolTipIns = undefined;
                    }
                    let catpurer = e.catpurer;
                    let txt = catpurer.innerText;
                    if (!catpurer.titleWidth) {
                        catpurer.titleWidth = $B.getCharWidth(txt);
                    }
                    let cw = catpurer.titleWidth;
                    let w = $B.Dom.width(catpurer);
                    if (cw > w) {
                        let ofs = this.getElObjOfs();
                        catpurer.fixpos = { top: ofs.top };
                        this.toolTipIns = $B.toolTip(catpurer, txt, "bottom", "#3763AD");
                        $B.DomUtils.bind(this.toolTipIns.elObj, tipMonseEvents);
                    }
                },
                mouseleave: (e) => {
                    if (this.toolTipIns) {
                        this.hideToolTiptimter = setTimeout(() => {
                            this.toolTipIns.close();
                            this.toolTipIns = undefined;
                        }, 500);
                    }
                }
            }
        }
        $B.Dom.bind($bhad, this.titleMouseTipEvs);
    },
    createNavTabs: function ($bwap,$doc,$head) {
        var $mb = $B.Dom.append($bwap, "<div style='position:absolute;top:0;left:0;width:100%;height:100%;border-left:31px solid #fff;' class='k_box_size'></div>");
        var $tab = $B.Dom.append($bwap, "<div style='position:absolute;top:0;left:0;width:31px;height:100%;border-right:1px solid #D4D4D4;' class='k_box_size'></div>");
        this.$leftTabWap = $tab;
        var cfg = editCfg.pagesCfg;
        var tabs = cfg.navTabs;
        var events = {
            click: (e) => {                
                let catpurer = e.catpurer;
                let el = catpurer.firstChild;
                if($B.Dom.hasClass(el,"actived")){
                    return;
                }
                let hideIdx = 0;
                $B.Dom.forSiblings(catpurer,($s)=>{
                    if($B.Dom.hasClass($s.firstChild,"actived")){
                        $B.Dom.removeClass($s.firstChild,"actived");
                        hideIdx = parseInt($B.Dom.attr($s,"i"));
                    }                    
                });
                $B.Dom.addClass(el,"actived");
                let i = parseInt($B.Dom.attr(catpurer,"i"));
                let childs = $mb.children;
                let hideEl = childs[hideIdx];
                let showEl = childs[i];
                $B.hide(hideEl,100);
                $B.show(showEl,100);
            }
        };
        let ofs = this.getElObjOfs();
        let fixpos = { top: ofs.top };
        for (let i = 0; i < tabs.length; i++) {
            let cls = "";
            let display = "display:none";
            if (i === 0) {
                display = "";
                cls = "actived";
            }
            let $t = $B.Dom.append($tab, "<div i='" + i + "' label='" + tabs[i].label + "' class='k_edit_nav_tab'><span class='" + cls + "'><i class='fa " + tabs[i].iconCls + "'></i></span></div>");
            $B.mouseTip($t, tabs[i].label, fixpos, "#9090C9");
            $B.Dom.click($t, events.click);
            let $C = $B.Dom.append($mb, "<div style='position:relative;width:100%;height:100%;" + display + ";'></div>");
            if (this[tabs[i].makerFn]) {
                this[tabs[i].makerFn]($C,$doc,$head);
            }
        }
    },
    tabDataMaker: function ($c,$doc,$head) {
        var cfg = editCfg.pagesCfg;
        let $title = $B.Dom.createEl("<div style='position:absolute;top:0;left:0;width:100%;height:28px;border-bottom:1px solid #E0E0E0;'></div>");
        let $by = $B.Dom.createEl("<div style='position:absolute;top:0;left:0;width:100%;height:100%;border-top:30px solid #fff;' class='k_box_size'><div style='width:100%;height:100%;'></div></div>");
        $B.Dom.append($c, $by);
        $B.Dom.append($c, $title);
        let $file = $B.Dom.append($title, "<div  style='width:100%;height:100%;position:absolute;top:0;left:0;border-right:40px solid #fff;overflow:hidden;' class='k_box_size k_edit_h_clamp'><h6 style='font-weight:normal;color:#5A5858;padding-left:2px;'></h6></div>");
        let $upload = $B.Dom.append($title, "<div  style='width:19px;height:100%;position:absolute;top:0;right:20px;padding-top:5px;' class='k_box_size'></div>");
        let $set = $B.Dom.append($title, "<div style='width:19px;height:100%;position:absolute;top:0;right:0;line-height:28px;padding-top:8px;cursor:pointer;' class='k_box_size'><i style='font-size:16px;' class='fa fa-list-bullet'></i></div>");
        let ofs = this.getElObjOfs();
        let fixpos = { top: ofs.top };
        $B.mouseTip($upload, cfg.uploadLabel, fixpos, "#9090C9");
        $B.mouseTip($set, cfg.selectData, fixpos, "#9090C9");
        let $h = $file.firstChild;
        this.bindTitleMouseTip($h);
        $h.innerText = "数据文档数据文档数据文档.json";
        this.bindDataUpload($upload);
        this.openDataList($set);
        var _callInner = function(){
            return $by.firstChild;
        };
        var _returnInner = function($body){
            let $ul = $B.Dom.append($body,"<ul></ul>");      
            new $B.Tree($ul, {          
                showLine: true,
                plainStyle: true,
                url: "http://127.0.0.1:8888/static/test/dataOrg.json",                
                checkbox: false
            });
        };   
        var js = "let $body = $B.myScrollbar(parent._callInner(), {isHide:true});parent._returnInner($body);";  
        UTILS.runInnerJS($head,_callInner,_returnInner,js);
    },
    bindDataUpload: function ($upload) {
        var cfg = editCfg.pagesCfg;
        let upOpt = {
            dislabel: true,
            style: { padding: "1px 1px", "height": "18px", "margin-right": "0px", "text-align": "center", "background": "#767676", "font-size": "12px" },          
            timeout: 180, //超时时间 秒
            url: 'http://127.0.0.1:8888/test/upload',
            multiple: false, //是否可批量选择
            immediate: true, //选择文件后是否立即自动上传，即不用用户点击提交按钮就上传
            accept: '.json,.xml',   // 可上传的文件类型 .xml,.xls,.xlsx,.png
            onSelected: function (file, fileName) { //选择文件后的事件请返回true 以便通过验证  
                console.log(fileName);
                return true;
            },
            success: function (res, data) { //成功时候的回调
                console.log("success ", res);
            },
            setParams: function () { //设置参数 return {}
                return {};
            },
            error: function (err) { //错误回调
                console.log("error " + res);
            }
        };
        new $B.Upload($upload, upOpt);
    },
    openDataList: function ($set) {
        var cfg = editCfg.pagesCfg;
        //弹出数据选择窗口
        $B.Dom.click($set, (e) => {            
            let $b = $B.Dom.createEl("<div class='k_box_size' style='height:480px'><div  style='height:100%;width:100%;position:absolute;top:0;left:0;border-top:30px solid #fff;' class='k_box_size'><table></table></div><div style='position:absolute;top:0;left:0;height:30px;width:100%'></div></div>");
            let $tab = $b.firstChild.firstChild;
            let $w = $B.window({
                fixed: true,
                content: $b,
                title: cfg.selectData,                
                width: 700,
                onClose:()=>{                   
                    dg.destroy();
                },
                toolbar: {
                    align: '100%',
                    buttons: [
                        {
                            color: '#EAEAEA',
                            text: editCfg.label.backOut,
                            iconCls: 'fa-ccw-1',
                            click: () => {
                                $w.close();
                            }
                        },
                        {
                            color: '#E3E8FF',
                            text: editCfg.label.ensure,
                            iconCls: 'fa-ok-circled2',
                            click: () => {
                                $w.close();
                            }
                        }
                    ]
                }
            });
            let args = {
                url: "http://127.0.0.1:8888/static/test/table.json",
                loadImd: true,
                pgBtnNum: 5,
                pageSize:10,
                fit2height: true,
                checkBox:false,
                oprCol: false,
                splitColLine: 'k_table_td_h_line', //分割线样式 k_table_td_v_line、k_table_td_h_line，k_table_td_none_line
                onClickCell: function (field, data) { //单击一个单元格时触发fn(field, value)
                    alert("onClickCell " + field + "  " + JSON.stringify(data));
                },
                rows: cfg.dataCols
            };
            dg = new $B.Table($tab, args);
            $b.lastChild.innerHTML = "<span style='padding-top:2px;display:inline-block;'><span style='padding-left:12px'><input type='text' style='width:200px;height:24px ;line-height:24px;'/></span><span style='padding-left:12px;'><button id='q0001' style='height:20px;line-height:20px;background:#C4DEF7;' class='k_toolbar_button k_box_size  k_toolbar_button_min'><i class='fa fa-search'></i><span style='padding-left:4px;'>"+cfg.queryLabel+"</span></buttom></span></span>";
            let $btn = $B.Dom.findbyId($b.lastChild,"q0001");
            $B.Dom.click($btn,()=>{
                dg.reload();
            });
        });
    },
    tabNavMaker: function ($c) {
        $c.innerHTML = "<p>标题导航！</p>";
    },
    tabTmplMaker: function ($c) {
        $c.innerHTML = "<p>模板列表</p>";
    },
    tabSearchMaker: function ($c) {
        $c.innerHTML = "<p>查找/替换</p>";
    }
});