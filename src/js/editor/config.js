
var svgIcon = {
    pen: '<svg viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg"  width="20" height="16"><path d="M871.34208 260.46976l-106.93632-106.96704c-24.41728-24.40192-64.11776-24.40192-88.53504 0l-449.95072 449.95072c-10.78784 10.80832-16.53248 24.63744-17.78688 38.79936l-62.83264 198.17472c-3.42016 11.09504-0.4608 23.15776 7.76192 31.3856 8.22272 8.20736 20.29056 11.20256 31.3856 7.76704l198.15424-62.86336c14.1568-1.23392 27.99616-6.98368 38.784-17.78688l449.95584-449.95584C895.75424 324.57216 895.75424 284.87168 871.34208 260.46976zM356.01408 760.94464l-133.39648 41.2928 41.3184-133.41696c0.42496-1.44896 0.64-2.89792 0.85504-4.36736l95.5904 95.59552C358.94784 760.27904 357.48352 760.47872 356.01408 760.94464zM388.19328 743.60832l-106.96192-106.96704 438.89152-438.8864 106.9568 106.96704L388.19328 743.60832z"  fill="#13227a"></path></svg>',
    left: '<svg style="width:16px;height:16px;position:relative;top:3px;cursor:pointer;" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="200" height="200"><defs><style type="text/css"></style></defs><path d="M783.872 542.122667l-0.042667-64.405334-477.610666-0.298666 225.28-225.322667-45.568-45.568L182.506667 509.952l303.829333 303.829333 45.525333-45.504-226.474666-226.453333 478.506666 0.298667z"></path></svg>',
    right: '<svg style="width:16px;height:16px;position:relative;top:3px;cursor:pointer;" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg"  xmlns:xlink="http://www.w3.org/1999/xlink" ><defs><style type="text/css"></style></defs><path d="M214.677333 542.122667l0.042667-64.405334 477.653333-0.298666-225.301333-225.322667 45.568-45.568 303.424 303.424L512.213333 813.781333l-45.504-45.504 226.453334-226.453333-478.485334 0.298667z"></path></svg>',
    reset: '<svg style="width:16px;height:16px;position:relative;top:3px;cursor:pointer;" viewBox="0 0 1278 1024" version="1.1" xmlns="http://www.w3.org/2000/svg"  xmlns:xlink="http://www.w3.org/1999/xlink"><defs><style type="text/css"></style></defs><path d="M1272.447024 472.907505l-150.052574 149.245613a32.021415 32.021415 0 0 1-44.681788 0L928.777033 472.906505c-17.717123-20.478986-5.895708-27.925618 6.45368-39.716035h136.525243A441.782135 441.782135 0 0 0 253.257466 304.732828L178.791151 275.257287a521.555187 521.555187 0 0 1 973.731808 157.934183h113.56338a26.653681 26.653681 0 0 1 6.360685 39.716035zM202.713967 440.946087l150.022575 149.556598a26.622682 26.622682 0 0 1-6.45468 39.717034H216.366292a442.247112 442.247112 0 0 0 789.701915 120.700026l75.213278 29.787526A521.555187 521.555187 0 0 1 134.513343 630.219719h-118.994111c-12.411386-11.791416-24.170804-19.238048-6.453681-39.717034l148.935629-149.556598a32.641385 32.641385 0 0 1 44.712787 0z"></path></svg>'
};
var editCfg = {
    hightLightClz: 'k_edit_tools_hlight',
    undoSize: 20,//undo回退步骤数量
    label: {
        "backOut":'取消',
        "ensure":"确认",
        "pleaseFocus":"请先点击聚焦文本域！",
        "brushConfirm":"是否需要复制段落样式？",   
        "noTitle":'否',
        "yesTitle":'是',
        "brushTitle":"格式刷确认？",      
        "clearConfirm":"请确认清空内容！",
        "clearTitle":"清理格式选项",
        "clearTitle2":"请选择清理格式选项！",
        "clearSelect":"清理选择",
        "clearPlist":"清理段落",
        "clearAll":"清理所有",
        "tableEdit": "表格修改",
        "unsuport":"不支持的操作！",
        "wordspace": '文本间距',
        "paragraph": "段落设置",
        "insertTable": "插入表格",
        "insertTableTip": "x行y列，按左键确认插入",
        "anchorTitle": "全选",
        "splitTd": "拆分",
        "megerTd": "合并",
        "insertCol": "插入列",
        "insertRow": "插入行",
        "insertFile": '插入文件',
        "insertImg": '插入图片',
        "editImg": "修改图片",
        "borderImg":"图片边框",
        "insertVideo": '插入视频',
        "inputColRow": '请输入行列数量',
        "insertTableMsg": '单元格内容必须为空！',
        "insertLink": "插入超链接",
        "uploadError":'上传异常!',
        "availableInsert":'无效插入！请您先点击需要插入的区域！',
        "availableBrush":"无效格式刷，请先划选样式区"
    },
    imgMenus:[
        {
            "label": "删除",
            "icon": "fa-cancel",
            "fn": 'deleteImgFn'
        },
        {
            "label": "修改",
            "icon": "fa-edit",
            "fn": 'editImgFn'
        },{
            "label": "边框",
            "icon": "fa-check-empty",
            "fn": 'borderImgFn'
        }
    ],
    ctxMenus: [
        {
            "label": "插入表格",
            "icon": "fa-table",
            "fn": 'openInsertTable'
        },
        {
            "label": "插入文件",
            "icon": "fa-upload",
            "fn": 'openInsertFile'
        },
        {
            "label": "插入图片",
            "icon": "fa-file-image",
            "fn": 'openInsertImage'
        },
        {
            "label": "插入超链接",
            "icon": "fa-link",
            "fn": 'openInsertLink'
        },
        {
            "label": "插入表情",
            "icon": "fa-smile",
            "fn": 'openInsertEmotion'
        },
        {
            "label": "段落设置",
            "icon": "fa-doc-text",
            "fn": 'openGraph'
        }
    ],
    tableNoBorder: '1px dashed rgb(175, 201, 222)',
    tableCtx: {
        '_insert_op_tr': [
            { fn: "insertRight", label: '右侧插入列' },
            { fn: "insertLeft", label: '左侧插入列' },
            { fn: "insertUp", label: '上方插入行' },
            { fn: "insertDown", label: '下方插入行' },
            { fn: "insertTable", label: '插入表格' }
        ],
        '_delete_op_tr': [
            { fn: "removeRow", label: "删除行" },
            { fn: "removeCol", label: "删除列" },
            { fn: "removeContent", label: "删除内容" },
            { fn: "removeTable", label: "删除表格" }
        ]
    },
    tools: [        
        { fn: "importFn", label: '导入文档', icon: 'fa-upload-cloud' },
        { fn: "printFn", label: '导出Word', icon: 'fa-file-word' },
        { fn: "printFn", label: '导出Pdf', icon: 'fa-file-pdf' },        
        { fn: "printFn", label: '打印', icon: 'fa-print' },
        { fn: "saveFn", label: '保存', icon: 'fa-floppy' },
        { fn: "paintBrushFn", label: '格式刷', icon: 'fa-brush', region: true ,focus:true},
        { fn: "clearFn", label: '清空', icon: 'fa-trash-empty' },
        { fn: "eraserFn", label: '清除格式', icon: 'fa-eraser' },       
        { fn: "undoFn", label: '回退', icon: 'fa-ccw-1' },
        { fn: "redoFn", label: '重做', icon: 'fa-cw-1' },
        { fn: "boldFn", label: '加粗', icon: 'fa-bold', region: true ,focus:true},
        { fn: "italicFn", label: '斜体', icon: 'fa-italic', region: true ,focus:true},
        { fn: "underlineFn", label: '下划线', icon: 'fa-underline', region: true ,focus:true},
        { fn: "strikethroughFn", label: '删除线', icon: 'fa-strike', region: true ,focus:true},
        { fn: "superscriptFn", label: '上标', icon: 'fa-superscript', region: true ,focus:true},
        { fn: "subscriptFn", label: '下标', icon: 'fa-subscript', region: true,focus:true },
        { fn: "fontFamilyFn", label: '字体', icon: 'fa-angle-down', region: true,focus:true, opts: [{ label: '微软雅黑', value: '微软雅黑,Microsoft YaHei' }, { label: '宋体', value: '宋体,SimSun' }, { label: '楷体', value: '楷体,楷体_GB2312,SimKai' }, { label: '黑体', value: '黑体,SimHei' }, { label: '隶书', value: '隶书,SimLi' }] },
        { fn: "fontSizeFn", label: '字体大小', icon: 'fa-angle-down', region: true,focus:true, opts: [{ label: '9px', value: '9px' }, { label: '10px', value: '10px' }, { label: '11px', value: '11px' }, { label: '12px', value: '12px' }, { label: '13px', value: '13px' }, { label: '14px', value: '14px' }, { label: '16px', value: '16px' }, { label: '18px', value: '18px' }, { label: '20px', value: '20px' }, { label: '22px', value: '22px' }, { label: '24px', value: '24px' }, { label: '28px', value: '28px' }, { label: '32px', value: '32px' }, { label: '36px', value: '36px' }] },
        { fn: "fontTitleFn", label: '正文', icon: 'fa-angle-down', region: true,focus:true, opts: [{ label: '正文', value: '14px' }, { label: '标题1', value: '28px' }, { label: '标题2', value: '24px' }, { label: '标题3', value: '22px' }, { label: '标题4', value: '18px' }, { label: '标题5', value: '16px' }] },
        { fn: "fontSpaceFn", label: '文字间距', icon: 'fa-text-width-1', region: true,focus:true },
        { fn: "fontColorFn", label: '文字颜色', icon: 'fa-pencil', region: true,focus:true },
        { fn: "backgroundColorFn", label: '背景色', icon: 'fa-pencil-squared', region: true,focus:true },
        { fn: "paragraphFn", label: '段落设置', icon: 'fa-doc-text',focus:true },
        { fn: "insertTableFn", label: '插入表格', icon: 'fa-table',focus:true },
        { fn: "alignLeftFn", label: '左对齐', icon: 'fa-align-left',focus:true },
        { fn: "alignCenterFn", label: '居中对齐', icon: 'fa-align-center',focus:true },
        { fn: "alignRightFn", label: '右对齐', icon: 'fa-align-right',focus:true },
        { fn: "linkFn", label: '超链接', icon: 'fa-link',focus:true },
        { fn: "pictureFn", label: '插入图片', icon: 'fa-file-image',focus:true },
        { fn: "uploadFn", label: '上传文件', icon: 'fa-upload',focus:true },
        { fn: "orderListFn", label: '有序列表',focus:true, icon: 'fa-list-numbered' ,opts: [{ label: '数字[1]', value: '1' }, { label: '中文数字[一]', value: '一' }, { label: '字母[a]', value: 'a' }, { label: '无', value: '' }]},
        { fn: "unorderListFn", label: '无序列表',focus:true, icon: 'fa-list-bullet',opts: [{ label: '空心圆', value: 'fa-circle-empty' }, { label: '实心圆', value: 'fa-record' }, { label: '方块', value: 'fa-stop' },{label:'箭头',value:'fa-angle-double-right'}, { label: '无', value: '' }] },
        { fn: "emotionFn", label: '表情', icon: 'fa-smile',focus:true },
        { fn: "insertIconFn", label: '插入图标', icon: 'fa-heart-empty',focus:true },
        { fn: "insertFloatFn", label: '插入浮动文本/图片', icon: 'fa-docs',focus:true }
        // { fn: "videoFn", label: '插入视频', icon: 'fa-video' },
        // { fn: "mapFn", label: '插入地图', icon: 'fa-location-alt' },
        // { fn: "insertCodeFn", label: '插入代码', icon: 'fa-file-code' },
        // { fn: "flowFn", label: '绘制流程', icon: 'fa-shuffle' },
        // { fn: "sourceFn", label: '源码', icon: 'fa-code' },
        // { fn: "mobileFn", label: '手机模拟', icon: 'fa-eye' },
        // { fn: "previewFn", label: '预览', icon: 'fa-eye' },
        // { fn: "fullprintFn", label: '全屏', icon: 'fa-resize-full' },
        // { fn: "helpFn", label: '使用帮助', icon: 'fa-question' }
    ],
    mobiles: [{ size: "320 * 568", title: 'IPHONE4' },
    { size: "360 * 640", title: '三星S5' },
    { size: "375 * 667", title: 'IPHONE6', default: true },
    { size: "414 * 736", title: '华为P20' },
    { size: "375 * 812", title: 'IPHONEX' },
    { size: "768 * 1024", title: 'IPAD' }],
    textFontSize: "14px",
    chlist:['一','二','三','四','五','六','七','八','九','十'],
    enlist:['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'],
    pElementCss: 'line-height:21px;font-size:14px;padding:0px;cursor:text;margin:0px;width:auto;text-align:left;background-color:none;',
    spanCss: 'font-size:14px;font-family:Microsoft Yahei;color:#1c1c29;background-color:none;font-weight: normal;',
    lineCss: 'line-height: 3px;height: 3px;border-bottom: 1px solid #B2B2B2;border-top: none;border-left: none;border-right: none;margin-top: 4px;margin-bottom: 4px;cursor: pointer',
    preview: 'preview.html',//预览页面 
    /**浮动文本启用的功能***/
    floatEidtor: {
        'save': true, 'bold': true, "paintBrush": true, "fontTitle": true,
        'italic': true, 'underline': true, 'strikethrough': true, 'superscript': true, 'subscript': true, 'fontFamily': true, 'insertIcon': true, 'eraser': true,
        'fontSize': true, 'fontSpace': true, 'fontColor': true, 'alignCenter': true, 'alignLeft': true, 'alignRight': true, 'video': true, 'split': true,
        'insertTable': true, 'flow': true, 'upload': true, 'link': true, 'emotion': true, 'insertCode': true, 'paragraph': true, 'backgroundColor': true, 'map': true
    },
    fontSpaceForm: "<div><p><span style='width:40%;padding-left:9px'>段落:<span style='padding-left:5px;'><input  style='cursor:pointer;' checked='checked' type='radio' id='for_p' name='target'/></span></span><span style='width:40%;padding-left:9px'> 选区:<span style='padding-left:5px;'><input id='for_s' style='cursor:pointer;' type='radio' name='target'/></span></span></p>"
        + "<p><span style='cursor:pointer' class='label reduce ev'>" + svgIcon.left + "减小</span><span style='cursor:pointer' class='label addplus ev'>" + svgIcon.right + "增大</span><span style='cursor:pointer' class='label reset ev'>" + svgIcon.reset + "重置</span></p>"
        + "<p><span class='label'>输入值</span><span  class='label'><input id='txtinput' placeholder='像数' type='text' value='0' style='width:50px'/></span><span  class='label'><button id='surebtn'>确认</button></span></p>"
        + "</div>",
    inputColRowForm: "<span>行：<input style='width:100px' id='row_input' value='2' type='text'/><span style='padding-left:20px;'>列：</span><input value='2' id='col_input'  style='width:100px' type='text'/></span>",
    iconForm: '<div><div class="clearfix"><div style="float:left;line-height:28px;" class="clearfix"><div style="float:left;">颜色：<i unbindcolorev="" id="icon_color" style="border:1px solid #f5f5f5;padding-left:12px;color:#FFFFFF;background-color:#7E7E7E;cursor:pointer;" class="fa fa-brush"></i></div>  <div style="float:left;padding-left:20px;">大小：</div><div style="float:left;height:21px;" id="_slider"></div></div><div id="_tip_wrap" style="float:left;width:50px;height:24px;line-height:24px;padding-left:12px;">12px</div></div>' +
        '<div class="clearfix" id="_icons" style="padding-top:20px;"><span style="float:left;width:16px;height:22px;font-family:fontello;color:#7E7E7E;font-size:16px;line-height:22px;margin:2px 12px;cursor:pointer;">&#xe801;</span><span style="float:left;width:16px;height:22px;font-family:fontello;color:#7E7E7E;font-size:16px;line-height:22px;margin:2px 12px;cursor:pointer;">&#x25a0;</span><span style="float:left;width:16px;height:22px;font-family:fontello;color:#7E7E7E;font-size:16px;line-height:22px;margin:2px 12px;cursor:pointer;">&#x266a;</span>' +
        '<span style="float:left;width:16px;height:22px;font-family:fontello;color:#7E7E7E;font-size:16px;line-height:22px;margin:2px 12px;cursor:pointer;">&#x26ab;</span><span style="float:left;width:16px;height:22px;font-family:fontello;color:#7E7E7E;font-size:16px;line-height:22px;margin:2px 12px;cursor:pointer;">&#xe055;</span><span style="float:left;width:16px;height:22px;font-family:fontello;color:#7E7E7E;font-size:16px;line-height:22px;margin:2px 12px;cursor:pointer;">&#xe0db;</span>' +
        '<span style="float:left;width:16px;height:22px;font-family:fontello;color:#7E7E7E;font-size:16px;line-height:22px;margin:2px 12px;cursor:pointer;">&#xe802;</span><span style="float:left;width:16px;height:22px;font-family:fontello;color:#7E7E7E;font-size:16px;line-height:22px;margin:2px 12px;cursor:pointer;">&#xe804;</span><span style="float:left;width:16px;height:22px;font-family:fontello;color:#7E7E7E;font-size:16px;line-height:22px;margin:2px 12px;cursor:pointer;">&#xe805;</span>' +
        '<span style="float:left;width:16px;height:22px;font-family:fontello;color:#7E7E7E;font-size:16px;line-height:22px;margin:2px 12px;cursor:pointer;">&#xe80b;</span><span style="float:left;width:16px;height:22px;font-family:fontello;color:#7E7E7E;font-size:16px;line-height:22px;margin:2px 12px;cursor:pointer;">&#xe80f;</span><span style="float:left;width:16px;height:22px;font-family:fontello;color:#7E7E7E;font-size:16px;line-height:22px;margin:2px 12px;cursor:pointer;">&#xe810;</span>' +
        '<span style="float:left;width:16px;height:22px;font-family:fontello;color:#7E7E7E;font-size:16px;line-height:22px;margin:2px 12px;cursor:pointer;">&#xe816;</span><span style="float:left;width:16px;height:22px;font-family:fontello;color:#7E7E7E;font-size:16px;line-height:22px;margin:2px 12px;cursor:pointer;">&#xe81f;</span><span style="float:left;width:16px;height:22px;font-family:fontello;color:#7E7E7E;font-size:16px;line-height:22px;margin:2px 12px;cursor:pointer;">&#xe828;</span>' +
        '<span style="float:left;width:16px;height:22px;font-family:fontello;color:#7E7E7E;font-size:16px;line-height:22px;margin:2px 12px;cursor:pointer;">&#xe82a;</span><span style="float:left;width:16px;height:22px;font-family:fontello;color:#7E7E7E;font-size:16px;line-height:22px;margin:2px 12px;cursor:pointer;">&#xe88c;</span><span style="float:left;width:16px;height:22px;font-family:fontello;color:#7E7E7E;font-size:16px;line-height:22px;margin:2px 12px;cursor:pointer;">&#xe870;</span>' +
        '<span style="float:left;width:16px;height:22px;font-family:fontello;color:#7E7E7E;font-size:16px;line-height:22px;margin:2px 12px;cursor:pointer;">&#xf006;</span><span style="float:left;width:16px;height:22px;font-family:fontello;color:#7E7E7E;font-size:16px;line-height:22px;margin:2px 12px;cursor:pointer;">&#xf0e6;</span><span style="float:left;width:16px;height:22px;font-family:fontello;color:#7E7E7E;font-size:16px;line-height:22px;margin:2px 12px;cursor:pointer;">&#xf118;</span>' +
        '<span style="float:left;width:16px;height:22px;font-family:fontello;color:#7E7E7E;font-size:16px;line-height:22px;margin:2px 12px;cursor:pointer;">&#xf119;</span><span style="float:left;width:16px;height:22px;font-family:fontello;color:#7E7E7E;font-size:16px;line-height:22px;margin:2px 12px;cursor:pointer;">&#xf11a;</span><span style="float:left;width:16px;height:22px;font-family:fontello;color:#7E7E7E;font-size:16px;line-height:22px;margin:2px 12px;cursor:pointer;">&#xe837;</span>' +
        '<span style="float:left;width:16px;height:22px;font-family:fontello;color:#7E7E7E;font-size:16px;line-height:22px;margin:2px 12px;cursor:pointer;">&#xf124;</span><span style="float:left;width:16px;height:22px;font-family:fontello;color:#7E7E7E;font-size:16px;line-height:22px;margin:2px 12px;cursor:pointer;">&#xf18a;</span><span style="float:left;width:16px;height:22px;font-family:fontello;color:#7E7E7E;font-size:16px;line-height:22px;margin:2px 12px;cursor:pointer;">&#xf29c;</span>' +
        '<span style="float:left;width:16px;height:22px;font-family:fontello;color:#7E7E7E;font-size:16px;line-height:22px;margin:2px 12px;cursor:pointer;">&#x1f44d;</span><span style="float:left;width:16px;height:22px;font-family:fontello;color:#7E7E7E;font-size:16px;line-height:22px;margin:2px 12px;cursor:pointer;">&#xf2c0;</span><span style="float:left;width:16px;height:22px;font-family:fontello;color:#7E7E7E;font-size:16px;line-height:22px;margin:2px 12px;cursor:pointer;">&#xf299;</span>' +
        '<span style="float:left;width:16px;height:22px;font-family:fontello;color:#7E7E7E;font-size:16px;line-height:22px;margin:2px 12px;cursor:pointer;">&#xe8c7;</span><span style="float:left;width:16px;height:22px;font-family:fontello;color:#7E7E7E;font-size:16px;line-height:22px;margin:2px 12px;cursor:pointer;">&#xe8ca;</span><span style="float:left;width:16px;height:22px;font-family:fontello;color:#7E7E7E;font-size:16px;line-height:22px;margin:2px 12px;cursor:pointer;">&#xe8cb;</span>' +
        '<span style="float:left;width:16px;height:22px;font-family:fontello;color:#7E7E7E;font-size:16px;line-height:22px;margin:2px 12px;cursor:pointer;">&#xf0c5;</span><span style="float:left;width:16px;height:22px;font-family:fontello;color:#7E7E7E;font-size:16px;line-height:22px;margin:2px 12px;cursor:pointer;">&#xe899;</span><span style="float:left;width:16px;height:22px;font-family:fontello;color:#7E7E7E;font-size:16px;line-height:22px;margin:2px 12px;cursor:pointer;">&#xe8bf;</span>' +
        '<span style="float:left;width:16px;height:22px;font-family:fontello;color:#7E7E7E;font-size:16px;line-height:22px;margin:2px 12px;cursor:pointer;">&#xf101;</span><span style="float:left;width:16px;height:22px;font-family:fontello;color:#7E7E7E;font-size:16px;line-height:22px;margin:2px 12px;cursor:pointer;">&#xf128;</span><span style="float:left;width:16px;height:22px;font-family:fontello;color:#7E7E7E;font-size:16px;line-height:22px;margin:2px 12px;cursor:pointer;">&#xf10c;</span>' +
        '<span style="float:left;width:16px;height:22px;font-family:fontello;color:#7E7E7E;font-size:16px;line-height:22px;margin:2px 12px;cursor:pointer;">&#xe897;</span><span style="float:left;width:16px;height:22px;font-family:fontello;color:#7E7E7E;font-size:16px;line-height:22px;margin:2px 12px;cursor:pointer;">&#xe874;</span><span style="float:left;width:16px;height:22px;font-family:fontello;color:#7E7E7E;font-size:16px;line-height:22px;margin:2px 12px;cursor:pointer;">&#xe8ac;</span>' +
        '<span style="float:left;width:16px;height:22px;font-family:fontello;color:#7E7E7E;font-size:16px;line-height:22px;margin:2px 12px;cursor:pointer;">&#x1f44d;</span><span style="float:left;width:16px;height:22px;font-family:fontello;color:#7E7E7E;font-size:16px;line-height:22px;margin:2px 12px;cursor:pointer;">&#xe86d;</span><span style="float:left;width:16px;height:22px;font-family:fontello;color:#7E7E7E;font-size:16px;line-height:22px;margin:2px 12px;cursor:pointer;">&#xe86c;</span>' +
        '<span style="float:left;width:16px;height:22px;font-family:fontello;color:#7E7E7E;font-size:16px;line-height:22px;margin:2px 12px;cursor:pointer;">&#xe815;</span><span style="float:left;width:16px;height:22px;font-family:fontello;color:#7E7E7E;font-size:16px;line-height:22px;margin:2px 12px;cursor:pointer;">&#xe897;</span><span style="float:left;width:16px;height:22px;font-family:fontello;color:#7E7E7E;font-size:16px;line-height:22px;margin:2px 12px;cursor:pointer;">&#xe819;</span>' +
        '<span style="float:left;width:16px;height:22px;font-family:fontello;color:#7E7E7E;font-size:16px;line-height:22px;margin:2px 12px;cursor:pointer;">&#xe8cc;</span><span style="float:left;width:16px;height:22px;font-family:fontello;color:#7E7E7E;font-size:16px;line-height:22px;margin:2px 12px;cursor:pointer;">&#xf0e5;</span><span style="float:left;width:16px;height:22px;font-family:fontello;color:#7E7E7E;font-size:16px;line-height:22px;margin:2px 12px;cursor:pointer;">&#xf0fd;</span>' +
        '<span style="float:left;width:16px;height:22px;font-family:fontello;color:#7E7E7E;font-size:16px;line-height:22px;margin:2px 12px;cursor:pointer;">&#xf1d6;</span><span style="float:left;width:16px;height:22px;font-family:fontello;color:#7E7E7E;font-size:16px;line-height:22px;margin:2px 12px;cursor:pointer;">&#xf192;</span><span style="float:left;width:16px;height:22px;font-family:fontello;color:#7E7E7E;font-size:16px;line-height:22px;margin:2px 12px;cursor:pointer;">&#xf157;</span>' +
        '<span style="float:left;width:16px;height:22px;font-family:fontello;color:#7E7E7E;font-size:16px;line-height:22px;margin:2px 12px;cursor:pointer;">&#xe8ba;</span><span style="float:left;width:16px;height:22px;font-family:fontello;color:#7E7E7E;font-size:16px;line-height:22px;margin:2px 12px;cursor:pointer;">&#xf27a;</span><span style="float:left;width:16px;height:22px;font-family:fontello;color:#7E7E7E;font-size:16px;line-height:22px;margin:2px 12px;cursor:pointer;">&#xe8b5;</span>' +
        '<span style="float:left;width:16px;height:22px;font-family:fontello;color:#7E7E7E;font-size:16px;line-height:22px;margin:2px 12px;cursor:pointer;">&#xe8a9;</span><span style="float:left;width:16px;height:22px;font-family:fontello;color:#7E7E7E;font-size:16px;line-height:22px;margin:2px 12px;cursor:pointer;">&#xf12a;</span><span style="float:left;width:16px;height:22px;font-family:fontello;color:#7E7E7E;font-size:16px;line-height:22px;margin:2px 12px;cursor:pointer;">&#xe888;</span>' +
        '<span style="float:left;width:16px;height:22px;font-family:fontello;color:#7E7E7E;font-size:16px;line-height:22px;margin:2px 12px;cursor:pointer;">&#xe8ab;</span><span style="float:left;width:16px;height:22px;font-family:fontello;color:#7E7E7E;font-size:16px;line-height:22px;margin:2px 12px;cursor:pointer;">&#xf109;</span><span style="float:left;width:16px;height:22px;font-family:fontello;color:#7E7E7E;font-size:16px;line-height:22px;margin:2px 12px;cursor:pointer;">&#xf0ed;</span>' +
        '<span style="float:left;width:16px;height:22px;font-family:fontello;color:#7E7E7E;font-size:16px;line-height:22px;margin:2px 12px;cursor:pointer;">&#xf0ee;</span><span style="float:left;width:16px;height:22px;font-family:fontello;color:#7E7E7E;font-size:16px;line-height:22px;margin:2px 12px;cursor:pointer;">&#xf25d;</span><span style="float:left;width:16px;height:22px;font-family:fontello;color:#7E7E7E;font-size:16px;line-height:22px;margin:2px 12px;cursor:pointer;">&#xf1f9;</span>' +
        '</div><div style="text-align:center;margin-top:20px;"><button id="_btn_">确认</button></div></div>',
    videoHtml: '<pre contenteditable="false" style="padding:1px;margin:0;" class="k_video_pre"><video controls="true" id="my_video_id" class="k_video_el video-js vjs-default-skin vjs-big-play-centered" src="_$video_url$_" controls preload="auto"   data-setup="{}">' +
        '_$source$_<object id="flash_fallback_1" class="vjs-flash-fallback"  type="application/x-shockwave-flash" data="javascript/video/flowplayer.swf">' +
        '<param name="movie" value="javascript/video/flowplayer.swf" /><param name="allowfullscreen" value="true" />' +
        '<param name="flashvars" value="config={"playlist":["images/confident.jpg", {"url": "_$video_url$_","autoPlay":false,"autoBuffering":true}]}" />' +
        '</object></video></pre>',
    borderForm: '<table style="width:300px;margin-bottom:0;padding-top:6px;" class="form_table k_edit_tools_table" >' +
        '<tr><td style="width:100px"><span style="padding:2px 12px;">边框位置：</span></td><td><span id="k_border_pos" style="display:inline-block;"><span class="_border_pos" v="left" style="border-width: 1px 1px 1px 2px; border-style: dashed dashed dashed solid; border-color: rgb(204, 204, 204) rgb(204, 204, 204) rgb(204, 204, 204) rgb(0, 0, 255); border-image: initial; cursor: pointer; display: inline-block; width: 15px; height: 15px; margin-right: 8px; background-color: transparent;"></span><span class="_border_pos" v="top" style="border-width: 2px 1px 1px; border-style: solid dashed dashed; border-color: rgb(0, 0, 255) rgb(204, 204, 204) rgb(204, 204, 204); border-image: initial; cursor: pointer; display: inline-block; width: 15px; height: 15px; margin-right: 8px; background-color: transparent;"></span><span class="_border_pos" v="right" style="border-width: 1px 2px 1px 1px; border-style: dashed solid dashed dashed; border-color: rgb(204, 204, 204) rgb(0, 0, 255) rgb(204, 204, 204) rgb(204, 204, 204); border-image: initial; cursor: pointer; display: inline-block; width: 15px; height: 15px; margin-right: 8px; background-color: transparent;"></span><span class="_border_pos" v="bottom" style="border-width: 1px 1px 2px; border-style: dashed dashed solid; border-color: rgb(204, 204, 204) rgb(204, 204, 204) rgb(0, 0, 255); border-image: initial; cursor: pointer; display: inline-block; width: 15px; height: 15px; margin-right: 8px; background-color: transparent;"></span></span></td></tr>' +
        '<tr><td style="width:100px"><span style="padding:2px 12px;">边框类型：</span></td><td><select id="k_border_style"  style="width:95px;"><option selected="selected" value="solid">实线</option><option value="dotted">点状线</option><option value="dashed">长点状线</option></select></td></tr>' +
        '<tr><td style="width:100px"><span style="padding:2px 12px;">边框大小：</span></td><td><select id="k_border_size"  style="width:95px;"><option value="1px" selected="selected">1px</option><option value="2px">2px</option><option value="3px">3px</option><option value="4px">4px</option><option value="5px">5px</option><option value="6px">6px</option><option value="7px">7px</option><option value="8px">8px</option><option value="9px">9px</option><option value="10px">10px</option></select></td></tr>' +
        '<tr><td style="width:100px"><span style="padding:2px 12px;">边框颜色：</span></td><td><span><i id="k_img_color" unbindcolorev="1" style="border: 1px solid rgb(245, 245, 245); padding-left: 12px; color: rgb(255, 255, 255); background-color: rgb(143, 143, 143); cursor: pointer; opacity: 1;" class="fa fa-brush"></i></span></td></tr>' +
        '<tr><td colspan="2" style="text-align:center;padding-top:6px;"><button id="_cls_btn" style="cursor:pointer;height:24px;line-height:22px;">关闭</button></td></tr></table>',
    paragraphForm: '<table style="width:100%;margin-bottom:0;" class="form_table k_edit_tools_table" >' +
        '<tr><td style="text-align:right;width:65px;" class="k_edit_tools_table_label">缩进：</td><td style="width:130px;"><input autocomplete="off" title="请输入字符数" style="width:100px" placeholder="字符" id="_indent_"  name="indent" type="text"/></td><td ><span style="padding:2px 12px;">边框位置：</span><span id="_border_pos" style="display:inline-block;"><span class="_border_pos" v="left" style="border-width: 1px 1px 1px 2px; border-style: dashed dashed dashed solid; border-color: rgb(204, 204, 204) rgb(204, 204, 204) rgb(204, 204, 204) rgb(0, 0, 255); border-image: initial; cursor: pointer; display: inline-block; width: 15px; height: 15px; margin-right: 8px; background-color: transparent;"></span><span class="_border_pos" v="top" style="border-width: 2px 1px 1px; border-style: solid dashed dashed; border-color: rgb(0, 0, 255) rgb(204, 204, 204) rgb(204, 204, 204); border-image: initial; cursor: pointer; display: inline-block; width: 15px; height: 15px; margin-right: 8px; background-color: transparent;"></span><span class="_border_pos" v="right" style="border-width: 1px 2px 1px 1px; border-style: dashed solid dashed dashed; border-color: rgb(204, 204, 204) rgb(0, 0, 255) rgb(204, 204, 204) rgb(204, 204, 204); border-image: initial; cursor: pointer; display: inline-block; width: 15px; height: 15px; margin-right: 8px; background-color: transparent;"></span><span class="_border_pos" v="bottom" style="border-width: 1px 1px 2px; border-style: dashed dashed solid; border-color: rgb(204, 204, 204) rgb(204, 204, 204) rgb(0, 0, 255); border-image: initial; cursor: pointer; display: inline-block; width: 15px; height: 15px; margin-right: 8px; background-color: transparent;"></span></span></td></tr>' +
        '<tr><td class="k_edit_tools_table_label"  style="text-align:right;">上边距：</td><td><input autocomplete="off"  title="单位像数"  style="width:100px"  placeholder="像素" id="_pdding-top_" name="margin-top" type="text"/></td><td><span style="padding:2px 12px;">边框类型：</span><select id="_border_style"  style="width:66px;"><option selected="selected" value="solid">实线</option><option value="dotted">点状线</option><option  value="dashed">长点状线</option></select></td></tr>' +
        '<tr><td class="k_edit_tools_table_label"  style="text-align:right;">下边距：</td><td><input autocomplete="off"  title="单位像数" style="width:100px"  placeholder="像素" id="_pdding-bottom_" name="margin-bottom" type="text"/></td><td><span style="padding:2px 12px;">边框大小：</span><select id="_border_size"><option value="1px" selected="selected">1px</option><option value="2px">2px</option><option value="3px">3px</option><option value="4px">4px</option><option value="5px">5px</option><option value="6px">6px</option><option value="7px">7px</option><option value="8px">8px</option><option value="9px">9px</option><option value="10px">10px</option></select></td></tr>' +
        '<tr><td class="k_edit_tools_table_label"  style="text-align:right;">左边距：</td><td><input autocomplete="off"  title="单位像数" style="width:100px"  placeholder="像素" id="_padding-left_" name="padding-left" type="text"/></td><td><span style="padding:2px 12px;">边框颜色：</span><span id="_border_color" unbindcolorev="1" style="background-color:#4F4B49;"><i style="border: 1px solid rgb(245, 245, 245); padding-left: 12px;  cursor: pointer;padding-left: 5px ;padding-right:5px;" class="fa fa-brush">' + svgIcon.pen + '</i></span></td></tr>' +
        '<tr><td class="k_edit_tools_table_label"  style="text-align:right;">右边距：</td><td><input autocomplete="off"  title="单位像数" style="width:100px"  placeholder="像素" id="_padding-right_" name="padding-right" type="text"/></td><td><span style="padding:2px 6px 2px 12px;">背景色：</span><span unbindcolorev="1" id="_bg_color"><i   style="border: 1px solid #AAAAAA; padding-left: 2px;cursor: pointer; padding-left: 5px ;padding-right:5px;" class="fa fa-brush">' + svgIcon.pen + '</i></span><span style="padding:2px 6px 2px 20px;">字体颜色：</span><span id="_font_color" unbindcolorev="1"><i style="border: 1px solid #AAAAAA; padding-left: 5px ;padding-right:5px; color: #ffffff; cursor: pointer; opacity: 1;" class="fa fa-brush">' + svgIcon.pen + '</i></span></td></tr>' +
        '<tr><td class="k_edit_tools_table_label"  style="text-align:right;">行高：</td><td colspan="2"><select style="width:100px"  id="_line-height_"  name="line-height" style="width:124px"><option name="line-height" value="1.5">单倍</option><option name="line-height" value="1.8">1.5倍</option><option name="line-height" value="2">双倍</option><option name="line-height" value="2.5">高倍</option><option value="">自定义</option></select><span style="padding:2px 12px;"><input placeholder="像数" type="text" id="txt_line_h" style="width:60px"/></span></td></tr>' +
        '</table>',
    fileForm: "<div><div id='_swith_tool' class='k_box_size' style='line-height:16px;border-bottom:1px solid #666666;height:23px;'><a  class='k_box_size'  style='cursor:pointer;padding:2px 6px;background: rgb(102, 102, 102);color:#fff;' id='_local_upload_'>本地上传</a>&nbsp;&nbsp;&nbsp;&nbsp;<a class='k_box_size' id='_remote_upload_' style='cursor:pointer;padding:2px 6px;'>在线附件</a></div>" +
        "<table style='margin-top:20px;padding-left:30px;width:100%;' id='_local_upload_form'  class='form_table k_edit_tools_table'>" +
        "<tr><td><div id='_file_upload_wrap'></div></td></tr></table>" +
        "<table style='margin-top:20px;padding-left:30px;display:none;' id='_remote_upload_form' class='form_table k_edit_tools_table'>" +
        "<tr><td style='width:55px'>地址：</td><td><input id='_addr_url' autocomplete='off' type='text' style='width:380px'/></td></tr></table>" +        
        "<div style='line-height:30px;padding-left:32px;'>名称：&nbsp;&nbsp;&nbsp;&nbsp;<input autocomplete='off' type='text' id='file_name' style='width:380px;'/></div>" +       
        "<div style='text-align:center;margin-top:16px;height:35px'><button id='_submit_upload_' style='cursor:pointer;'>确认</button></div><div id='_upload_res_msg'></div></div>",
    
    linkForm: "<table style='margin-top:20px' class='form_table k_edit_tools_table'><tr><td style='width:50px'>名称：</td><td><input autocomplete='off' type='text' id='_link_name'  style='width:300px'/></td></tr>" +
        "<tr><td>地址：</td><td><input id='_addr_url' autocomplete='off' type='text' style='width:300px'/></td></tr>" +
        "<tr><td colspan='2'>是否新窗口打开：<input id='_target_' class='k_check_style' checked='checked' type='checkbox' /></td></tr>" +
        "<tr><td colspan='2' style='text-align:center;padding:4px 5px;'><button style='cursor:pointer;' id='okbtn'>确认</button></td></tr></table>",
    linkMenu:'<div><p><span>访问：</span><a id="_op_link" target="blank"></a></p><p id="_del_link">删除链接</p></div>',
    splitForm: "<table style='margin-top:2px' class='form_table k_edit_tools_table'>" +
        "<tr><td style='width:80px;text-align:right;'>颜色：</td><td><input unbindcolorev='' id='_line_color_' style='width:150px' type='text' readonly='readonly'/></td></tr>" +
        "<tr><td style='width:80px;text-align:right;padding: 2px 16px 2px 2px;'>大小：</td><td><select id='_line_size' style='width:150px'><option selected='selected' value='1px'>1px</option><option value='2px'>2px</option><option value='3px'>3px</option><option value='4px'>4px</option><option value='5px'>5px</option><option value='6px'>6px</option><option value='7px'>7px</option><option value='8px'>8px</option><option value='9px'>9px</option><option value='10px'>10px</option></select></td></tr>" +
        "<tr><td style='width:80px;text-align:right;'>类型：</td><td><select  id='_line_style' style='width:150px;padding: 2px 16px 2px 2px;'><option selected='selected' value='solid'>单实线</option><option  value='dotted'>点状</option><option  value='dashed'>长点状</option><option  value='double'>双实线</option></select></td></tr>" +
        "<tr><td style='text-align:left;padding-left:28px;' colspan='2'>上边距：&nbsp;<input autocomplete='off' id='_line_top' type='text' placeholder='px' style='width:50px;padding:0px 2px'/> 下边距：<input id='_line_bottom' type='text' placeholder='px'  style='width:50px;padding:0px 2px'/></td></tr></table>",
    tablePropsForm: "<table class='k_edit_props_form'><tr><td>修饰对象:</td><td>表格<input autocomplete='off' value='1' id='_target_table_1' name='_target_' type='radio' style='margin:0px 4px'/>单元格<input  style='margin:0px 4px'  checked='checked' id='_target_cell_1'  value='0' name='_target_' type='radio'/></td></tr>" +
        "<tr><td  style='text-align:rgiht'>修饰边框:</td><td class='k_edit_border_wrap' id='k_edit_border_wrap'><div  title='上边框' tg='border-top' class='border_top _border_it'></div><div  title='右边框'  tg='border-right' class='border_right _border_it'></div><div  title='下边框' tg='border-bottom' class='border_bottom _border_it'></div><div  title='左边框' tg='border-left'  class='border_left _border_it'></div></td></tr>" +
        "<tr><td  style='text-align:rgiht'>边框大小:</td><td><select id='_border_size_opts_' style='width:50px;padding: 1px 16px 1px 1px;'><option selected='selected' value='1px'>细</option><option value='2px'>中</option><option value='3px'>粗</option></select><div id='_border_size_input' style='height:28px;width:60px;float:right;'></div></td></tr>" +
        "<tr><td  style='text-align:rgiht'>边框类型:</td><td><select id='_border_style_opts_' style='width:110px;padding: 1px 16px 1px 1px;'><option value='solid'>solid</option><option selected='selected' value='dashed'>dashed</option><option value='double'>double</option></select></td></tr>" +
        "<tr><td  style='text-align:right'>颜色：</td><td><div id='_border_color_picker' unbindcolorev='1' title='边框颜色' class='k_box_size _color_picker' style='background-color:#313131; width:20px;border:1px solid #ADA8A3;height:18px;float:left;margin-right:10px;'><i class='fa fa-brush-1'></i></div><div id='_font_color_picker' unbindcolorev='1' title='字体颜色' class='k_box_size _color_picker' style='width:20px;border:1px solid #ADA8A3;height:18px;float:left;margin-right:10px;'><i class='fa fa-brush-1'></i></div><div id='_fill_color_picker' unbindcolorev='1' title='填充颜色' class='k_box_size _color_picker' style='float:left;width:20px;border:1px solid #ADA8A3;height:18px;margin-right:10px;'><i class='fa fa-brush-1'></i></div></td></tr>" +
        "<tr><td  style='text-align:right'>对齐：</td><td id='_table_align'><div title='左对齐' class='k_box_size h_left _h' ><i style='position:relative;bottom:2px;' class='fa fa-align-left'></i></div><div  title='居中对齐' class='k_box_size h_center  _h'><i style='position:relative;bottom:2px;left:0px' class='fa fa-align-center'></i></div><div title='右对齐' class='k_box_size h_right  _h' ><i style='position:relative;bottom:2px;left:3px;' class='fa  fa-align-right'></i></div><div title='顶部对齐' class='k_box_size v_top _v' ><i style='position:relative;bottom:8px;left:2px;' class='fa  fa-menu-1'></i></div><div title='重直居中' class='k_box_size v_middle _v' ><i style='position:relative;bottom:3px;left:2px;' class='fa  fa-menu-1'></i></div><div title='底部对齐' class='k_box_size v_bottom _v' ><i style='position:relative;bottom:0px;left:2px;' class='fa  fa-menu-1'></i></div></td></tr>" + "<tr class='_insert_op_tr edit_prop_tr_hover' id='_insert_op_tr'><td colspan='2' class='k_edit_delete_op_tr'><div style='text-align:center;line-height:20px;'>插入<i style='float:right;font-size:16px;line-height:22px;' class='fa fa-angle-double-right'></i></div></td></tr>" +
        "<tr class='_delete_op_tr edit_prop_tr_hover' id='_delete_op_tr'><td colspan='2' class='k_edit_delete_op_tr'><div style='text-align:center;line-height:20px;'>删除<i style='float:right;font-size:16px;line-height:22px;' class='fa fa-angle-double-right'></i></div></td></tr>" +
        "<tr class='edit_prop_tr_hover' id='_graph_td_' ><td colspan='2' style='text-align:center;cursor:pointer;line-height:20px;'><i class='fa fa-doc-text' style='margin-right:4px'></i><a style='cursor: pointer;'>段落设置</a></td></tr>" +
        "<tr class='edit_prop_tr_hover' id='_split_merge_td_' style='display:none;'><td colspan='2' style='text-align:center;cursor:pointer;line-height:20px;'><i class='fa fa-columns' style='margin-right:4px'></i><a style='cursor: pointer;'>合并</a></td></tr>" +
        "</table>",
    imageForm: "<div><div id='_swith_tool' class='k_box_size' style='line-height:16px;border-bottom:1px solid #666666;height:23px;'><a  class='k_box_size'  style='cursor:pointer;padding:2px 6px;background: rgb(102, 102, 102);color:#fff;' id='_local_upload_'>本地上传</a>&nbsp;&nbsp;&nbsp;&nbsp;<a class='k_box_size' id='_remote_upload_' style='cursor:pointer;padding:2px 6px;'>网络图片</a></div>" +
        "<table style='margin-top:20px;padding-left:30px;width:100%;' id='_local_upload_form'  class='form_table k_edit_tools_table'>" +
        "<tr><td><div id='_file_upload_wrap'></div></td></tr></table>" +
        "<table style='margin-top:20px;padding-left:30px;display:none;' id='_remote_upload_form' class='form_table k_edit_tools_table'>" +
        "<tr><td style='width:55px'>地址：</td><td><input id='_addr_url' autocomplete='off' type='text' style='width:380px'/></td></tr></table>" +
        "<div style='line-height:30px;padding-left:35px;padding-top:5px;'>边距：&nbsp;&nbsp;&nbsp;&nbsp;上：<input autocomplete='off' id='_top_k_padding' value='0' type='text' style='width:50px;padding:1px 2px;'/>&nbsp;&nbsp;" +
        "下：<input id='_bottom_k_padding' autocomplete='off' value='0'  type='text' style='width:50px;padding:1px 2px;'/>&nbsp;&nbsp;左：<input value='0' autocomplete='off' id='_left_k_padding' type='text' style='width:50px;padding:1px 2px;'/>&nbsp;&nbsp;" +
        "右：<input id='_right_k_padding' autocomplete='off' value='0'  type='text' style='width:50px;padding:1px 2px;'/>&nbsp;&nbsp;</div>" +
        "<div style='line-height:30px;padding-left:35px;'>高度：&nbsp;&nbsp;&nbsp;&nbsp;<input autocomplete='off' type='text' id='_img_height' style='width:80px;padding:1px 2px;'/>&nbsp;&nbsp;&nbsp;&nbsp;宽度：<input id='_img_width' type='text' style='width:80px;padding:1px 2px;'/></div>" +
        "<div style='line-height:30px;padding-left:35px;padding-top:10px;' id='k_edit_img_layout' class='clearfix'><div style='float:left;'>位置：</div><a v='left' title='文字右环绕' class='k_edit_img_pos k_edit_img_pos_left'></a><a v='center' title='无环绕独占一行' class='k_edit_img_pos k_edit_img_pos_center'></a><a v='right' title='文字左环绕' class='k_edit_img_pos k_edit_img_pos_right'></a><a v='inner' title='行内图片' class='k_edit_img_pos k_edit_img_pos_innser pos_actived'></a><span style='color:red;margin-left:12px;display:block;float:left;'>请注意选择图片位置！</span><input type='hidden' id='_img_pos' value='inner'/></div>" +
        "<div style='line-height:30px;padding-left:35px;padding-top:10px;' class='clearfix'><div style='float:left;'>跳转地址：</div><input id='_href_' autocomplete='off' type='text' placeholder='点击图片打开的连接地址' style='width:380px'/></div>" +
        "<div style='text-align:center;margin-top:16px;height:35px'><button id='_submit_upload_' style='cursor:pointer;'>确认</button></div><div id='_upload_res_msg'></div></div>",
    imgEditForm:'<div><div style="line-height:30px;padding-left:35px;padding-top:15px;">边距：&nbsp;&nbsp;&nbsp;&nbsp;上：<input autocomplete="off" id="_top_k_padding" value="0" type="text" style="width:50px;padding:1px 2px;">&nbsp;&nbsp;'
        +'下：<input id="_bottom_k_padding" autocomplete="off" value="0" type="text" style="width:50px;padding:1px 2px;">&nbsp;&nbsp;左：<input value="0" autocomplete="off" id="_left_k_padding" type="text" style="width:50px;padding:1px 2px;">&nbsp;&nbsp;'
        +'右：<input id="_right_k_padding" autocomplete="off" value="0" type="text" style="width:50px;padding:1px 2px;">&nbsp;&nbsp;</div><div style="line-height:30px;padding-left:35px;">'
        +'高度：&nbsp;&nbsp;&nbsp;&nbsp;<input autocomplete="off" type="text" id="_img_height" style="width:80px;padding:1px 2px;">&nbsp;&nbsp;&nbsp;&nbsp;宽度：<input id="_img_width" type="text" style="width:80px;padding:1px 2px;"></div>'
        +'<div style="text-align:center;margin-top:16px;height:35px"><button id="_submit_upload_" style="cursor:pointer;">确认</button></div></div>',
    videoForm: "<div><div class='k_box_size' style='line-height:16px;border-bottom:1px solid #666666;height:23px;'><a  class='k_box_size'  style='cursor:pointer;padding:2px 6px;' id='_local_upload_'>本地上传</a>&nbsp;&nbsp;&nbsp;&nbsp;<a class='k_box_size' id='_remote_upload_' style='cursor:pointer;padding:2px 6px;'>网络视频</a></div>" +
        "<table style='margin-top:20px;padding-left:30px;' id='_local_upload_form'  class='form_table k_edit_tools_table'>" +
        "<tr><td colspan='2' id='_video_upload_wrap'></td></tr></table>" +
        "<table style='margin-top:20px;padding-left:30px;' id='_remote_upload_form' class='form_table k_edit_tools_table'>" +
        "<tr><td style='width:55px'>地址：</td><td><input id='_addr_video' type='text' autocomplete='off' style='width:380px'/></td></tr></table>" +
        "<div style='line-height:30px;padding-left:35px;padding-top:0px;'>边距：&nbsp;&nbsp;&nbsp;&nbsp;上：<input autocomplete='off' id='_top_v_padding' value='4' type='text' style='width:50px;padding:1px 2px;'/>&nbsp;&nbsp;" +
        "下：<input id='_bottom_v_padding' autocomplete='off' value='4'  type='text' style='width:50px;padding:1px 2px;'/>&nbsp;&nbsp;左：<input autocomplete='off' value='2' id='_left_v_padding' type='text' style='width:50px;padding:1px 2px;'/>&nbsp;&nbsp;" +
        "右：<input id='_right_v_padding' autocomplete='off' value='2'  type='text' style='width:50px;padding:1px 2px;'/>&nbsp;&nbsp;</div>" +
        "<div style='line-height:30px;padding-left:35px;'>高度：&nbsp;&nbsp;&nbsp;&nbsp;<input type='text' id='_video_height' autocomplete='off' value='360' style='width:80px;padding:1px 2px;'/>&nbsp;&nbsp;&nbsp;&nbsp;宽度：<input value='580' id='_video_width' type='text' style='width:80px;padding:1px 2px;'/></div>" +
        "<div style='line-height:30px;padding-left:35px;'><span title='以视频文件名结束的地址,如:xx.mp4' style='display:none;' id='_is_real_video'>是否纯视频地址:&nbsp;&nbsp;&nbsp;&nbsp;<label class='k_checkbox_label k_checkbox_anim'><input type='checkbox' id='_video_is' value='360' style='width:80px;padding:1px 2px;'/><i class='k_checkbox_i'></i></label></span></span><span id='_is_loop_play'>是否循环播放:&nbsp;&nbsp;&nbsp;&nbsp;<label class='k_checkbox_label k_checkbox_anim'><input type='checkbox' id='_video_repeat' value='360' style='width:80px;padding:1px 2px;'/><i class='k_checkbox_i'></i></label></span></div>" +
        "<div style='text-align:center;margin-top:16px;height:35px'><button id='_submit_upload_' style='cursor:pointer;'>确认</button></div><div id='_upload_res_msg'></div></div>",
    closedForm: "<table style='margin-top:20px' class='form_table k_edit_tools_table'><tr><td style='width:150px;text-align:right'>向下折叠：</td><td style='padding-right:30px'><select style='padding-right:5px;padding-left:5px;margin-right:6px;' id='_section_c'><option selected='selected' value='1'>1</option><option value='2'>2</option><option value='3'>3</option><option value='4'>4</option><option value='5'>5</option></select>段落</td></tr>" +
        "<tr><td style='width:150px;text-align:right'>初始状态： </td><td style='padding-right:30px'><label class='k_radio_label k_radio_anim'><input autocomplete='off' type='radio' name='_status' value='1' checked='checked'><i class='k_radio_i'></i>展开</label><label class='k_radio_label k_radio_anim'><input type='radio' name='_status' value='0'><i class='k_radio_i'></i>收起</label></td></tr>" +
        "<tr><td colspan='2' style='text-align:center;'><button style='cursor:pointer;'>确认</button></td></tr></table>",
    codeForm: '<div style="width:100%;height:100%;position:relative;" class="k_box_size"><div id="toolbar" style="height:25px;width:100%;position:absolute;top:0;z-index:2;border:1px dashed #ccc;">'
        + '<span style="float:left;margin:0 20px;">代码修饰：</span><span class="font_css" id="bold"  tabindex="0" title="加粗代码" style="display:block;width:20px;height:20px;float:left;margin:0 2px;margin-top:3px;cursor:pointer;font-weight: bold;text-align:center;line-height:20px;">B</span>'
        + '<span id="normal"  class="font_css"   title="取消加粗" tabindex="0" style="display:block;width:20px;height:20px;float:left;margin:0 2px;margin-top:3px;cursor:pointer;margin-left:20px;line-height:20px;text-align:center;">B</span>'
        + '<span id="red" color="#FF0005"  class="font_css"  title="字体红色" tabindex="0" style="display:block;width:20px;height:20px;float:left;margin:0 2px;margin-top:3px;cursor:pointer;margin-left:20px;line-height:20px;text-align:center;color:red;">A</span>'
        + '<span id="greed" color="#0FE000"  class="font_css"  title="绿色" tabindex="0" style="display:block;width:20px;height:20px;float:left;margin:0 2px;margin-top:3px;cursor:pointer;margin-left:20px;line-height:20px;text-align:center;color:#0FE000;">A</span>'
        + '<span id="blank" color="#666666"  class="font_css"  title="淡黑色" tabindex="0" style="display:block;width:20px;height:20px;float:left;margin:0 2px;margin-top:3px;cursor:pointer;margin-left:20px;line-height:20px;text-align:center;color:#666666;">A</span>'
        + '<span id="blank1" color="#000000"  class="font_css"  title="黑色" tabindex="0" style="display:block;width:20px;height:20px;float:left;margin:0 2px;margin-top:3px;cursor:pointer;margin-left:20px;line-height:20px;text-align:center;color:#666666;">A</span>'
        + '<span id="blinkred" color="#FF00D1"  class="font_css"  title="粉红色" tabindex="0" style="display:block;width:20px;height:20px;float:left;margin:0 2px;margin-top:3px;cursor:pointer;margin-left:20px;line-height:20px;text-align:center;color:#FF00D1;">A</span>'
        + '<span id="yello" color="#FFD600"  class="font_css"  title="黄色" tabindex="0" style="display:block;width:20px;height:20px;float:left;margin:0 2px;margin-top:3px;cursor:pointer;margin-left:20px;line-height:20px;text-align:center;color:#FFD600;">A</span>'
        + '<span id="blue" color="#0089FF"  class="font_css"  title="蓝色" tabindex="0" style="display:block;width:20px;height:20px;float:left;margin:0 2px;margin-top:3px;cursor:pointer;margin-left:20px;line-height:20px;text-align:center;color:#0089FF;">A</span>'
        + '<span id="bluelingth" color="#A5A5FF"  class="font_css"  title="淡蓝色" tabindex="0" style="display:block;width:20px;height:20px;float:left;margin:0 2px;margin-top:3px;cursor:pointer;margin-left:20px;line-height:20px;text-align:center;color:#A5A5FF;">A</span>'
        + '<span id="fontSize" tabindex="0" style="display:block;height:24px;float:left;margin:0 2px;margin-top:3px;cursor:pointer;margin-left:20px;line-height:20px;text-align:center;">字号：<select tabindex="0"><option value="14">14px</option><option value="16">16px</option><option value="18">18px</option></select></span>'
        + '<span id="isClearBackground" tabindex="0" style="display:block;height:24px;float:left;margin:0 2px;margin-top:3px;cursor:pointer;margin-left:25px;line-height:20px;text-align:center;">是否保留复制携带的背景色：<select tabindex="0"><option value="0">否</option><option value="1">是</option></select></span>'
        + '<span id="_clr_btn_" class="_btn_clr"  title="清空" tabindex="0" style="display:block;width:60px;height:20px;float:left;margin:0 2px;margin-top:3px;cursor:pointer;margin-left:20px;line-height:20px;text-align:center;color:#0089FF;">清空</span>'
        + '</div><div class="k_box_size" style="width:100%;height:100%;position:relative;z-index:1;border:1px dashed #ccc;border-bottom:35px solid #fff;border-top:28px solid #fff;"><pre id="input_txt" class="k_box_size" contenteditable="true" autocapitalize="off" autocomplete="off" autocorrect="off" spellcheck="false" style="width:100%;height:100%;padding:6px 10px;overflow:auto;"><div id="k_code_input" class="k_code_input" style="position:relative;width:100%;height:100%;min-width:100%;min-height:100%;box-sizing: border-box;-moz-box-sizing: border-box;-webkit-box-sizing: border-box;"><div>\u200B</div><div>\u200B</div></div></pre></div>'
        + '<div class="k_box_size" style="padding-top:4px;position:absolute;width:100%;height:35px;bottom:0;z-index:2;text-align:center;border-top:1px dashed #ccc;"><button>确认</button></div></div>',    
    map: {
        windowSize: { width: 600, height: 500 },
        insertSize: { height: 500, width: '100%' },
        page: 'map.html',
    },
    imgPos:"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABMAAABECAYAAABj2w6DAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoV2luZG93cykiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MDkwRjc4MTk0MjgwMTFFOUI0MUJEQjFBOUFDRjhEOEQiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MDkwRjc4MUE0MjgwMTFFOUI0MUJEQjFBOUFDRjhEOEQiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDowOTBGNzgxNzQyODAxMUU5QjQxQkRCMUE5QUNGOEQ4RCIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDowOTBGNzgxODQyODAxMUU5QjQxQkRCMUE5QUNGOEQ4RCIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PutXo8oAAAYRSURBVHjazFfbbttGED17JSnJcZIWKIo89sPa/llf+l8F8tSiLZLYEi9765ldUZFtJbEDFwiB1e6K5HAuZ87MaudcUUoVAKex7uusVdnpTd1buALDZzRKL8/atjZoe40LVymlzhQmGwS7gC8i8kclLkqHxFuIXGeHZBTvPfOlvfcPzFxNlHtPEWYf89BPv78tb1NEbzfAIWChk3YemHOungSn1/oWdlkW9Sm/8V5d/10cfNpBc5+MR3QW+zDCFs94GMzR4N1VbmbeN3EdJzMnxjEHTgdGJcDGPWyWD2lEdQOnE/KkcCeaEr0awXtRzS61oRxsuOb/PVI3wEcHnbaYGU0G9a6Z68vrFUJoi0yTtUO2FmkOMJpwiQfOGQ4blKxgltLM1FpfBO5q5tbusBsX9NMNvMkwQcHnDoHCgnoPVSZ+LEBnRuS+RquW6/9zvMHSaSy9wj/6BgfDROh73lc49BHBRHg6zMYY1acgsZppDOGAV+hGgy01mGnugRDx9gr9KOjyNPcW2lp7Jx/PzZS8FWGvqKFZGEGVmIAHziMGO+FF2oPWQusJ15oJdclnspfx5Ay4BFrxo1wraB+dmysFndPOHZOFbgZVCKViSEVeOEP3hU4id3R8xtZnqkWrFpciWoEsFJOAxGgRtgwEnZRngpdR5j9QEfJpS/t05keoD0OGNioFFskUKC2fCwipVB4zeWKGLBRC2Bh5mIIgXuKvZTa4keglim0yHGJXQbKeGunKBp5JbkVQ1bO741dJPbHofho+Dzk+KfRHTD6oE5wlkPZHEt9NHhDTiG0Z4Jh7C30zqg16bRHziO9o7x8/v1Fa2+qnT2p2WzaIY4Bj7h3IVR+Y/eIPTbTn+RaBgt4lc8Td1LzNUSQxz2amHj9mEsw24rDMDK1D5xTmQkHEgmEORt4vfROmPC7icc0WrfYFcdYY8jVIAMQUBQ0bQoHlbHEwUWMTWqnwevfRJK1PkVwZxkaCOJWMPgktE1NdRGQWXamOtLOQTTUmnxoVTben+MtHz8EuDKOvaNpuEr2ZH3mH7vCSjPAKewpPhXxvNdTUXuzc6zvRFPPWfd/3Rd8wWmrXYUzvWRgonS4Pywg9OBYLhw8Eb+a6alb+reYZol80qoHS+mS2tuwQ0l5hS3SP5CRFp3eKDt9/wDYnOPFxuGm28TYFKJp4iqrsZT4cDkr3Q0Lc0AzPnKRJ0e2JwIB+w1ZEGxB5eHUs1b11p+hRu9bMHIF8nB9/9Rtc7Ja6rmsZ8BRh0wHqUrc0z3PzW++72mf1+mUjOSWE5E7915bzRtb0ucPVSavVzHVdSVawR48jBwFdy/xQbdBHGmg9WiU34dES71DPqiFNZQYx7H6UD/sGvpVhCWSVGHqJqMjNwqjxyJgFg2F2CPIZfZAYxVRriZXUC6JbIwLBYdXkqBFVzxRQdNNAaDLy75H5WzVnQyOkOjjzvOT4IJr9b3+WtSM6723XdV8WjCHi2nn89esb9VlhfuX2Y9V6cOUXTKcZo4lf1izRPyIsl3yimfPIBbYF7KUwxfKInpZs2wCUawVuyBDBLblLYoKxVlr2bOFLwpTAtZqTqI05wQRkj5rRdo/E3M5meIyZqZnFEp3QeEzWMTew+swGmeCfwvTlUmelfaLzz+d1LWMivshB+N5vvyws1+TUpyH78/8GRjORypf4Hv/rZYUhinR99EcvZnzs4z+mlVBzPY2U6mUlPYm0jJL5kpukfnK2cCkDLATKaM36I/UpefE4y0lw3cvjbLKlPUMvm0hFKNjnZz7X6StPwhvqcaoeQsXXq7/X2XmWNbMSp3SLWnqC4yHWkzyHUkvTlGb4qVnA40PzEZp7mu9YZJI6ZgP9ylbPSHxdc8fEkjXXPI3QgZ1jLaXS4GHP2lVqW3kagjXVZrk3VriwILN413pEHquFr8biWyVHfem89LXDYsuytmd/QZ85mhbYvA1Htv3cNf7yg1I7f3pXsXV66LOnpM+9d204xK/uue+/q0XVgT2DdewXWu/w6Ov+u9+wmc+Ks2/WZ+pT58yvM/N4vDs/4p3/95RxhxytHGG0+jbI8T8BBgBqVSMMHRY5/gAAAABJRU5ErkJggg=="
};
let userCfg = {};
if ($B.config && $B.config.editor) {
    userCfg = $B.config.editor;
}
$B["extendObjectFn"](editCfg, userCfg);