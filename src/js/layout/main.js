

/***
 * opts = {
       el:布局容器id，或者元素
       top:{size:高度,css: { background: "#DEFCFF" }, overflow: "diy/auto/hidden" , diyScrollColor:'当overflow为diy时候的滚动条颜色'}
       bottom:{size:高度,css: { background: "#DEFCFF" }, overflow: "diy/auto/hidden" , diyScrollColor:'当overflow为diy时候的滚动条颜色'}
       left:{size:宽度,css: { background: "#DEFCFF" }, overflow: "diy/auto/hidden" , diyScrollColor:'当overflow为diy时候的滚动条颜色',
                       resizebar: {
                            iconColor:"#000",
                            backgroundColor:"#C9B9FF",
                            iconColorAct:"#FFF",
                            backgroundColorAct:"#8BCBF7"
                        }}   
       right:{size:宽度,css: { background: "#DEFCFF" }, overflow: "diy/auto/hidden" , diyScrollColor:'当overflow为diy时候的滚动条颜色',
                        resizebar: {
                            iconColor:"#000",
                            backgroundColor:"#C9B9FF",
                            iconColorAct:"#FFF",
                            backgroundColorAct:"#8BCBF7"
                        }},
        onRender:(flag,el)=>{} //用于获取每个区域的容器内容的div元素
 * }
 * ***/
function Layout(opts) {
    var elObj, id, topEl, leftEl, bottomEl, rightEl, centerEL, readerEls = [];
    if (typeof opts.el === "string") {
        elObj = document.getElementById(opts.el);
        id = opts.el;
    } else {
        elObj = opts.el;
        d = $B.DomUtils.attribute(elObj, "id");
        if (!id) {
            id = $B.getShortID();
        }
    }
    $B.DomUtils.css(elObj, { "padding": "0 0 0 0", "position": "relative", "overflow": "hidden" });
    $B.DomUtils.addClass(elObj, "k_box_size k_layout_el");
    centerEL = $B.DomUtils.createEl("<div style='width:100%;height:100%;position:absolute;top:0;left:0;overflow:auto;' id='" + id + "_center' class='k_box_size'></div>");
    if (opts.center) {
        if (opts.center.css) {
            $B.DomUtils.css(centerEL, opts.center.css);
        }
        if (opts.center.overflow) {
            if (opts.center.overflow === "diy") {
                let tmp = $B.DomUtils.createEl("<div style='width:100%;height:100%'></div>");
                $B.DomUtils.append(centerEL, tmp);
                let $c = $B.myScrollbar(tmp, {style:{color:opts.center.diyScrollColor}});
                readerEls.push({ flag: "center", el: $c });
            } else {
                $B.DomUtils.css(centerEL, { overflow: opts.center.overflow });
                readerEls.push({ flag: "center", el: centerEL });
            }
        } else {
            readerEls.push({ flag: "center", el: centerEL });
        }
    }
    if (opts.top) {
        topEl = $B.DomUtils.createEl("<div style='width:100%;height:" + opts.top.size + "px;position:absolute;top:0;left:0;overflow:auto;' id='" + id + "_top' class='k_box_size'></div>");
        $B.DomUtils.css(centerEL, { "border-top": opts.top.size + "px solid #fff" });
        if (opts.top.css) {
            $B.DomUtils.css(topEl, opts.top.css);
        }
        if (opts.top.overflow) {
            if (opts.top.overflow === "diy") {
                let tmp = $B.DomUtils.createEl("<div style='width:100%;height:100%'></div>");
                $B.DomUtils.append(topEl, tmp);
                let $c = $B.myScrollbar(tmp, {});
                readerEls.push({ flag: "top", el: $c });
            } else {
                $B.DomUtils.css(topEl, { overflow: opts.top.overflow });
                readerEls.push({ flag: "top", el: topEl });
            }
        } else {
            readerEls.push({ flag: "top", el: topEl });
        }
    }
    let leftResizeFN;
    if (opts.left) {
        leftEl = $B.DomUtils.createEl("<div style='height:100%;width:" + opts.left.size + "px;position:absolute;top:0;left:0;overflow:auto;' id='" + id + "_left' class='k_box_size'></div>");
        if (opts.top) {
            $B.DomUtils.css(leftEl, { "border-top": opts.top.size + "px solid #fff" });
        }
        if (opts.bottom) {
            $B.DomUtils.css(leftEl, { "border-bottom": opts.bottom.size + "px solid #fff" });
        }
        $B.DomUtils.css(centerEL, { "border-left": opts.left.size + "px solid #fff" });
        if (opts.left.css) {
            $B.DomUtils.css(leftEl, opts.left.css);
        }
        if (opts.left.resizebar) {
            leftResizeFN = (e) => {     
                $B.DomUtils.attribute(leftEl, {"called":1});           
                let el = $B.DomUtils.children(leftEl, ".k_layout_resize")[0];
                let $i = el.lastChild;
                let siblings = $B.DomUtils.siblings(el);
                if (!$B.DomUtils.attribute(leftEl, "closed")) {
                    let w = $B.DomUtils.width(leftEl);
                    $B.DomUtils.attribute(leftEl, { "closed": true,"_w":w });
                    $B.hide(siblings);
                    el.style.width = "10px";
                    $B.animate($i, { "rotateZ": "180deg" }, { duration: 200 });
                    $B.animate(leftEl, { width: "10px" }, { duration: 200 });
                    $B.animate(centerEL, { "border-left-width": "10px" }, { duration: 200,complete:()=>{
                        try{
                            $B.DomUtils.trigger(window,"resize")
                        }catch(e){}                      
                    } });
                    if(opts.left.resizebar.backgroundColorAct){
                        $B.DomUtils.css(el,{"background-color":opts.left.resizebar.backgroundColorAct});
                    }
                    if(opts.left.resizebar.iconColorAct){
                        $B.DomUtils.css($i,{"color":opts.left.resizebar.iconColorAct});
                    }
                } else {      
                    let w = parseFloat($B.DomUtils.attribute(leftEl,"_w"));                               
                    $B.animate(leftEl, { width: w + "px" }, { duration: 200 });
                    $B.animate(centerEL, { "border-left-width":w + "px" }, { duration: 200 ,complete:()=>{
                        $B.animate($i, { "rotateZ": "0deg" }, { duration: 200 });
                        $B.show(siblings);
                        $B.DomUtils.removeAttribute(leftEl, "closed");  
                        if(opts.left.resizebar.backgroundColorAct){
                            $B.DomUtils.css(el,{"background-color":opts.left.resizebar.backgroundColor});
                        }
                        if(opts.left.resizebar.iconColorAct){
                            $B.DomUtils.css($i,{"color":opts.left.resizebar.iconColor});
                        } 
                        try{
                            $B.DomUtils.trigger(window,"resize")
                        }catch(e){}   
                        el.style.display = "none";   
                        el.style.width = "8px";                     
                    }});
                }
                setTimeout(()=>{
                    $B.DomUtils.removeAttribute(leftEl, "called");
                },500);             
            };
            $B.DomUtils.bind(leftEl, {
                mousemove: (e) => {
                    let el = $B.DomUtils.children(leftEl, ".k_layout_resize")[0];
                    if (!$B.DomUtils.attribute(leftEl, "closed")) {
                        let ofs = $B.DomUtils.offset(leftEl);
                        let w = $B.DomUtils.width(leftEl);
                        let left = ofs.left + w - 15;
                        let mouseX = e.pageX;
                        if (mouseX > left) {
                            let h = $B.DomUtils.height(leftEl) / 2 - 50;
                            if (h < 10) {
                                h = 10;
                            }
                            el.lastChild.style.top = h + "px";
                            el.style.display = "";
                        } else {
                            el.style.display = "none";
                        }                        
                    }
                },
                mouseleave: (e) => {
                    let el = $B.DomUtils.children(e.target, ".k_layout_resize")[0];
                    if (!$B.DomUtils.attribute(leftEl, "closed")) {
                        el.style.display = "none";
                    }
                }
            });
        }
        let notDiy = true;
        if (opts.left.overflow) {
            if (opts.left.overflow === "diy") {
                notDiy = false;
                let tmp = $B.DomUtils.createEl("<div style='width:100%;height:100%'></div>");
                $B.DomUtils.append(leftEl, tmp);
                let $c = $B.myScrollbar(tmp,  {style:{color:opts.left.diyScrollColor}});
                readerEls.push({ flag: "left", el: $c });
                if (leftResizeFN) {
                    let $rs = $B.createEl("<div class='k_layout_resize' style='cursor:pointer;position:absolute;top:0;right:0;width:8px;height:100%;background:"+opts.left.resizebar.backgroundColor+";padding:0;margin:0;display:none;'><i style='width:100%;text-align:center;font-size:12px;color:"+opts.left.resizebar.iconColor+";position:absolute;top:100px;' class='fa fa-angle-double-left'></i></div>");
                    $B.DomUtils.append(leftEl, $rs);
                    $B.DomUtils.click($rs, leftResizeFN);
                    bindDrag($rs,leftEl,centerEL);
                }
            } else {
                $B.DomUtils.css(leftEl, { overflow: opts.left.overflow });
            }
        }
        if (notDiy) {
            let $contentEL = leftEl;
            if (leftResizeFN) {
                $B.DomUtils.css(leftEl, { overflow: "hidden" });
                $contentEL = $B.createEl("<div style='height:100%;width:100%;overflow:auto'></div>");
                if (opts.left.overflow) {
                    $B.DomUtils.css($contentEL, { overflow: opts.left.overflow });
                }               
                $B.DomUtils.append(leftEl,$contentEL);
                let $rs = $B.createEl("<div class='k_layout_resize' style='cursor:pointer;position:absolute;top:0;right:0;width:8px;height:100%;background:"+opts.left.resizebar.backgroundColor+";padding:0;margin:0;display:none;'><i style='width:100%;text-align:center;font-size:12px;color:"+opts.left.resizebar.iconColor+";position:absolute;top:100px;' class='fa fa-angle-double-left'></i></div>");
                $B.DomUtils.append(leftEl, $rs);
                $B.DomUtils.click($rs, leftResizeFN);
                bindDrag($rs,leftEl,centerEL);
            }
            readerEls.push({ flag: "left", el: $contentEL });
        }
    }
    if (opts.bottom) {
        bottomEl = $B.DomUtils.createEl("<div style='width:100%;height:" + opts.bottom.size + "px;position:absolute;bottom:0;left:0;overflow:auto;' id='" + id + "_bottom' class='k_box_size'></div>");
        $B.DomUtils.css(centerEL, { "border-bottom": opts.bottom.size + "px solid #fff" });
        if (opts.bottom.css) {
            $B.DomUtils.css(bottomEl, opts.bottom.css);
        }
        if (opts.bottom.overflow) {
            if (opts.bottom.overflow === "diy") {
                let tmp = $B.DomUtils.createEl("<div style='width:100%;height:100%'></div>");
                $B.DomUtils.append(bottomEl, tmp);
                let $c = $B.myScrollbar(tmp, {});
                readerEls.push({ flag: "bottom", el: $c });
            } else {
                $B.DomUtils.css(bottomEl, { overflow: opts.bottom.overflow });
                readerEls.push({ flag: "bottom", el: bottomEl });
            }
        } else {
            readerEls.push({ flag: "bottom", el: bottomEl });
        }
    }
    let rightResizeFN;
    if (opts.right) {
        rightEl = $B.DomUtils.createEl("<div style='height:100%;width:" + opts.right.size + "px;position:absolute;bottom:0;right:0;overflow:auto;' id='" + id + "_right' class='k_box_size'></div>");
        $B.DomUtils.css(centerEL, { "border-right": opts.right.size + "px solid #fff" });
        if (opts.top) {
            $B.DomUtils.css(rightEl, { "border-top": opts.top.size + "px solid #fff" });
        }
        if (opts.bottom) {
            $B.DomUtils.css(rightEl, { "border-bottom": opts.bottom.size + "px solid #fff" });
        }
        if (opts.right.css) {
            $B.DomUtils.css(rightEl, opts.right.css);
        }
        if (opts.right.resizebar) {
            rightResizeFN = (e)=>{
                $B.DomUtils.attribute(rightEl, {"called":1});           
                let el = $B.DomUtils.children(rightEl, ".k_layout_resize")[0];
                let $i = el.lastChild;
                let siblings = $B.DomUtils.siblings(el);
                if (!$B.DomUtils.attribute(rightEl, "closed")) {
                    let w = $B.DomUtils.width(rightEl);
                    $B.DomUtils.attribute(rightEl, { "closed": true,"_w":w });
                    $B.hide(siblings);
                    el.style.width = "10px";
                    $B.animate($i, { "rotateZ": "180deg" }, { duration: 200 });
                    $B.animate(rightEl, { width: "10px" }, { duration: 200 });
                    $B.animate(centerEL, { "border-right-width": "10px" }, { duration: 200,complete:()=>{
                        try{
                            $B.DomUtils.trigger(window,"resize")
                        }catch(e){}                      
                    } });
                    if(opts.right.resizebar.backgroundColorAct){
                        $B.DomUtils.css(el,{"background-color":opts.right.resizebar.backgroundColorAct});
                    }
                    if(opts.right.resizebar.iconColorAct){
                        $B.DomUtils.css($i,{"color":opts.right.resizebar.iconColorAct});
                    }
                } else {      
                    let w = parseFloat($B.DomUtils.attribute(rightEl,"_w"));                               
                    $B.animate(rightEl, { width: w + "px" }, { duration: 200 });
                    $B.animate(centerEL, { "border-right-width":w + "px" }, { duration: 200 ,complete:()=>{
                        $B.animate($i, { "rotateZ": "0deg" }, { duration: 200 });
                        $B.show(siblings);
                        setTimeout(()=>{
                            $B.DomUtils.removeAttribute(rightEl, "closed"); 
                        },300);                         
                        if(opts.right.resizebar.backgroundColorAct){
                            $B.DomUtils.css(el,{"background-color":opts.right.resizebar.backgroundColor});
                        }
                        if(opts.right.resizebar.iconColorAct){
                            $B.DomUtils.css($i,{"color":opts.right.resizebar.iconColor});
                        } 
                        try{
                            $B.DomUtils.trigger(window,"resize")
                        }catch(e){}  
                        el.style.display = "none";   
                        el.style.width = "8px";                                       
                    }});
                }
                setTimeout(()=>{
                    $B.DomUtils.removeAttribute(rightEl, "called");
                },500);                 
            };
            $B.DomUtils.bind(rightEl, {
                mousemove: (e) => {
                    let el = $B.DomUtils.children(rightEl, ".k_layout_resize")[0];
                    if (!$B.DomUtils.attribute(rightEl, "closed")) {
                        let ofs = $B.DomUtils.offset(rightEl);
                        let left = ofs.left +  20;
                        let mouseX = e.pageX;
                        if (mouseX < left) {
                            let h = $B.DomUtils.height(rightEl) / 2 - 50;
                            if (h < 10) {
                                h = 10;
                            }
                            el.lastChild.style.top = h + "px";
                            el.style.display = "";
                            console.log("mousemove top "+h);
                        } else {
                            el.style.display = "none";
                        }                        
                    }
                },
                mouseleave: (e) => {
                    let el = $B.DomUtils.children(e.target, ".k_layout_resize")[0];
                    if (!$B.DomUtils.attribute(rightEl, "closed")) {
                        el.style.display = "none";
                    }
                }
            });
        }
        let notDiy = true;
        if (opts.right.overflow) {
            if (opts.right.overflow === "diy") {
                notDiy = false;
                let tmp = $B.DomUtils.createEl("<div style='width:100%;height:100%'></div>");
                $B.DomUtils.append(rightEl, tmp);
                let $c = $B.myScrollbar(tmp,  {style:{color:opts.right.diyScrollColor}});
                readerEls.push({ flag: "right", el: $c });
                if (rightResizeFN) {
                    let $rs = $B.createEl("<div class='k_layout_resize' style='display:none;cursor:pointer;position:absolute;top:0;left:0;width:8px;height:100%;background:"+opts.right.resizebar.backgroundColor+";padding:0;margin:0;'><i style='width:100%;text-align:center;font-size:12px;color:"+opts.right.resizebar.iconColor+";position:absolute;top:100px;' class='fa fa-angle-double-right'></i></div>");
                    $B.DomUtils.append(rightEl, $rs);
                    $B.DomUtils.click($rs, rightResizeFN);
                    bindDrag($rs,rightEl,centerEL,true);
                }
            } else {
                $B.DomUtils.css(rightEl, { overflow: opts.right.overflow });               
            }
        } 
        if(notDiy) {
            let $contentEL = rightEl;
            if (rightResizeFN) {
                $B.DomUtils.css(rightEl, { overflow: "hidden" });
                $contentEL = $B.createEl("<div style='height:100%;width:100%;overflow:auto'></div>");
                if (opts.right.overflow) {
                    $B.DomUtils.css($contentEL, { overflow: opts.right.overflow });
                }               
                $B.DomUtils.append(rightEl,$contentEL);
                let $rs = $B.createEl("<div class='k_layout_resize' style='cursor:pointer;position:absolute;top:0;left:0;width:8px;height:100%;background:"+opts.right.resizebar.backgroundColor+";padding:0;margin:0;display:none;'><i style='width:100%;text-align:center;font-size:12px;color:"+opts.right.resizebar.iconColor+";position:absolute;top:100px;' class='fa fa-angle-double-right'></i></div>");
                $B.DomUtils.append(rightEl, $rs);
                $B.DomUtils.click($rs, rightResizeFN);
                bindDrag($rs,rightEl,centerEL,true);
            }
            readerEls.push({ flag: "right", el: $contentEL });
        }
    }
    $B.DomUtils.append(elObj, centerEL);
    if (leftEl) {
        $B.DomUtils.append(elObj, leftEl);
    }
    if (rightEl) {
        $B.DomUtils.append(elObj, rightEl);
    }
    if (topEl) {
        $B.DomUtils.append(elObj, topEl);
    }
    if (bottomEl) {
        $B.DomUtils.append(elObj, bottomEl);
    }
    if (opts.onRender) {
        setTimeout(() => {
            for (let i = 0; i < readerEls.length; i++) {
                opts.onRender(readerEls[i].flag, readerEls[i].el);
            }
        }, 1);
    }
    return id;
}
function bindDrag($rs,panelEl,centerEL,isRight){
    $B.draggable($rs,{
        holdTime:350,
        isProxy: true, //是否产生一个空代理进行拖动
        axis:'h',
        onTimeIsUpFn:()=>{
            if( $B.DomUtils.attribute(panelEl,"called")){
                return true;
            }
            if($B.DomUtils.attribute(panelEl,"closed")){
                return true;
            }
        },
        onStopDrag:(e)=>{
            let dv = e.state._data;
            let leftOffset = dv.leftOffset;
            let newWidth,borderAttr ;
            if(isRight){
                newWidth = $B.DomUtils.width(panelEl) - leftOffset;
                borderAttr = "border-right-width";
            }else{
                newWidth = $B.DomUtils.width(panelEl) + leftOffset;
                borderAttr = "border-left-width";
            }           
            if(newWidth < 10){
                newWidth = 10;
            }
            let cssObj ={};
            cssObj[borderAttr] = newWidth +"px";
            $B.animate(panelEl, { width: newWidth + "px" }, { duration: 200 });
            $B.animate(centerEL, cssObj, { duration: 200,complete:()=>{
                try{
                    $B.DomUtils.trigger(window,"resize")
                }catch(e){}                      
            } 
            });                           
        },
        onProxyEnd:(e)=>{
            return false;
        }
    });
}
$B["Layout"] = Layout;