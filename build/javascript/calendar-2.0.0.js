/*! BUI - v2.0.0 - 2022-03-13 09:03:17 
Copyright (c): kevin.huang  https://gitee.com/kevin-huang/bui2.0.git (qq:757723114) 
Released under MIT License*/
(function ( global, factory) {
//   "use strict";
	if ( typeof module === "object" && typeof module.exports === "object" ) {
		module.exports = global.document ?
			factory( global, true ) :
			function( w ) {
				if ( !w.document ) {
					throw new Error( "Bui requires a window with a document" );
				}
				return factory( w );
			};
	} else {
		  factory( global );
	}  
})( typeof window !== "undefined" ? window : this, function( window, noGlobal ) {
//   "use strict";
  var $B = window["$B"] ?  window["$B"] : {};

var defaultOpts = {
    show: false,
    fixed: false,//是否固定显示
    fitWidth: false, //是否将input宽度和 控件宽度调整一致
    initValue: undefined, //可以是date对象，可以是字符串
    shadow: true,//是否需要阴影
    fmt: 'yyyy-MM-dd hh:mm:ss',
    tipTime: 2.5,
    clickHide: false,
    readonly: true,
    isSingel: false, //是否是单列模式   
    range: { min: undefined, max: undefined },  //时间日期选择范围定义,min/max = now / inputId ,'yyyyy-MM-dd hh:mm:ss' 
    onChanging: undefined, //onChanging = function(date){}
    onChange: undefined
};
var config = $B.config ? $B.config.calendar : {
    year: '年',
    month: '月',
    clear: '清空',
    now: '今天',
    close: '关闭',
    monthArray: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
    weekArray: ['日', '一', '二', '三', '四', '五', '六'],
    error: '输入是时间格式错误！',
    minTip: '输入时间不能小于[ x ]',
    maxTip: '输入时间不能大于[ x ]',
    error: '输入值不符合格式要求！'
};
var bodyEl;
class Calendar extends $B.BaseControl {
    constructor(elObj, opts) {
        if (!bodyEl) {
            bodyEl = $B.getBody();
        }
        super();
        this.config = config;
        this.opts = $B.extendObjectFn(true, {}, defaultOpts, opts);
        super.setElObj(elObj);
        $B.DomUtils.addClass(this.elObj, "k_calendar_input k_box_size");
        $B.DomUtils.attribute(this.elObj, { "autocomplete": "off" });
        if (this.opts.isSingel) { //全局单例模式            
            this.elObj._calopt = { range: this.opts.range, initValue: opts.initValue, onChanging: this.opts.onChanging,onChange:this.opts.onChange };
            this.opts.fixed = false;
            this.opts.show = false;
            var ins = window["_k_calendar_ins_"];
            if (ins) {
                ins.setTarget(this.elObj, true);
                return ins;
            } else {
                window["_k_calendar_ins_"] = this;
            }
        }
        this.currentDate = new Date();//默认初始化当前时间   
        this.isMonthStyle = this.opts.fmt === "yyyy-MM";
        this.isDayStyle = this.opts.fmt === "yyyy-MM-dd";
        this.isSecondStyle = this.opts.fmt === "yyyy-MM-dd hh:mm:ss";
        this.isTimeStyle = this.opts.fmt === "hh:mm:ss" || this.opts.fmt === "hh:mm";
        if (this.opts.fitWidth) {
            $B.DomUtils.width(this.elObj, 202);
        }
        var value = this.elObj.value;
        if (value !== "") {
            var currentDate = this._txt2Date(value);
            if (currentDate) {
                this.opts.initValue = currentDate;
            }
        }
        let _droplistid = $B.getUUID();
        $B.DomUtils.attribute(this.elObj, { "_droplistid": _droplistid });
        var i, len, _this = this;
        this._initValue(true);
        this._bindInputEvents();
        this.ddlObj = $B.DomUtils.createEl("<div id='" + _droplistid + "' _dlistgid='" + this.id + "' class='k_calendar_wrap  k_box_size clearfix' style='width:202px;z-index:" + $B.config.maxZindex + ";'></div>");
        if (this.opts.fixed) {
            $B.DomUtils.attribute(this.ddlObj, { "z-index": 1 });
            this.opts.show = true;
        }
        if (!this.opts.show) {
            $B.DomUtils.css(this.ddlObj, { "display": "none" });
        }
        if (this.isTimeStyle) {
            $B.DomUtils.css(this.ddlObj, { height: 250 });
            this.createTimeOptList();
        } else {
            /**顶部工具栏**/
            this.headerPanel = $B.DomUtils.createEl("<div class='k_calendar_pannel k_box_size clearfix' style='padding-left:2px;padding-top:2px;'><span class='_prev_year _event' style='width:24px'><i class='fa fa-angle-double-left'></i></span><span  style='width:21px' class='_prev_month _event'><i class='fa fa-angle-left'></i></span><span class='_cur_year  _event' style='width:46px;'>" + this.year + "</span><span>" + config.year + "</span><span class='_cur_month _event' style='width:28px;' >" + this.month + "</span><span>" + config.month + "</span><span style='width:25px' class='_next_month _event'><i class='fa fa-angle-right'></i></span><span class='_next_year _event' style='width:24px'><i class='fa fa-angle-double-right'></i></span></div>");
            $B.DomUtils.append(this.ddlObj, this.headerPanel);
            this.bindHeaderPanelEvents();
            /**周**/
            this.weekPanel = $B.DomUtils.createEl("<div class='k_calendar_week k_box_size' style='padding-left:2px;margin-bottom:5px;'></div>");
            for (i = 0, len = config.weekArray.length; i < len; ++i) {
                $B.DomUtils.append(this.weekPanel, "<span class='k_calendar_week_day'>" + config.weekArray[i] + "</span>");
            }
            $B.DomUtils.append(this.ddlObj, this.weekPanel);
            /**天列表**/
            this.daysPanel = $B.DomUtils.createEl("<div class='k_calendar_days k_box_size clearfix' style='padding-left:2px;'></div>");
            $B.DomUtils.append(this.ddlObj, this.daysPanel);
            this.createDays();
            /**时间***/
            this.timePanel = $B.DomUtils.createEl("<div class='k_calendar_time k_box_size' style='margin:1px 0px;padding-left:2px;text-align:center;'></div>");
            $B.DomUtils.append(this.ddlObj, this.timePanel);
            this.createTimeTools();
            /***工具栏***/
            this.toolPanel = $B.DomUtils.createEl("<div class='k_calendar_bools k_box_size clearfix' style='padding-left:1px;'></div>");
            this.createTools();
            $B.DomUtils.append(this.ddlObj, this.toolPanel);
            if (this.isDayStyle) {
                $B.DomUtils.hide(this.timePanel);
            }
            if (this.isMonthStyle) {//月模式
                $B.DomUtils.hide(this.daysPanel);
                $B.DomUtils.hide(this.timePanel);
                let $cur = $B.DomUtils.children(this.headerPanel, "._cur_month")[0];
                this.headToolClickEvent({ target: $cur });
                $B.DomUtils.css(this.toolPanel, { "margin-top": "168px" });
            }
        }
        this.setPosition();
        $B.DomUtils.append(bodyEl, this.ddlObj);
        if (!this.opts.fixed) {
            $B.DomUtils.addClass(this.ddlObj, "k_dropdown_list_wrap");
            $B.createGlobalBodyHideEv();
        } 
    }
    createTimeOptList() {
        var el1, el2, el3;
        if (this.opts.fmt === "hh:mm") {/**时分模式   makeNiceScroll***/
            el1 = $B.DomUtils.createEl("<div class='k_calendar_hours k_box_size' style='width:50%;height:100%;float:left;text-align:center;overflow:hidden;'></div>");
            $B.DomUtils.append(this.ddlObj, el1);
            el2 = $B.DomUtils.createEl("<div  class='k_calendar_minutes k_box_size' style='width:50%;height:100%;float:left;text-align:center;overflow:hidden;'></div>");
            $B.DomUtils.append(this.ddlObj, el2);
        } else {/**时分秒模式***/
            el1 = $B.DomUtils.createEl("<div class='k_calendar_hours k_box_size' style='width:33.3333%;height:100%;float:left;text-align:center;overflow:hidden;'></div>");
            $B.DomUtils.append(this.ddlObj, el1);
            el2 = $B.DomUtils.createEl("<div class='k_calendar_minutes k_box_size' style='width:33.3333%;height:100%;float:left;text-align:center;overflow:hidden;'></div>");
            $B.DomUtils.append(this.ddlObj, el2);
            el3 = $B.DomUtils.createEl("<div class='k_calendar_seconds k_box_size' style='width:33.3333%;height:100%;float:left;text-align:center;overflow:hidden;'></div>");
            $B.DomUtils.append(this.ddlObj, el3);
        }
        var _this = this;
        if (!this.opts.fixed) {
            var $btn = $B.DomUtils.createEl("<a style='position:absolute;top:1px ;right:1px;cursor:pointer;line-height:14px;'><i class='fa fa-cancel-circled'></i></a>");
            $B.DomUtils.append(this.ddlObj, $btn);
            $B.DomUtils.click($btn, function () {
                _this.hide();
                return false;
            });
        }
        var wrapHour = el1,
            wrapMinu = el2,
            wrapSec = el3;
        var envents = {
            click: function (e) {
                var $t = e.target;
                if ($B.DomUtils.hasClass($t, "k_calendar_time_opts")) {
                    $B.DomUtils.addClass($t, "actived");
                    let siblings = $B.DomUtils.siblings($t);
                    $B.DomUtils.removeClass(siblings, "actived");
                    var val = parseInt($t.innerText);
                    if ($B.DomUtils.hasClass($t, "_k_hours")) {
                        _this.hour = val;
                    } else if ($B.DomUtils.hasClass($t, "_k_minutes")) {
                        _this.minutes = val;
                    } else {
                        _this.seconds = val;
                    }
                    var newDate = _this._var2Date();
                    _this._setCurrentDate(newDate);
                    _this.update2target(_this.currentDate);
                }
                return false;
            },
            mouseenter: function (e) {
                let css = { "padding-right": "7px" };
                let childs = $B.DomUtils.children(e.target);
                for (let i = 0; i < childs.length; i++) {
                    $B.DomUtils.css(childs[i], css);
                }
                $B.DomUtils.css(e.target, { "overflow": "auto" });
            },
            mouseleave: function (e) {
                let css = { "padding-right": "15px" };
                let childs = $B.DomUtils.children(e.target);
                for (let i = 0; i < childs.length; i++) {
                    $B.DomUtils.css(childs[i], css);
                }
                $B.DomUtils.css(e.target, { "overflow": "hidden" });
            }
        };
        /***** this.hour  this.minutes this.seconds ******/
        var i = 0, txt, $el;
        while (i < 24) {
            txt = i;
            if (i < 10) {
                txt = "0" + i;
            }
            $el = $B.DomUtils.createEl("<div class='k_calendar_time_opts _k_hours k_box_size' style='padding-right:15px'>" + txt + "</div>");
            $B.DomUtils.append(wrapHour, $el);
            if (i === _this.hour) {
                $B.DomUtils.addClass($el, "actived");
            }
            i++;
        }
        $B.DomUtils.bind(wrapHour, envents);
        this._createMinuAndSecItems(wrapMinu, '_k_minutes', envents);
        if (wrapSec) {
            this._createMinuAndSecItems(wrapSec, '_k_seconds', envents);
        }
    }
    _createMinuAndSecItems(wrap, cls, envents) {
        var i = 0, txt, $el;
        while (i < 60) {
            txt = i;
            if (i < 10) {
                txt = "0" + i;
            }
            $el = $B.DomUtils.createEl("<div class='k_calendar_time_opts " + cls + " k_box_size' style='padding-right:15px'>" + txt + "</div>");
            $B.DomUtils.append(wrap, $el);
            if (cls === "_k_minutes") {
                if (this.minutes === i) {
                    $B.DomUtils.addClass($el, "actived");
                }
            } else if (this.seconds === i) {
                $B.DomUtils.addClass($el, "actived");
            }
            i++;
        }
        $B.DomUtils.bind(wrap, envents);
    }
    _time2fullDateTxt(value) {
        if (this.isTimeStyle) {
            if (/^(20|21|22|23|[0-1]\d)?:?([0-5]\d)?:?([0-5]\d)?$/.test(value)) {
                return this.currentDate.format("yyyy-MM-dd") + " " + value;
            }
        }
        return value;
    }
    /**
     * 根据输入框值 初始化currentDate  year、month变量 
     * isInit 是否是new 初始化调用
     * ***/
    _initValue(isInit) {
        var value = $B.trimFn(this.elObj.value);
        if (value !== "") {
            this.currentDate = this._txt2Date(value);
        }
        if (isInit) {
            var initVal = this.opts.initValue;
            if (this.opts.isSingel) {
                initVal = this.elObj._calopt.initValue;
            }
            //取 initValue
            if (initVal) {
                if (typeof initVal === "string") {
                    this.currentDate = this._txt2Date(initVal);
                } else {
                    this.currentDate = initVal;
                }
                this.elObj.value = this.currentDate.format(this.opts.fmt);
            }
        }
        this._setVarByDate(this.currentDate);
    }
    activedTimeUi() {
        var _this = this;
        this._activedUIopt(".k_calendar_hours", _this.hour);
        this._activedUIopt(".k_calendar_minutes", _this.minutes);
        this._activedUIopt(".k_calendar_seconds", _this.seconds);
    }
    _activedUIopt(cls, val) {
        let $el = $B.DomUtils.children(this.ddlObj, cls)[0];
        let h = $B.DomUtils.height($el.parentNode);
        h = h - h / 6;
        let optIts = $B.DomUtils.findByClass($el, ".k_calendar_time_opts");
        let top = 0;
        let add = true;
        let actItEL;
        for (let i = 0; i < optIts.length; i++) {
            var $t = optIts[i];
            if(add){
                top = top + $B.DomUtils.height($t);
            }
            if (parseInt($t.innerText) === val) {
                add = false;
                $B.DomUtils.addClass($t, "actived");
                actItEL = $t;
            } else {
                $B.DomUtils.removeClass($t, "actived");
            }
        }
        let diff = top - h;        
        if(diff > 0){
            let nextEL =actItEL.nextSibling;
            let help = 5;
            while(help > 0 && nextEL){
                diff = diff +  $B.DomUtils.height(nextEL);
                nextEL = nextEL.nextSibling;
                help--;
            }
            $B.DomUtils.scrollTop($el,diff);
        }
    }
    _setBorderColor() {
        var borderColor = $B.DomUtils.css(this.elObj, "border-color");
        $B.DomUtils.css(this.ddlObj, { "border-color": borderColor });
    }
    /**
  * 绑定顶部年/月改变事件
  * ***/
    bindHeaderPanelEvents() {
        var _this = this;
        var clickFn = function (e) {
            if (_this.hourItsPanel) {
                $B.DomUtils.hide(_this.hourItsPanel);
            }
            if (_this.mmItsPanel) {
                $B.DomUtils.hide(_this.mmItsPanel);
            }
            var $t = e.target;
            if ($t.tagName === "I") {
                $t = $t.parentNode;
            }
            var newDate = _this._var2Date();//获取当前 this.currentDate的副本，用于加减运算
            var curDay = newDate.getDate();
            var update = true;
            if ($B.DomUtils.hasClass($t, "_prev_year")) {
                newDate.setFullYear(newDate.getFullYear() - 1);
            } else if ($B.DomUtils.hasClass($t, "_prev_month")) {
                //兼容有溢出的场景 先取上一个月最大日期
                var lastMonthDate = new Date(newDate.getFullYear(), newDate.getMonth(), 0, _this.hour, _this.minutes, _this.seconds);
                if (curDay < lastMonthDate.getDate()) {
                    lastMonthDate.setDate(curDay);
                }
                newDate = lastMonthDate;
            } else if ($B.DomUtils.hasClass($t, "_cur_year")) {
                _this._hideMonthPanel();
                update = false; //
                if (_this.yearPanel && !$B.DomUtils.isHide(_this.yearPanel)) {
                    $B.hide(_this.yearPanel, 100);
                } else {
                    _this._createYearOpts(newDate.getFullYear());
                }
            } else if ($B.DomUtils.hasClass($t, "_cur_month")) {
                if (_this.yearPanel) {
                    $B.DomUtils.hide(_this.yearPanel);
                }
                update = false;
                if (!_this.monthPanel) {
                    _this._createMonthOpts();
                } else {
                    if (_this.isMonthStyle) {//月模式不可以关闭
                        if (!_this.opts.fixed) {
                            _this.slideToggle();
                        }
                    } else if ($B.DomUtils.isHide(_this.monthPanel)) {
                        $B.show(_this.monthPanel, 100);
                    } else {
                        $B.hide(_this.monthPanel, 100);
                    }
                }
            } else if ($B.DomUtils.hasClass($t, "_next_month")) {
                //需要考虑月溢出场景
                var nextMonthDate = new Date(newDate.getFullYear(), newDate.getMonth() + 2, 0, _this.hour, _this.minutes, _this.seconds);
                if (curDay < nextMonthDate.getDate()) {
                    nextMonthDate.setDate(curDay);
                }
                newDate = nextMonthDate;
            } else if ($B.DomUtils.hasClass($t, "_next_year")) {
                newDate.setFullYear(newDate.getFullYear() + 1);
            }
            if (update) {
                if (_this.yearPanel) {
                    $B.DomUtils.hide(_this.yearPanel);
                }
                _this._hideMonthPanel();
                if (_this._beforeChange(newDate)) { //先调用日期时间变更
                    _this._updateVar(newDate);
                    _this.update2target(newDate);
                    _this.updateYearAndMonthUi(newDate);
                    _this.rebuildDaysUi(newDate);
                    if (_this.isMonthStyle) {//月模式
                        var curMonth = newDate.getMonth() + 1;
                        _this._activeMonthUi(curMonth);
                    }
                }
            }
            return false;
        };
        $B.DomUtils.click(this.headerPanel, clickFn);
        this.headToolClickEvent = clickFn;
    }
    /**
     * 年选择列表
     * ***/
    _createYearOpts(year) {//创建年选项
        /**年点击事件***/
        var _this = this;
        if (!this._yearOnclick) {
            this._yearOnclick = function (e) {
                var $t, $i;
                var $t = e.target;
                var $i = $B.DomUtils.children($t);
                if ($t.tagName === "I") {
                    $i = [$t];
                }
                if ($i.length > 0) {
                    $i = $i[0];
                    $t = $i.parentNode;
                    var startYear;
                    let siblings = $B.DomUtils.siblings($t, "._year_num");
                    if ($B.DomUtils.hasClass($i, "fa-angle-double-left")) {//
                        startYear = parseInt(siblings[0].innerText) - 18;
                    } else {
                        startYear = parseInt(siblings[siblings.length - 1].innerText) + 1;
                    }
                    var startTg = siblings[0];
                    while (startTg && $B.DomUtils.hasClass(startTg, "_year_num")) {
                        startTg.innerText = startYear;
                        if (startYear === _this.year) {
                            $B.DomUtils.addClass(startTg, "actived");
                        } else {
                            $B.DomUtils.removeClass(startTg, "actived");
                        }
                        startYear++;
                        startTg = startTg.nextSibling;
                    }
                } else {
                    var year = parseInt($t.innerText);
                    _this.year = year;
                    var newDate = _this._var2Date();
                    if (_this._beforeChange(newDate)) {
                        _this._setCurrentDate(newDate);
                        _this.rebuildDaysUi();
                        _this.updateYearAndMonthUi(_this.currentDate);
                        _this.update2target(_this.currentDate);
                        $B.hide(_this.yearPanel, 200);
                    }
                }
                return false;
            };
        }
        if (this.yearPanel) {
            let childs = $B.DomUtils.children(this.yearPanel, ".k_box_size");
            $B.DomUtils.remove(childs);
            $B.DomUtils.show(this.yearPanel);
        } else {
            var _this = this;
            this.yearPanel = $B.DomUtils.createEl("<div class='k_box_size clearfix k_calendar_ym_panel k_dropdown_shadow' style='padding-left:12px;position:absolute;top:30px;left:0;z-index:2110000000;width:100%;background:#fff;'></div>");
            $B.DomUtils.append(this.ddlObj, this.yearPanel);
            var $closed = $B.DomUtils.createEl("<div style='position:absolute;top:-2px;right:5px;width:12px;height:12px;line-height:12px;text-align:center;cursor:pointer;'><i class='fa fa-cancel-2' style='font-size:14px;color:#AAAAB4'></i></div>");
            $B.DomUtils.append(this.yearPanel, $closed);
            $B.DomUtils.click($closed, function () {
                $B.DomUtils.hide(_this.yearPanel);
                return false;
            });
            $B.DomUtils.click(this.yearPanel, this._yearOnclick);
        }
        var startYear = year - 12;
        var help = 22, txt, $it;
        while (help > 0) {
            if (help === 22) {
                txt = "<i style='font-size:16px;color:#A0BFDE' class='fa fa-angle-double-left'></i>";
                $it = $B.DomUtils.createEl("<div class='k_box_size' style='float:left;line-height:20px;padding:5px 5px;cursor:pointer;width:43px;text-align:center;'>" + txt + "</div>");
                $B.DomUtils.append(this.yearPanel, $it);
            }
            txt = startYear;
            $it = $B.DomUtils.createEl("<div class='k_box_size _year_num' style='float:left;line-height:20px;padding:5px 5px;cursor:pointer;width:43px;text-align:center;'>" + txt + "</div>");
            $B.DomUtils.append(this.yearPanel, $it);
            if (txt === year) {
                $B.DomUtils.addClass($it, "actived");
            }
            if (help === 1) {
                txt = "<i style='font-size:16px;color:#A0BFDE' class='fa fa-angle-double-right'></i>";
                $it = $B.DomUtils.createEl("<div class='k_box_size' style='float:left;line-height:20px;padding:5px 5px;cursor:pointer;width:43px;text-align:center;'>" + txt + "</div>");
                $B.DomUtils.append(this.yearPanel, $it);
            }
            help--;
            startYear++;
        }
    }
    /**
     * 月模式下用
     * **/
    _activeMonthUi(month) {
        let mouths = $B.DomUtils.children(this.monthPanel);
        for (let i = 0; i < mouths.length; i++) {
            var $m = mouths[i];
            if (month === parseInt($m.innerText)) {
                $B.DomUtils.addClass($m, "actived");
            } else {
                $B.DomUtils.removeClass($m, "actived");
            }
        }
    }
    _hideMonthPanel() {
        if (!this.isMonthStyle && this.monthPanel) {
            $B.hide(this.monthPanel, 150);
        }
    }
    /***
     * 月选择列表
     * **/
    _createMonthOpts() {
        var _this = this;
        /**月点击事件**/
        if (!this._monthOnclick) {
            this._monthOnclick = function (e) {
                var $t = e.target;
                var month = parseInt($t.innerText);
                _this.month = month;
                var newDate = _this._var2Date();
                if (_this._beforeChange(newDate)) {
                    if (_this.isMonthStyle) {//月模式
                        $B.DomUtils.addClass($t, "actived");
                        let siblings = $B.DomUtils.siblings($t);
                        $B.DomUtils.removeClass(siblings, "actived");
                        if (_this.opts.clickHide) {
                            _this.hide();
                        }
                    }
                    _this._setCurrentDate(newDate);
                    _this.rebuildDaysUi();
                    _this.updateYearAndMonthUi(_this.currentDate);
                    _this.update2target(_this.currentDate);
                    _this._hideMonthPanel();
                }
                return false;
            };
        }
        if (!this.monthPanel) {
            this.monthPanel = $B.DomUtils.createEl("<div class='k_box_size clearfix k_calendar_ym_panel' style='padding-bottom:4px; padding-top:8px;padding-left: 18px; position: absolute; top: 30px; left: 0px; z-index: 2100000000; width: 100%; background: rgb(255, 255, 255);'></div>");
            $B.DomUtils.append(this.ddlObj, this.monthPanel);
            $B.DomUtils.click(this.monthPanel, this._monthOnclick);
            var i = 1, txt, $it;
            while (i < 13) {
                txt = i;
                if (i < 10) {
                    txt = "0" + i;
                }
                $it = $B.DomUtils.createEl("<div style='float:left;line-height:32px;padding:5px 5px;cursor:pointer;width:45px;text-align:center;'>" + txt + "</div>");
                $B.DomUtils.append(this.monthPanel, $it);
                if (i === this.month) {
                    $B.DomUtils.addClass($it, "actived");
                }
                i++;
            }
            if (!this.isMonthStyle) {
                $B.DomUtils.addClass(this.monthPanel, "k_dropdown_shadow");
                var $closed = $B.DomUtils.createEl("<div style='position:absolute;top:-2px;right:5px;width:12px;height:12px;line-height:12px;text-align:center;cursor:pointer;'><i class='fa fa-cancel-2' style='font-size:14px;color:#AAAAB4'></i></div>");
                $B.DomUtils.append(this.monthPanel, $closed);
                $B.DomUtils.click($closed, function () {
                    $B.DomUtils.hide(_this.monthPanel);
                    return false;
                });
            }
        } else {
            $B.DomUtils.show(_this.monthPanel);
            let childs = $B.DomUtils.children(this.monthPanel);
            for (let i = 0; i < childs.length; i++) {
                var $s = childs[i];
                if (parseInt($s.innerText) === _this.month) {
                    $B.DomUtils.addClass($s, "actived");
                } else {
                    $B.DomUtils.removeClass($s, "actived");
                }
            }
        }
    }
    /**底部工具栏按钮***/
    createTools() {
        var _this = this;
        var clickFn = function (e) {
            var $t = e.target;
            if (_this.yearPanel) {
                $B.DomUtils.hide(_this.yearPanel);
            }
            if (!_this.isMonthStyle && _this.monthPanel) {
                $B.DomUtils.hide(_this.monthPanel);
            }
            if ($B.DomUtils.hasClass($t, "002")) {
                var now = new Date();
                _this.year = now.getFullYear();
                _this.month = now.getMonth() + 1;
                _this.day = now.getDate();
                _this.hour = now.getHours();
                _this.minutes = now.getMinutes();
                _this.seconds = now.getSeconds();
                var newDate = _this._var2Date();
                if (_this._beforeChange(newDate)) {
                    _this._setCurrentDate(newDate);
                    _this.rebuildDaysUi();
                    _this.updateYearAndMonthUi(_this.currentDate);
                    _this.update2target(_this.currentDate);
                    _this._updateTimeUi();
                    if (_this.isMonthStyle) {
                        _this._activeMonthUi(_this.month);
                    }
                    if (_this.opts.clickHide) {
                        _this.hide();
                    }
                }
            } else if ($B.DomUtils.hasClass($t, "003")) {
                _this.elObj.value = "";
                let childs = $B.DomUtils.children(_this.daysPanel);
                $B.DomUtils.removeClass(childs, "activted");
            } else {
                _this.hide();
            }
            return false;
        };
        var b1 = $B.DomUtils.createEl("<div class='002'>" + this.config.now + "</div>");
        $B.DomUtils.append(this.toolPanel, b1);
        $B.DomUtils.click(b1, clickFn);
        var b2 = $B.DomUtils.createEl("<div class='003'>" + this.config.clear + "</div>");
        $B.DomUtils.append(this.toolPanel, b2);
        if(this.opts.readonly){
            $B.DomUtils.addClass(b2,"btn_disabled");
        }else{
            $B.DomUtils.click(b2, clickFn);
        }        
        if (this.opts.fixed) {
            $B.DomUtils.width(b1, 99);
            $B.DomUtils.width(b2, 99);
        } else {
            let $i = $B.DomUtils.createEl("<div class='004'>" + this.config.close + "</div>");
            $B.DomUtils.append(this.toolPanel, $i);
            $B.DomUtils.click($i, clickFn);
        }
    }
    _getStrTime() {
        var h = this.hour;
        var m = this.minutes;
        var s = this.seconds;
        if (h < 10) { h = '0' + h; }
        if (m < 10) { m = '0' + m; }
        if (s < 10) { s = '0' + s; }
        return { h: h, m: m, s: s };
    }
    /**时分秒工具栏**/
    createTimeTools() {
        var _this = this;
        var onHclick = function (e) {
            var $s = e.target;
            if ($s.tagName === "SPAN") {
                var v = $s.innerHTML;
                let id = $B.DomUtils.attribute(_this.userInput, "id");
                if (id === "_k_cal_hour") {
                    _this.hour = parseInt(v);
                } else if (id === "_k_cal_minu") {
                    _this.minutes = parseInt(v);
                } else {
                    _this.seconds = parseInt(v);
                }
                var newDate = _this._var2Date();
                if (_this._beforeChange(newDate)) {
                    _this.userInput.value = v;
                    $B.DomUtils.removeClass(_this.userInput, "actived");
                    _this._setCurrentDate(newDate);
                    _this.update2target(newDate);
                    $B.DomUtils.hide($s.parentNode);
                }
            }
            return false;
        };
        var onClickFn = function (e) {
            var $t = e.target;
            if (_this.yearPanel) {
                $B.DomUtils.hide(_this.yearPanel);
            }
            if (_this.monthPanel && !_this.isMonthStyle) {
                $B.DomUtils.hide(_this.monthPanel);
            }
            var i, len, $closed, isShow = true;
            var curVal = $t.value, spanArray;
            if ($B.DomUtils.attribute($t, "id") === "_k_cal_hour") {
                if (_this.mmItsPanel) {
                    $B.DomUtils.hide(_this.mmItsPanel);
                }
                if (!_this.hourItsPanel) {
                    _this.hourItsPanel = $B.DomUtils.createEl("<div class='k_box_size clearfix k_calendar_hours_panel k_dropdown_shadow' style='padding-bottom:2px;padding-left: 12px; position: absolute; top: 30px; left: 0px; z-index: 2112000000; width: 100%; background: rgb(255, 255, 255);'></div>");
                    for (i = 0; i < 24; i++) {
                        $B.DomUtils.append(_this.hourItsPanel, "<span>" + (i < 10 ? '0' + i : i) + "</span>");
                    }
                    $B.DomUtils.append(_this.ddlObj, _this.hourItsPanel);
                    $B.DomUtils.click(_this.hourItsPanel, onHclick);
                    $closed = $B.DomUtils.createEl("<div style='position:absolute;top:-2px;right:5px;width:12px;height:12px;line-height:12px;text-align:center;cursor:pointer;'><i class='fa fa-cancel-2' style='font-size:14px;color:#AAAAB4'></i></div>");
                    $B.DomUtils.append(_this.hourItsPanel, $closed);
                    $B.DomUtils.click($closed, function (e) {
                        $B.DomUtils.hide(_this.hourItsPanel);
                        $B.DomUtils.removeClass(_this.userInput, "actived");
                        return false;
                    });
                } else {
                    if ($B.DomUtils.isHide(_this.hourItsPanel)) {
                        $B.show(_this.hourItsPanel, 200);
                    } else {
                        $B.hide(_this.hourItsPanel, 200);
                        isShow = false;
                    }
                }
                if (isShow) {
                    spanArray = $B.DomUtils.children(_this.hourItsPanel, "span");
                }
            } else {
                if (_this.hourItsPanel) {
                    $B.hide(_this.hourItsPanel, 200);
                }
                if (!_this.mmItsPanel) {
                    _this.mmItsPanel = $B.DomUtils.createEl("<div class='k_box_size clearfix k_calendar_mm_panel k_dropdown_shadow' style='padding-top:0px;padding-left: 12px; position: absolute; top: 29px; left: 0px; z-index: 2112000000; width: 100%; background: rgb(255, 255, 255);'></div>");
                    for (i = 0; i < 60; i++) {
                        $B.DomUtils.append(_this.mmItsPanel, "<span>" + (i < 10 ? '0' + i : i) + "</span>");
                    }
                    $B.DomUtils.append(_this.ddlObj, _this.mmItsPanel);
                    $B.DomUtils.click(_this.mmItsPanel, onHclick);
                    $closed = $B.DomUtils.createEl("<div style='position:absolute;top:-2px;right:5px;width:12px;height:12px;line-height:12px;text-align:center;cursor:pointer;'><i class='fa fa-cancel-2' style='font-size:14px;color:#AAAAB4'></i></div>");
                    $B.DomUtils.append(_this.mmItsPanel, $closed);
                    $B.DomUtils.click($closed, function () {
                        $B.hide(_this.mmItsPanel, 200);
                        $B.DomUtils.removeClass(_this.userInput, "actived");
                        return false;
                    });
                } else {
                    if ($B.DomUtils.isHide(_this.mmItsPanel)) {
                        $B.show(_this.mmItsPanel, 200);
                    } else {
                        if (_this.userInput && _this.userInput === $t) {
                            $B.hide(_this.mmItsPanel, 200);
                            isShow = false;
                        }
                    }
                }
                if (isShow) {
                    spanArray = $B.DomUtils.children(_this.mmItsPanel, "span");
                }
            }
            _this.userInput = $t;
            if (isShow) {
                $B.DomUtils.addClass($t, "actived");
                let siblings = $B.DomUtils.siblings($t);
                $B.DomUtils.removeClass(siblings, "actived");
                for (let i = 0; i < spanArray.length; i++) {
                    var $s = spanArray[i];
                    if ($s.innerText === curVal) {
                        $B.DomUtils.addClass($s, "actived");
                    } else {
                        $B.DomUtils.removeClass($s, "actived");
                    }
                }
            } else {
                $B.DomUtils.removeClass($t, "actived");
            }
            return false;
        };
        var s = this._getStrTime();
        this.$hour = $B.DomUtils.createEl("<input readonly='readonly' style='width:30px;text-align:center;' id='_k_cal_hour' value='" + s.h + "'/>");
        $B.DomUtils.append(this.timePanel, this.$hour);
        $B.DomUtils.click(this.timePanel, onClickFn);
        $B.DomUtils.append(this.timePanel, "<span>：</span>");
        this.$minutes = $B.DomUtils.createEl("<input  readonly='readonly' style='width:30px;text-align:center' id='_k_cal_minu' value='" + s.m + "'/>");
        $B.DomUtils.append(this.timePanel, this.$minutes);
        $B.DomUtils.click(this.$minutes, onClickFn);
        if (this.isSecondStyle) {
            $B.DomUtils.append(this.timePanel, "<span>：</span>");
            this.$seconds = $B.DomUtils.createEl("<input  readonly='readonly' style='width:30px;text-align:center' id='_k_cal_secs' value='" + s.s + "'/>");
            $B.DomUtils.append(this.timePanel, this.$seconds);
            $B.DomUtils.click(this.$seconds, onClickFn);
        }
    }
    /***
     * this.year,this.month 转为 date对象返回
     * ***/
    _var2Date() {
        //存在bug，this.day,29,30,31可能是不存在的，需要修正
        try {
            var tmpDate = new Date(this.year, this.month, 0);
            var maxDay = tmpDate.getDate();
            if (this.day > maxDay) {
                this.day = maxDay;
            }
            var date = new Date(this.year, this.month - 1, this.day, this.hour, this.minutes, this.seconds);
            return date;
        } catch (ex) {
            $B.error(this.config.error);
            return undefined;
        }
        return date;
    }
    _updateTimeUi() {
        var s = this._getStrTime();
        if (this.$hour) {
            this.$hour.value = s.h;
        }
        if (this.$minutes) {
            this.$minutes.value = s.m;
        }
        if (this.$seconds) {
            this.$seconds.value = s.s;
        }
    }
    /**
     * 更新时分秒 下拉框
     * ***/
    _updateMinAndCec() {
        this._updateTimeUi();
    }
    /**
     * 根据newDate新时间重建列表,
     * ****/
    rebuildDaysUi(newDate) {
        if (newDate) {
            this.year = newDate.getFullYear();
            this.month = newDate.getMonth() + 1;
        }
        if (this.daysPanel) {
            var startDate = this._getStartDate();
            var day, month, tmpStr;
            var childs = $B.DomUtils.children(this.daysPanel);
            var one = childs[0];
            while (one) {
                day = startDate.getDate();
                month = startDate.getMonth() + 1;
                tmpStr = startDate.format("yyyy-MM-dd");
                if (month !== this.month) {
                    $B.DomUtils.addClass(one, "day_dull");
                    $B.DomUtils.removeClass(one, "activted");
                    $B.DomUtils.attribute(one, { "title": tmpStr });
                } else {
                    $B.DomUtils.removeClass(one, "day_dull");
                    $B.DomUtils.removeAttribute(one, "title");
                    if (day === this.day) {
                        $B.DomUtils.addClass(one, "activted");
                    } else {
                        $B.DomUtils.removeClass(one, "activted");
                    }
                }
                one.innerText = day;
                $B.DomUtils.attribute(one, { "t": tmpStr })
                startDate.setDate(startDate.getDate() + 1);
                one = one.nextSibling;
            }
        }
    }
    /**
     * 根据新时间更新年月ui
     * ***/
    updateYearAndMonthUi(newDate) {
        if (this.headerPanel) {
            var year = newDate.getFullYear();
            var monthIdx = newDate.getMonth();
            var month = this.config.monthArray[monthIdx];
            let $cur = $B.DomUtils.children(this.headerPanel, "._cur_year");
            if ($cur.length > 0) {
                $cur[0].innerText = year;
                $cur[0].nextSibling.nextSibling.innerText = month;
            }
        }
    }
    /**
     * 将date根据 config.fmt格式化后设置到target中
     * ***/
    update2target(date, notify) {
        var strValue = date.format(this.opts.fmt);
        let oldValue = this.elObj.value;
        this.elObj.value = strValue;
        var onChange = this.opts.onChange;
        if (this.opts.isSingel) {
            onChange = this.elObj._calopt.onChange;
        }    
        if(onChange){
            if(typeof notify === "undefined" || !notify){
                onChange(this.id,strValue,oldValue);
            }
        }
    }
    /***
     * 根据this.year,this.month 获取星期日开始时间
     * ***/
    _getStartDate() {
        var curMonthIdx = this.month - 1;
        var curMonthFisrtDate = new Date(this.year, curMonthIdx, 1);
        var weekDay = curMonthFisrtDate.getDay();
        var startDate = new Date(curMonthFisrtDate);
        while (weekDay > 0) {
            startDate.setDate(startDate.getDate() - 1);
            weekDay--;
        }
        return startDate;
    }
    /**
     * 创建天日期列表
     * ****/
    createDays() {
        var curMonthIdx = this.month - 1;
        var startDate = this._getStartDate();
        var count = 42, tmpStr, tmpSpan, tmpDay, tmpMonth;
        if (!this._dayOnclick) {
            var _this = this;
            this._dayOnclick = function (e) {
                var $t = e.target;
                var arr = $B.DomUtils.attribute($t, "t").split("-");
                _this.year = parseInt(arr[0]);
                _this.month = parseInt(arr[1]);
                _this.day = parseInt(arr[2]);
                var newDate = _this._var2Date();
                if (_this._beforeChange(newDate)) {
                    _this._setCurrentDate(newDate);
                    _this.updateYearAndMonthUi(_this.currentDate);
                    _this.update2target(_this.currentDate);
                    $B.DomUtils.addClass($t, "activted");
                    let siblings = $B.DomUtils.siblings($t);
                    $B.DomUtils.removeClass(siblings, "activted");
                    if (_this.opts.clickHide) {
                        _this.hide();
                    }
                }
                return false;
            };
        };
        while (count > 0) {
            tmpStr = startDate.format("yyyy-MM-dd");
            tmpDay = startDate.getDate();
            tmpMonth = startDate.getMonth();
            tmpSpan = $B.DomUtils.createEl("<span t='" + tmpStr + "'>" + tmpDay + "</span>");
            $B.DomUtils.append(this.daysPanel, tmpSpan);
            if (curMonthIdx !== tmpMonth) {
                $B.DomUtils.addClass(tmpSpan, "day_dull");
                $B.DomUtils.attribute(tmpSpan, { "title": tmpStr });
                $B.DomUtils
            } else {
                if (tmpDay === this.day) {
                    $B.DomUtils.addClass(tmpSpan, "activted");
                }
            }
            startDate.setDate(startDate.getDate() + 1);
            count--;
        }
        $B.DomUtils.click(this.daysPanel, this._dayOnclick);
    }
    _txt2Date(txtDate) {
        if (txtDate === "") {
            return undefined;
        }
        txtDate = this._time2fullDateTxt(txtDate);
        /*****解决 2019-11 格式配置错误的问题 ******/
        var arrTmp = [],
            year,
            month,
            day = 1,
            hour = 0,
            minutes = 0,
            seconds = 0;
        if (txtDate.indexOf('-') > 0) {
            arrTmp = txtDate.split('-');
        } else if (txtDate.indexOf('/') > 0) {
            arrTmp = txtDate.split('/');
        }
        if (arrTmp.length === 2) {
            year = arrTmp[0];
            month = parseInt(arrTmp[1]) - 1;
        } else {
            var m = txtDate.match(/^(\s*\d{4}\s*)(-|\/)\s*(0?[1-9]|1[0-2]\s*)(-|\/)?(\s*[012]?[0-9]|3[01]\s*)?\s*([01]?[0-9]|2[0-4]\s*)?:?(\s*[012345]?[0-9]|\s*)?:?(\s*[012345]?[0-9]\s*)?$/);
            if (m === null || !m[1] && !m[3]) {
                return undefined;
            }
            year = parseInt(m[1]),
                month = parseInt(m[3]) - 1;
            if (m[5]) {
                day = parseInt(m[5]);
            }
            if (m[6]) {
                hour = parseInt(m[6]);
            }
            if (m[7]) {
                minutes = parseInt(m[7]);
            }
            if (m[8]) {
                seconds = parseInt(m[8]);
            }
        }
        var date = new Date(year, month, day, hour, minutes, seconds);
        return date;
    }
    _time2fullDateTxt(value) {
        if (this.isTimeStyle) {
            if (/^(20|21|22|23|[0-1]\d)?:?([0-5]\d)?:?([0-5]\d)?$/.test(value)) {
                return this.currentDate.format("yyyy-MM-dd") + " " + value;
            }
        }
        return value;
    }
    //全局单列模式
    setTarget($input, isInit) {
        if (this.elObj && this.elObj === $input) {
            return;
        }
        this.elObj = $input;
        this._bindInputEvents();
        this._initValue(isInit);
        this.rebuildDaysUi();
        this.updateYearAndMonthUi(this.currentDate);
        this._updateMinAndCec();
    }
    _bindInputEvents() {
        var _this = this;
        var binding = $B.DomUtils.attribute(this.elObj, "k_calr_ins") === undefined;
        if (this.opts.readonly) {
            $B.DomUtils.attribute(this.elObj, { "readonly": true });
        } else if (binding) {//启用了用户输入                          
            var onInputEvFn = function () {
                var v = _this.elObj.value;
                var inputDate = _this._txt2Date(v);
                if (inputDate) {
                    if (_this._beforeChange(inputDate)) {
                        _this._setCurrentDate(inputDate);
                        _this._setVarByDate(inputDate);
                        _this.rebuildDaysUi();
                        _this.updateYearAndMonthUi(_this.currentDate);
                        if (_this.isMonthStyle) {//如果是月模式
                            _this._createMonthOpts();
                        } else if (_this.isTimeStyle) {
                            _this.activedTimeUi();
                        } else {
                            _this._updateMinAndCec();
                        }
                    }
                } else {
                    $B.error(_this.config.error, 2);
                    var srcVal = _this.currentDate.format(_this.opts.fmt);
                    _this.elObj.value = srcVal;
                }
            };
            this.hasInputed = false;
            $B.DomUtils.input(this.elObj, function () {
                _this.hasInputed = true;
                clearTimeout(_this.userInputTimer);
                _this.userInputTimer = setTimeout(function () {
                    _this.hasInputed = false;
                    onInputEvFn();
                }, 1000);
            });
        }
        if (binding) {
            $B.DomUtils.attribute(this.elObj, { "k_calr_ins": 1 });
            $B.DomUtils.bind(this.elObj, {
                "mousedown": function () {
                    $B._0001currentdroplistEl = _this.elObj;
                },
                "blur": function (e) {
                    _this._setBorderColor();
                    if (_this.hasInputed) {
                        _this.hasInputed = false;
                        clearTimeout(_this.userInputTimer);
                        onInputEvFn();
                    }
                },
                "click": function (e) {
                    if (_this.opts.isSingel) {
                        let isSwith = _this.elObj !== e.target;
                        if (isSwith) {
                            _this.setTarget(e.target, false);
                            _this.show();
                            _this._setBorderColor();
                            return false;
                        }
                    }
                    _this._setBorderColor();
                    if (!_this.opts.fixed) {
                        _this.slideToggle();
                    }
                    return false;
                }
            });
        }
    }
    /**
     * 将date参数设置到currentDate
     * ***/
    _setCurrentDate(date) {
        this.currentDate = date;
        /**适配验证***/
        if ($B.DomUtils.hasClass(this.elObj, "k_input_value_err")) {
            $B.DomUtils.removeClass(this.elObj, "k_input_value_err");
            let siblings = $B.DomUtils.siblings(this.elObj, ".k_validate_tip_wrap");
            $B.DomUtils.remove(siblings);
        }
    }
    _getMinMaxCfg(flag) {
        var _date, v;
        var range = this.opts.range;
        if (this.opts.isSingel) { //如果是单列模式
            range = this.elObj._calopt.range;
        }
        if (range[flag]) {
            if (range[flag].indexOf("#") > -1) { //ID
                let id = range[flag];
                let input = $B.DomUtils.findbyId(id);
                v = input.value;
                _date = this._txt2Date(v);
            } else if (range[flag] === "now") {
                _date = new Date();
            } else {
                v = range[flag];
                _date = this._txt2Date(v);
            }
        }
        return _date;
    }
    /**时间变更前回调
        * 如果不符合要求，调用this._setVarByDate(this.currentDate); 恢复变量
        * ***/
    _beforeChange(newDate) {
        var go = true;
        var minDate = this._getMinMaxCfg("min"),
            maxDate = this._getMinMaxCfg("max");
        var res = this._legalDate(newDate, minDate, maxDate);
        if (res !== "") {
            $B.toolTip(this.elObj, res, this.opts.tipTime);
            go = false;
        }
        var changeFn = this.opts.onChanging;
        if (this.opts.isSingel) {
            changeFn = this.elObj._calopt.onChanging;
        }
        if (go && typeof changeFn === "function") {
            res = changeFn(newDate, newDate.format(this.opts.fmt));
            if (typeof res !== "undefined" && res !== "") {
                $B.toolTip(this.elObj, res, this.opts.tipTime);
                go = false;
            }
        }
        if (!go) {//this.currentDate没有被修改, 恢复被修改的变量
            this._setVarByDate(this.currentDate);
        }
        return go;
    }
    /*** 
     * 将date对象提取year、month、day设置到变量this.year,this.month......
     * ***/
    _setVarByDate(date) {
        this.year = date.getFullYear();
        this.month = date.getMonth() + 1;
        this.day = date.getDate();
        this.hour = date.getHours();
        this.minutes = date.getMinutes();
        this.seconds = date.getSeconds();
    }
    /****
         * 最小值，最大值非法限制检查
         * ***/
    _legalDate(newDate, minDate, maxDate) {
        if (minDate) {
            if (newDate.getTime() < minDate.getTime()) {
                return this.config.minTip.replace("x", minDate.format(this.opts.fmt));
            }
        }
        if (maxDate) {
            if (newDate.getTime() > maxDate.getTime()) {
                return this.config.maxTip.replace("x", maxDate.format(this.opts.fmt));
            }
        }
        return "";
    }
    /***
    * 将newDate更新至 currentDate及year\month\day变量
    * ***/
    _updateVar(newDate) {
        this._setCurrentDate(newDate);
        this._setVarByDate(newDate);
    }
    /***
     * 设置时间 date = New Date / yyyy-MM-dd 
     * ***/
    setValue(date,notify) {       
        var newDate;
        if (typeof date === "string") {
            newDate = this._txt2Date(date);
        } else {
            newDate = date;
        }
        this._setCurrentDate(newDate);
        this._setVarByDate(newDate);
        this.rebuildDaysUi();
        this.updateYearAndMonthUi(this.currentDate);
        this._updateMinAndCec();
        this.update2target(this.currentDate,notify);
    }
    setPosition() {
        var ofs = $B.DomUtils.offset(this.elObj);
        var css, height;
        if (this._isTopPosition()) {
            height = this._getPanelHeight();
            css = { top: ofs.top - height, left: ofs.left };
        } else {
            height = $B.DomUtils.outerHeight(this.elObj) - 1;
            css = { top: ofs.top + height, left: ofs.left };
        }
        var bodyWidth = $B.DomUtils.width(bodyEl);
        var maxLeft = ofs.left + 202;
        var diff = maxLeft - bodyWidth;
        if (diff > 0) {
            css.left = css.left - diff;
        }
        $B.DomUtils.css(this.ddlObj, css);
    }
    slideToggle() {
        if ($B.DomUtils.css(this.ddlObj, "display") === "none") {
            this.show();
        } else {
            this.hide();
        }
    }
    _getPanelHeight() {
        var height = 276;
        if (this.isMonthStyle || this.isDayStyle || this.isTimeStyle) {
            height = 250;
        }
        return height;
    }
    _isTopPosition() {
        var bodyHeight = $B.DomUtils.height(bodyEl);
        var ofs = $B.DomUtils.offset(this.elObj);
        var h = $B.DomUtils.outerHeight(this.elObj);
        var h = ofs.top + h + this._getPanelHeight();
        if ((h - bodyHeight) > 0) {
            return true;
        }
        return false;
    }
    hide() {
        if (this.opts.fixed) {
            return;
        }
        if (this.hourItsPanel) {
            $B.DomUtils.hide(this.hourItsPanel);
        }
        if (this.mmItsPanel) {
            $B.DomUtils.hide(this.mmItsPanel);
        }
        if (this._isTopPosition()) {
            $B.DomUtils.hide(this.ddlObj);
        } else {
            $B.slideUp(this.ddlObj, 200);
        }
        if (this.yearPanel) {//恢复ui
            $B.DomUtils.hide(this.yearPanel);
        }
        this._hideMonthPanel();
    }
    show() {
        this.setPosition();
        if (this.opts.fixed) {
            return;
        }
        $B.DomUtils.detach(this.ddlObj);
        $B.DomUtils.append(bodyEl, this.ddlObj);
        if (this._isTopPosition()) {
            $B.DomUtils.show(this.ddlObj);
            this.timeScrollView();
        } else {
            $B.slideDown(this.ddlObj, 200,()=>{
                this.timeScrollView();
            });
        }
    }
    timeScrollView(){
        if(this.isTimeStyle){           
            this.activedTimeUi();
        }
    }
    destroy(isForce) {
        this.elObj._calopt = undefined;
        $B.DomUtils.remove(this.ddlObj);
        super.destroy(isForce);
    }
}
$B["Calendar"] = Calendar; 
 if(!window["$B"] ) {window["$B"] = $B;} 
  return window["$B"] ; 
});
