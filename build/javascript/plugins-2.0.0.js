/*! BUI - v2.0.0 - 2022-03-13 09:03:17 
Copyright (c): kevin.huang  https://gitee.com/kevin-huang/bui2.0.git (qq:757723114) 
Released under MIT License*/
(function ( global, factory) {
//   "use strict";
	if ( typeof module === "object" && typeof module.exports === "object" ) {
		module.exports = global.document ?
			factory( global, true ) :
			function( w ) {
				if ( !w.document ) {
					throw new Error( "Bui requires a window with a document" );
				}
				return factory( w );
			};
	} else {
		  factory( global );
	}  
})( typeof window !== "undefined" ? window : this, function( window, noGlobal ) {
//   "use strict";
  var $B = window["$B"] ?  window["$B"] : {};

var draggableDefaults = {
    nameSpace: 'draggable', //命名空间，一个对象可以绑定多种拖动实现
    which: undefined, //鼠标键码，是否左键,右键 才能触发拖动，默认左右键均可 1,3
    defaultZindex: 999999, //当z-index为auto时采用的层级数值
    holdTime: undefined, //按下鼠标多少毫秒才拖动，默认不设置立即可拖动
    globalDraging:true,//是否是全局拖动，避免多层触发拖动
    isProxy: false, //是否产生一个空代理进行拖动
    disabled: false, //是否禁用拖动
    handler: undefined, //触发拖动的对象，默认是对象本身
    cursor: 'move', //鼠标样式
    axis: undefined, // v垂直方 h水平，默认全向   
    setMatrixRate:undefined,//设置缩放比例        
    onDragReady: undefined, //鼠标按下时候准备拖动前，返回true则往下执行，返回false则停止
    onStartDrag: undefined, //开始拖动事件
    onDrag: undefined, //拖动中事件
    onStopDrag: undefined, //拖动结束事件
    onMouseUp: undefined, //当没有发生拖动，鼠标放开时候调用
    onProxyEnd: undefined, // function(args,pos) 代理拖动结束，返回false则不更新代理
    onCreateProxy: undefined // function(proxyObj,srcObj)，代理创建监听
};
/***1.5秒后及时清理state上的引用****/
function clearDragArgs(state) {
    setTimeout(function () {
        for (var p in state) {
            if (state.hasOwnProperty(p)) {
                delete state[p];
            }
        }
    }, 1500);
}
/***
 * 根据鼠标计算位置
 * ****/
function _compute(e) {
    var state = e.params,
        data = state._data,
        opts = state.options;
    //拖动后的 = 原先+拖动位移
    var leftOffset = e.pageX - data.startX,
        topOffset = e.pageY - data.startY;
    if (opts.axis === 'v') { //如果是垂直，则只改top
        leftOffset = 0;
    } else if (opts.axis === 'h') { //如果是水平拖动 则只改left
        topOffset = 0;
    }
    var left = data.startLeft + leftOffset,
        top = data.startTop + topOffset;
    data.leftOffset = leftOffset;
    data.topOffset = topOffset;
    data.oldLeft = data.left;
    data.oldTop = data.top;
    data.left = left;
    data.top = top;
    var apply = true;
    if (state["onDragFn"]) {
        var args = {
            shiftKey:e.shiftKey,
            ctrlKey:e.ctrlKey,
            altKey:e.altKey,
            state: state,
            which: opts.which
        };
        var res = opts.onDrag(args);
        if (typeof res !== 'undefined') {
            apply = res;
        }
    }    
    data["apply"] = apply;
}
//移动事件 hasCallStartFn
function _docMove(e) {
    var state = e.params;
    var opts = state.options;
    var data = state._data;
    data.pageX = e.pageX;
    data.pageY = e.pageY;
    /***** 是否已经调研了 onStartDrag *********/
    if (!state["hasCallStartFn"]) {
        //处理没有发生鼠标移动但是_docMove被触发的情况！！
        var leftOffset = e.pageX - data.startX,
            topOffset = e.pageY - data.startY;
        if (leftOffset === 0 && topOffset === 0) {
            console.log("过滤没有发生实际移动的 _docMove");
            return;
        }
        state["hasCallStartFn"] = true;       
        if (state.callStartDragFn) {
            //提取拖动目标的zIndex 2147483647
            //var zIndex = state.movingTarget.css("z-index");
            var zIndex = $B.DomUtils.css(state.movingTarget,"zIndex");
            if (zIndex === "auto") {
                zIndex = opts.defaultZindex;
            }
            state["zIndex"] = zIndex;
            //state.movingTarget.css("z-index", 2147483647);
            $B.DomUtils.css(state.movingTarget,{"z-index":2147483647});
            try {                
                opts.onStartDrag({
                    shiftKey:e.shiftKey,
                    ctrlKey:e.ctrlKey,
                    altKey:e.altKey,
                    state: state
                });
            } catch (ex) {
                if (console.error) {
                    console.error("onStartDrag error " + ex);
                } else {
                    console.log("onStartDrag error " + ex);
                }
            }
        }
    }
    _compute(e);
    if (data["apply"] && state.movingTarget) {
        if (data.matrixRate) { //存在缩放比例
            data.top = data.top / data.matrixRate;
            data.left = data.left / data.matrixRate;
        }
        var css = {
            cursor: state.options.cursor
        };
        if (opts.axis === 'v') {
            css.top = data.top + data.fixTop;
        } else if (opts.axis === 'h') {
            css.left = data.left + data.fixLeft;
        } else {
            css.top = data.top + data.fixTop;
            css.left = data.left + data.fixLeft;
        }
        //确保为整数
        css.top = parseInt(css.top);
        css.left = parseInt(css.left);
        $B.DomUtils.css(state.movingTarget,css);
    }
    if (typeof opts.notClearRange === "undefined") {
        //先清除document中的选择区域
        $B.clearDomSelected();
    }
}
/***
 * 应该domcument监听鼠标放开事件
 * ***/
function _docUp(e) {
    var state = e.params,
        data = state._data,
        opts = state.options,
        nameSpace = opts.nameSpace,
        target = state.target,
        body = $B.getBody();
    data.pageX = e.pageX;
    data.pageY = e.pageY;
    $B.DomUtils.offEvents(window.document,nameSpace+".*");
    $B.DomUtils.css(target,{"cursor":data.srcsor});
    window["_globalDraging"] = undefined;
    if (opts.isProxy) { //代理移动方式,则需要更新目标位置           
        var css = {
            left: data.srcPos.left + data.leftOffset,
            top: data.srcPos.top + data.topOffset
        };
        var go = true;
        if (opts.onProxyEnd) {
            var ret = opts.onProxyEnd(state, css);
            if (typeof ret !== "undefined") {
                go = ret;
            }
        }
        $B.DomUtils.remove(state["movingTarget"]);
        if (go) {
            css["position"] = "absolute";
            $B.DomUtils.css(target,css);
        }
    }
    var onStopDrag = opts.onStopDrag;
    var returnVar;
    if (typeof onStopDrag === "function" && state["hasCallStartFn"]) {
        returnVar = onStopDrag({
            shiftKey:e.shiftKey,
            ctrlKey:e.ctrlKey,
            altKey:e.altKey,
            state: state,
            e:e._e
        });
    }    
    if (state["hasCallStartFn"]) {
        var zIndex = state["zIndex"];
        $B.DomUtils.css(state.movingTarget,{"z-index":zIndex});
    } else {
        if (opts.onMouseUp) {
           let v = opts.onMouseUp({
                state: state,
                e:e._e
            });
            if(typeof v !== "undefined"){
                returnVar = v;
            }
        }
    }
    clearDragArgs();
    $B.DomUtils.css(body,{"cursor":"default"});
    if (typeof returnVar !== "undefined") {
        return returnVar;
    }
}
//监听document鼠标按下事件
function _docDown(e) {
    var state = e.params,
        opts = state.options,
        data = state._data;
    var movingTarget = state.target;
    var body = $B.getBody();
    if (opts.isProxy) {
        var ofs = data.srcOfs,
            w = data.width,
            h = data.height,
            fs = $B.DomUtils.css(state.target,"font-size"),
            fcolor = $B.DomUtils.css(state.target,"color");
        var pp = $B.DomUtils.css(state.target,"padding") ;
        var clz = "";
        if ($B.DomUtils.hasClass(state.target,"k_box_size")) {
            clz = "k_box_size";
        }
        movingTarget = $B.DomUtils.createEl("<div style='background-color:#ADADAD;opacity :0.6;font-size:" + fs + ";color:" + fcolor + ";padding:" + pp + ";cursor:" + opts.cursor + ";position:absolute;top:" + ofs.top + "px;left:" + ofs.left + "px' class='k_draggable_proxy " + clz + "'></div>");
         $B.DomUtils.width(movingTarget,w);
        $B.DomUtils.height(movingTarget,h);
        if (opts.onCreateProxy) {
            opts.onCreateProxy(movingTarget, state.target, e);
        }
        $B.DomUtils.append(body,movingTarget);
    }
    if (typeof opts.onDrag === "function") {
        state["onDragFn"] = true;
    }
    state["movingTarget"] = movingTarget;    
    $B.DomUtils.css(body,{"cursor":opts.cursor});
}
/***
 * 鼠标放开
 ****/
function onMouseUp(e) {
    window["_globalDraging"] = undefined;
    var dragtimer = $B.DomUtils.propData(this,"dragtimer");
    if (dragtimer) {
        clearTimeout(dragtimer);
        var state = e.params;
        $B.DomUtils.removePropData(this,"dragtimer");
        if (!state["hasCallStartFn"]) { //如果没有发生拖动
            var opts = state.options;
            if (opts.onMouseUp) {
                opts.onMouseUp({
                    state: state,
                    e:e._e
                });
            }
        }
        clearDragArgs(state);
    }
}
/**
 * args应对编程trigger触发的鼠标事件
 * e参数中不存在pageX、pageY的情况
 * **/
function onMouseDown(e, args) {   
    if (args && !e["pageX"]) {
        e["pageX"] = args.pageX;
        e["pageY"] = args.pageY;
        e["which"] = args.which;
    }
    if (e.ctrlKey) {       
        return true;
    }
    if(window["_globalDraging"] ){//已经存在全局的拖动
        console.log(" 已经存在全局的拖动");
        return true;
    }
    var state = e.params;
    var el = state.target;
    var options = state.options;
    if (!options.which || options.which === e.which) {
        if (!options.disabled) {
            var go = true;
            if (typeof options.onDragReady === 'function') {
               let ret  = options.onDragReady.call(el, state, e);
               if(typeof ret !== "undefined"){
                go = ret;
               }
            }
            if (go) {
                var posParentEl = el.parentNode;
                var posAttr;
                while (posParentEl ) {
                    posAttr =  $B.DomUtils.css(posParentEl,"position");
                    if (posAttr === "relative" || posAttr === "absolute") {
                        break;
                    }
                    if( posParentEl.nodeName === "BODY"){
                        break;
                    }
                    posParentEl = posParentEl.parentNode;
                }
                var matrix = $B.getMatrixArray(posParentEl); //父元素是否存在缩放
                var matrixRate;
                if (matrix) {
                    matrixRate = matrix[0];
                }
                if (typeof options.setMatrixRate === 'function') {
                    var rate = options.setMatrixRate();
                    if (typeof rate !== "undefined") {
                        matrixRate = rate;
                    }
                }             
                var parentOfs = $B.DomUtils.offset(posParentEl);
                var offset =  $B.DomUtils.offset(el);// $t.offset();
                var position = $B.DomUtils.position(el);
                //如果有css transform旋转，需要计算出 修正的偏移量    
                var ofs = $B.getAnglePositionOffset(el);
                var fixTop = ofs.fixTop;
                var fixLeft = ofs.fixLeft;
                var parent = el.parentNode;            
                var  scrollLeft = $B.DomUtils.scrollLeft(parent);
                var  scrollTop =  $B.DomUtils.scrollTop(parent);
                if(options.setScrollFn){
                    var sclRet = options.setScrollFn(parent);
                    if(typeof sclRet !== "undefined"){
                        scrollLeft = sclRet.scrollLeft;
                        scrollTop = sclRet.scrollTop;
                    }
                }
                var srcsor = el.style.cursor;
                if (!srcsor) {
                    srcsor = 'default';
                }
                var objWidth = $B.DomUtils.outerWidth(el);
                var objHeight = $B.DomUtils.outerHeight(el);
                if (options.isProxy) {
                    /***** 代理是放置于body下的，要用offset替换position *******/
                    position = offset;
                } else {                   
                    var resetCss = {
                        "position": "absolute"
                    };
                    if (options.axis === 'v') {
                        resetCss.top = position.top + fixTop ; //+ scrollTop
                    } else if (options.axis === 'h') {
                        resetCss.left = position.left + fixLeft ;//+ scrollLeft
                    } else {
                        resetCss.top = position.top + fixTop ;//+ scrollTop
                        resetCss.left = position.left + fixLeft ;//+ scrollLeft
                    }
                    if (matrixRate) {
                        resetCss.top = resetCss.top / matrixRate;
                        resetCss.left = resetCss.left / matrixRate;
                        objWidth = objWidth * matrixRate;
                        objHeight = objHeight * matrixRate;
                    }
                    $B.DomUtils.css(el,resetCss);
                    position = $B.DomUtils.position(el);
                }
                //封装拖动数据data对象，记录width,height,pageX,pageY,left 和top 等拖动信息                   
                var data = {
                    matrixRate: matrixRate,
                    parentOfs: parentOfs,
                    srcOfs: offset,
                    srcPos: position,
                    startLeft: position.left , //开始的位置信息	+ scrollLeft
                    startTop: position.top , //开始的位置信息	+ scrollTop
                    scrollLeft: scrollLeft,
                    scrollTop: scrollTop,
                    left: position.left , //当前left位置信息	+ scrollLeft
                    top: position.top , //当前top位置信息       + scrollTop
                    oldLeft: undefined, //旧的left位置 
                    oldTop: undefined, //旧的top位置	
                    startX: e.pageX, //鼠标点击时的x坐标
                    startY: e.pageY, //鼠标点击时的y坐标
                    pageX: e.pageX,
                    pageY: e.pageY,
                    width: objWidth,
                    height: objHeight,
                    fixTop: fixTop,
                    fixLeft: fixLeft,
                    srcsor: srcsor, //原来的鼠标样式
                    leftOffset: 0, //鼠标left偏移
                    topOffset: 0 //鼠标top偏移
                };
                state["hasCallStartFn"] = false; //是否已经调用了onStartDrag   
                state["_data"] = data;
                state["parent"] = parent; //拖动对象的父dom
                state["callStartDragFn"] = typeof options.onStartDrag === "function";
                var nameSpace = options.nameSpace;
                if (typeof options.holdTime !== 'undefined') { //定时触发事件
                    var _this = el;
                    var timer = setTimeout(function () {
                        if(options.onTimeIsUpFn){
                            if(options.onTimeIsUpFn()){
                               return;
                            }
                        }                      
                        if(options.globalDraging){
                            window["_globalDraging"] = true;
                        }                       
                        $B.DomUtils.css(el,{"cursor":state.options.cursor});
                        //往document上绑定鼠标移到监听 
                        _docDown({
                            params:state
                        });
                        $B.DomUtils.removeData(_this,"dragtimer");
                        $B.DomUtils.addListener(window.document,nameSpace+".mousemove",_docMove,state);
                        $B.DomUtils.addListener(window.document,nameSpace+".mouseup",_docUp,state);
                    }, options.holdTime);
                    $B.DomUtils.setData(_this,"dragtimer",timer);
                    $B.DomUtils.addListener(window.document,"clearDragTimer.mouseup",function(e){
                        clearTimeout(e.params.timer);
                        $B.DomUtils.offEvents(window.document,"clearDragTimer.*");
                    },{timer: timer});
                    // $doc.on('mouseup.clearDragTimer', {
                    //     timer: timer
                    // }, function (e) {
                    //     $doc.off('mouseup.clearDragTimer');
                    //     clearTimeout(e.params.timer);
                    // });
                } else {     
                    if(options.globalDraging){
                        window["_globalDraging"] = true;
                    }               
                    $B.DomUtils.css(el,{"cursor":state.options.cursor});
                    if(e.isTrigger){
                        _docDown({
                            params:state
                        });
                    }else{
                        $B.DomUtils.addListener(window.document,nameSpace+".mousedown",_docDown,state);
                    }                   
                    $B.DomUtils.addListener(window.document,nameSpace+".mousemove",_docMove,state);
                    $B.DomUtils.addListener(window.document,nameSpace+".mouseup",_docUp,state);
                }
            }
        }
    }
}
/****
 * 构造函数
 * options：defaults参数
 * *****/
$B.draggable = function (el,options) {
    var nameSpace = "draggable";
    var dragOpt;
    if(typeof el === 'string'){
        el = window.document.getElementById(el);
    }
    if (typeof options === 'string') {
        if (arguments.length > 1) {
            nameSpace = arguments[1];
        }
        var dataKey = nameSpace+"_opt";
        switch (options) {
            case 'enable': //启用拖动
                dragOpt =  $B.getData(el,dataKey).state.options;
                dragOpt.disabled = false;
                el.style.cursor = dragOpt.cursor;
                break;
            case 'disable': //禁用拖动
                dragOpt =  $B.getData(el,dataKey).state.options;
                dragOpt.disabled = false;
                el.style.cursor = "default";
                break;
            case 'unbind': //解除拖动
                let state = $B.getData(el,dataKey).state;
                $B.offEvents(state.handler,nameSpace + '.');
                delete state.handler;
                delete state.target;
                delete state.options;
                delete state.parent;
                $B.removeData(el,dataKey);       
                break;        
            default:
                throw new Error("不支持:" + options);
        }
    }
    var opts = $B.extendObjectFn(true,{},draggableDefaults,options);
    if (opts.nameSpace) {
        nameSpace = opts.nameSpace; //默认的命名空间
    }
    var handler = el; //handler是实际触发拖动的对象
    if (opts.handler) {
        handler = (typeof opts.handler === 'string' ? $B.DomUtils.findbyId(el,opts.handler) : opts.handler);      
        $B.DomUtils.css(handler,{cursor:options.cursor});
    }
    //存在则先解除已经绑定的拖动
    let state = $B.DomUtils.getData(el,nameSpace);
    if (state) {        
        $B.DomUtils.offEvents(state.handler,nameSpace+".*");
        delete state["handler"];
        delete state["target"];
        delete state["parent"];
        delete state["options"];
        delete state["_data"];
    }
    state = {
        handler: handler,
        target: el,
        options: opts
    };
    $B.DomUtils.setData(el,nameSpace, state);
    //注册事件 draggable 为命名空间
    $B.DomUtils.addListener(handler,nameSpace+".mousedown",onMouseDown,state);
    if (typeof opts.holdTime !== 'undefined') {
        $B.DomUtils.addListener(handler,nameSpace+".mouseup",onMouseDown,state);
    }
};
var scrollDefaults = {
    isHide: true,
    scrollAxis: 'xy',
    unScroll:'hidden',
    onScrollFn: undefined,
    style: {
        "radius": "6px",
        "size": "7px",
        "color": "#5DA0FF"
    }
};

function createScrollStyle(styleClzz, clazzName, style) {
    var rgb = $B.hex2RgbObj(style.color);
    var styleCtx = clazzName + '{width:' + style.size + ';height:' + style.size + '}';
    styleCtx = styleCtx + "." + styleClzz + "::-webkit-scrollbar-thumb," + "." + styleClzz + "::-webkit-scrollbar-thumb:vertical {box-shadow: inset 0 0 4px rgba(" + rgb.r + "," + rgb.g + "," + rgb.b + ",1);border-radius:" + style.radius + ";}";
    styleCtx = styleCtx + "." + styleClzz + "::-webkit-scrollbar-thumb:hover," + "." + styleClzz + "::-webkit-scrollbar-thumb:vertical:hover{box-shadow: inset 0 0 4px rgba(" + rgb.r + "," + rgb.g + "," + rgb.b + ",.9);background-color: " + style.color + ";}";
    $B.createHeaderStyle(styleClzz, styleCtx);
}

function getScrollYArgs(scrollTop, scrollHeight, clientHeight) {
    var heightPercentage = (clientHeight * 100 / scrollHeight);
    var vbarHeigth = clientHeight * (heightPercentage / 100);
    var vscrollSize = Math.max(clientHeight, scrollHeight) - clientHeight;
    var scrollRate = (clientHeight - vbarHeigth) / vscrollSize; 
    var posY = scrollTop * scrollRate;
    return { posY: posY, percent: heightPercentage,scrollRate:scrollRate };
}

function getScrollXArgs(scrollLeft, scrollWidth, clientWidth) {
    var widthPercentage = (clientWidth * 100 / scrollWidth);
    var vbarWidth = clientWidth * (widthPercentage / 100);
    var vscrollSize = Math.max(clientWidth, scrollWidth) - clientWidth;
    var scrollRate = (clientWidth - vbarWidth) / vscrollSize;
    var posX = scrollLeft * scrollRate;
    return { posX: posX, percent: widthPercentage,scrollRate:scrollRate };
}

function scrollAxisY(opts, $wrap) {
    var $sol = $B.DomUtils.children($wrap,".k_scrollbar_y");
    var wrap = $wrap;
    var style = opts.style;
    var scrollHeight = wrap.scrollHeight;
    var clientHeight = wrap.clientHeight;
    //console.log(clientHeight+"  "+scrollHeight);
    if (scrollHeight > (clientHeight + 2)) {
        var ret = getScrollYArgs(wrap.scrollTop, scrollHeight, clientHeight);       
        var posY = ret.posY;
        var percent = ret.percent;
        if (!$sol  || $sol.length === 0) {
            $sol = $B.DomUtils.createEl("<div class='k_scrollbar_y k_scrollbar_axis' style='cursor:pointer;position:absolute;right:0;width:" + style.size + ";border-radius:" + style.radius + ";opacity: 0.45;background:" + style.color + "'></div>");
            $B.DomUtils.append($wrap,$sol);
            if($B.draggable){
                $B.draggable($sol,{
                    nameSpace:'kscrollabr',
                    cursor:'pointer',
                    axis: 'v', // v or h  水平还是垂直方向拖动 ，默认全向
                    onDragReady:function(state){
                        $B.DomUtils.removePropData(state.target,"_scring");                      
                        return true;
                    },
                    setScrollFn:function($p){                       
                        let pel = $p.parentNode;
                        var scr = {
                            scrollLeft : $B.DomUtils.scrollLeft(pel),
                            scrollTop : $B.DomUtils.scrollTop(pel)
                        };
                        return scr;
                    },
                    onStartDrag:function(args){                   
                        var state = args.state;   
                        var $p = state.target.parentNode;   
                        var cheight =  $B.DomUtils.height($p);
                        var scrHeight = $p.scrollHeight;
                        var scrTop = $p.scrollTop;
                        var elHeight = $B.DomUtils.height(state.target);
                        var maxTop = cheight - elHeight;
                        var ret1 = getScrollYArgs(scrTop, scrHeight, cheight);
                        state.ret = ret1;
                        state.maxTop = maxTop;
                        $B.DomUtils.propData(state.target,{"_scring":"true"});
                    },
                    onDrag:function(args){
                        var state = args.state;
                        if(state._data.top < 0){
                            state._data.top = 0 ;
                        }else if(state._data.top > state.maxTop){
                            state._data.top = state.maxTop;
                        }
                        var scrollPos = state._data.top / state.ret.scrollRate;
                        $B.DomUtils.scrollTop($wrap,scrollPos);
                    },
                    onStopDrag:function(args){
                        $B.DomUtils.removePropData(args.state.target,"_scring");
                        return false;
                    },
                    onMouseUp:function(args){   
                        $B.DomUtils.removePropData(args.state.target,"_scring");
                    }
                });
            }
        }
        $B.DomUtils.css($sol,{ "height": percent + "%", "top": posY });
        $B.DomUtils.show($sol);
        $B.DomUtils.addClass($sol,"k_scrolly_show");
        $B.DomUtils.removeClass($sol,"k_scrolly_hide");          
    } else if ($sol) {
        $B.DomUtils.hide($sol);
        $B.DomUtils.addClass($sol,"k_scrolly_hide");
        $B.DomUtils.removeClass($sol,"k_scrolly_show");
    }
}
function scrollAxisX(opts, $wrap) {    
    var $sol = $B.DomUtils.children($wrap,".k_scrollbar_x");
    var wrap = $wrap;
    var style = opts.style;
    var scrollWidth = wrap.scrollWidth;
    var clientWidth = wrap.clientWidth;
    if (scrollWidth > (clientWidth + 2)) {
        var ret = getScrollXArgs(wrap.scrollLeft, scrollWidth, clientWidth);
        var posX = ret.posX;
        var percent = ret.percent;
        if (!$sol || $sol.length === 0) {
            $sol = $B.DomUtils.createEl("<div class='k_scrollbar_x k_scrollbar_axis' style='cursor:pointer;position:absolute;bottom:0;height:" + style.size + ";border-radius:" + style.radius + ";opacity: 0.45;background:" + style.color + "'></div>");
            $B.DomUtils.append($wrap,$sol);
            if($B.draggable){
                $B.draggable($sol,{
                    cursor:'pointer',
                    nameSpace:'kscrollabr',
                    axis: 'h', // v or h  水平还是垂直方向拖动 ，默认全向
                    onDragReady:function(state){
                        $B.DomUtils.removePropData(state.target,"_scring");
                        return true;
                    },
                    setScrollFn:function($p){
                        let pel = $p.parentNode;
                        var scr = {
                            scrollLeft : $B.DomUtils.scrollLeft(pel),
                            scrollTop : $B.DomUtils.scrollTop(pel)
                        };                        
                        return scr;
                    },
                    onStartDrag:function(args){
                        var state = args.state;
                        var $p = state.target.parentNode;   
                        var cwidth =   $B.DomUtils.width($p);
                        var scrWidth = $p.scrollWidth;
                        var scrLeft = $p.scrollLeft;                       
                        var ret1 = getScrollXArgs(scrLeft, scrWidth, cwidth);
                        state.ret = ret1;
                        var maxLeft = cwidth -  $B.DomUtils.width(state.target);
                        state.maxLeft = maxLeft;
                        $B.DomUtils.propData(state.target,{"_scring":"true"});
                    },
                    onDrag:function(args){
                        var state = args.state;
                        if(state._data.left < 0){
                            state._data.left = 0 ;
                        }else if(state._data.left > state.maxLeft){
                            state._data.left = state.maxLeft;
                        }
                        var scrollPos = state._data.left / state.ret.scrollRate;
                        $B.DomUtils.scrollLeft($wrap,scrollPos);
                    },
                    onStopDrag:function(args){
                        $B.DomUtils.removePropData(args.state.target,"_scring");
                        return false;
                    },
                    onMouseUp:function(args){
                        $B.DomUtils.removePropData(args.state.target,"_scring");
                    }
                });
            }        
        }
        $B.DomUtils.css($sol,{ "width": percent + "%", "left": posX });
        $B.DomUtils.show($sol);
        $B.DomUtils.addClass($sol,"k_scrollx_show");
        $B.DomUtils.removeClass($sol,"k_scrollx_hide");
    } else if ($sol.length >0) {
        $B.DomUtils.hide($sol);
        $B.DomUtils.addClass($sol,"k_scrollx_hide");
        $B.DomUtils.removeClass($sol,"k_scrollx_show");
    }
}
function myscrollbar(el,opts) {    
    opts = $B.extendObjectFn(true,{}, scrollDefaults,opts);    
    var styleClzz = "SCROLLBAR" + $B.generateMixed(5);
    var elCss = { "overflow": "hidden"};
    var scrollCss = {
        "overflow": "auto"
    };
    if (opts.scrollAxis === "x") {
        scrollCss = {
            "overflow-x": "auto",
            "overflow-y": opts.unScroll
        };
    } else if (opts.scrollAxis === "y") {
        scrollCss = {
            "overflow-x": opts.unScroll,
            "overflow-y": "auto"
        };
    }
    opts.sizeInt = parseInt(opts.style.size.replace("px", ""));
    var posAttr = $B.DomUtils.css(el,"position");
   
    if(posAttr !== "relative" && posAttr !== "absolute"){
        elCss["position"] = "relative";
    }
    $B.DomUtils.css(el,elCss);     
    var childs = $B.DomUtils.detachChilds(el);   
    var $wrapScroll = $B.DomUtils.createEl("<div class='k_scrollbar_wrap k_box_size "+styleClzz+"' style='width:100%;height:100%;'></div>");
    $B.DomUtils.append(el,$wrapScroll);
    $B.DomUtils.css($wrapScroll,scrollCss);    
    $B.DomUtils.scroll($wrapScroll,(e)=>{
        var scrollx = $B.DomUtils.scrollLeft($wrapScroll);
        var scrolly =  $B.DomUtils.scrollTop($wrapScroll);
        var $soly = $B.DomUtils.children($wrapScroll,".k_scrollbar_y");  //y轴滚动条
        var ret;     
        if ($soly.length >0 && $B.DomUtils.hasClass($soly,"k_scrolly_show") ) {
            if(!$B.DomUtils.propData($soly,"_scring")){
                let scrollHeight = $wrapScroll.scrollHeight;
                let clientHeight = $wrapScroll.clientHeight;
                ret = getScrollYArgs(scrolly, scrollHeight, clientHeight);                
                $B.DomUtils.css($soly,{ "height": ret.percent + "%", "top": ret.posY });
                $B.DomUtils.show($soly);
            }           
        }
        var $solx =  $B.DomUtils.children($wrapScroll,".k_scrollbar_x");  //x轴滚动条
        if ($solx.length >0 && $B.DomUtils.hasClass($solx,"k_scrollx_show") ) {
            if(!$B.DomUtils.propData($solx,"_scring")){
                let scrollWidth = $wrapScroll.scrollWidth;
                let clientWidth = $wrapScroll.clientWidth;
                ret = getScrollXArgs(scrollx, scrollWidth, clientWidth);              
                $B.DomUtils.css($solx,{ "width": ret.percent + "%", "left": ret.posX });
                $B.DomUtils.show($solx);
            }            
        }
        if (opts.onScrollFn) {
            opts.onScrollFn.call($wrapScroll, scrollx, scrolly);
        }
    });
    var $wrap = $B.DomUtils.createEl("<div style='position:relative;width:auto;height:auto;overflow:visible;'></div>");
    $B.DomUtils.append($wrapScroll,$wrap);   
    $B.DomUtils.append($wrap,childs);
    var clazzName = "." + styleClzz + "::-webkit-scrollbar";    
    var sclStyle =  $B.extendObjectFn(true,{}, opts.style);
    if (opts.isHide){
        sclStyle.size = '0px';
    }else{
        sclStyle.size = opts.style.size;
    }    
    sclStyle.color = "#F7F7F7"; 
    createScrollStyle(styleClzz, clazzName, sclStyle);
    $B.DomUtils.css($wrapScroll,{"scrollbar-color":"transparent transparent","scrollbar-track-color":"transparent","-ms-scrollbar-track-color": "transparent"});
    var mkScrollFn =  function (e) {
        if(opts.scrollAxis.indexOf("x") >= 0){
            scrollAxisX(opts, $wrapScroll);
        }
        if(opts.scrollAxis.indexOf("y") >= 0){
            scrollAxisY(opts, $wrapScroll);
        }   
        if(opts.onMkScrollFn){
            opts.onMkScrollFn($wrapScroll);
        }        
    };
    mkScrollFn(); 
    let b1 = $B.DomUtils.children($wrapScroll,".k_scrollbar_y");
    let b2 = $B.DomUtils.children($wrapScroll,".k_scrollbar_x");
    $B.DomUtils.addListener($wrapScroll,"myscrollabr.mouseenter",mkScrollFn );
    if (opts.isHide) {
        if(b1.length > 0){
            $B.DomUtils.hide(b1);
        }
        if(b2.length >0){
            $B.DomUtils.hide(b2);
        } 
        $B.DomUtils.addListener($wrapScroll,"myscrollabr.mouseleave", function (e) {
            var $s = this;
            var $c = $B.DomUtils.children($s,".k_scrollbar_y");
            if($c.length >0 && !$B.DomUtils.propData($c,"_scring")){                
                $B.DomUtils.hide($c);
            }            
            $c = $B.DomUtils.children($s,".k_scrollbar_x");
            if($c.length >0 && !$B.DomUtils.propData($c,"_scring")){                
                $B.DomUtils.hide($c);
            }
        });
    }
    setTimeout(function(){
        $B.DomUtils.onDomNodeChanged(function(ele,isAdd){
            let isInDom = $B.DomUtils.inDom($wrap);
            if(!isInDom){ //清理这个回调吧，
                return false;
            }            
            let isInnner = false;
            if($wrap.contains){
                isInnner = $wrap.contains(ele);
            }else{
                let pel = ele.parentNode;
                while(pel){
                    if(pel === $wrap){
                        isInnner = true;
                        break;
                    }
                    pel = pel.parentNode;
                }
            }
            if(isInnner){
                if(opts.isHide){
                    let $c = $B.DomUtils.children($wrapScroll,".k_scrollbar_y");
                    if($c.length >0 && $B.DomUtils.isHide($c)){
                        return ;
                    }
                    $c = $B.DomUtils.children($wrapScroll,".k_scrollbar_x");
                    if($c.length >0 && $B.DomUtils.isHide($c)){
                        return ;
                    }
                }
                setTimeout(()=>{
                    $B.DomUtils.trigger($wrapScroll,"myscrollabr.mouseenter");
                },20);                
            }
        });  
    },200);
    return $wrap;
}
$B["myScrollbar"] = myscrollbar; 
 
 if(!window["$B"] ) {window["$B"] = $B;} 
  return window["$B"] ; 
});
