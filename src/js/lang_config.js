var $B = window["$B"] ? window["$B"] : {};
$B.config = {
    file404:'文件不存在！',
    returnEmptyData: '无数据！',
    loading: '正在加载中......',
    loadingBackground: '#5D39F0',
    oprColName: '操作',
    onlyCheckOneData:'只能勾选一条数据',
    busyTipTitle: '正在处理中...',
    exporting:'正在下载中......',
    urlError:'无效url！',
    exportException:'下载出现异常！',
    permission: '无权限！',
    notlogin: '尚未登录系统！',
    successTitle: '成功提示！',
    alertTitle: '警告提示！',
    errorTitle: '错误提示！',
    messageTitle: '提示信息！',
    confirmTitle: '确认提示！',
    buttonOkText: '确认',
    buttonCancleText: '取消',
    maxZindex: 2147483647,
    unAuthor:'未授权！',
    winDefOpts: {
        iconCls: "fa-popup", //图标class，font-awesome字体图标
        iconColor: "#6A50FC",//图标颜色
        shadow: true, //是否需要阴影
        radius: "2px", //圆角px定义
        header: true, //是否显示头部
        zIndex: 200000000,//层级
        dataType: 'html', //当为url请求时，html/json/iframe
        draggable: true, //是否可以拖动
        moveProxy: false, //是否代理移动方式
        draggableHandler: 'header', //拖动触发焦点
        closeable: true, //是否关闭
        closeType: 'destroy', //关闭类型 hide(隐藏，可重新show)/ destroy 直接从dom中删除
        expandable: false, //可左右收缩
        maxminable: true, //可变化小大
        collapseable: true, //上下收缩
        resizeable: true//右下角拖拉大小
    },
    tableTopToolOpts: {},
    tableTrToolOpts: {},
    paginationSummary: '共{x}条记录，每页{y}条记录',
    firstPageTxt: '第一页',
    lastPageTxt: '最后一页',
    calendar: {
        year: '年',
        month: '月',
        clear: '清空',
        now: '今天',
        close: '关闭',
        monthArray: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
        weekArray: ['日', '一', '二', '三', '四', '五', '六'],
        error: '输入是时间格式错误！',
        minTip: '输入时间不能小于[ x ]',
        maxTip: '输入时间不能大于[ x ]',
        error: '输入值不符合格式要求！'
    },
    upload: {
        label: '请选择文件',
        emptyWarning: '无可上传的文件！',
        delete: '删除文件',
        uploading: '正在上传 ',
        success: '已上传成功！',
        errMsg: '上传失败！',
        clearLabel: '清空完成！'
    },
    verify: {
        regLabel:'输入值不符合要求！',
        require: { label: '该项必填' },
        remote: { label: '该字段[x]已经存在' },
        range: { label: '输入值必须在{1}至{2}范围内' },
        minlength: { label: '输入文字长度必须大于{1}' },
        maxlength: { label: '输入文字长度必须小于{1}' },
        char: { reg: /^(\w|[\u4E00-\u9FA5])+$/, label: '必须为字母、汉字、数字、下划线' },
        wchar: { reg: /^\w+$/, label: '必须为字母、数字、下划线' },
        enchar: { reg: /^[a-zA-Z]+$/, label: '必须填写字母类型的字符' },
        chchar: { reg: /^[\u4E00-\u9FA5|\d*]+$/, label: '必须填写汉字及数字' },
        email: { reg: /^([a-zA-Z0-9_-])+.([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(.[a-zA-Z0-9_-])+/, label: '必须输入正确的邮件' },
        phone: { reg: /^(0|86|17951)?(13[0-9]|15[012356789]|17[678]|18[0-9]|14[57])[0-9]{8}$/, label: '请输入正确的手机号码' },
        telphone: { reg: /^(\(\d{3,4}\)|\d{3,4}-)?\d{7,8}$/, label: '请输入正确的座机号码' },
        number: { reg: /^-?\d+(.?\d+)?$/, label: '必须输入数值文字' },
        digits: { reg: /^[0-9][0-9]*$/, label: '必须输入整形数值' },
        password: { reg: /(?=.*\d)(?=.*[a-zA-Z])(?=.*[^a-zA-Z0-9]).{6,20}/, label: '最少6位包含大小写字母，数字，特殊字符' },
        userName: { reg: /^[a-zA-Z\s_\d]+$/, label: '必须为字母、数字、下划线、中间可包含空格' },
        url: { reg: /((http(s)?|ftp):\/\/)?([\w-]+\.)+[\w-]+(\/[\w- .\/?%&=]*)?(:\d+)?/, label: '请输入合法的url地址' },
        idCardNo: { reg: /^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$|^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}([0-9]|X)$/, label: '请输入合法的身份证号码' }
    },
    tabmenu: {
        "opened":{ icon: 'fa-link-ext', text: '打开新窗口' },
        "reload": { icon: 'fa-arrows-cw', text: '刷新内容' },
        "closeNow": { icon: 'fa-cancel', text: '关闭当前' },
        "closeRight": { icon: 'fa-forward-1', text: '关闭右侧' },
        "closeLeft": { icon: 'fa-reply-1', text: '关闭左侧' },
        "closeMenu": { icon: 'fa-cancel-circled', text: '关闭菜单' }
    },
    curd:{
        checkforUpdate:'请勾选需要更新的数据',
        checkforDelete:'请勾选需要删除的数据',
        confirmDelete:'您真的要删除数据吗？',
        table:{
            fit2height: true, //是否只能占满父元素(垂直方向)
            loadImd: true, //是否立即加载    
            oprCol: true, //是否需要行操作列  
            pgposition: 'bottom', //分页工具栏位置 top,bottom,both ,none none表示不需要分页栏
            pageSize: 35, //页大小           
            lineClamp: 3,//省略号行数         
            pgBtnNum: 10, //页按钮数量          
            pgSummary: true,//是否需要总数量，页数提示          
            iconCls: 'fa-table', //图标   fa-table          
            splitColLine: 'k_table_td_h_line_1', //分割线样式 k_table_td_v_line、k_table_td_h_line，k_table_td_none_line, k_table_td_h_line_1
            topBtnStyle: {
                methodsObject: 'tableMethods',
                style: 'normal', //plain   plain / min  / normal /  big             
                showText: true
            },
            trBtnStyle: {
                align: 'center', //对齐方式，默认是left 、center、right
                methodsObject: 'trMethods',
                style: 'plain', //plain   plain / min  / normal /  big             
                showText: true
            }
        },
        actions: {           
            add: "add",
            del: "delete",
            query: "query",
            get: "get",
            update: "update",
            page: "page"
        },
        window: {
            iconCls: "fa-edit", //图标class，font-awesome字体图标
            iconColor: "#6A50FC",//图标颜色
            shadow: true, //是否需要阴影
            radius: "0px", //圆角px定义
            header: true, //是否显示头部          
            draggable: true, //是否可以拖动 
            triggerHide:true, //触发全局隐藏          
            draggableHandler: 'header', //拖动触发焦点
            closeable: true, //是否关闭           
            expandable: false, //可左右收缩
            maxminable: true, //可变化小大
            collapseable: false, //上下收缩
            resizeable: true//右下角拖拉大小 
        }
    },
    editor:{
        
    }
};