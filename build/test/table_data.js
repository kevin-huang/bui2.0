var toolbars = [{
    params: {
        pr1: 12345678
    }, //额外的参数
    disabled: false, //是否禁用
    text: '按钮3', //文本
    click: function (pr) { //点击事件，如果存在click，则字符串形式的handler不可用
        alert(JSON.stringify(pr));
    }
}, {
    params: {
        pr1: 123,
        p1: 456
    }, //额外的参数
    disabled: false, //是否禁用
    text: '按钮4', //文本
    click: 'lineDetailFn', 
    methodsObject: 'toolMethods'
}];

var treeData = [{
    "id": "2cc7b22456736e8001567377314b0000",
    "text": "我的项目",
    "toolbar": toolbars,
    "data": {
        "controller": "/action/form",
        "createTime": "2016-08-10 16:00:06",
        "depth": 0,
        "id": "2cc7b22456736e8001567377314b0000",
        "isEnable": true,
        "isLeaf": true,
        "isVisible": true,
        "menuIconCss": "menu_mainpage",
        "menuText": "我的项目",
        "orderIndex": 0,
        "pageName": "index",
        "pid": "0"
    },
    "pid": "0"
},
{
    "id": "2cc7b22456736e80015673a67e570002",
    "text": "项目管理",
    "toolbar": toolbars,
    "data": {
        "controller": "/action/formmgr",
        "createTime": "2016-08-10 16:51:46",
        "depth": 0,
        "id": "2cc7b22456736e80015673a67e570002",
        "isEnable": true,
        "isLeaf": true,
        "isVisible": true,
        "menuIconCss": "menu_control",
        "menuText": "项目管理",
        "orderIndex": 4,
        "pageName": "index",
        "params": "gridid=form",
        "pid": "0"
    },
    "pid": "0"
},
{
    "id": "2cc7b22456736e80015673a72a0c0003",
    "text": "页面管理",
    "toolbar": toolbars,
    "data": {
        "controller": "/action/template",
        "createTime": "2016-08-10 16:52:30",
        "depth": 0,
        "id": "2cc7b22456736e80015673a72a0c0003",
        "isEnable": true,
        "isLeaf": true,
        "isVisible": true,
        "menuIconCss": "menu_work",
        "menuText": "页面管理",
        "orderIndex": 6,
        "pageName": "index",
        "pid": "0"
    },
    "pid": "0"
},
{
    "id": "ff80808156e6944e0156fb3cadc10089",
    "text": "控件管理",
    "toolbar": toolbars,
    "data": {
        "controller": "/action/",
        "depth": 0,
        "id": "ff80808156e6944e0156fb3cadc10089",
        "isEnable": true,
        "isLeaf": true,
        "isVisible": true,
        "menuIconCss": "menu_attr",
        "menuText": "控件管理",
        "orderIndex": 7,
        "pid": "0"
    },
    "children": [{
            "id": "2cc7b2f05caf1733015caf1aa1a10000",
            "text": "控件数据管理",
            "toolbar": toolbars,
            "data": {
                "controller": "/action/component",
                "createTime": "2017-06-16 12:12:57",
                "id": "2cc7b2f05caf1733015caf1aa1a10000",
                "isEnable": true,
                "isLeaf": true,
                "isVisible": true,
                "menuText": "控件数据管理",
                "orderIndex": 1,
                "pageName": "index",
                "params": "gridid=component",
                "pid": "ff80808156e6944e0156fb3cadc10089"
            },
            "pid": "ff80808156e6944e0156fb3cadc10089"
        },
        {
            "id": "2cc7b2f05d830e36015d8315045f0000",
            "text": "控件分类管理",
            "toolbar": toolbars,
            "data": {
                "controller": "/action/componentgroup",
                "createTime": "2017-07-27 16:06:18",
                "id": "2cc7b2f05d830e36015d8315045f0000",
                "isEnable": true,
                "isLeaf": true,
                "isVisible": true,
                "menuText": "控件分类管理",
                "orderIndex": 2,
                "params": "gridid=componentgroup",
                "pid": "ff80808156e6944e0156fb3cadc10089"
            },
            "pid": "ff80808156e6944e0156fb3cadc10089"
        },
        {
            "id": "2cc7b2f05caf1733015caf1b2b320002",
            "text": "资源管理",
            "toolbar": toolbars,
            "closed": false,
            "data": {
                "controller": "/action/resource",
                "createTime": "2017-06-16 12:13:32",
                "id": "2cc7b2f05caf1733015caf1b2b320002",
                "isEnable": true,
                "isLeaf": true,
                "isVisible": true,
                "menuText": "资源管理",
                "orderIndex": 4,
                "params": "gridid=resource",
                "pid": "ff80808156e6944e0156fb3cadc10089"
            },
            "pid": "ff80808156e6944e0156fb3cadc10089",
            "children": [{
                "id": "2cc7b22456736e80015673a89a210004",
                "text": "系统管理",
                "toolbar": toolbars,
                "closed": true,
                "data": {
                    "createTime": "2016-08-10 16:54:04",
                    "depth": 0,
                    "id": "2cc7b22456736e80015673a89a210004",
                    "isEnable": true,
                    "isLeaf": false,
                    "isVisible": true,
                    "menuIconCss": "menu_opts",
                    "menuText": "系统管理",
                    "orderIndex": 8,
                    "pid": "0"
                },
                "children": [{
                        "id": "1",
                        "text": "权限管理",
                        "toolbar": toolbars,
                        "closed": true,
                        "data": {
                            "createTime": "2016-09-12 18:15:20",
                            "depth": 0,
                            "id": "1",
                            "isEnable": true,
                            "isLeaf": false,
                            "isVisible": true,
                            "menuText": "权限管理",
                            "orderIndex": 9,
                            "pid": "2cc7b22456736e80015673a89a210004"
                        },
                        "children": [{
                                "id": "23",
                                "text": "用户管理",
                                "toolbar": toolbars,
                                "data": {
                                    "controller": "/base/user",
                                    "createTime": "2016-06-27 20:16:30",
                                    "depth": 1,
                                    "id": "23",
                                    "isEnable": true,
                                    "isLeaf": true,
                                    "isVisible": true,
                                    "menuIconCss": "menu_users",
                                    "menuText": "用户管理",
                                    "orderIndex": 1,
                                    "params": "gridid=user",
                                    "pid": "1"
                                },
                                "pid": "1"
                            },
                            {
                                "id": "24",
                                "text": "角色管理",
                                "toolbar": toolbars,
                                "data": {
                                    "controller": "/base/role",
                                    "createTime": "2016-06-27 20:16:30",
                                    "depth": 1,
                                    "id": "24",
                                    "isEnable": true,
                                    "isLeaf": true,
                                    "isVisible": true,
                                    "menuIconCss": "menu_users",
                                    "menuText": "角色管理",
                                    "orderIndex": 2,
                                    "params": "gridid=role",
                                    "pid": "1"
                                },
                                "pid": "1"
                            },
                            {
                                "id": "25",
                                "text": "功能管理",
                                "toolbar": toolbars,
                                "data": {
                                    "controller": "/base/func",
                                    "createTime": "2016-06-27 20:16:30",
                                    "depth": 1,
                                    "id": "25",
                                    "isEnable": true,
                                    "isLeaf": true,
                                    "isVisible": true,
                                    "menuIconCss": "menu_controller",
                                    "menuText": "功能管理",
                                    "orderIndex": 3,
                                    "params": "gridid=func",
                                    "pid": "1"
                                },
                                "pid": "1"
                            },
                            {
                                "id": "10",
                                "text": "菜单管理",
                                "toolbar": toolbars,
                                "data": {
                                    "controller": "/base/menu",
                                    "createTime": "2016-06-27 20:16:30",
                                    "depth": 1,
                                    "id": "10",
                                    "isEnable": true,
                                    "isLeaf": true,
                                    "isVisible": true,
                                    "menuIconCss": "menu_settings",
                                    "menuText": "菜单管理",
                                    "orderIndex": 4,
                                    "params": "gridid=menu",
                                    "pid": "1"
                                },
                                "pid": "1"
                            }
                        ],
                        "pid": "2cc7b22456736e80015673a89a210004"
                    },
                    {
                        "id": "2cc7b224555216e801555218afdd0000",
                        "text": "系统字典管理",
                        "toolbar": toolbars,
                        "data": {
                            "controller": "/base/dict",
                            "createTime": "2016-06-27 20:16:30",
                            "depth": 1,
                            "id": "2cc7b224555216e801555218afdd0000",
                            "isEnable": true,
                            "isLeaf": true,
                            "isVisible": true,
                            "menuText": "系统字典管理",
                            "orderIndex": 10,
                            "params": "gridid=dict",
                            "pid": "2cc7b22456736e80015673a89a210004"
                        },
                        "pid": "2cc7b22456736e80015673a89a210004"
                    }
                ],
                "pid": "0"
            }]
        },
        {
            "id": "2cc7b2f05caf1733015caf1ae6f80001",
            "text": "属性管理",
            "toolbar": toolbars,
            "data": {
                "controller": "/action/attribute",
                "createTime": "2017-06-16 12:13:14",
                "id": "2cc7b2f05caf1733015caf1ae6f80001",
                "isEnable": true,
                "isLeaf": true,
                "isVisible": true,
                "menuText": "属性管理",
                "orderIndex": 3,
                "params": "gridid=action-attribute",
                "pid": "ff80808156e6944e0156fb3cadc10089"
            },
            "pid": "ff80808156e6944e0156fb3cadc10089"
        },
        {
            "id": "2cc7b2f05cf2189b015cf219fac70000",
            "text": "值内容管理",
            "toolbar": toolbars,
            "data": {
                "controller": "/action/meta",
                "createTime": "2017-06-29 12:26:47",
                "id": "2cc7b2f05cf2189b015cf219fac70000",
                "isEnable": true,
                "isLeaf": true,
                "isVisible": true,
                "menuText": "值内容管理",
                "orderIndex": 5,
                "params": "gridid=meta",
                "pid": "ff80808156e6944e0156fb3cadc10089"
            },
            "children": [{
                "id": "2cc7b2f05d9ca329015d9ce70caf0000",
                "text": "数据绑定测试",
                "toolbar": toolbars,
                "data": {
                    "controller": "/action/form",
                    "createTime": "2017-08-01 16:26:13",
                    "id": "2cc7b2f05d9ca329015d9ce70caf0000",
                    "isEnable": true,
                    "isLeaf": true,
                    "isVisible": true,
                    "menuText": "数据绑定测试",
                    "orderIndex": 6,
                    "pageName": "databind",
                    "params": "page=databind",
                    "pid": "2cc7b2f05cf2189b015cf219fac70000"
                },
                "pid": "2cc7b2f05cf2189b015cf219fac70000"
            }],
            "pid": "ff80808156e6944e0156fb3cadc10089"
        },
        {
            "id": "2cc7b2f05d9ca329015d9ce84faf0001",
            "text": "数据绑定测试",
            "toolbar": toolbars,
            "data": {
                "controller": "/action/databind",
                "createTime": "2017-08-01 16:27:36",
                "id": "2cc7b2f05d9ca329015d9ce84faf0001",
                "isEnable": true,
                "isLeaf": true,
                "isVisible": true,
                "menuText": "数据绑定测试",
                "orderIndex": 6,
                "pid": "ff80808156e6944e0156fb3cadc10089"
            },
            "pid": "ff80808156e6944e0156fb3cadc10089"
        }
    ],
    "pid": "0"
},
{
    "id": "2cc7b22456736e80015673a89a210004",
    "text": "系统管理",
    "toolbar": toolbars,
    "data": {
        "createTime": "2016-08-10 16:54:04",
        "depth": 0,
        "id": "2cc7b22456736e80015673a89a210004",
        "isEnable": true,
        "isLeaf": false,
        "isVisible": true,
        "menuIconCss": "menu_opts",
        "menuText": "系统管理",
        "orderIndex": 8,
        "pid": "0"
    },
    "children": [{
            "id": "1",
            "text": "权限管理",
            "toolbar": toolbars,
            "data": {
                "createTime": "2016-09-12 18:15:20",
                "depth": 0,
                "id": "1",
                "isEnable": true,
                "isLeaf": false,
                "isVisible": true,
                "menuText": "权限管理",
                "orderIndex": 9,
                "pid": "2cc7b22456736e80015673a89a210004"
            },
            "children": [{
                    "id": "23",
                    "text": "用户管理",
                    "toolbar": toolbars,
                    "data": {
                        "controller": "/base/user",
                        "createTime": "2016-06-27 20:16:30",
                        "depth": 1,
                        "id": "23",
                        "isEnable": true,
                        "isLeaf": true,
                        "isVisible": true,
                        "menuIconCss": "menu_users",
                        "menuText": "用户管理",
                        "orderIndex": 1,
                        "params": "gridid=user",
                        "pid": "1"
                    },
                    "pid": "1"
                },
                {
                    "id": "24",
                    "text": "角色管理",
                    "toolbar": toolbars,
                    "data": {
                        "controller": "/base/role",
                        "createTime": "2016-06-27 20:16:30",
                        "depth": 1,
                        "id": "24",
                        "isEnable": true,
                        "isLeaf": true,
                        "isVisible": true,
                        "menuIconCss": "menu_users",
                        "menuText": "角色管理",
                        "orderIndex": 2,
                        "params": "gridid=role",
                        "pid": "1"
                    },
                    "pid": "1"
                },
                {
                    "id": "25",
                    "text": "功能管理",
                    "toolbar": toolbars,
                    "data": {
                        "controller": "/base/func",
                        "createTime": "2016-06-27 20:16:30",
                        "depth": 1,
                        "id": "25",
                        "isEnable": true,
                        "isLeaf": true,
                        "isVisible": true,
                        "menuIconCss": "menu_controller",
                        "menuText": "功能管理",
                        "orderIndex": 3,
                        "params": "gridid=func",
                        "pid": "1"
                    },
                    "pid": "1"
                },
                {
                    "id": "10",
                    "text": "菜单管理",
                    "toolbar": toolbars,
                    "data": {
                        "controller": "/base/menu",
                        "createTime": "2016-06-27 20:16:30",
                        "depth": 1,
                        "id": "10",
                        "isEnable": true,
                        "isLeaf": true,
                        "isVisible": true,
                        "menuIconCss": "menu_settings",
                        "menuText": "菜单管理",
                        "orderIndex": 4,
                        "params": "gridid=menu",
                        "pid": "1"
                    },
                    "pid": "1"
                }
            ],
            "closed": true,
            "pid": "2cc7b22456736e80015673a89a210004"
        },
        {
            "id": "2cc7b224555216e801555218afdd0000",
            "text": "系统字典管理",
            "toolbar": toolbars,
            "data": {
                "controller": "/base/dict",
                "createTime": "2016-06-27 20:16:30",
                "depth": 1,
                "id": "2cc7b224555216e801555218afdd0000",
                "isEnable": true,
                "isLeaf": true,
                "isVisible": true,
                "menuText": "系统字典管理",
                "orderIndex": 10,
                "params": "gridid=dict",
                "pid": "2cc7b22456736e80015673a89a210004"
            },
            "pid": "2cc7b22456736e80015673a89a210004"
        }
    ],
    "closed": true,
    "pid": "0"
}
];

var tableData = {
"resultList": [{
        "toolbar": [{
                "id": "1",
                "iconCls": "fa-send-o",
                "disabled": false,
                "text": "测试",
                "methodsObject": "toolMethods",
                "click": "lineTestFn"
            },
            {
                "id": "2",
                "iconCls": "fa-trash",
                "disabled": true,
                "text": "删除",
                "methodsObject": "toolMethods",
                "click": "lineDleteFn"
            },
            {
                "id": "line_btn_0",
                "cmd": "list",
                "text": "详情",
                "methodsObject": "toolMethods",
                "params": [{
                        "cmd": "list"
                    },
                    {
                        "privilage": "1"
                    }
                ],
                "visualable": true,
                "disabled": false,
                "iconCls": "fa-doc-text",
                "click": "lineDetailFn"
            }
        ],
        "createTime": "2017-06-09 16:26:58",
        "status": 1,
        "actionName": "uploadTestData",
        "content": "编辑流程",
        "id": "ff8080815c8aa869015c8bf6ad1a4a4c",
        "operateip": "172.86.50.5",
        "cmd": "update",
        "createUser": "超级管理员",
        "extFeildsMap": {
            "ROWNUM_": 31
        },
        "controller": "/idp/proflow",
        "menuName": "项目流程管理"
    },
    {
        "toolbar": [{
                "id": "1",
                "iconCls": "fa-send-o",
                "disabled": false,
                "text": "测试",
                "methodsObject": "toolMethods",
                "click": "lineTestFn"
            },
            {
                "id": "2",
                "iconCls": "fa-trash",
                "disabled": true,
                "text": "删除",
                "methodsObject": "toolMethods",
                "click": "lineDleteFn"
            },
            {
                "id": "line_btn_0",
                "cmd": "list",
                "text": "详情",
                "methodsObject": "toolMethods",
                "params": [{
                        "cmd": "list"
                    },
                    {
                        "privilage": "1"
                    }
                ],
                "visualable": true,
                "disabled": false,
                "iconCls": "fa-doc-text",
                "click": "lineDetailFn"
            }
        ],
        "createTime": "2017-06-09 16:26:58",
        "status": 1,
        "actionName": "uploadTestData",
        "content": "编辑流程",
        "id": "ff8080815c8aa869015c8bf6ad1a4a4c",
        "operateip": "172.86.50.5",
        "cmd": "update",
        "createUser": "超级管理员",
        "extFeildsMap": {
            "ROWNUM_": 31
        },
        "controller": "/idp/proflow",
        "menuName": "项目流程管理"
    },
    {
        "toolbar": [{
                "id": "1",
                "iconCls": "fa-send-o",
                "disabled": false,
                "text": "测试",
                "methodsObject": "toolMethods",
                "click": "lineTestFn"
            },
            {
                "id": "2",
                "iconCls": "fa-trash",
                "disabled": true,
                "text": "删除",
                "methodsObject": "toolMethods",
                "click": "lineDleteFn"
            },
            {
                "id": "line_btn_0",
                "cmd": "list",
                "text": "详情",
                "methodsObject": "toolMethods",
                "params": [{
                        "cmd": "list"
                    },
                    {
                        "privilage": "1"
                    }
                ],
                "visualable": true,
                "disabled": false,
                "iconCls": "fa-doc-text",
                "click": "lineDetailFn"
            }
        ],
        "createTime": "2017-06-09 16:26:58",
        "status": 1,
        "actionName": "uploadTestData",
        "content": "编辑流程",
        "id": "ff8080815c8aa869015c8bf6ad1a4a4c",
        "operateip": "172.86.50.5",
        "cmd": "update",
        "createUser": "超级管理员",
        "extFeildsMap": {
            "ROWNUM_": 31
        },
        "controller": "/idp/proflow",
        "menuName": "项目流程管理"
    },
    {
        "toolbar": [{
                "id": "1",
                "iconCls": "fa-send-o",
                "disabled": false,
                "text": "测试",
                "methodsObject": "toolMethods",
                "click": "lineTestFn"
            },
            {
                "id": "2",
                "iconCls": "fa-trash",
                "disabled": true,
                "text": "删除",
                "methodsObject": "toolMethods",
                "click": "lineDleteFn"
            },
            {
                "id": "line_btn_0",
                "cmd": "list",
                "text": "详情",
                "methodsObject": "toolMethods",
                "params": [{
                        "cmd": "list"
                    },
                    {
                        "privilage": "1"
                    }
                ],
                "visualable": true,
                "disabled": false,
                "iconCls": "fa-doc-text",
                "click": "lineDetailFn"
            }
        ],
        "createTime": "2017-06-09 16:26:58",
        "status": 1,
        "actionName": "uploadTestData",
        "content": "编辑流程",
        "id": "ff8080815c8aa869015c8bf6ad1a4a4c",
        "operateip": "172.86.50.5",
        "cmd": "update",
        "createUser": "超级管理员",
        "extFeildsMap": {
            "ROWNUM_": 31
        },
        "controller": "/idp/proflow",
        "menuName": "项目流程管理"
    },
    {
        "toolbar": [{
                "id": "1",
                "iconCls": "fa-send-o",
                "disabled": false,
                "text": "测试",
                "methodsObject": "toolMethods",
                "click": "lineTestFn"
            },
            {
                "id": "2",
                "iconCls": "fa-trash",
                "disabled": true,
                "text": "删除",
                "methodsObject": "toolMethods",
                "click": "lineDleteFn"
            },
            {
                "id": "line_btn_0",
                "cmd": "list",
                "text": "详情",
                "methodsObject": "toolMethods",
                "params": [{
                        "cmd": "list"
                    },
                    {
                        "privilage": "1"
                    }
                ],
                "visualable": true,
                "disabled": false,
                "iconCls": "fa-doc-text",
                "click": "lineDetailFn"
            }
        ],
        "createTime": "2017-06-09 16:26:58",
        "status": 1,
        "actionName": "uploadTestData",
        "content": "编辑流程",
        "id": "ff8080815c8aa869015c8bf6ad1a4a4c",
        "operateip": "172.86.50.5",
        "cmd": "update",
        "createUser": "超级管理员",
        "extFeildsMap": {
            "ROWNUM_": 31
        },
        "controller": "/idp/proflow",
        "menuName": "项目流程管理项目流程管理项目流程管理项目流程管理项目流程管理项目流程管理项目流程管理项目流程管理项目流程管理项目流程管理项目流程管理项目流程管理项目流程管理项目流程管理项目流程管理项目流程管理项目流程管理项目流程管理项目流程管理项目流程管理项目流程管理项目流程管理项目流程管理项目流程管理"
    },
    {
        "toolbar": [{
                "id": "1",
                "iconCls": "fa-send-o",
                "disabled": false,
                "text": "测试",
                "methodsObject": "toolMethods",
                "click": "lineTestFn"
            },
            {
                "id": "2",
                "iconCls": "fa-trash",
                "disabled": true,
                "text": "删除",
                "methodsObject": "toolMethods",
                "click": "lineDleteFn"
            },
            {
                "id": "line_btn_0",
                "cmd": "list",
                "text": "详情",
                "methodsObject": "toolMethods",
                "params": [{
                        "cmd": "list"
                    },
                    {
                        "privilage": "1"
                    }
                ],
                "visualable": true,
                "disabled": false,
                "iconCls": "fa-doc-text",
                "click": "lineDetailFn"
            }
        ],
        "createTime": "2017-06-09 16:26:58",
        "status": 1,
        "actionName": "uploadTestData",
        "content": "编辑流程",
        "id": "ff8080815c8aa869015c8bf6ad1a4a4c",
        "operateip": "172.86.50.5",
        "cmd": "update",
        "createUser": "超级管理员",
        "extFeildsMap": {
            "ROWNUM_": 31
        },
        "controller": "/idp/proflow",
        "menuName": "项目流程管理"
    },
    {
        "toolbar": [{
                "id": "1",
                "iconCls": "fa-send-o",
                "disabled": false,
                "text": "测试",
                "methodsObject": "toolMethods",
                "click": "lineTestFn"
            },
            {
                "id": "2",
                "iconCls": "fa-trash",
                "disabled": true,
                "text": "删除",
                "methodsObject": "toolMethods",
                "click": "lineDleteFn"
            },
            {
                "id": "line_btn_0",
                "cmd": "list",
                "text": "详情",
                "methodsObject": "toolMethods",
                "params": [{
                        "cmd": "list"
                    },
                    {
                        "privilage": "1"
                    }
                ],
                "visualable": true,
                "disabled": false,
                "iconCls": "fa-doc-text",
                "click": "lineDetailFn"
            }
        ],
        "createTime": "2017-06-09 16:26:58",
        "status": 1,
        "actionName": "uploadTestData",
        "content": "编辑流程",
        "id": "ff8080815c8aa869015c8bf6ad1a4a4c",
        "operateip": "172.86.50.5",
        "cmd": "update",
        "createUser": "超级管理员",
        "extFeildsMap": {
            "ROWNUM_": 31
        },
        "controller": "/idp/proflow",
        "menuName": "项目流程管理"
    },
    {
        "toolbar": [{
                "id": "1",
                "iconCls": "fa-send-o",
                "disabled": false,
                "text": "测试",
                "methodsObject": "toolMethods",
                "click": "lineTestFn"
            },
            {
                "id": "2",
                "iconCls": "fa-trash",
                "disabled": true,
                "text": "删除",
                "methodsObject": "toolMethods",
                "click": "lineDleteFn"
            },
            {
                "id": "line_btn_0",
                "cmd": "list",
                "text": "详情",
                "methodsObject": "toolMethods",
                "params": [{
                        "cmd": "list"
                    },
                    {
                        "privilage": "1"
                    }
                ],
                "visualable": true,
                "disabled": false,
                "iconCls": "fa-doc-text",
                "click": "lineDetailFn"
            }
        ],
        "createTime": "2017-06-09 16:26:58",
        "status": 1,
        "actionName": "uploadTestData",
        "content": "编辑流程",
        "id": "ff8080815c8aa869015c8bf6ad1a4a4c",
        "operateip": "172.86.50.5",
        "cmd": "update",
        "createUser": "超级管理员",
        "extFeildsMap": {
            "ROWNUM_": 31
        },
        "controller": "/idp/proflow",
        "menuName": "项目流程管理"
    },
    {
        "toolbar": [{
                "id": "1",
                "iconCls": "fa-send-o",
                "disabled": false,
                "text": "测试",
                "methodsObject": "toolMethods",
                "click": "lineTestFn"
            },
            {
                "id": "2",
                "iconCls": "fa-trash",
                "disabled": true,
                "text": "删除",
                "methodsObject": "toolMethods",
                "click": "lineDleteFn"
            },
            {
                "id": "line_btn_0",
                "cmd": "list",
                "text": "详情",
                "methodsObject": "toolMethods",
                "params": [{
                        "cmd": "list"
                    },
                    {
                        "privilage": "1"
                    }
                ],
                "visualable": true,
                "disabled": false,
                "iconCls": "fa-doc-text",
                "click": "lineDetailFn"
            }
        ],
        "createTime": "2017-06-09 16:26:58",
        "status": 1,
        "actionName": "uploadTestData",
        "content": "编辑流程",
        "id": "ff8080815c8aa869015c8bf6ad1a4a4c",
        "operateip": "172.86.50.5",
        "cmd": "update",
        "createUser": "超级管理员",
        "extFeildsMap": {
            "ROWNUM_": 31
        },
        "controller": "/idp/proflow",
        "menuName": "项目流程管理"
    },
    {
        "toolbar": [{
                "id": "1",
                "iconCls": "fa-send-o",
                "disabled": false,
                "text": "测试",
                "methodsObject": "toolMethods",
                "click": "lineTestFn"
            },
            {
                "id": "2",
                "iconCls": "fa-trash",
                "disabled": true,
                "text": "删除",
                "methodsObject": "toolMethods",
                "click": "lineDleteFn"
            },
            {
                "id": "line_btn_0",
                "cmd": "list",
                "text": "详情",
                "methodsObject": "toolMethods",
                "params": [{
                        "cmd": "list"
                    },
                    {
                        "privilage": "1"
                    }
                ],
                "visualable": true,
                "disabled": false,
                "iconCls": "fa-doc-text",
                "click": "lineDetailFn"
            }
        ],
        "createTime": "2017-06-09 16:26:58",
        "status": 1,
        "actionName": "uploadTestData",
        "content": "编辑流程",
        "id": "ff8080815c8aa869015c8bf6ad1a4a4c",
        "operateip": "172.86.50.5",
        "cmd": "update",
        "createUser": "超级管理员",
        "extFeildsMap": {
            "ROWNUM_": 31
        },
        "controller": "/idp/proflow",
        "menuName": "项目流程管理"
    },
    {
        "toolbar": [{
                "id": "1",
                "iconCls": "fa-send-o",
                "disabled": false,
                "text": "测试",
                "methodsObject": "toolMethods",
                "click": "lineTestFn"
            },
            {
                "id": "2",
                "iconCls": "fa-trash",
                "disabled": true,
                "text": "删除",
                "methodsObject": "toolMethods",
                "click": "lineDleteFn"
            },
            {
                "id": "line_btn_0",
                "cmd": "list",
                "text": "详情",
                "methodsObject": "toolMethods",
                "params": [{
                        "cmd": "list"
                    },
                    {
                        "privilage": "1"
                    }
                ],
                "visualable": true,
                "disabled": false,
                "iconCls": "fa-doc-text",
                "click": "lineDetailFn"
            }
        ],
        "createTime": "2017-06-09 16:26:58",
        "status": 1,
        "actionName": "uploadTestData",
        "content": "编辑流程",
        "id": "ff8080815c8aa869015c8bf6ad1a4a4c",
        "operateip": "172.86.50.5",
        "cmd": "update",
        "createUser": "超级管理员",
        "extFeildsMap": {
            "ROWNUM_": 31
        },
        "controller": "/idp/proflow",
        "menuName": "项目流程管理"
    }
],
"totalSize": 100
};
