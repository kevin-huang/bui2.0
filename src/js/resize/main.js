var defaultOpts = {
    target: undefined, //需要重置大小的目标元素
    zoomScale: false, //是否等比例缩放
    scaleRate: 1,//缩放比例
    poitStyle: {  //8个点配置
        color: '#FF292E',
        "font-size": "8px",
        width: "8px",
        height: "8px",
        background: "#FF0000",
        "border-radius": "4px"
    },
    lineStyle: {
        "border-color": "#FF292E",
        "border-size": "1px"
    },
    onDragReady: undefined,
    dragStart: undefined,
    onDrag: undefined,
    dragEnd: undefined
};
class Resize extends $B.BaseControl {
    constructor(opts) {
        super();
        this.elObjArray = [];
        this.opts = $B.extendObjectFn(true, {}, defaultOpts, opts);
        this.target = this.opts.target;
        this.id = $B.getUUID();
        var _this = this;
        var movingTarget,
            mvFlag,
            zoomScaleUpdateSet = {},
            tgsize,
            tgradio,
            tgpos;
        var onUpdateFn = this.opts.onUpdateFn;
        var onBeforeUpdateFn = this.opts.onBeforeUpdateFn;

        function _dragStart(args) {            
            if(args.shiftKey){
                _this.opts.zoomScale = true;
            }
            var state = args.state;
            movingTarget = state.target;
            mvFlag = movingTarget["resizeData"];
            state._data.mvFlag = mvFlag;
            if (mvFlag._type) {
                /*** 记录点大小 *****/
                mvFlag.size2d = $B.DomUtils.width(movingTarget) / 2;
            }
            tgsize = {
                height: $B.DomUtils.outerHeight(_this.target),
                width: $B.DomUtils.outerWidth(_this.target)
            };
            tgradio = tgsize.width / tgsize.height;
            tgpos = $B.DomUtils.position(_this.target);
            zoomScaleUpdateSet = {};
            zoomScaleUpdateSet["line1"] = {
                width: $B.DomUtils.outerWidth(_this.line1),
                position: $B.DomUtils.position(_this.line1)
            };
            zoomScaleUpdateSet["line2"] = {
                height: $B.DomUtils.outerHeight(_this.line2),
                position: $B.DomUtils.position(_this.line2)
            };
            zoomScaleUpdateSet["line3"] = {
                width: $B.DomUtils.outerWidth(_this.line3),
                position: $B.DomUtils.position(_this.line3)
            };
            zoomScaleUpdateSet["line4"] = {
                height: $B.DomUtils.outerHeight(_this.line4),
                position: $B.DomUtils.position(_this.line4)
            };
            zoomScaleUpdateSet["point0"] = {
                position: $B.DomUtils.position(_this.poitArr[0])
            };
            zoomScaleUpdateSet["point1"] = {
                position: $B.DomUtils.position(_this.poitArr[1])
            };
            zoomScaleUpdateSet["point2"] = {
                position: $B.DomUtils.position(_this.poitArr[2])
            };
            zoomScaleUpdateSet["point3"] = {
                position: $B.DomUtils.position(_this.poitArr[3])
            };
            _this.zoomScaleUpdateSet = zoomScaleUpdateSet;
            if (_this.opts.dragStart) {
                _this.opts.dragStart.call(movingTarget, args);
            }           
        }
        function _zoomUpated(state, _index, _type, update) {
            var movData = state._data;
            var leftOffset = movData.leftOffset;
            var topOffset = movData.topOffset;
            var leftOfsRate, topOfsRate;
            var line1Data = zoomScaleUpdateSet["line1"];
            var line2Data = zoomScaleUpdateSet["line2"];
            var line3Data = zoomScaleUpdateSet["line3"];
            var line4Data = zoomScaleUpdateSet["line4"];
            var targetCss = {};
            if (_type) { //4个点
                if (_this.opts.zoomScale) {
                    if (_index === 0 || _index === 2) {
                        topOffset = leftOffset / tgradio;
                        movData.top = movData.startTop + topOffset;
                    } else {
                        topOffset = -(leftOffset / tgradio);
                        movData.top = movData.startTop + topOffset;
                    }
                }
                leftOfsRate = leftOffset / _this.opts.scaleRate;
                topOfsRate = topOffset / _this.opts.scaleRate;
                var line1Css = {},
                    line2Css = {},
                    line3Css = {},
                    line4Css = {},
                    otherOfs, w, otherOfsRate;
                if (_index === 0) {
                    line1Css["top"] = line1Data.position.top + topOffset;
                    line1Css["left"] = line1Data.position.left + leftOffset;
                    line1Css["width"] = line1Data.width - leftOffset;
                    _this.line1.style.top = line1Css["top"] + "px";
                    _this.line1.style.left = line1Css["left"] + "px";
                    _this.line1.style.width = line1Css["width"]  + "px";
                   
                    line2Css["top"] = line2Data.position.top + topOffset;
                    line2Css["height"] = line2Data.height - topOffset;
                    _this.line2.style.top = line2Css["top"] + "px";
                    _this.line2.style.height = line2Css["height"] + "px";
                    

                    line3Css["left"] = line3Data.position.left + leftOffset;
                    line3Css["width"] = line3Data.width - leftOffset;
                    _this.line3.style.left = line3Css["left"] + "px";
                    _this.line3.style.width = line3Css["width"] + "px";
                    

                    line4Css["height"] = line4Data.height - topOffset;
                    line4Css["top"] = line4Data.position.top + topOffset;
                    line4Css["left"] = line4Data.position.left + leftOffset;
                    _this.line4.style.height = line4Css["height"] + "px";
                    _this.line4.style.top = line4Css["top"] + "px";
                    _this.line4.style.left = line4Css["left"]  + "px";
                    
                    _this._updatePoitPosition(0, topOffset, 1);
                    _this._updatePoitPosition(leftOffset, 0, 3);
                    targetCss["width"] = tgsize.width - leftOfsRate;
                    targetCss["height"] = tgsize.height - topOfsRate;
                    targetCss["top"] = tgpos.top + topOffset;
                    targetCss["left"] = tgpos.left + leftOffset;
                } else if (_index === 1) {
                    line1Css["top"] = line1Data.position.top + topOffset;
                    line1Css["width"] = line1Data.width + leftOffset;
                    _this.line1.style.height = line1Css["top"] + "px";
                    _this.line1.style.width = line1Css["width"] + "px";                   

                    line2Css["top"] = line2Data.position.top + topOffset;
                    line2Css["height"] = line2Data.height - topOffset;
                    line2Css["left"] = line2Data.position.left + leftOffset;
                    _this.line2.style.top = line2Css["top"] + "px";
                    _this.line2.style.left = line2Css["left"] + "px";
                    _this.line2.style.height = line2Css["height"]  + "px";                   

                    line3Css["width"] = line3Data.width + leftOffset;
                    _this.line3.style.width = line3Css["width"]  + "px";                    

                    line4Css["top"] = line4Data.position.top + topOffset;
                    line4Css["height"] = line4Data.height - topOffset;
                    _this.line4.style.top = line4Css["top"] + "px";
                    _this.line4.style.height = line4Css["height"] + "px";                   

                    _this._updatePoitPosition(0, topOffset, 0);
                    _this._updatePoitPosition(leftOffset, 0, 2);
                    targetCss["width"] = tgsize.width + leftOfsRate;
                    targetCss["height"] = tgsize.height - topOfsRate;
                    targetCss["top"] = tgpos.top + topOffset;
                } else if (_index === 2) {
                    line1Css["width"] = line1Data.width + leftOffset;
                    _this.line1.style.width = line1Css["width"]  + "px";                    

                    line2Css["height"] = line2Data.height + topOffset;
                    line2Css["left"] = line2Data.position.left + leftOffset;
                    _this.line2.style.left = line2Css["left"] + "px";
                    _this.line2.style.height = line2Css["height"]  + "px";                    

                    line3Css["width"] = line3Data.width + leftOffset;
                    line3Css["top"] = line3Data.position.top + topOffset;
                    _this.line3.style.width = line3Css["width"]  + "px";
                    _this.line3.style.top = line3Css["top"]  + "px";                    

                    line4Css["height"] = line4Data.height + topOffset;
                    _this.line4.style.height = line4Css["height"] + "px";
                   
                    _this._updatePoitPosition(leftOffset, 0, 1);
                    _this._updatePoitPosition(0, topOffset, 3);
                    targetCss["width"] = tgsize.width + leftOfsRate;
                    targetCss["height"] = tgsize.height + topOfsRate;
                } else {
                    line1Css["left"] = line1Data.position.left + leftOffset;
                    line1Css["width"] = line1Data.width - leftOffset;
                    _this.line1.style.left = line1Css["left"] + "px";
                    _this.line1.style.width = line1Css["width"] + "px";
                   
                    line2Css["height"] = line2Data.height + topOffset;
                    _this.line2.style.height = line2Css["height"]  + "px";
                   

                    line3Css["top"] = line3Data.position.top + topOffset;
                    line3Css["left"] = line3Data.position.left + leftOffset;
                    line3Css["width"] = line3Data.width - leftOffset;
                    _this.line3.style.top = line3Css["top"] + "px";
                    _this.line3.style.left = line3Css["left"] + "px";
                    _this.line3.style.width = line3Css["width"] + "px";
                    

                    line4Css["height"] = line4Data.height + topOffset;
                    line4Css["left"] = line4Data.position.left + leftOffset;
                    _this.line4.style.height = line4Css["height"] + "px";
                    _this.line4.style.left = line4Css["left"] + "px";
                   

                    _this._updatePoitPosition(leftOffset, 0, 0);
                    _this._updatePoitPosition(0, topOffset, 2);

                    targetCss["width"] = tgsize.width - leftOfsRate;
                    targetCss["height"] = tgsize.height + topOfsRate;
                    targetCss["left"] = tgpos.left + leftOffset;
                }
            } else { //4条边
                leftOfsRate = leftOffset / _this.opts.scaleRate;
                topOfsRate = topOffset / _this.opts.scaleRate;
                if (_index === 0) {
                    otherOfs = topOffset / 2;
                    otherOfsRate = topOfsRate / 2;
                    var newTop = line2Data.position.top + topOffset;
                    line2Css = {
                        "top": newTop
                    };
                    line4Css = {
                        "top": newTop
                    };
                    // _this.line2.outerHeight(line2Data.height - topOffset);
                    // _this.line4.outerHeight(line4Data.height - topOffset);
                    $B.DomUtils.outerHeight(_this.line2,line2Data.height - topOffset);
                    $B.DomUtils.outerHeight(_this.line4,line4Data.height - topOffset);

                    var point0ShiftLeft = 0;
                    var point1ShiftLeft = 0;

                    targetCss["top"] = tgpos.top + topOffset;
                    targetCss["height"] = tgsize.height - topOfsRate;

                    if (_this.opts.zoomScale) { //等比例缩放
                        otherOfs = otherOfs * tgradio;
                        movData.left = line1Data.position.left + otherOfs;
                        w = $B.DomUtils.outerHeight() * tgradio;
                        $B.DomUtils.outerHeight(_this.line1,w);
                        $B.DomUtils.outerHeight(_this.line3,w);
                        $B.DomUtils.css(_this.line3,{left :  line3Data.position.left + otherOfs});
                        // w = _this.line2.outerHeight() * tgradio;
                        // _this.line1.outerWidth(w);
                        // _this.line3.outerWidth(w);
                        // _this.line3.css("left", line3Data.position.left + otherOfs);
                        line2Css["left"] = line2Data.position.left - otherOfs;
                        line4Css["left"] = line4Data.position.left + otherOfs;
                        point0ShiftLeft = otherOfs;
                        point1ShiftLeft = -otherOfs;
                        _this._updatePoitPosition(-otherOfs, 0, 2);
                        _this._updatePoitPosition(otherOfs, 0, 3);
                        targetCss["left"] = tgpos.left + otherOfs;
                        targetCss["width"] = tgradio * targetCss['height'];
                        line1Css = {
                            left: line1Data.position.left + otherOfs
                        };
                        //_this.line1.css(line1Css);
                        $B.DomUtils.css(_this.line1,line1Css);
                    }
                    _this._updatePoitPosition(point0ShiftLeft, topOffset, 0);
                    _this._updatePoitPosition(point1ShiftLeft, topOffset, 1);
                    $B.DomUtils.css(_this.line2,line2Css);
                    $B.DomUtils.css(_this.line4,line4Css);
                    // _this.line2.css(line2Css);
                    // _this.line4.css(line4Css);
                } else if (_index === 1) {
                    otherOfs = leftOffset / 2;
                    w = line1Data.width + leftOffset;
                    line1Css = {
                        width: w
                    };
                    line2Css = {
                        left: line1Data.position.left + leftOffset
                    };
                    line3Css = {
                        width: w
                    };
                    targetCss["width"] = tgsize.width + leftOfsRate;
                    var p1TopOffset = topOffset;
                    var p2TopOffset = topOffset;
                    if (_this.opts.zoomScale) { //等比例缩放
                        otherOfs = otherOfs / tgradio;
                        movData.top = line2Data.position.top - otherOfs;
                        line2Css["height"] = (line3Data.width + leftOffset) / tgradio;
                        line2Css["top"] = line2Data.position.top - otherOfs;
                        p1TopOffset = -otherOfs;
                        p2TopOffset = otherOfs;
                        _this._updatePoitPosition(0, p1TopOffset, 0);
                        _this._updatePoitPosition(0, p2TopOffset, 3);
                        $B.DomUtils.css(_this.line4,{height: line2Css["height"], top: line4Data.position.top - otherOfs});
                        $B.DomUtils.css(_this.line1,{top: line1Data.position.top - otherOfs});
                        $B.DomUtils.css(_this.line3,{top: line3Data.position.top + otherOfs});
                       
                        targetCss["height"] = targetCss["width"] / tgradio;
                        targetCss["top"] = tgpos.top - otherOfs;
                    }
                    _this._updatePoitPosition(leftOffset, p1TopOffset, 1);
                    _this._updatePoitPosition(leftOffset, p2TopOffset, 2);
                    $B.DomUtils.css(_this.line1,line1Css);
                    $B.DomUtils.css(_this.line2,line2Css);
                    $B.DomUtils.css(_this.line3,line3Css);                    
                } else if (_index === 2) {
                    otherOfs = topOffset / 2;
                    var p2LeftOffset = 0;
                    var p3LeftOffset = 0;
                    var h = line2Data.height + topOffset;
                    line2Css = {
                        height: h
                    };
                    line4Css = {
                        height: h
                    };
                    line3Css = {
                        top: line3Data.position.top + topOffset
                    };
                    targetCss["height"] = tgsize.height + topOfsRate;
                    if (_this.opts.zoomScale) { //等比例缩放
                        otherOfs = otherOfs * tgradio;
                        movData.left = line3Data.position.left - otherOfs;
                        line3Css["width"] = line4Css.height * tgradio;
                        line3Css["left"] = line3Data.position.left - otherOfs;
                        line1Css = {
                            "width": line3Css["width"],
                            left: line1Data.position.left - otherOfs
                        };
                        $B.DomUtils.css(_this.line1,line1Css);                       
                        line2Css["left"] = line2Data.position.left + otherOfs;
                        line4Css["left"] = line4Data.position.left - otherOfs;
                        p2LeftOffset = otherOfs;
                        p3LeftOffset = -otherOfs;
                        _this._updatePoitPosition(-otherOfs, 0, 0);
                        _this._updatePoitPosition(otherOfs, 0, 1);
                        targetCss["width"] = tgradio * targetCss['height'];
                        targetCss["left"] = tgpos.left - otherOfs;
                    }
                    $B.DomUtils.css(_this.line4,line4Css);
                    $B.DomUtils.css(_this.line2,line2Css);
                    $B.DomUtils.css(_this.line3,line3Css);                   
                    _this._updatePoitPosition(p2LeftOffset, topOffset, 2);
                    _this._updatePoitPosition(p3LeftOffset, topOffset, 3);
                } else {
                    otherOfs = leftOffset / 2;
                    line1Css = {
                        width: line1Data.width - leftOffset,
                        left: line1Data.position.left + leftOffset
                    },
                        line3Css = {
                            width: line1Css.width,
                            left: line1Css.left
                        },
                        line4Css = {
                            left: line4Data.left - leftOffset
                        };
                    targetCss["width"] = tgsize.width - leftOfsRate;
                    targetCss["left"] = tgpos.left + leftOffset;
                    var point0TopOffset = 0;
                    var point3TopOffset = 0;
                    if (_this.opts.zoomScale) { //等比例缩放
                        otherOfs = otherOfs / tgradio;
                        point0TopOffset = otherOfs;
                        point3TopOffset = -otherOfs;
                        _this._updatePoitPosition(0, point0TopOffset, 1);
                        _this._updatePoitPosition(0, point3TopOffset, 2);
                        movData.top = line4Data.position.top + otherOfs;
                        line4Css["height"] = (line1Data.width - leftOffset) / tgradio;
                        line4Css["top"] = line4Data.position.top + otherOfs;
                        line2Css = {
                            height: line4Css["height"],
                            top: movData.top
                        };
                        _this.line2.css(line2Css);
                        line1Css["top"] = line1Data.position.top + otherOfs;
                        line3Css["top"] = line3Data.position.top - otherOfs;
                        targetCss["height"] = targetCss["width"] / tgradio;
                        targetCss["top"] = tgpos.top + otherOfs;
                    }                    
                    $B.DomUtils.css(_this.line4,line4Css);
                    $B.DomUtils.css(_this.line1,line1Css);
                    $B.DomUtils.css(_this.line3,line3Css);
                    _this._updatePoitPosition(leftOffset, point0TopOffset, 0);
                    _this._updatePoitPosition(leftOffset, point3TopOffset, 3);
                }
            }
            if (_this.target) {
                if (update) {
                    if (onBeforeUpdateFn) {
                        onBeforeUpdateFn(targetCss);
                    }
                    if (_this.opts.scaleRate) {
                        if (typeof targetCss.top !== "undefined") {
                            targetCss.top = targetCss.top / _this.opts.scaleRate;
                        }
                        if (typeof targetCss.left !== "undefined") {
                            targetCss.left = targetCss.left / _this.opts.scaleRate;
                        }
                    }                    
                    $B.DomUtils.css(_this.target,targetCss);
                    if (onUpdateFn) {
                        onUpdateFn(_this.target, movData);
                    }
                }
            } else {
                console.log("_this.target.css(targetCss); is null");
            }
        }
        function _onDrag(args) {
            var state = args.state;
            var _idx = mvFlag.index;
            var _type = mvFlag._type;
            var update = true;
            if (_this.opts.onDrag) {
                var ret = _this.opts.onDrag.call(movingTarget, args);
                if (typeof ret !== "undefined") {
                    update = ret;
                }
            }
            if (update) {
                _zoomUpated(state, _idx, _type, update);
            }
        }
        function _dragEnd(args) {
            if (_this.opts.dragEnd) {
                _this.opts.dragEnd.call(movingTarget, args);
            }
            movingTarget = undefined;
            mvFlag = undefined;
        }
        var dragOpt = {
            nameSpace: 'dragrezie', //命名空间，一个对象可以绑定多种拖动方式
            which: 1, //鼠标键码，是否左键1,右键3 才能触发拖动，默认左右键均可
            cursor: 'move',
            axis: undefined, // v or h  水平还是垂直方向拖动 ，默认全向
            onStartDrag: _dragStart,
            onDrag: _onDrag,
            onStopDrag: _dragEnd
        };
        var $body = $B.getBody();
        this.line1 = $B.DomUtils.createEl("<div style='cursor:s-resize;height:3px;position:absolute;border-top:1px solid;display:none;z-index:2147483647' _id='k_resize_line_0' class='k_resize_element k_box_size k_resize_line_0'></div>");
        this.line2 = $B.DomUtils.createEl("<div style='cursor:w-resize;width:3px;position:absolute;border-right:1px solid;display:none;z-index:2147483647' _id='k_resize_line_1' class='k_resize_element k_box_size k_resize_line_1'></div>");
        this.line3 = $B.DomUtils.createEl("<div style='cursor:s-resize;height:3px;position:absolute;border-bottom:1px solid;display:none;z-index:2147483647' _id='k_resize_line_2' class='k_resize_element k_box_size k_resize_line_2'></div>");
        this.line4 = $B.DomUtils.createEl("<div style='cursor:w-resize;width:3px;position:absolute;border-left:1px solid;display:none;z-index:2147483647'_id='k_resize_line_3'  class='k_resize_element k_box_size k_resize_line_3'></div>");
        $B.DomUtils.append($body,this.line1);
        $B.DomUtils.append($body,this.line2);
        $B.DomUtils.append($body,this.line3);
        $B.DomUtils.append($body,this.line4);
        this.elObjArray.push(this.line1);
        this.elObjArray.push(this.line2);
        this.elObjArray.push(this.line3);
        this.elObjArray.push(this.line4);
        dragOpt["cursor"] = "s-resize";
        dragOpt["axis"] = "v";
        $B.DomUtils.css(this.line1,this.opts.lineStyle);
        this.line1["resizeData"] = {
            _type: 0,
            index: 0
        };
        $B.draggable(this.line1,dragOpt);
        dragOpt["cursor"] = "w-resize";
        dragOpt["axis"] = "h";
        $B.DomUtils.css(this.line2,this.opts.lineStyle);
        this.line2["resizeData"] = {
            _type: 0,
            index: 1
        };
        $B.draggable(this.line2,dragOpt);

        dragOpt["cursor"] = "s-resize";
        dragOpt["axis"] = "v";

        $B.DomUtils.css(this.line3,this.opts.lineStyle);
        this.line3["resizeData"] = {
            _type: 0,
            index: 2
        };
        $B.draggable(this.line3,dragOpt);
        dragOpt["cursor"] = "w-resize";
        dragOpt["axis"] = "h";
        $B.DomUtils.css(this.line4,this.opts.lineStyle);
        this.line4["resizeData"] = {
            _type: 0,
            index: 3
        };
        $B.draggable(this.line4,dragOpt);
        this._fixLineStyle();
        this.poitArr = {};
        var i = 0;
        //var clzIcon = this.opts.poitStyle.icon;        
        var cursor;
        dragOpt["axis"] = undefined;
        var poitCss = {
            width: "8px",
            height: "8px",
            background: "#FF0000",
            "border-radius": "4px"
        };
        while (i < 4) {
            if (i === 0) {
                cursor = "se-resize";
            } else if (i === 1) {
                cursor = "ne-resize";
            } else if (i === 2) {
                cursor = "se-resize";
            } else {
                cursor = "ne-resize";
            }
            dragOpt["cursor"] = cursor;
            let piontHtml = "<div style='display:none;position:absolute;z-index:2147483647;cursor:" + cursor + "' class='k_resize_element k_resize_element_point k_box_size k_resize_point_" + i + "' _id='k_resize_point_" + i + "'></div>";
            let tmp = $B.DomUtils.createEl(piontHtml);
            $B.DomUtils.append($body,tmp);
            this.elObjArray.push(tmp);
            $B.DomUtils.css(tmp,poitCss);
            $B.DomUtils.css($B.DomUtils.children(tmp),this.opts.poitStyle);
            $B.draggable(tmp,dragOpt);
            tmp["resizeData"] = { _type: 1, index: i};
            this.poitArr[i] = tmp;
            i = ++i;
        }        
        if (this.target) {
            this.bind(this.target);
        }
    }
    _fixLineStyle() {
        $B.DomUtils.css(this.line1,{
            "border-left": "none",
            "border-right": "none",
            "border-bottom": "none"
        });
        $B.DomUtils.css(this.line2,{
            "border-left": "none",
            "border-top": "none",
            "border-bottom": "none"
        });
        $B.DomUtils.css(this.line3,{
            "border-left": "none",
            "border-right": "none",
            "border-top": "none"
        });
        $B.DomUtils.css(this.line4,{
            "border-top": "none",
            "border-right": "none",
            "border-bottom": "none"
        });        
    }
    setStyle(pointStyle, lineStyle) {
        $B.DomUtils.css(this.line4,lineStyle);
        $B.DomUtils.css(this.line3,lineStyle);
        $B.DomUtils.css(this.line2,lineStyle);
        $B.DomUtils.css(this.line1,lineStyle);
        this._fixLineStyle();
        var _this = this;
        Object.keys(this.poitArr).forEach(function (key) {
            $B.DomUtils.css(_this.poitArr[key],pointStyle);
        });
    }
    zoomScale(zoom) {
        this.opts.zoomScale = zoom;
    }
    setTarget(target) {
        if (!this.target || this.target!== target) {
            this.target = target;
        }
    }
    bind(target) {
       // console.log("bind target >>>>>>");
        this.setTarget(target);       
        var ofs = $B.DomUtils.offset(target);
        var size = {
            width: $B.DomUtils.outerWidth( target),
            height:$B.DomUtils.outerHeight( target)
        };
        if (this.opts.onBinding) { //用于外部设置缩放后的大小调整
            this.opts.onBinding(size, this.opts);
        }       
        $B.DomUtils.css(this.line1,{
            top: (ofs.top - 1) + "px",
            left: ofs.left + "px",
            width:size.width
        });      
        $B.DomUtils.css(this.line2,{
            top: ofs.top + "px",
            left: (ofs.left + size.width - 2) + "px",
            height: size.height
        });
        $B.DomUtils.css(this.line3,{
            top: (ofs.top + size.height - 2) + "px",
            left: ofs.left + "px",
            width: size.width
        });
        $B.DomUtils.css(this.line4,{
            top: ofs.top + "px",
            left: ofs.left - 1 + "px",
            height: size.height
        });
        this.show();
        this._initPoitPosition();
        return this;
    }
    _updatePoitPosition(leftOffset, topOffset, updateKey) {
        var point = this.poitArr[updateKey];
        var poitData = this.zoomScaleUpdateSet["point" + updateKey];        
        $B.DomUtils.css(point,{
            top: poitData.position.top + topOffset,
            left: poitData.position.left + leftOffset
        });
    }
    _initPoitPosition() {
        var poitKesy = Object.keys(this.poitArr);
        if (poitKesy.length > 0) {
            var line1Data = {
                width: $B.DomUtils.outerWidth( this.line1),
                position: $B.DomUtils.position( this.line1)
            };
            var line2Data = {
                height: $B.DomUtils.outerHeight(this.line2),
                position: $B.DomUtils.position( this.line2)
            };
            var line3Data = {
                width:  $B.DomUtils.outerWidth( this.line3),
                position: $B.DomUtils.position( this.line3)
            };
            var _this = this;
            var poitW,poitH;
            poitKesy.forEach(function (idx) {
                var key = parseInt(idx);
                var poit = _this.poitArr[key];
                if(!poitW){
                    poitW = $B.DomUtils.width(poit) /2;
                    poitH = $B.DomUtils.height(poit) /2;
                }               
                var _pos;
                if (key === 0) {
                    _pos = {
                        top: (line1Data.position.top - poitH) + "px",
                        left: (line1Data.position.left - poitW) + "px"
                    };
                } else if (key === 1) {
                    _pos = {
                        top: (line1Data.position.top - poitH) + "px",
                        left: (line2Data.position.left - poitW / 2) + "px"
                    };
                } else if (key === 2) {
                    _pos = {
                        top: (line3Data.position.top - poitH / 2) + "px",
                        left: (line2Data.position.left - poitW / 2) + "px"
                    };
                } else if (key === 3) {
                    _pos = {
                        top: (line3Data.position.top - poitH / 2) + "px",
                        left: (line3Data.position.left - poitW) + "px"
                    };
                }
                $B.DomUtils.css(poit,_pos);
            });
        }
    }
    _drag(flag, opt) {
        var _this = this;
        if (flag === "line") {
            $B.draggable(this.line1,opt,"dragrezie");
            $B.draggable(this.line2,opt,"dragrezie");
            $B.draggable(this.line3,opt,"dragrezie");
            $B.draggable(this.line4,opt,"dragrezie");
        } else if (flag === "point") {
            Object.keys(this.poitArr).forEach(function (idx) {
                var key = parseInt(idx);
                var poit = _this.poitArr[key];               
                $B.draggable(poit,opt,"dragrezie");
            });
        } else if (flag === "right") {           
            $B.draggable(this.line2,opt,"dragrezie");
            $B.draggable(this.line3,opt,"dragrezie");
            $B.draggable(this.poitArr[1],opt,"dragrezie");
            $B.draggable(this.poitArr[2],opt,"dragrezie");
        } else if (flag === "left") {           
            $B.draggable(this.line1,opt,"dragrezie");
            $B.draggable(this.line4,opt,"dragrezie");
            $B.draggable(this.poitArr[3],opt,"dragrezie");
            $B.draggable(this.poitArr[0],opt,"dragrezie");

        } else if (flag === 'LRLine') { //禁用除上下线的所有点和线           
            $B.draggable(this.line2,opt,"dragrezie");
            $B.draggable(this.line4,opt,"dragrezie");
            Object.keys(this.poitArr).forEach(function (idx) {
                var key = parseInt(idx);
                var poit = _this.poitArr[key];               
               $B.draggable(poit,opt,"dragrezie");
            });
        } else {           
            $B.draggable(this.line1,opt,"dragrezie");
            $B.draggable(this.line2,opt,"dragrezie");
            $B.draggable(this.line3,opt,"dragrezie");
            $B.draggable(this.line4,opt,"dragrezie");
            Object.keys(this.poitArr).forEach(function (idx) {
                var key = parseInt(idx);
                var poit = _this.poitArr[key];               
               $B.draggable(poit,opt,"dragrezie");
            });
        }
    }
    /**
         * 禁用拖动
         * flag[line/point/left/right] 不传值则禁用所有
         * line:禁用线
         * point:禁用点
         * left：禁用上边线，左边线，左边点
         * right:禁用下边线，右边线，右边点
         * **/
    disable(flag) {
        this._drag(flag, "disable");
    }
    /**
     * 启用拖动 
     * flag[line/point/left/right] 不传值则启用所有
     * line:启用线
     * point:启用点
     * left：启用上边线，左边线，左边点
     * right:启用下边线，右边线，右边点   
     * **/
    enable(flag) {
        this._drag(flag, "enable");
    }
    unbind() {
        this.target = undefined;
        this.hide();
        return this;
    }
    show(target) {
        //旋转后，不允许再resize
        if (target) {
            this.bind(target);
            var rotate = $B.DomUtils.attribute( target,"rotate");
            if (rotate && (rotate === "90" || rotate === "270")) {
                this.hide();
                return;
            }
        } else {
            for(let i =0 ; i < this.elObjArray.length ;i++){
                $B.DomUtils.show(this.elObjArray[i]);
            }   
        }
        return this;
    }
    hide(target) {
        if (!target || (this.target && target && target === this.target)) {
            for(let i =0 ; i < this.elObjArray.length ;i++){
                $B.DomUtils.hide(this.elObjArray[i]);
            }           
        }
        return this;
    }
    isHide() {
        return this.line1.style.display === "none";
    }
    isShow() {
        return  this.line1.style.display !== "none";
    }
    destroy(isForce) {
        for(let i =0 ; i < this.elObjArray.length ;i++){
            $B.DomUtils.remove(this.elObjArray[i]);
        }
        this.elObjArray = undefined;
        super.destroy();
    }
}
$B["Resize"] = Resize;