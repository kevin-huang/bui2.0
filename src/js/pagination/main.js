var defaultOpts = {
    pageSize: 15, //页大小
    pageList: [15, 20, 25, 30, 35, 40, 55, 70, 80, 90, 100], //页大小项
    page: 1, //当前页
    total: 101, //总记录
    buttons: 10, //页按钮数量
    position: 'left', // right center left
    buttonCss: undefined,
    simpleStyle: false,
    summary: true, //是否需要页大小，页总数汇总显示
    onClick: undefined
};
function _createNumsFn(ins) {
    var opt = ins.opts;
    if (opt.page > this.pageCount) {
        opt.page = 1;
    }
    var fragment = document.createDocumentFragment();
    var i = 0;
    //根据当前页，和页大小，总页数计算出开始页 
    var tmp = opt.page;
    while (tmp >= 0) {
        if ((tmp % opt.buttons) === 0 && tmp !== opt.page) {
            break;
        }
        tmp--;
    }
    var startPage = tmp + 1;
    var firstNumEl, lastNumEl;
    while (i < opt.buttons) {
        if (startPage > ins.pageCount) {
            break;
        }
        let $n = $B.DomUtils.createEl("<span class='k_pagination_num'>" + startPage + "</span>");
        if (!firstNumEl) {
            firstNumEl = $n;
        }
        lastNumEl = $n;
        if (startPage === opt.page) {
            ins.setCurrentPage($n);
        }
        if (opt.buttonCss) {
            $B.DomUtils.css($n, opt.buttonCss);
        }
        fragment.appendChild($n);
        startPage++;
        i++;
    }
    $B.DomUtils.removeChilds(ins.$NumSpan);
    $B.DomUtils.append(ins.$NumSpan, fragment);
}
function _createTools(ins) {
    let $el;
    if (ins.opts.summary && !ins.opts.simpleStyle) {
        let sumaryTxt = $B.config && $B.config.paginationSummary ? $B.config.paginationSummary : 'record:{x}，{y}pages';
        sumaryTxt = sumaryTxt.replace('{x}', '<span class="k_pg_total">' + ins.opts.total + '</span>');
        sumaryTxt = sumaryTxt.replace('{y}', '<span class="k_pg_pagesize k_dropdown_list_el"><span>' + ins.opts.pageSize + '</span><i class="fa fa-down-open-big k_dropdown_list_el"></i></span>');
        sumaryTxt = '<span class="k_pg_summary">' + sumaryTxt + '</span>';
        $el = $B.DomUtils.append(ins.$prevSpan, sumaryTxt);
        ins.$total = $B.DomUtils.findByClass($el, ".k_pg_total");
        ins.$pageSize = $B.DomUtils.findByClass($el, ".k_pg_pagesize")[0];
    }
    //首页
    if (!ins.opts.simpleStyle) {
        let firstPageTxt = $B.config && $B.config.firstPageTxt ? $B.config.firstPageTxt : 'firstPage';
        $el = $B.DomUtils.createEl("<span style='cursor:pointer' class='k_pg_first_page'>" + firstPageTxt + "</span>");
        $B.DomUtils.append(ins.$prevSpan, $el);
        ins.$firstPageBtn = $el;
    }
    //前进一个批次
    $el = $B.DomUtils.createEl("<span class='k_pg_prev_pages'><i style='padding:0 5px;' class='fa-angle-double-left'></i></span>");
    $B.DomUtils.append(ins.$prevSpan, $el);
    ins.$prevPagesBtn = $el;

    //前一页功能
    $el = $B.DomUtils.createEl("<span class='k_pg_prev_page'><i style='padding:0 5px;' class='fa-angle-left'></i></span>");
    $B.DomUtils.append(ins.$prevSpan, $el);
    ins.$prevPageBtn = $el;

    //后一页功能
    $el = $B.DomUtils.createEl("<span class='k_pg_next_page'><i style='padding:0 5px;' class='fa-angle-right'></i></span>");
    $B.DomUtils.append(ins.$nextSpan, $el);
    ins.$nextPageBtn = $el;

    //后进一个批次
    $el = $B.DomUtils.createEl("<span class='k_pg_next_pages'><i style='padding:0 5px;' class='fa-angle-double-right'></i></span>");
    $B.DomUtils.append(ins.$nextSpan, $el);
    ins.$nextPagesBtn = $el;

    if (!ins.opts.simpleStyle) {
        //最后一页 
        let lastPageTxt = $B.config && $B.config.lastPageTxt ? $B.config.lastPageTxt : 'lastPage';
        $el = $B.DomUtils.createEl("<span style='cursor:pointer' class='k_pg_last_page'>" + lastPageTxt + "</span>");
        $B.DomUtils.append(ins.$nextSpan, $el);
        ins.$lastPageBtn = $el;
    }
}
class Pagination extends $B.BaseControl {
    constructor(elObj, opts) {
        //console.log("create Pagination >>>>>>>>>>>>>>");
        super();
        this.opts = $B.extendObjectFn(true, {}, defaultOpts, opts);
        super.setElObj(elObj);
        $B.DomUtils.addClass(this.elObj, "k_pagination_wrap k_disabled_selected");
        $B.DomUtils.css(this.elObj, { "text-align": this.opts.position });
        this.$prevSpan = $B.DomUtils.append(this.elObj, "<span class='k_pg_prevspan'></span>");
        this.$NumSpan = $B.DomUtils.append(this.elObj, "<span></span>");
        this.$nextSpan = $B.DomUtils.append(this.elObj, "<span class='k_pg_nextspan'></span>");     
        _createTools(this);
        this.countPage();
        _createNumsFn(this);
        this.go2page(this.opts.page);
        this.bindEvents();
    }
    setCurrentPage(el) {
        if (this.currentBtn) {
            $B.DomUtils.removeClass(this.currentBtn, "k_pagination_actived");
        }
        $B.DomUtils.addClass(el, "k_pagination_actived");
        this.currentBtn = el;
    }
    countPage() {
        this.pageCount = Math.ceil(this.opts.total / this.opts.pageSize) || 1; //总页数 
    }
    dropdownList() {
        if (!this.pageItems) {
            this.pageItems = [];
            var isFined = false;
            for (let i = 0; i < this.opts.pageList.length; i++) {
                let it = { id:i, text: this.opts.pageList[i] };
                this.pageItems.push(it);
                if (this.opts.pageSize === this.opts.pageList[i]) {
                    isFined = true;
                    it["selected"] = true;
                }
            }
            if (!isFined) {
                this.pageItems.unshift({id:this.opts.pageSize, text: this.opts.pageSize, selected: true });
            }
            this.$dropList =  $B.createDropList(this.$pageSize, this.pageItems,
                {
                    onClick: (data, el) => {
                        this.$pageSize.firstChild.innerText = data.text;
                        this.opts.pageSize = parseInt(data.text);
                        if (this.opts.onClick) {
                            setTimeout(() => {
                                this.opts.page = 1;
                                this.opts.onClick(this.opts.page, this.opts.pageSize);
                            }, 10);
                        }
                        return true;
                    }
                });
        }
    }
    go2page(page) {
        this.opts.page = page;
        let ret;
        if (this.$prevPageBtn) {
            if (page === 1) {
                $B.DomUtils.addClass(this.$prevPageBtn, "k_pg_forbid_css");
                $B.DomUtils.addClass(this.$prevPageBtn.firstChild, "k_pg_forbid_css");
            } else {
                $B.DomUtils.removeClass(this.$prevPageBtn, "k_pg_forbid_css");
                $B.DomUtils.removeClass(this.$prevPageBtn.firstChild, "k_pg_forbid_css");
            }
        }
        if (this.$nextPageBtn) {
            if (page === this.pageCount) {
                $B.DomUtils.addClass(this.$nextPageBtn, "k_pg_forbid_css");
                $B.DomUtils.addClass(this.$nextPageBtn.firstChild, "k_pg_forbid_css");
            } else {
                $B.DomUtils.removeClass(this.$nextPageBtn, "k_pg_forbid_css");
                $B.DomUtils.removeClass(this.$nextPageBtn.firstChild, "k_pg_forbid_css");
            }
        }
        //console.log("go2page " + page);
        let num1 = parseInt(this.$NumSpan.firstChild.innerText);
        let num2 = parseInt(this.$NumSpan.lastChild.innerText);
        if (page < num1 || page > num2) {
            _createNumsFn(this);
            ret = true;
        }
        num1 = parseInt(this.$NumSpan.firstChild.innerText);
        num2 = parseInt(this.$NumSpan.lastChild.innerText);
        //前一批次，后一批次ui更新
        if (this.$prevPagesBtn) {
            if (num1 === 1) { //位于第一页批次
                $B.DomUtils.addClass(this.$prevPagesBtn, "k_pg_forbid_css");
                $B.DomUtils.addClass(this.$prevPagesBtn.firstChild, "k_pg_forbid_css");
            } else {
                $B.DomUtils.removeClass(this.$prevPagesBtn, "k_pg_forbid_css");
                $B.DomUtils.removeClass(this.$prevPagesBtn.firstChild, "k_pg_forbid_css");
            }
        }
        if (this.$nextPagesBtn) {
            if ((num1 + this.opts.buttons) >= this.pageCount) {
                $B.DomUtils.addClass(this.$nextPagesBtn, "k_pg_forbid_css");
                $B.DomUtils.addClass(this.$nextPagesBtn.firstChild, "k_pg_forbid_css");
            } else {
                $B.DomUtils.removeClass(this.$nextPagesBtn, "k_pg_forbid_css");
                $B.DomUtils.removeClass(this.$nextPagesBtn.firstChild, "k_pg_forbid_css");
            }
        }
        return ret;
    }
    prevOrNextList(el, css) {
        if ($B.DomUtils.hasClass(el, css) || $B.DomUtils.hasClass(el.parentNode, css)) {
            let newPage;
            if (css === "k_pg_prev_pages") {//前一批次
                newPage = parseInt(this.$NumSpan.firstChild.innerText) - 1;
            } else {//后一批次
                newPage = parseInt(this.$NumSpan.lastChild.innerText) + 1;
            }
            this.go2page(newPage);
            return true;
        }
    }
    prevOrNext(el, css) {
        if ($B.DomUtils.hasClass(el, css) || $B.DomUtils.hasClass(el.parentNode, css)) {
            let page = css === "k_pg_prev_page" ? this.opts.page - 1 : this.opts.page + 1;
            if (page < 1 || page > this.pageCount) {
                return true;
            }
            if (this.currentBtn) {
                $B.DomUtils.removeClass(this.currentBtn, "k_pagination_actived");
            }
            if (css === "k_pg_prev_page" && this.currentBtn.previousSibling) {
                this.currentBtn = this.currentBtn.previousSibling;
                $B.DomUtils.addClass(this.currentBtn, "k_pagination_actived");
            } else if (this.currentBtn.nextSibling) {
                this.currentBtn = this.currentBtn.nextSibling;
                $B.DomUtils.addClass(this.currentBtn, "k_pagination_actived");
            }
            this.go2page(page);
            return true;
        }
    }
    bindEvents() {
        $B.DomUtils.bind(this.elObj, {
            click: (e) => {
                let el = e.target;
                if ($B.DomUtils.hasClass(el, "k_pagination_wrap") || $B.DomUtils.hasClass(el, "k_pg_forbid_css") 
                || $B.DomUtils.hasClass(el, "k_pg_summary") || $B.DomUtils.hasClass(el, "k_pg_total") || $B.DomUtils.hasClass(el, "k_pg_pagesize")) {
                    return false;
                }
                if ($B.DomUtils.hasClass(el.parentNode, "k_pg_pagesize")) {
                    this.dropdownList();
                    return false;
                }
                if (this.opts.onClick) {
                    setTimeout(() => {
                        this.opts.onClick(this.opts.page, this.opts.pageSize);
                    }, 10);
                }
                if (this.prevOrNext(el, "k_pg_prev_page")) {
                    return false;
                }
                if (this.prevOrNext(el, "k_pg_next_page")) {
                    return false;
                }
                if (this.prevOrNextList(el, "k_pg_prev_pages")) {
                    return false;
                }
                if (this.prevOrNextList(el, "k_pg_next_pages")) {
                    return false;
                }
                if (this.currentBtn) {
                    $B.DomUtils.removeClass(this.currentBtn, "k_pagination_actived");
                }
                if ($B.DomUtils.hasClass(el, "k_pg_first_page")) {
                    if (!this.go2page(1)) {
                        this.currentBtn = this.$NumSpan.firstChild;
                        $B.DomUtils.addClass(this.currentBtn, "k_pagination_actived");
                    }
                    return false;
                }
                if ($B.DomUtils.hasClass(el, "k_pg_last_page")) {
                    if (!this.go2page(this.pageCount)) {
                        let pg = this.pageCount + '';
                        let $ns = $B.DomUtils.children(this.$NumSpan);
                        for (let i = 0; i < $ns.length; i++) {
                            if ($ns[i].innerText === pg) {
                                this.currentBtn = $ns[i];
                                $B.DomUtils.addClass(this.currentBtn, "k_pagination_actived");
                                break;
                            }
                        }
                    }
                    return false;
                }
                if ($B.DomUtils.hasClass(el, "k_pagination_num")) {
                    $B.DomUtils.addClass(el, "k_pagination_actived");
                    this.currentBtn = el;
                    this.go2page(parseInt(el.innerText));
                }
                return false;
            }
        });
    }
    update(page,pageSize,total){
        if(this.$dropList){
            this.$dropList.style.display = "none";
        }
        this.opts.page = page;
        this.opts.pageSize = pageSize;
        this.opts.total = total;
        this.countPage();
        if(page > this.pageCount){ //恢复到第一页
            this.opts.page = 1;
        }
        _createNumsFn(this);
        this.go2page(page);
        this.$total[0].innerText = total;
        this.$pageSize.firstChild.innerText = pageSize;
    }
    destroy(isForce) {
        if(this.$dropList){
            this.$dropList.clearAll();
        }
        super.destroy();
    }
}
$B["Pagination"] = Pagination;