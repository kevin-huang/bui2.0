/**
 * Stack栈封装
 * ***/
class Stack extends $B.BaseControl {
    constructor(size) {
        super();
        this.record = new Array(size);
        this.size = size; //栈大小
        this.length = 0; //当前记录的长度
    }
    /**压入记录
    * 返回当前栈长度
    * **/
    push(data) {
        //栈满，移除底部一个记录
        if (this.length === this.size) {
            this.record.pop();
            this.length--;
        }
        var l = this.record.unshift(data);
        this.length++;
        if (this.length > this.size) {
            this.length = this.size;
        }
        return this.length;
    }
    /**记录出栈
     * 返回一个记录
     * **/
    pop() {
        this.length--;
        if (this.length < 0) {
            this.length = 0;
        }
        return this.record.shift();
    }
    /**清空栈**/
    clear() {
        this.record = new Array(this.size);
        this.length = 0;
    }
    /**循环记录**/
    each(fn) {
        var len = this.length;
        for (var i = 0; i < len; i++) {
            fn(this.record[i], i);
        }
    }
    /**
     * 获取当前栈大小
     * **/
     length() {
        return this.length;
    }
}
$B["Stack"] = Stack;