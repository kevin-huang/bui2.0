var defaultOpts = {
    heightStyle: 'auto',
    iconCls: 'fa-angle-double-right', //收起图标   
    iconPositon: 'right', //图标位置
    iconColor: '#666666', //图标颜色
    fontStyle: {
        "font-size": "14px",
        "font-weight": "bold",
        "color": "#666666"
    }, //标题字体颜色、大小配置
    activedStyle: {
        "background": "#71B9EA",
        "color": "#FFFFFF",
        "iconColor": '#FFFFFF'
    }, //激活状态样式            
    accordionStyle: {
        "background": "#F6F6F6",
        "border": "1px solid #C5C5C5"
    }, //手风琴边框、颜色定义
};
function _createItme(item, i) {
    var it = {
        header: $B.DomUtils.createEl("<h6 index='" + i + "' title='" + item.title + "' style='overflow:hidden' class='k_accordion_header k_box_size'><span>" + item.title + "</span></h6>"),
        body: $B.DomUtils.createEl("<div index='" + i + "' style='display:none' class='k_accordion_body k_box_size'><div style='height:100%;width:100%;padding:6px 8px;' class='k_box_size'></div></div>")
    };
    var elAttr={};
    var $bel = it.body.firstChild;
    if (item.url && item.url !== "") {        
        elAttr["_url"] = item.url;
        elAttr["_type"] = item.type ? item.type : 'iframe';
        $B.DomUtils.attribute($bel, elAttr);
        if(elAttr._type !== 'iframe'){
            $B.myScrollbar($bel, {});
        }else{
            let ifrEL = $B.getIframeEl("k_tabs_content_ifr");           
            $B.DomUtils.append($bel, ifrEL);
            $B.DomUtils.css($bel,{position:"relative"});
        }
    } else if (item.content) {
        let $e = $B.myScrollbar($bel, {});
        $B.DomUtils.append($e, item.content);
    }
    if (item.icon && item.icon !== "") {
        $B.DomUtils.prepend(it.header, "<i style='padding-right:4px' class='fa " + item.icon + "'></i>");
    }
    return it;
}
class Accordion extends $B.BaseControl {
    constructor(elObj, opts) {
        super();
        super.setElObj(elObj);
        this.opts = $B.extendObjectFn(true, {}, defaultOpts, opts);
        $B.DomUtils.addClass(this.elObj, "k_box_size k_accordion_main_wrap");
        this.hideProps = {
            "borderTopWidth": "hide",
            "borderBottomWidth": "hide",
            "paddingTop": "hide",
            "paddingBottom": "hide",
            "height": "hide"
        };
        this.showProps = {
            "borderTopWidth": "show",
            "borderBottomWidth": "show",
            "paddingTop": "show",
            "paddingBottom": "show",
            "height": "show"
        };
        let fragment = document.createDocumentFragment();
        let activedItem;
        let ml = "14";
        if (this.opts.iconPositon === "left") {
            ml = "0";
        }
        for (let i = 0, len = this.opts.items.length; i < len; ++i) {
            let item = this.opts.items[i];
            let it = _createItme(item, i);
            $B.DomUtils.css(it.header, this.opts.fontStyle);
            $B.DomUtils.css(it.header, this.opts.accordionStyle);
            $B.DomUtils.css(it.body, { "border": this.opts.accordionStyle.border });
            $B.DomUtils.css(it.body, { "border-top": "none" });
            $B.DomUtils.attribute(it.body, { "_title": item.title });
            if (this.opts.iconCls && this.opts.iconCls !== "") {
                let $icon = $B.DomUtils.createEl("<div style='height:100%;margin-left:" + ml + "px;margin-right:12px;float:" + this.opts.iconPositon + ";' class='k_accordion_header_icon'><i style='line-height:1.8em;' class='fa " + this.opts.iconCls + "'></i></div>");
                $B.DomUtils.append(it.header, $icon);
                $B.DomUtils.css(it.header, { color: this.opts.iconColor });
            }
            fragment.appendChild(it.header);
            fragment.appendChild(it.body);
            if (item.actived || i === 0) {
                activedItem = it.header;
            }
            this.bindEvents(it.header);
            if(this.opts.onCreate){
                let $b;
                if($B.DomUtils.attribute(it.body.firstChild,"_url") && $B.DomUtils.attribute(it.body.firstChild,"_type") === "iframe"){
                    $b = it.body.firstChild;
                }else{
                    $b = it.body.firstChild.firstChild;
                }
                setTimeout(()=>{
                    this.opts.onCreate.call($b,item.title);
                },1);
            }
        }
        this.elObj.appendChild(fragment);
        if(activedItem){
            $B.DomUtils.trigger(activedItem,"click");
        }
    }
    bindEvents($header){
        if(!this.onClick){
            this.onClick=(e)=>{
                let el = e.target;
                if(el.tagName !== "H6"){
                    while(el){
                        el = el.parentNode;
                        if(el.tagName === "H6"){
                            break;
                        }
                    }
                }
                if(this.activedItem ){
                    if(this.activedItem === el){
                        return false;
                    }
                    this._retoreStyle(this.activedItem);
                }
                this.activedItem = el;
                this._setActivedStyle();
                let childNodes = this.elObj.children;
                let allHeaderHeight = 0;
                let curShowEL;
                for(let i = 0 ;i < childNodes.length ;i++){
                    let $l = childNodes[i];
                    if($l.tagName === "H6"){
                        allHeaderHeight = allHeaderHeight + $B.DomUtils.outerHeight($l) + 2;
                    }else if($l.style.display !== "none"){
                        curShowEL = $l;
                    }
                }
                let allHeight = $B.DomUtils.outerHeight(this.elObj);
                let accHeight = allHeight - allHeaderHeight;                
                el.nextSibling.style.height = accHeight +"px";
                if(curShowEL){                    
                    $B.slideUp(curShowEL,200);                    
                }
                let $body = el.nextSibling;
                let title = $B.DomUtils.attribute($body,"_title");
                $B.slideDown($body,200,()=>{
                    let _s = $body.firstChild.firstChild;
                    $B.DomUtils.trigger(_s,"myscrollabr.mouseleave");
                    if($B.DomUtils.hasClass(_s,"k_scrollbar_wrap")){
                        _s = _s.firstChild;
                    }                   
                   this._exeLoad($body,title);
                   if(this.opts.onOpened){
                        setTimeout(()=>{                          
                            this.opts.onOpened.call(_s,title);
                        },1);
                   }
                });
            };
        }
        $B.DomUtils.click($header,this.onClick);
    }
    _exeLoad($body,title){
        let $wrap = $body.firstChild;
        let url = $B.DomUtils.attribute($wrap,"_url");        
        if(url){            
            let _this = this;
            let isFun = typeof   this.opts.onLoaded === "function";          
            let loading = $B.getLoadingEl();
            let type = $B.DomUtils.attribute($wrap,"_type") ;         
            if(type === "html"){
                let $c = $wrap.firstChild.firstChild;
                if(!$c.firstChild){
                    $c.appendChild(loading);
                    $B.htmlLoad({
                        url: url,
                        success: function (res) {
                            $c.innerHTML = res;
                            if (isFun) {
                                _this.opts.onLoaded.call($c, {},title,'html');
                            }
                            $B.bindInputClear($c);
                        },
                        complete: function () {
                            $B.removeLoading(loading, () => {
                                loading = undefined;
                            });
                        }
                    }, $c);
                }
            }else if(type === "json"){
                let $c = $wrap.firstChild.firstChild;
                if(!$c.firstChild){
                    $c.appendChild(loading);
                    let method = (url.indexOf(".data") > 0 || url.indexOf(".json") > 0) ? "GET" : "POST";
                    $B.request({
                        dataType: 'json',
                        url: url,
                        type: method,
                        onErrorEval: true,
                        ok: function (message, data) {
                            if (isFun) {
                                _this.opts.onLoaded.call( $c, data,title,'json');
                            }
                        },
                        final: function (res) {
                            try {
                                $B.removeLoading(loading, () => {
                                    loading = undefined;
                                });
                            } catch (ex) {
                            }
                        }
                    });
                }
            }else {
                let ifr = $wrap.firstChild;               
                if(!$B.DomUtils.attribute(ifr,"loaded")){
                    $wrap.appendChild(loading);
                    $B.DomUtils.onload(ifr, () => {
                        $B.DomUtils.attribute(ifr,{"loaded":1});
                        try {
                            $B.removeLoading(loading, () => {
                                loading = undefined;
                            });
                        } catch (ex) {
                        }
                        if (isFun) {
                            _this.opts.onLoaded.call( ifr, "",title,'iframe');
                        }
                    });
                    ifr.src = url;
                }               
            }
        }
    }
    _retoreStyle($it){        
        var $txt = $B.DomUtils.children($it,"span")[0];
        $B.DomUtils.removeClass($it,"k_accordion_header_actived");
        var $icon = $B.DomUtils.children($it,".k_accordion_header_icon")[0].firstChild;
        $B.animate($icon, { "rotateZ": "0deg" }, { duration: 300 });
        $B.DomUtils.css($it,this.opts.fontStyle);
        $B.DomUtils.css($it,{"background": this.opts.accordionStyle.background});
        let $ficon = $B.DomUtils.children($it,"i");
        $B.DomUtils.css($ficon,{"color": this.opts.iconColor});
        $B.DomUtils.css($icon,{"color": this.opts.iconColor});
        $B.DomUtils.css($txt,{"color": this.opts.iconColor});
    }
    _setActivedStyle(){
        var $it = this.activedItem;
        var $txt = $B.DomUtils.children($it,"span")[0];
        var $icon = $B.DomUtils.children($it,".k_accordion_header_icon")[0].firstChild;
        $B.DomUtils.addClass($it,"k_accordion_header_actived");
        $B.animate($icon, { "rotateZ": "90deg" }, { duration: 300 });
        $B.DomUtils.css($it,this.opts.activedStyle);
        let $ficon = $B.DomUtils.children($it,"i"); 
        $B.DomUtils.css($ficon,{color:this.opts.activedStyle.iconColor});
        $B.DomUtils.css($txt,{color:this.opts.activedStyle.iconColor});        
        $B.DomUtils.css($icon,{color:this.opts.activedStyle.iconColor});
        var $body = $it.nextSibling;
        $B.DomUtils.css($body,this.opts.accordionStyle);
    }
}
$B["Accordion"] = Accordion;