/*! BUI - v2.0.0 - 2022-03-13 09:03:17 
Copyright (c): kevin.huang  https://gitee.com/kevin-huang/bui2.0.git (qq:757723114) 
Released under MIT License*/
/*! BUI - v2.0.0 - 2022-03-13 09:03:17 
Copyright (c): kevin.huang  https://gitee.com/kevin-huang/bui2.0.git (qq:757723114) 
Released under MIT License*/
/*! VelocityJS.org (1.5.2). (C) 2014 Julian Shapiro. MIT @license: en.wikipedia.org/wiki/MIT_License */
/*! VelocityJS.org jQuery Shim (1.0.1). (C) 2014 The jQuery Foundation. MIT @license: en.wikipedia.org/wiki/MIT_License. */
!function(a){"use strict";function b(a){var b=a.length,d=c.type(a);return"function"!==d&&!c.isWindow(a)&&(!(1!==a.nodeType||!b)||("array"===d||0===b||"number"==typeof b&&b>0&&b-1 in a))}if(!a.jQuery){var c=function(a,b){return new c.fn.init(a,b)};c.isWindow=function(a){return a&&a===a.window},c.type=function(a){return a?"object"==typeof a||"function"==typeof a?e[g.call(a)]||"object":typeof a:a+""},c.isArray=Array.isArray||function(a){return"array"===c.type(a)},c.isPlainObject=function(a){var b;if(!a||"object"!==c.type(a)||a.nodeType||c.isWindow(a))return!1;try{if(a.constructor&&!f.call(a,"constructor")&&!f.call(a.constructor.prototype,"isPrototypeOf"))return!1}catch(d){return!1}for(b in a);return b===undefined||f.call(a,b)},c.each=function(a,c,d){var e=0,f=a.length,g=b(a);if(d){if(g)for(;e<f&&!1!==c.apply(a[e],d);e++);else for(e in a)if(a.hasOwnProperty(e)&&!1===c.apply(a[e],d))break}else if(g)for(;e<f&&!1!==c.call(a[e],e,a[e]);e++);else for(e in a)if(a.hasOwnProperty(e)&&!1===c.call(a[e],e,a[e]))break;return a},c.data=function(a,b,e){if(e===undefined){var f=a[c.expando],g=f&&d[f];if(b===undefined)return g;if(g&&b in g)return g[b]}else if(b!==undefined){var h=a[c.expando]||(a[c.expando]=++c.uuid);return d[h]=d[h]||{},d[h][b]=e,e}},c.removeData=function(a,b){var e=a[c.expando],f=e&&d[e];f&&(b?c.each(b,function(a,b){delete f[b]}):delete d[e])},c.extend=function(){var a,b,d,e,f,g,h=arguments[0]||{},i=1,j=arguments.length,k=!1;for("boolean"==typeof h&&(k=h,h=arguments[i]||{},i++),"object"!=typeof h&&"function"!==c.type(h)&&(h={}),i===j&&(h=this,i--);i<j;i++)if(f=arguments[i])for(e in f)f.hasOwnProperty(e)&&(a=h[e],d=f[e],h!==d&&(k&&d&&(c.isPlainObject(d)||(b=c.isArray(d)))?(b?(b=!1,g=a&&c.isArray(a)?a:[]):g=a&&c.isPlainObject(a)?a:{},h[e]=c.extend(k,g,d)):d!==undefined&&(h[e]=d)));return h},c.queue=function(a,d,e){if(a){d=(d||"fx")+"queue";var f=c.data(a,d);return e?(!f||c.isArray(e)?f=c.data(a,d,function(a,c){var d=c||[];return a&&(b(Object(a))?function(a,b){for(var c=+b.length,d=0,e=a.length;d<c;)a[e++]=b[d++];if(c!==c)for(;b[d]!==undefined;)a[e++]=b[d++];a.length=e}(d,"string"==typeof a?[a]:a):[].push.call(d,a)),d}(e)):f.push(e),f):f||[]}},c.dequeue=function(a,b){c.each(a.nodeType?[a]:a,function(a,d){b=b||"fx";var e=c.queue(d,b),f=e.shift();"inprogress"===f&&(f=e.shift()),f&&("fx"===b&&e.unshift("inprogress"),f.call(d,function(){c.dequeue(d,b)}))})},c.fn=c.prototype={init:function(a){if(a.nodeType)return this[0]=a,this;throw new Error("Not a DOM node.")},offset:function(){var b=this[0].getBoundingClientRect?this[0].getBoundingClientRect():{top:0,left:0};return{top:b.top+(a.pageYOffset||document.scrollTop||0)-(document.clientTop||0),left:b.left+(a.pageXOffset||document.scrollLeft||0)-(document.clientLeft||0)}},position:function(){var a=this[0],b=function(a){for(var b=a.offsetParent;b&&"html"!==b.nodeName.toLowerCase()&&b.style&&"static"===b.style.position.toLowerCase();)b=b.offsetParent;return b||document}(a),d=this.offset(),e=/^(?:body|html)$/i.test(b.nodeName)?{top:0,left:0}:c(b).offset();return d.top-=parseFloat(a.style.marginTop)||0,d.left-=parseFloat(a.style.marginLeft)||0,b.style&&(e.top+=parseFloat(b.style.borderTopWidth)||0,e.left+=parseFloat(b.style.borderLeftWidth)||0),{top:d.top-e.top,left:d.left-e.left}}};var d={};c.expando="velocity"+(new Date).getTime(),c.uuid=0;for(var e={},f=e.hasOwnProperty,g=e.toString,h="Boolean Number String Function Array Date RegExp Object Error".split(" "),i=0;i<h.length;i++)e["[object "+h[i]+"]"]=h[i].toLowerCase();c.fn.init.prototype=c.fn,a.Velocity={Utilities:c}}}(window),function(a){"use strict";"object"==typeof module&&"object"==typeof module.exports?module.exports=a():"function"==typeof define&&define.amd?define(a):a()}(function(){"use strict";return function(a,b,c,d){function e(a){for(var b=-1,c=a?a.length:0,d=[];++b<c;){var e=a[b];e&&d.push(e)}return d}function f(a){return u.isWrapped(a)?a=s.call(a):u.isNode(a)&&(a=[a]),a}function g(a){var b=o.data(a,"velocity");return null===b?d:b}function h(a,b){var c=g(a);c&&c.delayTimer&&!c.delayPaused&&(c.delayRemaining=c.delay-b+c.delayBegin,c.delayPaused=!0,clearTimeout(c.delayTimer.setTimeout))}function i(a,b){var c=g(a);c&&c.delayTimer&&c.delayPaused&&(c.delayPaused=!1,c.delayTimer.setTimeout=setTimeout(c.delayTimer.next,c.delayRemaining))}function j(a){return function(b){return Math.round(b*a)*(1/a)}}function k(a,c,d,e){function f(a,b){return 1-3*b+3*a}function g(a,b){return 3*b-6*a}function h(a){return 3*a}function i(a,b,c){return((f(b,c)*a+g(b,c))*a+h(b))*a}function j(a,b,c){return 3*f(b,c)*a*a+2*g(b,c)*a+h(b)}function k(b,c){for(var e=0;e<p;++e){var f=j(c,a,d);if(0===f)return c;c-=(i(c,a,d)-b)/f}return c}function l(){for(var b=0;b<t;++b)x[b]=i(b*u,a,d)}function m(b,c,e){var f,g,h=0;do{g=c+(e-c)/2,f=i(g,a,d)-b,f>0?e=g:c=g}while(Math.abs(f)>r&&++h<s);return g}function n(b){for(var c=0,e=1,f=t-1;e!==f&&x[e]<=b;++e)c+=u;--e;var g=(b-x[e])/(x[e+1]-x[e]),h=c+g*u,i=j(h,a,d);return i>=q?k(b,h):0===i?h:m(b,c,c+u)}function o(){y=!0,a===c&&d===e||l()}var p=4,q=.001,r=1e-7,s=10,t=11,u=1/(t-1),v="Float32Array"in b;if(4!==arguments.length)return!1;for(var w=0;w<4;++w)if("number"!=typeof arguments[w]||isNaN(arguments[w])||!isFinite(arguments[w]))return!1;a=Math.min(a,1),d=Math.min(d,1),a=Math.max(a,0),d=Math.max(d,0);var x=v?new Float32Array(t):new Array(t),y=!1,z=function(b){return y||o(),a===c&&d===e?b:0===b?0:1===b?1:i(n(b),c,e)};z.getControlPoints=function(){return[{x:a,y:c},{x:d,y:e}]};var A="generateBezier("+[a,c,d,e]+")";return z.toString=function(){return A},z}function l(a,b){var c=a;return u.isString(a)?y.Easings[a]||(c=!1):c=u.isArray(a)&&1===a.length?j.apply(null,a):u.isArray(a)&&2===a.length?z.apply(null,a.concat([b])):!(!u.isArray(a)||4!==a.length)&&k.apply(null,a),!1===c&&(c=y.Easings[y.defaults.easing]?y.defaults.easing:x),c}function m(a){if(a){var b=y.timestamp&&!0!==a?a:r.now(),c=y.State.calls.length;c>1e4&&(y.State.calls=e(y.State.calls),c=y.State.calls.length);for(var f=0;f<c;f++)if(y.State.calls[f]){var h=y.State.calls[f],i=h[0],j=h[2],k=h[3],l=!k,q=null,s=h[5],t=h[6];if(k||(k=y.State.calls[f][3]=b-16),s){if(!0!==s.resume)continue;k=h[3]=Math.round(b-t-16),h[5]=null}t=h[6]=b-k;for(var v=Math.min(t/j.duration,1),w=0,x=i.length;w<x;w++){var z=i[w],B=z.element;if(g(B)){var D=!1;if(j.display!==d&&null!==j.display&&"none"!==j.display){if("flex"===j.display){var E=["-webkit-box","-moz-box","-ms-flexbox","-webkit-flex"];o.each(E,function(a,b){A.setPropertyValue(B,"display",b)})}A.setPropertyValue(B,"display",j.display)}j.visibility!==d&&"hidden"!==j.visibility&&A.setPropertyValue(B,"visibility",j.visibility);for(var F in z)if(z.hasOwnProperty(F)&&"element"!==F){var G,H=z[F],I=u.isString(H.easing)?y.Easings[H.easing]:H.easing;if(u.isString(H.pattern)){var J=1===v?function(a,b,c){var d=H.endValue[b];return c?Math.round(d):d}:function(a,b,c){var d=H.startValue[b],e=H.endValue[b]-d,f=d+e*I(v,j,e);return c?Math.round(f):f};G=H.pattern.replace(/{(\d+)(!)?}/g,J)}else if(1===v)G=H.endValue;else{var K=H.endValue-H.startValue;G=H.startValue+K*I(v,j,K)}if(!l&&G===H.currentValue)continue;if(H.currentValue=G,"tween"===F)q=G;else{var L;if(A.Hooks.registered[F]){L=A.Hooks.getRoot(F);var M=g(B).rootPropertyValueCache[L];M&&(H.rootPropertyValue=M)}var N=A.setPropertyValue(B,F,H.currentValue+(p<9&&0===parseFloat(G)?"":H.unitType),H.rootPropertyValue,H.scrollData);A.Hooks.registered[F]&&(A.Normalizations.registered[L]?g(B).rootPropertyValueCache[L]=A.Normalizations.registered[L]("extract",null,N[1]):g(B).rootPropertyValueCache[L]=N[1]),"transform"===N[0]&&(D=!0)}}j.mobileHA&&g(B).transformCache.translate3d===d&&(g(B).transformCache.translate3d="(0px, 0px, 0px)",D=!0),D&&A.flushTransformCache(B)}}j.display!==d&&"none"!==j.display&&(y.State.calls[f][2].display=!1),j.visibility!==d&&"hidden"!==j.visibility&&(y.State.calls[f][2].visibility=!1),j.progress&&j.progress.call(h[1],h[1],v,Math.max(0,k+j.duration-b),k,q),1===v&&n(f)}}y.State.isTicking&&C(m)}function n(a,b){if(!y.State.calls[a])return!1;for(var c=y.State.calls[a][0],e=y.State.calls[a][1],f=y.State.calls[a][2],h=y.State.calls[a][4],i=!1,j=0,k=c.length;j<k;j++){var l=c[j].element;b||f.loop||("none"===f.display&&A.setPropertyValue(l,"display",f.display),"hidden"===f.visibility&&A.setPropertyValue(l,"visibility",f.visibility));var m=g(l);if(!0!==f.loop&&(o.queue(l)[1]===d||!/\.velocityQueueEntryFlag/i.test(o.queue(l)[1]))&&m){m.isAnimating=!1,m.rootPropertyValueCache={};var n=!1;o.each(A.Lists.transforms3D,function(a,b){var c=/^scale/.test(b)?1:0,e=m.transformCache[b];m.transformCache[b]!==d&&new RegExp("^\\("+c+"[^.]").test(e)&&(n=!0,delete m.transformCache[b])}),f.mobileHA&&(n=!0,delete m.transformCache.translate3d),n&&A.flushTransformCache(l),A.Values.removeClass(l,"velocity-animating")}if(!b&&f.complete&&!f.loop&&j===k-1)try{f.complete.call(e,e)}catch(r){setTimeout(function(){throw r},1)}h&&!0!==f.loop&&h(e),m&&!0===f.loop&&!b&&(o.each(m.tweensContainer,function(a,b){if(/^rotate/.test(a)&&(parseFloat(b.startValue)-parseFloat(b.endValue))%360==0){var c=b.startValue;b.startValue=b.endValue,b.endValue=c}/^backgroundPosition/.test(a)&&100===parseFloat(b.endValue)&&"%"===b.unitType&&(b.endValue=0,b.startValue=100)}),y(l,"reverse",{loop:!0,delay:f.delay})),!1!==f.queue&&o.dequeue(l,f.queue)}y.State.calls[a]=!1;for(var p=0,q=y.State.calls.length;p<q;p++)if(!1!==y.State.calls[p]){i=!0;break}!1===i&&(y.State.isTicking=!1,delete y.State.calls,y.State.calls=[])}var o,p=function(){if(c.documentMode)return c.documentMode;for(var a=7;a>4;a--){var b=c.createElement("div");if(b.innerHTML="\x3c!--[if IE "+a+"]><span></span><![endif]--\x3e",b.getElementsByTagName("span").length)return b=null,a}return d}(),q=function(){var a=0;return b.webkitRequestAnimationFrame||b.mozRequestAnimationFrame||function(b){var c,d=(new Date).getTime();return c=Math.max(0,16-(d-a)),a=d+c,setTimeout(function(){b(d+c)},c)}}(),r=function(){var a=b.performance||{};if("function"!=typeof a.now){var c=a.timing&&a.timing.navigationStart?a.timing.navigationStart:(new Date).getTime();a.now=function(){return(new Date).getTime()-c}}return a}(),s=function(){var a=Array.prototype.slice;try{return a.call(c.documentElement),a}catch(b){return function(b,c){var d=this.length;if("number"!=typeof b&&(b=0),"number"!=typeof c&&(c=d),this.slice)return a.call(this,b,c);var e,f=[],g=b>=0?b:Math.max(0,d+b),h=c<0?d+c:Math.min(c,d),i=h-g;if(i>0)if(f=new Array(i),this.charAt)for(e=0;e<i;e++)f[e]=this.charAt(g+e);else for(e=0;e<i;e++)f[e]=this[g+e];return f}}}(),t=function(){return Array.prototype.includes?function(a,b){return a.includes(b)}:Array.prototype.indexOf?function(a,b){return a.indexOf(b)>=0}:function(a,b){for(var c=0;c<a.length;c++)if(a[c]===b)return!0;return!1}},u={isNumber:function(a){return"number"==typeof a},isString:function(a){return"string"==typeof a},isArray:Array.isArray||function(a){return"[object Array]"===Object.prototype.toString.call(a)},isFunction:function(a){return"[object Function]"===Object.prototype.toString.call(a)},isNode:function(a){return a&&a.nodeType},isWrapped:function(a){return a&&a!==b&&u.isNumber(a.length)&&!u.isString(a)&&!u.isFunction(a)&&!u.isNode(a)&&(0===a.length||u.isNode(a[0]))},isSVG:function(a){return b.SVGElement&&a instanceof b.SVGElement},isEmptyObject:function(a){for(var b in a)if(a.hasOwnProperty(b))return!1;return!0}},v=!1;if(a.fn&&a.fn.jquery?(o=a,v=!0):o=b.Velocity.Utilities,p<=8&&!v)throw new Error("Velocity: IE8 and below require jQuery to be loaded before Velocity.");if(p<=7)return void(jQuery.fn.velocity=jQuery.fn.animate);var w=400,x="swing",y={State:{isMobile:/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(b.navigator.userAgent),isAndroid:/Android/i.test(b.navigator.userAgent),isGingerbread:/Android 2\.3\.[3-7]/i.test(b.navigator.userAgent),isChrome:b.chrome,isFirefox:/Firefox/i.test(b.navigator.userAgent),prefixElement:c.createElement("div"),prefixMatches:{},scrollAnchor:null,scrollPropertyLeft:null,scrollPropertyTop:null,isTicking:!1,calls:[],delayedElements:{count:0}},CSS:{},Utilities:o,Redirects:{},Easings:{},Promise:b.Promise,defaults:{queue:"",duration:w,easing:x,begin:d,complete:d,progress:d,display:d,visibility:d,loop:!1,delay:!1,mobileHA:!0,_cacheValues:!0,promiseRejectEmpty:!0},init:function(a){o.data(a,"velocity",{isSVG:u.isSVG(a),isAnimating:!1,computedStyle:null,tweensContainer:null,rootPropertyValueCache:{},transformCache:{}})},hook:null,mock:!1,version:{major:1,minor:5,patch:2},debug:!1,timestamp:!0,pauseAll:function(a){var b=(new Date).getTime();o.each(y.State.calls,function(b,c){if(c){if(a!==d&&(c[2].queue!==a||!1===c[2].queue))return!0;c[5]={resume:!1}}}),o.each(y.State.delayedElements,function(a,c){c&&h(c,b)})},resumeAll:function(a){var b=(new Date).getTime();o.each(y.State.calls,function(b,c){if(c){if(a!==d&&(c[2].queue!==a||!1===c[2].queue))return!0;c[5]&&(c[5].resume=!0)}}),o.each(y.State.delayedElements,function(a,c){c&&i(c,b)})}};b.pageYOffset!==d?(y.State.scrollAnchor=b,y.State.scrollPropertyLeft="pageXOffset",y.State.scrollPropertyTop="pageYOffset"):(y.State.scrollAnchor=c.documentElement||c.body.parentNode||c.body,y.State.scrollPropertyLeft="scrollLeft",y.State.scrollPropertyTop="scrollTop");var z=function(){function a(a){return-a.tension*a.x-a.friction*a.v}function b(b,c,d){var e={x:b.x+d.dx*c,v:b.v+d.dv*c,tension:b.tension,friction:b.friction};return{dx:e.v,dv:a(e)}}function c(c,d){var e={dx:c.v,dv:a(c)},f=b(c,.5*d,e),g=b(c,.5*d,f),h=b(c,d,g),i=1/6*(e.dx+2*(f.dx+g.dx)+h.dx),j=1/6*(e.dv+2*(f.dv+g.dv)+h.dv);return c.x=c.x+i*d,c.v=c.v+j*d,c}return function d(a,b,e){var f,g,h,i={x:-1,v:0,tension:null,friction:null},j=[0],k=0;for(a=parseFloat(a)||500,b=parseFloat(b)||20,e=e||null,i.tension=a,i.friction=b,f=null!==e,f?(k=d(a,b),g=k/e*.016):g=.016;;)if(h=c(h||i,g),j.push(1+h.x),k+=16,!(Math.abs(h.x)>1e-4&&Math.abs(h.v)>1e-4))break;return f?function(a){return j[a*(j.length-1)|0]}:k}}();y.Easings={linear:function(a){return a},swing:function(a){return.5-Math.cos(a*Math.PI)/2},spring:function(a){return 1-Math.cos(4.5*a*Math.PI)*Math.exp(6*-a)}},o.each([["ease",[.25,.1,.25,1]],["ease-in",[.42,0,1,1]],["ease-out",[0,0,.58,1]],["ease-in-out",[.42,0,.58,1]],["easeInSine",[.47,0,.745,.715]],["easeOutSine",[.39,.575,.565,1]],["easeInOutSine",[.445,.05,.55,.95]],["easeInQuad",[.55,.085,.68,.53]],["easeOutQuad",[.25,.46,.45,.94]],["easeInOutQuad",[.455,.03,.515,.955]],["easeInCubic",[.55,.055,.675,.19]],["easeOutCubic",[.215,.61,.355,1]],["easeInOutCubic",[.645,.045,.355,1]],["easeInQuart",[.895,.03,.685,.22]],["easeOutQuart",[.165,.84,.44,1]],["easeInOutQuart",[.77,0,.175,1]],["easeInQuint",[.755,.05,.855,.06]],["easeOutQuint",[.23,1,.32,1]],["easeInOutQuint",[.86,0,.07,1]],["easeInExpo",[.95,.05,.795,.035]],["easeOutExpo",[.19,1,.22,1]],["easeInOutExpo",[1,0,0,1]],["easeInCirc",[.6,.04,.98,.335]],["easeOutCirc",[.075,.82,.165,1]],["easeInOutCirc",[.785,.135,.15,.86]]],function(a,b){y.Easings[b[0]]=k.apply(null,b[1])});var A=y.CSS={RegEx:{isHex:/^#([A-f\d]{3}){1,2}$/i,valueUnwrap:/^[A-z]+\((.*)\)$/i,wrappedValueAlreadyExtracted:/[0-9.]+ [0-9.]+ [0-9.]+( [0-9.]+)?/,valueSplit:/([A-z]+\(.+\))|(([A-z0-9#-.]+?)(?=\s|$))/gi},Lists:{colors:["fill","stroke","stopColor","color","backgroundColor","borderColor","borderTopColor","borderRightColor","borderBottomColor","borderLeftColor","outlineColor"],transformsBase:["translateX","translateY","scale","scaleX","scaleY","skewX","skewY","rotateZ"],transforms3D:["transformPerspective","translateZ","scaleZ","rotateX","rotateY"],units:["%","em","ex","ch","rem","vw","vh","vmin","vmax","cm","mm","Q","in","pc","pt","px","deg","grad","rad","turn","s","ms"],colorNames:{aliceblue:"240,248,255",antiquewhite:"250,235,215",aquamarine:"127,255,212",aqua:"0,255,255",azure:"240,255,255",beige:"245,245,220",bisque:"255,228,196",black:"0,0,0",blanchedalmond:"255,235,205",blueviolet:"138,43,226",blue:"0,0,255",brown:"165,42,42",burlywood:"222,184,135",cadetblue:"95,158,160",chartreuse:"127,255,0",chocolate:"210,105,30",coral:"255,127,80",cornflowerblue:"100,149,237",cornsilk:"255,248,220",crimson:"220,20,60",cyan:"0,255,255",darkblue:"0,0,139",darkcyan:"0,139,139",darkgoldenrod:"184,134,11",darkgray:"169,169,169",darkgrey:"169,169,169",darkgreen:"0,100,0",darkkhaki:"189,183,107",darkmagenta:"139,0,139",darkolivegreen:"85,107,47",darkorange:"255,140,0",darkorchid:"153,50,204",darkred:"139,0,0",darksalmon:"233,150,122",darkseagreen:"143,188,143",darkslateblue:"72,61,139",darkslategray:"47,79,79",darkturquoise:"0,206,209",darkviolet:"148,0,211",deeppink:"255,20,147",deepskyblue:"0,191,255",dimgray:"105,105,105",dimgrey:"105,105,105",dodgerblue:"30,144,255",firebrick:"178,34,34",floralwhite:"255,250,240",forestgreen:"34,139,34",fuchsia:"255,0,255",gainsboro:"220,220,220",ghostwhite:"248,248,255",gold:"255,215,0",goldenrod:"218,165,32",gray:"128,128,128",grey:"128,128,128",greenyellow:"173,255,47",green:"0,128,0",honeydew:"240,255,240",hotpink:"255,105,180",indianred:"205,92,92",indigo:"75,0,130",ivory:"255,255,240",khaki:"240,230,140",lavenderblush:"255,240,245",lavender:"230,230,250",lawngreen:"124,252,0",lemonchiffon:"255,250,205",lightblue:"173,216,230",lightcoral:"240,128,128",lightcyan:"224,255,255",lightgoldenrodyellow:"250,250,210",lightgray:"211,211,211",lightgrey:"211,211,211",lightgreen:"144,238,144",lightpink:"255,182,193",lightsalmon:"255,160,122",lightseagreen:"32,178,170",lightskyblue:"135,206,250",lightslategray:"119,136,153",lightsteelblue:"176,196,222",lightyellow:"255,255,224",limegreen:"50,205,50",lime:"0,255,0",linen:"250,240,230",magenta:"255,0,255",maroon:"128,0,0",mediumaquamarine:"102,205,170",mediumblue:"0,0,205",mediumorchid:"186,85,211",mediumpurple:"147,112,219",mediumseagreen:"60,179,113",mediumslateblue:"123,104,238",mediumspringgreen:"0,250,154",mediumturquoise:"72,209,204",mediumvioletred:"199,21,133",midnightblue:"25,25,112",mintcream:"245,255,250",mistyrose:"255,228,225",moccasin:"255,228,181",navajowhite:"255,222,173",navy:"0,0,128",oldlace:"253,245,230",olivedrab:"107,142,35",olive:"128,128,0",orangered:"255,69,0",orange:"255,165,0",orchid:"218,112,214",palegoldenrod:"238,232,170",palegreen:"152,251,152",paleturquoise:"175,238,238",palevioletred:"219,112,147",papayawhip:"255,239,213",peachpuff:"255,218,185",peru:"205,133,63",pink:"255,192,203",plum:"221,160,221",powderblue:"176,224,230",purple:"128,0,128",red:"255,0,0",rosybrown:"188,143,143",royalblue:"65,105,225",saddlebrown:"139,69,19",salmon:"250,128,114",sandybrown:"244,164,96",seagreen:"46,139,87",seashell:"255,245,238",sienna:"160,82,45",silver:"192,192,192",skyblue:"135,206,235",slateblue:"106,90,205",slategray:"112,128,144",snow:"255,250,250",springgreen:"0,255,127",steelblue:"70,130,180",tan:"210,180,140",teal:"0,128,128",thistle:"216,191,216",tomato:"255,99,71",turquoise:"64,224,208",violet:"238,130,238",wheat:"245,222,179",whitesmoke:"245,245,245",white:"255,255,255",yellowgreen:"154,205,50",yellow:"255,255,0"}},Hooks:{templates:{textShadow:["Color X Y Blur","black 0px 0px 0px"],boxShadow:["Color X Y Blur Spread","black 0px 0px 0px 0px"],clip:["Top Right Bottom Left","0px 0px 0px 0px"],backgroundPosition:["X Y","0% 0%"],transformOrigin:["X Y Z","50% 50% 0px"],perspectiveOrigin:["X Y","50% 50%"]},registered:{},register:function(){for(var a=0;a<A.Lists.colors.length;a++){var b="color"===A.Lists.colors[a]?"0 0 0 1":"255 255 255 1";A.Hooks.templates[A.Lists.colors[a]]=["Red Green Blue Alpha",b]}var c,d,e;if(p)for(c in A.Hooks.templates)if(A.Hooks.templates.hasOwnProperty(c)){d=A.Hooks.templates[c],e=d[0].split(" ");var f=d[1].match(A.RegEx.valueSplit);"Color"===e[0]&&(e.push(e.shift()),f.push(f.shift()),A.Hooks.templates[c]=[e.join(" "),f.join(" ")])}for(c in A.Hooks.templates)if(A.Hooks.templates.hasOwnProperty(c)){d=A.Hooks.templates[c],e=d[0].split(" ");for(var g in e)if(e.hasOwnProperty(g)){var h=c+e[g],i=g;A.Hooks.registered[h]=[c,i]}}},getRoot:function(a){var b=A.Hooks.registered[a];return b?b[0]:a},getUnit:function(a,b){var c=(a.substr(b||0,5).match(/^[a-z%]+/)||[])[0]||"";return c&&t(A.Lists.units,c)?c:""},fixColors:function(a){return a.replace(/(rgba?\(\s*)?(\b[a-z]+\b)/g,function(a,b,c){return A.Lists.colorNames.hasOwnProperty(c)?(b||"rgba(")+A.Lists.colorNames[c]+(b?"":",1)"):b+c})},cleanRootPropertyValue:function(a,b){return A.RegEx.valueUnwrap.test(b)&&(b=b.match(A.RegEx.valueUnwrap)[1]),A.Values.isCSSNullValue(b)&&(b=A.Hooks.templates[a][1]),b},extractValue:function(a,b){var c=A.Hooks.registered[a];if(c){var d=c[0],e=c[1];return b=A.Hooks.cleanRootPropertyValue(d,b),b.toString().match(A.RegEx.valueSplit)[e]}return b},injectValue:function(a,b,c){var d=A.Hooks.registered[a];if(d){var e,f=d[0],g=d[1];return c=A.Hooks.cleanRootPropertyValue(f,c),e=c.toString().match(A.RegEx.valueSplit),e[g]=b,e.join(" ")}return c}},Normalizations:{registered:{clip:function(a,b,c){switch(a){case"name":return"clip";case"extract":var d;return A.RegEx.wrappedValueAlreadyExtracted.test(c)?d=c:(d=c.toString().match(A.RegEx.valueUnwrap),d=d?d[1].replace(/,(\s+)?/g," "):c),d;case"inject":return"rect("+c+")"}},blur:function(a,b,c){switch(a){case"name":return y.State.isFirefox?"filter":"-webkit-filter";case"extract":var d=parseFloat(c);if(!d&&0!==d){var e=c.toString().match(/blur\(([0-9]+[A-z]+)\)/i);d=e?e[1]:0}return d;case"inject":return parseFloat(c)?"blur("+c+")":"none"}},opacity:function(a,b,c){if(p<=8)switch(a){case"name":return"filter";case"extract":var d=c.toString().match(/alpha\(opacity=(.*)\)/i);return c=d?d[1]/100:1;case"inject":return b.style.zoom=1,parseFloat(c)>=1?"":"alpha(opacity="+parseInt(100*parseFloat(c),10)+")"}else switch(a){case"name":return"opacity";case"extract":case"inject":return c}}},register:function(){function a(a,b,c){if("border-box"===A.getPropertyValue(b,"boxSizing").toString().toLowerCase()===(c||!1)){var d,e,f=0,g="width"===a?["Left","Right"]:["Top","Bottom"],h=["padding"+g[0],"padding"+g[1],"border"+g[0]+"Width","border"+g[1]+"Width"];for(d=0;d<h.length;d++)e=parseFloat(A.getPropertyValue(b,h[d])),isNaN(e)||(f+=e);return c?-f:f}return 0}function b(b,c){return function(d,e,f){switch(d){case"name":return b;case"extract":return parseFloat(f)+a(b,e,c);case"inject":return parseFloat(f)-a(b,e,c)+"px"}}}p&&!(p>9)||y.State.isGingerbread||(A.Lists.transformsBase=A.Lists.transformsBase.concat(A.Lists.transforms3D));for(var c=0;c<A.Lists.transformsBase.length;c++)!function(){var a=A.Lists.transformsBase[c];A.Normalizations.registered[a]=function(b,c,e){switch(b){case"name":return"transform";case"extract":return g(c)===d||g(c).transformCache[a]===d?/^scale/i.test(a)?1:0:g(c).transformCache[a].replace(/[()]/g,"");case"inject":var f=!1;switch(a.substr(0,a.length-1)){case"translate":f=!/(%|px|em|rem|vw|vh|\d)$/i.test(e);break;case"scal":case"scale":y.State.isAndroid&&g(c).transformCache[a]===d&&e<1&&(e=1),f=!/(\d)$/i.test(e);break;case"skew":case"rotate":f=!/(deg|\d)$/i.test(e)}return f||(g(c).transformCache[a]="("+e+")"),g(c).transformCache[a]}}}();for(var e=0;e<A.Lists.colors.length;e++)!function(){var a=A.Lists.colors[e];A.Normalizations.registered[a]=function(b,c,e){switch(b){case"name":return a;case"extract":var f;if(A.RegEx.wrappedValueAlreadyExtracted.test(e))f=e;else{var g,h={black:"rgb(0, 0, 0)",blue:"rgb(0, 0, 255)",gray:"rgb(128, 128, 128)",green:"rgb(0, 128, 0)",red:"rgb(255, 0, 0)",white:"rgb(255, 255, 255)"};/^[A-z]+$/i.test(e)?g=h[e]!==d?h[e]:h.black:A.RegEx.isHex.test(e)?g="rgb("+A.Values.hexToRgb(e).join(" ")+")":/^rgba?\(/i.test(e)||(g=h.black),f=(g||e).toString().match(A.RegEx.valueUnwrap)[1].replace(/,(\s+)?/g," ")}return(!p||p>8)&&3===f.split(" ").length&&(f+=" 1"),f;case"inject":return/^rgb/.test(e)?e:(p<=8?4===e.split(" ").length&&(e=e.split(/\s+/).slice(0,3).join(" ")):3===e.split(" ").length&&(e+=" 1"),(p<=8?"rgb":"rgba")+"("+e.replace(/\s+/g,",").replace(/\.(\d)+(?=,)/g,"")+")")}}}();A.Normalizations.registered.innerWidth=b("width",!0),A.Normalizations.registered.innerHeight=b("height",!0),A.Normalizations.registered.outerWidth=b("width"),A.Normalizations.registered.outerHeight=b("height")}},Names:{camelCase:function(a){return a.replace(/-(\w)/g,function(a,b){return b.toUpperCase()})},SVGAttribute:function(a){var b="width|height|x|y|cx|cy|r|rx|ry|x1|x2|y1|y2";return(p||y.State.isAndroid&&!y.State.isChrome)&&(b+="|transform"),new RegExp("^("+b+")$","i").test(a)},prefixCheck:function(a){if(y.State.prefixMatches[a])return[y.State.prefixMatches[a],!0];for(var b=["","Webkit","Moz","ms","O"],c=0,d=b.length;c<d;c++){var e;if(e=0===c?a:b[c]+a.replace(/^\w/,function(a){return a.toUpperCase()}),u.isString(y.State.prefixElement.style[e]))return y.State.prefixMatches[a]=e,[e,!0]}return[a,!1]}},Values:{hexToRgb:function(a){var b,c=/^#?([a-f\d])([a-f\d])([a-f\d])$/i,d=/^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i;return a=a.replace(c,function(a,b,c,d){return b+b+c+c+d+d}),b=d.exec(a),b?[parseInt(b[1],16),parseInt(b[2],16),parseInt(b[3],16)]:[0,0,0]},isCSSNullValue:function(a){return!a||/^(none|auto|transparent|(rgba\(0, ?0, ?0, ?0\)))$/i.test(a)},getUnitType:function(a){return/^(rotate|skew)/i.test(a)?"deg":/(^(scale|scaleX|scaleY|scaleZ|alpha|flexGrow|flexHeight|zIndex|fontWeight)$)|((opacity|red|green|blue|alpha)$)/i.test(a)?"":"px"},getDisplayType:function(a){var b=a&&a.tagName.toString().toLowerCase();return/^(b|big|i|small|tt|abbr|acronym|cite|code|dfn|em|kbd|strong|samp|var|a|bdo|br|img|map|object|q|script|span|sub|sup|button|input|label|select|textarea)$/i.test(b)?"inline":/^(li)$/i.test(b)?"list-item":/^(tr)$/i.test(b)?"table-row":/^(table)$/i.test(b)?"table":/^(tbody)$/i.test(b)?"table-row-group":"block"},addClass:function(a,b){if(a)if(a.classList)a.classList.add(b);else if(u.isString(a.className))a.className+=(a.className.length?" ":"")+b;else{var c=a.getAttribute(p<=7?"className":"class")||"";a.setAttribute("class",c+(c?" ":"")+b)}},removeClass:function(a,b){if(a)if(a.classList)a.classList.remove(b);else if(u.isString(a.className))a.className=a.className.toString().replace(new RegExp("(^|\\s)"+b.split(" ").join("|")+"(\\s|$)","gi")," ");else{var c=a.getAttribute(p<=7?"className":"class")||"";a.setAttribute("class",c.replace(new RegExp("(^|s)"+b.split(" ").join("|")+"(s|$)","gi")," "))}}},getPropertyValue:function(a,c,e,f){function h(a,c){var e=0;if(p<=8)e=o.css(a,c);else{var i=!1;/^(width|height)$/.test(c)&&0===A.getPropertyValue(a,"display")&&(i=!0,A.setPropertyValue(a,"display",A.Values.getDisplayType(a)));var j=function(){i&&A.setPropertyValue(a,"display","none")};if(!f){if("height"===c&&"border-box"!==A.getPropertyValue(a,"boxSizing").toString().toLowerCase()){var k=a.offsetHeight-(parseFloat(A.getPropertyValue(a,"borderTopWidth"))||0)-(parseFloat(A.getPropertyValue(a,"borderBottomWidth"))||0)-(parseFloat(A.getPropertyValue(a,"paddingTop"))||0)-(parseFloat(A.getPropertyValue(a,"paddingBottom"))||0);return j(),k}if("width"===c&&"border-box"!==A.getPropertyValue(a,"boxSizing").toString().toLowerCase()){var l=a.offsetWidth-(parseFloat(A.getPropertyValue(a,"borderLeftWidth"))||0)-(parseFloat(A.getPropertyValue(a,"borderRightWidth"))||0)-(parseFloat(A.getPropertyValue(a,"paddingLeft"))||0)-(parseFloat(A.getPropertyValue(a,"paddingRight"))||0);return j(),l}}var m;m=g(a)===d?b.getComputedStyle(a,null):g(a).computedStyle?g(a).computedStyle:g(a).computedStyle=b.getComputedStyle(a,null),"borderColor"===c&&(c="borderTopColor"),e=9===p&&"filter"===c?m.getPropertyValue(c):m[c],""!==e&&null!==e||(e=a.style[c]),j()}if("auto"===e&&/^(top|right|bottom|left)$/i.test(c)){var n=h(a,"position");("fixed"===n||"absolute"===n&&/top|left/i.test(c))&&(e=o(a).position()[c]+"px")}return e}var i;if(A.Hooks.registered[c]){var j=c,k=A.Hooks.getRoot(j);e===d&&(e=A.getPropertyValue(a,A.Names.prefixCheck(k)[0])),A.Normalizations.registered[k]&&(e=A.Normalizations.registered[k]("extract",a,e)),i=A.Hooks.extractValue(j,e)}else if(A.Normalizations.registered[c]){var l,m;l=A.Normalizations.registered[c]("name",a),"transform"!==l&&(m=h(a,A.Names.prefixCheck(l)[0]),A.Values.isCSSNullValue(m)&&A.Hooks.templates[c]&&(m=A.Hooks.templates[c][1])),i=A.Normalizations.registered[c]("extract",a,m)}if(!/^[\d-]/.test(i)){var n=g(a);if(n&&n.isSVG&&A.Names.SVGAttribute(c))if(/^(height|width)$/i.test(c))try{i=a.getBBox()[c]}catch(q){i=0}else i=a.getAttribute(c);else i=h(a,A.Names.prefixCheck(c)[0])}return A.Values.isCSSNullValue(i)&&(i=0),y.debug>=2&&console.log("Get "+c+": "+i),i},setPropertyValue:function(a,c,d,e,f){var h=c;if("scroll"===c)f.container?f.container["scroll"+f.direction]=d:"Left"===f.direction?b.scrollTo(d,f.alternateValue):b.scrollTo(f.alternateValue,d);else if(A.Normalizations.registered[c]&&"transform"===A.Normalizations.registered[c]("name",a))A.Normalizations.registered[c]("inject",a,d),h="transform",d=g(a).transformCache[c];else{if(A.Hooks.registered[c]){var i=c,j=A.Hooks.getRoot(c);e=e||A.getPropertyValue(a,j),d=A.Hooks.injectValue(i,d,e),c=j}if(A.Normalizations.registered[c]&&(d=A.Normalizations.registered[c]("inject",a,d),c=A.Normalizations.registered[c]("name",a)),h=A.Names.prefixCheck(c)[0],p<=8)try{a.style[h]=d}catch(l){y.debug&&console.log("Browser does not support ["+d+"] for ["+h+"]")}else{var k=g(a);k&&k.isSVG&&A.Names.SVGAttribute(c)?a.setAttribute(c,d):a.style[h]=d}y.debug>=2&&console.log("Set "+c+" ("+h+"): "+d)}return[h,d]},flushTransformCache:function(a){var b="",c=g(a);if((p||y.State.isAndroid&&!y.State.isChrome)&&c&&c.isSVG){var d=function(b){return parseFloat(A.getPropertyValue(a,b))},e={translate:[d("translateX"),d("translateY")],skewX:[d("skewX")],skewY:[d("skewY")],scale:1!==d("scale")?[d("scale"),d("scale")]:[d("scaleX"),d("scaleY")],rotate:[d("rotateZ"),0,0]};o.each(g(a).transformCache,function(a){/^translate/i.test(a)?a="translate":/^scale/i.test(a)?a="scale":/^rotate/i.test(a)&&(a="rotate"),e[a]&&(b+=a+"("+e[a].join(" ")+") ",delete e[a])})}else{var f,h;o.each(g(a).transformCache,function(c){if(f=g(a).transformCache[c],"transformPerspective"===c)return h=f,!0;9===p&&"rotateZ"===c&&(c="rotate"),b+=c+f+" "}),h&&(b="perspective"+h+" "+b)}A.setPropertyValue(a,"transform",b)}};A.Hooks.register(),A.Normalizations.register(),y.hook=function(a,b,c){var e;return a=f(a),o.each(a,function(a,f){if(g(f)===d&&y.init(f),c===d)e===d&&(e=A.getPropertyValue(f,b));else{var h=A.setPropertyValue(f,b,c);"transform"===h[0]&&y.CSS.flushTransformCache(f),e=h}}),e};var B=function(){function a(){return k?z.promise||null:p}function e(a,e){function f(f){var k,n;if(i.begin&&0===D)try{i.begin.call(r,r)}catch(V){setTimeout(function(){throw V},1)}if("scroll"===G){var p,q,w,x=/^x$/i.test(i.axis)?"Left":"Top",B=parseFloat(i.offset)||0;i.container?u.isWrapped(i.container)||u.isNode(i.container)?(i.container=i.container[0]||i.container,p=i.container["scroll"+x],w=p+o(a).position()[x.toLowerCase()]+B):i.container=null:(p=y.State.scrollAnchor[y.State["scrollProperty"+x]],q=y.State.scrollAnchor[y.State["scrollProperty"+("Left"===x?"Top":"Left")]],w=o(a).offset()[x.toLowerCase()]+B),j={scroll:{rootPropertyValue:!1,startValue:p,currentValue:p,endValue:w,unitType:"",easing:i.easing,scrollData:{container:i.container,direction:x,alternateValue:q}},element:a},y.debug&&console.log("tweensContainer (scroll): ",j.scroll,a)}else if("reverse"===G){if(!(k=g(a)))return;if(!k.tweensContainer)return void o.dequeue(a,i.queue);"none"===k.opts.display&&(k.opts.display="auto"),"hidden"===k.opts.visibility&&(k.opts.visibility="visible"),k.opts.loop=!1,k.opts.begin=null,k.opts.complete=null,v.easing||delete i.easing,v.duration||delete i.duration,i=o.extend({},k.opts,i),n=o.extend(!0,{},k?k.tweensContainer:null);for(var E in n)if(n.hasOwnProperty(E)&&"element"!==E){var F=n[E].startValue;n[E].startValue=n[E].currentValue=n[E].endValue,n[E].endValue=F,u.isEmptyObject(v)||(n[E].easing=i.easing),y.debug&&console.log("reverse tweensContainer ("+E+"): "+JSON.stringify(n[E]),a)}j=n}else if("start"===G){k=g(a),k&&k.tweensContainer&&!0===k.isAnimating&&(n=k.tweensContainer);var H=function(e,f){var g,l=A.Hooks.getRoot(e),m=!1,p=f[0],q=f[1],r=f[2]
;if(!(k&&k.isSVG||"tween"===l||!1!==A.Names.prefixCheck(l)[1]||A.Normalizations.registered[l]!==d))return void(y.debug&&console.log("Skipping ["+l+"] due to a lack of browser support."));(i.display!==d&&null!==i.display&&"none"!==i.display||i.visibility!==d&&"hidden"!==i.visibility)&&/opacity|filter/.test(e)&&!r&&0!==p&&(r=0),i._cacheValues&&n&&n[e]?(r===d&&(r=n[e].endValue+n[e].unitType),m=k.rootPropertyValueCache[l]):A.Hooks.registered[e]?r===d?(m=A.getPropertyValue(a,l),r=A.getPropertyValue(a,e,m)):m=A.Hooks.templates[l][1]:r===d&&(r=A.getPropertyValue(a,e));var s,t,v,w=!1,x=function(a,b){var c,d;return d=(b||"0").toString().toLowerCase().replace(/[%A-z]+$/,function(a){return c=a,""}),c||(c=A.Values.getUnitType(a)),[d,c]};if(r!==p&&u.isString(r)&&u.isString(p)){g="";var z=0,B=0,C=[],D=[],E=0,F=0,G=0;for(r=A.Hooks.fixColors(r),p=A.Hooks.fixColors(p);z<r.length&&B<p.length;){var H=r[z],I=p[B];if(/[\d\.-]/.test(H)&&/[\d\.-]/.test(I)){for(var J=H,K=I,L=".",N=".";++z<r.length;){if((H=r[z])===L)L="..";else if(!/\d/.test(H))break;J+=H}for(;++B<p.length;){if((I=p[B])===N)N="..";else if(!/\d/.test(I))break;K+=I}var O=A.Hooks.getUnit(r,z),P=A.Hooks.getUnit(p,B);if(z+=O.length,B+=P.length,O===P)J===K?g+=J+O:(g+="{"+C.length+(F?"!":"")+"}"+O,C.push(parseFloat(J)),D.push(parseFloat(K)));else{var Q=parseFloat(J),R=parseFloat(K);g+=(E<5?"calc":"")+"("+(Q?"{"+C.length+(F?"!":"")+"}":"0")+O+" + "+(R?"{"+(C.length+(Q?1:0))+(F?"!":"")+"}":"0")+P+")",Q&&(C.push(Q),D.push(0)),R&&(C.push(0),D.push(R))}}else{if(H!==I){E=0;break}g+=H,z++,B++,0===E&&"c"===H||1===E&&"a"===H||2===E&&"l"===H||3===E&&"c"===H||E>=4&&"("===H?E++:(E&&E<5||E>=4&&")"===H&&--E<5)&&(E=0),0===F&&"r"===H||1===F&&"g"===H||2===F&&"b"===H||3===F&&"a"===H||F>=3&&"("===H?(3===F&&"a"===H&&(G=1),F++):G&&","===H?++G>3&&(F=G=0):(G&&F<(G?5:4)||F>=(G?4:3)&&")"===H&&--F<(G?5:4))&&(F=G=0)}}z===r.length&&B===p.length||(y.debug&&console.error('Trying to pattern match mis-matched strings ["'+p+'", "'+r+'"]'),g=d),g&&(C.length?(y.debug&&console.log('Pattern found "'+g+'" -> ',C,D,"["+r+","+p+"]"),r=C,p=D,t=v=""):g=d)}g||(s=x(e,r),r=s[0],v=s[1],s=x(e,p),p=s[0].replace(/^([+-\/*])=/,function(a,b){return w=b,""}),t=s[1],r=parseFloat(r)||0,p=parseFloat(p)||0,"%"===t&&(/^(fontSize|lineHeight)$/.test(e)?(p/=100,t="em"):/^scale/.test(e)?(p/=100,t=""):/(Red|Green|Blue)$/i.test(e)&&(p=p/100*255,t="")));if(/[\/*]/.test(w))t=v;else if(v!==t&&0!==r)if(0===p)t=v;else{h=h||function(){var d={myParent:a.parentNode||c.body,position:A.getPropertyValue(a,"position"),fontSize:A.getPropertyValue(a,"fontSize")},e=d.position===M.lastPosition&&d.myParent===M.lastParent,f=d.fontSize===M.lastFontSize;M.lastParent=d.myParent,M.lastPosition=d.position,M.lastFontSize=d.fontSize;var g={};if(f&&e)g.emToPx=M.lastEmToPx,g.percentToPxWidth=M.lastPercentToPxWidth,g.percentToPxHeight=M.lastPercentToPxHeight;else{var h=k&&k.isSVG?c.createElementNS("http://www.w3.org/2000/svg","rect"):c.createElement("div");y.init(h),d.myParent.appendChild(h),o.each(["overflow","overflowX","overflowY"],function(a,b){y.CSS.setPropertyValue(h,b,"hidden")}),y.CSS.setPropertyValue(h,"position",d.position),y.CSS.setPropertyValue(h,"fontSize",d.fontSize),y.CSS.setPropertyValue(h,"boxSizing","content-box"),o.each(["minWidth","maxWidth","width","minHeight","maxHeight","height"],function(a,b){y.CSS.setPropertyValue(h,b,"100%")}),y.CSS.setPropertyValue(h,"paddingLeft","100em"),g.percentToPxWidth=M.lastPercentToPxWidth=(parseFloat(A.getPropertyValue(h,"width",null,!0))||1)/100,g.percentToPxHeight=M.lastPercentToPxHeight=(parseFloat(A.getPropertyValue(h,"height",null,!0))||1)/100,g.emToPx=M.lastEmToPx=(parseFloat(A.getPropertyValue(h,"paddingLeft"))||1)/100,d.myParent.removeChild(h)}return null===M.remToPx&&(M.remToPx=parseFloat(A.getPropertyValue(c.body,"fontSize"))||16),null===M.vwToPx&&(M.vwToPx=parseFloat(b.innerWidth)/100,M.vhToPx=parseFloat(b.innerHeight)/100),g.remToPx=M.remToPx,g.vwToPx=M.vwToPx,g.vhToPx=M.vhToPx,y.debug>=1&&console.log("Unit ratios: "+JSON.stringify(g),a),g}();var S=/margin|padding|left|right|width|text|word|letter/i.test(e)||/X$/.test(e)||"x"===e?"x":"y";switch(v){case"%":r*="x"===S?h.percentToPxWidth:h.percentToPxHeight;break;case"px":break;default:r*=h[v+"ToPx"]}switch(t){case"%":r*=1/("x"===S?h.percentToPxWidth:h.percentToPxHeight);break;case"px":break;default:r*=1/h[t+"ToPx"]}}switch(w){case"+":p=r+p;break;case"-":p=r-p;break;case"*":p*=r;break;case"/":p=r/p}j[e]={rootPropertyValue:m,startValue:r,currentValue:r,endValue:p,unitType:t,easing:q},g&&(j[e].pattern=g),y.debug&&console.log("tweensContainer ("+e+"): "+JSON.stringify(j[e]),a)};for(var I in s)if(s.hasOwnProperty(I)){var J=A.Names.camelCase(I),K=function(b,c){var d,f,g;return u.isFunction(b)&&(b=b.call(a,e,C)),u.isArray(b)?(d=b[0],!u.isArray(b[1])&&/^[\d-]/.test(b[1])||u.isFunction(b[1])||A.RegEx.isHex.test(b[1])?g=b[1]:u.isString(b[1])&&!A.RegEx.isHex.test(b[1])&&y.Easings[b[1]]||u.isArray(b[1])?(f=c?b[1]:l(b[1],i.duration),g=b[2]):g=b[1]||b[2]):d=b,c||(f=f||i.easing),u.isFunction(d)&&(d=d.call(a,e,C)),u.isFunction(g)&&(g=g.call(a,e,C)),[d||0,f,g]}(s[I]);if(t(A.Lists.colors,J)){var L=K[0],O=K[1],P=K[2];if(A.RegEx.isHex.test(L)){for(var Q=["Red","Green","Blue"],R=A.Values.hexToRgb(L),S=P?A.Values.hexToRgb(P):d,T=0;T<Q.length;T++){var U=[R[T]];O&&U.push(O),S!==d&&U.push(S[T]),H(J+Q[T],U)}continue}}H(J,K)}j.element=a}j.element&&(A.Values.addClass(a,"velocity-animating"),N.push(j),k=g(a),k&&(""===i.queue&&(k.tweensContainer=j,k.opts=i),k.isAnimating=!0),D===C-1?(y.State.calls.push([N,r,i,null,z.resolver,null,0]),!1===y.State.isTicking&&(y.State.isTicking=!0,m())):D++)}var h,i=o.extend({},y.defaults,v),j={};switch(g(a)===d&&y.init(a),parseFloat(i.delay)&&!1!==i.queue&&o.queue(a,i.queue,function(b,c){if(!0===c)return!0;y.velocityQueueEntryFlag=!0;var d=y.State.delayedElements.count++;y.State.delayedElements[d]=a;var e=function(a){return function(){y.State.delayedElements[a]=!1,b()}}(d);g(a).delayBegin=(new Date).getTime(),g(a).delay=parseFloat(i.delay),g(a).delayTimer={setTimeout:setTimeout(b,parseFloat(i.delay)),next:e}}),i.duration.toString().toLowerCase()){case"fast":i.duration=200;break;case"normal":i.duration=w;break;case"slow":i.duration=600;break;default:i.duration=parseFloat(i.duration)||1}if(!1!==y.mock&&(!0===y.mock?i.duration=i.delay=1:(i.duration*=parseFloat(y.mock)||1,i.delay*=parseFloat(y.mock)||1)),i.easing=l(i.easing,i.duration),i.begin&&!u.isFunction(i.begin)&&(i.begin=null),i.progress&&!u.isFunction(i.progress)&&(i.progress=null),i.complete&&!u.isFunction(i.complete)&&(i.complete=null),i.display!==d&&null!==i.display&&(i.display=i.display.toString().toLowerCase(),"auto"===i.display&&(i.display=y.CSS.Values.getDisplayType(a))),i.visibility!==d&&null!==i.visibility&&(i.visibility=i.visibility.toString().toLowerCase()),i.mobileHA=i.mobileHA&&y.State.isMobile&&!y.State.isGingerbread,!1===i.queue)if(i.delay){var k=y.State.delayedElements.count++;y.State.delayedElements[k]=a;var n=function(a){return function(){y.State.delayedElements[a]=!1,f()}}(k);g(a).delayBegin=(new Date).getTime(),g(a).delay=parseFloat(i.delay),g(a).delayTimer={setTimeout:setTimeout(f,parseFloat(i.delay)),next:n}}else f();else o.queue(a,i.queue,function(a,b){if(!0===b)return z.promise&&z.resolver(r),!0;y.velocityQueueEntryFlag=!0,f(a)});""!==i.queue&&"fx"!==i.queue||"inprogress"===o.queue(a)[0]||o.dequeue(a)}var j,k,p,q,r,s,v,x=arguments[0]&&(arguments[0].p||o.isPlainObject(arguments[0].properties)&&!arguments[0].properties.names||u.isString(arguments[0].properties));u.isWrapped(this)?(k=!1,q=0,r=this,p=this):(k=!0,q=1,r=x?arguments[0].elements||arguments[0].e:arguments[0]);var z={promise:null,resolver:null,rejecter:null};if(k&&y.Promise&&(z.promise=new y.Promise(function(a,b){z.resolver=a,z.rejecter=b})),x?(s=arguments[0].properties||arguments[0].p,v=arguments[0].options||arguments[0].o):(s=arguments[q],v=arguments[q+1]),!(r=f(r)))return void(z.promise&&(s&&v&&!1===v.promiseRejectEmpty?z.resolver():z.rejecter()));var C=r.length,D=0;if(!/^(stop|finish|finishAll|pause|resume)$/i.test(s)&&!o.isPlainObject(v)){var E=q+1;v={};for(var F=E;F<arguments.length;F++)u.isArray(arguments[F])||!/^(fast|normal|slow)$/i.test(arguments[F])&&!/^\d/.test(arguments[F])?u.isString(arguments[F])||u.isArray(arguments[F])?v.easing=arguments[F]:u.isFunction(arguments[F])&&(v.complete=arguments[F]):v.duration=arguments[F]}var G;switch(s){case"scroll":G="scroll";break;case"reverse":G="reverse";break;case"pause":var H=(new Date).getTime();return o.each(r,function(a,b){h(b,H)}),o.each(y.State.calls,function(a,b){var c=!1;b&&o.each(b[1],function(a,e){var f=v===d?"":v;return!0!==f&&b[2].queue!==f&&(v!==d||!1!==b[2].queue)||(o.each(r,function(a,d){if(d===e)return b[5]={resume:!1},c=!0,!1}),!c&&void 0)})}),a();case"resume":return o.each(r,function(a,b){i(b,H)}),o.each(y.State.calls,function(a,b){var c=!1;b&&o.each(b[1],function(a,e){var f=v===d?"":v;return!0!==f&&b[2].queue!==f&&(v!==d||!1!==b[2].queue)||(!b[5]||(o.each(r,function(a,d){if(d===e)return b[5].resume=!0,c=!0,!1}),!c&&void 0))})}),a();case"finish":case"finishAll":case"stop":o.each(r,function(a,b){g(b)&&g(b).delayTimer&&(clearTimeout(g(b).delayTimer.setTimeout),g(b).delayTimer.next&&g(b).delayTimer.next(),delete g(b).delayTimer),"finishAll"!==s||!0!==v&&!u.isString(v)||(o.each(o.queue(b,u.isString(v)?v:""),function(a,b){u.isFunction(b)&&b()}),o.queue(b,u.isString(v)?v:"",[]))});var I=[];return o.each(y.State.calls,function(a,b){b&&o.each(b[1],function(c,e){var f=v===d?"":v;if(!0!==f&&b[2].queue!==f&&(v!==d||!1!==b[2].queue))return!0;o.each(r,function(c,d){if(d===e)if((!0===v||u.isString(v))&&(o.each(o.queue(d,u.isString(v)?v:""),function(a,b){u.isFunction(b)&&b(null,!0)}),o.queue(d,u.isString(v)?v:"",[])),"stop"===s){var h=g(d);h&&h.tweensContainer&&(!0===f||""===f)&&o.each(h.tweensContainer,function(a,b){b.endValue=b.currentValue}),I.push(a)}else"finish"!==s&&"finishAll"!==s||(b[2].duration=1)})})}),"stop"===s&&(o.each(I,function(a,b){n(b,!0)}),z.promise&&z.resolver(r)),a();default:if(!o.isPlainObject(s)||u.isEmptyObject(s)){if(u.isString(s)&&y.Redirects[s]){j=o.extend({},v);var J=j.duration,K=j.delay||0;return!0===j.backwards&&(r=o.extend(!0,[],r).reverse()),o.each(r,function(a,b){parseFloat(j.stagger)?j.delay=K+parseFloat(j.stagger)*a:u.isFunction(j.stagger)&&(j.delay=K+j.stagger.call(b,a,C)),j.drag&&(j.duration=parseFloat(J)||(/^(callout|transition)/.test(s)?1e3:w),j.duration=Math.max(j.duration*(j.backwards?1-a/C:(a+1)/C),.75*j.duration,200)),y.Redirects[s].call(b,b,j||{},a,C,r,z.promise?z:d)}),a()}var L="Velocity: First argument ("+s+") was not a property map, a known action, or a registered redirect. Aborting.";return z.promise?z.rejecter(new Error(L)):b.console&&console.log(L),a()}G="start"}var M={lastParent:null,lastPosition:null,lastFontSize:null,lastPercentToPxWidth:null,lastPercentToPxHeight:null,lastEmToPx:null,remToPx:null,vwToPx:null,vhToPx:null},N=[];o.each(r,function(a,b){u.isNode(b)&&e(b,a)}),j=o.extend({},y.defaults,v),j.loop=parseInt(j.loop,10);var O=2*j.loop-1;if(j.loop)for(var P=0;P<O;P++){var Q={delay:j.delay,progress:j.progress};P===O-1&&(Q.display=j.display,Q.visibility=j.visibility,Q.complete=j.complete),B(r,"reverse",Q)}return a()};y=o.extend(B,y),y.animate=B;var C=b.requestAnimationFrame||q;if(!y.State.isMobile&&c.hidden!==d){var D=function(){c.hidden?(C=function(a){return setTimeout(function(){a(!0)},16)},m()):C=b.requestAnimationFrame||q};D(),c.addEventListener("visibilitychange",D)}return a.Velocity=y,a!==b&&(a.fn.velocity=B,a.fn.velocity.defaults=y.defaults),o.each(["Down","Up"],function(a,b){y.Redirects["slide"+b]=function(a,c,e,f,g,h){var i=o.extend({},c),j=i.begin,k=i.complete,l={},m={height:"",marginTop:"",marginBottom:"",paddingTop:"",paddingBottom:""};i.display===d&&(i.display="Down"===b?"inline"===y.CSS.Values.getDisplayType(a)?"inline-block":"block":"none"),i.begin=function(){0===e&&j&&j.call(g,g);for(var c in m)if(m.hasOwnProperty(c)){l[c]=a.style[c];var d=A.getPropertyValue(a,c);m[c]="Down"===b?[d,0]:[0,d]}l.overflow=a.style.overflow,a.style.overflow="hidden"},i.complete=function(){for(var b in l)l.hasOwnProperty(b)&&(a.style[b]=l[b]);e===f-1&&(k&&k.call(g,g),h&&h.resolver(g))},y(a,m,i)}}),o.each(["In","Out"],function(a,b){y.Redirects["fade"+b]=function(a,c,e,f,g,h){var i=o.extend({},c),j=i.complete,k={opacity:"In"===b?1:0};0!==e&&(i.begin=null),i.complete=e!==f-1?null:function(){j&&j.call(g,g),h&&h.resolver(g)},i.display===d&&(i.display="In"===b?"auto":"none"),y(this,k,i)}}),y}(window.jQuery||window.Zepto||window,window,window?window.document:undefined)});
/*! BUI - v2.0.0 - 2022-03-13 09:03:17 
Copyright (c): kevin.huang  https://gitee.com/kevin-huang/bui2.0.git (qq:757723114) 
Released under MIT License*/
(function ( global, factory) {
//   "use strict";
	if ( typeof module === "object" && typeof module.exports === "object" ) {
		module.exports = global.document ?
			factory( global, true ) :
			function( w ) {
				if ( !w.document ) {
					throw new Error( "Bui requires a window with a document" );
				}
				return factory( w );
			};
	} else {
		  factory( global );
	}  
})( typeof window !== "undefined" ? window : this, function( window, noGlobal ) {
//   "use strict";
  var $B = window["$B"] ?  window["$B"] : {};

var $body, DomUtils,_onNodeRemoveEventTimer;
function _getBody() {
    if (!$body) {
        $body = document.body;
        $B.DomUtils.css($body, { "position": "relative" });
    }
    return $body;
}
$B["getBody"] = _getBody;

var class2type = {},
    hasOwn = class2type.hasOwnProperty,
    toString = class2type.toString;

/***
 * 获取数据类型
 * ***/
function getDataType(obj) {
    var toString = Object.prototype.toString;
    var map = {
        '[object Boolean]': 'boolean',
        '[object Number]': 'number',
        '[object String]': 'string',
        '[object Function]': 'function',
        '[object Array]': 'array',
        '[object Date]': 'date',
        '[object RegExp]': 'regExp',
        '[object Undefined]': 'undefined',
        '[object Null]': 'null',
        '[object Object]': 'object'
    };
    if (obj instanceof Element) {
        return 'element';
    }
    return map[toString.call(obj)];
}
$B["getDataTypeFn"] = getDataType;

/****
 * 确保是数组
 * ******/
function ensureArray(data) {
    if (Array.isArray(data)) {
        return data;
    }
    return [data];
}
$B["ensureArrayFn"] = ensureArray;


function _isInArray(elem, arr, i) {
    var len;
    if (arr) {
        if (Array.prototype.indexOf) {
            return Array.prototype.indexOf.call(arr, elem, i);
        }
        len = arr.length;
        i = i ? i < 0 ? Math.max(0, len + i) : i : 0;
        for (; i < len; i++) {
            if (i in arr && arr[i] === elem) {
                return i;
            }
        }
    }
    return -1;
}
$B["isInArrayFn"] = _isInArray;

function _diyTrim(str) {
    var trimLeft = /^\s+/,
        trimRight = /\s+$/,
        rnotwhite = /\S/;
    if (rnotwhite.test("\xA0")) {
        trimLeft = /^[\s\xA0]+/;
        trimRight = /[\s\xA0]+$/;
    }
    return str == null ? "" : str.replace(trimLeft, "").replace(trimRight, "");
}

function _trim(str) {
    var r;
    if (String.prototype.trim) {
        r = str.trim();
    } else {
        r = _diyTrim(str);
    }
    r = r.replace(/\u200B/g, '');
    return r;
}

$B["trimFn"] = _trim;

// 变成驼峰
function camelFn(str) {
    return str.replace(/-(\w)/g, function (m0, m1) {
        return m1.toUpperCase();
    });
}
// 变成破折
function dashesFn(str) {
    return str.replace(/[A-Z]/g, function (m0) {
        return '-' + m0.toLowerCase();
    });
}
$B["camelFn"] = camelFn;
$B["dashesFn"] = dashesFn;

/**
 * 返回obj元素在array中的位置
 * @param {Array} array
 * @param {*} obj
 * @returns {number}
 * @private
 */
function _indexOf(array, obj) {
    if (Array.prototype.indexOf) {
        return Array.prototype.indexOf.call(array, obj);
    }
    for (var i = 0, j = array.length; i < j; i++) {
        if (array[i] === obj) {
            return i;
        }
    }
    return -1;
}
$B["indexOfArrayFn"] = _indexOf;
/**
 * 是否是数组
 * @param {*} object
 * @returns {boolean}
 * @private
 */
function _isArray(object) {
    return Object.prototype.toString.call(object) === '[object Array]';
}
$B["isArrayFn"] = _isArray;
/**
 *是否是字符串
 * @param {*} object
 * @returns {boolean}
 * @private
 */
function _isString(object) {
    return typeof object === 'string';
}
$B["isStringFn"] = _isString;
/**
 * 是否是数字
 * @param {*} object
 * @returns {boolean}
 * @private
 */
function _isNumeric(object) {
    var ret = typeof object === 'number' && isFinite(object);
    if (!ret) {
        var regPos = /^\d+(\.\d+)?$/; //非负浮点数
        var regNeg = /^(-(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*)))$/; //负浮点数
        if (regPos.test(object) && regNeg.test(object)) {
            ret = true;
        } else {
            ret = false;
        }
    }
    return ret;
}
$B["isNumericFn"] = _isNumeric;
/**
 * 是否是object对象
 * @param {*} object
 * @returns {boolean}
 * @private
 */
function _isObject(object) {
    return typeof object === 'object';
}

function _isEmptyObject(object) {
    if (_isObject(object)) {
        return Object.keys(object).length === 0;
    }
    return false;
}
$B["isEmptyObjectFn"] = _isEmptyObject;

/**
 * 检查是否是函数
 * @param {*} object
 * @returns {boolean}
 * @private
 */
function _isFunction(object) {
    return typeof object === 'function';
}
$B["isFunctionFn"] = _isFunction;
/**
 * 检查是否是{}对象
 * @param object
 * @returns {*|boolean}
 * @private
 */
function _isLiteralObject(obj) {
    var key;
    if (!obj || typeof obj !== "object" || obj.nodeType) {
        return false;
    }
    try {
        if (obj.constructor &&
            !hasOwn.call(obj, "constructor") &&
            !hasOwn.call(obj.constructor.prototype, "isPrototypeOf")) {
            return false;
        }
    } catch (e) {
        return false;
    }
    for (key in obj) {
    }
    return key === undefined || hasOwn.call(obj, key);
    //return object && typeof object === "object" && Object.getPrototypeOf(object) === Object.getPrototypeOf({});
}
$B["isPlainObjectFn"] = _isLiteralObject;

/**
 * 检查是否是可迭代的元素
 * @param {Object} object
 * @returns {boolean}
 * @private
 */
function _isIterable(object) {
    if (object instanceof HTMLElement 
        || object instanceof Node || (typeof object === 'object' && object.nodeName && object.tagName)) {
        return false;
    }
    var r = _isLiteralObject(object) || _isArray(object) || (typeof object === 'object' && object !== null && object['length'] !== undefined);
    return r;
}
$B["isIterableFn"] = _isIterable;

/**
 * 深拷贝克隆一个对象
 * @param {Object} object
 * @private
 */
function _cloneObject(object) {
    var copy;
    var property;
    if (!_isObject(object) || object === null) {
        copy = object;
        return copy;
    }
    if (_isArray(object)) {
        copy = [];
        for (var i = 0, l = object.length; i < l; i++) {
            copy[i] = _cloneObject(object[i]);
        }
        return copy;
    }
    copy = {};
    for (property in object) {
        if (!object.hasOwnProperty(property)) {
            continue;
        }
        if (_isObject(object[property]) && object[property] !== null) {
            copy[property] = _cloneObject(object[property]);
        } else {
            copy[property] = object[property];
        }
    }
    return copy;
}
$B["cloneObjectFn"] = _cloneObject;



/****
 * 对象合并 ：将srcObj拷贝到destObj上
 * destObj:合并到的对象
 * srcObj：合并的来源对象
 * notOverride:不覆盖已经存在的key destObj, srcObj, notOverride
 * ******/
function _extendObjectFn() {
    var src, copyIsArray, copy, name, options, clone,
        target = arguments[0] || {},
        i = 1,
        length = arguments.length,
        deep = false;

    if (typeof target === "boolean") {
        deep = target;
        target = arguments[i] || {};
        i++;
    }

    if (typeof target !== "object" && typeof target !== "function") {
        target = {};
    }

    if (i === length) {
        target = this;
        i--;
    }

    for (; i < length; i++) {
        if ((options = arguments[i])) {
            for (name in options) {
                if (!options.hasOwnProperty(name)) {
                    continue;
                }
                src = target[name];
                copy = options[name];
                if (target === copy) {
                    continue;
                }
                if (deep && copy && (_isLiteralObject(copy) || (copyIsArray = _isArray(copy)))) {
                    if (copyIsArray) {
                        copyIsArray = false;
                        clone = src && _isArray(src) ? src : [];

                    } else {
                        clone = src && _isLiteralObject(src) ? src : {};
                    }
                    target[name] = _extendObjectFn(deep, clone, copy);

                } else if (copy !== undefined) {
                    target[name] = copy;
                }
            }
        }
    }
    return target;
}
$B["extendObjectFn"] = _extendObjectFn;

function needReplacePx(prop) {
    prop = prop.toLowerCase();
    if(prop.indexOf("border") >= 0 && prop.indexOf("width") < 0){
        return false;
    }
    if (prop.indexOf("width") > -1 || prop.indexOf("height") > -1 
    || prop.indexOf("top") > -1 || prop.indexOf("left") > -1 
    || prop.indexOf("padding") > -1 || prop.indexOf("margin") > -1
    ) {
        return true;
    }
    return false;
}
/**
 * 获取样式的style样式
 * @param element
 * @param prop
 * @returns {*}
 * @private
 */
function _getComputedStyle(element, prop) {
    var computedStyle;
    if (typeof window.getComputedStyle === 'function') { //normal browsers
        computedStyle = window.getComputedStyle(element);
    } else if (typeof document.currentStyle !== undefined) { //shitty browsers
        computedStyle = element.currentStyle;
    } else {
        computedStyle = element.style;
    }
    if (prop) {
        let value = computedStyle[prop];
        if (needReplacePx(prop)) {
            if(value.indexOf("px") > 0){
                value = parseFloat(value.replace("px", ""));
            }else if(/^-?\d+(.?\d+)?$/.test(value)){
                value = parseFloat(value);
            }
        }
        return value;
    } else {
        return computedStyle;
    }
}
$B["getComputedStyleFn"] = _getComputedStyle;
/**
 * 循环对象object
 * @param object
 * @param callback
 * @private
 */
function _each(object, callback) {
    var i, l;
    if (_isArray(object) || (typeof object === 'object' && object['length'] !== undefined)) {
        for (i = 0, l = object.length; i < l; i++) {
            callback.apply(object[i], [object[i], i]);
        }
        return;
    }
    if (_isLiteralObject(object)) {
        var keys = Object.keys(object);
        for (i = 0; i < keys.length; i++) {
            callback.apply(object[keys[i]], [object[keys[i]], keys[i]]);
        }
    }
}
$B["foreachFn"] = _each;

var _domReadyHandlers = [];
var _domLoadedHandlers = [];
var _isDomReady = false;
var _isDomLoaded = false;
var _animationLastTime;
/**这些属性不自动添加px单位**/
var CSSNumber = {
    "animationIterationCount": true,
    "animation-iteration-count": true,
    "columnCount": true,
    "column-count": true,
    "fillOpacity": true,
    "fill-opacity": true,
    "flexGrow": true,
    "flex-grow": true,
    "flexShrink": true,
    "flex-shrink": true,
    "opacity": true,
    "order": true,
    "orphans": true,
    "widows": true,
    "zIndex": true,
    "z-index": true,
    "zoom": true,
    "WebkitLineClamp":true,    
    "-webkit-line-clamp": true
};
var addListener = document.addEventListener ? 'addEventListener' : 'attachEvent';
var removeListener = document.removeEventListener ? 'removeEventListener' : 'detachEvent';
var eventPrefix = document.addEventListener ? '' : 'on';
var createEvent = document.createEvent ? 'createEvent' : 'createEventObject';
var dispatchEvent = document.dispatchEvent ? 'dispatchEvent' : 'fireEvent';
var vendors = ['-moz-', '-ms-', '-webkit-', '-o-', ''];
var cssMap = {
    "background-color":"backgroundColor",
    "z-index":"zIndex"  
};
var cssNameProperty = function (prop) { 
    var exp = /-([a-z0-9])/;
    while (exp.test(prop)) {
        prop = prop.replace(exp, RegExp.$1.toUpperCase());
    }
    return prop;
};
var requestAnimationFrame = window.requestAnimationFrame;
var cancelAnimationFrame = window.cancelAnimationFrame || window.cancelRequestAnimationFrame;
var div = document.createElement('div');
var style = _getComputedStyle(div);

//ie版本
var ie = (function () {
    var rv;
    if (navigator.appName === 'Microsoft Internet Explorer') {
        var ua = navigator.userAgent;
        var re = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
        if (re.exec(ua) != null) {
            rv = parseFloat(RegExp.$1);
        }
    }
    return rv;
}());

//transition detection
var transitionSupport = (function () {
    for (var i in vendors) {
        if (_isString(style[vendors[i] + 'transition'])) {
            return true;
        }
    }
    return false;
})();

//request animation pollyfill
if (!requestAnimationFrame || !cancelAnimationFrame) {
    for (var i = 0; i < vendors.length; i++) {
        var vendor = vendors[i];
        requestAnimationFrame = requestAnimationFrame || window[vendor + 'RequestAnimationFrame'];
        cancelAnimationFrame = cancelAnimationFrame || window[vendor + 'CancelAnimationFrame'] || window[vendor + 'CancelRequestAnimationFrame'];
    }
}

if (!requestAnimationFrame || !cancelAnimationFrame) {
    requestAnimationFrame = function (callback) {
        var currentTime = new Date().getTime();
        var timeToCall = Math.max(0, 16 - (currentTime - _animationLastTime));
        var id = window.setTimeout(function _requestAnimationFrameTimeout() {
            callback(currentTime + timeToCall);
        }, timeToCall);

        _animationLastTime = currentTime + timeToCall;
        return id;
    };

    cancelAnimationFrame = function (id) {
        window.clearTimeout(id);
    };
}

function _getOffset(elem) {
    var docElem, win,
        box = { top: 0, left: 0 },
        doc = elem && elem.ownerDocument;//当前文档
    if (!doc) {
        return;
    }
    docElem = doc.documentElement;
    if (typeof elem.getBoundingClientRect !== 'undefined') {//如果元素有getBoundingClientRect方法
        box = elem.getBoundingClientRect();//调用该方法
    }
    win = window;//如果是window就返回window，如果是document，返回document.defaultView
    return {
        //元素相对于视窗的距离+滚动距离-边框宽度
        top: box.top + win.pageYOffset - docElem.clientTop,
        left: box.left + win.pageXOffset - docElem.clientLeft
    };
}

var Mouse = {
    BUTTON_LEFT: 0,
    BUTTON_MIDDLE: 1,
    BUTTON_RIGHT: 2
};
//事件参数二次封装
function EventArgs(e) {
    this._e = e;
    if (!EventArgs.prototype.stopPropagation) {
        EventArgs.prototype.stopPropagation = function () {
            if (this._e.stopPropagation) {
                this._e.stopPropagation();
            } else {
                this._e.cancelBubble = true;
            }
        };
    }

    if (!EventArgs.prototype.stopPropagation) {
        EventArgs.prototype.preventDefault = function () {
            if (this._e.preventDefault) {
                this._e.preventDefault();
            } else {
                this._e.returnValue = false;
            }
        };
    }
    this.target = this._e.target || this._e.srcElement;
    this.ctrlKey = this._e.ctrlKey;
    this.shiftKey = this._e.shiftKey;
    this.altKey = this._e.altKey;
    this.layerY = this._e.layerY || this._e.offsetY;
    this.layerX = this._e.layerX || this._e.offsetX;
    this.x = this._e.x || this._e.clientX;
    this.y = this._e.y || this._e.clientY;
    this.pageX = this._e.pageX;
    this.pageY = this._e.pageY;
    this.keyCode = this._e.keyCode;
    this.name = this.type = this._e.type;
    this.path = this._e.path;    
    this.which = this._e.which;
    if (ie & ie < 9) {
        this.button = this._e.button === 1 ? Mouse.BUTTON_LEFT : (this._e.button === 4 ? Mouse.BUTTON_MIDDLE : Mouse.BUTTON_RIGHT);
    } else if (this._e.hasOwnProperty('which')) {
        this.button = this._e.which === 1 ? Mouse.BUTTON_LEFT : (this._e.which === 2 ? Mouse.BUTTON_MIDDLE : Mouse.BUTTON_RIGHT);
    } else {
        this.button = this._e.button;
    }
}
//dom事件机制封装
function commHandleEvent(event) {
    event = event || window.event;
    let handlers = this.events[event.type];
   
    if(_isEmptyObject(handlers)){        
        return;
    }
    var keys = Object.keys(handlers);
    let ret, handler,isEmpty = true;
    for (let i = 0; i < keys.length; i++) {
        handler = handlers[keys[i]];
        if(!handler){
            console.log("handler ex");
            continue; 
        }
        isEmpty = false;
        let ev = new EventArgs(event);
        ev.eventNamespace = handler.namespace;
        if (handler.params) {
            ev.params = handler.params;
        }
        let res;
        if (typeof handler.fn === "function") {
            ev.catpurer = this;
            res = handler.fn.call(this, ev);
        }
        if (typeof ret === "undefined" && typeof res !== "undefined") {
            ret = !res;
        }
    }
    if (ret) {//执行阻止冒泡
        if (event.stopPropagation) {
            event.stopPropagation();
        } else {
            event.cancelBubble = true;
        }
        event.returnValue = false; 
    }
    if(isEmpty){
        console.log("not events clear el");
        delete this.events[event.type];
    }
}

var radioCheckboxHooks = {
    set: function (elem, value) {
        if (Array.isArray(value)) {
            return "";
        }
    },
    get: function (elem) {
        return elem.getAttribute("value") === null ? "on" : elem.value;
    }
};
let regName = /^(\w+)\[(.+)\]$/;
function getNameAndAttr(arg1){
    let ret = {name:arg1};
    if(regName.test(arg1)){
        let math = regName.exec(arg1);
        ret.name = math[1];
        ret.attr = math[2];
    }
    return ret;
}
var valHooks = {
    option: {
        get: function (elem) {
            var val = elem.getAttribute("value");
            return val != null ? _trim(val) : _trim(elem.innerText);
        },
        set: function (elem, value) {
        }
    },
    select: {
        get: function (elem) {
            var value, option,
                options = elem.options,
                index = elem.selectedIndex,//当前选中项 单选默认0，多选默认-1
                //如果是单选下拉框或者当前没有选中项，one为true
                one = elem.type === "select-one" || index < 0,
                values = one ? null : [],//one为true，则values为null，否则为[]
                max = one ? index + 1 : options.length,//单选最大为1,多选为options.length
                i = index < 0 ?
                    max :
                    one ? index : 0;
            for (; i < max; i++) {//遍历
                option = options[i];
                if ((option.selected || i === index) && (typeof option.disabled !== "undefined" ? !option.disabled : option.getAttribute("disabled") === null) && (!option.parentNode.disabled || option.parentNode.nodeName.toLowerCase() !== "optgroup")) {
                    value = valHooks.option.get(option);
                    if (one) {//单选直接返回
                        return value;
                    }
                    values.push(value);//多选推入数组
                }
            }
            return values;//多选且没有没有默认项，不经过循环直接返回
        },
        set: function (elem, value) {
            var optionSet, option,
                options = elem.options,
                values = ensureArray(value),//转为数组,value可以是任何值 "a"=>["a"]
                i = options.length;//选项数量
            //遍历
            while (i--) {
                option = options[i];
                //如果当前选项的值在values数组中，selected为true，否则为false
                //可以用于设置多选的下拉单值
                /*用法：
                    <select name="" id="" class="slt" multiple>
                        <option value="a">aaa</option>
                        <option value="b">bbbb</option>
                        <option value="c">ccccc</option>
                    </select>
                    <script>
                        $('.slt').val(["a","c"]);
                    </script>
                 */
                if ((option.selected = _isInArray(option.value, values) >= 0)) {
                    optionSet = true;//标识
                }
            }

            // force browsers to behave consistently when non-matching value is set
            if (!optionSet) {
                elem.selectedIndex = -1;
            }
            return values;
        }
    },
    radio: radioCheckboxHooks,
    checkbox: radioCheckboxHooks
};
var DomApiObj = {
    /***
     * 对元素添加一个事件监听
     * element：元素
     * eventName ：事件名称,支持命名空间 namespac.click
     * listener:事件处理函数
     * params:额外的参数
     * **/
    addListener: function (eventName, listener, params) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        if (_isArray(element)) {
            for (let i = 0; i < element.length; i++) {
                DomApiObj.addListener.call(element[i], eventName, listener, params);
            }
            return this;
        }
        if (!element.eventIndex) {
            element.eventIndex = 1;
        } else {
            element.eventIndex++;
        }
        if (!element.events) {
            element.events = {};
        }
        var namespace = "", eventKey = element.eventIndex;
        if (eventName.indexOf(".") > 0) {
            let arr = eventName.split(".");
            namespace = arr[0];
            eventName = arr[1];
        }
        let handlers = element.events[eventName];
        if (!handlers) {
            handlers = {};
            handlers[eventKey] = {
                params: params,
                namespace: namespace,
                fn: listener
            };
            if (element.addEventListener) {
                element.addEventListener(eventName, commHandleEvent, false);
            } else {
                element["on" + eventName] = commHandleEvent;
            }
            listener.eventIndex = element.eventIndex;
            element.events[eventName] = handlers;
        } else {
            //避免重复添加
            if (listener.eventIndex) {
                if (handlers[listener.eventIndex] && handlers[listener.eventIndex].fn === listener) {                   
                    return DomUtils;
                }
            }
            handlers[eventKey] = {
                params: params,
                namespace: namespace,
                fn: listener
            };
            listener.eventIndex = element.eventIndex;
        }
        return this;
    },
    /***
     * 对元素移除事件
     * element：元素
     * eventName ：事件名称,支持命名空间 namespac.click
     * listener:事件处理函数
     * **/
    removeListener: function (eventName, listener) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        if (_isArray(element)) {
            for (let i = 0; i < element.length; i++) {
                DomApiObj.removeListener.call(element[i], eventName, listener);
            }
            return this;
        }
        if (element.events) {
            if (typeof eventName !== "string") {
                return;
            }
            var namespace;
            if (eventName.indexOf(".") > 0) {
                let arr = eventName.split(".");
                namespace = arr[0];
                eventName = arr[1];
            }
            if (eventName === "*") {
                listener = undefined;
            }
            if (element.events[eventName] || (eventName === "*" && namespace)) {
                if (namespace) {//如果存在命名空间
                    let allEventNames = Object.keys(element.events); // click 、mousedown、mouseup、change............   
                    let i, j, eventMap, eventKeys, eventKey, handler;
                    for (i = 0; i < allEventNames.length; i++) {
                        if (eventName === "*" || eventName === allEventNames[i] || typeof listener === "undefined") {
                            eventMap = element.events[allEventNames[i]];//click 、mousedown、mouseup、change事件集合
                            eventKeys = Object.keys(eventMap);//eventindex
                            for (j = 0; j < eventKeys.length; j++) {
                                eventKey = eventKeys[j];
                                handler = eventMap[eventKey];
                                if (handler.namespace !== "") { //事件是存在命名空间的
                                    if (handler.namespace === namespace) {
                                        if (typeof listener === "undefined") {
                                            if (eventName === "*" || eventName === allEventNames[i]) {//移除命名空间下的所有事件
                                                delete eventMap[eventKey];
                                            }
                                        } else {
                                            if (eventName === allEventNames[i] && (listener.eventIndex + '') === eventKey) {
                                                delete eventMap[eventKey];
                                            }
                                        }
                                    }
                                }
                            }
                            if (_isEmptyObject(eventMap)) {
                                delete element.events[allEventNames[i]];
                                element.removeEventListener(allEventNames[i], commHandleEvent);//移除通用处理
                            }
                        }
                    }
                } else {
                    if (listener) {
                        delete element.events[eventName][listener.eventIndex];
                    } else {
                        element.events[eventName] = {};
                    }
                }
            }
            if (!element.events[eventName] || Object.keys(element.events[eventName]).length === 0) {
                delete element.events[eventName];
                element.removeEventListener(eventName, commHandleEvent);//移除通用处理
            }
        } else {
            element.removeEventListener(eventName, commHandleEvent);//移除通用处理
        }
        return this;
    },
    /***
     * 触发事件
     * ****/
    trigger: function (eventName, args) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        if (_isArray(element)) {
            for (let i = 0; i < element.length; i++) {
                DomApiObj.trigger.call(element[i], eventName, args);
            }
            return this;
        }
        if (element.events) {
            var namespace = "";
            if (eventName.indexOf(".") > 0) {
                let arr = eventName.split(".");
                namespace = arr[0];
                eventName = arr[1];
            }
            let handlers = element.events[eventName];
            if(!handlers){
                return;
            }
            let keys = Object.keys(handlers);
            let pr = { target: element, isTrigger: true, params: {} };
            if (args) {
                Object.assign(pr, args);
            }
            for (let i = 0; i < keys.length; i++) {
                let evObj = handlers[keys[i]];
                if (namespace === "" || namespace === evObj.namespace) {
                    pr.params = evObj.params;
                    evObj.fn.call(element, pr);
                }
            }
        }
        return this;
    },
    /***
     * 解除element元素的某一类事件
     * eventName=click、mousedown、mouseup 事件名称或者 namespace.eventName 或者 namespace.*
     * ***/
    offEvents: function (eventName) {        
        var element = this;
        if (this._el) {
            element = this._el;
        }
        if (_isArray(element)) {
            for (let i = 0; i < element.length; i++) {
                DomApiObj.offEvents.call(element[i], eventName);
            }
            return this;
        }
        if (element.events) {
            if (eventName) {
                DomApiObj.removeListener.call(element, eventName);
            } else {
                var eventNames = Object.keys(element.events);
                for (var i = 0; i < eventNames.length; ++i) {
                    DomApiObj.removeListener.call(element, eventNames[i]);
                }
            }
        }
        return this;
    },
    /***
     * 批量绑定事件
     * events = {
     *      click:fn,
     *      dblclick:fn
     * }
     * ***/
    bind: function (events) {
        if (!_isLiteralObject(events)) {
            throw new Error("events is not a  object {}");
        }
        var element = this;
        if (this._el) {
            element = this._el;
        }
        if (_isArray(element)) {
            for (let i = 0; i < element.length; i++) {
                DomApiObj.bind.call(element[i], events);
            }
            return this;
        }
        let keys = Object.keys(events);
        for (let i = 0; i < keys.length; i++) {
            let eventName = keys[i];
            if (DomApiObj[eventName]) {
                DomApiObj[eventName].call(element, events[eventName]);
            }
        }
        return this;
    },
    click: function (listener) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        if (_isArray(element)) {
            for (let i = 0; i < element.length; i++) {
                DomApiObj.click.call(element[i], listener);
            }
            return this;
        }
        DomApiObj.addListener.call(element, "click", listener);
        return this;
    },
    paste:function(listener){
        var element = this;
        if (this._el) {
            element = this._el;
        }
        if (_isArray(element)) {
            for (let i = 0; i < element.length; i++) {
                DomApiObj.paste.call(element[i], listener);
            }
            return this;
        }
        DomApiObj.addListener.call(element, "paste", listener);
        return this;
    },
    contextmenu: function (listener) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        if (_isArray(element)) {
            for (let i = 0; i < element.length; i++) {
                DomApiObj.contextmenu.call(element[i], listener);
            }
            return this;
        }
        DomApiObj.addListener.call(element, "contextmenu", listener);
        return this;
    },
    dblclick: function (listener) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        if (_isArray(element)) {
            for (let i = 0; i < element.length; i++) {
                DomApiObj.dblclick.call(element[i], listener);
            }
            return this;
        }
        DomApiObj.addListener.call(element, "dblclick", listener);
        return this;
    },
    mouseover: function (listener) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        if (_isArray(element)) {
            for (let i = 0; i < element.length; i++) {
                DomApiObj.mouseover.call(element[i], listener);
            }
            return this;
        }
        DomApiObj.addListener.call(element, "mouseover", listener);
        return this;
    },
    mouseout: function (listener) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        if (_isArray(element)) {
            for (let i = 0; i < element.length; i++) {
                DomApiObj.mouseout.call(element[i], listener);
            }
            return this;
        }
        DomApiObj.addListener.call(element, "mouseout", listener);
        return this;
    },
    mousedown: function (listener) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        if (_isArray(element)) {
            for (let i = 0; i < element.length; i++) {
                DomApiObj.mousedown.call(element[i], listener);
            }
            return this;
        }
        DomApiObj.addListener.call(element, "mousedown", listener);
        return this;
    },
    mouseup: function (listener) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        if (_isArray(element)) {
            for (let i = 0; i < element.length; i++) {
                DomApiObj.mouseup.call(element[i], listener);
            }
            return this;
        }
        DomApiObj.addListener.call(element, "mouseup", listener);
        return this;
    },
    mouseenter: function (listener) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        if (_isArray(element)) {
            for (let i = 0; i < element.length; i++) {
                DomApiObj.mouseenter.call(element[i], listener);
            }
            return this;
        }
        DomApiObj.addListener.call(element, "mouseenter", listener);
        return this;
    },
    mouseleave: function (listener) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        if (_isArray(element)) {
            for (let i = 0; i < element.length; i++) {
                DomApiObj.mouseleave.call(element[i], listener);
            }
            return this;
        }
        DomApiObj.addListener.call(element, "mouseleave", listener);
        return this;
    },
    mousemove: function (listener) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        if (_isArray(element)) {
            for (let i = 0; i < element.length; i++) {
                DomApiObj.mousemove.call(element[i], listener);
            }
            return this;
        }
        DomApiObj.addListener.call(element, "mousemove", listener);
        return this;
    },
    touchstart: function (listener) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        if (_isArray(element)) {
            for (let i = 0; i < element.length; i++) {
                DomApiObj.touchstart.call(element[i], listener);
            }
            return this;
        }
        DomApiObj.addListener.call(element, "touchstart", listener);
        return this;
    },
    touchend: function (listener) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        if (_isArray(element)) {
            for (let i = 0; i < element.length; i++) {
                DomApiObj.touchend.call(element[i], listener);
            }
            return this;
        }
        DomApiObj.addListener.call(element, "touchend", listener);
        return this;
    },
    touchmove: function (listener) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        if (_isArray(element)) {
            for (let i = 0; i < element.length; i++) {
                DomApiObj.touchmove.call(element[i], listener);
            }
            return this;
        }
        DomApiObj.addListener.call(element, "touchmove", listener);
        return this;
    },
    touchcancel: function (listener) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        if (_isArray(element)) {
            for (let i = 0; i < element.length; i++) {
                DomApiObj.touchcancel.call(element[i], listener);
            }
            return this;
        }
        DomApiObj.addListener.call(element, "touchcancel", listener);
        return this;
    },
    focus: function (listener) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        if (_isArray(element)) {
            for (let i = 0; i < element.length; i++) {
                DomApiObj.focus.call(element[i], listener);
            }
            return this;
        }
        DomApiObj.addListener.call(element, "focus", listener);
        return this;
    },
    blur: function (listener) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        if (_isArray(element)) {
            for (let i = 0; i < element.length; i++) {
                DomApiObj.blur.call(element[i], listener);
            }
            return this;
        }
        DomApiObj.addListener.call(element, "blur", listener);
        return this;
    },
    select: function (listener) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        if (_isArray(element)) {
            for (let i = 0; i < element.length; i++) {
                DomApiObj.select.call(element[i], listener);
            }
            return this;
        }
        DomApiObj.addListener.call(element, "select", listener);
        return this;
    },
    change: function (listener) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        if (_isArray(element)) {
            for (let i = 0; i < element.length; i++) {
                DomApiObj.change.call(element[i], listener);
            }
            return this;
        }
        DomApiObj.addListener.call(element, "change", listener);
        return this;
    },
    submit: function (listener) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        if (_isArray(element)) {
            for (let i = 0; i < element.length; i++) {
                DomApiObj.submit.call(element[i], listener);
            }
            return this;
        }
        DomApiObj.addListener.call(element, "submit", listener);
        return this;
    },
    reset: function (listener) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        if (_isArray(element)) {
            for (let i = 0; i < element.length; i++) {
                DomApiObj.reset.call(element[i], listener);
            }
            return this;
        }
        DomApiObj.addListener.call(element, "reset", listener);
        return this;
    },
    onload: function (listener) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        if (_isArray(element)) {
            for (let i = 0; i < element.length; i++) {
                DomApiObj.onload.call(element[i], listener);
            }
            return this;
        }
        DomApiObj.addListener.call(element, "load", listener);
        return this;
    },
    scroll: function (listener) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        if (_isArray(element)) {
            for (let i = 0; i < element.length; i++) {
                DomApiObj.scroll.call(element[i], listener);
            }
            return this;
        }
        DomApiObj.addListener.call(element, "scroll", listener);
        return this;
    },
    unload: function (listener) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        if (_isArray(element)) {
            for (let i = 0; i < element.length; i++) {
                DomApiObj.unload.call(element[i], listener);
            }
            return this;
        }
        DomApiObj.addListener.call(element, "unload", listener);
        return this;
    },
    resize: function (listener) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        if (_isArray(element)) {
            for (let i = 0; i < element.length; i++) {
                DomApiObj.resize.call(element[i], listener);
            }
            return this;
        }
        DomApiObj.addListener.call(element, "resize", listener);
        return this;
    },
    keydown: function (listener) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        if (_isArray(element)) {
            for (let i = 0; i < element.length; i++) {
                DomApiObj.keydown.call(element[i], listener);
            }
            return this;
        }
        DomApiObj.addListener.call(element, "keydown", listener);
        return this;
    },
    keyup: function (listener) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        if (_isArray(element)) {
            for (let i = 0; i < element.length; i++) {
                DomApiObj.keyup.call(element[i], listener);
            }
            return this;
        }
        DomApiObj.addListener.call(element, "keyup", listener);
        return this;
    },
    keypress: function (listener) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        if (_isArray(element)) {
            for (let i = 0; i < element.length; i++) {
                DomApiObj.keypress.call(element[i], listener);
            }
            return this;
        }
        DomApiObj.addListener.call(element, "keypress", listener);
        return this;
    },
    input:function(listener){
        var element = this;
        if (this._el) {
            element = this._el;
        }
        if (_isArray(element)) {
            for (let i = 0; i < element.length; i++) {
                DomApiObj.input.call(element[i], listener);
            }
            return this;
        }
        DomApiObj.addListener.call(element, "input", listener);
        return this;
    },
    /**
     * 是否是dom元素
     * @param {object} element
     * @returns {boolean}
     */
    isElement: function () {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        if (_isArray(element)) {
            element = element[0];
        }
        if (typeof HTMLElement === 'object') {
            return element instanceof HTMLElement;
        }
        return element && typeof element === 'object' && element.nodeType === 1 && typeof element.nodeName === 'string';
    },
    /**
     * 是否是 DOMNode
     * @param node
     * @returns {*}
     */
    isNode: function () {
        var node = this;
        if (this._el) {
            node = this._el;
        }
        if (_isArray(node)) {
            node = node[0];
        }
        if (typeof Node === 'object') {
            return node instanceof Node;
        }
        return node && typeof node === 'object' && typeof node.nodeType === 'number' && typeof node.nodeName === 'string';
    },
    /***
     * 获取元素element，位置、高宽信息
     * ***/
    domInfo: function () {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        if (_isArray(element)) {
            element = element[0];
        }
        if (!DomApiObj.isElement.call(element)) {
            return false;
        }
        var rect = element.getBoundingClientRect();
        var info = {
            top: parseFloat(rect.top.toFixed(2)), //Math.round(rect.top)
            right: parseFloat(rect.right.toFixed(2)),
            bottom: parseFloat(rect.bottom.toFixed(2)),
            left: parseFloat(rect.left.toFixed(2)),
            width: rect.width ? parseFloat(rect.width.toFixed(2)) : parseFloat(element.offsetWidth.toFixed(2)),
            height: rect.height ? parseFloat(rect.height.toFixed(2)) : parseFloat(element.offsetHeight.toFixed(2))
        };
        if (info.width <= 0) {
            info.width = parseFloat(_getComputedStyle(element, 'width'));
        }
        if (info.height <= 0) {
            info.height = parseFloat(_getComputedStyle(element, 'height'));
        }
        let docElem = document.documentElement;
        let scrollTop = docElem.scrollTop;
        let scrollLeft = docElem.scrollLeft;
        info.pageTop = info.top + scrollTop;
        info.pageLeft = info.left + scrollLeft;
        info.scrollHeight = element.scrollHeight;
        info.scrollWidth = element.scrollWidth;
        info.offsetWidth = element.offsetWidth;
        info.offsetHeight = element.offsetHeight;
        info.pageScrollTop = scrollTop;
        info.pageScrollLeft = scrollLeft;
        info.elScrollTop = element.scrollTop;
        info.elScrollLeft = element.scrollTop;
        return info;
    },
    /*****
     * selector api 返回数组
     * element：元素
     * selector：刷选条件
     * *****/
    findBySelector: function (selector) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        if (_isArray(element)) {
            element = element[0];
        }
        var result = [];
        if (DomApiObj.isNode.call(element)) {
            result = element.querySelectorAll(selector);
            result = Array.from(result);
        } else {
            result = document.querySelectorAll(selector);
            result = Array.from(result);
        }

        return result;
    },
    findById:function(args){
        return this.findbyId(args);
    },
    findByid:function(args){
        return this.findbyId(args);
    },
    /***
     * 根据id查找元素
     * arg1:id / dom元素
     * arg2:当arg1是dom时候，arg2为id
     * ***/
    findbyId: function (arg2) {
        var arg1 = this;
        if (this._el) {
            arg1 = this._el;
        }
        if (_isArray(arg1)) {
            arg1 = arg1[0];
        }
        if (typeof arg2 === "undefined") {
            if (arg1.substring(0, 1) === "#") {
                arg1 = arg2.substring(1);
            }
            return document.getElementById(arg1);
        } else {
            if (arg1.querySelector) {
                if (arg2.substring(0, 1) !== "#") {
                    arg2 = "#" + arg2;
                }
                return arg1.querySelector(arg2);
            } else {
                if (arg2.substring(0, 1) === "#") {
                    arg2 = arg2.substring(1);
                }
                return document.getElementById(arg1);
            }

        }
    },
    /***
     * 根据tag Name查询元素
     * arg1:name / dom元素
     * arg2:当arg1是dom时候，arg2为name
     * ***/
    findByTagName: function (arg2) {       
        var arg1 = this;
        if (this._el) {
            arg1 = this._el;
        }
        if (_isArray(arg1)) {
            arg1 = arg1[0];
        }
        let ret;
        let s;
        if (typeof arg2 === "undefined") {
            s = getNameAndAttr(arg1);
            ret = document.getElementsByTagName(s.name);           
        } else {
            s = getNameAndAttr(arg2);
            ret = arg1.querySelectorAll(s.name);
        }
        if(s.attr){
            let res = [];
            let arr = s.attr.split("=");
            let name = arr[0];
            let val = arr[1];
            for(let i =0 ; i < ret.length ;i++){
                let el = ret[i];
                if($B.DomUtils.attribute(el,name) === val){
                    res.push(el);
                }
            }
            ret = res;
        }else{
            ret = Array.from(ret);
        }
        return ret;
    },
    /***
     * 根据class查询元素
     * arg1：className / dom元素
     * arg2：当arg1是dom元素，arg2位className
     * ***/
    findByClass: function (arg2) {
        var arg1 = this;
        if (this._el) {
            arg1 = this._el;
        }
        if (_isArray(arg1)) {
            arg1 = arg1[0];
        }
        let ret;
        if (typeof arg2 === "undefined") {
            if (arg1.substring(0, 1) === ".") {
                arg1 = arg1.substring(1);
            }
            if (document.querySelectorAll) {
                ret = document.querySelectorAll("." + arg1);
            }
            if (document.getElementsByClassName) {
                ret = document.getElementsByClassName(arg1);
            }
        } else if (arg1.querySelectorAll) {
            if (arg2.substring(0, 1) !== ".") {
                arg2 = "." + arg2;
            }
            ret = arg1.querySelectorAll(arg2);
        }
        var arr = [];
        for (let i = 0; i < ret.length; i++) {
            arr.push(ret[i]);
        }
        return arr;
    },
    /***
     * 获取元素element的父元素
     * ***/
    parent: function () {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        if (_isArray(element)) {
            element = element[0];
        }
        if (!DomApiObj.isNode.call(element)) {
            throw new Error(element + " is not a DOMNode object");
        }
        return element.parentNode;
    },
    /***
     * 获取element的子元素，arg:元素名称 / className / id / true获取包含文本元素的所有子元素
     * ****/
    children: function (arg) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        if (_isArray(element)) {
            element = element[0];
        }
        let _type = typeof arg;
        if (_type === 'boolean' && arg) {
            return element.childNodes;
        }
        var result = [], i, child;
        if (_isString(arg)) {
            let firstChar = arg.substring(0, 1);
            let tagName = arg.toLowerCase();
            if (firstChar === "#") {
                let r = DomApiObj.getChildrenById.call(element, arg);
                return r;
            }
            if (firstChar === ".") {
                return DomApiObj.getChildrenByClass.call(element, arg);
            }
            for (i = 0; i < element.childNodes.length; i++) {
                child = element.childNodes[i];
                if (child.nodeName.toLowerCase() === tagName) {
                    result.push(child);
                }
            }

            return result;
        }
        for (i = 0; i < element.childNodes.length; ++i) {
            child = element.childNodes[i];
            if (child.nodeType === 1) {
                result.push(child);
            }
        }
        return result;
    },
    /***
     * 根据className 获取element的子元素
     * ****/
    getChildrenByClass: function (clazz) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        if (_isArray(element)) {
            element = element[0];
        }
        var ret = [];
        var childs = element.children;
        for (var i = 0; i < childs.length; i++) {
            if (DomApiObj.hasClass.call(childs[i], clazz)) {
                ret.push(childs[i]);
            }
        }
        return ret;
    },
    /***
     * 根据id获取element元素的子元素
     * ***/
    getChildrenById: function (id) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        if (_isArray(element)) {
            element = element[0];
        }
        var childs = element.children;
        if (id.indexOf("#") === 0) {
            id = id.substring(1);
        }
        for (var i = 0; i < childs.length; i++) {
            if (id === DomApiObj.attribute.call(childs[i], "id")) {
                return childs[i];
            }
        }
        return undefined;
    },
    /**
     * 根据标签名称获取element的子元素
     * ****/
    getChildrenByName: function (name) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        if (_isArray(element)) {
            element = element[0];
        }
        var ret = [];
        var childs = element.children;
        var isArr = _isArray(name);
        for (var i = 0; i < childs.length; i++) {
            if (isArr) {
                if (_isInArray(childs[i].nodeName, name)) {
                    ret.push(childs[i]);
                }
            } else {
                if (childs[i].nodeName === name) {
                    ret.push(childs[i]);
                }
            }
        }
        return ret;
    },
    /***
     * 获取所有相邻节点
     * canTextNode是否包括文本节点,或者是class类名
     * *****/
    siblings: function (canTextNode) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        if (_isArray(element)) {
            element = element[0];
        }        
        var nextAll = DomApiObj.nextAll.call(element, canTextNode);
        var preAll = DomApiObj.previousAll.call(element, canTextNode);
        preAll.push.apply(preAll, nextAll);
        return preAll;
    },
    forSiblings:function(fn){
        var element = this;
        if (this._el) {
            element = this._el;
        }
        if (_isArray(element)) {
            element = element[0];
        }  
        let prev = element.previousSibling;  
        while(prev ){
            if(prev.nodeType === 1){
               let r = fn(prev);
               if(r !== undefined && !r){
                    break;
               }
            }
            prev = prev.previousSibling;
        }   
        let next = element.nextSibling;  
        while(next ){
            if(next.nodeType === 1){               
                let r = fn(next);
               if(r !== undefined && !r){
                    break;
               }
            }
            next = next.nextSibling;
        }  
    },
    /**
     * 获取元素的下一个同级元素，
     * canTextNode：是否包括文本节点
     * ****/
    next: function (canTextNode) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        if (_isArray(element)) {
            element = element[0];
        }
        if (!DomApiObj.isNode.call(element)) {
            throw new Error(element + " is not a DOMNode object");
        }
        var result = element.nextSibling;
        if (!result) {
            return;
        }
        if (canTextNode) {
            return result;
        }
        if (result.nodeType !== 1) {
            return DomApiObj.next.call(result);
        }
        return result;
    },
    /***
     * 获取element元素后面的所有同级元素
     * canTextNode：是否包括文本节点,或者是class类名
     * ***/
    nextAll: function (canTextNode) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        if (_isArray(element)) {
            element = element[0];
        }
        let clz = (typeof canTextNode === "string" && canTextNode.indexOf(".")) === 0 ? canTextNode : undefined;
        if(clz){
            canTextNode = false;
        }
        var ret = [];
        var nextEle = DomApiObj.next.call(element, canTextNode);
        while (nextEle) {
            if(clz){
                if(DomApiObj.hasClass.call(nextEle,clz)){
                    ret.push(nextEle);
                }              
            }else{
                ret.push(nextEle);
            }            
            nextEle = DomApiObj.next.call(nextEle, canTextNode);
        }
        return ret;
    },
    /***
     * 获取element元素的前一个同级元素
     * canTextNode：是否包括文本节点
     * ***/
    previous: function (canTextNode) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        if (_isArray(element)) {
            element = element[0];
        }
        if (!DomApiObj.isNode.call(element)) {
            throw new Error(element + " is not a DOMNode object");
        }
        var result = element.previousSibling;
        if (!result) {
            return;
        }
        if (canTextNode) {
            return result;
        }
        if (result.nodeType !== 1) {
            return DomApiObj.previous.call(result);
        }
        return result;
    },
    /***
     * 获取element元素的前所有的同级元素
     * canTextNode：是否包括文本节点,或者是class类名
     * ***/
    previousAll: function (canTextNode) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        if (_isArray(element)) {
            element = element[0];
        }
        let clz = (typeof canTextNode === "string" && canTextNode.indexOf(".")) === 0 ? canTextNode : undefined;
        if(clz){
            canTextNode = false;
        }
        var ret = [];
        var preEle = DomApiObj.previous.call(element, canTextNode);
        while (preEle) {
            if(clz){
                if(DomApiObj.hasClass.call(preEle,clz)){
                    ret.unshift(preEle);
                }                
            }else{
                ret.unshift(preEle);
            } 
            preEle = DomApiObj.previous.call(preEle, canTextNode);
        }
        return ret;
    },
    /****
     * 设置/获取element的属性,获取属性:attribute=string/[s1,s2]
     * 设置属性attribute={}
     * ****/
    attribute: function (attribute) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        var isArr = _isIterable(element);
        if (isArr && _isLiteralObject(attribute)) { //多个元素同时设置属性
            _each(element, function (e) {
                DomApiObj.attribute.call(e, attribute);
            });
            return this;
        }
        if (isArr) {
            element = element[0];
        }
        if (!DomApiObj.isNode.call(element)) {
            throw new Error(element + " is not a DOMNode object");
        }
        var result, i, keys;
        //获取一个属性
        if (typeof attribute === "string") {
            if (attribute === 'class' && element['className'] !== undefined) {//class
                result = element.className;
            } else if (attribute === 'for' && element['htmlFor'] !== undefined) {//for
                result = element.htmlFor;
            } else if (attribute === 'value' && element['value'] !== undefined) {//value?               
                return element.value;
            } else {
                if(attribute === "checked"){
                    result =   element.checked;
                }else{
                    result = element.getAttribute(attribute);
                }
            }            
            if (result === null) {
                result = undefined;
            }
            return result;
        }
        //获取多个
        if (_isArray(attribute)) {
            keys = Object.keys(attribute);
            for (i = 0; i < keys.length; ++i) {
                result[attribute[keys[i]]] = DomApiObj.attribute.call(element, attribute[keys[i]]);
            }
            return result;
        }
        //设置属性
        if (_isLiteralObject(attribute)) {
            keys = Object.keys(attribute);
            for (i = 0; i < keys.length; ++i) {
                if (attribute[keys[i]] === null || attribute[keys[i]] === undefined) {
                    element.removeAttribute(keys[i]);
                    if(keys[i] === "checked"){
                        element.checked = false;
                    }
                    if(keys[i] === "selected"){
                        element.selected = false;
                    }
                } else {                   
                    if(keys[i] === "checked"){
                        element.checked = attribute[keys[i]];
                    }else if(keys[i] === "selected"){
                        element.selected = attribute[keys[i]];
                    }else{
                        element.setAttribute(keys[i], attribute[keys[i]]);
                    }
                }
            }
            return this;
        }
    },
    /***
     * 获取或者设置元素element的宽度
     * ***/
    width: function (width) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        var ret = getSetWH(element, "width", width);
        if (typeof ret === "undefined") {
            return this;
        }
        return ret;
    },
    /***
     * 获取或者设置元素element的高度
     * ***/
    height: function (height) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        var ret = getSetWH(element, "height", height);
        if (typeof ret === "undefined") {
            return this;
        }
        return ret;
    },
    /**
     * 获取或者设置元素element的内宽度
     * ***/
    innerWidth: function (width) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        var ret = getSetWH(element, "width", width, "inner");
        if (typeof ret === "undefined") {
            return this;
        }
        return ret;
    },
    /**
     * 获取或者设置元素element的内高度
     * ***/
    innerHeight: function (height) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        var ret = getSetWH(element, "height", height, "inner");
        if (typeof ret === "undefined") {
            return this;
        }
        return ret;
    },
    /**
     * 获取或者设置元素element的外宽度
     * ***/
    outerWidth: function (width) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        var ret = getSetWH(element, "width", width, "outer");
        if (typeof ret === "undefined") {
            return this;
        }
        return ret;
    },
    /**
     * 获取或者设置元素element的外高度
     * ***/
    outerHeight: function (height) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        var ret = getSetWH(element, "height", height, "outer");
        if (typeof ret === "undefined") {
            return this;
        }
        return ret;
    },
    /****
     * 设置/获取element元素的scrollLeft
     * *****/
    scrollLeft: function (left) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        var ret = _getSetScroll(element, "scrollLeft", left);
        if (typeof ret === "undefined") {
            return this;
        }
        return ret;
    },
    /****
     * 设置/获取element元素的scrollTop
     * *****/
    scrollTop: function (top) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        var ret = _getSetScroll(element, "scrollTop", top);
        if (typeof ret === "undefined") {
            return this;
        }
        return ret;
    },
    /***
     * 设置/获取element元素的position
     * *****/
    position: function (pos) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        if (pos) {//设置位置
            DomApiObj.css.call(element, pos);
            return this;
        } else {//获取位置属性
            if (Array.isArray(element)) {
                element = element[0];
            }
            var offsetParent, offset,
                parentOffset = { top: 0, left: 0 };
            if (!DomApiObj.inDom.call(element)) {
                return parentOffset;
            }
            if (DomApiObj.css.call(element, "position") === "fixed") {
                offset = element.getBoundingClientRect();
            } else {
                offsetParent = DomApiObj.getOffsetParent.call(element);
                offset = DomApiObj.offset.call(element);
                if (offsetParent.nodeName.toLowerCase() !== "html") {
                    parentOffset = DomApiObj.offset.call(offsetParent);
                }
                parentOffset.top += DomApiObj.css.call(offsetParent, "borderTopWidth");
                parentOffset.left += DomApiObj.css.call(offsetParent, "borderLeftWidth");
            }
            return {
                top: offset.top - parentOffset.top - DomApiObj.css.call(element, "marginTop"),
                left: offset.left - parentOffset.left - DomApiObj.css.call(element, "marginLeft")
            };
        }
    },
    /***
     * 元素是否脱离了文档
     * ***/
    inDom() {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        if (_isArray(element)) {
            element = element[0];
        }
        let r = window.document.contains(element);
        if(!r){
            let prt = element.parentNode;
            while(prt){
                if(prt.nodeName === "BODY"){                  
                    r = true;
                    break;
                }
                prt = prt.parentNode;
            }
        }
        return r;
    },
    /***
     * 获取element元素相对位置的参考父元素
     * ****/
    getOffsetParent() {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        element = _getArrayFirstEl(element);
        var offsetParent = element.offsetParent;
        while (offsetParent && offsetParent.nodeName.toLowerCase() !== "html" && DomApiObj.css.call(offsetParent, "position") === "static") {
            offsetParent = offsetParent.offsetParent;
        }
        return offsetParent || document.documentElement;
    },
    /***
     * 获取element的相对视窗的偏移量数据
     * ****/
    offset: function () {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        element = _getArrayFirstEl(element);
        var ofs = {
            top: 0,
            left: 0
        };
        if (!DomApiObj.inDom.call(element)) {
            return ofs;
        }
        if (element.getBoundingClientRect) {
            let box = element.getBoundingClientRect();
            ofs.top = box.top;
            ofs.left = box.left;
        }
        let docElem = element.ownerDocument.documentElement;
        let ret = {
            top: ofs.top + (window.pageYOffset || docElem.scrollTop) - (docElem.clientTop || 0),
            left: ofs.left + (window.pageXOffset || docElem.scrollLeft) - (docElem.clientLeft || 0)
        };
        return ret;
    },
    /****
     * 设置/获取元素的css样式，获取时候style参数必须是字符串的css名称
     * ****/
    css: function (style) {       
        var element = this;
        if (this._el) {
            element = this._el;
        }
        var isArr = _isIterable(element);
        if (isArr && _isLiteralObject(style)) { //多个元素一起设置样式
            _each(element, function (e) {
                DomApiObj.css.call(e, style);
            });
            return this;
        }
        if (isArr) { //是数组
            if(element.length === 0){
                return;
            }
            element = element[0];
        }
        if (!DomApiObj.isNode.call(element)) {
            throw new Error(element + " is not a DOMNode object");
        }
        if (typeof style === "string") {
            return _getComputedStyle(element, cssNameProperty(style));
        }
        var keys, i;
        if (_isArray(style)) {
            var css = {};
            keys = Object.keys(style);
            for (i = 0; i < keys.length; ++i) {
                css[style[keys[i]]] = _getComputedStyle(element, cssNameProperty(style[keys[i]]));
            }
            return css;
        }
        if (_isLiteralObject(style)) {
            keys = Object.keys(style);
            for (i = 0; i < keys.length; ++i) {
                let cName = cssNameProperty(keys[i]);
                let value = style[keys[i]] + "";                              
                if (value.indexOf("#") < 0 && value.indexOf("%") < 0 && /^-?\d+\.?\d*$/.test(value) && !CSSNumber[cName]) {
                    value = value + "px";
                }
                if(cName === "backgroundColor" && value === "none"){
                    value = "transparent";
                }
                element.style[cName] = value;
            }
            return this;
        }
    },
    /***
     * 获取元素element的class
     * ***/
    getClass: function () {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        element = _getArrayFirstEl(element);
        if (!DomApiObj.isNode.call(element)) {
            throw new Error(element + " is not a DOMNode object");
        }
        var attribute = DomApiObj.attribute.call(element, 'class');
        if (!attribute) {
            return [];
        }
        attribute = attribute.split(' ');
        var classNames = [], keys, i;
        keys = Object.keys(attribute);
        for (i = 0; i < keys.length; ++i) {
            if (attribute[keys[i]] === '') {
                continue;
            }
            classNames.push(attribute[keys[i]]);
        }
        return classNames;
    },
    /***
     * element元素是否存在className，className可以是数组
     * ****/
    hasClass: function (className) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        element = _getArrayFirstEl(element);
        if (!DomApiObj.isNode.call(element)) {
            throw new Error(element + " is not a DOMNode object");
        }
        if (_isString(className)) {
            if (className.indexOf(".") === 0) {
                className = className.substring(1);
            }
            return _indexOf(DomApiObj.getClass.call(element), className) > -1 ? true : false;
        } else if (_isArray(className)) {
            var elementClasses = DomApiObj.getClass.call(element);
            for (var i = 0; i < className.length; i++) {
                if (_indexOf(className[i], elementClasses) === -1) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    },
    /****
     * element元素添加class
     * ****/
    addClass: function (className) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        if (_isIterable(element)) {
            _each(element, function (e) {
                DomApiObj.addClass.call(e, className);
            });
            return this;
        }
        if (!DomApiObj.isNode.call(element)) {
            throw new Error(element + " is not a DOMNode object");
        }
        if (_isArray(className)) {
            var i, keys = Object.keys(className);
            for (i = 0; i < keys.length; i++) {
                DomApiObj.addClass.call(element, className[keys[i]]);
            }
            return DomUtils;
        }
        var classes = DomApiObj.getClass.call(element);
        if (_indexOf(classes, className) === -1) {
            classes.push(className);
        }
        classes = classes.join(' ');
        DomApiObj.attribute.call(element, { class: classes });
        return this;
    },
    /*****
     * 移除元素的class
     * ******/
    removeClass: function (className) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        if (_isIterable(element)) {
            _each(element, function (e) {
                DomApiObj.removeClass.call(e, className);
            });
            return this;
        }
        if (!DomApiObj.isNode.call(element)) {
            throw new Error("DomUtils.removeClass" + element + " is not a DOMNode object");
        }
        if (!className) {
            return DomApiObj.attribute.call(element, { class: "" });
        }     
        var classes = DomApiObj.getClass.call(element);
        var arr = className.split(/\s+/);
        for (let i = 0; i < arr.length; i++) {
            let j = _indexOf(classes, arr[i]);
            if (j !== -1) {
                classes.splice(j, 1);
            }
        }
        DomApiObj.attribute.call(element, { class: classes.join(' ') });
        return this;
    },
    /***
     * 复制元素
     * isDeep是否深复制，复制数据及事件
     * ****/
    copy: function (isDeep) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        element = _getArrayFirstEl(element);
        if (!DomApiObj.isNode.call(element)) {
            throw new Error("DomUtils.copy" + element + " is not a DOMNode object");
        }
        var newEl = element.cloneNode(true);
        if (isDeep) {
            var events = element.events;
            if (events) {
                newEl.events = events;//复用复制的事件
                let namekeys = Object.keys[events];
                for (let i = 0; i < namekeys.length; i++) {
                    let eventName = namekeys[i];
                    if (newEl.addEventListener) {
                        newEl.addEventListener(eventName, commHandleEvent, false);
                    } else {
                        newEl["on" + eventName] = commHandleEvent;
                    }
                }
            }
            var _eldata = element._eldata;
            if (_eldata) {
                newEl._eldata = _eldata;
            }
        }
        return newEl;
    },
    /**
     * 往元素设置/读取html内容
     * ***/
    html: function (string) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        var isArr = _isIterable(element);
        if (isArr && _isString(string)) {
            _each(element, function (e) {
                DomApiObj.html.call(e, string);
            });
            return this;
        }
        if (!DomApiObj.isNode.call(element)) {
            throw new Error("DomUtils.html" + element + " is not a DOMNode object");
        }
        if (_isString(string)) {
            element.innerHTML = string;
            return this;
        }
        element = _getArrayFirstEl(element);
        return element.innerHTML;
    },
    /**
     * 往元素设置/读取innertText
     * ***/
    text: function (string) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        var isArr = _isIterable(element);
        if (isArr && _isString(string)) {
            _each(element, function (e) {
                DomApiObj.text.call(e, string);
            });
            return this;
        }
        if (!DomApiObj.isNode.call(element)) {
            throw new Error("DomUtils.text " + element + " is not a DOMNode object");
        }
        if (_isString(string)) {
            if (element.innerText) {
                element.innerText = string;
            } else {
                element.textContent = string;
            }
            return this;
        }
        element = _getArrayFirstEl(element);
        if (element.innerText) {
            return element.innerText;
        }
        return element.textContent;
    },
    /***
     * 设置或者获取元素的value值
     * ***/
    value: function (value) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        element = _getArrayFirstEl(element);
        var hooks, ret;
        if (typeof value === "undefined") {//获取值
            hooks = valHooks[element.nodeName.toLowerCase()];
            if (hooks && hooks.get) {
                return hooks.get(element);
            }
            ret = element.value;
            return typeof ret === "string" ? _trim(ret) : ret == null ? "" : ret;
        } else {//设置值
            hooks = valHooks[element.nodeName.toLowerCase()];
            if (hooks && hooks.set) {
                return hooks.set(element);
            }
            element.value = value;
        }
        return this;
    },
    /**
     * 判定元素是否处于隐藏状态
     * ***/
    isHide: function () {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        element = _getArrayFirstEl(element);
        return DomApiObj.css.call(element, "display") === "none";
    },
    /***
     * 显示元素
     * ***/
    show: function (animate) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        var isArr = _isIterable(element);
        if (isArr) {
            _each(element, function (e) {
                DomApiObj.show.call(e);
            });
            return this;
        }
        if (animate && animate.length > 1) {
            let args = [];
            for (let i = 0; i < animate.length; i++) {
                args.push(animate[i]);
            }
            $B.show.apply(this, args);
        } else {
            element.style.display = "";
            if(  element.style.opacity === '0' || element.style.opacity === 0){
                element.style.opacity = 1;
            }
        }
        return this;
    },
    /**
     * 隐藏元素
     * ***/
    hide: function (animate) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        var isArr = _isIterable(element);
        if (isArr) {
            _each(element, function (e) {
                DomApiObj.hide.call(e);
            });
            return this;
        }
        if (animate && animate.length > 1) {
            let args = [];
            for (let i = 0; i < animate.length; i++) {
                args.push(animate[i]);
            }
            $B.hide.apply(this, args);
        } else {
            //element.style.display = "";
            element.style.display = "none";
        }
        return this;
    },
    /**
     * 将html/元素添加到element元素内部的后面
     * ***/
    append: function (html) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        var res = _elementInsert(element, 'append', html);
        if (res) {
            return res;
        }
        if (!DomApiObj.isNode.call(element)) {
            throw new Error("DomUtils.append " + element + " is not a DOMNode object");
        }
        if (Array.isArray(html)) {
            var ret = [];
            for (let i = 0; i < html.length; i++) {
                ret.push(DomApiObj.append.call(element, html[i]));
            }
            return ret;
        }
        if (_isString(html)) {
            html = DomUtils.createEl(html);
        }     
        if(Array.isArray(html)){
            for(let i =0 ; i < html.length ;i++){
                element.appendChild(html[i]);
            }
        }else{
            element.appendChild(html);
        }
        clearTimeout(_onNodeRemoveEventTimer);
        return html;
    },
    /**
     * 将html/元素添加到element元素内部的前面
     * ***/
    prepend: function (html) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        var res = _elementInsert(element, 'prepend', html);
        if (res) {
            return res;
        }
        if (!DomApiObj.isNode.call(element)) {
            throw new Error("DomUtils.prepend " + element + " is not a DOMNode object");
        }
        if (Array.isArray(html)) {
            var ret = [];
            for (let i = 0; i < html.length; i++) {
                ret.push(DomApiObj.prepend.call(element, html[i]));
            }
            return ret;
        }
        if (_isString(html)) {
            html = DomUtils.createEl(html);
        }
        if(Array.isArray(html)){
            for(let i =0 ; i < html.length ;i++){
                element.insertBefore(html[i], element.firstChild);
            }
        }else{
            element.insertBefore(html, element.firstChild);
        }        
        clearTimeout(_onNodeRemoveEventTimer);
        return html;
    },
    /***
     * 将html/元素插入到element后面
     * ***/
    after: function (html) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        var res = _elementInsert(element, 'after', html);
        if (res) {
            return res;
        }
        if (!DomApiObj.isNode.call(element)) {
            throw new Error("DomUtils.after " + element + " is not a DOMNode object");
        }
        if (Array.isArray(html)) {
            var ret = [];
            for (let i = 0; i < html.length; i++) {
                ret.push(DomApiObj.after.call(element, html[i]));
            }
            return ret;
        }
        if (_isString(html)) {
            html = DomUtils.createEl(html);
        }
        if(Array.isArray(html)){
            for(let i =0 ; i < html.length ;i++){
                element.parentNode.insertBefore(html[i], element.nextSibling);
            }
        }else{
            element.parentNode.insertBefore(html, element.nextSibling);
        }        
        clearTimeout(_onNodeRemoveEventTimer);
        return html;
    },
    /***
     * 将html/元素插入到element前面
     * ***/
    before: function (html) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        var res = _elementInsert(element, 'before', html);
        if (res) {
            return res;
        }
        if (!DomApiObj.isNode.call(element)) {
            throw new Error("DomUtils.before " + element + " is not a DOMNode object");
        }
        if (Array.isArray(html)) {
            var ret = [];
            for (let i = 0; i < html.length; i++) {
                ret.push(DomApiObj.before.call(element, html[i]));
            }
            return ret;
        }
        if (_isString(html)) {
            html = DomUtils.createEl(html);
        }
        if(Array.isArray(html)){
            for(let i =0 ; i < html.length ;i++){
                element.parentNode.insertBefore(html[i], element);
            }
        }else{
            element.parentNode.insertBefore(html, element);
        }        
        clearTimeout(_onNodeRemoveEventTimer);
        return html;
    },
    /***
     * 将html/元素替换element前面
     * ***/
    replace: function (html) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        var res = _elementInsert(element, 'replace', html);
        if (res) {
            return res;
        }
        if (!DomApiObj.isNode.call(element)) {
            throw new Error("DomUtils.replace " + element + " is not a DOMNode object");
        }
        if (Array.isArray(html)) {
            var ret = [];
            for (let i = 0; i < html.length; i++) {
                ret.push(DomApiObj.replace.call(element, html[i]));
            }
            return ret;
        }
        if (_isString(html)) {
            html = DomUtils.createEl(html);
        }
        DomApiObj.clearElement.call(element);
        element.parentNode.replaceChild(html, element);
        return html;
    },
    /**
     * 清理element的数据和事件
     * **/
    clearElement: function () {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        if (forachExecute(element, "clearElement")) {
            return this;
        }
        element._eldata = undefined;
        var events = element.events;
        if (events) {
            element.events = undefined;
            var eventKeys = Object.keys(events);
            for (let i = 0; i < eventKeys.length; i++) {
                let eventName = eventKeys[i];
                element.removeEventListener(eventName, commHandleEvent);//移除通用处理
                events[eventName] = undefined;
            }
        }
        return this;
    },
    /**
     * 移除element下所有子元素
     * 或者是某个class的子元素
     * **/
    removeChilds: function (args) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        if (forachExecute(element, "removeChilds")) {
            return this;
        }
        var ret = [];
        for (let i = 0; i < element.childNodes.length; i++) {
            let child = element.childNodes[i];
            if (args ) {
                if ( child.nodeType === 1  && args.indexOf(".") === 0) { //按class
                    if (DomApiObj.hasClass.call(child, args.substring(1))) {
                        ret.push(child);
                    }
                }
            } else {
                ret.push(child);
            }
        }
        for (let i = 0; i < ret.length; i++) {
            let child = ret[i];
            DomApiObj.clearElement.call(child);
            element.removeChild(child);
        }
        return ret;
    },
    /**
     * 移除element元素,会将相关数据和事件移除
     * ***/
    remove: function () {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        if (forachExecute(element, "remove")) {
            return;
        }
        if (!DomApiObj.isNode.call(element)) {
            throw new Error("DomUtils.remove " + element + " is not a DOMNode object");
        }
        var parent = element.parentNode;
        DomApiObj.clearElement.call(element);
        if (parent) {
            return parent.removeChild(element);
        }
        return element;
    },
    /**
     * 移除element元素,并且还保留数据和事件移除
     * ***/
    detach: function () {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        let exeRet = forachExecute(element, "detach");
        if (exeRet) {
            return exeRet;
        }
        if (!DomApiObj.isNode.call(element)) {
            throw new Error("DomUtils.remove " + element + " is not a DOMNode object");
        }
        var parent = element.parentNode;
        if(parent){
            element._isRetainEvent = true; //跳过domRemove监听，不需要清除元素上的数据和事件   
            parent.removeChild(element);
            setTimeout(function () {
                element._isRetainEvent = undefined;
            }, 10);
        }       
        return element;
    },
    /***
     * 移除element下的所有元素（保留事件-数据），并将这些元素数组形式返回
     * ***/
    detachChilds: function (args) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        if (Array.isArray(element)) {
            element = element[0];
        }
        if (!DomApiObj.isNode.call(element)) {
            throw new Error("DomUtils.remove " + element + " is not a DOMNode object");
        }
        var ret = [];
        while (element.childNodes.length > 0) {
            let child = element.childNodes[0];
            if (args) {
                if (args.indexOf(".") === 0) { //按class
                    if (!DomApiObj.hasClass.call(child, args.substring(1))) {
                        continue;
                    }
                }
            }
            DomApiObj.detach.call(child);
            if (!(child.nodeType === 3 && $B.replaceSpaceChar(child.nodeValue) === "")) {
                ret.push(child);
            }
        }
        return ret;
    },
    /***
     * 设置，读取简单类型的属性，属性数据会在标签上形成data-xxx属性
     * args:属性名称，或者{prop:value},移除数据用{name:undefined/null}
     * ***/
    propData: function (args) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        var isArr = _isIterable(element);
        var isSet = _isLiteralObject(args);
        if (isArr && isSet) {
            _each(element, function (e) {
                DomApiObj.propData.call(e, args);
            });
            return true;
        }
        if (isArr) {
            element = element[0];
        }
        if (!DomApiObj.isNode.call(element)) {
            throw new Error("DomUtils.data " + element + " is not a DOMNode object");
        }
        var i, dataset, attrs, keys, attr, l;
        //获取元素上的所有数据
        if (args === undefined) {
            if (element.hasOwnProperty('dataset')) {
                return element.dataset;
            } else {//通过属性获取
                dataset = {};
                for (i = 0, l = element.attributes.length; i < l; i++) {
                    attr = element.attributes.item(i);
                    if (attr.nodeName.substr(0, 5) !== 'data-') {
                        continue;
                    }
                    dataset[attr.nodeName.substr(5)] = attr.nodeValue;
                }
                return dataset;
            }
        }
        //获取一个属性
        if (_isString(args)) {
            return DomApiObj.attribute.call(element, 'data-' + args);
        }
        //获取多个数据
        if (_isArray(args)) {
            dataset = {};
            for (i = 0, l = args.length; i < l; i++) {
                dataset[args[i]] = DomApiObj.attribute.call(element, 'data-' + args[i]);
            }
            return dataset;
        }
        //设置数据{}
        if (isSet) {
            attrs = {};
            keys = Object.keys(args);
            for (i = 0; i < keys.length; ++i) {
                attrs['data-' + keys[i]] = args[keys[i]];
            }
            DomApiObj.attribute.call(element, attrs);
        }
        return this;
    },
    /***
     * 移除简单类型属性
     * key=属性名称
     * ****/
    removePropData: function (key) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        var isArr = _isIterable(element);
        if (isArr) {
            _each(element, function (e) {
                DomApiObj.removePropData.call(e, key);
            });
            return;
        }
        key = "data-" + key;
        element.removeAttribute(key);
        return this;
    },
    removeAttribute: function (key) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        var isArr = _isIterable(element);
        if (isArr) {
            _each(element, function (e) {
                DomApiObj.removeAttribute.call(e, key);
            });
            return;
        }
        element.removeAttribute(key);
        return this;
    },
    /**
     * 设置元素数据，key：数据key，dataObject数据对象
     * 该api会在element._eldata上生成数据对象
     * ***/
    setData: function (key, dataObject) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        var isArr = _isIterable(element);
        if (isArr) {
            _each(element, function (e) {
                DomApiObj.setData.call(e, key, dataObject);
            });
            return true;
        }
        var cached = element._eldata;
        if (!cached) {
            cached = {};
            element._eldata = cached;
        }
        if (dataObject === undefined || dataObject === null) {
            delete cached[key];
        } else {
            cached[key] = dataObject;
        }
        return this;
    },
    /***
     * 根据key获取element元素的数据
     * ***/
    getData: function (key) {
        var element = this;
        if (this._el) {
            element = this._el;
        }
        element = _getArrayFirstEl(element);
        var cached = element._eldata;
        if (!cached) {
            return undefined;
        }
        return cached[key];
    }
};
DomUtils = {
    $: function (arg1, arg2) {
        let el = arg1;
        var obj = {
            _el: el
        };
        obj["__proto__"] = DomApiObj;
        return obj;
    },
    /***
     * 对元素添加一个事件监听
     * element：元素
     * eventName ：事件名称,支持命名空间 namespac.click
     * listener:事件处理函数
     * params:额外的参数
     * **/
    addListener: function (element, eventName, listener, params) {
        return DomApiObj.addListener.call(element, eventName, listener, params);
    },
    /***
     * 对元素移除事件
     * element：元素
     * eventName ：事件名称,支持命名空间 namespac.click
     * listener:事件处理函数
     * **/
    removeListener: function (element, eventName, listener) {
        return DomApiObj.removeListener.call(element, eventName, listener);
    },
    /***
     * 触发事件
     * ****/
    trigger: function (element, eventName, args) {
        return DomApiObj.trigger.call(element, eventName, args);
    },
    /***
     * 接触element元素的某一类事件
     * eventName=click、mousedown、mouseup 事件名称或者 namespace.eventName 或者 namespace.*
     * ***/
    offEvents: function (element, eventName) {
        return DomApiObj.offEvents.call(element, eventName);
    },
    /***
     * 绑定事件
     * ***/
    bind: function (element, arg1, arg2) {
        if (_isLiteralObject(arg1)) {
            return DomApiObj.bind.call(element, arg1);
        } else if (_isFunction(arg2)) {
            if (DomApiObj[arg1]) {
                return DomApiObj[arg1].call(element, arg2);
            }
        }
    },
    click: function (element, listener) {
        return DomApiObj.click.call(element, listener);
    },
    contextmenu: function (element, listener) {
        return DomApiObj.contextmenu.call(element, listener);
    },
    paste:function(element, listener){
        return DomApiObj.paste.call(element, listener);
    },
    dblclick: function (element, listener) {
        return DomApiObj.dblclick.call(element, listener);
    },
    mouseover: function (element, listener) {
        return DomApiObj.mouseover.call(element, listener);
    },
    mouseout: function (element, listener) {
        return DomApiObj.mouseout.call(element, listener);
    },
    mousedown: function (element, listener) {
        return DomApiObj.mousedown.call(element, listener);
    },
    mouseup: function (element, listener) {
        return DomApiObj.mouseup.call(element, listener);
    },
    mouseenter: function (element, listener) {
        return DomApiObj.mouseenter.call(element, listener);
    },
    mouseleave: function (element, listener) {
        return DomApiObj.mouseleave.call(element, listener);
    },
    mousemove: function (element, listener) {
        return DomApiObj.mousemove.call(element, listener);
    },
    touchstart: function (element, listener) {
        return DomApiObj.touchstart.call(element, listener);
    },
    touchend: function (element, listener) {
        return DomApiObj.touchend.call(element, listener);
    },
    touchmove: function (element, listener) {
        return DomApiObj.touchmove.call(element, listener);
    },
    touchcancel: function (element, listener) {
        return DomApiObj.touchcancel.call(element, listener);
    },
    focus: function (element, listener) {
        return DomApiObj.focus.call(element, listener);
    },
    blur: function (element, listener) {
        return DomApiObj.blur.call(element, listener);
    },
    select: function (element, listener) {
        return DomApiObj.select.call(element, listener);
    },
    change: function (element, listener) {
        return DomApiObj.change.call(element, listener);
    },
    submit: function (element, listener) {
        return DomApiObj.submit.call(element, listener);
    },
    reset: function (element, listener) {
        return DomApiObj.reset.call(element, listener);
    },
    onload: function (element, listener) {
        return DomApiObj.onload.call(element, listener);
    },
    scroll: function (element, listener) {
        return DomApiObj.scroll.call(element, listener);
    },
    unload: function (element, listener) {
        return DomApiObj.unload.call(element, listener);
    },
    resize: function (element, listener) {
        return DomApiObj.resize.call(element, listener);
    },
    keydown: function (element, listener) {
        return DomApiObj.keydown.call(element, listener);
    },
    keyup: function (element, listener) {
        return DomApiObj.keyup.call(element, listener);
    },
    keypress: function (element, listener) {
        return DomApiObj.keypress.call(element, listener);
    },
    input:function(element, listener){
        return DomApiObj.input.call(element, listener);
    },
    /**
     * 是否是dom元素
     * @param {object} element
     * @returns {boolean}
     */
    isElement: function (element) {
        return DomApiObj.isElement.call(element);
    },
    /**
     * 是否是 DOMNode
     * @param node
     * @returns {*}
     */
    isNode: function (node) {
        return DomApiObj.isNode.call(node);
    },
    /***
     * 获取元素element，位置、高宽信息
     * ***/
    domInfo: function (element) {
        return DomApiObj.domInfo.call(element);
    },
    /*****
     * selector api 返回数组
     * element：元素
     * selector：刷选条件
     * *****/
    findBySelector: function (element, selector) {
        return DomApiObj.findBySelector.call(element, selector);
    },
    /***
     * 根据id查找元素
     * arg1:id / dom元素
     * arg2:当arg1是dom时候，arg2为id
     * ***/
    findbyId: function (arg1, arg2) {
        if(!arg2){
            arg2 = arg1;
            arg1 = document;
        }
        return DomApiObj.findbyId.call(arg1, arg2);
    },
    /***
     * 根据tag Name查询元素
     * arg1:name / dom元素
     * arg2:当arg1是dom时候，arg2为name
     * ***/
    findByTagName: function (arg1, arg2) {
        return DomApiObj.findByTagName.call(arg1, arg2);
    },
    /***
     * 根据class查询元素
     * arg1：className / dom元素
     * arg2：当arg1是dom元素，arg2位className
     * ***/
    findByClass: function (arg1, arg2) {
        return DomApiObj.findByClass.call(arg1, arg2);
    },
    /***
     * 获取元素element的父元素
     * ***/
    parent: function (element) {
        return DomApiObj.parent.call(element);
    },
    /***
     * 获取element的子元素，arg:元素名称 / className / id / true获取包含文本元素的所有子元素
     * ****/
    children: function (element, arg) {
        return DomApiObj.children.call(element, arg);
    },
    /***
     * 根据className 获取element的子元素
     * ****/
    getChildrenByClass: function (element, clazz) {
        return DomApiObj.getChildrenByClass.call(element, clazz);
    },
    /***
     * 根据id获取element元素的子元素
     * ***/
    getChildrenById: function (element, id) {
        return DomApiObj.getChildrenById.call(element, id);
    },
    /**
     * 根据标签名称获取element的子元素
     * ****/
    getChildrenByName: function (element, name) {
        return DomApiObj.getChildrenByName.call(element, name);
    },
    siblings: function (element, canTextNode) {
        return DomApiObj.siblings.call(element, canTextNode);
    },
    forSiblings:function (element, fn) {
        return DomApiObj.forSiblings.call(element, fn);
    },
    /**
     * 获取元素的下一个同级元素，
     * canTextNode：是否包括文本节点
     * ****/
    next: function (element, canTextNode) {
        return DomApiObj.next.call(element, canTextNode);
    },
    /***
     * 获取element元素后面的所有同级元素
     * canTextNode：是否包括文本节点
     * ***/
    nextAll: function (element, canTextNode) {
        return DomApiObj.nextAll.call(element, canTextNode);
    },
    /***
     * 获取element元素的前一个同级元素
     * canTextNode：是否包括文本节点
     * ***/
    previous: function (element, canTextNode) {
        return DomApiObj.previous.call(element, canTextNode);
    },
    /***
     * 获取element元素的前所有的同级元素
     * canTextNode：是否包括文本节点
     * ***/
    previousAll: function (element, canTextNode) {
        return DomApiObj.previousAll.call(element, canTextNode);
    },
    /***
     * 获取或者设置元素element的宽度
     * ***/
    width: function (element, width) {
        return DomApiObj.width.call(element, width);
    },
    /***
     * 获取或者设置元素element的高度
     * ***/
    height: function (element, height) {
        return DomApiObj.height.call(element, height);
    },
    /**
     * 获取或者设置元素element的内宽度
     * ***/
    innerWidth: function (element, width) {
        return DomApiObj.innerWidth.call(element, width);
    },
    /**
     * 获取或者设置元素element的内高度
     * ***/
    innerHeight: function (element, height) {
        return DomApiObj.innerHeight.call(element, height);
    },
    /**
     * 获取外部width/height
     * **/
    outerSize: function (element) {
        var w = DomApiObj.outerWidth.call(element);
        var h = DomApiObj.outerHeight.call(element);
        return {
            width: w,
            height: h
        };
    },
    /**
     * 获取或者设置元素element的外宽度
     * ***/
    outerWidth: function (element, width) {
        return DomApiObj.outerWidth.call(element, width);
    },
    /**
     * 获取或者设置元素element的外高度
     * ***/
    outerHeight: function (element, height) {
        return DomApiObj.outerHeight.call(element, height);
    },
    /****
     * 设置/获取element元素的scrollLeft
     * *****/
    scrollLeft: function (element, left) {
        return DomApiObj.scrollLeft.call(element, left);
    },
    /****
     * 设置/获取element元素的scrollTop
     * *****/
    scrollTop: function (element, top) {
        return DomApiObj.scrollTop.call(element, top);
    },
    /***
     * 设置/获取element元素的position
     * *****/
    position: function (element, pos) {
        return DomApiObj.position.call(element, pos);
    },
    /***
     * 元素是否脱离了文档
     * ***/
    inDom(element) {
        return DomApiObj.inDom.call(element);
    },
    /***
     * 获取element元素相对位置的参考父元素
     * ****/
    getOffsetParent(element) {
        return DomApiObj.getOffsetParent.call(element);
    },
    /***
     * 获取element的相对视窗的偏移量数据
     * ****/
    offset: function (element) {
        return DomApiObj.offset.call(element);
    },
    /****
     * 设置/获取元素的css样式，获取时候style参数必须是字符串的css名称
     * ****/
    css: function (element, style) {
        return DomApiObj.css.call(element, style);
    },
    /***
     * 获取元素element的class
     * ***/
    getClass: function (element) {
        return DomApiObj.getClass.call(element);
    },
    /***
     * element元素是否存在className，className可以是数组
     * ****/
    hasClass: function (element, className) {
        return DomApiObj.hasClass.call(element, className);
    },
    /****
     * element元素添加class
     * ****/
    addClass: function (element, className) {
        return DomApiObj.addClass.call(element, className);
    },
    /*****
     * 移除元素的class
     * ******/
    removeClass: function (element, className) {
        return DomApiObj.removeClass.call(element, className);
    },
    /***
     * 复制元素
     * isDeep是否深复制，复制数据及事件
     * ****/
    copy: function (element, isDeep) {
        return DomApiObj.copy.call(element, isDeep);
    },
    /**
     * 往元素设置/读取html内容
     * ***/
    html: function (element, string) {
        return DomApiObj.html.call(element, string);
    },
    /**
     * 往元素设置/读取innertText
     * ***/
    text: function (element, string) {
        return DomApiObj.text.call(element, string);
    },
    /***
     * 设置或者获取元素的value值
     * ***/
    value: function (element, value) {
        return DomApiObj.value.call(element, value);
    },
    /**
     * 判定元素是否处于隐藏状态
     * ***/
    isHide: function (element) {
        return DomApiObj.isHide.call(element);
    },
    /***
     * 显示元素
     * ***/
    show: function (element, args) {
        return DomApiObj.show.call(element, arguments);
    },
    /**
     * 隐藏元素
     * ***/
    hide: function (element, args) {
        return DomApiObj.hide.call(element, arguments);
    },
    /**
     * 将html/元素添加到element元素内部的后面
     * ***/
    append: function (element, html) {
        return DomApiObj.append.call(element, html);
    },
    /**
     * 将html/元素添加到element元素内部的前面
     * ***/
    prepend: function (element, html) {
        return DomApiObj.prepend.call(element, html);
    },
    /***
     * 将html/元素插入到element后面
     * ***/
    after: function (element, html) {
        return DomApiObj.after.call(element, html);
    },
    /***
     * 将html/元素插入到element前面
     * ***/
    before: function (element, html) {
        return DomApiObj.before.call(element, html);
    },
    /***
     * 将html/元素替换element
     * ***/
    replace: function (element, html) {
        return DomApiObj.replace.call(element, html);
    },
    /**
     * 清理element的数据和事件
     * **/
    clearElement: function (element) {
        if (!element || element.length === 0) {
            return;
        }
        return DomApiObj.clearElement.call(element);
    },
    /**
     * 移除element下所有子元素
     * **/
    removeChilds: function (element, clazz) {
        if (!element || element.length === 0) {
            return;
        }
        return DomApiObj.removeChilds.call(element, clazz);
    },
    /**
     * 移除element元素,会将相关数据和事件移除
     * ***/
    remove: function (element) {
        if (!element || element.length === 0) {
            return;
        }
        return DomApiObj.remove.call(element);
    },
    /**
     * 移除element元素,并且还保留数据和事件移除
     * ***/
    detach: function (element) {
        return DomApiObj.detach.call(element);
    },
    /***
     * 移除element下的所有元素（保留事件-数据），并将这些元素数组形式返回
     * ***/
    detachChilds: function (element, args) {
        return DomApiObj.detachChilds.call(element, args);
    },
    /***
     * 设置，读取简单类型的属性，属性数据会在标签上形成data-xxx属性
     * args:属性名称，或者{prop:value},移除数据用{name:undefined/null}
     * ***/
    propData: function (element, args) {
        return DomApiObj.propData.call(element, args);
    },
    /***
     * 移除简单类型属性
     * key=属性名称
     * ****/
    removePropData: function (element, key) {
        return DomApiObj.removePropData.call(element, key);
    },
    removeAttribute: function (element, key) {
        return DomApiObj.removeAttribute.call(element, key);
    },
    removeAttr:function(element, key){
        return DomApiObj.removeAttribute.call(element, key);
    },
    attribute: function (element, args) {
        return DomApiObj.attribute.call(element, args);
    },
    attr:function(element, args){
        return DomApiObj.attribute.call(element, args);
    },
    removeData: function (element, key) {
        return DomApiObj.setData.call(element, key);
    },
    /**
     * 设置元素数据，key：数据key，dataObject数据对象
     * 该api会在element._eldata上生成数据对象
     * ***/
    setData: function (element, key, dataObject) {
        return DomApiObj.setData.call(element, key, dataObject);
    },
    /***
     * 根据key获取element元素的数据
     * ***/
    getData: function (element, key) {
        return DomApiObj.getData.call(element, key);
    },
    /***
     * 根据html创建元素，这个api需要进一步验证
     * *****/
    createEl: function (html) {
        var parent;
        if (/^<td/.test(html) || /^<th/.test(html)) {
            parent = document.createElement("tr");
        } else if (/^<tr/.test(html)) {
            parent = document.createElement("tbody");
        } else if (/^<tbody/.test(html) || /^<thead/.test(html) || /^<caption/.test(html) || /^<tfoot/.test(html)) {
            parent = document.createElement("table");
        } else if ((/^<dt/.test(html) || /^<dd/.test(html))) {
            parent = document.createElement("dl");
        } else if (/^<li/.test(html)) {
            parent = document.createElement("ul");
        } else {
            parent = document.createElement('div'); //创建一个空的div  
        }
        this.html(parent, html);
        var resNodes = parent.childNodes;
        var res = [];
        for (let i = 0; i < resNodes.length; i++) {
            let node = resNodes[i];
            res.push(node);
            parent.removeChild(node);
        }
        if (res.length === 1) {
            return res[0];
        }
        return res;
    },
    requestAnimationFrame: function () {
        return requestAnimationFrame;
    },
    cancelAnimationFrame: function () {
        return cancelAnimationFrame;
    },
    /**
     * 获取可视窗口的大小
     * ***/
    getViewSize: function () {
        if (document.compatMode === "BackCompat") {
            return {
                width: document.body.clientWidth,
                height: document.body.clientHeight
            };
        } else {
            return {
                width: document.documentElement.clientWidth,
                height: document.documentElement.clientHeight
            };
        }
    },
    /**
     * 获取页面包括滚动条的大小
     * ***/
    getPageSize: function () {
        if (document.compatMode === "BackCompat") {
            return {
                width: Math.max(document.body.scrollWidth,
                    document.body.clientWidth),
                height: Math.max(document.body.scrollHeight,
                    document.body.clientHeight)
            };
        } else {
            return {
                width: Math.max(document.documentElement.scrollWidth,
                    document.documentElement.clientWidth),
                height: Math.max(document.documentElement.scrollHeight,
                    document.documentElement.clientHeight)
            };
        }
    },
    /*****
     * 设置dom loaded后的回调函数
     * *****/
    onDomLoaded: function (handler) {
        if (_isDomLoaded !== false) {
            handler.call(null, _isDomLoaded);
            return DomUtils;
        }
        _domLoadedHandlers.push(handler);
        return DomUtils;
    },
    /*****
     * 设置dom ready后的回调函数
     * *****/
    onDomReady: function (handler) {
        if (_isDomReady !== false) {
            handler.call(null, _isDomReady);
            return DomUtils;
        }
        _domReadyHandlers.push(handler);
        return DomUtils;
    },
    /***
     * 设置dom元素删除、新增的监听
     * handler = function(element,flag){}
     * element相关联的元素，flag=0表示删除，1表示新增
     ****/
    onDomNodeChanged: function (handler) {
        if (!this._domChangedListner) {
            this._domChangedListner = [];
        }
        handler.errorcout = 0;
        this._domChangedListner.push(handler);
    },
    /***
     * 解除dom变化监听
     * ***/
    offDomNodeChanged: function (handler) {
        if (this._domChangedListner) {
            var newEvents = [];
            for (let i = 0; i < this._domChangedListner.length; i++) {
                if (handler !== this._domChangedListner[i]) {
                    newEvents.push(this._domChangedListner[i]);
                }
            }
            this._domChangedListner = newEvents;
        }
    },
    invokeDomChangedEvents: function (element, flag) {
        if (this._domChangedListner) {
            var newEvents = [];
            for (let i = 0; i < this._domChangedListner.length; i++) {
                let fn = this._domChangedListner[i];
                try {
                    let ret = fn(element, flag);
                    if (typeof ret !== 'undefined' && !ret) {
                        fn.errorcout = 5;//清理这个回调
                    } else {
                        fn.errorcout = 0;
                    }
                } catch (x) {
                    fn.errorcout++;
                }
                if (fn.errorcout < 3) {
                    newEvents.push(fn);
                }
            }
            this._domChangedListner = newEvents;
        }
    }
};

function forachExecute(element, opr) {
    if (_isIterable(element)) {
        var retArr = [];
        _each(element, function (e) {
            let ret = DomUtils[opr](e);
            retArr.push(ret);
        });
        return retArr;
    }
    return undefined;
}

function _elementInsert(element, opr, html) {
    var isArr = _isIterable(element);
    if (isArr) {
        var res = [];
        _each(element, function (e) {
            res.push(DomUtils[opr](e, html));
        });
        return res.length === 1 ? res[0] : res;
    }
    return undefined;
}

function _getArrayFirstEl(element) {
    var isArr = _isIterable(element);
    if (isArr) {
        element = element[0];
    }
    return element;
}

/***
 * 读写scroll值
 * ***/
function _getSetScroll(element, prop, value) {
    var isArr = _isIterable(element);
    if (typeof value === "undefined") {
        if (isArr) {
            element = element[0];
        }
        if (element.tagName === "BODY" || element === document) {
            if (prop === "scrollTop") {
                return document.documentElement.scrollTop || window.pageYOffset || document.body.scrollTop;
            }
            return document.documentElement.scrollLeft || window.pageXOffset || document.body.scrollLeft;
        }
        return element[prop];
    } else {
        if (isArr) {
            _each(element, function (e) {
                _getSetScroll(e, prop, value);
            });
        } else {
            element[prop] = value;
        }
    }
}

/******
 * 读写dom的高、宽
 * ********/
function getSetWH(element, attrName, val, exra) {
    if(!element || element.nodeType === 3){
        return typeof val === "undefined" ?  0 : DomUtils;
    }
    if (typeof val === "undefined") { //只有两个参数是读操作
        let v = DomUtils.domInfo(element)[attrName];
        let boxSize = DomUtils.css(element, "box-sizing");
        if (v !== 0 && exra) { //存在 exra则说明是 inner、outer
            if (attrName === "width") { /***innerWidth**/
                // if (boxSize === "content-box") {
                //     // let pl = DomUtils.css(element, "padding-left");
                //     // let pr = DomUtils.css(element, "padding-right");
                //     // v = v + pl + pr;                  
                // }
                if (exra === "outer" && boxSize !== "border-box") {//outerWidth：  border
                    let bl = DomUtils.css(element, "border-left-width");
                    let br = DomUtils.css(element, "border-right-width");
                    v = v + bl + br;
                }else if(exra === "inner"){
                    let isTd = element.nodeName === "TD";
                    let bl = DomUtils.css(element, "padding-left");
                    let br = DomUtils.css(element, "padding-right");
                    v = v - bl - br;
                    let l = $B.DomUtils.css(element, "border-right-width");
                    let r = $B.DomUtils.css(element, "border-left-width");
                    if(isTd){
                        l = l / 2;
                        r = r / 2;
                    }
                    v = v - l - r;
                }
            } else { //高度 
                //innerHeight               
                if (exra === "outer" && boxSize !== "border-box") {//outerHeight border
                    let bl = DomUtils.css(element, "border-top-width");
                    let br = DomUtils.css(element, "border-bottom-width");
                    v = v + bl + br;
                }else if(exra === "inner"){
                    let isTd = element.nodeName === "TD";
                    let t = $B.DomUtils.css(element, "border-top-width");
                    let b = $B.DomUtils.css(element, "border-bottom-width");
                    if(isTd){
                        t = t / 2;
                        b = b / 2;
                    }
                    v = v - t - b;
                    let bl = DomUtils.css(element, "padding-top");
                    let br = DomUtils.css(element, "padding-bottom");
                    v = v - bl - br;
                }
            }
        }
        return v;
    } else {//写操作
        if (_isNumeric(val)) {
            let strVal = val + "";
            if (/.+\d+$/.test(strVal)) {
                val = val + "px";
            }
        }
        let css = {};
        css[attrName] = val;
        DomUtils.css(element, css);
        return DomUtils;
    }
}
/***
 * DOMReady处理函数
 * ***/
function _onDOMReady(e) {
    DomUtils.body = DomUtils.findByTagName('body')[0];
    DomUtils.head = DomUtils.findByTagName('head')[0];
    var event = new EventArgs(e);
    _isDomReady = event;
    _each(_domReadyHandlers, function (fn) {
        fn.call(null, event);
    });
}
/**
 * DOMLoaded处理函数
 * ***/
function _onDOMLoaded(e) {
    var event = new EventArgs(e);
    _isDomLoaded = event;
    _each(_domLoadedHandlers, function (fn) {
        fn.call(null, event);
    });
}

//on load
if (window.onload !== null) {
    _domLoadedHandlers.push(window.onload);
}
window.onload = _onDOMLoaded;

//on ready
var readyFn;
if (addListener === 'addEventListener') {
    readyFn = function (e) {
        document[removeListener]('DOMContentLoaded', readyFn, false);//移除
        readyFn = undefined;
        _onDOMReady(e);
    };
    document[addListener]('DOMContentLoaded', readyFn, false);
} else {
    readyFn = function (e) {
        if (document.readyState === 'complete') {
            document[removeListener]('onreadystatechange', readyFn);//移除
            readyFn = undefined;
            _onDOMReady(e);
        }
    };
    document[addListener]('onreadystatechange', readyFn);
}
//创建dom变化监听，用于触发事件清理逻辑
function clearElementEvents(el) {
    if (el.nodeType === 1) {
        if (el.events) {
           
            DomUtils.offEvents(el);
        }
        if(el._clearAllFn){
            el._clearAllFn();
        }
        let _droplistid = $B.DomUtils.attribute(el, "_droplistid");
        if (_droplistid) { //移除对应的下拉dom           
            setTimeout(function () {
                let ele = document.getElementById(_droplistid);
                if (ele) {
                    if(ele._clearAllFn){
                        ele._clearAllFn();
                    }
                    $B.DomUtils.remove(ele);
                }
            }, 1);
        }
        var children = el.children;
        if (children) {
            for (let i = 0; i < children.length; i++) {
                clearElementEvents(children[i]);
            }
        }
    }

}

var onNodeRemovedFn = function (event) {
    event = event._e;
    if (event.target.nodeType === 1) {
        DomUtils.invokeDomChangedEvents(event.target, 0);
        if (event.target._isRetainEvent) { //避免被多次触发的场景           
            return;
        }
        event.target._isRetainEvent = true;
        var children = event.target.children;
        if (children) {
            let childs = Array.from(children);            
            _onNodeRemoveEventTimer = setTimeout(() => {
                for (let i = 0; i < childs.length; i++) {
                    clearElementEvents(childs[i]);
                }
            }, 8);
        }
        clearElementEvents(event.target);
        setTimeout(function () {
            event.target._isRetainEvent = undefined;
        }, 9);
    }
};
DomUtils.addListener(document, "DOMNodeRemoved", onNodeRemovedFn);
var DOMNodeInsertedFn = function (event) {
    DomUtils.invokeDomChangedEvents(event.target, 1);
};
DomUtils.addListener(document, "DOMNodeInserted", DOMNodeInsertedFn);
$B.DomUtils = DomUtils;
$B.Dom = DomUtils;
$B.onLoad = $B.DomUtils.onDomLoaded;

/*******动画*******/
function _isVelocityEl() {
    if (!window["Velocity"]) {
        return;
    }
    return window["Velocity"];
}
$B.createEl = function(html){
   return DomUtils.createEl(html);
}; 
/** Velocity 动画配置项的默认值 
 * props = {   
    duration: 400,         // 动画执行时间
    easing: "swing",       // 缓动效果
    queue: "",             // 队列
    begin: undefined,      // 动画开始时的回调函数
    progress: undefined,   // 动画执行中的回调函数（该函数会随着动画执行被不断触发）
    complete: undefined,   // 动画结束时的回调函数
    display: undefined,    // 动画结束时设置元素的 css display 属性
    visibility: undefined, // 动画结束时设置元素的 css visibility 属性
    loop: false,           // 循环
    delay: false,          // 延迟
    mobileHA: true         // 移动端硬件加速（默认开启）
}
 * **/
$B.velocity = function (el, css, props) {
    var fn = _isVelocityEl();
    if (fn) {
        fn(el, css, props);
    }
};
$B.animate = $B.velocity;
function exeAnimate(animate, args) {
    var fn = _isVelocityEl();
    if (fn) {
        let duration = 500;
        let completeFn, elArr, el;
        for (let i = 0; i < args.length; i++) {
            if (i === 0) {
                el = args[0];
            } else if (_isFunction(args[i])) {
                completeFn = args[i];
            } else if (_isNumeric(args[i])) {
                duration = parseInt(args[i]);
            }
        }
        elArr = el;
        if (!_isArray(el)) {
            elArr = [];
            elArr.push(el);
        }

        var opt = { duration: duration };
        for (let i = 0; i < elArr.length; i++) {
            if (completeFn && i === elArr.length - 1) {
                opt["complete"] = completeFn;
            }
            let display = undefined;
            if (animate === "slideDown" || animate === "show" || animate === "fadeIn") { //显示
                if (elArr[i]["_display"] && elArr[i]["_display"] !== "none") {
                    display = elArr[i]["_display"];
                } else {
                    let tagName = elArr[i].tagName;
                    if (tagName === "TABLE") {
                        display = "table";
                    } else if (tagName === "TBODY") {
                        display = "table-row-group";
                    } else if (tagName === "THEAD") {
                        display = "table-header-group";
                    } else if (tagName === "TFOOT") {
                        display = "table-footer-group";
                    } else if (tagName === "TR") {
                        display = "table-row";
                    } else if (tagName === "TD") {
                        display = "table-cell";
                    } else {
                        display = undefined;
                    }
                }
            } else { //隐藏记录display
                let srcdisplay = $B.DomUtils.css(elArr[i], "display");
                elArr[i]["_display"] = srcdisplay;
            }
            opt.display = display;
            fn(elArr[i], animate, opt);
        }
    }
}
$B.slideUp = function () {
    exeAnimate("slideUp", arguments);
};
$B.slideDown = function () {
    exeAnimate("slideDown", arguments);
};
$B.toggle = function(){
    let el = arguments[0];
    if(!el.style || el.style.display !== "none"){
        exeAnimate("slideUp", arguments);
    }else{
        exeAnimate("slideDown", arguments);
    }
};
$B.hide = function () {
    exeAnimate("fadeOut", arguments);
};
$B.show = function () {
    exeAnimate("fadeIn", arguments);
};
$B.fadeIn = function () {
    exeAnimate("fadeIn", arguments);
};
$B.fadeOut = function () {
    exeAnimate("fadeOut", arguments);
};

(function ($) {
    var
        document = window.document,
        key,
        name,
        rscript = /<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi,
        scriptTypeRE = /^(?:text|application)\/javascript/i,
        xmlTypeRE = /^(?:text|application)\/xml/i,
        jsonType = 'application/json',
        htmlType = 'text/html',
        blankRE = /^\s*$/,
        escape = encodeURIComponent,
        originAnchor = document.createElement('a');

    originAnchor.href = window.location.href;

    // Empty function, used as default callback
    function emptyFn() { }

    var ajaxSettings = {
        // Default type of request
        type: 'GET',
        // Callback that is executed before request
        beforeSend: emptyFn,//fn(data, type)
        // Callback that is executed if the request succeeds
        success: emptyFn, //fn(data, status, xhr)
        // Callback that is executed the the server drops error
        error: emptyFn,//fn(xhr, type, error)
        // Callback that is executed on request complete (both: error and success)
        complete: emptyFn,//fn(type, xhr, settings)
        // Transport
        xhr: function () {
            return new window.XMLHttpRequest();
        },
        // MIME types mapping
        // IIS returns Javascript as "application/x-javascript"
        accepts: {
            script: 'text/javascript, application/javascript, application/x-javascript',
            json: jsonType,
            xml: 'application/xml, text/xml',
            html: htmlType,
            text: 'text/plain'
        },
        // Whether the request is to another domain
        crossDomain: false,
        // Default timeout
        timeout: 0,
        onErrorEval:false,
        // Whether data should be serialized to string
        processData: true,
        // Whether the browser should be allowed to cache GET responses
        cache: true,
        //Used to handle the raw response data of XMLHttpRequest.
        //This is a pre-filtering function to sanitize the response.
        //The sanitized response should be returned
        dataFilter: emptyFn
    };

    // triggers an extra global event "ajaxBeforeSend" that's like "ajaxSend" but cancelable
    function ajaxBeforeSend(xhr, settings) {
        if (settings.beforeSend(xhr, settings) === false) {
            return false;
        }
        //triggerGlobal(settings, context, 'ajaxSend', [xhr, settings]);
    }
    function ajaxSuccess(data, xhr, settings) {
        var status = 'success';
        settings.success(data, status, xhr);
        settings.complete(status, xhr, settings);
    }
    // type: "timeout", "error", "abort", "parsererror"
    function ajaxError(error, type, xhr, settings) {
        settings.error(xhr, type, error);
        settings.complete(type, xhr, settings);
    }

    function ajaxDataFilter(data, type, settings) {
        if (settings.dataFilter === emptyFn) {
            return data;
        }
        return settings.dataFilter(data, type);
    }

    function mimeToDataType(mime) {
        if (mime) {
            mime = mime.split(';', 2)[0];
        }
        return mime && (mime === htmlType ? 'html' :
            mime === jsonType ? 'json' :
                scriptTypeRE.test(mime) ? 'script' :
                    xmlTypeRE.test(mime) && 'xml') || 'text';
    }

    function appendQuery(url, query) {
        if (query === '') {
            return url;
        }
        return (url + '&' + query).replace(/[&?]{1,2}/, '?');
    }

    function serialize(params, obj, traditional, scope) {
        var type,
            array = $B.isArrayFn(obj),
            hash = $B.isPlainObjectFn(obj);
        var keys = Object.keys(obj);
        for(let i =0 ; i < keys.length ;i++){
            let key = keys[i];
            let value = obj[key];
            if (scope) {
                key = traditional ? scope :
                    scope + '[' + (hash || type === 'object' || type === 'array' ? key : '') + ']';
            }
            // handle data in serializeArray() format
            if (!scope && array) {
                params._addFN(value.name, value.value);
            } else if (type === "array" || (!traditional && type === "object")) {// recurse into nested objects
                serialize(params, value, traditional, key);
            } else {
                params._addFN(key, value);
            }
        }        
    }

    function object2param(obj, traditional) {
        var params = [];
        params._addFN = function (key, value) {
            if (typeof value === "function") {
                value = value();
            }
            if (value === null) {
                value = "";
            }
            this.push(escape(key) + '=' + escape(value));
        };
        serialize(params, obj, traditional);
        params._addFN = undefined;
        return params.join('&').replace(/%20/g, '+');
    }

    // serialize payload and append it to the URL for GET requests
    function serializeData(options) {       
        if (options.processData && options.data && typeof (options.data) !== "string") {
            options.data = object2param(options.data, options.traditional);
        }
        if (options.data && (!options.type || options.type.toUpperCase() === 'GET' || 'jsonp' === options.dataType)) {
            options.url = appendQuery(options.url, options.data);
            options.data = undefined;
        }  
    }


    $.ajaxJSONP = function (options) {
        if (!('type' in options)) {
            return $.ajax(options);
        }
        var jsonpID = +new Date();
        var _callbackName = options.jsonpCallback,
            callbackName = ($B.isFunctionFn(_callbackName) ? _callbackName() : _callbackName) || ('Zepto' + (jsonpID++)),
            scriptEl = document.createElement('script'),
            originalCallback = window[callbackName],
            responseData,
            abort = function (errorType) {
                $B.DomUtils.trigger(scriptEl,"error");
                //$(script).triggerHandler('error', errorType || 'abort')
            },
            xhr = { abort: abort },
            abortTimeout;


        var scriptFn = function (e, errorType) {
            clearTimeout(abortTimeout);
            $B.DomUtils.offEvents(scriptEl);
            if (e.type === 'error' || !responseData) {
                //error, type, xhr, settings
                ajaxError(null, errorType || 'error', xhr, options);
            } else {
                ajaxSuccess(responseData[0], xhr, options);
            }

            window[callbackName] = originalCallback;
            if (responseData && $B.isFunctionFn(originalCallback)) {
                originalCallback(responseData[0]);
            }
            originalCallback = responseData = undefined;
            $B.DomUtils.remove(scriptEl);
        };
        //$(script).on('load error', );
        $B.DomUtils.addListener(scriptEl, "load", scriptFn);
        $B.DomUtils.addListener(scriptEl, "error", scriptFn);

        if (ajaxBeforeSend(xhr, options) === false) {
            abort('abort');
            return xhr;
        }

        window[callbackName] = function () {
            responseData = arguments;
        };

        scriptEl.src = options.url.replace(/\?(.+)=\?/, '?$1=' + callbackName);
        document.head.appendChild(scriptEl);

        if (options.timeout > 0) {
            let timeout = options.timeout * 1000;
            abortTimeout = setTimeout(() => {
                abort('timeout');
            }, timeout);
            return xhr;
        }
    };
    $.ajax = function (options) {
        var settings = $B.extendObjectFn({}, ajaxSettings, options || {}),
            urlAnchor, hashIndex;

        if (!settings.crossDomain) {
            urlAnchor = document.createElement('a');
            urlAnchor.href = settings.url;
            // cleans up URL for .href (IE only), see https://github.com/madrobby/zepto/pull/1049
            urlAnchor.href = urlAnchor.href;
            settings.crossDomain = (originAnchor.protocol + '//' + originAnchor.host) !== (urlAnchor.protocol + '//' + urlAnchor.host);
        }

        if (!settings.url) {
            settings.url = window.location.toString();
        }

        if ((hashIndex = settings.url.indexOf('#')) > -1) {
            settings.url = settings.url.slice(0, hashIndex);
        }
        if( settings.contentType === "application/json"){
            settings.processData = false;
        }

        serializeData(settings);

        var dataType = settings.dataType,
            hasPlaceholder = /\?.+=\?/.test(settings.url);
        if (hasPlaceholder) {
            dataType = 'jsonp';
        }

        if (settings.cache === false || ((!options || options.cache !== true) && ('script' === dataType || 'jsonp' === dataType))) {
            settings.url = appendQuery(settings.url, '_=' + Date.now());
        }


        if ('jsonp' === dataType) {
            if (!hasPlaceholder) {
                settings.url = appendQuery(settings.url, settings.jsonp ? (settings.jsonp + '=?') : settings.jsonp === false ? '' : 'callback=?');
            }
            return $.ajaxJSONP(settings);
        }

        var mime = settings.accepts[dataType],
            headers = {},
            protocol = /^([\w-]+:)\/\//.test(settings.url) ? RegExp.$1 : window.location.protocol,
            xhr = settings.xhr(),
            nativeSetHeader = xhr.setRequestHeader,
            abortTimeout;
        var setHeader = function (name, value) {
            headers[name.toLowerCase()] = [name, value];
        };

        if (!settings.crossDomain) {
            setHeader('X-Requested-With', 'XMLHttpRequest');
        }
        setHeader('Accept', mime || '*/*');
        if (mime = settings.mimeType || mime) {
            if (mime.indexOf(',') > -1) {
                mime = mime.split(',', 2)[0];
            }
            xhr.overrideMimeType && xhr.overrideMimeType(mime);
        }
        if (settings.contentType || (settings.contentType !== false && settings.data && settings.type.toUpperCase() !== 'GET')) {
            setHeader('Content-Type', settings.contentType || 'application/x-www-form-urlencoded');
        }
        if (settings.headers) {
            let keys = settings.headers;
            keys.forEach(function (key) {
                setHeader(key, settings.headers[key]);
            });
        }
        xhr.setRequestHeader = setHeader;
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4) {
                xhr.onreadystatechange = emptyFn;
                clearTimeout(abortTimeout);
                var result,
                    error = false;
                if ((xhr.status >= 200 && xhr.status < 300) || xhr.status === 304 || (xhr.status === 0 && protocol === 'file:')) {
                    dataType = dataType || mimeToDataType(settings.mimeType || xhr.getResponseHeader('content-type'));
                    if (xhr.responseType === 'arraybuffer' || xhr.responseType === 'blob') {
                        result = xhr.response;
                    } else {
                        result = xhr.responseText;
                        try {
                            // http://perfectionkills.com/global-eval-what-are-the-options/
                            // sanitize response accordingly if data filter callback provided
                            result = ajaxDataFilter(result, dataType, settings);
                            if (dataType === 'script') {
                                //(1, eval)(result);
                            } else if (dataType === 'xml') {
                                result = xhr.responseXML;
                            } else if (dataType === 'json') {
                                try{
                                    result = blankRE.test(result) ? null : JSON.parse(result);
                                }catch(e){
                                    if(settings.onErrorEval){
                                        result = blankRE.test(result) ? null : eval('('+result+')');
                                    }else{
                                        throw e;
                                    }
                                }
                            }
                        } catch (e) {
                            error = e;
                        }
                        if (error) {
                            return ajaxError(error, 'parsererror', xhr, settings);
                        }
                    }
                    ajaxSuccess(result, xhr, settings);
                    //脚本支持
                    if (dataType === "html") {
                        setTimeout(()=>{
                            let fragment = document.createDocumentFragment();
                            let wrap = document.createElement("div");
                            wrap.innerHTML = result;
                            fragment.appendChild(wrap);
                            let scriptArray = wrap.querySelectorAll("script");                           
                            let loading = [];
                            let jsExeTag = [];
                            let onLoadfN = function(){
                                loading.shift();
                                document.head.removeChild(this);
                            };
                            let max = 1;
                            for (let i = 0; i < scriptArray.length; i++) {
                                let script = scriptArray[i].textContent;     
                                let src = scriptArray[i].src;                                                          
                                let scriptel = document.createElement("script");
                                scriptel.type="text/javascript";
                                if(src){
                                    loading.push(1);
                                    scriptel.onload = onLoadfN;
                                    scriptel.onerror = onLoadfN;
                                    scriptel.src = src;
                                    document.head.appendChild(scriptel);
                                    max++;
                                }else{
                                    scriptel.text = script;
                                    jsExeTag.push(scriptel);
                                }                        
                            }
                            if(jsExeTag.length > 0){
                                let count = 0;
                                max = max * 5 * 4;
                                let ivt = setInterval(()=>{
                                    if( loading.length === 0 || count > max){
                                        clearInterval(ivt);
                                        for(let i =0 ; i < jsExeTag.length ;i++){
                                            document.head.appendChild(jsExeTag[i]);
                                            document.head.removeChild(jsExeTag[i]);
                                        }
                                        jsExeTag =undefined;
                                    }                                   
                                    count++;
                                },300);
                            }
                        },1);
                    }
                } else {
                    ajaxError(xhr.statusText || null, xhr.status ? 'error' : 'abort', xhr, settings);
                }
            }
        };

        if (ajaxBeforeSend(xhr, settings) === false) {
            xhr.abort();
            ajaxError(null, 'abort', xhr, settings);
            return xhr;
        }
        if(settings.onProgress){
            xhr.upload.addEventListener('progress', function (e) {
                var progressRate = Math.round((e.loaded / e.total) * 100 );
                if(progressRate > 100){
                    progressRate = 100;
                }                
                settings.onProgress(progressRate);
            })
        }

        var async = 'async' in settings ? settings.async : true;
        xhr.open(settings.type, settings.url, async, settings.username, settings.password);

        if (settings.xhrFields) {
            let keys = settings.xhrFields;
            keys.forEach(function (key) {
                xhr[key] = settings.xhrFields[key];
            });
        }
        let keys = Object.keys(headers);
        keys.forEach(function (key) {
            nativeSetHeader.apply(xhr, headers[key]);
        });

        if (settings.timeout > 0) {
            let timer = settings.timeout * 1000;
            abortTimeout = setTimeout(() => {
                xhr.onreadystatechange = emptyFn;
                xhr.abort();
                ajaxError("timeout", 'timeout', xhr, settings);
            }, timer);
        }
        let sendData = null;
        if(settings.data){
            sendData = settings.data;
            if(settings.contentType === "application/json"){
                sendData = JSON.stringify(sendData);
            }
        }
        xhr.send(sendData);
        return xhr;
    };
})($B);

_extendObjectFn($B, {
    /**
     *打开一个窗口
    *arg={
            full:false,//是否满屏，当为true时候，高宽无效  
            autoHeight:true,//自动根据内容设置高度
            size: { width: 'auto', height: 'auto' },         
            title: '', //标题
            isTop: false,
            iconCls: null, //图标class，font-awesome字体图标
            iconColor: undefined,//图标颜色
            headerColor: undefined,//头部颜色
            toolbar: null, //工具栏对象参考工具栏组件配置说明，可以是创建函数
            toolbarStyle: undefined,//参考工具栏样式定义
            shadow: true, //是否需要阴影
            radius: undefined, //圆角px定义
            header: true, //是否显示头部
            zIndex: 200000000,//层级
            content: null, //静态内容
            url: '',//请求地址
            dataType: 'html', //当为url请求时，html/json/iframe
            draggable: false, //是否可以拖动
            moveProxy: false, //是否代理移动方式
            draggableHandler: 'header', //拖动触发焦点
            closeable: false, //是否关闭
            closeType: 'hide', //关闭类型 hide(隐藏，可重新show)/ destroy 直接从dom中删除
            expandable: false, //可左右收缩
            maxminable: false, //可变化小大
            collapseable: false, //上下收缩
            resizeable: false,//右下角拖拉大小
            onResized: null, //function (pr) { },//大小变化事件
            onLoaded: null, //function () { },//加载后
            onClose: null, //关闭前
            onClosed: null, //function () { },//关闭后
            onExpanded: null, // function (pr) { },//左右收缩后
            onCollapsed: null, // function (pr) { }//上下收缩后
            onCreated: null //function($content,$header){} panel创建完成事件
        }
    返回一个具有close(timeout) api的对象
    ***/
    window: function (args) {
        if (!$B.Panel) {
            alert("Panel is not find!");
            return;
        }
        var _$body, mask = true;
        if (args.isTop) {
            _$body = $B.DomUtils.css(window.top.document.body, { "position": "relative" });
        } else {
            _$body = $B.getBody();
        }
        var _bodyw = parseInt($B.DomUtils.outerWidth(_$body)),
            _bodyh = parseInt($B.DomUtils.outerHeight(_$body));
        if (typeof args.mask !== 'undefined') {
            mask = args.mask;
        }
        if (args.full) {
            mask = false;
            args.size = { width: _bodyw , height: _bodyh };
        } else if (!args.size) {
            args.size = { width: 600, height: 500 };
        }
        if(args.width){
            args.size.width = args.width;
        }
        if(args.height){
            args.size.height = args.height;
        }
        if(typeof args.size.width === "string" ){
            if(args.size.width.indexOf("%")){
                args.size.width = parseFloat(args.size.width.replace("%","")) / 100 * _bodyw;
            }else{
                args.size.width = parseFloat(args.size.width);
            }
        }
        if(typeof args.size.height === "string" ){
            if(args.size.height.indexOf("%")){
                args.size.height = parseFloat(args.size.height.replace("%","")) / 100 * _bodyh;
            }else{
                args.size.height = parseFloat(args.size.height);
            }
        }
        if (args.fixed || args.full) {
            args.draggable = false;
            args.collapseable = false;
            args.maxminable = false;
            args.resizeable = false;
        }
        if(!args.zIndex){
            args.zIndex = 2000000000;
        }
        let avH = _bodyh / 2;
        //自动内容高度
        var auto = typeof args.autoHeight !== "undefined" ? args.autoHeight : true;
        if (auto && args.content) {
            let cc = typeof args.content === "string" ? args.content : args.content.outerHTML;
            let visualWidth = args.size.width - 10;
            let visual = $B.DomUtils.createEl("<div style='position:absolute;top:-999999px;z-index:0;width:" + visualWidth + "px'>" + cc + "</div>");
            $B.DomUtils.append(_$body, visual);
            let h = visual.clientHeight + 2;
            if (h < 35) {
                h = 35;
            }
            if (typeof args.header === "undefined" || args.header) {
                h = h + 35;
            }
            let maxH = _bodyh - 100;
            if (h > maxH) {
                h = maxH;
            }
            $B.DomUtils.remove(visual);
            if (args.toolbar) {
                h = h + 35;
            }
            args.size.height = Math.floor( h);
        }
        var _l = (_bodyw - args.size.width) / 2;
        var _t = (_bodyh - args.size.height) / 2 ;
        avH = _bodyh / 5;
        if(_t > avH){
            _t = avH;
        }
        _t = _t + (window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0);
        var $mask = $B.DomUtils.getChildrenById(_$body, "k_window_mask_bg");
        var uuid = this.generateMixed(6);
        if (mask) {
            var mskCss = {};
            var maskWidth = parseInt(_$body.scrollWidth - 1);
            var maskHeight = parseInt(_$body.scrollHeight - 1);
            if (!$mask) {
                $mask = $B.DomUtils.createEl("<div style='z-index:" +  (args.zIndex - 2) + ";position:abosulte;width:" + maskWidth + "px;height:" + maskHeight + "px' for='" + uuid + "' id='k_window_mask_bg'></div>");
                $B.DomUtils.append(_$body, $mask);
            } else {
                mskCss = { width: maskWidth, height: maskHeight };
            }
            if (args.opacity) {
                mskCss["opacity"] = args.opacity;
            }
            if ($B.DomUtils.isHide($mask)) {
                mskCss["top"] = 0;
                $B.DomUtils.css($mask, mskCss);
                $B.DomUtils.show($mask);
                $B.DomUtils.attribute($mask, { "for": uuid });
            } else {
                $B.DomUtils.css($mask, mskCss);
            }
        }
        var $win = $B.DomUtils.createEl("<div  id='" + uuid + "' style='position:absolute;z-index:" + args.zIndex + ";' class='k_window_main_wrap'></div>");
        var posIsPlaintObj, bodyOverflow, velocity2pos;
        if (args.position) {
            posIsPlaintObj = $B.isPlainObjectFn(args.position);
            if (posIsPlaintObj) {
                $B.DomUtils.css($mask, args.position);
                $B.DomUtils.append(_$body, $win);
            } else {
                let winCss, hidepos = -888888;
                velocity2pos = args.position;
                if (args.position === "bottom") {
                    bodyOverflow = $B.DomUtils.css(_$body, "overflow");
                    $B.DomUtils.css(_$body, { "overflow": "hidden" });
                    winCss = {
                        bottom: hidepos,
                        right: 0
                    };
                } else {
                    winCss = {
                        top: hidepos,
                        left: _l
                    };
                }
                $B.DomUtils.css($win, winCss);
                $B.DomUtils.append(_$body, $win);
            }
        } else {
            if (_t < 1) {
                _t = 0;
            }
            if (_l < 1) {
                _l = 0;
            }
            $B.DomUtils.css($win, {
                top: _t,
                left: _l
            });
            $B.DomUtils.append(_$body, $win);
        }
        var panel, closeTimer,
            defOpt = $B.config && $B.config.winDefOpts ? $B.config.winDefOpts : {
                iconCls: "fa-popup", //图标class，font-awesome字体图标
                iconColor: "#6A50FC",//图标颜色
                shadow: true, //是否需要阴影
                radius: "2px", //圆角px定义
                header: true, //是否显示头部
                zIndex: 2147483647,//层级
                dataType: 'html', //当为url请求时，html/json/iframe
                triggerHide:true, //触发全局隐藏
                draggable: true, //是否可以拖动
                moveProxy: false, //是否代理移动方式
                draggableHandler: 'header', //拖动触发焦点
                closeable: true, //是否关闭
                closeType: 'destroy', //关闭类型 hide(隐藏，可重新show)/ destroy 直接从dom中删除
                expandable: false, //可左右收缩
                maxminable: true, //可变化小大
                collapseable: true, //上下收缩
                resizeable: true//右下角拖拉大小 
            };
        var opts = $B.extendObjectFn({}, defOpt, args);
        opts.onClosed = function () { //关闭后
            clearTimeout(closeTimer);
            if ($mask) {
                $B.DomUtils.hide($mask);
            }
            if (typeof args.onClosed === 'function') {
                setTimeout(() => {
                    args.onClosed();
                }, 1);
            }
        };
        if (args.timeout && args.timeout < 5) {
            opts.closeable = false;
        }
        if (args.isTop) {
            $B.DomUtils.css($win, { "top": -888888 });
            if (window.top.$B) {
                $B.DomUtils.attribute($win, { "istop": 1 });
                panel = new window.top.$B.Panel($win, opts);
                $B.velocity($win, { top: 1 }, { duration: 500 });
            }
        }
        if (!panel) {
            panel = new $B.Panel($win, opts);
        }
        if (args.timeout) {
            closeTimer = setTimeout(() => {
                if (opts.closeType === "destroy") {
                    panel.destroy();
                } else {
                    panel.close(true);
                }
            }, args.timeout * 1000);
        }
        if (velocity2pos) {
            let vcss, existEls, fix = 0;
            if (velocity2pos === "bottom") {
                existEls = $B.DomUtils.children(_$body, ".k_window_bottom_");
                vcss = { bottom: 0, right: 2 };
                $B.DomUtils.addClass(panel.elObj, "k_window_bottom_");
            } else {
                vcss = { top: 0 };
                existEls = $B.DomUtils.children(_$body, ".k_window_top_");
                $B.DomUtils.addClass(panel.elObj, "k_window_top_");
            }
            for (let i = 0; i < existEls.length; i++) {
                if ($B.DomUtils.css(existEls[i], "display") !== "none") {
                    fix = fix + existEls[i].clientHeight + 3;
                }
            }
            if (typeof vcss.bottom !== "undefined") {
                vcss.bottom = vcss.bottom + fix;
            } else {
                vcss.top = vcss.top + fix;
            }
            $B.velocity(panel.elObj, vcss, { duration: 360 });
        }
        return panel;
    },
    /***
     *{
            mask:false,
            title:'',
            position:'top',
            content:'提示信息！',
            closeable:false,
            width:宽度,
            onClosed:fn
     }
     *****/
    busyTip: function (args) {
        var title;
        let isStr = typeof args === "string";
        if (args) {
            title = isStr ? args : args.title;
        }
        if (!title) {
            title = $B.config ? $B.config.busyTipTitle : 'please waiting.......';
        }
        let content = "<div><p class='k_processing_top_tip' style='text-align:center;'><i class='fa fa-spin5 animate-spin'></i><span style='padding-left:10px'>" + title + "</span></p></div>";
        var ops = {
            header: false,
            mask: false,
            position: 'top',
            closeable: false, //是否关闭
            draggable: false, //是否可以拖动
            expandable: false, //可左右收缩
            maxminable: false, //可变化小大
            collapseable: false, //上下收缩
            resizeable: false,//右下角拖拉大小
            size: { width: 230 },
            content: content
        };
        if (args && !isStr) {
            ops = $B.extendObjectFn(ops, args);
        }
        return this.window(ops);
    },
    _getToolTipos: function (el, message, fixPos) {
        let shiftPos;
        if (fixPos && typeof fixPos !== "string") {
            shiftPos = fixPos;
            fixPos = undefined;
        }
        if(typeof el["fixpos"] === "object"){
            shiftPos = el["fixpos"];
            el["fixpos"] = undefined;
        }
        let w = 8, h = 8, pos;
        var _$body = $B.getBody();
        if ($B.DomUtils.isNode(el)) {
            w = w + $B.DomUtils.outerWidth(el);
            h = h + $B.DomUtils.outerHeight(el);
            pos = $B.DomUtils.offset(el);
        }
        let scrTop = $B.DomUtils.scrollTop(document);
        let scrLeft = $B.DomUtils.scrollLeft(document);
        let elLeft = pos.left;
        let elTop = pos.top;
        let bodyWidth = _$body.offsetWidth + scrLeft;
        let bodyHeight = _$body.offsetHeight + scrTop;
        let r = 0.95;
        if(bodyWidth > 800){
            r = 0.85;
        }else if(bodyWidth > 1000){
            r = 0.8;
        }else if(bodyWidth > 1300){
            r = 0.7;
        }else if(bodyWidth > 1500){
            r = 0.6;
        }
        var maxWidth =  bodyWidth * r  ;
        let visual = $B.DomUtils.createEl("<span class='k_tool_tip_helper k_box_size' style='max-width:" + maxWidth + "px;position:absolute;top:-10000px;left:-10000px;z-index:0;padding:5px'><p style=''><i style='' class='fa fa-info-1'></i><span style='padding-left:8px'>" + message + "</span></p></span>");
        $B.DomUtils.append(_$body, visual);
        let charWidth = Math.ceil($B.DomUtils.outerWidth(visual));
        let charHeight = Math.ceil($B.DomUtils.outerHeight(visual));
        $B.DomUtils.remove(visual);
        //检测放置合适的位置 transform: rotate(90deg)
        // 1，先检测右侧是否够空间，不够放下方，下方不够放上方，上方不够放右侧
        let isOk = false, iconCss;
        let avaiWidth = bodyWidth - pos.left - w;
        if ((!fixPos && avaiWidth > charWidth) || (fixPos === "right")) {
            isOk = true;
            pos.left = pos.left + w;
            let aviHeight = bodyHeight - pos.top;
            let diff = 0;
            if (aviHeight < charHeight) {
                diff = charHeight - aviHeight + 1;
                pos.top = pos.top - diff;
            }
            iconCss = {
                transform: "rotate(180deg)",
                top: diff + 'px',
                left: '-8px'
            };
        }
        if (!isOk) { //放下方           
            pos.left = bodyWidth - charWidth;
            if (pos.left > elLeft) {
                pos.left = elLeft;
            }
            pos.top = pos.top + h;
            let aviHeight = bodyHeight - pos.top;
            let diff = elLeft - pos.left + 2;
            if (diff < 2) {
                diff = 2;
            }
            iconCss = {
                transform: "rotate(270deg)",
                top: '-12px',
                left: diff + "px"
            };
            let isfixBottom = fixPos === "bottom";
            if (!isfixBottom) {
                //检测底部是否够空间
                if ((aviHeight < charHeight && !fixPos) || (fixPos === "top")) {
                    pos.top = elTop - charHeight - 6;
                    delete iconCss.top;
                    iconCss.bottom = "-12px";
                    iconCss.transform = "rotate(90deg)";
                }
            }
        }   
        if(shiftPos){
            if(shiftPos.top ){
                pos.top = pos.top + shiftPos.top;
            }
            if(shiftPos.left ){
                pos.left = pos.left + shiftPos.left;
            }
        }     
        return {
            pos: pos,
            iconCss: iconCss,
            maxWidth: maxWidth + 20,
            charWidth: charWidth
        };
    },
    /*****
     * pos:元素，或者位置
     * msgFn:提示信息/或者返回信息的函数
     * timeout:关闭时间 或者 up 、right 、bottom 位置信息
     * color:背景色
     * *******/
    toolTip: function (pos, msgFn, timeout,color) {
        let id = $B.getUUID();
        var _$body = $B.getBody();
        let targetEl = pos;
        if ($B.DomUtils.isNode(pos)) {
            let tipId = $B.DomUtils.attribute(pos, 'tooltip_id');
            if (tipId) {
                let el = $B.DomUtils.findbyId(_$body, tipId);
                if (el) {
                    $B.DomUtils.remove(el);
                }
            }
            $B.DomUtils.attribute(pos, { "tooltip_id": id });
        }
        let message = msgFn;
        if(typeof msgFn === "function"){
            message = msgFn();
        }
        let res = this._getToolTipos(pos, message, timeout);
        let charWidth = res.charWidth;
        let maxWidth = res.maxWidth;
        pos = res.pos;
        let widthcss;
        if (charWidth < maxWidth) {
            widthcss = "width:" + charWidth;
        } else {
            widthcss = "max-width:" + maxWidth;
        }
        let iconCss = res.iconCss;
        let txtHtml;
        if(message.indexOf("</") < 0){
            txtHtml = "<p style='text-align:left;padding:0;margin:0;'><i class='fa fa-info-1'></i><span style='padding-left:6px'>" + message + "</span></p>";
        }else{
            txtHtml = "<div>" + message + "</div>";
        }
        let el = $B.DomUtils.createEl("<div  id='" + id + "'  style='" + widthcss + "px;display:none;position:absolute;top:" + pos.top + "px;left:" + pos.left + "px;z-index:" 
        + $B.config.maxZindex + "' class='k_window_tooltip_wrap k_box_size'>"+txtHtml+"<div style='position:absolute;' class='k_tooltip_attrow'><i class='fa fa-play'></i></div></div>");
        let icon = $B.DomUtils.children(el, ".k_tooltip_attrow");
        $B.DomUtils.css(icon, iconCss);
        if(typeof color === "string"){
            el.style.backgroundColor = color;
            icon[0].firstChild.style.color = color;
        }
        $B.DomUtils.append(_$body, el);
        var closeTimer;
        $B.fadeIn(el, {
            duration: 200
        });
        var isTimeout = $B.isNumericFn(timeout);
        let ret = {
            fixPos: isTimeout ? undefined : timeout,
            elObj: el,
            targetEl: targetEl,
            message: msgFn,
            show: function (el, fixPos) {
                let fix =  this.fixPos;
                if (fixPos) {
                    fix = fixPos;
                }
                let msg = this.message;
                if(typeof  this.message === "function"){
                    msg = this.message();
                }
                let res = $B._getToolTipos(el, msg, fix);
                res.pos["max-width"] = res.maxWidth;
                $B.DomUtils.css(this.elObj, res.pos);
                let $icon = $B.DomUtils.children(this.elObj, ".k_tooltip_attrow");
                $B.DomUtils.css($icon, res.iconCss);
                this.elObj.firstChild.lastChild.innerHTML = msg;
                $B.fadeIn(this.elObj, {
                    duration: 200
                });
            },
            hide: function () {
                $B.fadeOut(this.elObj, {
                    duration: 200
                });
            },
            close: function (imdly) {
                let fn =()=>{
                    clearTimeout(closeTimer);
                    this.elObj.instance = undefined;
                    $B.DomUtils.remove(this.elObj);
                    this.message = undefined;
                    this.elObj = undefined;
                    this.close = undefined;
                    this.fixPos = undefined;
                    this.show = undefined;
                    this.hide = undefined;
                    this.targetEl = undefined;
                };
                if(imdly){
                    fn();
                }else{
                    $B.fadeOut(this.elObj, {
                        duration: 200,
                        complete: fn
                    });
                }                
            }
        };
        if (isTimeout) {
            closeTimer = setTimeout(() => {
                ret.close();
            }, timeout * 1000);
        }
        el.instance = ret;
        return ret;
    },
    /**
     * 元素绑定鼠标提示
     * el:元素
     * message:提示信息/或者返回信息的函数
     * fixPos :up 、right 、bottom 位置信息
     * color:背景色
     * **/
    mouseTip: function (el, message, fixPos,color) {
        var flag = $B.DomUtils.attribute(el, 'has_tooltip');
        if (!flag) {
            $B.DomUtils.attribute(el, { 'has_tooltip': 1 });
            $B.DomUtils.bind(el, {
                mouseenter: function (e) { 
                    var ins = $B.DomUtils.getData(this, "tooltipInd");
                    if (ins && ins.show) {
                        ins.show(this, fixPos);
                    } else {
                        ins = $B.toolTip(this, message, fixPos,color);
                        $B.DomUtils.setData(this, "tooltipInd", ins);
                    }
                },
                mouseleave: function (e) {
                    var ins = $B.DomUtils.getData(this, "tooltipInd");
                    if (ins && ins.hide) {
                        ins.hide();
                    }
                }
            });
        }
    },
    /**
     *成功信息
    *arg={
            title:'请您确认',
            mask:false,
            position:'top',
            iconCls:'图标样式',
            iconColor:
            content:'提示信息！',
            toolbar:[],//工具栏，如果传入，则不生成默认的按钮
            contentIcon:'内容区域的图标',
            width:宽度
    }
    ***/
    success: function (args) {
        var opts = this._getWinOpt.apply(this, arguments);
        var title = typeof args !== "undefined" ? args.title : undefined;
        if (!title) {
            title = $B.config ? $B.config.successTitle : 'success message';
        }
        opts = $B.extendObjectFn({}, { title: title, iconCls: 'fa-check', iconColor: '#08E358', contentIcon: 'fa-ok-circled' }, opts);
        var win = this._window(opts, "#08E358");
        return win;
    },
    /**
     * message 警告信息对话框
     * opts = {
            title:'请您确认',
            mask:false,
            position:'top',
            iconCls:'图标样式',
            iconColor:
            content:'提示信息！',
            toolbar:[],//工具栏，如果传入，则不生成默认的按钮
            contentIcon:'内容区域的图标',
            width:宽度
    }
    * ***/
    alert: function (args) {
        var opts = this._getWinOpt.apply(this, arguments);
        var title = typeof args !== "undefined" ? args.title : undefined;
        if (!title) {
            title = $B.config ? $B.config.alertTitle : 'alert message';
        }
        opts = $B.extendObjectFn({}, { title: title, iconCls: 'fa-attention-1', iconColor: '#EDA536', contentIcon: 'fa-attention-alt' }, opts);
        var win = this._window(opts, "#EDA536");
        return win;
    },
    /**
     * message 错误信息对话框
     * opts = {
            title:'请您确认',
            mask:false,
            position:'top',
            iconCls:'图标样式',
            iconColor:
            content:'提示信息！',
            toolbar:[],//工具栏，如果传入，则不生成默认的按钮
            contentIcon:'内容区域的图标',
            width:宽度
    }
    * ***/
    error: function (args) {
        var opts = this._getWinOpt.apply(this, arguments);
        var title = typeof args !== "undefined" ? args.title : undefined;
        if (!title) {
            title = $B.config ? $B.config.errorTitle : 'error message';
        }
        opts = $B.extendObjectFn({}, { title: title, iconCls: 'fa-cancel-circled-1', iconColor: '#F72EA7', contentIcon: 'fa-emo-unhappy' }, opts);
        var win = this._window(opts, "#F72EA7");
        return win;
    },
    /**
     *信息提示框
    *args={
            title:'请您确认',
            mask:false,
            position:'top',
            iconCls:'图标样式',
            iconColor:
            content:'提示信息！',
            toolbar:[],//工具栏，如果传入，则不生成默认的按钮
            contentIcon:'内容区域的图标',
            width:宽度
    }
    ***/
    message: function (args) {
        var opts = this._getWinOpt.apply(this, arguments);
        var title = typeof args !== "undefined" ? args.title : undefined;
        if (!title) {
            title = $B.config ? $B.config.messageTitle : 'message';
        }
        opts = $B.extendObjectFn({}, { title: title, iconCls: 'fa-comment', iconColor: '#3BA7EA', contentIcon: 'fa-comment-1' }, opts);
        var win = this._window(opts, "#3BA7EA");
        return win;
    },
    _getWinOpt: function (args) {
        var opts = {
            width: args.width ? args.width : 400,
            title: undefined, //标题
            shadow: typeof args.shadow !== "undefined" ? args.shadow : true, //是否需要阴影
            mask: typeof args.mask !== "undefined" ? args.mask : true, //是否需要遮罩层
            position: typeof args.position !== "undefined" ? args.position : undefined,
            timeout: args.timeout,
            onClosed:typeof args.onClosed === "function" ? args.onClosed : undefined,
            collapseable: false,
            maxminable: false,
            resizeable: false,
            closeable: typeof args.closeable !== "undefined" ? args.closeable : true
        };
        if (args.iconCls) {
            opts.iconCls = args.iconCls;
        }
        if (args.iconColor) {
            opts.iconColor = args.iconColor;
        }
        if (args.contentIcon) {
            opts.contentIcon = args.contentIcon;
        }
        var content;
        if (typeof args === "string") {
            content = args;
            if (arguments.length >= 2) {
                if ($B["isNumericFn"](arguments[1])) {
                    opts.timeout = arguments[1];
                } else {
                    opts.position = arguments[1];
                }
            }
        } else if (args.content) {
            content = args.content;
        }
        opts.content = content;
        if (typeof args.toolbar !== "undefined") {
            opts["toolbar"] = args.toolbar;
        }
        return opts;
    },
    /**
     * 确认提示框
     * args={
            title:'请您确认',
            iconCls:'图标样式',
            content:'提示信息！',
            toolbar:[],//工具栏，如果传入，则不生成默认的按钮
            contentIcon:'内容区域的图标',
            width:width ,//宽度        
            okIcon :"fa-ok-circled",
            noIcon : "fa-ok-circled",
            okText : "确认",           
            noText : "取消",
            okFn:fn, //确认回调
            noFn:fn, //否定回调
    }
    * ***/
    confirm: function (args) {        
        let title = args.title;
        if (!title) {
            title = $B.config ? $B.config.confirmTitle : 'please confirm';
        }
        var opts = {
            size: { width: args.width ? args.width : 400 },
            title: title, //标题
            iconCls: args.iconCls ? args.iconCls : 'fa-question', //图标cls，对应icon.css里的class
            shadow: typeof args.shadow !== "undefined" ? args.shadow : true, //是否需要阴影
            mask: typeof args.mask !== "undefined" ? args.mask : true, //是否需要遮罩层
            iconColor: typeof args.iconColor !== "undefined" ? args.iconColor : undefined,
            position: typeof args.position !== "undefined" ? args.position : undefined,
            collapseable: false,
            maxminable: false,
            resizeable: false,
            contentIcon: args.contentIcon ? args.contentIcon : "fa-help-1"
        };
        var okFn = args.okFn,
            noFn = args.noFn;
        var okIcon = args.okIcon ? args.okIcon : "fa-ok-circled",
            okText = args.okText ? args.okText : $B.config ? $B.config.buttonOkText : 'submit',
            noIcon = args.noIcon ? args.noIcon : "fa-reply-all",
            noText = args.noText ? args.noText : $B.config ? $B.config.buttonCancleText : 'cancel';
        var content, win;
        if (typeof args === "string") {
            content = args;
            if (arguments.length >= 2) {
                if (typeof arguments[1] === "function") {
                    okFn = arguments[1];
                }
            }
            if (arguments.length === 3) {
                if (typeof arguments[2] === "function") {
                    noFn = arguments[2];
                }
            }
            if (arguments.length === 4) {
                if (typeof arguments[3] === "function") {
                    opts.onClosed = arguments[3];
                }
            }
        } else if (args.content) {
            content = args.content;
        }
        opts.content = content;
        if (typeof args.toolbar === "function") {
            opts["toolbar"] = args.toolbar;
        } else {
            opts["toolbar"] = function () {
                var wrap = this;
                setTimeout(() => { $B.DomUtils.css(wrap, { "padding-bottom": 0 }); }, 1);
                var tool = $B.DomUtils.append(wrap, "<div class='k_confirm_buttons_wrap'></div>");
                var ybtn = $B.DomUtils.append(tool, "<button class='yes'><i   class='fa " + okIcon + "'>\u200B</i>" + okText + "</button>");
                $B.DomUtils.click(ybtn, function () {
                    var goClose = true;
                    if (typeof okFn === "function") {
                        var res = okFn();
                        if (typeof res !== "undefined") {
                            goClose = res;
                        }
                    }
                    if (goClose) {
                        win.close(true);
                    }
                });
                var nbtn = $B.DomUtils.append(tool, "<button class='no'><i  class='fa " + noIcon + "'>\u200B</i>" + noText + "</button>");
                $B.DomUtils.click(nbtn, function () {
                    var goClose = true;
                    if (typeof noFn === "function") {
                        var res = noFn();
                        if (typeof res !== "undefined") {
                            goClose = res;
                        }
                    }
                    if (goClose) {
                        win.close(true);
                    }
                });
                setTimeout(() => {
                    let bg = $B.DomUtils.css(ybtn, "background-color");
                    if ($B.isDeepColor(bg)) {
                        ybtn.style.color = "#ffffff";
                    } else {
                        ybtn.style.color = "#1A242C";
                    }
                    bg = $B.DomUtils.css(nbtn, "background-color");
                    if ($B.isDeepColor(bg)) {
                        nbtn.style.color = "#ffffff";
                    } else {
                        nbtn.style.color = "#1A242C";
                    }
                }, 0);
                return tool;
            };
        }
        if(args.onClosed){
            opts.onClosed = args.onClosed;
        }
        win = this._window(opts, '#B2894B');
        return win;
    },
    _window: function (args, iconColor) {
        let title = args.title;
        if (!title) {
            title = $B.config ? $B.config.confirmTitle : 'please set windwo title';
        }
        var opts = {
            size: { width: args.width ? args.width : 400 },
            title: title, //标题
            iconCls: args.iconCls ? args.iconCls : 'fa-question', //图标cls，对应icon.css里的class
            shadow: typeof args.shadow !== "undefined" ? args.shadow : true, //是否需要阴影
            mask: typeof args.mask !== "undefined" ? args.mask : true, //是否需要遮罩层
            timeout: typeof args.timeout !== "undefined" ? args.timeout : undefined,
            iconColor: typeof args.iconColor !== "undefined" ? args.iconColor : undefined,
            position: typeof args.position !== "undefined" ? args.position : undefined,
            collapseable: false,
            maxminable: false,
            resizeable: false,
            closeable: args.closeable,
            toolbar: args.toolbar
        };
        var win,
            contentIcon = args.contentIcon ? args.contentIcon : "fa-help-1";
        var content;
        if (typeof args === "string") {
            content = args;
        } else if (args.content) {
            content = args.content;
        }
        let id = "c" + $B.generateMixed(8);
        if (typeof ccontent === "string" || !content) {
            content = '<div style="position:relative;min-height:35px"><div id="' + id + '" class="k_box_size" style="border-left:50px solid #fff;"><p>' + content + '</p></div><div  class="k_box_size" style="position:absolute;top:2px;left:1px; height:100%;padding:0px 10px;"><i style="line-height:35px;font-size:32px;color:' + iconColor + '" class="fa ' + contentIcon + '"></i></div></div>';
        } else {
            let el = content;
            content = $B.DomUtils.createEl('<div style="position:relative;min-height:35px"><div id="' + id + '" class="k_box_size" style="border-left:50px solid #fff;"><p></p></div><div  class="k_box_size" style="position:absolute;top:2px;left:1px; height:100%;padding:0px 10px;"><i style="line-height:35px;font-size:32px;color:' + iconColor + '" class="fa ' + contentIcon + '"></i></div></div>');
            let pel = $B.DomUtils.findByTagName(content, "p");
            $B.DomUtils.append(pel, el);
        }
        opts.content = content;
        opts.onCreated = function ($body) {
            let sp = $B.DomUtils.findbyId($body, id);
            let $p = $B.DomUtils.children(sp, "p")[0];
            if ($p.clientHeight < 30) {
                $B.DomUtils.css($p, { "line-height": "36px" });
            }
        };
        opts.onClosed = args.onClosed;
        win = this.window(opts);
        return win;
    }
});
var CHARSArr = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
var $spen_CharWidth;
/*********原生扩展***********/
Date.prototype.format = function (format) {
    var date = {
        "M+": this.getMonth() + 1,
        "d+": this.getDate(),
        "h+": this.getHours(),
        "m+": this.getMinutes(),
        "s+": this.getSeconds(),
        "q+": Math.floor((this.getMonth() + 3) / 3),
        "S+": this.getMilliseconds()
    };
    if (/(y+)/i.test(format)) {
        format = format.replace(RegExp.$1, (this.getFullYear() + '').substr(4 - RegExp.$1.length));
    }
    for (var k in date) {
        if (new RegExp("(" + k + ")").test(format)) {
            format = format.replace(RegExp.$1, RegExp.$1.length === 1 ? date[k] : ("00" + date[k]).substr(("" + date[k]).length));
        }
    }
    return format;
};
// 解决四维运算,js计算失去精度的问题,仅支持5位小数点 
function _fixsStrNUM(n){
    let s = n.toString();
    if(s.indexOf(".") > 0){
        let arr = s.split(".");
        let len = arr[1].length;
        if(len > 10){
            len = 10;
            n = parseFloat(arr[0]+"."+arr[1].substring(0,5));
        }
        return {
            n:n,
            r:len
        }
    }
    return {
        n:n,
        r:1
    };
}
//加法   
Number.prototype.add = function (arg) {
    var r1 = _fixsStrNUM(this);
    var r2 =  _fixsStrNUM(arg);
    var m = r1.r > r2.r ? r1.r : r2.r;  
    m = Math.pow(10,m);
    return (r1.n * m + r2.n * m) / m;
};
//减法   
Number.prototype.sub = function (arg) {
    return this.add(-arg);
};
//乘法   
Number.prototype.mul = function (arg) {
    var m = 0, s1 = this.toString(), s2 = arg.toString();
    try { m += s1.split(".")[1].length } catch (e) { }
    try { m += s2.split(".")[1].length } catch (e) { }
    return Number(s1.replace(".", "")) * Number(s2.replace(".", "")) / Math.pow(10, m)
};
//除法   
Number.prototype.div = function (arg) {
    var t1 = 0, t2 = 0, r1, r2;
    try { t1 = this.toString().split(".")[1].length } catch (e) { }
    try { t2 = arg.toString().split(".")[1].length } catch (e) { }
    with (Math) {
        r1 = Number(this.toString().replace(".", ""))
        r2 = Number(arg.toString().replace(".", ""))
        return (r1 / r2) * pow(10, t2 - t1);
    }
};
//十六进制颜色值的正则表达式
var HexReg = /^#([0-9a-fA-f]{3}|[0-9a-fA-f]{6})$/;
/****变量定义******/
var ajaxOpts = {
    waiting: false,
    timeout: 2000 * 60,
    type: "POST",
    dataType: 'json',
    contentType: undefined,
    notEncode: false,
    async: true,
    onErrorEval: false,
    processData: true,
    cache: false,
    dataFilter: undefined,//fn(data, type) 参数过滤处理
    beforeSend: undefined, //fn(xhr, settings) 返回false则不执行
    error: function (xhr, status, errorThrown) {
        this.removeWaiting();
        this.recoverButton();
        var res = status === "timeout" ? "timeout  error!" : "http status " + xhr.status;
        if (window.console) {
            console.log(xhr.responseText);
        }
        try {
            res = eval('(' + xhr.responseText + ')');
        } catch (e) {
        }
        this.onReturn(res, status, xhr);
        this.fail(res, status, xhr);
        this.final(res, status, xhr);
        console.log(xhr.responseText);
    },
    success: function (res, status, xhr) {
        this.removeWaiting();
        this.recoverButton();
        this.onReturn(res, status, xhr);
        if (typeof res.code !== "undefined") {
            if (res.code === 0) {
                var data = res.data;
                if (res.strConvert) {
                    data = eval('(' + res.data + ')');
                }
                this.ok(res.message, data, res);
            } else if (res.code === 99999) {
                if (res.data === "notlogin") {
                    $B.error(res.message);
                    setTimeout(function () {
                        if (window.ctxPath) {
                            window.top.location = $B.getHttpHost(window.ctxPath);
                        }
                    }, 1600);
                } else {
                    var permission = "this is not permission";
                    if ($B.config && $B.config.permission) {
                        permission = $B.config.permission;
                    }
                    $B.error(permission);
                }
            } else {
                this.fail(res.message, res);
            }
        } else {
            this.ok(res, res);
        }
        this.final(res, status, xhr);
    },
    /**
     *当返回结果是正确时的处理
     *data:返回的数据
     *message:提示的信息
     **/
    ok: function (message, data) {
    },
    /***
     *当返回结果是非正确时的处理
     ***/
    fail: function (msg, res) {
        let $b = $B.getBody();
        if ($B.DomUtils.getChildrenByClass($b, "._request_fail_window_").length === 0) {
            let errWin = $B.error(msg, 2);
            $B.DomUtils.addClass(errWin.elObj, "_request_fail_window_");
            if (msg.indexOf("not login") >= 0) {
                window.top.location.href = $B.getHttpHost(window.ctxPath);
            }
        } else {
            console.log(" fail fail 》》》》》》》》》》》》》》》 ", msg, res);
        }
    },
    onReturn: function (res, status, xhr) {
    },
    /**
     * 无论如何都回调的函数
     ****/
    final: function (res, status, xhr) { //无论成功，失败，错误都执行的回调
    },
    recoverButton: function () {
        if (this["target"]) {
            //this["target"].removeAttribute("disabled");
            $B.DomUtils.removeAttribute(this["target"], "disabled");
            $B.DomUtils.html(this["target"], this.recoverText);
            this["target"] = undefined;
        }
    },
    removeWaiting: function () {
        if (this.waiting) {
            this.waiting = undefined;
            this.waitingObj.close();
            this.waitingObj = undefined;
        }
    }
};
_extendObjectFn($B, {
    iframeHtml: "<iframe  class='' frameborder='0' style='overflow:visible;height:100%;width:100%;display:block;vertical-align:top;'  src='' ></iframe>",
    loadingHtml: "<div class='k_box_size k_loading_el' style='position:absolute;z-index:2147483600;width:100%;height:26px;top:2px;left:0;' class='loading'><div class='k_box_size' style='opacity: 0.5;position:absolute;top:0;left:0;width:100%;height:100%;z-index:2147483600;background-color:loadingBackground'></div><div class='k_box_size' style='width:100%;height:100%;line-height:26px;padding-left:16px;position:absolute;width:100%;height:100%;z-index:2147483611;color:#fff;text-align:center;'><i style='color:#fff;font-size:16px;' class='fa animate-spin fa-spin6'></i><span style='padding-left:5px;font-weight:normal;color:#fff;'>loadingTxt</span></div></div>",
    /**框架的ajax统一入口
     *所有ajax返回均以 res={code:'',message:'',data:{}}的格式返回
    *code=0表示服务器无异常运行并返回结果，code=1时，表示服务器出现异常并返回提示
    *message，用与服务器返回的信息提示
    *data,用于服务器返回的数据，如tree组件、datagrid组件返回的数据就保存到data当中
    args={
            waiting:false ,     //是否显示waiting
            timeout: 1000 * 60, //超时
            type: "POST",       //请求方式
            dataType: 'json',   //请求数据类型
            async: true,        //是否异步
            preRequest: fn,     //请求前，回调
            url:'',             //请求url
            data:{},            //请求参数
            ok:function(message,data){},    //成功回调，message:返回信息，data:返回数据
            fail:function(message){},       //失败回调，message:返回信息
            final:function(res){}           //无论成功/失败都调用的回调 res = {code:'',message:'',data:{}}
    }
    ****/
    request: function () {
        var args = arguments[0];
        var opts;
        if (args !== undefined) {
            opts = _extendObjectFn({}, ajaxOpts, args);
        } else {
            opts = ajaxOpts;
        }
        if (opts.data && typeof opts.data === "object" && opts.data.constructor.name !== "FormData") {
            for (var prop in opts.data) {
                if (opts.data[prop] === null) {
                    delete opts.data[prop];
                } else {
                    if (opts.notEncode) {
                        opts.data[prop] = opts.data[prop];
                    } else {
                        opts.data[prop] = this.htmlEncode(opts.data[prop]);
                    }
                }
            }
        }
        var queue = window["_submit_queues"];
        var submitBtn;
        if (queue && queue.length > 0) {
            let lastIdx = queue.length - 1;
            let diff = queue[lastIdx].date - new Date();
            if (diff <= 500) {//500毫秒内
                submitBtn = queue[lastIdx].btn;
                let q = queue.shift();
                q.btn = undefined;
            }
        }
        if (arguments.length > 1 || submitBtn) {
            let btn = arguments.length > 1 ? arguments[1] : submitBtn;
            opts["target"] = btn;
            $B.DomUtils.attribute(btn, { "disabled": "disabled" });
            let busyIng = "this is processing!";
            if ($B.config && $B.config.busyTipTitle) {
                busyIng = $B.config.busyTipTitle;
            }
            if (btn.nodeName === "INPUT") {
                opts["recoverText"] = btn.innerText;
                btn.val(busyIng);
            } else {
                opts["recoverText"] = btn.innerHTML;
                let color = btn.style.color;
                if (btn.lastChild) {
                    color = btn.lastChild.style.color;
                }
                btn.innerHTML = "<i style='padding:0;color:" + color + ";' class='fa fa-spin6 animate-spin'></i><span style='color:" + color + ";padding-left:5px;'>" + busyIng + "</span>";
            }
        }
        if (opts.waiting) {
            let $w = $B.busyTip({});
            opts.waitingObj = $w;
        }
        $B.ajax(opts);
    },
    forbidSelected: function () {
        document.selection && document.selection.empty && (document.selection.empty(), 1)
            || window.getSelection && window.getSelection().removeAllRanges();
    },
    _getSuitablePos: function ($wrap, targetel, $body) {
        let top, left, arrowLeft, maxHeight;
        let wrapWidth = $B.DomUtils.outerWidth($wrap);
        let fixWidth = $B.DomUtils.attribute(targetel, "fix_width");
        let wrapHeight = $B.DomUtils.outerHeight($wrap);
        if ($B.DomUtils.hasClass($wrap, "k_dropdown_list_wrap")) {
            let childLen = $B.DomUtils.attribute($wrap, "_itlen");
            if (childLen) {
                wrapHeight = 24 * parseInt(childLen);
            } else {
                let itWrap = $wrap.lastChild.firstChild.firstChild;
                if (itWrap.children && itWrap.children.length > 0) {
                    wrapHeight = 24 * itWrap.children.length;
                }
            }
            wrapHeight = wrapHeight + 2;
        }

        let elPos = $B.DomUtils.offset(targetel);
        let elHeight = $B.DomUtils.outerHeight(targetel);
        let elWidth = $B.DomUtils.outerWidth(targetel);
        if (fixWidth) {
            elWidth = parseFloat(fixWidth);
        }
        let bodyHeight = $body.clientHeight;
        let bodyWidth = $body.clientWidth;
        // var scrollTop = $B.DomUtils.scrollTop($body);
        // var scrollLeft = $B.DomUtils.scrollLeft($body);
        // elPos.top = elPos.top - scrollTop;
        // elPos.left = elPos.left - scrollLeft;
        //console.log(" _getSuitablePos = " + JSON.stringify(elPos));


        //先检测右下方是否够空间，确认top位置
        let aviHeight = bodyHeight - elPos.top - elHeight;
        let arrowCss = {};
        if (aviHeight >= wrapHeight || elPos.top < aviHeight) {//空间够
            top = elPos.top + elHeight + 8;
            arrowCss["top"] = -14;
            maxHeight = bodyHeight - top - 5;
            arrowCss["transform"] = "rotate(0)";
        } else if ((elPos.top - 6) > aviHeight) {//上面空间比下面的大
            top = elPos.top - wrapHeight - 6;
            arrowCss["bottom"] = -14;
            arrowCss["transform"] = "rotate(180deg)";
            if (top < 2) {
                top = 2;
            }
            maxHeight = elPos.top - 10;
        } else {
            wrapHeight = aviHeight - 8;
            top = elPos.top + elHeight + 8;
            arrowCss["top"] = -14;
            maxHeight = bodyHeight - top - 5;
            arrowCss["transform"] = "rotate(0)";
        }
        let wwWidth = wrapWidth;
        if (wrapWidth < elWidth) {
            wwWidth = elWidth;
        }
        let suitableWidth = bodyWidth * 0.45;
        let needFix2px = false;
        if (wwWidth > suitableWidth) {
            wwWidth = suitableWidth;
            needFix2px = true;
        }
        left = elPos.left;
        //检测合适的left,这里有问题，需要修正
        // left = elPos.left + parseInt(elWidth / 2) - parseInt(wwWidth / 2);
        // if (left < 2) {
        //     left = 2;
        // } else if ((left + wwWidth) > bodyWidth) {
        //     left = bodyWidth - wwWidth;
        // }
        // if (needFix2px) {
        //     left = left - 2;
        // }
        arrowLeft = elPos.left - left + elWidth / 2 - 9;
        arrowCss["left"] = arrowLeft;
        var res = { arrowPos: arrowCss, pos: { top: top, left: left, width: wwWidth, height: wrapHeight, "max-height": maxHeight } };
        return res;
    },
    _createDropListItems: function (items, opts, $inner, $input, resetHeight) {
        $inner.innerHTML = "";
        if (items.length === 0) {
            let nodata = ($B.config && $B.config.returnEmptyData) ? $B.config.returnEmptyData : 'the return is empty!';
            $inner.innerHTML = "<div style='padding-top:6px' class='k_dropdown_list_item not_data'><i style='color:#FF6D34' class='fa fa-info'></i><span style='padding-left:10px;color:#666;'>" + nodata + "</span></div>";
        } else {
            let wrap = $inner;
            let height = 0;
            for (let i = 0; i < items.length; i++) {
                let txt = opts.textField ? items[i][opts.textField] : items[i].text;
                let id = opts.idField ? items[i][opts.idField] : items[i].id;
                id = id + "";
                let $it = $B.DomUtils.append(wrap, "<div class='k_dropdown_list_item'>" + txt + "</div>");
                $B.DomUtils.attribute($it, { dataid: id });
                if (items[i].selected) {
                    $B.DomUtils.addClass($it, "k_dropdown_item_selected");
                    if ($input) {
                        if (!$B.DomUtils.attribute($input, "readonly") && !$B.DomUtils.attribute($input, "multiple")) {
                            $input.value = items[i].text;
                        }
                        if (opts.multiple || Array.isArray($input.selectedValue)) {
                            let put = true;
                            if (!Array.isArray($input.selectedValue)) {
                                $input.selectedValue = [];
                            } else {
                                let varr = $input.selectedValue;
                                for (let j = 0; j < varr.length; j++) {
                                    if (varr[j].id === id) {
                                        put = false;
                                        break;
                                    }
                                }
                            }
                            if (put) {
                                $input.selectedValue.push({ id: id, text: txt });
                            }
                        } else {
                            $input.selectedValue = [{ id: id, text: txt }];
                        }
                    }
                }
                height = height + 24;
            }
            if (resetHeight) {
                var ddlEl = $inner.parentNode.parentNode.parentNode;
                if ($B.DomUtils.css(ddlEl, "display") === "block") {
                    let body = $B.getBody();
                    let pos = $B.DomUtils.position(ddlEl);
                    let aviHeight = $B.DomUtils.height(body) - pos.top - 10;
                    if (height > aviHeight) {
                        height = aviHeight;
                    }
                    $B.DomUtils.height(ddlEl, height + 2);
                }
            }
        }

    },
    createGlobalBodyHideEv: function () {
        var $body = $B.getBody();
        if (!$B.DomUtils.attribute($body, "has_globel_droplist")) {
            $B.DomUtils.attribute($body, { "has_globel_droplist": 1 });
            $B.DomUtils.mousedown($body, (e) => {
                if (window["_globalDraging"]) {
                    return true;
                }
                let target = e.target;
                while (target) {
                    if (target === $B._0001currentdroplistEl) {
                        $B._0001currentdroplistEl = undefined;
                        return true;
                    }
                    if ($B.DomUtils.hasClass(target, "k_dropdown_list_wrap")) {
                        return true;
                    }
                    target = target.parentNode;
                    if (target.nodeName === "#document" || target.tagName === "BODY") {
                        break;
                    }
                }
                let elArr = $B.DomUtils.children($body, ".k_dropdown_list_wrap");
                if (elArr.length > 0) {
                    for (let i = 0; i < elArr.length; i++) {
                        let el = elArr[i];
                        if (el.style.display !== "none") {
                            $B.slideUp(el, 150);
                            let _dlistgid = $B.DomUtils.attribute(el, "_dlistgid");
                            if (_dlistgid) {
                                let $i = document.getElementById(_dlistgid);
                                if ($i) {
                                    let $ic = $B.DomUtils.children($i, "i");
                                    if ($ic.length > 0) {
                                        $B.animate($ic[0], { "rotateZ": "0deg" }, { duration: 200 });
                                    }
                                }
                            }
                        }
                    }
                }
            });
            let elCachedArr = [], cachedTimer;
            $B.DomUtils.resize(window, (e) => {
                cachedTimer = setTimeout(() => {
                    elCachedArr = [];
                }, 1000);
                if (elCachedArr.length > 0) {
                    for (let i = 0; i < elCachedArr.length; i++) {
                        let el = elCachedArr[i].el;
                        let srcEl = elCachedArr[i].srcEl;
                        let ofs = $B.DomUtils.offset(srcEl);
                        let left = ofs.left;
                        el.style.left = left + "px";
                    }
                } else {
                    let allDdls = $B.DomUtils.children($body, ".k_dropdown_list_wrap");
                    for (let i = 0; i < allDdls.length; i++) {
                        let el = allDdls[i];
                        if (!$B.DomUtils.isHide(el)) {
                            let elId = $B.DomUtils.attribute(el, "_dlistgid");
                            let srcEl = document.getElementById(elId);
                            if (srcEl) {
                                let ofs = $B.DomUtils.offset(srcEl);
                                let left = ofs.left;
                                el.style.left = left + "px";
                                elCachedArr.push({
                                    el: el,
                                    srcEl: srcEl
                                });
                            }
                        }
                    }
                }

            });
        }
    },
    /***
     * el:需要下拉选项的元素
     * items:[{id:id,text:txt,selected:true}]
     * opts:{onClick:fn,        //返回true则执行自动关闭
     *       onCreate:fn,
     *       textField，
     *       idField   
     *       motionless:true    是否静默
     *       multiple:false     是否多选
     *       isTree:false       是否树形
     * }
     * ****/
    createDropList: function (el, items, opts) {
        if (Array.isArray(el)) {
            el = el[0];
        }
        var $wrap;
        var $body = $B.getBody();
        this.createGlobalBodyHideEv();
        var $icon = $B.DomUtils.children(el, "i");
        let globalDownid = $B.DomUtils.attribute(el, "_droplistid");
        if (!globalDownid) {
            let id = $B.getUUID();
            $B.DomUtils.attribute(el, { "_droplistid": id });
            $wrap = $B.DomUtils.createEl("<div style='padding:0;margin:0;width:auto;display:none;z-index:" + $B.config.maxZindex + ";top:-100000px;position:absolute;' id='" + id + "' class='k_dropdown_list_wrap k_box_size'></div>");
            let targetid = $B.DomUtils.attribute(el, "id");
            if (!targetid) {
                targetid = $B.getUUID();
                $B.DomUtils.attribute(el, { "id": targetid });
            }
            $B.DomUtils.attribute($wrap, { "_dlistgid": targetid });
            let $inner = $B.DomUtils.createEl("<div class='k_dropdown_inner' style='position:absolute;top:-1000000000px;width:auto;padding:0;margin:0;'></div>");
            let $input = el.firstChild;
            if ($input.tagName !== "INPUT" && $B.DomUtils.attribute($input, "type") !== "text") {
                $input = undefined;
            }
            if (!opts.onCreate) {
                this._createDropListItems(items, opts, $inner, $input);//
            }
            let defautlClick = true;
            if (opts.onCreate) {
                let ret = opts.onCreate(items, opts, $inner, $input);
                if (typeof ret !== "undefined") {
                    if ($B.isPlainObjectFn(ret)) {
                        defautlClick = ret.defautlClick;
                        $B.DomUtils.attribute($wrap, { "_itlen": ret._itlen });
                    } else {
                        defautlClick = ret;
                    }
                }
            }
            $B.DomUtils.append($body, $inner);
            let h = $B.DomUtils.outerHeight($inner);
            let w = $B.DomUtils.outerWidth($inner);
            if (h < 30) {
                h = 30;
            }
            $B.DomUtils.css($wrap, { width: w + 2, height: h + 2 });
            $B.DomUtils.detach($inner);
            $B.DomUtils.removeAttribute($inner, "style");
            $B.DomUtils.css($inner, { height: '100%', width: '100%' });
            $B.DomUtils.append($wrap, $inner);
            $B.myScrollbar($inner, {});
            $B.DomUtils.prepend($wrap, "<div style='position:absolute;width:12px;' class='_droplist_attrow'><i style='font-size:18px;' class='fa fa-up-dir'></i></div>");
            $B.DomUtils.append($body, $wrap);
            $B.DomUtils.mousedown(el, function () {
                $B._0001currentdroplistEl = this;
            });
            if (defautlClick) {
                $B.DomUtils.click($inner, (e) => {
                    if ($B.DomUtils.hasClass(e.target, "not_data")) {
                        return true;
                    }
                    if ($B.DomUtils.hasClass(e.target, "k_dropdown_list_item")) {
                        let id = $B.DomUtils.attribute(e.target, "dataid");
                        let data = { id: id, text: e.target.innerText };
                        let isSelected = true;
                        if (opts.multiple) { //如果是多选
                            if ($B.DomUtils.hasClass(e.target, "k_dropdown_item_selected")) {
                                $B.DomUtils.removeClass(e.target, "k_dropdown_item_selected");
                                isSelected = false;
                            } else {
                                $B.DomUtils.addClass(e.target, "k_dropdown_item_selected");
                            }
                        } else {
                            let siglings = $B.DomUtils.siblings(e.target);
                            for (let i = 0; i < siglings.length; i++) {
                                if ($B.DomUtils.hasClass(siglings[i], "k_dropdown_item_selected")) {
                                    $B.DomUtils.removeClass(siglings[i], "k_dropdown_item_selected");
                                    break;
                                }
                            }
                            $B.DomUtils.addClass(e.target, "k_dropdown_item_selected");
                        }
                        if (opts.onClick) {
                            setTimeout(() => {
                                if (opts.onClick(data, e.target, isSelected)) {
                                    if ($icon.length > 0) {
                                        $B.animate($icon[0], { "rotateZ": "0deg" }, { duration: 200 });
                                    }
                                    $B.fadeOut($wrap, 260);
                                }
                            }, 1);
                        } else {
                            if ($icon.length > 0) {
                                $B.animate($icon[0], { "rotateZ": "0deg" }, { duration: 200 });
                            }
                            $B.fadeOut($wrap, 260);
                        }
                    }
                });
            }
            $B.DomUtils.click(el, function () {
                $B.createDropList(this);
                if ($B.DomUtils.hasClass(this, "k_combox_wrap")) {
                    if (!$B.DomUtils.attribute(this.firstChild, "readonly")) {
                        this.firstChild.focus();
                    }
                }
            });
            $wrap.hideFn = function () {
                $B.fadeOut(this, 260);
                if ($icon.length > 0) {
                    $B.animate($icon[0], { "rotateZ": "0deg" }, { duration: 200 });
                }
            };
            $wrap.showFn = function () {
                if ($B.DomUtils.css(this, "display") === "none") {
                    $B.createDropList(el);
                }
            };
            $wrap.unSelectedFn = function (dataid) {
                let childs = this.lastChild.firstChild.firstChild.children;
                for (let i = 0; i < childs.length; i++) {
                    if (dataid === $B.DomUtils.attribute(childs[i], "dataid")) {
                        $B.DomUtils.removeClass(childs[i], "k_dropdown_item_selected")
                        break;
                    }
                }
            };
            $wrap.selectedFn = function (dataid) {
                let childs = this.lastChild.firstChild.firstChild.children;
                for (let i = 0; i < childs.length; i++) {
                    if (dataid === $B.DomUtils.attribute(childs[i], "dataid")) {
                        $B.DomUtils.addClass(childs[i], "k_dropdown_item_selected")
                        break;
                    }
                }
            };
            $wrap._clearAllFn = function () {
                $wrap.hideFn = undefined;
                $wrap.showFn = undefined;
                $wrap.selectedFn = undefined;
                $wrap.unSelectedFn = undefined;
                $wrap._clearAllFn = undefined;
                $wrap.clearAll = undefined;
            };
            $wrap.clearAll = $wrap._clearAllFn;
        } else {
            $wrap = $B.DomUtils.children($body, "#" + globalDownid);
            if ($B.DomUtils.css($wrap, "display") === "block") {
                if ($icon.length > 0) {
                    $B.animate($icon[0], { "rotateZ": "0deg" }, { duration: 200 });
                }
                $B.fadeOut($wrap, 260);
                return;
            }
        }
        if (opts && opts.motionless) {
            return $wrap;
        }
        if ($wrap) {
            if ($B.DomUtils.css($wrap, "display") === "none") {
                $B.DomUtils.css($wrap, { "display": "block", "top": "-99999999px" });
            }
            let ret = this._getSuitablePos($wrap, el, $body);
            let $arrow = $B.DomUtils.children($wrap, "._droplist_attrow")[0];
            $B.DomUtils.css($arrow, ret.arrowPos);
            let styleAttr = $B.DomUtils.attribute($arrow, "style");
            let styleObj = $B.style2cssObj(styleAttr);
            if (ret.arrowPos.top) {
                delete styleObj.bottom;
            } else {
                delete styleObj.top;
            }
            styleAttr = $B.cssObj2string(styleObj);
            $B.DomUtils.attribute($arrow, { "style": styleAttr });
            $B.DomUtils.css($wrap, ret.pos);
            $wrap = $B.DomUtils.detach($wrap);
            $B.DomUtils.append($body, $wrap);
            if ($icon.length > 0) {
                $B.animate($icon[0], { "rotateZ": "180deg" }, { duration: 200 });
            }
            let $c = $wrap.lastChild.firstChild.firstChild;
            if ($c.children && $c.children.length > 0) {
                for (let i = 0; i < $c.children.length; i++) {
                    $c.children[i].style.display = "block";
                }
            }
            $B.fadeIn($wrap, 260, function () {
                //获取所有项目的最大宽度，设置每一个项目的width为最大宽度
                let $iner = $wrap.lastChild;
                let tmpArr = $B.DomUtils.children($iner.firstChild.firstChild);
                let maxWidth = 0;
                for (let i = 0; i < tmpArr.length; i++) {
                    if (tmpArr[i].scrollWidth > maxWidth) {
                        maxWidth = tmpArr[i].scrollWidth;
                    }
                }
                if (maxWidth > ret.pos.width) {
                    maxWidth = maxWidth + "px";
                    for (let i = 0; i < tmpArr.length; i++) {
                        tmpArr[i].style.width = maxWidth;
                    }
                }
            });
        }
        return $wrap;
    },
    params2urlString: function (params, encode) {
        if (typeof encode === "undefined") {
            encode = true;
        }
        let keys = Object.keys(params);
        let res = [], val;
        for (let i = 0; i < keys.length; i++) {
            if (encode) {
                val = encodeURIComponent(params[keys[i]]);
            } else {
                val = params[keys[i]];
            }
            res.push(keys[i] + "=" + val);
        }
        res.join("&");
    },
    getIframeEl: function (clazz) {
        var el = $B.DomUtils.createEl($B.iframeHtml);
        if (clazz) {
            $B.DomUtils.addClass(el, clazz);
        }
        return el;
    },
    getIconLoading: function (clazz) {
        let el = $B.DomUtils.createEl("<div class=''><i style='font-size:16px' class='fa fa-spin5'></i></div>");
        if (typeof clazz === "string") {
            $B.DomUtils.addClass(el, clazz);
        } else if ($B.isPlainObjectFn(clazz)) {
            let i = $B.children(el, "i");
            $B.DomUtils.css(i, clazz);
        }
        return el;
    },
    getLoadingEl: function (text, background) {
        let cfg = $B.getLoadingCfg();
        if (!text) {
            text = cfg.text;
        }
        if (!background) {
            background = cfg.color;
        }
        var html = $B.loadingHtml.replace("loadingTxt", text);
        if (background) {
            html = html.replace("loadingBackground", background);
        }
        let el = $B.DomUtils.createEl(html);
        let isDeep = this.isDeepColor(background);
        if (!isDeep) {
            let childs = $B.DomUtils.children(el)[1];
            $B.DomUtils.css(childs, { color: "#666" });
            childs = $B.DomUtils.children(childs);
            $B.DomUtils.css(childs, { color: "#666" });
        }
        return el;
    },
    getLoadingCfg: function () {
        let config = $B["config"];
        let loadingBackground = "#5D39F0";
        let loadingTxt = "this is loading , please waiting!";
        if (config) {
            if (config.loadingBackground) {
                loadingBackground = config.loadingBackground;
            }
            if (config.loading) {
                loadingTxt = config.loading;
            }
        }
        return {
            color: loadingBackground,
            text: loadingTxt
        };
    },
    removeLoading: function (loading, fn) {
        $B.fadeOut(loading, () => {
            $B.DomUtils.remove(loading);
            if (fn) {
                fn();
            }
        }, 200);
    },
    getCharWidth: function (text, fs) {
        if (typeof fs === 'undefined') {
            fs = 14;
        }
        if (!$spen_CharWidth) {
            $spen_CharWidth = $B.DomUtils.createEl("<span style='position:absolute;white-space:nowrap;top:-90000000px;left:10000000px'></span>");
            $B.DomUtils.append($B.getBody(), $spen_CharWidth);
        }
        $spen_CharWidth.style.fontSize = fs + "px";
        var w = 0;
        try {
            $spen_CharWidth.innerText = this.htmlEncode(text);
            w = Math.ceil($B.DomUtils.width($spen_CharWidth));
            setTimeout(function () {
                $spen_CharWidth.innerText = "";
            }, 1);
        } catch (ex) {
            console.log(ex);
        }
        return w;
    },
    /***
     * 是否是深色
     * color: 颜色，支持十六进制或者rgba格式
     * ***/
    isDeepColor: function (color) {
        if (color === "none") {
            return false;
        }
        var tempArray
        if (color.indexOf("#") >= 0) {
            color = $B.hex2RgbObj(color);
            tempArray = [];
            tempArray.push(color.r);
            tempArray.push(color.g);
            tempArray.push(color.b);
        } else {
            let rgbval = color.replace("rgb(", "").replace(")", "");
            tempArray = rgbval.split(",");
        }
        var level = tempArray[0] * 0.299 + tempArray[1] * 0.587 + tempArray[2] * 0.114;
        if (level <= 180) {
            return true;
        } else {
            return false;
        }
    },
    rgb2Hsb: function (arg) {
        var rgb = arg;
        if (typeof rgb === "string") {

        }
        var hsb = {
            h: 0,
            s: 0,
            b: 0
        };
        var min = Math.min(rgb.r, rgb.g, rgb.b);
        var max = Math.max(rgb.r, rgb.g, rgb.b);
        var delta = max - min;
        hsb.b = max;
        hsb.s = max !== 0 ? 255 * delta / max : 0;
        if (hsb.s !== 0) {
            if (rgb.r === max) {
                hsb.h = (rgb.g - rgb.b) / delta;
            } else if (rgb.g === max) {
                hsb.h = 2 + (rgb.b - rgb.r) / delta;
            } else {
                hsb.h = 4 + (rgb.r - rgb.g) / delta;
            }
        } else {
            hsb.h = -1;
        }
        hsb.h *= 60;
        if (hsb.h < 0) {
            hsb.h += 360;
        }
        hsb.s *= 100 / 255;
        hsb.b *= 100 / 255;
        return hsb;
    },
    hex2Hsb: function (hex) {
        var hsb = this.rgb2Hsb(this.hex2RgbObj(hex));
        if (hsb.s === 0) {
            hsb.h = 360;
        }
        return hsb;
    },
    hsb2Hex: function (hsb) {
        return this.rgb2Hex(this.hsb2Rgb(hsb));
    },
    hsb2Rgb: function (hsb) {
        var rgb = {};
        var h = Math.round(hsb.h);
        var s = Math.round(hsb.s * 255 / 100);
        var v = Math.round(hsb.b * 255 / 100);
        if (s === 0) {
            rgb.r = rgb.g = rgb.b = v;
        } else {
            var t1 = v;
            var t2 = (255 - s) * v / 255;
            var t3 = (t1 - t2) * (h % 60) / 60;
            if (h === 360) {
                h = 0;
            }
            if (h < 60) {
                rgb.r = t1;
                rgb.b = t2;
                rgb.g = t2 + t3;
            } else if (h < 120) {
                rgb.g = t1;
                rgb.b = t2;
                rgb.r = t1 - t3;
            } else if (h < 180) {
                rgb.g = t1;
                rgb.r = t2;
                rgb.b = t2 + t3;
            } else if (h < 240) {
                rgb.b = t1;
                rgb.r = t2;
                rgb.g = t1 - t3;
            } else if (h < 300) {
                rgb.b = t1;
                rgb.g = t2;
                rgb.r = t2 + t3;
            } else if (h < 360) {
                rgb.r = t1;
                rgb.g = t2;
                rgb.b = t1 - t3;
            } else {
                rgb.r = 0;
                rgb.g = 0;
                rgb.b = 0;
            }
        }
        return {
            r: Math.round(rgb.r),
            g: Math.round(rgb.g),
            b: Math.round(rgb.b)
        };
    },
    /***
     * rgba颜色转十六进制
     * ****/
    rgb2Hex: function (rgbColor) {
        var that = rgbColor;
        if (/^(rgb|RGB)/.test(that)) {
            var aColor = that.replace(/(?:\(|\)|rgb(a)?|RGB(A)?)*/g, "").split(",");
            var strHex = "#";
            let len = aColor.length;
            if (len > 3) {
                len = 3;
            }
            for (var i = 0; i < len; i++) {
                var hex = Number(aColor[i]).toString(16);
                if (hex.length === 1) {
                    hex = "0" + hex;
                }
                if (hex === "0") {
                    hex += hex;
                }
                strHex += hex;
            }
            if (strHex.length !== 7) {
                strHex = that;
            }
            return strHex.toUpperCase();
        } else if (HexReg.test(that)) {
            var aNum = that.replace(/#/, "").split("");
            if (aNum.length === 6) {
                return that.toUpperCase();
            } else if (aNum.length === 3) {
                var numHex = "#";
                for (var j = 0; j < aNum.length; j += 1) {
                    numHex += (aNum[j] + aNum[j]);
                }
                return numHex.toUpperCase();
            }
        } else if ($B.isPlainObjectFn(that)) {
            var hex = [
                that.r.toString(16),
                that.g.toString(16),
                that.b.toString(16)
            ];
            for (let i = 0; i < hex.length; i++) {
                let val = hex[i];
                if (val.length === 1) {
                    hex[i] = '0' + val;
                }
            }
            if (typeof that.a !== "undefined") {
                hex.push(that.a);
            }
            return '#' + hex.join('');
        } else {
            return that.toUpperCase();
        }
    },
    /**
     * 十六进制转rgba object对象
     * ***/
    hex2RgbObj: function (hexColor) {
        if (HexReg.test(hexColor)) {
            if (hexColor.length === 4) {
                var sColorNew = "#";
                for (var i = 1; i < 4; i += 1) {
                    sColorNew += hexColor.slice(i, i + 1).concat(hexColor.slice(i, i + 1));
                }
                hexColor = sColorNew;
            }
            //处理六位的颜色值
            var sColorChange = [];
            for (var j = 1; j < 7; j += 2) {
                sColorChange.push(parseInt("0x" + hexColor.slice(j, j + 2)));
            }
            return {
                r: sColorChange[0],
                g: sColorChange[1],
                b: sColorChange[2]
            };
        }
    },
    /**
  * 十六进制转rgba 字符串
  * ***/
    hex2Rgb: function (hexColor) {
        var rgbObj = this.hex2RgbObj(hexColor);
        if (rgbObj) {
            return "RGB(" + rgbObj.r + "," + rgbObj.g + "," + rgbObj.b + ")";
        }
    },
    rgbaStr2Obj: function (str) {
        let ret = /^rgb|RGB\((\d+),\s*(\d+),\s*(\d+),\s*([0,1]\.\d+)\)$/.exec(str);
        return {
            r: ret[1],
            g: ret[2],
            b: ret[3]
        };
    },
    getContrastColor: function (value) {
        if (!value) {
            return 'rgb(105, 118, 166)';
        }
        var rgb = value;
        if (typeof rgb === "string" && value.toLowerCase().indexOf("rgb") < 0) {
            rgb = this.hex2Rgb(value);
        }
        rgb.r = 255 - rgb.r;
        rgb.g = 255 - rgb.g;
        rgb.b = 255 - rgb.b;
        return ['rgb(', rgb.r, ',', rgb.g, ',', rgb.b, ')'].join('');
    },
    /**
     * 获取当前document的body，并且将其position声明为 relative
     * ***/
    getDomBody: function () {
        if (!$body) {
            $body = document.body.style.position = "relative";
        }
        return $body;
    },
    /****获取的当前浏览器的主机应用地址 *****/
    getHttpHost: function (ctxPath) {
        var proto = window.location.protocol;
        var host = proto + "//" + window.location.host;
        var ctx;
        if (!ctxPath && window.ctxPath) {
            ctx = window.ctxPath;
        } else if (ctxPath) {
            ctx = ctxPath;
        }
        if (ctx) {
            host = host + ctx;
        }
        return host;
    },
    /**
     * 用于优化递归实现的函数
     * ***/
    recursionFn: function (fn, isRemain) {
        var active = false;
        var accumulated = [];
        return function executeFn() {
            if (arguments.length === 1) {
                if (typeof arguments[0] === "string" && arguments[0] === "destroy") {
                    fn = undefined;
                    accumulated = undefined;
                    return;
                }
            }
            accumulated.push(arguments);//每次将参数传入. 例如, 1 100000
            if (!active) {
                active = true;
                while (accumulated.length) {
                    let args = accumulated.pop();
                    fn.apply(this, args);
                }
                active = false;
                if (!isRemain) {
                    fn = undefined;
                }
            }
        };
    },
    /***
     * args={
     *      url:,
     *      success:fn(data, status, xhr)
     *      error:fn(xhr, type, error),
     *      complete:fn()
     * }
     * ***/
    htmlLoad: function (args, targetEl) {
        var loadingEl;
        if (targetEl) {
            let el = $B.DomUtils.children(targetEl, ".k_loading_el");
            if (el.length === 0) {
                loadingEl = $B.getLoadingEl();
                $B.DomUtils.append(targetEl, loadingEl);
            }
            let p = $B.DomUtils.children(targetEl, ".k_req_error_p");
            $B.DomUtils.remove(p);
        }
        var defopt = {
            success: function (data, status, xhr) {
                if (targetEl) {
                    targetEl.innerHTML = data;
                }
            },
            error: function (xhr, type, error) {
                if (targetEl) {
                    targetEl.innerHTML = "<p class='k_req_error_p' style='text-align:center;'><i style='color:#D6D603' class='fa fa-attention'></i><span style='padding-left:10px'>" + xhr.statusText + "：" + xhr.status + "</span></p>";
                }
            },
            complete: function () {
                if (loadingEl) {
                    $B.removeLoading(loadingEl);
                }
            }
        };
        var opt;
        if (typeof args === "string") {
            opt = defopt;
            opt["url"] = args;
        } else {
            opt = $B.extendObjectFn({}, defopt, args);
        }
        $B.ajax(opt);
    },
    /**返回当前时间的格式化***/
    formateNow: function (format) {
        if (!format) {
            format = "yyyyMMddhhmmss";
        }
        return this.formateDate(new Date(), format);
    },
    /***格式化data***/
    formateDate: function (date, format) {
        return date.format(format);
    },
    /***生成随机数***/
    random: function (lower, upper) {
        if (!lower) {
            lower = 0;
        }
        if (!upper) {
            upper = 10000;
        }
        return Math.floor(Math.random() * (upper - lower)) + lower;
    },
    /***
     * 获取元素el的transform矩阵信息
     * ****/
    getMatrixArray: function (el) {
        var res;
        var matrix = el.style["transform"];
        if (matrix && matrix !== "none") {
            var values = matrix.split('(')[1].split(')')[0].split(',');
            res = [];
            for (var i = 0; i < values.length; i++) {
                res.push(parseFloat(values[i]));
            }
        }
        return res;
    },
    /**获取元素旋转后的位置偏移量**/
    getAnglePositionOffset: function (el) {
        var ofs = { fixTop: 0, fixLeft: 0 };
        var matrixArr = this.getMatrixArray(el);
        if (matrixArr) {
            var pos = $B.DomUtils.position(el);
            var angle = $B.getMatrixAngle(matrixArr);
            if (angle !== 0) {
                var clone = document.createElement("div");
                var style = el.getAttribute("style");
                clone.setAttribute("style", style);
                $B.DomUtils.css(clone, { "transform": "rotate(0deg)", "opacity": "0", "position": "absolute", "z-index": -111 });
                el.parentNode.append(clone);
                var clonePos = $B.DomUtils.position(clone);
                ofs.fixTop = clonePos.top - pos.top;
                ofs.fixLeft = -(pos.left - clonePos.left);
                el.parentNode.removeChild(clone);
            }
        }
        return ofs;
    },
    /**
    * 获取旋转的角度
    * matrix = css("transform")
    * **/
    getMatrixAngle: function (matrixArr) {
        var a = matrixArr[0];
        var b = matrixArr[1];
        var angle = Math.round(Math.atan2(b, a) * (180 / Math.PI));
        return angle;
    },
    /***
     * pos 是否在 el元素内
     * pos = {top:,left}
     * el :jq元素
     * ***/
    isInElement: function (pos, el, isRelative) {
        var ofs;
        if (isRelative) {
            ofs = el.position();
        } else {
            ofs = el.offset();
        }
        var w = el.outerWidth();
        var h = el.outerHeight();
        var matrix = $B.getMatrixArray(el);
        if (matrix) { //如果存在缩放               
            var rate = parseFloat(matrix[0]);
            h = h * rate;
            w = w * rate;
        }
        var endTop = ofs.top + h;
        var endLeft = ofs.left + w;
        if (pos.top >= ofs.top && pos.top <= endTop && pos.left >= ofs.left && pos.left <= endLeft) {
            return true;
        }
        return false;
    },
    /****
     * 对str字符串进行html符号替换，防止xss
     * *****/
    htmlEncode: function (str) {
        if (!str || typeof str.replace === "undefined") {
            return str;
        }
        var s = "";
        if (str.length === 0) {
            return "";
        }
        // s = str.replace(/%/g,"%25");
        s = str.replace(/</g, "&lt;");
        s = s.replace(/>/g, "&gt;");
        s = s.replace(/eval\((.*)\)/g, "");
        s = s.replace(/<.*script.*>/, "");
        /*双引号 单引号不替换
        s = s.replace(/\'/g,"&#39;");
        s = s.replace(/\"/g,"&quot;");*/
        return s;
    },
    /*****
     * 将html encode替换的符号，恢复
     * ******/
    htmlDecode: function (str) {
        if (typeof str.replace === "undefined") {
            return str;
        }
        var s = "";
        if (str.length === 0) {
            return "";
        }
        s = str.replace(/&amp;/g, "&");
        s = s.replace(/&lt;/g, "<");
        s = s.replace(/&gt;/g, ">");
        s = s.replace(/&#39;/g, "\'");
        s = s.replace(/&quot;/g, "\"");
        return s;
    },
    /**
     * 动态创建页面style样式
     * className：样式class名称
     * styleText：样式内容
     * ***/
    createHeaderStyle: function (className, styleText) {
        var header = document.getElementsByTagName('head')[0];
        this._appendHeaderStyle(header, className, styleText);
    },
    _appendHeaderStyle: function (header, className, styleText) {
        var element;
        var childs = header.children;
        for (var i = 0; i < childs.length; i++) {
            if (childs[i].className === className) {
                element = childs[i];
                break;
            }
        }
        if (element) {
            header.removeChild(element);
        }
        var style = document.createElement('style');
        style.type = "text/css";
        style.className = className;
        style.innerText = styleText;
        header.appendChild(style);
    },
    /***
     * 获取uuid
     * ***/
    getUUID: function () {
        return this.generateDateUUID();
    },
    getIdIdx: function () {
        if (!window["_$_idx_"]) {
            window["_$_idx_"] = 1;
        } else {
            window["_$_idx_"]++;
        }
        return window["_$_idx_"];
    },
    /**
    * 生成短位数的uuid
    * **/
    getShortID: function () {
        var prex = (new Date()).format("hhmmssSS") + this.getIdIdx();
        var str = this.generateMixed(5);
        return "k_" + prex + str;
    },
    /**
     * 生成uuid
     * fmt：格式yyyyMMddhhmmssSSS,
     * count
     * **/
    generateDateUUID: function (fmt, count) {
        var c = count ? count : 5;
        var formt = fmt ? fmt : "yyMMddhhmmssSS";
        var prex = (new Date()).format(formt) + this.getIdIdx();
        return "k_" + prex + this.generateMixed(c);
    },
    /***产生混合随机数
     *@param n 位数 默认6
     ***/
    generateMixed: function (n) {
        var _n = n ? n : 6;
        var res = [];
        for (var i = 0; i < _n; i++) {
            var id = Math.ceil(Math.random() * 35);
            res.push(CHARSArr[id]);
        }
        return res.join("");
    },
    /**是否是url**/
    isUrl: function (str) {
        if (typeof str !== 'string') {
            return false;
        }
        if (str && str.indexOf("/") < 0) {
            return false;
        }
        return /^((http(s)?|ftp):\/\/)?([\w-]+\.)*[\w-]+(\/[\w-.\/?%&=]*)?(:\d+)?/.test(str);
    },
    /**
     * css样式对象转字符串格式
     * ***/
    cssObj2string: function (cssObj) {
        var res = [];
        var keys = Object.keys(cssObj);
        var key;
        for (var i = 0; i < keys.length; i++) {
            key = keys[i];
            res.push(key + ":" + cssObj[key]);
        }
        return res.join(";");
    },
    /****
     * style字符串转css对象
     * ***/
    style2cssObj: function (style) {
        if (typeof style !== "string") {
            style = $B.DomUtils.attribute(style, "style");
        }
        var res = {};
        if (!style || style === "") {
            return res;
        }
        var tmp = style.split(";");
        var oneArr;
        for (var i = 0; i < tmp.length; i++) {
            if (tmp[i] !== "") {
                oneArr = tmp[i].split(":");
                res[$B.trimFn(oneArr[0])] = $B.trimFn(oneArr[1]);
            }
        }
        return res;
    },
    /***
     * 将参数object对象，转为字符串形式
     * dataObj：{}数据对象
     * isMakeRandom：是否形成一个随机参数
     * encodeUrl：是否进行encodeURIComponent
     * ***/
    formatUrlParams: function (dataObj, isMakeRandom, encodeUrl) {
        var arr = [];
        var rnd, encode;
        rnd = typeof isMakeRandom !== "undefined" ? isMakeRandom : false;
        encode = typeof encodeUrl !== "undefined" ? encodeUrl : true;
        let keys = Object.keys(dataObj);
        for (let i = 0; i < keys.length; i++) {
            if (encode) {
                arr.push(encodeURIComponent(keys[i]) + "=" + encodeURIComponent(dataObj[keys[i]]));
            } else {
                arr.push(encodeURIComponent(keys[i]) + "=" + dataObj[keys[i]]);
            }
        }
        if (rnd) {
            arr.push(("_rmd=" + Math.random()).replace(".", ""));
        }
        return arr.join("&");
    },
    isNotEmpty: function (v) {
        return v !== null && typeof v !== 'undefiend' && v !== "";
    },
    /***
    * 写cookie
    * ***/
    writeCookie: function (name, value, expiredays) {
        if (!this.isNotEmpty(expiredays)) {
            expiredays = 1;
        }
        try {
            var exdate = new Date();
            exdate.setDate(exdate.getDate() + expiredays);
            document.cookie = name + "=" + window.escape(value) + ";expires=" + exdate.toGMTString();
        } catch (ex) {
            console.log("write cookied err ", ex);
        }
    },
    /**
     * 读取cookie
     * ***/
    getCookie: function (c_name) {
        if (document.cookie.length > 0) {
            var c_start = document.cookie.indexOf(c_name + "=");
            if (c_start !== -1) {
                c_start = c_start + c_name.length + 1;
                var c_end = document.cookie.indexOf(";", c_start);
                if (c_end === -1) {
                    c_end = document.cookie.length;
                }
                return window.unescape(document.cookie.substring(c_start, c_end));
            }
        }
        return "";
    },
    /**
     * 循环数组
     * ***/
    foreach: function (arr, fn) {
        for (let i = 0; i < arr.length; i++) {
            fn(arr[i]);
        }
    },
    /***
     * 替换换行，回车
     * ***/
    replaceSpaceChar: function (str) {
        var string = str;
        try {
            string = string.replace(/\r\n/g, "");
            string = string.replace(/\n/g, "");
            string = string.replace(/\s+/g, "");
        } catch (e) {
        }
        return string;
    },
    /**获取滚动条宽度**/
    getScrollWidth: function () {
        var key = '_CURRENT_SCROLL_SIZE_';
        if (typeof window[key] !== "undefined") {
            return window[key];
        }
        var noScroll, scroll, oDiv = document.createElement("DIV");
        oDiv.style.cssText = "position:absolute; top:-1000px; width:100px; height:100px; overflow:hidden;";
        noScroll = document.body.appendChild(oDiv).clientWidth;
        oDiv.style.overflowY = "scroll";
        scroll = oDiv.clientWidth;
        document.body.removeChild(oDiv);
        window[key] = noScroll - scroll;
        return window[key];
    },
    vueForm: function (el, bean, formCtls, opts) {
        if (typeof Vue !== "function") {
            console.log("please import vue!");
            alert("please import vue!");
            return;
        }
        var verifyOnChange = opts.verifyOnChange;
        opts.verifyOnChange = false;
        let elId;
        if (typeof el === "string") {
            elId = el.replace("#", "");
            el = document.getElementById(elId);
        } else {
            elId = el.id;
            if(!elId){
                elId = this.getUUID();
                $B.DomUtils.attribute(el, { id: elId });
            }          
        }
        if (!$B.isPlainObjectFn(opts.verify)) {
            opts.verify = {};
        }
        if (!$B.isPlainObjectFn(bean)) {
            bean = {};
        }
        if (!$B.isPlainObjectFn(formCtls)) {
            formCtls = {};
        }
        let formData = {};
        let watch = {};
        let diyCtlArray = [];
        let diyDataBean = {};
        let lableIdMap = {};
        let mouseTipEls = [];
        this._loopFormEl(el, (el) => {
            if (!$B.DomUtils.attribute(el, "not_bind")) {
                let tagName = el.tagName;
                let id = el.id;
                let formName = id;
                let ctlOpt = formCtls[id];               
                if (tagName === "INPUT") {
                    if (ctlOpt) {//如果是自定义的控件
                        diyCtlArray.push({
                            id: id,
                            ctlOpt: ctlOpt
                        });
                        if (ctlOpt.ctl === "Combox") {
                            formData[id] = [];
                        } else if (ctlOpt.ctl === "Calendar") {
                            formData[id] = "";
                        } else if (ctlOpt.ctl === "switchCtl") {
                            if (typeof bean[id] !== "undefined") {
                                formData[id] = bean[id];
                                el.value = bean[id];
                            } else {
                                if (el.value !== "1") {
                                    el.value = 0;
                                    bean[id] = 0;
                                } else if (typeof bean[id] === "undefined") {
                                    bean[id] = el.value;
                                }
                                formData[id] = bean[id];
                            }
                        } else if (ctlOpt.ctl === "NUMInputCtl") {
                            $B.DomUtils.attribute(el, { "v-model": id });
                            let $pan = $B.Dom.after(el, "<span class='k_fnumber_wrap'></span>");
                            el.parentNode.removeChild(el);
                            $B.Dom.append($pan, el);
                            if (typeof bean[id] !== "undefined") {
                                formData[id] = bean[id];
                                el.value = bean[id];
                            } else {
                                formData[id] = 0;
                                el.value = 0;
                            }
                        }
                        diyDataBean[id] = bean[id];
                        delete bean[id];
                    } else {
                        let type = el.type;
                        if (type === "text" || type === "password" || type === "hidden") {
                            if (type === "hidden" && formName.indexOf("old_") === 0) {
                                let fname = formName.replace("old_", "");
                                el.value = bean[fname] ? bean[fname] : "";
                            }
                            formData[id] = el.value;
                            $B.DomUtils.attribute(el, { "v-model": id });
                        } else if (type === "radio") {
                            if (typeof formData[el.name] === "undefined") {
                                formData[el.name] = el.value;
                            }
                            if (el.checked) {
                                formData[el.name] = el.value;
                            }
                            $B.DomUtils.attribute(el, { "v-model": el.name });
                            formName = el.name;
                            if (!lableIdMap[formName]) {
                                lableIdMap[formName] = [];
                            }
                            lableIdMap[formName].push(el);
                        } else if (type === "checkbox") {
                            if (!formData[el.name]) {
                                formData[el.name] = [];
                                if (bean[el.name] && !Array.isArray(bean[el.name])) {
                                    bean[el.name] = bean[el.name].split(";");
                                }
                            }
                            if (el.checked) {
                                formData[el.name].push(el.value);
                            }
                            $B.DomUtils.attribute(el, { "v-model": el.name });
                            formName = el.name;
                            if (!lableIdMap[formName]) {
                                lableIdMap[formName] = [];
                            }
                            lableIdMap[formName].push(el);
                        }
                    }
                } else if (tagName === "TEXTAREA") {
                    formData[id] = el.innerText ? el.innerText : "";
                    $B.DomUtils.attribute(el, { "v-model": id });
                } else if (tagName === "SELECT") {
                    let child = el.children;
                    formData[id] = "";
                    for (let i = 0; i < child.length; i++) {
                        if (child[i].selected) {
                            formData[id] = child[i].value;
                            break;
                        }
                    }
                    $B.DomUtils.attribute(el, { "v-model": id });
                } else if (tagName === "UL") {//树控件
                    if (ctlOpt) {//如果是自定义的控件
                        if (ctlOpt.ctl === "Tree") {
                            formData[id] = undefined;
                        }
                        diyDataBean[id] = bean[id];
                        delete bean[id];
                        diyCtlArray.push({
                            id: id,
                            ctlOpt: ctlOpt
                        });
                    }
                }
                watch[formName] = this._getWatchFn(formName, opts);
                if($B.Dom.attr(el,"tip_lbl")){                   
                    if(!el.id){
                        el.id = $B.getUUID();
                    }
                    mouseTipEls.push(el.id);
                }
            }
        });
        $B.extendObjectFn(formData, bean);
        let lblKeys = Object.keys(lableIdMap);
        for (let i = 0; i < lblKeys.length; i++) {
            let lblName = lblKeys[i];
            let lsEls = lableIdMap[lblName];
            let itEl = lsEls[lsEls.length - 1];
            let pEl = itEl.parentNode;
            if (pEl.tagName === "LABEL") {
                $B.DomUtils.attribute(pEl, { id: "k_lbl_" + lblName });
            } else {
                $B.DomUtils.attribute(itEl, { id: "k_lbl_" + lblName });
            }
        }        
        let vueObj = new Vue({
            el: "#" + elId,
            data: formData,
            watch: watch,
            mounted: function () {
            },
            updated: function () {
                if (opts.onUpateFn) {
                    setTimeout(() => {
                        opts.onUpateFn.call(this);
                    }, 1);
                }
            }
        });
        var onValueChange = function (name, newVal, oldVal) {
            if ($B.isPlainObjectFn(newVal)) {
                newVal = $B.cloneObjectFn(newVal);
            }
            let skipFieldMvm = "_$skip_" + name;
            vueObj[skipFieldMvm] = name;
            formData[name] = newVal;
            setTimeout(() => {
                delete vueObj[skipFieldMvm];
            }, 1);
        };
        //创建自定义控件,控件必须实现onChange，setValue
        let diyCtlMap = {};
        vueObj["_$diyCtlMap"] = diyCtlMap;
        let formEl = document.getElementById(elId);
        opts.$form = formEl;
        for (let i = 0; i < diyCtlArray.length; i++) {
            let ctlEl = $B.DomUtils.findbyId(formEl, diyCtlArray[i].id);
            let ctlOpt = diyCtlArray[i].ctlOpt;
            let ctlName = ctlOpt.ctl;
            if ($B[ctlName]) {
                let ctlObj;
                ctlOpt.opts.onChange = onValueChange;
                if (ctlName.indexOf("Ctl") > 0) {
                    ctlObj = $B[ctlName](ctlEl, ctlOpt.opts);
                } else {
                    ctlObj = new $B[ctlName](ctlEl, ctlOpt.opts);
                }
                diyCtlMap[diyCtlArray[i].id] = ctlObj;
                //修改值以触发自定义控件的ui绑定
                if (diyDataBean[diyCtlArray[i].id]) {
                    formData[diyCtlArray[i].id] = diyDataBean[diyCtlArray[i].id];
                    delete diyDataBean[diyCtlArray[i].id];
                }
            } else {
                console.log("[" + ctlName + "] 没有找到！");
            }
        }
        formData["toJSON"] = function (array2str) {
            let keys = Object.keys(this);
            let res = {};
            for (let i = 0; i < keys.length; i++) {
                let key = keys[i];
                if (typeof this[key] !== "function" && key !== "$form") {
                    if (Array.isArray(this[key])) {
                        if (array2str) {
                            res[key] = $B._arr2str(this[key]);
                        } else {
                            res[key] = [...this[key]];
                        }
                    } else {
                        res[key] = this[key];
                    }
                }
            }
            return res;
        };
        formData["toJSONStr"] = function (array2str) {
            return JSON.stringify(this.toJSON(array2str));
        };
        formData["toDestroy"] = function () {
            opts.verify = undefined;
            let ctls = Object.values(diyCtlMap);
            for (let i = 0; i < ctls.length; i++) {
                ctls[i].destroy();
            }
            vueObj.$destroy();
            vueObj["_$diyCtlMap"] = undefined;
            formData["toDestroy"] = undefined;
            formData["toJSON"] = undefined;
            formData["toJSONStr"] = undefined;
            formData["toVerify"] = undefined;
            formData["$form"] = undefined;
            opts.$form = undefined;
            formData.__ob__ = undefined;
            formData = undefined;
            diyCtlMap = undefined;
            opts = undefined;
            formEl = undefined;
        };
        formData["toVerify"] = function () {
            if ($B.isEmptyObjectFn(opts.verify)) {
                return true;
            }
            let formKeys = Object.keys(opts.verify);
            let isPast = true;
            for (let i = 0; i < formKeys.length; i++) {
                let formName = formKeys[i];
                let value = formData[formName];
                if (typeof value === "undefined") {
                    value = "";
                }
                let vrfCfg = opts.verify[formName];
                let r = $B._exeVerify(formName, value, vrfCfg, formEl);
                if (!r) {
                    isPast = r;
                }
            }
            return isPast;

        };
        setTimeout(() => {
            opts.verifyOnChange = verifyOnChange;
            //鼠标提示支持
            for(let i =0 ; i < mouseTipEls.length ;i++){
                let id = mouseTipEls[i];
                let el = $B.DomUtils.findbyId(formEl,id);
                $B.mouseTip(el, $B.Dom.attr(el,"tip_lbl"), undefined, "#9090C9");
            }
        }, 20);
        this.bindInputClear(formEl, (elName) => {
            if (formData[elName]) {
                formData[elName] = "";
                return true;
            }
            console.log("formData is undefined！");
            return false;
        });
        formData.$form = formEl;      
        return formData;
    },
    /**
 * 创建开关控件
 * @param  el
 * @param  opts {
            trueText:"是",
            falseText:"否",                    
            onChange:(value)=>{
                console.log(value);
            }
        }
    */
    switchCtl: function (el, opts) {
        let id;
        if (typeof el === "string") {
            id = el.replace("#", "");
            el = document.getElementById(id);
        } else {
            let id = $B.DomUtils.attribute(el, "id");
            if (!id) {
                id = "sw_" + $B.getShortID();
                $B.DomUtils.attribute(el, { "id": id });
            }
        }
        let chkCls = "k_switch_core_checked";
        if (el.value === "" || el.value !== "1") {
            el.value = "0";
            chkCls = "";
        }
        let $wrap = $B.DomUtils.createEl("<span class='k_switch_wrap'><span class='k_switch_left_label'></span><span class='k_switch_core " + chkCls + "'></span><span class='k_switch_right_label'></span></span>");
        if (opts.trueText) {
            $wrap.firstChild.innerText = opts.trueText;
            $B.DomUtils.addClass($wrap.firstChild, "k_switch_l_padding");
        }
        if (opts.falseText) {
            $wrap.lastChild.innerText = opts.falseText;
            $B.DomUtils.addClass($wrap.lastChild, "k_switch_r_padding");
        }
        if (chkCls === "") {
            $B.DomUtils.addClass($wrap.lastChild, "k_switch_label_checked");
        } else {
            $B.DomUtils.addClass($wrap.firstChild, "k_switch_label_checked");
        }
        $B.DomUtils.after(el, $wrap);
        $B.DomUtils.remove(el);
        $B.DomUtils.append($wrap, el);
        $B.DomUtils.click($wrap, (e) => {
            let $el = e.target;
            while ($el) {
                if ($B.DomUtils.hasClass($el, "k_switch_wrap")) {
                    break;
                }
                $el = $el.parentNode;
            }
            let childs = $B.DomUtils.children($el, "span");
            let oldClazz = [];
            for (let i = 0; i < childs.length; i++) {
                oldClazz.push($B.DomUtils.attribute(childs[i], "class"));
            }
            let $c = $B.DomUtils.children($el, ".k_switch_core")[0];
            let oldVal = $el.lastChild.value;
            if ($B.DomUtils.hasClass($c, "k_switch_core_checked")) {
                $B.DomUtils.removeClass($c, "k_switch_core_checked");
                $el.lastChild.value = "0";
                $B.DomUtils.removeClass($c.previousSibling, "k_switch_label_checked");
                $B.DomUtils.addClass($c.nextSibling, "k_switch_label_checked");
            } else {
                $B.DomUtils.addClass($c, "k_switch_core_checked");
                $el.lastChild.value = "1";
                $B.DomUtils.addClass($c.previousSibling, "k_switch_label_checked");
                $B.DomUtils.removeClass($c.nextSibling, "k_switch_label_checked");
            }
            if (opts.onChange) {
                opts.onChange.call($el, $el.lastChild.id, $el.lastChild.value, oldVal, oldClazz);
            }
        });
        if (!opts.notReturn) {
            return {
                setValue: function (val, triggerChange) {
                    let isChecked = true;
                    if (typeof val === "boolean") {
                        isChecked = val;
                    } else if (typeof val === "number") {
                        isChecked = val === 1;
                    } else {
                        isChecked = val === "1";
                    }
                    let $c = $B.DomUtils.children($wrap, ".k_switch_core")[0];
                    if (isChecked) {
                        $B.DomUtils.addClass($c, "k_switch_core_checked");
                        $wrap.lastChild.value = "1";
                        $B.DomUtils.addClass($c.previousSibling, "k_switch_label_checked");
                        $B.DomUtils.removeClass($c.nextSibling, "k_switch_label_checked");
                    } else {
                        $B.DomUtils.removeClass($c, "k_switch_core_checked");
                        $wrap.lastChild.value = "0";
                        $B.DomUtils.removeClass($c.previousSibling, "k_switch_label_checked");
                        $B.DomUtils.addClass($c.nextSibling, "k_switch_label_checked");
                    }
                },
                destroy: function () {
                    $B.DomUtils.remove($wrap);
                    opts.onChange = undefined;
                    opts = undefined;
                    this.setValue = undefined;
                    this.destroy = undefined;
                }
            };
        }
    },
    NUMInputCtl: function (el, opts) {       
        if (!$B.Dom.hasClass(el, "k_number_input")) {
            if (!$B.Dom.hasClass(el.parentNode, "k_fnumber_wrap")) {
                let $span = $B.Dom.after(el, "<span class='k_fnumber_wrap'></span>");
                el.parentNode.removeChild(el);
                $B.Dom.append($span, el);
            }
            $B.Dom.addClass(el, "k_number_input");
            let $prt = el.parentNode;
            let btn = $B.Dom.append($prt, "<span style='position:absolute;top:0px;right:2px;display:inline-block;line-height: 12px;' class='up-dir'><i class='fa fa-up-dir'></i></span>");
            let btn1 = $B.Dom.append($prt, "<span style='position:absolute;bottom:0px;right:2px;display:inline-block;line-height: 12px;' class='down-dir'><i class='fa fa-down-dir'></i></span>");
            let clickFN = (e) => {
                let step = 1;
                if (opts.step) {
                    step = parseFloat(opts.step);
                }
                let catpurer = e.catpurer;
                let oldVal = $B.trimFn(el.value);
                if ($B.config.verify.number.reg.test(oldVal)) {
                    oldVal = parseFloat(oldVal);
                } else {
                    oldVal = 0;
                }
                let newVal;
                if ($B.Dom.hasClass(catpurer, "up-dir")) {
                    newVal = oldVal.add(step);
                } else {
                    newVal = oldVal.sub(step);
                }
                if(typeof opts.min !== "undefined"){
                    if(newVal < opts.min){
                        return false;
                    }
                }
                if(typeof opts.max !== "undefined"){
                    if(newVal > opts.max){
                        return false;
                    }
                }
                el.value = newVal;               
                opts.onChange(el.id, newVal, oldVal);                
            };
            $B.Dom.click(btn, clickFN);
            $B.Dom.click(btn1, clickFN);
        }

    },
    _getWatchFn: function (formName, opts) {
        return function (newVal, oldVal) {
            let diyCtlMap = this._$diyCtlMap;
            let diyCtlINS = diyCtlMap[formName];
            if (diyCtlINS) {
                let skipFieldMvm = "_$skip_" + formName;
                if (!this[skipFieldMvm]) {
                    if (diyCtlINS.setValue) {
                        let argsVal = newVal;
                        if (Array.isArray(newVal) || $B.isPlainObjectFn(newVal)) {
                            argsVal = $B.cloneObjectFn(newVal);
                        }
                        diyCtlINS.setValue(argsVal, false);
                    } else {
                        console.log("the ctl not impl setValue!");
                    }
                }
            }
            //值发生变化，可触发验证
            let isError = false;
            if (opts.verifyOnChange) {
                let verifyArgs = opts.verify[formName];
                if (verifyArgs) {
                    isError = !$B._exeVerify(formName, newVal, verifyArgs, opts.$form);
                }
            }
            if (opts.onChange) {
                opts.onChange(formName, newVal, oldVal,isError);
            }
        };
    },
    /**
     * 验证一个表单项目
     * ***/
    _exeVerify: function (formName, newVal, verifyArgs, $form) {
        //console.log("exe verify ",newVal,formName,verifyArgs);
        let ret = [], res;
        if (Array.isArray(verifyArgs)) {
            for (let i = 0; i < verifyArgs.length; i++) {
                if (typeof verifyArgs[i] === "function") {
                    res = verifyArgs[i](formName, newVal, $form);
                } else {
                    res = $B._verify(verifyArgs[i], newVal);
                }
                if (res) {
                    ret.push(res);
                }
            }
        } else {
            if (typeof verifyArgs === "function") {
                res = verifyArgs(formName, newVal, $form);
            } else {
                res = $B._verify(verifyArgs, newVal);
            }
            if (res) {
                ret.push(res);
            }
        }
        let el = $B.DomUtils.findbyId($form, formName);
        if (!el) {
            el = $B.DomUtils.findbyId($form, "k_lbl_" + formName);
        }
        if (el) {
            let parentEl = el.parentNode;  
            if($B.Dom.css(parentEl,"display") === "inline-block" || $B.Dom.hasClass(parentEl.parentNode,"k_form_it_wrap")){
                parentEl = parentEl.parentNode;
            }
            let lplPrt = parentEl;
            let $row,$col,$item;
            let i = 0 ;
            while(i < 10 && lplPrt){
                if(lplPrt.nodeName === "BODY"){
                    break;
                }
                if($B.Dom.hasClass(lplPrt,"k_flow_form_item")){
                    $item = lplPrt;
                }
                if($B.Dom.hasClass(lplPrt,"k_flex_form_col")){
                    $col = lplPrt;
                }
                if($B.Dom.hasClass(lplPrt,"k_flex_form_row")){
                    $row = lplPrt;
                    break;
                }
                lplPrt = lplPrt.parentNode;
                i++;
            }           
            let errEl = $B.DomUtils.children(parentEl, ".k_verify_err_el");
            if (errEl.length == 0) {
                errEl = $B.DomUtils.append(parentEl, "<div style='display:none;position:relative;' class='k_verify_err_el'></div>");
            } else {
                errEl = errEl[0];
            }
            if (ret.length > 0) {
                let display = "inline-block";
                ret = $B.uniqueArray(ret);
                let errMsg = ret.join("; ");
                let len = $B.getCharWidth(errMsg) + 20;
                errEl.style.display = "none";
                errEl.style.width = len +"px";
                 //计算合适的位置
                if($row){ //flex布局表单                   
                    if(!$col.nextSibling && !$item.nextSibling){
                        let rwidth = $B.Dom.width($row);
                        let preWidth = 0;
                        while($col){
                            preWidth = preWidth + $B.Dom.outerWidth($col);
                            $col = $col.previousSibling;
                        }                     
                        let alvWidth = rwidth - preWidth;
                        if(alvWidth < len){
                            display = "block";
                            errEl.style.width = (alvWidth - 2) + "px";
                        }
                    }else{
                        let colWidth = $B.Dom.width($item) -2;
                        errEl.style.width = colWidth + "px";
                        display = "block";
                    }
                }           
                if ($B.DomUtils.hasClass(el, "k_combox_wrap")) {
                    $B.DomUtils.addClass(el.firstChild, "k_input_value_err");
                } else {
                    $B.DomUtils.addClass(el, "k_input_value_err");
                }
                errEl.innerText = errMsg;
                if(display === "inline-block"){
                    errEl.style.paddingLeft = "2px"; 
                }else{
                    errEl.style.paddingLeft = "0px"; 
                }
                errEl.style.display = display;
            } else {
                if ($B.DomUtils.hasClass(el, "k_combox_wrap")) {
                    $B.DomUtils.removeClass(el.firstChild, "k_input_value_err");
                } else {
                    $B.DomUtils.removeClass(el, "k_input_value_err");
                }
                errEl.style.display = "none";
            }
        }
        return ret.length === 0;
    },
    _verify: function (rule, val) {
        if ($B.config) {
            if (typeof rule === "string") {//内置的规则
                return $B._defaultVerify(val, rule);
            } else if ($B.isPlainObjectFn(rule)) {
                if (rule.rule) {//内置的规则
                    let ruleName = rule.rule;
                    let label = rule.label;
                    return $B._defaultVerify(val, ruleName, label, rule);
                } else if (rule.reg) {//自定义正则表达式                    
                    if (val !== "" && !rule.reg.test(val)) {
                        let label = $B.config.verify.regLabel;
                        if (rule.label) {
                            label = rule.label;
                        }
                        return label;
                    }
                }
            }
        }
    },
    _defaultVerify: function (val, ruleName, label, rule) {
        let vcfg = $B.config.verify;
        let cfg = vcfg[ruleName];
        if (cfg) {
            if (!label) {
                label = cfg.label;
            }
            if ("remote" === ruleName) {//远程验证
                console.log("远程验证 尚未实现！");
            } else if ("require" === ruleName) {
                if (Array.isArray(val)) {
                    if (val.length === 0) {
                        return label;
                    }
                } else {
                    val = $B.trimFn(val);
                    if (val === "") {
                        return label;
                    }
                }
            } else if ("range" === ruleName) {
                if (val !== "") {
                    if (!vcfg.number.reg.test(val)) {
                        return vcfg.number.label;
                    }
                    let min = rule.min;
                    let max = rule.max;
                    let number = parseFloat(val);
                    if (number < min || number > max) {
                        return label.replace("{1}", min).replace("{2}", max);
                    }
                }
            } else if ("minlength" === ruleName) {
                if (val !== "" && val.length < rule.len) {
                    return label.replace("{1}", rule.len);
                }
            } else if ("maxlength" === ruleName) {
                if (val !== "" && val.length > rule.len) {
                    return label.replace("{1}", rule.len);
                }
            } else {
                if (val !== "" && !cfg.reg.test(val)) {
                    return label;
                }
            }
        }
    },
    _arr2str: function (arr) {
        let tmp = [];
        for (let i = 0; i < arr.length; i++) {
            if (Array.isArray(arr[i])) {
                tmp.push(this._arr2str(arr[i]));
            } else {
                tmp.push(arr[i]);
            }
        }
        return tmp.join(";");
    },
    _loopFormEl: function (el, onLoopFn) {
        onLoopFn(el);
        var children = el.children;
        for (let i = 0; i < children.length; i++) {
            let child = children[i];
            this._loopFormEl(child, onLoopFn);
        }
    },
    uniqueArray: function (arr) {
        return Array.from(new Set(arr));
    },
    /**
     * 生成16位的对称加密key
     * **/
    AESGenKey: function (length = 16) {
        let random = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        let str = "";
        for (let i = 0; i < length; i++) {
            str = str + random.charAt(Math.random() * random.length)
        }
        return str;
    },
    /**
     * des加密 依赖于encrypt-min.js
     * ***/
    AESEncrypt: function (plaintext, key) {
        if (typeof window["CryptoJS"] === "undefined") {
            console.log("没有加载 》》 CryptoJS");
            return message;
        }
        if (plaintext instanceof Object) {
            plaintext = JSON.stringify(plaintext)
        }
        let encrypted = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(plaintext),
            CryptoJS.enc.Utf8.parse(key),
            { mode: CryptoJS.mode.ECB, padding: CryptoJS.pad.Pkcs7 });
        return encrypted.toString();
    },
    /**
     * des解密
     * **/
    AESDecrypt: function (ciphertext, key) {
        if (typeof window["CryptoJS"] === "undefined") {
            console.log("没有加载 》》 CryptoJS");
            return message;
        }
        let decrypt = CryptoJS.AES.decrypt(ciphertext, CryptoJS.enc.Utf8.parse(key),
            { mode: CryptoJS.mode.ECB, padding: CryptoJS.pad.Pkcs7 });
        let decString = CryptoJS.enc.Utf8.stringify(decrypt).toString();
        if (decString.charAt(0) === "{" || decString.charAt(0) === "[") {
            decString = JSON.parse(decString);
        }
        return decString;
    },
    //RSA 位数，这里要跟后端对应
    RSAGenKeys: function (bits = 1024) {
        if (typeof window["JSEncrypt"] === "undefined") {
            console.log("没有加载 JSEncrypt");
            return message;
        }
        let genKeyPair = {};
        if (!this.genKeyPairGener) {
            let genKeyPairGen = new JSEncrypt({ default_key_size: bits });
            this.genKeyPairGener = genKeyPairGen;
        }
        //获取私钥
        genKeyPair.privateKey = this.genKeyPairGener.getPrivateKey();//.replace("----BEGIN RSA PRIVATE KEY-----\n","").replace("\n-----END RSA PRIVATE KEY-----","");
        //获取公钥
        genKeyPair.publicKey = this.genKeyPairGener.getPublicKey();//.replace("-----BEGIN PUBLIC KEY-----\n","").replace("\n-----END PUBLIC KEY-----","");       
        return genKeyPair;
    },
    //公钥加密
    RSAEncrypt: function (plaintext, publicKey) {
        if (!this.genKeyPairGener) {
            alert("please invoke RSAGenKeys !");
            return;
        }
        if (plaintext instanceof Object) {
            plaintext = JSON.stringify(plaintext)
        }
        publicKey && this.genKeyPairGener.setPublicKey(publicKey);
        return this.genKeyPairGener.encryptLong(plaintext);
    },
    //私钥解密
    RSADecrypt: function (ciphertext, privateKey) {
        if (!this.genKeyPairGener) {
            alert("please invoke RSAGenKeys !");
            return;
        }
        privateKey && this.genKeyPairGener.setPrivateKey(privateKey);
        let decString = this.genKeyPairGener.decryptLong(ciphertext);
        if (decString.charAt(0) === "{" || decString.charAt(0) === "[") {
            decString = JSON.parse(decString);
        }
        return decString;
    },
    bindInputClear: function (form, onClearFn) {
        if (typeof form === "string") {
            form = document.getElementById(form.replace("#", ""));
        }
        let inputs = $B.DomUtils.findByTagName(form, "input[type=text]");
        let pws = $B.DomUtils.findByTagName(form, "input[type=password]");
        let textArea = $B.DomUtils.findByTagName(form, "textarea");
        if (!window["_inputClearEvs"]) {
            window["_inputClearEvs"] = {
                mouseenter: function (e) {
                    let $clearBtn = $B.DomUtils.findbyId(document.body, "#k_global_clear_btn");
                    if (!$clearBtn) {
                        $clearBtn = $B.DomUtils.createEl("<div id='k_global_clear_btn' style='position:absolute;display:none;z-index:2147483647;width:8px;height:14px;cursor:pointer;'><i style='color:#ccc;' class='fa  fa-cancel-1'></i></div>");
                        $B.DomUtils.append(document.body, $clearBtn);
                        $B.DomUtils.click($clearBtn, function () {
                            let el = this._el;
                            this.style.display = "none";
                            if (el) {
                                el.value = "";
                                this._el = undefined;
                                if (el.onClearFn) {
                                    if (!el.onClearFn(el.id)) {
                                        el.onClearFn = undefined;
                                    }
                                }
                                el.focus();
                            }
                        });
                    }
                    if (this.value !== "") {
                        let ofs = $B.DomUtils.offset(this);
                        let w = $B.DomUtils.outerWidth(this);
                        let h = $B.DomUtils.outerHeight(this);
                        ofs.top = ofs.top + (h - 20) / 2;
                        ofs.left = ofs.left + w - 10;
                        $B.DomUtils.css($clearBtn, ofs);
                        $clearBtn.style.display = "block";
                        $clearBtn._el = this;
                    }
                },
                mouseleave: function (e) {
                    let x = e.pageX;
                    let y = e.pageY;
                    let min = $B.DomUtils.offset(this);
                    let w = $B.DomUtils.outerWidth(this);
                    let h = $B.DomUtils.outerHeight(this);
                    let max = {
                        top: min.top + h,
                        left: min.left + w
                    };
                    if (!(x > min.left && x < max.left && y > min.top && y < max.top)) {
                        let $clearBtn = $B.DomUtils.findbyId(document.body, "#k_global_clear_btn");
                        if ($clearBtn) {
                            $clearBtn._el = undefined;
                            $clearBtn.style.display = "none";
                        }
                    }
                }
            };
        }
        Array.prototype.push.apply(inputs, pws);
        Array.prototype.push.apply(inputs, textArea);
        for (let i = 0, len = inputs.length; i < len; i++) {
            if (!$B.DomUtils.attribute(inputs[i], "readonly") && !$B.DomUtils.attribute(inputs[i], "disabled")) {
                let $in = inputs[i];
                if(!$B.Dom.getData($in,"clear_ev")){
                    let canBind = !$B.DomUtils.hasClass($in, "k_calendar_input") && !$B.DomUtils.hasClass($in, "k_combox_input")
                    &&  !$B.DomUtils.hasClass($in, "k_number_input");
                    if (canBind) {
                        $in.onClearFn = onClearFn;
                        $B.DomUtils.addClass($in, "k_box_size");
                        $B.DomUtils.css($in, { "padding-right": "10px" });
                        $B.DomUtils.bind($in, window["_inputClearEvs"]);
                        $B.DomUtils.setData($in,  "clear_ev", true );
                    }
                }             
            }
        }
    },
    /**
     * $wap: iframe容器
     * htmlEl: iframe内部的html
     * style:写入的样式
     * ***/
    createTextIfr: function ($wap, htmlEl, style, ifrCss) {
        let $ifr = $B.DomUtils.createEl('<iframe class="text_ifr" frameborder="0" style="width:100%;height:100%;" scrolling="no"></iframe>');
        if (ifrCss) {
            $B.DomUtils.css($ifr, ifrCss);
        }
        $B.DomUtils.append($wap, $ifr);
        var $body = $ifr.contentWindow.document.body;
        var $head = $ifr.contentWindow.document.head;
        $B.DomUtils.append($body, htmlEl);

        this._appendHeaderStyle($head, "base", "*{padding: 0;margin: 0;font-size: 14px;line-height: 1.5em;font-family: 'Microsoft Yahei', Helvetica, Arial, sans-serif;color: #1A242C;}"
            + "html,body{width:100%;height:100%}  *:focus {outline: none} textarea{padding: 2px 2px;text-align: left;border: 1px solid #ccc;} button{background: none;white-space: nowrap;border: 1px solid rgb(196, 224, 255);border-radius: 2px;padding: 1px 3px;line-height: 1.5em;cursor: pointer;font-size:13px;}"
            + "input::-webkit-input-placeholder，input::-moz-placeholder，input:-moz-placeholder，input:-ms-input-placeholder{color:#A9A9A9;}"
            + "input[type=text]:focus , input[type=password]:focus, textarea:focus{border-color: #409eff;} table{border-collapse: separate;border-spacing: 0;table-layout: fixed;} table td{padding:5px 2px;}"
            + "input.k_input_value_err,textarea.k_input_value_err,select.k_input_value_err{border:1px dashed #FF6E00 !important;}"
            + ".clearfix {*zoom:1;}.clearfix:before,.clearfix:after{display:table;line-height:0;content:'';} .clearfix:after{clear:both;}::-webkit-scrollbar{width:8px;height:8px;}::-webkit-scrollbar-button,::-webkit-scrollbar-button:vertical{display:none;}"
            + "::-webkit-scrollbar-track,::-webkit-scrollbar-track:vertical{box-shadow:inset006pxtransparent;background-color:transparent;}::-webkit-scrollbar-thumb,::-webkit-scrollbar-thumb:vertical{box-shadow:inset006pxrgba(100,173,250,.5);border-radius:6px;}"
            + "::-webkit-scrollbar-thumb:hover,::-webkit-scrollbar-thumb:vertical:hover{box-shadow:inset006pxrgba(100,173,250,.8);background-color:#74B9FF;}::-webkit-scrollbar-corner,::-webkit-scrollbar-corner:vertical{background-color:none;}"
            + "::-webkit-scrollbar-resizer,::-webkit-scrollbar-resizer:vertical{background-color:#ff6e00;}.ifr_inner_delbtn{position:relative;top:-5px;left:5px;}.ifr_inner_delbtn:before{content:'x';font-weight: bold;}"
            + 'select{border: solid 1px #ccc; appearance:none;-moz-appearance:none;-webkit-appearance: none;padding: 2px 12px 2px 2px;background: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAALdJREFUOE/dk8EKwjAMhtP6DLv4CL5ECQXp3aMv071O9QmEEVp28Cp49w28yigLUrAyddvB3pZbIP/f5ksioDBEoR6WZEBExMxea23nuDRNY6WUChEx1b0ZhBA2fd87Zj5OmbzEWynlXil1+zBISdu26xjjacwkv9x13c4Yc8+//JmCc25VVdUFAA6IWKfCLPbea2stD1ucHCMRXZMJM4thz998ZveAiM4A8MjAxuAuaZH+PapiBk8qN0kR6TiUfAAAAABJRU5ErkJggg==") no-repeat scroll right center transparent;}select::-ms-expand {display: none;}'
            + "input[type=text],input[type=password] {-webkit-appearance: textfield;background-color: white;-webkit-rtl-ordering: logical;user-select: text;cursor: auto;padding: 3px 2px;outline: none;border: 1px solid #ccc;line-height: 18px;box-sizing: border-box;font-size:13px;}");
        if (style) {
            let userStyle = style;
            if (Array.isArray(style)) {
                userStyle = style.join("");
            }
            this._appendHeaderStyle($head, "userstyle", userStyle);
        }
    },
    clearDomSelected: function () {
        try {
            if (document.selection) {
                document.selection.empty();
            } else if (window.getSelection) {
                window.getSelection().removeAllRanges();
            }
        } catch (r) { }
    },
    getInputVal: function (el, defVal) {
        let v = $B.trimFn(el.value);
        if (v === "" && typeof defVal !== "undefined") {
            return defVal
        }
        return v;
    },
    setSelectValue: function (el, val) {
        val = val + "";
        let childs = el.children;
        for (let i = 0; i < childs.length; i++) {
            if (childs[i].value === val) {
                $B.Dom.attr(childs[i], { selected: "selected" });
            } else {
                $B.Dom.removeAttr(childs[i], "selected");
            }
        }
    },
    /**
     * 按大写分隔字符，
     * splitStr：分隔符，空则返回数组
     * ***/
    splitByUper:function(txt,splitStr){
        let res;
        if(splitStr){
            res = txt.replace(/([A-Z])/g, splitStr+'$1').toLowerCase();
        }else{
            res = txt.split(/(?=[A-Z])/);
            for(let i =0 ;i < res.length;i++){
                res[i] = res[i].toLowerCase();
            }
        }
        return res;
    },
    str2camal:function(txt,splitStr){
        if(!splitStr){
            splitStr = "-";
        }
        let arr = txt.split(splitStr);
        for(let i = 0 ;i < arr.length ;i++){
            arr[i] = arr[i].toLowerCase();
            if(i > 0){
                arr[i] = arr[i].replace(/( |^)[a-z]/g,(L)=>L.toUpperCase());
            }
        }
        return arr.join("");
    }
});
/******定义一个基类
 * 封装基本的删除，情况，设置控件element的api
 * ******/
class BaseControl {
    constructor() {
        //console.log("BaseControl has bean call");
        this.id = $B.getUUID();
    }
    setElObj(args) {
        if (typeof args === "string") {
            args = args.replace("#", "");
            this.elObj = document.getElementById(args);
            this.id = args;
        } else {
            this.elObj = args;
            let id = $B.DomUtils.attribute(this.elObj, "id");
            if (id) {
                this.id = id;
            } else {
                this.id = $B.getUUID();
                $B.DomUtils.attribute(this.elObj, { "id": this.id });
            }
        }
        // if (!this.id) {
        //     this.id = $B.getUUID();
        // }
    }
    clearDom() {
        if (this.elObj) {
            $B.DomUtils.removeChilds(this.elObj);
        }
    }
    delProps(excuObjName) {
        for (var p in this) {
            if (this.hasOwnProperty(p)) {
                if (this[p] !== null && this[p] !== undefined) {
                    if (p !== "super" && typeof (this[p].destroy) === "function") {
                        if (!excuObjName || excuObjName !== this[p]) {
                            this[p].destroy(this);
                        }
                    }
                }
                delete this[p];
            }
        }
    }
    destroy(excuObjName) {
        if (this.elObj) {
            $B.DomUtils.remove(this.elObj);
            this.elObj = undefined;
        }
        this.delProps(excuObjName);
    }
    clearProps() {
        for (var p in this) {
            if (this.hasOwnProperty(p)) {
                delete this[p];
            }
        }
    }
    clear() {
        this.clearDom();
        this.delProps();
    }
}
$B["BaseControl"] = BaseControl;
//设置一个用于a标签下载指向的iframe
var count = 0;
var itv001 = setTimeout(function () {
    let $body = document.body;
    if ($body) {
        clearInterval(itv001);
        if (!$B.Dom.findbyId($body, "k_down_load_ifr")) {
            let isLoadding = false;
            let ifr = $B.Dom.createEl('<iframe  name="k_down_load_ifr" id="k_down_load_ifr" frameborder="0" style="display:none;vertical-align:top;" scroll="none" width="0" height="0" ></iframe>');
            $B.Dom.append($body, ifr);
            $B.DomUtils.onload(ifr, () => {
                if (isLoadding) {
                    $B.error($B.config.file404, 2);
                }
            });
            setTimeout(function () {
                isLoadding = true;
            }, 300);
        }
    }
    count++;
    if (count > 20) {
        clearInterval(itv001);
    }
}, 300); 
 if(!window["$B"] ) {window["$B"] = $B;} 
  return window["$B"] ; 
});


/*! BUI - v2.0.0 - 2022-03-13 09:03:17 
Copyright (c): kevin.huang  https://gitee.com/kevin-huang/bui2.0.git (qq:757723114) 
Released under MIT License*/
(function ( global, factory) {
//   "use strict";
	if ( typeof module === "object" && typeof module.exports === "object" ) {
		module.exports = global.document ?
			factory( global, true ) :
			function( w ) {
				if ( !w.document ) {
					throw new Error( "Bui requires a window with a document" );
				}
				return factory( w );
			};
	} else {
		  factory( global );
	}  
})( typeof window !== "undefined" ? window : this, function( window, noGlobal ) {
//   "use strict";
  var $B = window["$B"] ?  window["$B"] : {};

var defaultOpts = {
    heightStyle: 'auto',
    iconCls: 'fa-angle-double-right', //收起图标   
    iconPositon: 'right', //图标位置
    iconColor: '#666666', //图标颜色
    fontStyle: {
        "font-size": "14px",
        "font-weight": "bold",
        "color": "#666666"
    }, //标题字体颜色、大小配置
    activedStyle: {
        "background": "#71B9EA",
        "color": "#FFFFFF",
        "iconColor": '#FFFFFF'
    }, //激活状态样式            
    accordionStyle: {
        "background": "#F6F6F6",
        "border": "1px solid #C5C5C5"
    }, //手风琴边框、颜色定义
};
function _createItme(item, i) {
    var it = {
        header: $B.DomUtils.createEl("<h6 index='" + i + "' title='" + item.title + "' style='overflow:hidden' class='k_accordion_header k_box_size'><span>" + item.title + "</span></h6>"),
        body: $B.DomUtils.createEl("<div index='" + i + "' style='display:none' class='k_accordion_body k_box_size'><div style='height:100%;width:100%;padding:6px 8px;' class='k_box_size'></div></div>")
    };
    var elAttr={};
    var $bel = it.body.firstChild;
    if (item.url && item.url !== "") {        
        elAttr["_url"] = item.url;
        elAttr["_type"] = item.type ? item.type : 'iframe';
        $B.DomUtils.attribute($bel, elAttr);
        if(elAttr._type !== 'iframe'){
            $B.myScrollbar($bel, {});
        }else{
            let ifrEL = $B.getIframeEl("k_tabs_content_ifr");           
            $B.DomUtils.append($bel, ifrEL);
            $B.DomUtils.css($bel,{position:"relative"});
        }
    } else if (item.content) {
        let $e = $B.myScrollbar($bel, {});
        $B.DomUtils.append($e, item.content);
    }
    if (item.icon && item.icon !== "") {
        $B.DomUtils.prepend(it.header, "<i style='padding-right:4px' class='fa " + item.icon + "'></i>");
    }
    return it;
}
class Accordion extends $B.BaseControl {
    constructor(elObj, opts) {
        super();
        super.setElObj(elObj);
        this.opts = $B.extendObjectFn(true, {}, defaultOpts, opts);
        $B.DomUtils.addClass(this.elObj, "k_box_size k_accordion_main_wrap");
        this.hideProps = {
            "borderTopWidth": "hide",
            "borderBottomWidth": "hide",
            "paddingTop": "hide",
            "paddingBottom": "hide",
            "height": "hide"
        };
        this.showProps = {
            "borderTopWidth": "show",
            "borderBottomWidth": "show",
            "paddingTop": "show",
            "paddingBottom": "show",
            "height": "show"
        };
        let fragment = document.createDocumentFragment();
        let activedItem;
        let ml = "14";
        if (this.opts.iconPositon === "left") {
            ml = "0";
        }
        for (let i = 0, len = this.opts.items.length; i < len; ++i) {
            let item = this.opts.items[i];
            let it = _createItme(item, i);
            $B.DomUtils.css(it.header, this.opts.fontStyle);
            $B.DomUtils.css(it.header, this.opts.accordionStyle);
            $B.DomUtils.css(it.body, { "border": this.opts.accordionStyle.border });
            $B.DomUtils.css(it.body, { "border-top": "none" });
            $B.DomUtils.attribute(it.body, { "_title": item.title });
            if (this.opts.iconCls && this.opts.iconCls !== "") {
                let $icon = $B.DomUtils.createEl("<div style='height:100%;margin-left:" + ml + "px;margin-right:12px;float:" + this.opts.iconPositon + ";' class='k_accordion_header_icon'><i style='line-height:1.8em;' class='fa " + this.opts.iconCls + "'></i></div>");
                $B.DomUtils.append(it.header, $icon);
                $B.DomUtils.css(it.header, { color: this.opts.iconColor });
            }
            fragment.appendChild(it.header);
            fragment.appendChild(it.body);
            if (item.actived || i === 0) {
                activedItem = it.header;
            }
            this.bindEvents(it.header);
            if(this.opts.onCreate){
                let $b;
                if($B.DomUtils.attribute(it.body.firstChild,"_url") && $B.DomUtils.attribute(it.body.firstChild,"_type") === "iframe"){
                    $b = it.body.firstChild;
                }else{
                    $b = it.body.firstChild.firstChild;
                }
                setTimeout(()=>{
                    this.opts.onCreate.call($b,item.title);
                },1);
            }
        }
        this.elObj.appendChild(fragment);
        if(activedItem){
            $B.DomUtils.trigger(activedItem,"click");
        }
    }
    bindEvents($header){
        if(!this.onClick){
            this.onClick=(e)=>{
                let el = e.target;
                if(el.tagName !== "H6"){
                    while(el){
                        el = el.parentNode;
                        if(el.tagName === "H6"){
                            break;
                        }
                    }
                }
                if(this.activedItem ){
                    if(this.activedItem === el){
                        return false;
                    }
                    this._retoreStyle(this.activedItem);
                }
                this.activedItem = el;
                this._setActivedStyle();
                let childNodes = this.elObj.children;
                let allHeaderHeight = 0;
                let curShowEL;
                for(let i = 0 ;i < childNodes.length ;i++){
                    let $l = childNodes[i];
                    if($l.tagName === "H6"){
                        allHeaderHeight = allHeaderHeight + $B.DomUtils.outerHeight($l) + 2;
                    }else if($l.style.display !== "none"){
                        curShowEL = $l;
                    }
                }
                let allHeight = $B.DomUtils.outerHeight(this.elObj);
                let accHeight = allHeight - allHeaderHeight;                
                el.nextSibling.style.height = accHeight +"px";
                if(curShowEL){                    
                    $B.slideUp(curShowEL,200);                    
                }
                let $body = el.nextSibling;
                let title = $B.DomUtils.attribute($body,"_title");
                $B.slideDown($body,200,()=>{
                    let _s = $body.firstChild.firstChild;
                    $B.DomUtils.trigger(_s,"myscrollabr.mouseleave");
                    if($B.DomUtils.hasClass(_s,"k_scrollbar_wrap")){
                        _s = _s.firstChild;
                    }                   
                   this._exeLoad($body,title);
                   if(this.opts.onOpened){
                        setTimeout(()=>{                          
                            this.opts.onOpened.call(_s,title);
                        },1);
                   }
                });
            };
        }
        $B.DomUtils.click($header,this.onClick);
    }
    _exeLoad($body,title){
        let $wrap = $body.firstChild;
        let url = $B.DomUtils.attribute($wrap,"_url");        
        if(url){            
            let _this = this;
            let isFun = typeof   this.opts.onLoaded === "function";          
            let loading = $B.getLoadingEl();
            let type = $B.DomUtils.attribute($wrap,"_type") ;         
            if(type === "html"){
                let $c = $wrap.firstChild.firstChild;
                if(!$c.firstChild){
                    $c.appendChild(loading);
                    $B.htmlLoad({
                        url: url,
                        success: function (res) {
                            $c.innerHTML = res;
                            if (isFun) {
                                _this.opts.onLoaded.call($c, {},title,'html');
                            }
                            $B.bindInputClear($c);
                        },
                        complete: function () {
                            $B.removeLoading(loading, () => {
                                loading = undefined;
                            });
                        }
                    }, $c);
                }
            }else if(type === "json"){
                let $c = $wrap.firstChild.firstChild;
                if(!$c.firstChild){
                    $c.appendChild(loading);
                    let method = (url.indexOf(".data") > 0 || url.indexOf(".json") > 0) ? "GET" : "POST";
                    $B.request({
                        dataType: 'json',
                        url: url,
                        type: method,
                        onErrorEval: true,
                        ok: function (message, data) {
                            if (isFun) {
                                _this.opts.onLoaded.call( $c, data,title,'json');
                            }
                        },
                        final: function (res) {
                            try {
                                $B.removeLoading(loading, () => {
                                    loading = undefined;
                                });
                            } catch (ex) {
                            }
                        }
                    });
                }
            }else {
                let ifr = $wrap.firstChild;               
                if(!$B.DomUtils.attribute(ifr,"loaded")){
                    $wrap.appendChild(loading);
                    $B.DomUtils.onload(ifr, () => {
                        $B.DomUtils.attribute(ifr,{"loaded":1});
                        try {
                            $B.removeLoading(loading, () => {
                                loading = undefined;
                            });
                        } catch (ex) {
                        }
                        if (isFun) {
                            _this.opts.onLoaded.call( ifr, "",title,'iframe');
                        }
                    });
                    ifr.src = url;
                }               
            }
        }
    }
    _retoreStyle($it){        
        var $txt = $B.DomUtils.children($it,"span")[0];
        $B.DomUtils.removeClass($it,"k_accordion_header_actived");
        var $icon = $B.DomUtils.children($it,".k_accordion_header_icon")[0].firstChild;
        $B.animate($icon, { "rotateZ": "0deg" }, { duration: 300 });
        $B.DomUtils.css($it,this.opts.fontStyle);
        $B.DomUtils.css($it,{"background": this.opts.accordionStyle.background});
        let $ficon = $B.DomUtils.children($it,"i");
        $B.DomUtils.css($ficon,{"color": this.opts.iconColor});
        $B.DomUtils.css($icon,{"color": this.opts.iconColor});
        $B.DomUtils.css($txt,{"color": this.opts.iconColor});
    }
    _setActivedStyle(){
        var $it = this.activedItem;
        var $txt = $B.DomUtils.children($it,"span")[0];
        var $icon = $B.DomUtils.children($it,".k_accordion_header_icon")[0].firstChild;
        $B.DomUtils.addClass($it,"k_accordion_header_actived");
        $B.animate($icon, { "rotateZ": "90deg" }, { duration: 300 });
        $B.DomUtils.css($it,this.opts.activedStyle);
        let $ficon = $B.DomUtils.children($it,"i"); 
        $B.DomUtils.css($ficon,{color:this.opts.activedStyle.iconColor});
        $B.DomUtils.css($txt,{color:this.opts.activedStyle.iconColor});        
        $B.DomUtils.css($icon,{color:this.opts.activedStyle.iconColor});
        var $body = $it.nextSibling;
        $B.DomUtils.css($body,this.opts.accordionStyle);
    }
}
$B["Accordion"] = Accordion; 
 if(!window["$B"] ) {window["$B"] = $B;} 
  return window["$B"] ; 
});

/*! BUI - v2.0.0 - 2022-03-13 09:03:17 
Copyright (c): kevin.huang  https://gitee.com/kevin-huang/bui2.0.git (qq:757723114) 
Released under MIT License*/
(function ( global, factory) {
//   "use strict";
	if ( typeof module === "object" && typeof module.exports === "object" ) {
		module.exports = global.document ?
			factory( global, true ) :
			function( w ) {
				if ( !w.document ) {
					throw new Error( "Bui requires a window with a document" );
				}
				return factory( w );
			};
	} else {
		  factory( global );
	}  
})( typeof window !== "undefined" ? window : this, function( window, noGlobal ) {
//   "use strict";
  var $B = window["$B"] ?  window["$B"] : {};

var defaultOpts = {
    title: '', //标题
    isTop: false,
    iconCls: null, //图标class，font-awesome字体图标
    iconColor: undefined,//图标颜色
    headerColor: undefined,//头部颜色
    toolbar: null, //工具栏对象参考工具栏组件配置说明，可以是创建函数
    toolbarStyle: undefined,//工具栏样式定义
    size: { width: 'auto', height: 'auto' },
    shadow: true, //是否需要阴影
    radius: undefined, //圆角px定义
    header: true, //是否显示头部
    zIndex: 2147483647,//层级
    content: null, //静态内容
    url: '',//请求地址
    dataType: 'html', //当为url请求时，html/json/iframe
    closeType: 'hide', //关闭类型 hide(隐藏，可重新show)/ destroy 直接从dom中删除    
    moveProxy: false, //是否代理移动方式
    draggableHandler: 'header', //拖动触发焦点
    triggerHide:false,
    closeable: false, //是否关闭
    draggable: false, //是否可以拖动
    expandable: false, //可左右收缩
    maxminable: false, //可变化小大
    collapseable: false, //上下收缩
    resizeable: false,//右下角拖拉大小
    onResized: null, //function (pr) { },//大小变化事件
    onLoaded: null, //function () { },//加载后
    onClose: null, //关闭前
    onClosed: null, //function () { },//关闭后
    onExpanded: null, // function (pr) { },//左右收缩后
    onCollapsed: null, // function (pr) { }//上下收缩后
    onCreated: null //function($content,$header){} panel创建完成事件
};

var footToolbarHtml = '<div class="k_panel_foot_toolbar k_box_size" ></div>';
var contentHtml = '<div class="k_panel_content k_box_size" ></div>';
var headerIconHtml = '<div style="padding-right:1px;height:100%;text-align:center;" class="k_panel_header_icon  btn"><i style="font-size:16px;line-height:1.5em;" class="fa {icon}">\u200B</i></div>';
var headerHtml = '<div style="height:30px" class="k_panel_header"><div style="width:100%;height:100%;" class="k_panel_header_content"><h1 class="k_box_size" style="white-space:nowrap;text-overflow:ellipsis;overflow:hidden;">{title}</h1><div class="k_panel_header_toolbar k_box_size"></div></div></div>';

class Panel extends $B.BaseControl {
    constructor(elObj, opts) {
        super();
        var _this = this,
            $h1,
            headerHeight = 0,
            headerTop = 0;
        super.setElObj(elObj);
        $B.DomUtils.addClass(this.elObj, "k_panel_main k_box_size");
        this.opts = $B.extendObjectFn(true, {}, defaultOpts, opts);
        if (this.opts.shadow) {
            $B.DomUtils.addClass(this.elObj, "k_box_shadow");
        }
        if (this.opts.radius) {
            $B.DomUtils.css(this.elObj, { "border-radius": this.opts.radius });
        }
        let paddingRgt = 0;
        if (this.opts.header) {
            if (this.opts.expandable) {
                paddingRgt = 20;
            }
            if (this.opts.closeable) {
                paddingRgt = paddingRgt + 20;
            }
            if (this.opts.maxminable) {
                paddingRgt = paddingRgt + 20;
            }
            if (this.opts.collapseable) {
                paddingRgt = paddingRgt + 20;
            }
            if (typeof this.opts.title === "string") {
                this.$header = $B.DomUtils.createEl(headerHtml.replace("{title}", this.opts.title));
                $h1 = $B.DomUtils.findBySelector(this.$header, "h1");
            } else {
                this.$header = $B.DomUtils.createEl(headerHtml.replace("{title}", ""));
                $h1 = $B.DomUtils.findBySelector(this.$header, "h1");
                $B.DomUtils.append($h1, this.opts.title);
            }
            $B.DomUtils.append(this.elObj, this.$header);
            headerHeight = $B.DomUtils.outerHeight(_this.$header);
            $B.DomUtils.css($h1, { "line-height": headerHeight + "px", "padding-right": paddingRgt + "px" });
        }
        this.$body = $B.DomUtils.createEl(contentHtml);
        var bodyCss = { "border-top": "solid #fff " + headerHeight + "px" };
        if (this.opts.bodyCss) {
            $B.extendObjectFn(bodyCss, this.opts.bodyCss);
        }
        $B.DomUtils.css(this.$body, bodyCss);
        $B.DomUtils.prepend(this.elObj, this.$body);
        //加入工具栏功能
        this.toolArray = [];
        var toolbar;
        if ($B.Toolbar) {/***依赖toobar组件***/
            let paddingBottom = "2px";
            if (typeof this.opts.toolbar === 'function') { //由外部创建工具栏
                toolbar = $B.DomUtils.createEl(footToolbarHtml);
                this.opts.toolbar.call(toolbar);
                if (this.$header) {
                    $B.DomUtils.before(this.$header, toolbar);
                } else {
                    $B.DomUtils.append(this.elObj, toolbar);
                }
            } else if (this.opts.toolbar) {
                toolbar = $B.DomUtils.createEl(footToolbarHtml);
                if (this.$header) {
                    $B.DomUtils.before(this.$header, toolbar);
                } else {
                    $B.DomUtils.append(this.elObj, toolbar);
                }
                var toolbarOpts;
                if (Array.isArray(this.opts.toolbar)) {
                    toolbarOpts = { buttons: this.opts.toolbar, "align": "center" };
                    if (this.opts.toolbarAlign) {
                        toolbarOpts.align = this.opts.toolbarAlign;
                    }
                } else {
                    toolbarOpts = this.opts.toolbar;
                }
                if (this.opts.toolbarStyle) {
                    $B.extendObjectFn(toolbarOpts, this.opts.toolbarStyle);
                }
                if(toolbarOpts.align && toolbarOpts.align === "100%"){
                    paddingBottom = "0px";
                }
                let toolbarIns = new $B.Toolbar(toolbar, toolbarOpts);
                this.toolArray.push(toolbarIns);
            }
            if (toolbar) {               
                let h = $B.DomUtils.outerHeight(toolbar);
                $B.DomUtils.css(toolbar, { "padding-bottom": paddingBottom });
                $B.DomUtils.css(this.$body, { "border-bottom": h + "px solid #fff" });
            }
        }
        this._$outerBody = this.$body;
        if ($B.myScrollbar && this.opts.dataType !== "iframe") {
            this.$body = $B.myScrollbar(this.$body, {});
            $B.DomUtils.css(this.$body, { "padding":"2px 4px"});
        } else {
            $B.DomUtils.css(this.$body, { "overflow": "auto" ,"padding":"2px 4px"});
        }
        var isUrl = true;
        if (this.opts.content !== null && this.opts.content !== "") {
            isUrl = false;
            $B.DomUtils.append(this.$body, this.opts.content);
        }
        $B.DomUtils.css(this.elObj, this.opts.size);
        if (this.opts.iconCls != null && this.opts.header) {
            let $icon = $B.DomUtils.createEl(headerIconHtml.replace("{icon}", this.opts.iconCls));
            $B.DomUtils.css($icon, { "line-height": headerHeight + "px" });
            $B.DomUtils.prepend($B.DomUtils.children(this.$header, ".k_panel_header_content"), $icon);
            $icon = $B.DomUtils.children($icon, "i");
            if (this.opts.iconColor) {
                $B.DomUtils.css($icon, { "color": this.opts.iconColor });
            }
        }
        let $toolbar;
        if (this.opts.header) {
            $toolbar = $B.DomUtils.children(this.$header, ".k_panel_header_content");
            $toolbar = $B.DomUtils.children($toolbar, ".k_panel_header_toolbar");
            if (this.opts.expandable) { //可左右收缩
                let $a = $B.DomUtils.createEl("<a style='line-height:" + headerHeight + "px' class='k_panel_tool_icon'><i class='fa fa-left-open-2'></i></a>");
                $B.DomUtils.append($toolbar, $a);
                $B.DomUtils.click($a, function () {
                    let $_t = $B.DomUtils.children(this, "i");
                    if ($B.DomUtils.hasClass($_t, "fa-left-open-2")) {
                        $B.DomUtils.addClass($_t, "fa-right-open-2");
                        $B.DomUtils.removeClass($_t, "fa-left-open-2");
                        var allEls = $B.DomUtils.children(_this.elObj);
                        $B.fadeOut(allEls, 100, () => {
                            _this.expandWidth = $B.DomUtils.css(_this.elObj, "width");
                            $B.animate(_this.elObj, {
                                width: 30
                            }, {
                                duration: 100,
                                complete: () => {
                                    $B.DomUtils.addClass(_this.elObj, "k_panel_body_retractile");
                                    let $e = $B.DomUtils.createEl("<a style='display:block;height:22px;width:100%;text-align:center;position:absolute;top:6px;cursor:pointer;' class='k_panel_tool_icon'><i class='fa fa-right-open-2'></i></a>");
                                    $B.DomUtils.append(_this.elObj, $e);
                                    $B.DomUtils.click($e, () => {
                                        $B.DomUtils.removeClass($_t, "fa-right-open-2");
                                        $B.DomUtils.addClass($_t, "fa-left-open-2");
                                        $B.animate(_this.elObj, {
                                            width: _this.expandWidth
                                        }, {
                                            duration: 100,
                                            complete: () => {
                                                $B.DomUtils.removeClass(_this.elObj, "k_panel_body_retractile");
                                                $B.DomUtils.remove($e);
                                                $B.fadeIn(allEls, 100, () => {
                                                    if (typeof _this.opts.onExpanded === 'function') {
                                                        setTimeout(() => {
                                                            _this.opts.onExpanded.call(_this.elObj, "max");
                                                        }, 1);
                                                    }
                                                });
                                            }
                                        });
                                        return false;
                                    });
                                    $B.DomUtils.click(_this.elObj, function () {
                                        $B.DomUtils.trigger($e, "click");
                                        $B.DomUtils.offEvents(_this.elObj, "click");
                                        return false;
                                    });
                                    if (typeof _this.opts.onExpanded === 'function') {
                                        setTimeout(() => {
                                            _this.opts.onExpanded.call(_this.elObj, "min");
                                        }, 1);
                                    }
                                }
                            });
                        });
                    }
                    return false;
                });
            }
            if (this.opts.maxminable) { //可变化小大                
                let $a = $B.DomUtils.createEl("<a style='line-height:" + headerHeight + "px' class='k_panel_tool_icon'><i class='fa fa-resize-full-2'></i></a>");
                $B.DomUtils.append($toolbar, $a);
                $B.DomUtils.click($a, function (e) {
                    let $i = $B.DomUtils.children(this, "i");
                    let pr;
                    $B.DomUtils.css(_this.elObj, { "position": "absolute" });
                    if ($B.DomUtils.hasClass($i, "fa-resize-full-2")) {
                        let srcWidth = $B.DomUtils.css(_this.elObj, "width");
                        let srcHeight = $B.DomUtils.css(_this.elObj, "height");
                        _this.srcSize = { width: srcWidth, height: srcHeight };
                        let $parent = _this.elObj.parentNode;
                        let zIndex = $B.DomUtils.css(_this.elObj, "zIndex");
                        _this.zIndex = zIndex;
                        $B.DomUtils.css(_this.elObj, { "z-index": _this.opts.zIndex });
                        let p_width = $B.DomUtils.width($parent);
                        let p_height = $B.DomUtils.height($parent);
                        pr = {
                            width: p_width,
                            height: p_height
                        };
                        if (_this.opts.draggable) {
                            pr["top"] = 0;
                            pr["left"] = 0;
                            let pos = $B.DomUtils.position(_this.elObj);
                            _this.srcPos = pos;
                        }
                        $B.animate(_this.elObj, pr, {
                            duration: 120, complete: () => {
                                if (typeof _this.opts.onResized === "function") {
                                    setTimeout(() => {
                                        _this.opts.onResized.call(_this.elObj, "max");
                                    }, 1);
                                }
                            }
                        });
                        $B.DomUtils.removeClass($i, "fa-resize-full-2");
                        $B.DomUtils.addClass($i, "fa-resize-normal");
                    } else {
                        pr = _this.srcSize;
                        if (_this.opts.draggable) {
                            let src_pos = _this.srcPos;
                            pr["top"] = src_pos.top;
                            pr["left"] = src_pos.left;
                        }
                        pr["z-index"] = _this.zIndex;
                        $B.animate(_this.elObj, pr, {
                            duration: 120, complete: () => {
                                if (typeof _this.opts.onResized === "function") {
                                    setTimeout(() => {
                                        _this.opts.onResized.call(_this.elObj, "min");
                                    }, 1);
                                }
                            }
                        });
                        $B.DomUtils.removeClass($i, "fa-resize-normal");
                        $B.DomUtils.addClass($i, "fa-resize-full-2");
                    }
                    return false;
                });
                //加入双击功能
                if (this.$header) {
                    $B.DomUtils.bind(this.$header, {
                        click: function () {
                            return false;
                        },
                        dblclick: function () {
                            $B.DomUtils.trigger($a, "click");
                            return false;
                        }
                    });
                }
            }
            if (this.opts.collapseable) { //上下收缩
                let $a = $B.DomUtils.createEl("<a style='line-height:" + headerHeight + "px' class='k_panel_tool_icon'><i class='fa fa-down-open-2'></i></a>");
                $B.DomUtils.append($toolbar, $a);
                $B.DomUtils.click($a, function () {
                    let $_t = $B.DomUtils.children(this, "i");
                    let allEls = $B.DomUtils.previousAll(_this.$header);
                    if ($B.DomUtils.hasClass($_t, "fa-down-open-2")) {
                        $B.fadeOut(allEls, 100, function () {
                            _this.lastHeight = $B.DomUtils.height(_this.elObj);
                            $B.animate(_this.elObj, {
                                height: headerHeight
                            }, { duration: 150 });
                            $B.DomUtils.removeClass($_t, "fa-down-open-2");
                            $B.DomUtils.addClass($_t, "fa-up-open-2");
                            if (typeof _this.opts.onCollapsed === "function") {
                                setTimeout(function () {
                                    _this.opts.onCollapsed.call(_this.elObj, "min");
                                }, 1);
                            }
                        });

                    } else {
                        $B.animate(_this.elObj, {
                            height: _this.lastHeight
                        }, {
                            duration: 150, complete: function () {
                                $B.DomUtils.removeClass($_t, "fa-up-open-2");
                                $B.DomUtils.addClass($_t, "fa-down-open-2");
                                $B.fadeIn(allEls, 100, function () {
                                    if (typeof _this.opts.onCollapsed === "function") {
                                        setTimeout(function () {
                                            _this.opts.onCollapsed.call(_this.elObj, "max");
                                        }, 1);
                                    }
                                });
                            }
                        });
                    }
                    return false;
                });
            }
        }
        if (this.opts.closeable) { //可关闭窗口
            let $a;
            if ($toolbar) {
                $a = $B.DomUtils.createEl("<a style='line-height:" + headerHeight + "px' class='k_panel_tool_icon'><i class='fa fa-cancel'></i></a>");
                $B.DomUtils.append($toolbar, $a);
            } else {
                $a = $B.DomUtils.createEl("<a style='display:block;position:absolute;top:0px;right:0px;width:14px;height:14px;z-index:2147483647;cursor:pointer;'><i style='color:#A0A2A2' class='fa fa-cancel-circled'></i></a>");
                $B.DomUtils.append(this.elObj, $a);
            }
            $B.DomUtils.click($a, function () {
                if (_this.opts.closeType === "hide") {
                    _this.close();
                } else {
                    _this.destroy();
                }
                return false;
            });
        }
        if (this.opts.draggable && $B.draggable) {
            var dragOpt = {
                isProxy: this.opts.moveProxy,
                onDragReady: this.opts.onDragReady, //鼠标按下时候准备拖动前，返回true则往下执行，返回false则停止
                onStartDrag: this.opts.onStartDrag, //开始拖动事件
                onDrag: this.opts.onDrag, //拖动中事件
                onStopDrag: this.opts.onStopDrag //拖动结束事件
            };
            dragOpt["onDragReady"] =  (state, e)=> {               
                if(this.opts.triggerHide){
                    let $body = $B.getBody();
                    $B.DomUtils.trigger($body,"mousedown");
                }
                if (e.target.nodeName === "I" && $B.DomUtils.hasClass(e.target.parentElement, "k_panel_tool_icon")) {
                    return false;
                }
            };
            if (this.opts.draggableHandler === 'header' && this.opts.header) {
                dragOpt["handler"] = this.$header;
            }
            $B.draggable(_this.elObj, dragOpt);
        }
        if (toolbar) {
            let _h = $B.DomUtils.outerHeight(toolbar);
            let borderCss = {
                "border-bottom-width": _h + "px",
                "border-bottom-style": "solid",
                "border-bottom-color": "#ffffff"
            };
            $B.DomUtils.css(this._$outerBody, borderCss);
        }
        if (typeof this.opts.onCreated === 'function') {
            this.opts.onCreated.call(this.elObj, this.$body, this.$header);
        }
        if (isUrl && this.opts.url !== null && this.opts.url !== "") {
            this.load({
                url: this.opts.url,
                dataType: this.opts.dataType
            });
        }
        if (this.opts.resizeable) {
            $B.DomUtils.css(this.elObj,{"overflow":"hidden"});
            let $el = $B.DomUtils.createEl("<div style='position:absolute;bottom:-3px;right:-3px;width:10px;height:20px;transform: rotate(45deg);z-index:2147483647;font-size:16px;cursor:pointer;'><i style='' class='fa fa-angle-double-right k_panel_resizeable_icon'></i></div>");
            $B.DomUtils.append(this.elObj, $el);
            $B.DomUtils.mousedown($el, function (e) {
                var size = $B.DomUtils.outerSize(_this.elObj);
                var h = size.height;
                var w = size.width;
                var ofs = $B.DomUtils.offset(_this.elObj);
                var $s = $B.DomUtils.createEl("<div style='position:absolute;width:" + w + "px;height:" + h + "px;top:" + ofs.top + "px;left:" + ofs.left + "px;z-index:2147483647;border:1px dashed rgb(12, 156, 229);'></div>");
                var $node = $B.DomUtils.createEl("<div style='position:absolute;bottom:0px;right:0px;width:10px;height:10px'></div>");
                $B.DomUtils.append($s, $node);
                $B.DomUtils.append($s, $node);
                $B.DomUtils.append($B.getBody(), $s);
                $B.draggable($node, {
                    onStartDrag: function (arg) {                       
                        let size = $B.DomUtils.outerSize(_this.elObj);
                        arg.state.elh = size.height;
                        arg.state.elw = size.width;
                    },
                    onDrag: function (args) {
                        var state = args.state;
                        var leftOffset = state._data.leftOffset;
                        var topOffset = state._data.topOffset;
                        $B.DomUtils.css($s, {
                            height: state.elh + topOffset,
                            width: state.elw + leftOffset
                        });
                    },
                    onStopDrag: function (args) {
                        $B.DomUtils.remove($s);
                        var state = args.state;
                        var leftOffset = state._data.leftOffset;
                        var topOffset = state._data.topOffset;
                        var newSize = {
                            height: state.elh + topOffset,
                            width: state.elw + leftOffset
                        };
                        _this.resize(newSize);
                    },
                    onMouseUp: function (args) {
                        $B.DomUtils.remove($s);
                    }
                });
                $B.DomUtils.trigger($node, "mousedown", e);
            });
        }
    }
    reload() {
        this.load();
    }
    /**用于加载数据
     args={
        url: null,//url地址
        dataType:'html/json/iframe'
    }**/
    load(args) {
        var url = this.opts.url;
        if ($B.isUrl(this.opts.content)) {
            url = this.opts.content;
        }
        var dataType = this.opts.dataType;
        var _this = this;
        if (args) {
            url = args.url;
            dataType = args.dataType;
        }
        var isFun = typeof this.opts.onLoaded === 'function';
        if (url.indexOf("?") > 0) {
            url = url + "&_t_=" + $B.generateMixed(5);
        } else {
            url = url + "?_t_=" + $B.generateMixed(5);
        }
        this.url = url;
        if(!this.loading){
            this.loading = $B.getLoadingEl();
            let loadEl = this.$body;
            if(dataType !== "iframe"){
                loadEl = this.$body.parentNode;
            }
            $B.DomUtils.append(loadEl, this.loading);
        }
        setTimeout( ()=> {
            if (dataType === "html") {
                $B.htmlLoad({
                    url: url,
                    success: function (res) {                        
                        _this.$body.innerHTML = res;           
                        if (isFun) {
                            setTimeout(()=>{
                                _this.opts.onLoaded.call(_this.$body, {});
                            },1);                           
                        }
                        $B.bindInputClear(_this.$body);                        
                    },
                    complete:function(){
                        let $l = _this.loading;
                        _this.loading = undefined;
                        try {
                            $B.removeLoading($l,()=>{                                   
                            });
                        } catch (ex) {
                        }
                    }
                },this.$body);
            } else if (dataType === "json") {
                let method = ( url.indexOf(".data") > 0 || url.indexOf(".json") > 0 ) ? "GET" : "POST";
                $B.request({
                    dataType: 'json',
                    url: url,
                    type:method,
                    onErrorEval:true,
                    ok: function (message, data) {
                        if (isFun) {
                            _this.opts.onLoaded.call(_this.$body, data);
                        }
                    },
                    final: function (res) {
                        let $l = _this.loading;
                        _this.loading = undefined;
                        try {
                            $B.removeLoading($l,()=>{                               
                            });
                        } catch (ex) {
                        }
                    },
                    fail:function(arg1,arg2,arg3){
                    }
                });
            } else {
                let goAppend = false;
                if (!_this.iframe) {
                    goAppend = true;
                    _this.iframe = $B.getIframeEl("k_panel_content_ifr");
                    var ifrId = $B.getUUID();
                    $B.DomUtils.attribute(_this.iframe, { "name": ifrId, "id": ifrId });
                    $B.DomUtils.onload(_this.iframe, () => {
                        var url = $B.DomUtils.attribute(_this.iframe, "src");
                        if (url !== "") {                           
                            let $l = _this.loading;
                            _this.loading = undefined;
                            try {
                                $B.removeLoading($l,()=>{                                   
                                });
                            } catch (ex) {
                            }
                            if (isFun) {
                                _this.opts.onLoaded.call(_this.$body, {});
                            }
                            var ua = window.navigator.userAgent;
                            var isFirefox = ua.indexOf("Firefox") !== -1;
                            try {
                                var ifrBody = this.contentDocument.body;
                                $B.DomUtils.append(ifrBody, "<span id='_window_ifr_id_' style='display:none'>" + ifrId + "</span>");
                                if (isFirefox) {
                                    // ifrBody.find("a").each(function () {
                                    //     var $a = $(this);
                                    //     if ($a.attr("href").toLowerCase().indexOf("javascript") > -1) {
                                    //         $a.attr("href", "#");
                                    //     }
                                    // });
                                }
                            } catch (ex) { }
                        }
                    });
                }
                _this.iframe.src = url;
                if (goAppend) {
                    $B.DomUtils.append(_this.$body, _this.iframe);
                }
            }
        }, 1);
    }
    /**
     * 更新静态内容 content
     * ***/
    updateContent(content) {
        $B.DomUtils.html(this.$body, content);
    }
    /**
     * args={title:'标题',iconCls:'图标样式'}/args=title
     ***/
    setTitle(args) {
        if (!this.opts.header) {
            return;
        }
        var div = $B.DomUtils.children(this.$header, "div");
        var h1 = $B.DomUtils.children(div, "h1");
        if (typeof args === 'string') {
            $B.DomUtils.html(h1, args);
        } else {
            if (args.title) {
                $B.DomUtils.html(h1, args.title);
            }
            if (args.iconCls) {
                var icon = $B.DomUtils.children(div, ".k_panel_header_icon");
                icon = $B.DomUtils.children(icon, "i");
                $B.DomUtils.attribute(icon, { "class": "fa " + args.iconCls });
            }
        }
    }
    _onAnimate(fn){        
        if (this.opts.position) {
            let hidepos = $B.DomUtils.outerHeight( this.elObj);
            let duration = { duration: 260, complete: fn };
            if (this.opts.position === "bottom") {
                $B.animate(this.elObj, { bottom: -hidepos },duration );
            } else if(this.opts.position === "top") {
                $B.animate(this.elObj, { top: -hidepos }, duration);
            }else{
                $B.fadeOut(this.elObj, 200, fn);
            }
        } else {
            $B.fadeOut(this.elObj, 200, fn);
        }
    }
    _canClose(){
        var goClose = true;
        if (typeof this.opts.onClose === 'function') {
            let r = this.opts.onClose.call(this.elObj);
            if(r !== undefined){
                goClose = r;
            }
        }
        return goClose;
    }
    /**关闭
     * ifForce:是否强制关闭，强制则忽略onClose的返回
     * **/
    close(isForce) {
        if (!this.opts) {
            return;
        }
        if (this.opts.closeType === "destroy") {
            this.destroy();
            return;
        }
        if(!isForce){
            if( !this._canClose()){
                return;
            }
        }
        var _this = this;
        this._onAnimate(function () {
            $B.DomUtils.hide(_this.elObj);
            _this._invokeClosedFn();
        });
    }
    /**显示**/
    show(callBack) {
        if (this.elObj) {
            $B.fadeIn(this.elObj, 200, callBack);
        }
    }
    hide() {
        this.close(true);
    }
    fadeOut(timeOut, callBack) {
        var m = 300;
        if (timeOut) {
            m = timeOut;
        }
        $B.fadeOut(this.elObj, 200, () => {
            this.close(true);
            if (callBack) {
                callBack();
            }
        });
    }
    _invokeClosedFn() {
        if (this.opts.dataType !== "iframe") {

        }
        if (typeof this.opts.onClosed === 'function') {
            try {
                this.opts.onClosed.call(this.elObj);
            } catch (e) {
                console.log(e);
            }
        }
    }
    /**
     * 查询元素
     * ***/
    find(id) {
        let el = $B.DomUtils.findbyId(this.$body,id);
        return el;
    }
    findByClass(cls){
        let res = $B.Dom.findByClass(this.$body,cls);
        return res;
    }
    /**
     * 销毁
     * **/
    destroy(isForce) {
        if (!this.opts) {
            return;
        }
        if(!isForce){
            if( !this._canClose()){
                return;
            }
        }
        var _this = this;        
        this._onAnimate(function () {
            _this._invokeClosedFn();
            for (var i = 0, len = _this.toolArray.length; i < len; ++i) {
                _this.toolArray[i].destroy();
            }
            _this._destroy();
        });        
    }
    _destroy() {
        super.destroy();
    }
    /*重设大小
     * args={width:,height:}
     * **/
    resize(args, callBack) {
        var rs = {};
        var src_size = {};
        if (args.width) {
            rs["width"] = args.width;
            src_size["width"] = args.width;
        } else {
            src_size["width"] = $B.DomUtils.width(this.elObj);
        }
        if (args.height) {
            rs["height"] = args.height;
            src_size["height"] = args.height;
        } else {
            src_size["height"] = $B.DomUtils.height(this.elObj);
        }
        this.srcSize = src_size;
        $B.animate(this.elObj, src_size, {
            duration: 100, complete: () => {
                this.onResized();
            }
        });
    }    
    onResized(params) {
        
    }
    /***获取大小***/
    getSize() {
        return {
            width: $B.DomUtils.outerWidth(this.elObj),
            height: $B.DomUtils.outerHeight(this.elObj)
        };
    }
    /**获取iframe**/
    getIfr() {
        if (this.opts.dataType !== 'iframe') {
            return;
        }
        return this.iframe;
    }
    setAttr(attr) {
        $B.DomUtils.attribute(this.elObj,attr);
    }
}
$B["Panel"] = Panel; 
 if(!window["$B"] ) {window["$B"] = $B;} 
  return window["$B"] ; 
});

/*! BUI - v2.0.0 - 2022-03-13 09:03:17 
Copyright (c): kevin.huang  https://gitee.com/kevin-huang/bui2.0.git (qq:757723114) 
Released under MIT License*/
(function ( global, factory) {
//   "use strict";
	if ( typeof module === "object" && typeof module.exports === "object" ) {
		module.exports = global.document ?
			factory( global, true ) :
			function( w ) {
				if ( !w.document ) {
					throw new Error( "Bui requires a window with a document" );
				}
				return factory( w );
			};
	} else {
		  factory( global );
	}  
})( typeof window !== "undefined" ? window : this, function( window, noGlobal ) {
//   "use strict";
  var $B = window["$B"] ?  window["$B"] : {};

var draggableDefaults = {
    nameSpace: 'draggable', //命名空间，一个对象可以绑定多种拖动实现
    which: undefined, //鼠标键码，是否左键,右键 才能触发拖动，默认左右键均可 1,3
    defaultZindex: 999999, //当z-index为auto时采用的层级数值
    holdTime: undefined, //按下鼠标多少毫秒才拖动，默认不设置立即可拖动
    globalDraging:true,//是否是全局拖动，避免多层触发拖动
    isProxy: false, //是否产生一个空代理进行拖动
    disabled: false, //是否禁用拖动
    handler: undefined, //触发拖动的对象，默认是对象本身
    cursor: 'move', //鼠标样式
    axis: undefined, // v垂直方 h水平，默认全向   
    setMatrixRate:undefined,//设置缩放比例        
    onDragReady: undefined, //鼠标按下时候准备拖动前，返回true则往下执行，返回false则停止
    onStartDrag: undefined, //开始拖动事件
    onDrag: undefined, //拖动中事件
    onStopDrag: undefined, //拖动结束事件
    onMouseUp: undefined, //当没有发生拖动，鼠标放开时候调用
    onProxyEnd: undefined, // function(args,pos) 代理拖动结束，返回false则不更新代理
    onCreateProxy: undefined // function(proxyObj,srcObj)，代理创建监听
};
/***1.5秒后及时清理state上的引用****/
function clearDragArgs(state) {
    setTimeout(function () {
        for (var p in state) {
            if (state.hasOwnProperty(p)) {
                delete state[p];
            }
        }
    }, 1500);
}
/***
 * 根据鼠标计算位置
 * ****/
function _compute(e) {
    var state = e.params,
        data = state._data,
        opts = state.options;
    //拖动后的 = 原先+拖动位移
    var leftOffset = e.pageX - data.startX,
        topOffset = e.pageY - data.startY;
    if (opts.axis === 'v') { //如果是垂直，则只改top
        leftOffset = 0;
    } else if (opts.axis === 'h') { //如果是水平拖动 则只改left
        topOffset = 0;
    }
    var left = data.startLeft + leftOffset,
        top = data.startTop + topOffset;
    data.leftOffset = leftOffset;
    data.topOffset = topOffset;
    data.oldLeft = data.left;
    data.oldTop = data.top;
    data.left = left;
    data.top = top;
    var apply = true;
    if (state["onDragFn"]) {
        var args = {
            shiftKey:e.shiftKey,
            ctrlKey:e.ctrlKey,
            altKey:e.altKey,
            state: state,
            which: opts.which
        };
        var res = opts.onDrag(args);
        if (typeof res !== 'undefined') {
            apply = res;
        }
    }    
    data["apply"] = apply;
}
//移动事件 hasCallStartFn
function _docMove(e) {
    var state = e.params;
    var opts = state.options;
    var data = state._data;
    data.pageX = e.pageX;
    data.pageY = e.pageY;
    /***** 是否已经调研了 onStartDrag *********/
    if (!state["hasCallStartFn"]) {
        //处理没有发生鼠标移动但是_docMove被触发的情况！！
        var leftOffset = e.pageX - data.startX,
            topOffset = e.pageY - data.startY;
        if (leftOffset === 0 && topOffset === 0) {
            console.log("过滤没有发生实际移动的 _docMove");
            return;
        }
        state["hasCallStartFn"] = true;       
        if (state.callStartDragFn) {
            //提取拖动目标的zIndex 2147483647
            //var zIndex = state.movingTarget.css("z-index");
            var zIndex = $B.DomUtils.css(state.movingTarget,"zIndex");
            if (zIndex === "auto") {
                zIndex = opts.defaultZindex;
            }
            state["zIndex"] = zIndex;
            //state.movingTarget.css("z-index", 2147483647);
            $B.DomUtils.css(state.movingTarget,{"z-index":2147483647});
            try {                
                opts.onStartDrag({
                    shiftKey:e.shiftKey,
                    ctrlKey:e.ctrlKey,
                    altKey:e.altKey,
                    state: state
                });
            } catch (ex) {
                if (console.error) {
                    console.error("onStartDrag error " + ex);
                } else {
                    console.log("onStartDrag error " + ex);
                }
            }
        }
    }
    _compute(e);
    if (data["apply"] && state.movingTarget) {
        if (data.matrixRate) { //存在缩放比例
            data.top = data.top / data.matrixRate;
            data.left = data.left / data.matrixRate;
        }
        var css = {
            cursor: state.options.cursor
        };
        if (opts.axis === 'v') {
            css.top = data.top + data.fixTop;
        } else if (opts.axis === 'h') {
            css.left = data.left + data.fixLeft;
        } else {
            css.top = data.top + data.fixTop;
            css.left = data.left + data.fixLeft;
        }
        //确保为整数
        css.top = parseInt(css.top);
        css.left = parseInt(css.left);
        $B.DomUtils.css(state.movingTarget,css);
    }
    if (typeof opts.notClearRange === "undefined") {
        //先清除document中的选择区域
        $B.clearDomSelected();
    }
}
/***
 * 应该domcument监听鼠标放开事件
 * ***/
function _docUp(e) {
    var state = e.params,
        data = state._data,
        opts = state.options,
        nameSpace = opts.nameSpace,
        target = state.target,
        body = $B.getBody();
    data.pageX = e.pageX;
    data.pageY = e.pageY;
    $B.DomUtils.offEvents(window.document,nameSpace+".*");
    $B.DomUtils.css(target,{"cursor":data.srcsor});
    window["_globalDraging"] = undefined;
    if (opts.isProxy) { //代理移动方式,则需要更新目标位置           
        var css = {
            left: data.srcPos.left + data.leftOffset,
            top: data.srcPos.top + data.topOffset
        };
        var go = true;
        if (opts.onProxyEnd) {
            var ret = opts.onProxyEnd(state, css);
            if (typeof ret !== "undefined") {
                go = ret;
            }
        }
        $B.DomUtils.remove(state["movingTarget"]);
        if (go) {
            css["position"] = "absolute";
            $B.DomUtils.css(target,css);
        }
    }
    var onStopDrag = opts.onStopDrag;
    var returnVar;
    if (typeof onStopDrag === "function" && state["hasCallStartFn"]) {
        returnVar = onStopDrag({
            shiftKey:e.shiftKey,
            ctrlKey:e.ctrlKey,
            altKey:e.altKey,
            state: state,
            e:e._e
        });
    }    
    if (state["hasCallStartFn"]) {
        var zIndex = state["zIndex"];
        $B.DomUtils.css(state.movingTarget,{"z-index":zIndex});
    } else {
        if (opts.onMouseUp) {
           let v = opts.onMouseUp({
                state: state,
                e:e._e
            });
            if(typeof v !== "undefined"){
                returnVar = v;
            }
        }
    }
    clearDragArgs();
    $B.DomUtils.css(body,{"cursor":"default"});
    if (typeof returnVar !== "undefined") {
        return returnVar;
    }
}
//监听document鼠标按下事件
function _docDown(e) {
    var state = e.params,
        opts = state.options,
        data = state._data;
    var movingTarget = state.target;
    var body = $B.getBody();
    if (opts.isProxy) {
        var ofs = data.srcOfs,
            w = data.width,
            h = data.height,
            fs = $B.DomUtils.css(state.target,"font-size"),
            fcolor = $B.DomUtils.css(state.target,"color");
        var pp = $B.DomUtils.css(state.target,"padding") ;
        var clz = "";
        if ($B.DomUtils.hasClass(state.target,"k_box_size")) {
            clz = "k_box_size";
        }
        movingTarget = $B.DomUtils.createEl("<div style='background-color:#ADADAD;opacity :0.6;font-size:" + fs + ";color:" + fcolor + ";padding:" + pp + ";cursor:" + opts.cursor + ";position:absolute;top:" + ofs.top + "px;left:" + ofs.left + "px' class='k_draggable_proxy " + clz + "'></div>");
         $B.DomUtils.width(movingTarget,w);
        $B.DomUtils.height(movingTarget,h);
        if (opts.onCreateProxy) {
            opts.onCreateProxy(movingTarget, state.target, e);
        }
        $B.DomUtils.append(body,movingTarget);
    }
    if (typeof opts.onDrag === "function") {
        state["onDragFn"] = true;
    }
    state["movingTarget"] = movingTarget;    
    $B.DomUtils.css(body,{"cursor":opts.cursor});
}
/***
 * 鼠标放开
 ****/
function onMouseUp(e) {
    window["_globalDraging"] = undefined;
    var dragtimer = $B.DomUtils.propData(this,"dragtimer");
    if (dragtimer) {
        clearTimeout(dragtimer);
        var state = e.params;
        $B.DomUtils.removePropData(this,"dragtimer");
        if (!state["hasCallStartFn"]) { //如果没有发生拖动
            var opts = state.options;
            if (opts.onMouseUp) {
                opts.onMouseUp({
                    state: state,
                    e:e._e
                });
            }
        }
        clearDragArgs(state);
    }
}
/**
 * args应对编程trigger触发的鼠标事件
 * e参数中不存在pageX、pageY的情况
 * **/
function onMouseDown(e, args) {   
    if (args && !e["pageX"]) {
        e["pageX"] = args.pageX;
        e["pageY"] = args.pageY;
        e["which"] = args.which;
    }
    if (e.ctrlKey) {       
        return true;
    }
    if(window["_globalDraging"] ){//已经存在全局的拖动
        console.log(" 已经存在全局的拖动");
        return true;
    }
    var state = e.params;
    var el = state.target;
    var options = state.options;
    if (!options.which || options.which === e.which) {
        if (!options.disabled) {
            var go = true;
            if (typeof options.onDragReady === 'function') {
               let ret  = options.onDragReady.call(el, state, e);
               if(typeof ret !== "undefined"){
                go = ret;
               }
            }
            if (go) {
                var posParentEl = el.parentNode;
                var posAttr;
                while (posParentEl ) {
                    posAttr =  $B.DomUtils.css(posParentEl,"position");
                    if (posAttr === "relative" || posAttr === "absolute") {
                        break;
                    }
                    if( posParentEl.nodeName === "BODY"){
                        break;
                    }
                    posParentEl = posParentEl.parentNode;
                }
                var matrix = $B.getMatrixArray(posParentEl); //父元素是否存在缩放
                var matrixRate;
                if (matrix) {
                    matrixRate = matrix[0];
                }
                if (typeof options.setMatrixRate === 'function') {
                    var rate = options.setMatrixRate();
                    if (typeof rate !== "undefined") {
                        matrixRate = rate;
                    }
                }             
                var parentOfs = $B.DomUtils.offset(posParentEl);
                var offset =  $B.DomUtils.offset(el);// $t.offset();
                var position = $B.DomUtils.position(el);
                //如果有css transform旋转，需要计算出 修正的偏移量    
                var ofs = $B.getAnglePositionOffset(el);
                var fixTop = ofs.fixTop;
                var fixLeft = ofs.fixLeft;
                var parent = el.parentNode;            
                var  scrollLeft = $B.DomUtils.scrollLeft(parent);
                var  scrollTop =  $B.DomUtils.scrollTop(parent);
                if(options.setScrollFn){
                    var sclRet = options.setScrollFn(parent);
                    if(typeof sclRet !== "undefined"){
                        scrollLeft = sclRet.scrollLeft;
                        scrollTop = sclRet.scrollTop;
                    }
                }
                var srcsor = el.style.cursor;
                if (!srcsor) {
                    srcsor = 'default';
                }
                var objWidth = $B.DomUtils.outerWidth(el);
                var objHeight = $B.DomUtils.outerHeight(el);
                if (options.isProxy) {
                    /***** 代理是放置于body下的，要用offset替换position *******/
                    position = offset;
                } else {                   
                    var resetCss = {
                        "position": "absolute"
                    };
                    if (options.axis === 'v') {
                        resetCss.top = position.top + fixTop ; //+ scrollTop
                    } else if (options.axis === 'h') {
                        resetCss.left = position.left + fixLeft ;//+ scrollLeft
                    } else {
                        resetCss.top = position.top + fixTop ;//+ scrollTop
                        resetCss.left = position.left + fixLeft ;//+ scrollLeft
                    }
                    if (matrixRate) {
                        resetCss.top = resetCss.top / matrixRate;
                        resetCss.left = resetCss.left / matrixRate;
                        objWidth = objWidth * matrixRate;
                        objHeight = objHeight * matrixRate;
                    }
                    $B.DomUtils.css(el,resetCss);
                    position = $B.DomUtils.position(el);
                }
                //封装拖动数据data对象，记录width,height,pageX,pageY,left 和top 等拖动信息                   
                var data = {
                    matrixRate: matrixRate,
                    parentOfs: parentOfs,
                    srcOfs: offset,
                    srcPos: position,
                    startLeft: position.left , //开始的位置信息	+ scrollLeft
                    startTop: position.top , //开始的位置信息	+ scrollTop
                    scrollLeft: scrollLeft,
                    scrollTop: scrollTop,
                    left: position.left , //当前left位置信息	+ scrollLeft
                    top: position.top , //当前top位置信息       + scrollTop
                    oldLeft: undefined, //旧的left位置 
                    oldTop: undefined, //旧的top位置	
                    startX: e.pageX, //鼠标点击时的x坐标
                    startY: e.pageY, //鼠标点击时的y坐标
                    pageX: e.pageX,
                    pageY: e.pageY,
                    width: objWidth,
                    height: objHeight,
                    fixTop: fixTop,
                    fixLeft: fixLeft,
                    srcsor: srcsor, //原来的鼠标样式
                    leftOffset: 0, //鼠标left偏移
                    topOffset: 0 //鼠标top偏移
                };
                state["hasCallStartFn"] = false; //是否已经调用了onStartDrag   
                state["_data"] = data;
                state["parent"] = parent; //拖动对象的父dom
                state["callStartDragFn"] = typeof options.onStartDrag === "function";
                var nameSpace = options.nameSpace;
                if (typeof options.holdTime !== 'undefined') { //定时触发事件
                    var _this = el;
                    var timer = setTimeout(function () {
                        if(options.onTimeIsUpFn){
                            if(options.onTimeIsUpFn()){
                               return;
                            }
                        }                      
                        if(options.globalDraging){
                            window["_globalDraging"] = true;
                        }                       
                        $B.DomUtils.css(el,{"cursor":state.options.cursor});
                        //往document上绑定鼠标移到监听 
                        _docDown({
                            params:state
                        });
                        $B.DomUtils.removeData(_this,"dragtimer");
                        $B.DomUtils.addListener(window.document,nameSpace+".mousemove",_docMove,state);
                        $B.DomUtils.addListener(window.document,nameSpace+".mouseup",_docUp,state);
                    }, options.holdTime);
                    $B.DomUtils.setData(_this,"dragtimer",timer);
                    $B.DomUtils.addListener(window.document,"clearDragTimer.mouseup",function(e){
                        clearTimeout(e.params.timer);
                        $B.DomUtils.offEvents(window.document,"clearDragTimer.*");
                    },{timer: timer});
                    // $doc.on('mouseup.clearDragTimer', {
                    //     timer: timer
                    // }, function (e) {
                    //     $doc.off('mouseup.clearDragTimer');
                    //     clearTimeout(e.params.timer);
                    // });
                } else {     
                    if(options.globalDraging){
                        window["_globalDraging"] = true;
                    }               
                    $B.DomUtils.css(el,{"cursor":state.options.cursor});
                    if(e.isTrigger){
                        _docDown({
                            params:state
                        });
                    }else{
                        $B.DomUtils.addListener(window.document,nameSpace+".mousedown",_docDown,state);
                    }                   
                    $B.DomUtils.addListener(window.document,nameSpace+".mousemove",_docMove,state);
                    $B.DomUtils.addListener(window.document,nameSpace+".mouseup",_docUp,state);
                }
            }
        }
    }
}
/****
 * 构造函数
 * options：defaults参数
 * *****/
$B.draggable = function (el,options) {
    var nameSpace = "draggable";
    var dragOpt;
    if(typeof el === 'string'){
        el = window.document.getElementById(el);
    }
    if (typeof options === 'string') {
        if (arguments.length > 1) {
            nameSpace = arguments[1];
        }
        var dataKey = nameSpace+"_opt";
        switch (options) {
            case 'enable': //启用拖动
                dragOpt =  $B.getData(el,dataKey).state.options;
                dragOpt.disabled = false;
                el.style.cursor = dragOpt.cursor;
                break;
            case 'disable': //禁用拖动
                dragOpt =  $B.getData(el,dataKey).state.options;
                dragOpt.disabled = false;
                el.style.cursor = "default";
                break;
            case 'unbind': //解除拖动
                let state = $B.getData(el,dataKey).state;
                $B.offEvents(state.handler,nameSpace + '.');
                delete state.handler;
                delete state.target;
                delete state.options;
                delete state.parent;
                $B.removeData(el,dataKey);       
                break;        
            default:
                throw new Error("不支持:" + options);
        }
    }
    var opts = $B.extendObjectFn(true,{},draggableDefaults,options);
    if (opts.nameSpace) {
        nameSpace = opts.nameSpace; //默认的命名空间
    }
    var handler = el; //handler是实际触发拖动的对象
    if (opts.handler) {
        handler = (typeof opts.handler === 'string' ? $B.DomUtils.findbyId(el,opts.handler) : opts.handler);      
        $B.DomUtils.css(handler,{cursor:options.cursor});
    }
    //存在则先解除已经绑定的拖动
    let state = $B.DomUtils.getData(el,nameSpace);
    if (state) {        
        $B.DomUtils.offEvents(state.handler,nameSpace+".*");
        delete state["handler"];
        delete state["target"];
        delete state["parent"];
        delete state["options"];
        delete state["_data"];
    }
    state = {
        handler: handler,
        target: el,
        options: opts
    };
    $B.DomUtils.setData(el,nameSpace, state);
    //注册事件 draggable 为命名空间
    $B.DomUtils.addListener(handler,nameSpace+".mousedown",onMouseDown,state);
    if (typeof opts.holdTime !== 'undefined') {
        $B.DomUtils.addListener(handler,nameSpace+".mouseup",onMouseDown,state);
    }
};
var scrollDefaults = {
    isHide: true,
    scrollAxis: 'xy',
    unScroll:'hidden',
    onScrollFn: undefined,
    style: {
        "radius": "6px",
        "size": "7px",
        "color": "#5DA0FF"
    }
};

function createScrollStyle(styleClzz, clazzName, style) {
    var rgb = $B.hex2RgbObj(style.color);
    var styleCtx = clazzName + '{width:' + style.size + ';height:' + style.size + '}';
    styleCtx = styleCtx + "." + styleClzz + "::-webkit-scrollbar-thumb," + "." + styleClzz + "::-webkit-scrollbar-thumb:vertical {box-shadow: inset 0 0 4px rgba(" + rgb.r + "," + rgb.g + "," + rgb.b + ",1);border-radius:" + style.radius + ";}";
    styleCtx = styleCtx + "." + styleClzz + "::-webkit-scrollbar-thumb:hover," + "." + styleClzz + "::-webkit-scrollbar-thumb:vertical:hover{box-shadow: inset 0 0 4px rgba(" + rgb.r + "," + rgb.g + "," + rgb.b + ",.9);background-color: " + style.color + ";}";
    $B.createHeaderStyle(styleClzz, styleCtx);
}

function getScrollYArgs(scrollTop, scrollHeight, clientHeight) {
    var heightPercentage = (clientHeight * 100 / scrollHeight);
    var vbarHeigth = clientHeight * (heightPercentage / 100);
    var vscrollSize = Math.max(clientHeight, scrollHeight) - clientHeight;
    var scrollRate = (clientHeight - vbarHeigth) / vscrollSize; 
    var posY = scrollTop * scrollRate;
    return { posY: posY, percent: heightPercentage,scrollRate:scrollRate };
}

function getScrollXArgs(scrollLeft, scrollWidth, clientWidth) {
    var widthPercentage = (clientWidth * 100 / scrollWidth);
    var vbarWidth = clientWidth * (widthPercentage / 100);
    var vscrollSize = Math.max(clientWidth, scrollWidth) - clientWidth;
    var scrollRate = (clientWidth - vbarWidth) / vscrollSize;
    var posX = scrollLeft * scrollRate;
    return { posX: posX, percent: widthPercentage,scrollRate:scrollRate };
}

function scrollAxisY(opts, $wrap) {
    var $sol = $B.DomUtils.children($wrap,".k_scrollbar_y");
    var wrap = $wrap;
    var style = opts.style;
    var scrollHeight = wrap.scrollHeight;
    var clientHeight = wrap.clientHeight;
    //console.log(clientHeight+"  "+scrollHeight);
    if (scrollHeight > (clientHeight + 2)) {
        var ret = getScrollYArgs(wrap.scrollTop, scrollHeight, clientHeight);       
        var posY = ret.posY;
        var percent = ret.percent;
        if (!$sol  || $sol.length === 0) {
            $sol = $B.DomUtils.createEl("<div class='k_scrollbar_y k_scrollbar_axis' style='cursor:pointer;position:absolute;right:0;width:" + style.size + ";border-radius:" + style.radius + ";opacity: 0.45;background:" + style.color + "'></div>");
            $B.DomUtils.append($wrap,$sol);
            if($B.draggable){
                $B.draggable($sol,{
                    nameSpace:'kscrollabr',
                    cursor:'pointer',
                    axis: 'v', // v or h  水平还是垂直方向拖动 ，默认全向
                    onDragReady:function(state){
                        $B.DomUtils.removePropData(state.target,"_scring");                      
                        return true;
                    },
                    setScrollFn:function($p){                       
                        let pel = $p.parentNode;
                        var scr = {
                            scrollLeft : $B.DomUtils.scrollLeft(pel),
                            scrollTop : $B.DomUtils.scrollTop(pel)
                        };
                        return scr;
                    },
                    onStartDrag:function(args){                   
                        var state = args.state;   
                        var $p = state.target.parentNode;   
                        var cheight =  $B.DomUtils.height($p);
                        var scrHeight = $p.scrollHeight;
                        var scrTop = $p.scrollTop;
                        var elHeight = $B.DomUtils.height(state.target);
                        var maxTop = cheight - elHeight;
                        var ret1 = getScrollYArgs(scrTop, scrHeight, cheight);
                        state.ret = ret1;
                        state.maxTop = maxTop;
                        $B.DomUtils.propData(state.target,{"_scring":"true"});
                    },
                    onDrag:function(args){
                        var state = args.state;
                        if(state._data.top < 0){
                            state._data.top = 0 ;
                        }else if(state._data.top > state.maxTop){
                            state._data.top = state.maxTop;
                        }
                        var scrollPos = state._data.top / state.ret.scrollRate;
                        $B.DomUtils.scrollTop($wrap,scrollPos);
                    },
                    onStopDrag:function(args){
                        $B.DomUtils.removePropData(args.state.target,"_scring");
                        return false;
                    },
                    onMouseUp:function(args){   
                        $B.DomUtils.removePropData(args.state.target,"_scring");
                    }
                });
            }
        }
        $B.DomUtils.css($sol,{ "height": percent + "%", "top": posY });
        $B.DomUtils.show($sol);
        $B.DomUtils.addClass($sol,"k_scrolly_show");
        $B.DomUtils.removeClass($sol,"k_scrolly_hide");          
    } else if ($sol) {
        $B.DomUtils.hide($sol);
        $B.DomUtils.addClass($sol,"k_scrolly_hide");
        $B.DomUtils.removeClass($sol,"k_scrolly_show");
    }
}
function scrollAxisX(opts, $wrap) {    
    var $sol = $B.DomUtils.children($wrap,".k_scrollbar_x");
    var wrap = $wrap;
    var style = opts.style;
    var scrollWidth = wrap.scrollWidth;
    var clientWidth = wrap.clientWidth;
    if (scrollWidth > (clientWidth + 2)) {
        var ret = getScrollXArgs(wrap.scrollLeft, scrollWidth, clientWidth);
        var posX = ret.posX;
        var percent = ret.percent;
        if (!$sol || $sol.length === 0) {
            $sol = $B.DomUtils.createEl("<div class='k_scrollbar_x k_scrollbar_axis' style='cursor:pointer;position:absolute;bottom:0;height:" + style.size + ";border-radius:" + style.radius + ";opacity: 0.45;background:" + style.color + "'></div>");
            $B.DomUtils.append($wrap,$sol);
            if($B.draggable){
                $B.draggable($sol,{
                    cursor:'pointer',
                    nameSpace:'kscrollabr',
                    axis: 'h', // v or h  水平还是垂直方向拖动 ，默认全向
                    onDragReady:function(state){
                        $B.DomUtils.removePropData(state.target,"_scring");
                        return true;
                    },
                    setScrollFn:function($p){
                        let pel = $p.parentNode;
                        var scr = {
                            scrollLeft : $B.DomUtils.scrollLeft(pel),
                            scrollTop : $B.DomUtils.scrollTop(pel)
                        };                        
                        return scr;
                    },
                    onStartDrag:function(args){
                        var state = args.state;
                        var $p = state.target.parentNode;   
                        var cwidth =   $B.DomUtils.width($p);
                        var scrWidth = $p.scrollWidth;
                        var scrLeft = $p.scrollLeft;                       
                        var ret1 = getScrollXArgs(scrLeft, scrWidth, cwidth);
                        state.ret = ret1;
                        var maxLeft = cwidth -  $B.DomUtils.width(state.target);
                        state.maxLeft = maxLeft;
                        $B.DomUtils.propData(state.target,{"_scring":"true"});
                    },
                    onDrag:function(args){
                        var state = args.state;
                        if(state._data.left < 0){
                            state._data.left = 0 ;
                        }else if(state._data.left > state.maxLeft){
                            state._data.left = state.maxLeft;
                        }
                        var scrollPos = state._data.left / state.ret.scrollRate;
                        $B.DomUtils.scrollLeft($wrap,scrollPos);
                    },
                    onStopDrag:function(args){
                        $B.DomUtils.removePropData(args.state.target,"_scring");
                        return false;
                    },
                    onMouseUp:function(args){
                        $B.DomUtils.removePropData(args.state.target,"_scring");
                    }
                });
            }        
        }
        $B.DomUtils.css($sol,{ "width": percent + "%", "left": posX });
        $B.DomUtils.show($sol);
        $B.DomUtils.addClass($sol,"k_scrollx_show");
        $B.DomUtils.removeClass($sol,"k_scrollx_hide");
    } else if ($sol.length >0) {
        $B.DomUtils.hide($sol);
        $B.DomUtils.addClass($sol,"k_scrollx_hide");
        $B.DomUtils.removeClass($sol,"k_scrollx_show");
    }
}
function myscrollbar(el,opts) {    
    opts = $B.extendObjectFn(true,{}, scrollDefaults,opts);    
    var styleClzz = "SCROLLBAR" + $B.generateMixed(5);
    var elCss = { "overflow": "hidden"};
    var scrollCss = {
        "overflow": "auto"
    };
    if (opts.scrollAxis === "x") {
        scrollCss = {
            "overflow-x": "auto",
            "overflow-y": opts.unScroll
        };
    } else if (opts.scrollAxis === "y") {
        scrollCss = {
            "overflow-x": opts.unScroll,
            "overflow-y": "auto"
        };
    }
    opts.sizeInt = parseInt(opts.style.size.replace("px", ""));
    var posAttr = $B.DomUtils.css(el,"position");
   
    if(posAttr !== "relative" && posAttr !== "absolute"){
        elCss["position"] = "relative";
    }
    $B.DomUtils.css(el,elCss);     
    var childs = $B.DomUtils.detachChilds(el);   
    var $wrapScroll = $B.DomUtils.createEl("<div class='k_scrollbar_wrap k_box_size "+styleClzz+"' style='width:100%;height:100%;'></div>");
    $B.DomUtils.append(el,$wrapScroll);
    $B.DomUtils.css($wrapScroll,scrollCss);    
    $B.DomUtils.scroll($wrapScroll,(e)=>{
        var scrollx = $B.DomUtils.scrollLeft($wrapScroll);
        var scrolly =  $B.DomUtils.scrollTop($wrapScroll);
        var $soly = $B.DomUtils.children($wrapScroll,".k_scrollbar_y");  //y轴滚动条
        var ret;     
        if ($soly.length >0 && $B.DomUtils.hasClass($soly,"k_scrolly_show") ) {
            if(!$B.DomUtils.propData($soly,"_scring")){
                let scrollHeight = $wrapScroll.scrollHeight;
                let clientHeight = $wrapScroll.clientHeight;
                ret = getScrollYArgs(scrolly, scrollHeight, clientHeight);                
                $B.DomUtils.css($soly,{ "height": ret.percent + "%", "top": ret.posY });
                $B.DomUtils.show($soly);
            }           
        }
        var $solx =  $B.DomUtils.children($wrapScroll,".k_scrollbar_x");  //x轴滚动条
        if ($solx.length >0 && $B.DomUtils.hasClass($solx,"k_scrollx_show") ) {
            if(!$B.DomUtils.propData($solx,"_scring")){
                let scrollWidth = $wrapScroll.scrollWidth;
                let clientWidth = $wrapScroll.clientWidth;
                ret = getScrollXArgs(scrollx, scrollWidth, clientWidth);              
                $B.DomUtils.css($solx,{ "width": ret.percent + "%", "left": ret.posX });
                $B.DomUtils.show($solx);
            }            
        }
        if (opts.onScrollFn) {
            opts.onScrollFn.call($wrapScroll, scrollx, scrolly);
        }
    });
    var $wrap = $B.DomUtils.createEl("<div style='position:relative;width:auto;height:auto;overflow:visible;'></div>");
    $B.DomUtils.append($wrapScroll,$wrap);   
    $B.DomUtils.append($wrap,childs);
    var clazzName = "." + styleClzz + "::-webkit-scrollbar";    
    var sclStyle =  $B.extendObjectFn(true,{}, opts.style);
    if (opts.isHide){
        sclStyle.size = '0px';
    }else{
        sclStyle.size = opts.style.size;
    }    
    sclStyle.color = "#F7F7F7"; 
    createScrollStyle(styleClzz, clazzName, sclStyle);
    $B.DomUtils.css($wrapScroll,{"scrollbar-color":"transparent transparent","scrollbar-track-color":"transparent","-ms-scrollbar-track-color": "transparent"});
    var mkScrollFn =  function (e) {
        if(opts.scrollAxis.indexOf("x") >= 0){
            scrollAxisX(opts, $wrapScroll);
        }
        if(opts.scrollAxis.indexOf("y") >= 0){
            scrollAxisY(opts, $wrapScroll);
        }   
        if(opts.onMkScrollFn){
            opts.onMkScrollFn($wrapScroll);
        }        
    };
    mkScrollFn(); 
    let b1 = $B.DomUtils.children($wrapScroll,".k_scrollbar_y");
    let b2 = $B.DomUtils.children($wrapScroll,".k_scrollbar_x");
    $B.DomUtils.addListener($wrapScroll,"myscrollabr.mouseenter",mkScrollFn );
    if (opts.isHide) {
        if(b1.length > 0){
            $B.DomUtils.hide(b1);
        }
        if(b2.length >0){
            $B.DomUtils.hide(b2);
        } 
        $B.DomUtils.addListener($wrapScroll,"myscrollabr.mouseleave", function (e) {
            var $s = this;
            var $c = $B.DomUtils.children($s,".k_scrollbar_y");
            if($c.length >0 && !$B.DomUtils.propData($c,"_scring")){                
                $B.DomUtils.hide($c);
            }            
            $c = $B.DomUtils.children($s,".k_scrollbar_x");
            if($c.length >0 && !$B.DomUtils.propData($c,"_scring")){                
                $B.DomUtils.hide($c);
            }
        });
    }
    setTimeout(function(){
        $B.DomUtils.onDomNodeChanged(function(ele,isAdd){
            let isInDom = $B.DomUtils.inDom($wrap);
            if(!isInDom){ //清理这个回调吧，
                return false;
            }            
            let isInnner = false;
            if($wrap.contains){
                isInnner = $wrap.contains(ele);
            }else{
                let pel = ele.parentNode;
                while(pel){
                    if(pel === $wrap){
                        isInnner = true;
                        break;
                    }
                    pel = pel.parentNode;
                }
            }
            if(isInnner){
                if(opts.isHide){
                    let $c = $B.DomUtils.children($wrapScroll,".k_scrollbar_y");
                    if($c.length >0 && $B.DomUtils.isHide($c)){
                        return ;
                    }
                    $c = $B.DomUtils.children($wrapScroll,".k_scrollbar_x");
                    if($c.length >0 && $B.DomUtils.isHide($c)){
                        return ;
                    }
                }
                setTimeout(()=>{
                    $B.DomUtils.trigger($wrapScroll,"myscrollabr.mouseenter");
                },20);                
            }
        });  
    },200);
    return $wrap;
}
$B["myScrollbar"] = myscrollbar; 
 
 if(!window["$B"] ) {window["$B"] = $B;} 
  return window["$B"] ; 
});

/*! BUI - v2.0.0 - 2022-03-13 09:03:17 
Copyright (c): kevin.huang  https://gitee.com/kevin-huang/bui2.0.git (qq:757723114) 
Released under MIT License*/
(function ( global, factory) {
//   "use strict";
	if ( typeof module === "object" && typeof module.exports === "object" ) {
		module.exports = global.document ?
			factory( global, true ) :
			function( w ) {
				if ( !w.document ) {
					throw new Error( "Bui requires a window with a document" );
				}
				return factory( w );
			};
	} else {
		  factory( global );
	}  
})( typeof window !== "undefined" ? window : this, function( window, noGlobal ) {
//   "use strict";
  var $B = window["$B"] ?  window["$B"] : {};

var defaultOpts = {
    target: undefined, //需要重置大小的目标元素
    zoomScale: false, //是否等比例缩放
    scaleRate: 1,//缩放比例
    poitStyle: {  //8个点配置
        color: '#FF292E',
        "font-size": "8px",
        width: "8px",
        height: "8px",
        background: "#FF0000",
        "border-radius": "4px"
    },
    lineStyle: {
        "border-color": "#FF292E",
        "border-size": "1px"
    },
    onDragReady: undefined,
    dragStart: undefined,
    onDrag: undefined,
    dragEnd: undefined
};
class Resize extends $B.BaseControl {
    constructor(opts) {
        super();
        this.elObjArray = [];
        this.opts = $B.extendObjectFn(true, {}, defaultOpts, opts);
        this.target = this.opts.target;
        this.id = $B.getUUID();
        var _this = this;
        var movingTarget,
            mvFlag,
            zoomScaleUpdateSet = {},
            tgsize,
            tgradio,
            tgpos;
        var onUpdateFn = this.opts.onUpdateFn;
        var onBeforeUpdateFn = this.opts.onBeforeUpdateFn;

        function _dragStart(args) {            
            if(args.shiftKey){
                _this.opts.zoomScale = true;
            }
            var state = args.state;
            movingTarget = state.target;
            mvFlag = movingTarget["resizeData"];
            state._data.mvFlag = mvFlag;
            if (mvFlag._type) {
                /*** 记录点大小 *****/
                mvFlag.size2d = $B.DomUtils.width(movingTarget) / 2;
            }
            tgsize = {
                height: $B.DomUtils.outerHeight(_this.target),
                width: $B.DomUtils.outerWidth(_this.target)
            };
            tgradio = tgsize.width / tgsize.height;
            tgpos = $B.DomUtils.position(_this.target);
            zoomScaleUpdateSet = {};
            zoomScaleUpdateSet["line1"] = {
                width: $B.DomUtils.outerWidth(_this.line1),
                position: $B.DomUtils.position(_this.line1)
            };
            zoomScaleUpdateSet["line2"] = {
                height: $B.DomUtils.outerHeight(_this.line2),
                position: $B.DomUtils.position(_this.line2)
            };
            zoomScaleUpdateSet["line3"] = {
                width: $B.DomUtils.outerWidth(_this.line3),
                position: $B.DomUtils.position(_this.line3)
            };
            zoomScaleUpdateSet["line4"] = {
                height: $B.DomUtils.outerHeight(_this.line4),
                position: $B.DomUtils.position(_this.line4)
            };
            zoomScaleUpdateSet["point0"] = {
                position: $B.DomUtils.position(_this.poitArr[0])
            };
            zoomScaleUpdateSet["point1"] = {
                position: $B.DomUtils.position(_this.poitArr[1])
            };
            zoomScaleUpdateSet["point2"] = {
                position: $B.DomUtils.position(_this.poitArr[2])
            };
            zoomScaleUpdateSet["point3"] = {
                position: $B.DomUtils.position(_this.poitArr[3])
            };
            _this.zoomScaleUpdateSet = zoomScaleUpdateSet;
            if (_this.opts.dragStart) {
                _this.opts.dragStart.call(movingTarget, args);
            }           
        }
        function _zoomUpated(state, _index, _type, update) {
            var movData = state._data;
            var leftOffset = movData.leftOffset;
            var topOffset = movData.topOffset;
            var leftOfsRate, topOfsRate;
            var line1Data = zoomScaleUpdateSet["line1"];
            var line2Data = zoomScaleUpdateSet["line2"];
            var line3Data = zoomScaleUpdateSet["line3"];
            var line4Data = zoomScaleUpdateSet["line4"];
            var targetCss = {};
            if (_type) { //4个点
                if (_this.opts.zoomScale) {
                    if (_index === 0 || _index === 2) {
                        topOffset = leftOffset / tgradio;
                        movData.top = movData.startTop + topOffset;
                    } else {
                        topOffset = -(leftOffset / tgradio);
                        movData.top = movData.startTop + topOffset;
                    }
                }
                leftOfsRate = leftOffset / _this.opts.scaleRate;
                topOfsRate = topOffset / _this.opts.scaleRate;
                var line1Css = {},
                    line2Css = {},
                    line3Css = {},
                    line4Css = {},
                    otherOfs, w, otherOfsRate;
                if (_index === 0) {
                    line1Css["top"] = line1Data.position.top + topOffset;
                    line1Css["left"] = line1Data.position.left + leftOffset;
                    line1Css["width"] = line1Data.width - leftOffset;
                    _this.line1.style.top = line1Css["top"] + "px";
                    _this.line1.style.left = line1Css["left"] + "px";
                    _this.line1.style.width = line1Css["width"]  + "px";
                   
                    line2Css["top"] = line2Data.position.top + topOffset;
                    line2Css["height"] = line2Data.height - topOffset;
                    _this.line2.style.top = line2Css["top"] + "px";
                    _this.line2.style.height = line2Css["height"] + "px";
                    

                    line3Css["left"] = line3Data.position.left + leftOffset;
                    line3Css["width"] = line3Data.width - leftOffset;
                    _this.line3.style.left = line3Css["left"] + "px";
                    _this.line3.style.width = line3Css["width"] + "px";
                    

                    line4Css["height"] = line4Data.height - topOffset;
                    line4Css["top"] = line4Data.position.top + topOffset;
                    line4Css["left"] = line4Data.position.left + leftOffset;
                    _this.line4.style.height = line4Css["height"] + "px";
                    _this.line4.style.top = line4Css["top"] + "px";
                    _this.line4.style.left = line4Css["left"]  + "px";
                    
                    _this._updatePoitPosition(0, topOffset, 1);
                    _this._updatePoitPosition(leftOffset, 0, 3);
                    targetCss["width"] = tgsize.width - leftOfsRate;
                    targetCss["height"] = tgsize.height - topOfsRate;
                    targetCss["top"] = tgpos.top + topOffset;
                    targetCss["left"] = tgpos.left + leftOffset;
                } else if (_index === 1) {
                    line1Css["top"] = line1Data.position.top + topOffset;
                    line1Css["width"] = line1Data.width + leftOffset;
                    _this.line1.style.height = line1Css["top"] + "px";
                    _this.line1.style.width = line1Css["width"] + "px";                   

                    line2Css["top"] = line2Data.position.top + topOffset;
                    line2Css["height"] = line2Data.height - topOffset;
                    line2Css["left"] = line2Data.position.left + leftOffset;
                    _this.line2.style.top = line2Css["top"] + "px";
                    _this.line2.style.left = line2Css["left"] + "px";
                    _this.line2.style.height = line2Css["height"]  + "px";                   

                    line3Css["width"] = line3Data.width + leftOffset;
                    _this.line3.style.width = line3Css["width"]  + "px";                    

                    line4Css["top"] = line4Data.position.top + topOffset;
                    line4Css["height"] = line4Data.height - topOffset;
                    _this.line4.style.top = line4Css["top"] + "px";
                    _this.line4.style.height = line4Css["height"] + "px";                   

                    _this._updatePoitPosition(0, topOffset, 0);
                    _this._updatePoitPosition(leftOffset, 0, 2);
                    targetCss["width"] = tgsize.width + leftOfsRate;
                    targetCss["height"] = tgsize.height - topOfsRate;
                    targetCss["top"] = tgpos.top + topOffset;
                } else if (_index === 2) {
                    line1Css["width"] = line1Data.width + leftOffset;
                    _this.line1.style.width = line1Css["width"]  + "px";                    

                    line2Css["height"] = line2Data.height + topOffset;
                    line2Css["left"] = line2Data.position.left + leftOffset;
                    _this.line2.style.left = line2Css["left"] + "px";
                    _this.line2.style.height = line2Css["height"]  + "px";                    

                    line3Css["width"] = line3Data.width + leftOffset;
                    line3Css["top"] = line3Data.position.top + topOffset;
                    _this.line3.style.width = line3Css["width"]  + "px";
                    _this.line3.style.top = line3Css["top"]  + "px";                    

                    line4Css["height"] = line4Data.height + topOffset;
                    _this.line4.style.height = line4Css["height"] + "px";
                   
                    _this._updatePoitPosition(leftOffset, 0, 1);
                    _this._updatePoitPosition(0, topOffset, 3);
                    targetCss["width"] = tgsize.width + leftOfsRate;
                    targetCss["height"] = tgsize.height + topOfsRate;
                } else {
                    line1Css["left"] = line1Data.position.left + leftOffset;
                    line1Css["width"] = line1Data.width - leftOffset;
                    _this.line1.style.left = line1Css["left"] + "px";
                    _this.line1.style.width = line1Css["width"] + "px";
                   
                    line2Css["height"] = line2Data.height + topOffset;
                    _this.line2.style.height = line2Css["height"]  + "px";
                   

                    line3Css["top"] = line3Data.position.top + topOffset;
                    line3Css["left"] = line3Data.position.left + leftOffset;
                    line3Css["width"] = line3Data.width - leftOffset;
                    _this.line3.style.top = line3Css["top"] + "px";
                    _this.line3.style.left = line3Css["left"] + "px";
                    _this.line3.style.width = line3Css["width"] + "px";
                    

                    line4Css["height"] = line4Data.height + topOffset;
                    line4Css["left"] = line4Data.position.left + leftOffset;
                    _this.line4.style.height = line4Css["height"] + "px";
                    _this.line4.style.left = line4Css["left"] + "px";
                   

                    _this._updatePoitPosition(leftOffset, 0, 0);
                    _this._updatePoitPosition(0, topOffset, 2);

                    targetCss["width"] = tgsize.width - leftOfsRate;
                    targetCss["height"] = tgsize.height + topOfsRate;
                    targetCss["left"] = tgpos.left + leftOffset;
                }
            } else { //4条边
                leftOfsRate = leftOffset / _this.opts.scaleRate;
                topOfsRate = topOffset / _this.opts.scaleRate;
                if (_index === 0) {
                    otherOfs = topOffset / 2;
                    otherOfsRate = topOfsRate / 2;
                    var newTop = line2Data.position.top + topOffset;
                    line2Css = {
                        "top": newTop
                    };
                    line4Css = {
                        "top": newTop
                    };
                    // _this.line2.outerHeight(line2Data.height - topOffset);
                    // _this.line4.outerHeight(line4Data.height - topOffset);
                    $B.DomUtils.outerHeight(_this.line2,line2Data.height - topOffset);
                    $B.DomUtils.outerHeight(_this.line4,line4Data.height - topOffset);

                    var point0ShiftLeft = 0;
                    var point1ShiftLeft = 0;

                    targetCss["top"] = tgpos.top + topOffset;
                    targetCss["height"] = tgsize.height - topOfsRate;

                    if (_this.opts.zoomScale) { //等比例缩放
                        otherOfs = otherOfs * tgradio;
                        movData.left = line1Data.position.left + otherOfs;
                        w = $B.DomUtils.outerHeight() * tgradio;
                        $B.DomUtils.outerHeight(_this.line1,w);
                        $B.DomUtils.outerHeight(_this.line3,w);
                        $B.DomUtils.css(_this.line3,{left :  line3Data.position.left + otherOfs});
                        // w = _this.line2.outerHeight() * tgradio;
                        // _this.line1.outerWidth(w);
                        // _this.line3.outerWidth(w);
                        // _this.line3.css("left", line3Data.position.left + otherOfs);
                        line2Css["left"] = line2Data.position.left - otherOfs;
                        line4Css["left"] = line4Data.position.left + otherOfs;
                        point0ShiftLeft = otherOfs;
                        point1ShiftLeft = -otherOfs;
                        _this._updatePoitPosition(-otherOfs, 0, 2);
                        _this._updatePoitPosition(otherOfs, 0, 3);
                        targetCss["left"] = tgpos.left + otherOfs;
                        targetCss["width"] = tgradio * targetCss['height'];
                        line1Css = {
                            left: line1Data.position.left + otherOfs
                        };
                        //_this.line1.css(line1Css);
                        $B.DomUtils.css(_this.line1,line1Css);
                    }
                    _this._updatePoitPosition(point0ShiftLeft, topOffset, 0);
                    _this._updatePoitPosition(point1ShiftLeft, topOffset, 1);
                    $B.DomUtils.css(_this.line2,line2Css);
                    $B.DomUtils.css(_this.line4,line4Css);
                    // _this.line2.css(line2Css);
                    // _this.line4.css(line4Css);
                } else if (_index === 1) {
                    otherOfs = leftOffset / 2;
                    w = line1Data.width + leftOffset;
                    line1Css = {
                        width: w
                    };
                    line2Css = {
                        left: line1Data.position.left + leftOffset
                    };
                    line3Css = {
                        width: w
                    };
                    targetCss["width"] = tgsize.width + leftOfsRate;
                    var p1TopOffset = topOffset;
                    var p2TopOffset = topOffset;
                    if (_this.opts.zoomScale) { //等比例缩放
                        otherOfs = otherOfs / tgradio;
                        movData.top = line2Data.position.top - otherOfs;
                        line2Css["height"] = (line3Data.width + leftOffset) / tgradio;
                        line2Css["top"] = line2Data.position.top - otherOfs;
                        p1TopOffset = -otherOfs;
                        p2TopOffset = otherOfs;
                        _this._updatePoitPosition(0, p1TopOffset, 0);
                        _this._updatePoitPosition(0, p2TopOffset, 3);
                        $B.DomUtils.css(_this.line4,{height: line2Css["height"], top: line4Data.position.top - otherOfs});
                        $B.DomUtils.css(_this.line1,{top: line1Data.position.top - otherOfs});
                        $B.DomUtils.css(_this.line3,{top: line3Data.position.top + otherOfs});
                       
                        targetCss["height"] = targetCss["width"] / tgradio;
                        targetCss["top"] = tgpos.top - otherOfs;
                    }
                    _this._updatePoitPosition(leftOffset, p1TopOffset, 1);
                    _this._updatePoitPosition(leftOffset, p2TopOffset, 2);
                    $B.DomUtils.css(_this.line1,line1Css);
                    $B.DomUtils.css(_this.line2,line2Css);
                    $B.DomUtils.css(_this.line3,line3Css);                    
                } else if (_index === 2) {
                    otherOfs = topOffset / 2;
                    var p2LeftOffset = 0;
                    var p3LeftOffset = 0;
                    var h = line2Data.height + topOffset;
                    line2Css = {
                        height: h
                    };
                    line4Css = {
                        height: h
                    };
                    line3Css = {
                        top: line3Data.position.top + topOffset
                    };
                    targetCss["height"] = tgsize.height + topOfsRate;
                    if (_this.opts.zoomScale) { //等比例缩放
                        otherOfs = otherOfs * tgradio;
                        movData.left = line3Data.position.left - otherOfs;
                        line3Css["width"] = line4Css.height * tgradio;
                        line3Css["left"] = line3Data.position.left - otherOfs;
                        line1Css = {
                            "width": line3Css["width"],
                            left: line1Data.position.left - otherOfs
                        };
                        $B.DomUtils.css(_this.line1,line1Css);                       
                        line2Css["left"] = line2Data.position.left + otherOfs;
                        line4Css["left"] = line4Data.position.left - otherOfs;
                        p2LeftOffset = otherOfs;
                        p3LeftOffset = -otherOfs;
                        _this._updatePoitPosition(-otherOfs, 0, 0);
                        _this._updatePoitPosition(otherOfs, 0, 1);
                        targetCss["width"] = tgradio * targetCss['height'];
                        targetCss["left"] = tgpos.left - otherOfs;
                    }
                    $B.DomUtils.css(_this.line4,line4Css);
                    $B.DomUtils.css(_this.line2,line2Css);
                    $B.DomUtils.css(_this.line3,line3Css);                   
                    _this._updatePoitPosition(p2LeftOffset, topOffset, 2);
                    _this._updatePoitPosition(p3LeftOffset, topOffset, 3);
                } else {
                    otherOfs = leftOffset / 2;
                    line1Css = {
                        width: line1Data.width - leftOffset,
                        left: line1Data.position.left + leftOffset
                    },
                        line3Css = {
                            width: line1Css.width,
                            left: line1Css.left
                        },
                        line4Css = {
                            left: line4Data.left - leftOffset
                        };
                    targetCss["width"] = tgsize.width - leftOfsRate;
                    targetCss["left"] = tgpos.left + leftOffset;
                    var point0TopOffset = 0;
                    var point3TopOffset = 0;
                    if (_this.opts.zoomScale) { //等比例缩放
                        otherOfs = otherOfs / tgradio;
                        point0TopOffset = otherOfs;
                        point3TopOffset = -otherOfs;
                        _this._updatePoitPosition(0, point0TopOffset, 1);
                        _this._updatePoitPosition(0, point3TopOffset, 2);
                        movData.top = line4Data.position.top + otherOfs;
                        line4Css["height"] = (line1Data.width - leftOffset) / tgradio;
                        line4Css["top"] = line4Data.position.top + otherOfs;
                        line2Css = {
                            height: line4Css["height"],
                            top: movData.top
                        };
                        _this.line2.css(line2Css);
                        line1Css["top"] = line1Data.position.top + otherOfs;
                        line3Css["top"] = line3Data.position.top - otherOfs;
                        targetCss["height"] = targetCss["width"] / tgradio;
                        targetCss["top"] = tgpos.top + otherOfs;
                    }                    
                    $B.DomUtils.css(_this.line4,line4Css);
                    $B.DomUtils.css(_this.line1,line1Css);
                    $B.DomUtils.css(_this.line3,line3Css);
                    _this._updatePoitPosition(leftOffset, point0TopOffset, 0);
                    _this._updatePoitPosition(leftOffset, point3TopOffset, 3);
                }
            }
            if (_this.target) {
                if (update) {
                    if (onBeforeUpdateFn) {
                        onBeforeUpdateFn(targetCss);
                    }
                    if (_this.opts.scaleRate) {
                        if (typeof targetCss.top !== "undefined") {
                            targetCss.top = targetCss.top / _this.opts.scaleRate;
                        }
                        if (typeof targetCss.left !== "undefined") {
                            targetCss.left = targetCss.left / _this.opts.scaleRate;
                        }
                    }                    
                    $B.DomUtils.css(_this.target,targetCss);
                    if (onUpdateFn) {
                        onUpdateFn(_this.target, movData);
                    }
                }
            } else {
                console.log("_this.target.css(targetCss); is null");
            }
        }
        function _onDrag(args) {
            var state = args.state;
            var _idx = mvFlag.index;
            var _type = mvFlag._type;
            var update = true;
            if (_this.opts.onDrag) {
                var ret = _this.opts.onDrag.call(movingTarget, args);
                if (typeof ret !== "undefined") {
                    update = ret;
                }
            }
            if (update) {
                _zoomUpated(state, _idx, _type, update);
            }
        }
        function _dragEnd(args) {
            if (_this.opts.dragEnd) {
                _this.opts.dragEnd.call(movingTarget, args);
            }
            movingTarget = undefined;
            mvFlag = undefined;
        }
        var dragOpt = {
            nameSpace: 'dragrezie', //命名空间，一个对象可以绑定多种拖动方式
            which: 1, //鼠标键码，是否左键1,右键3 才能触发拖动，默认左右键均可
            cursor: 'move',
            axis: undefined, // v or h  水平还是垂直方向拖动 ，默认全向
            onStartDrag: _dragStart,
            onDrag: _onDrag,
            onStopDrag: _dragEnd
        };
        var $body = $B.getBody();
        this.line1 = $B.DomUtils.createEl("<div style='cursor:s-resize;height:3px;position:absolute;border-top:1px solid;display:none;z-index:2147483647' _id='k_resize_line_0' class='k_resize_element k_box_size k_resize_line_0'></div>");
        this.line2 = $B.DomUtils.createEl("<div style='cursor:w-resize;width:3px;position:absolute;border-right:1px solid;display:none;z-index:2147483647' _id='k_resize_line_1' class='k_resize_element k_box_size k_resize_line_1'></div>");
        this.line3 = $B.DomUtils.createEl("<div style='cursor:s-resize;height:3px;position:absolute;border-bottom:1px solid;display:none;z-index:2147483647' _id='k_resize_line_2' class='k_resize_element k_box_size k_resize_line_2'></div>");
        this.line4 = $B.DomUtils.createEl("<div style='cursor:w-resize;width:3px;position:absolute;border-left:1px solid;display:none;z-index:2147483647'_id='k_resize_line_3'  class='k_resize_element k_box_size k_resize_line_3'></div>");
        $B.DomUtils.append($body,this.line1);
        $B.DomUtils.append($body,this.line2);
        $B.DomUtils.append($body,this.line3);
        $B.DomUtils.append($body,this.line4);
        this.elObjArray.push(this.line1);
        this.elObjArray.push(this.line2);
        this.elObjArray.push(this.line3);
        this.elObjArray.push(this.line4);
        dragOpt["cursor"] = "s-resize";
        dragOpt["axis"] = "v";
        $B.DomUtils.css(this.line1,this.opts.lineStyle);
        this.line1["resizeData"] = {
            _type: 0,
            index: 0
        };
        $B.draggable(this.line1,dragOpt);
        dragOpt["cursor"] = "w-resize";
        dragOpt["axis"] = "h";
        $B.DomUtils.css(this.line2,this.opts.lineStyle);
        this.line2["resizeData"] = {
            _type: 0,
            index: 1
        };
        $B.draggable(this.line2,dragOpt);

        dragOpt["cursor"] = "s-resize";
        dragOpt["axis"] = "v";

        $B.DomUtils.css(this.line3,this.opts.lineStyle);
        this.line3["resizeData"] = {
            _type: 0,
            index: 2
        };
        $B.draggable(this.line3,dragOpt);
        dragOpt["cursor"] = "w-resize";
        dragOpt["axis"] = "h";
        $B.DomUtils.css(this.line4,this.opts.lineStyle);
        this.line4["resizeData"] = {
            _type: 0,
            index: 3
        };
        $B.draggable(this.line4,dragOpt);
        this._fixLineStyle();
        this.poitArr = {};
        var i = 0;
        //var clzIcon = this.opts.poitStyle.icon;        
        var cursor;
        dragOpt["axis"] = undefined;
        var poitCss = {
            width: "8px",
            height: "8px",
            background: "#FF0000",
            "border-radius": "4px"
        };
        while (i < 4) {
            if (i === 0) {
                cursor = "se-resize";
            } else if (i === 1) {
                cursor = "ne-resize";
            } else if (i === 2) {
                cursor = "se-resize";
            } else {
                cursor = "ne-resize";
            }
            dragOpt["cursor"] = cursor;
            let piontHtml = "<div style='display:none;position:absolute;z-index:2147483647;cursor:" + cursor + "' class='k_resize_element k_resize_element_point k_box_size k_resize_point_" + i + "' _id='k_resize_point_" + i + "'></div>";
            let tmp = $B.DomUtils.createEl(piontHtml);
            $B.DomUtils.append($body,tmp);
            this.elObjArray.push(tmp);
            $B.DomUtils.css(tmp,poitCss);
            $B.DomUtils.css($B.DomUtils.children(tmp),this.opts.poitStyle);
            $B.draggable(tmp,dragOpt);
            tmp["resizeData"] = { _type: 1, index: i};
            this.poitArr[i] = tmp;
            i = ++i;
        }        
        if (this.target) {
            this.bind(this.target);
        }
    }
    _fixLineStyle() {
        $B.DomUtils.css(this.line1,{
            "border-left": "none",
            "border-right": "none",
            "border-bottom": "none"
        });
        $B.DomUtils.css(this.line2,{
            "border-left": "none",
            "border-top": "none",
            "border-bottom": "none"
        });
        $B.DomUtils.css(this.line3,{
            "border-left": "none",
            "border-right": "none",
            "border-top": "none"
        });
        $B.DomUtils.css(this.line4,{
            "border-top": "none",
            "border-right": "none",
            "border-bottom": "none"
        });        
    }
    setStyle(pointStyle, lineStyle) {
        $B.DomUtils.css(this.line4,lineStyle);
        $B.DomUtils.css(this.line3,lineStyle);
        $B.DomUtils.css(this.line2,lineStyle);
        $B.DomUtils.css(this.line1,lineStyle);
        this._fixLineStyle();
        var _this = this;
        Object.keys(this.poitArr).forEach(function (key) {
            $B.DomUtils.css(_this.poitArr[key],pointStyle);
        });
    }
    zoomScale(zoom) {
        this.opts.zoomScale = zoom;
    }
    setTarget(target) {
        if (!this.target || this.target!== target) {
            this.target = target;
        }
    }
    bind(target) {
       // console.log("bind target >>>>>>");
        this.setTarget(target);       
        var ofs = $B.DomUtils.offset(target);
        var size = {
            width: $B.DomUtils.outerWidth( target),
            height:$B.DomUtils.outerHeight( target)
        };
        if (this.opts.onBinding) { //用于外部设置缩放后的大小调整
            this.opts.onBinding(size, this.opts);
        }       
        $B.DomUtils.css(this.line1,{
            top: (ofs.top - 1) + "px",
            left: ofs.left + "px",
            width:size.width
        });      
        $B.DomUtils.css(this.line2,{
            top: ofs.top + "px",
            left: (ofs.left + size.width - 2) + "px",
            height: size.height
        });
        $B.DomUtils.css(this.line3,{
            top: (ofs.top + size.height - 2) + "px",
            left: ofs.left + "px",
            width: size.width
        });
        $B.DomUtils.css(this.line4,{
            top: ofs.top + "px",
            left: ofs.left - 1 + "px",
            height: size.height
        });
        this.show();
        this._initPoitPosition();
        return this;
    }
    _updatePoitPosition(leftOffset, topOffset, updateKey) {
        var point = this.poitArr[updateKey];
        var poitData = this.zoomScaleUpdateSet["point" + updateKey];        
        $B.DomUtils.css(point,{
            top: poitData.position.top + topOffset,
            left: poitData.position.left + leftOffset
        });
    }
    _initPoitPosition() {
        var poitKesy = Object.keys(this.poitArr);
        if (poitKesy.length > 0) {
            var line1Data = {
                width: $B.DomUtils.outerWidth( this.line1),
                position: $B.DomUtils.position( this.line1)
            };
            var line2Data = {
                height: $B.DomUtils.outerHeight(this.line2),
                position: $B.DomUtils.position( this.line2)
            };
            var line3Data = {
                width:  $B.DomUtils.outerWidth( this.line3),
                position: $B.DomUtils.position( this.line3)
            };
            var _this = this;
            var poitW,poitH;
            poitKesy.forEach(function (idx) {
                var key = parseInt(idx);
                var poit = _this.poitArr[key];
                if(!poitW){
                    poitW = $B.DomUtils.width(poit) /2;
                    poitH = $B.DomUtils.height(poit) /2;
                }               
                var _pos;
                if (key === 0) {
                    _pos = {
                        top: (line1Data.position.top - poitH) + "px",
                        left: (line1Data.position.left - poitW) + "px"
                    };
                } else if (key === 1) {
                    _pos = {
                        top: (line1Data.position.top - poitH) + "px",
                        left: (line2Data.position.left - poitW / 2) + "px"
                    };
                } else if (key === 2) {
                    _pos = {
                        top: (line3Data.position.top - poitH / 2) + "px",
                        left: (line2Data.position.left - poitW / 2) + "px"
                    };
                } else if (key === 3) {
                    _pos = {
                        top: (line3Data.position.top - poitH / 2) + "px",
                        left: (line3Data.position.left - poitW) + "px"
                    };
                }
                $B.DomUtils.css(poit,_pos);
            });
        }
    }
    _drag(flag, opt) {
        var _this = this;
        if (flag === "line") {
            $B.draggable(this.line1,opt,"dragrezie");
            $B.draggable(this.line2,opt,"dragrezie");
            $B.draggable(this.line3,opt,"dragrezie");
            $B.draggable(this.line4,opt,"dragrezie");
        } else if (flag === "point") {
            Object.keys(this.poitArr).forEach(function (idx) {
                var key = parseInt(idx);
                var poit = _this.poitArr[key];               
                $B.draggable(poit,opt,"dragrezie");
            });
        } else if (flag === "right") {           
            $B.draggable(this.line2,opt,"dragrezie");
            $B.draggable(this.line3,opt,"dragrezie");
            $B.draggable(this.poitArr[1],opt,"dragrezie");
            $B.draggable(this.poitArr[2],opt,"dragrezie");
        } else if (flag === "left") {           
            $B.draggable(this.line1,opt,"dragrezie");
            $B.draggable(this.line4,opt,"dragrezie");
            $B.draggable(this.poitArr[3],opt,"dragrezie");
            $B.draggable(this.poitArr[0],opt,"dragrezie");

        } else if (flag === 'LRLine') { //禁用除上下线的所有点和线           
            $B.draggable(this.line2,opt,"dragrezie");
            $B.draggable(this.line4,opt,"dragrezie");
            Object.keys(this.poitArr).forEach(function (idx) {
                var key = parseInt(idx);
                var poit = _this.poitArr[key];               
               $B.draggable(poit,opt,"dragrezie");
            });
        } else {           
            $B.draggable(this.line1,opt,"dragrezie");
            $B.draggable(this.line2,opt,"dragrezie");
            $B.draggable(this.line3,opt,"dragrezie");
            $B.draggable(this.line4,opt,"dragrezie");
            Object.keys(this.poitArr).forEach(function (idx) {
                var key = parseInt(idx);
                var poit = _this.poitArr[key];               
               $B.draggable(poit,opt,"dragrezie");
            });
        }
    }
    /**
         * 禁用拖动
         * flag[line/point/left/right] 不传值则禁用所有
         * line:禁用线
         * point:禁用点
         * left：禁用上边线，左边线，左边点
         * right:禁用下边线，右边线，右边点
         * **/
    disable(flag) {
        this._drag(flag, "disable");
    }
    /**
     * 启用拖动 
     * flag[line/point/left/right] 不传值则启用所有
     * line:启用线
     * point:启用点
     * left：启用上边线，左边线，左边点
     * right:启用下边线，右边线，右边点   
     * **/
    enable(flag) {
        this._drag(flag, "enable");
    }
    unbind() {
        this.target = undefined;
        this.hide();
        return this;
    }
    show(target) {
        //旋转后，不允许再resize
        if (target) {
            this.bind(target);
            var rotate = $B.DomUtils.attribute( target,"rotate");
            if (rotate && (rotate === "90" || rotate === "270")) {
                this.hide();
                return;
            }
        } else {
            for(let i =0 ; i < this.elObjArray.length ;i++){
                $B.DomUtils.show(this.elObjArray[i]);
            }   
        }
        return this;
    }
    hide(target) {
        if (!target || (this.target && target && target === this.target)) {
            for(let i =0 ; i < this.elObjArray.length ;i++){
                $B.DomUtils.hide(this.elObjArray[i]);
            }           
        }
        return this;
    }
    isHide() {
        return this.line1.style.display === "none";
    }
    isShow() {
        return  this.line1.style.display !== "none";
    }
    destroy(isForce) {
        for(let i =0 ; i < this.elObjArray.length ;i++){
            $B.DomUtils.remove(this.elObjArray[i]);
        }
        this.elObjArray = undefined;
        super.destroy();
    }
}
$B["Resize"] = Resize; 
 if(!window["$B"] ) {window["$B"] = $B;} 
  return window["$B"] ; 
});

/*! BUI - v2.0.0 - 2022-03-13 09:03:17 
Copyright (c): kevin.huang  https://gitee.com/kevin-huang/bui2.0.git (qq:757723114) 
Released under MIT License*/
(function ( global, factory) {
//   "use strict";
	if ( typeof module === "object" && typeof module.exports === "object" ) {
		module.exports = global.document ?
			factory( global, true ) :
			function( w ) {
				if ( !w.document ) {
					throw new Error( "Bui requires a window with a document" );
				}
				return factory( w );
			};
	} else {
		  factory( global );
	}  
})( typeof window !== "undefined" ? window : this, function( window, noGlobal ) {
//   "use strict";
  var $B = window["$B"] ?  window["$B"] : {};

var btnHtml = "<button  style='{style}' cmd='{cmd}' id='{id}' class='k_toolbar_button k_box_size btn k_toolbar_button_{cls}'><span>{text}</span></button>";
var disabledCls = "k_toolbar_button_disabled";
var defaultOpts = {
    params: undefined, //用于集成到tree datagrid时 行按钮的数据参数
    methodsObject: 'methodsObject', //事件集合对象
    align: 'left', //对齐方式，默认是left 、center、right
    style: 'normal', // plain / min  / normal /  big
    showText: true, // min 类型可以设置是否显示文字
    subMenuPosition: 'auto', //二级菜单的位置，auto/bottom 两种位置支持
    buttons: [] //请参考buttons
};
class Toolbar extends $B.BaseControl {
    constructor(elObj, opts) {
        super();
        super.setElObj(elObj);
        $B.DomUtils.addClass(this.elObj, "k_toolbar_main k_disabled_selected");
        this.buttonWrap = $B.DomUtils.createEl("<div></div>");
        $B.DomUtils.addClass(this.buttonWrap, "_button_wrap_");
        $B.DomUtils.append(this.elObj, this.buttonWrap);
        this.opts = $B.extendObjectFn(true, {}, defaultOpts, opts);
        if (this.opts.align === 'center') {
            $B.DomUtils.css(this.elObj, { "text-align": "center" });
            $B.DomUtils.css(this.buttonWrap, { "width": "100%" });
        }else if(this.opts.align !== "100%"){
            $B.DomUtils.css(this.buttonWrap, { "float": this.opts.align });
            $B.DomUtils.addClass(this.elObj, "clearfix");
        }
        if( this.opts.align === "100%"){
            this.widthRadio = 100 / this.opts.buttons.length;
        }       
        this.isGroup = Array.isArray(this.opts.buttons[0]);
        for (let i = 0, l = this.opts.buttons.length; i < l; ++i) {
            let opt = this.opts.buttons[i];
            if (this.isGroup) {
                let lastBtn;
                for (let j = 0, jlen = opt.length; j < jlen; ++j) {
                    lastBtn = this._createButtonByopt(opt[j], true, this.opts.style, this.buttonWrap).btn;
                }
                if (i !== l - 1) {
                    $B.DomUtils.css(lastBtn, { "border-right": "1px solid #C1C1C1" });
                }
            } else {
                this._createButtonByopt(opt, false, this.opts.style, this.buttonWrap);
            }
        }
        this.bindEvents(this.buttonWrap);
    }
    bindEvents(eventWrap) {
        $B.DomUtils.click(eventWrap, (e) => {
            var target = e.target;
            if (!$B.DomUtils.hasClass(target, "_button_wrap_")) {
                let nodeName = target.nodeName;
                let el = target;
                if (nodeName !== "BUTTON") {
                    el = target.parentNode;
                }
                if ($B.DomUtils.hasClass(el, disabledCls)) {//禁用状态
                    return;
                }
                if ($B.DomUtils.hasClass(el.parentNode, "k_context_menu_container")) {
                    setTimeout(() => {
                        this.hideSubMenuOnTimer(el.parentNode, 1);
                    });
                }
                this._fireEvent(el);
                return false;//阻止冒泡             
            }
        });
    }
    _getOptById(id) {
        var opt = this._loopGetOptById(id, this.opts.buttons);
        return opt;
    }
    _loopGetOptById(id, buttons) {
        var opt, childrens = [];
        for (let i = 0; i < buttons.length; i++) {
            if (Array.isArray(buttons[i])) {
                opt = this._loopGetOptById(id, buttons[i]);
                if (opt) {
                    break;
                }
            } else {
                if (buttons[i].id === id) {
                    opt = buttons[i];
                    break;
                }
                if (buttons[i].children) {
                    for (let j = 0; j < buttons[i].children.length; j++) {
                        childrens.push(buttons[i].children[j]);
                    }
                }
            }
        }
        if (!opt && childrens.length > 0) {
            opt = this._loopGetOptById(id, childrens);
        }
        return opt;
    }
    _fireEvent(btn) {
        var opt = this._getOptById(btn.id);
        if (opt && opt.click) {
            if ($B.isFunctionFn(opt.click)) {
                opt.click.call(btn, this.opts.params, opt);
            } else if (this.opts.methodsObject) {
                let mOBJ = window[this.opts.methodsObject];
                if (mOBJ) {
                    var fn = mOBJ[opt.click];
                    if ($B.isFunctionFn(fn)) {
                        fn.call(btn, this.opts.params, opt);
                    }
                }
            }
        }
    }
    _createButtonByopt(opt, isGroup, style, buttonWrap) {
        var created = true;
        if (typeof opt.visualable !== 'undefined') {
            created = opt.visualable;
        }
        if (created) {
            return this.createButton(opt, opt.text, isGroup, style, buttonWrap);
        }
    }
    createButton(opt, btnTxt, isGroup, style, buttonWrap) {
        var showText = this.opts.showText;
        if (typeof opt.showText !== "undefined") {
            showText = opt.showText;
        }
        var _this = this,
            bgColor = "",
            fontColor,
            $icon,
            $btn,
            $txtSpan,
            iconColor,
            txt = showText ? btnTxt : '';
        if (!opt.id || opt.id === "") { //生成一个id
            opt.id = $B.getUUID();
        }
        if(this.opts.style === "plain" && opt.color && !opt.fontColor){
            opt.fontColor = opt.color;
        }
        var isIconBtn = false;
        if (typeof showText !== 'undefined' && !showText) {
            if (!opt.iconCls || opt.iconCls === "") {
                opt.iconCls = "fa-mouse-pointer";
            }
            isIconBtn = true;
        }
        var isSubMenu = $B.DomUtils.hasClass(buttonWrap, "k_context_menu_container");
        var isGlobalColor = this.opts.color && this.opts.color !== "";
        var isSelftColor = opt.color && opt.color !== "";
        var isDiyBgColor = isGlobalColor || isSelftColor;
        //自定义背景色
        if (isDiyBgColor) {
            if (isSelftColor) { //优先按钮自身的配置
                bgColor = "background-color:" + opt.color + ";";
            } else {
                bgColor = "background-color:" + this.opts.color + ";";
            }
        }
        //自定义文本/按钮颜色
        var isGllobalFontColor = this.opts.fontColor && this.opts.fontColor !== "";
        var isSelftFontColor = opt.fontColor && opt.fontColor !== "";
        var isDiyFontColor = isGllobalFontColor || isSelftFontColor;
        if (isDiyFontColor) {
            if (isSelftFontColor) {
                fontColor = { "color": opt.fontColor };
            } else {
                fontColor = { "color": this.opts.fontColor };
            }
        }else if(!showText){ 
            if(this.opts.style === "plain" && opt.color){
                fontColor = { "color": opt.color };
                isDiyFontColor = true;
            }
        }
        var html = btnHtml.replace("{cmd}", opt.cmd).replace("{id}", opt.id).replace("{cls}", style).replace("{text}", txt).replace("{style}", bgColor);
        if (this.opts.params) {
            delete this.opts.params.Toolbar;
        }
        $btn = $B.DomUtils.createEl(html);
        if(this.widthRadio){
            $btn.style.width = this.widthRadio + "%";
            $btn.style.padding = "0px 0px";
            $btn.style.margin = "0px 0px";
            $btn.style.borderRadius = "0px";
        }
        $txtSpan = $B.DomUtils.children($btn, "span");
        if (isDiyFontColor) {
            $B.DomUtils.css($txtSpan, fontColor);
        }
        let nowrapCss = { "white-space": "nowrap" };
        $B.DomUtils.css($btn, nowrapCss);
        $B.DomUtils.css($txtSpan, nowrapCss);
        let canBorder = true;
        if (isSubMenu && !isIconBtn) {
            canBorder = false;
        }
        if (canBorder) {
            if (opt.border) {
                $B.DomUtils.css($btn, { "border": opt.border });
            } else if (this.opts.border) {
                $B.DomUtils.css($btn, { "border": this.opts.border });
            }
        }
        if (opt.radius) {
            $B.DomUtils.css($btn, { "border-radius": opt.radius });
        } else if (this.opts.radius) {
            $B.DomUtils.css($btn, { "border-radius": this.opts.radius });
        }
        if (isGroup) {
            $B.DomUtils.css($btn, { "margin-right": 0, "border-radius": 0, "-moz-border-radius": 0, "-webkit-border-radius": 0 });
        }
        if (opt.disabled) {
            $B.DomUtils.addClass($btn, disabledCls);
        }
        if (txt === "") {
            $B.DomUtils.attribute($btn, { "title": opt.text });
        }
        if (opt.iconCls && opt.iconCls !== '') {
            let isPlain = _this.opts.style === 'plain';
            if (isPlain) {
                $B.DomUtils.attribute($btn, { "title": btnTxt });
                $B.DomUtils.css($btn, { "background": "none" });
            }
            let fs = "";
            if (_this.opts.fontSize) {
                fs = "style='font-size:" + _this.opts.fontSize + "px'";
            }
            if (opt.childrens && opt.childrens.length > 0) {
                $B.DomUtils.append($btn, '<i style="padding-left:4px" ' + fs + ' class="fa ' + opt.iconCls + '"></i>&nbsp');
            } else {
                $B.DomUtils.prepend($btn, '<i ' + fs + ' class="fa ' + opt.iconCls + '"></i>&nbsp');
            }
            iconColor = fontColor; //与字体颜色一致
            if (opt.iconColor) {
                iconColor = { "color": opt.iconColor };
            }
            $icon = $B.DomUtils.children($btn, "i");
            if (iconColor) {
                $B.DomUtils.css($icon, iconColor);
            }
            if (isIconBtn) {
                $B.DomUtils.css($icon, { "padding": "0px 0px" });
            } else {
                $B.DomUtils.css($txtSpan, { "padding-left": "5px" });
            }
        }  
        var notPriv = opt.params && opt.params.unAuthor ;
        if (notPriv) {
            $B.DomUtils.addClass($btn, "k_no_privilage_cls");
            $B.DomUtils.addClass($btn, disabledCls);
            $B.DomUtils.attribute($btn, {title:$B.config.unAuthor});
        }
        $B.DomUtils.append(buttonWrap, $btn);
        //当没有指定字体颜色、自动调整图标、文本颜色       
        if (!isDiyFontColor) {
            if (!isSubMenu) {
                let ret = this._autoColor($btn, $txtSpan, $icon, opt);
                iconColor = ret.iconColor;
                bgColor = ret.bgColor;
            }
        }
        this._bindSubMenuEvents($btn);
        if (opt.children) {
            var $i = $B.DomUtils.createEl("<i style='position:relative;font-size:10px;padding-right:-8px;padding-left:5px;top:0.5em;' class='fa fa-ellipsis'></i>");
            if (iconColor) {
                $B.DomUtils.css($i, iconColor);
            }
            $B.DomUtils.append($btn, $i);
        }
        return { btn: $btn, txtSpan: $txtSpan, icon: $icon };
    }
    _autoColor($btn, $txtSpan, $icon, opt) {
        let bgColor = $B.DomUtils.css($btn, "backgroundColor");
        let iconColor;
        bgColor = $B.DomUtils.css($btn, "backgroundColor");
        if ($B.isDeepColor(bgColor)) {
            iconColor = { color: "#ffffff" };
            if ($icon && !opt.iconColor) {
                $B.DomUtils.css($icon, iconColor);
            }
            $B.DomUtils.css($txtSpan, iconColor);
        } else { //应用全局的字体颜色即可
            if ($icon && !opt.iconColor) {
                this.clearFontColor($icon);
            }
            this.clearFontColor($txtSpan);
            iconColor = undefined;
        }
        return { iconColor: iconColor, bgColor: bgColor };
    }
    _createSubChildren(children, el, parentId) {
        var _this = this;
        var body = $B.getBody();
        var domInfo = $B.DomUtils.domInfo(el);
        var btnId = el.id;
        var id = this.id + "_" + btnId;
        var top = domInfo.pageTop;
        var left = domInfo.pageLeft;
        var isBottom = this.opts.subMenuPosition === "bottom";
        var minWidth = "";
        if (isBottom && !parentId) { //如果是底部位置            
            top = top + domInfo.height - 2;
            minWidth = "min-width:" + domInfo.width + "px;";
        } else {
            left = left + domInfo.width - 4;
            top = top + domInfo.height / 2;
        }
        let wrap = $B.DomUtils.children(body, "#" + id);
        if (wrap) {
            $B.DomUtils.detach(wrap);
            $B.DomUtils.css(wrap, { top: -10000, left: -10000, "min-width": domInfo.width + "px" });
            $B.DomUtils.append(body, wrap);
            $B.DomUtils.show(wrap);
            this.autoPosition(wrap, domInfo, { top: top, left: left });
            return wrap;
        }
        wrap = $B.DomUtils.createEl("<div id='" + id + "' style='top:-10000px;left:-10000px;position:absolute;padding:6px 8px;" + minWidth + "' class='k_context_menu_container _button_wrap_ k_box_shadow k_disabled_selected" + this.id + "'></div>");
        $B.DomUtils.propData(wrap, { "forbtnid": btnId });
        if (parentId) {
            $B.DomUtils.propData(wrap, { pid: parentId });
        }
        for (let i = 0; i < children.length; i++) {
            children[i].color = undefined;
            children[i].border = undefined;
            let ret = this._createButtonByopt(children[i], false, "plain", wrap); //只能是简单类型            
            let css = { "display": "block", "margin": "0px 0px 3px 0px", "min-width": "100%" };
            if (!this.opts.showText) {
                css["text-align"] = "center";
            } else {
                css["text-align"] = "left";
                if (!children[i].radius) {
                    css["border-radius"] = "0px";
                }
            }
            $B.DomUtils.css(ret.btn, css);
        }
        $B.DomUtils.append(body, wrap);
        //计算大小，合适的位置
        this.autoPosition(wrap, domInfo, { top: top, left: left });
        if (!this.subWrapEvents) {
            this.subWrapEvents = {
                mouseleave: function (e) {
                    _this.hideSubMenuOnTimer(this);
                },
                mouseenter: function () {
                    clearTimeout(_this.hideSubMenuTimer);
                    clearTimeout(_this.hideButtonSubmenuTimer);
                }
            };
        }
        $B.DomUtils.mouseleave(wrap, this.subWrapEvents.mouseleave);
        $B.DomUtils.mouseenter(wrap, this.subWrapEvents.mouseenter);
        this.bindEvents(wrap);
        return wrap;
    }
    /***自适应位置显示***/
    autoPosition(wrap, elInfo, pos) {
        let pid = $B.DomUtils.propData(wrap, "pid");
        var info = $B.DomUtils.domInfo(wrap);
        var bodyInfo = $B.DomUtils.domInfo(document.body);
        var avibleHeight = bodyInfo.height - elInfo.top;
        var avibleWidth = bodyInfo.width - elInfo.left;
        if (pid) { //非一级菜单
            avibleWidth = avibleWidth - elInfo.width;
        }
        var isBottom = this.opts.subMenuPosition === "bottom";
        if (isBottom) {
            avibleHeight = avibleHeight - elInfo.height - 2;
        } else {
            avibleHeight = avibleHeight - elInfo.height / 2;
            avibleWidth = avibleWidth - elInfo.width - 4;
        }
        var diff = avibleWidth - info.width;
        if (diff < 0) {
            if (isBottom && !pid) {
                pos.left = pos.left + diff - 5;
            } else {
                pos.left = elInfo.pageLeft - info.width;
            }
        }
        diff = avibleHeight - info.height;
        if (diff < 0) {
            pos.top = elInfo.pageTop - info.height;
        }
        $B.DomUtils.css(wrap, pos);
    }
    /***
     * timeout方式隐藏子菜单
     * ***/
    hideSubMenuOnTimer(el, timeout) {
        clearTimeout(this.hideSubMenuTimer);
        var timer = 600;
        if (typeof timeout !== "undefined") {
            timer = timeout;
        }
        this.hideSubMenuTimer = setTimeout(() => {
            let forbtnid = $B.DomUtils.propData(el, "forbtnid");
            if (this.currentActivedBtnId === forbtnid) {
                return false;
            }
            let childrens = $B.DomUtils.children(el, "button");
            for (let i = 0; i < childrens.length; i++) {
                let child = childrens[i];
                if (child._subMenu) {
                    this.hideSubMenu(child);
                }
            }
            var pid = $B.DomUtils.propData(el, "pid");
            if (pid) {
                var pWrap = $B.DomUtils.findbyId(pid);
                if (pWrap) {
                    $B.DomUtils.hide(pWrap, 200);
                }
            }
            $B.DomUtils.hide(el, 260);
        }, timer);
    }
    /**
     * 绑定子菜单按钮的事件
     * ***/
    _bindSubMenuEvents($btn) {
        if (!this.subEvents) {
            var _this = this;
            this.subEvents = {
                mouseenter: function (e) {
                    let target = e.target;
                    let nodeName = target.nodeName;
                    let el = target;
                    if (nodeName !== "BUTTON") {
                        el = target.parentNode;
                    }
                    _this.currentActivedBtnId = el.id;
                    _this.currentEnterBtnId = el.id;
                    var previousAll = $B.DomUtils.previousAll(el);
                    var nextAll = $B.DomUtils.nextAll(el);
                    if (previousAll) {
                        for (let i = 0; i < previousAll.length; i++) {
                            _this.hideSubMenu(previousAll[i]);
                        }
                    }
                    if (nextAll) {
                        for (let i = 0; i < nextAll.length; i++) {
                            _this.hideSubMenu(nextAll[i]);
                        }
                    }                  
                    let opt = _this._getOptById(el.id);
                    if (opt.children && opt.children.length > 0) {
                        var parentNode = el.parentNode;
                        var pid;
                        if ($B.DomUtils.hasClass(parentNode, 'k_context_menu_container')) {
                            pid = parentNode.id;
                        }
                        let subMenu = _this._createSubChildren(opt.children, el, pid);
                        el._subMenu = subMenu;
                    }
                },
                mouseleave: function (e) {
                    _this.currentActivedBtnId = undefined;
                    let target = e.target;
                    let nodeName = target.nodeName;
                    let el = target;
                    if (nodeName !== "BUTTON") {
                        el = target.parentNode;
                    }
                    let parentNode = el.parentNode;
                    if (_this.currentEnterBtnId === el.id && $B.DomUtils.hasClass(parentNode, 'k_context_menu_container')) {
                        _this.currentEnterBtnId = undefined;
                        return;
                    }
                    if (this._subMenu) {
                        clearTimeout(_this.hideButtonSubmenuTimer);
                        _this.hideButtonSubmenuTimer = setTimeout(() => {
                            _this.hideSubMenu(this);
                        }, 500);
                    }
                }
            };
        }
        $B.DomUtils.mouseleave($btn, this.subEvents.mouseleave);
        $B.DomUtils.mouseenter($btn, this.subEvents.mouseenter);
    }
    /**
     * 级联隐藏子菜单
     * ***/
    hideSubMenu(el) {
        if (el._subMenu) {
            //级联关闭            
            $B.DomUtils.hide(el._subMenu, 200);
            let childrens = $B.DomUtils.children(el._subMenu, "button");
            for (let i = 0; i < childrens.length; i++) {
                let child = childrens[i];
                if (child._subMenu) {
                    this.hideSubMenu(child);
                }
            }
            el._subMenu = undefined;
        }
    }
    /**
     * 清理颜色声明，恢复全局颜色
     * ***/
    clearFontColor(el) {
        let style = $B.DomUtils.attribute(el, "style");
        if (style) {
            let styleObj = $B.style2cssObj(style);
            delete styleObj.color;
            style = $B.cssObj2string(styleObj);
            $B.DomUtils.attribute(el, { "style": style });
        }
    }
    /**
    *启用按钮（可以批量启用）
    *args  btnIds=[] //按钮的id数组
    ***/
    enableButtons(args) {
        for (var i = 0, l = args.length; i < l; ++i) {
            let id = args[i];
            let el = $B.DomUtils.children(this.buttonWrap, "#" + id);
            $B.DomUtils.removeClass(el, disabledCls);
        }
    }
    /**
     *禁用按钮（可以批量禁用）
     *args  btnIds=[] //按钮的id数组
     ***/
    disableButtons(args) {
        for (var i = 0, l = args.length; i < l; ++i) {
            let id = args[i];
            let el = $B.DomUtils.children(this.buttonWrap, "#" + id);
            $B.DomUtils.addClass(el, disabledCls);
        }
    }
    /***
     *删除按钮（可以批量删除）
     *args btnIds=[] //按钮的id数组
     ***/
    delButtons(args) {
        for (let i = 0, l = args.length; i < l; ++i) {
            let id = args[i];
            let el = $B.DomUtils.children(this.buttonWrap, "#" + id);
            $B.DomUtils.remove(el);
            let ret = this._removeOpt(id, this.opts.buttons);
            if (ret.length !== this.opts.buttons.length) {
                this.opts.buttons = ret;
            }
        }
    }
    _removeDomByOpt(opt) {
        if (opt.children) {
            for (let i = 0; i < opt.children.length; i++) {
                this._removeDomByOpt(opt.children[i]);
            }
            let domId = this.id + "_" + opt.id;
            var body = $B.getBody();
            var el = $B.DomUtils.children(body, "#" + domId);
            if (el) {
                $B.DomUtils.remove(el);
            }
        }
    }
    _removeOpt(id, btnOpts) {
        var newOpts = [];
        var isFinded = false;
        var nextChilds = [];
        var optDel;
        for (let i = 0; i < btnOpts.length; i++) {
            if (id === btnOpts[i].id) {
                isFinded = true;
                optDel = btnOpts[i];
            } else {
                newOpts.push(btnOpts[i]);
            }
            if (btnOpts[i].children) {
                nextChilds.push(btnOpts[i]);
            }
        }
        if (!isFinded) {
            for (let i = 0; i < nextChilds.length; i++) {
                let ret = this._removeOpt(id, nextChilds[i].children);
                if (ret.length !== nextChilds[i].children) {
                    nextChilds[i].children = ret;
                    break;
                }
            }
        } else {
            this._removeDomByOpt(optDel);
        }
        return newOpts;
    }
    /**
     *添加按钮（可以批量添加）
     *args buttons=[]//按钮的json配置
     ***/
    addButtons(args) {
        for (var i = 0, l = args.length; i < l; ++i) {
            var opt = args[i];
            this.opts.buttons.push(opt);
            this._createButtonByopt(opt, this.isGroup, this.opts.style, this.buttonWrap);
        }
    }
    destroy(excuObjName) {
        //子菜单要关联移除
        var body = $B.getBody();
        $B.DomUtils.removeChilds(body, "." + this.id);
        super.destroy(excuObjName);
    }
}
$B["Toolbar"] = Toolbar; 
 if(!window["$B"] ) {window["$B"] = $B;} 
  return window["$B"] ; 
});

/*! BUI - v2.0.0 - 2022-03-13 09:03:17 
Copyright (c): kevin.huang  https://gitee.com/kevin-huang/bui2.0.git (qq:757723114) 
Released under MIT License*/
(function ( global, factory) {
//   "use strict";
	if ( typeof module === "object" && typeof module.exports === "object" ) {
		module.exports = global.document ?
			factory( global, true ) :
			function( w ) {
				if ( !w.document ) {
					throw new Error( "Bui requires a window with a document" );
				}
				return factory( w );
			};
	} else {
		  factory( global );
	}  
})( typeof window !== "undefined" ? window : this, function( window, noGlobal ) {
//   "use strict";
  var $B = window["$B"] ?  window["$B"] : {};

var defaultOpts = {
    data: null, //'数据'
    clickFirst: false,//创建完成后，点击第一个节点
    islistData: false,
    params: null, //远程加载时候附加的参数,可以定义再传其他字段作为参数['filed1','filed2']，默认不设置只传pid
    url: null, //请求数据的地址【如果有些请求参数是固定不变的，请去url中设置】
    textField: 'text', //菜单名称字段，默认为text
    idField: 'id', //菜单id字段,默认为id
    pidField: "pid",
    showLine: true, //显示连线
    getIdAndTxt: false, //获取勾选数据是否包括id及text
    showOptimize: 0, //当大数据量时候，采用的显示优化功能，配置0则不启用，否则每创建showOptimize条数据则执行显示
    checkSingle: false,//复选时候，不联动 父级、下级联动，默认值false    
    canClickParent: true, //点击事件时，是否可以点击父节点
    nodeParentIcon: 'k_tree_fold_closed',//父节点图标关闭状态
    nodeParentOpenIcon: 'k_tree_fold_open',      //打开状态图标
    leafNodeIcon: 'k_tree_file_icon',                //子节点图标 
    chkEmptyIcon: 'k_tree_check_empty',            //不选
    chkAllIcon: 'k_tree_check_all',            //全选
    chkSomeIcon: 'k_tree_check_some',              //部分选
    fontIconColor: undefined,//字体图标颜色定义
    checkedColor: undefined,//复选的图标颜色定义
    clickCheck: false, //是否点击复选       
    plainStyle: false, //true 为简单无图标样式
    tree2list: true,//回调api中的参数是否转为列表类型
    checkbox: false, //是否需要选择框
    disChecked: false, //是否禁用复选框 默认false
    clickItemCls: 'k_tree_clicked_cls', //点击行颜色  
    toolbarHide: undefined,//工具栏是否自动隐藏      
    toolbar: undefined, //是否需要工具栏,如果需要工具栏，则数据项中需要有toolbar工具栏组件的json
    toolAutoHide: true,
    onCreating: null,//创建每一项前回调,fn()
    onItemCreated: null, //项创建完成事件
    onClick: null, //function (data) { },//点击事件
    onLoaded: null, //加载完成事件function (data) { }
    onChecked: null, // function (data, params, checked) { } 选择事件,不建议注册onCheck事件，如果需要获取当前选择的数据，调用对象的getChecked即可
    onChange: null //function(name,newVal,oldVal)
};
//树转列表
function _tree2list(ins, resArr, beans) {
    if (!Array.isArray(beans)) {
        beans = [beans];
    }
    var nextChilds = [];
    for (let i = 0; i < beans.length; i++) {
        let bean = beans[i];
        if (bean.data) {
            resArr.push(bean.data);
        } else {
            resArr.push({ id: bean.id, text: bean.text });
        }
        if (bean.children) {
            Array.prototype.push.apply(nextChilds, bean.children);
        }
    }
    if (nextChilds.length > 0) {
        ins.optimizeTree2ListFn(ins, resArr, nextChilds);
    }
}

//列表转树形
function _list2tree(ins, res, list, pid) {
    let nextDataList = [];
    let idField = ins.opts.idField;
    let textField = ins.opts.textField;
    let pidField = ins.opts.pidField;
    for (let i = 0; i < list.length; i++) {
        let one = list[i];
        if (one[pidField] === pid) {
            res.push({
                id: one[idField],
                text: one[textField],
                data: one
            });
        } else {
            nextDataList.push(one);
        }
    }
    if (nextDataList.length > 0) {
        for (let i = 0; i < res.length; i++) {
            let one = res[i];
            let id = one[idField];
            let children = [];
            ins.optimize2treeFn(ins, children, nextDataList, id);
            if (children.length > 0) {
                one["children"] = children;
            }
        }
    }
}
function _createTreeItem(ins, ul, param, list) {
    ins.itemCounting++;
    let deep = param.deep;
    let endCount = param.endCount;
    let fragment = document.createDocumentFragment();
    if (!ins.liEl) {
        let liHtml = "<li><div event='itemEvent' style='display:inline-block;min-width:100%;white-space:nowrap;' class='k_tree_item_wrap k_box_size'><div class='k_tree_text k_box_size'></div></div></li>";
        ins.liEl = $B.DomUtils.createEl(liHtml);
    }
    if (!ins.deepEl) {
        let deepHtml = '<div class="k_tree_deep_size"></div>';
        ins.deepEl = $B.DomUtils.createEl(deepHtml);
    }
    if (!ins.childUl) {
        ins.childUl = $B.DomUtils.createEl("<ul class='k_tree_ul'>");
    }
    let nextDeep = deep + 1;
    let hasParent = false;
    let lastChild, imgCssName = "k_tree_icon_img";
    let lastIdx = list.length - 1;
    let isToolbar = Array.isArray(ins.opts.toolbar);
    let toolbarBtn, toolbarWrap;
    if (isToolbar) {
        toolbarBtn = $B.DomUtils.createEl("<div class='k_tree_button'><i class='fa'></i><span></span></div>");
        toolbarWrap = $B.DomUtils.createEl("<div class='k_tree_button_wrap'></div>");
    }
    for (let i = 0; i < list.length; i++) {
        let iconNode, checkNode, data, id, text, $li, $wrap, $txt, _deep = deep, clz, nextEndCount;
        let tmpEndCount = endCount;
        data = list[i];
        let isParent = $B.isArrayFn(data.children);
        let isChildEmpty = isParent && data.children.length === 0 ? true : false;
        id = data.id;
        text = data.text;
        if(!id){
            id = data.data[ins.opts.idField];
        }
        if(!text){
            text = data.data[ins.opts.textField];
        }
        if (isChildEmpty) {
            data.closed = true;
        }else if(typeof data.closed === "undefined"){
            if(this.openDeep && this.openDeep !== _deep){
                data.closed = true;
            }
        }  
        $li = ins.liEl.cloneNode(true);
        $wrap = $B.DomUtils.children($li, ins._divName);
        if (ins.opts.onCreating) {
            ins.opts.onCreating.call($wrap, data, isParent);
        }
        $B.DomUtils.setData($wrap, ins.dataBeanKey, data);
        $B.DomUtils.attribute($wrap, { deep: deep });
        $B.DomUtils.attribute($li, { id: "_" + id });
        ins.bindEvents($wrap);
        $txt = $B.DomUtils.findByClass($li, ins._textClz)[0];
        $B.DomUtils.text($txt, text);
        while (_deep > 0) {
            let $deepEl = ins.deepEl.cloneNode(true);
            $B.DomUtils.before($txt, $deepEl);
            _deep--;
        }
        if (!ins.opts.plainStyle) { //加入父节点文件夹图标
            iconNode = ins.deepEl.cloneNode(true);
            $B.DomUtils.attribute(iconNode, { event: 'parentEvent' });
            if (isParent) {
                if (data.closed) {
                    clz = ins.opts.nodeParentIcon + " _parent_";
                } else {
                    clz = ins.opts.nodeParentOpenIcon + " _parent_";
                }
                if (ins.isFontIcon) {
                    $B.DomUtils.addClass(iconNode, "k_tree_font_icon _parent_");
                    let $i = $B.DomUtils.append(iconNode, "<i event='parentEvent' class='fa " + clz + "'></i>");
                    if (ins.opts.fontIconColor) {
                        $B.DomUtils.css($i, { color: ins.opts.fontIconColor });
                    }
                } else {
                    $B.DomUtils.addClass(iconNode, imgCssName + " " + clz);
                }
            } else {
                if (ins.isFontIcon) {
                    $B.DomUtils.addClass(iconNode, "k_tree_font_icon");
                    let $i = $B.DomUtils.append(iconNode, "<i class='fa " + ins.opts.leafNodeIcon + "'></i>");
                    if (ins.opts.fontIconColor) {
                        $B.DomUtils.css($i, { color: ins.opts.fontIconColor });
                    }
                } else {
                    $B.DomUtils.addClass(iconNode, imgCssName + " " + ins.opts.leafNodeIcon);
                }
            }
            $B.DomUtils.before($txt, iconNode);
        }
        if (ins.opts.checkbox) {
            checkNode = ins.deepEl.cloneNode(true);
            if (ins.opts.disChecked) {
                $B.DomUtils.addClass(checkNode, "k_tree_check_disabled");
            }
            $B.DomUtils.before($txt, checkNode);
            $B.DomUtils.attribute(checkNode, { event: 'checkEvent' });
            $B.DomUtils.addClass(checkNode, "k_tree_check_box");
            let defChkIcon = ins.opts.chkEmptyIcon;
            if (data.checked) {
                defChkIcon = ins.opts.chkAllIcon;
            }
            if (ins.isFontIcon) {
                let fontNode = $B.DomUtils.append(checkNode, "<i event='checkEvent' class='fa'></i>");
                $B.DomUtils.addClass(fontNode, defChkIcon);
                if (ins.opts.fontIconColor) {
                    $B.DomUtils.css(fontNode, { color: ins.opts.fontIconColor });
                }
                if (data.checked && ins.opts.checkedColor) {
                    $B.DomUtils.css(fontNode, { color: ins.opts.checkedColor });
                }
            } else {
                $B.DomUtils.addClass(checkNode, imgCssName + " " + defChkIcon);
            }
        }
        //补充线样式
        if (ins.opts.plainStyle || ins.opts.showLine) {
            _deep = deep;
            let isFirst = i === 0;
            let isLast = i === lastIdx;
            let startNode = $txt;
            if (checkNode) {
                startNode = checkNode;
            }
            if (iconNode) {
                startNode = iconNode;
            }
            startNode = startNode.previousSibling;
            let fixListCls = 'k_tree_line_last';
            if (isParent) { //如果是父节点
                if (isFirst && isLast) {
                    if (data.closed) {
                        $B.DomUtils.addClass(startNode, imgCssName + " k_tree_line_last_closed _node_");
                    } else {
                        $B.DomUtils.addClass(startNode, imgCssName + " k_tree_line_open _node_");
                    }
                } else if (isFirst) {
                    if (data.closed) {
                        $B.DomUtils.addClass(startNode, imgCssName + " k_tree_line_closed _node_");
                    } else {
                        $B.DomUtils.addClass(startNode, imgCssName + " k_tree_line_open _node_");
                    }
                } else if (isLast) {
                    if (data.closed) {
                        $B.DomUtils.addClass(startNode, imgCssName + " k_tree_line_last_closed _node_");
                    } else {
                        $B.DomUtils.addClass(startNode, imgCssName + " k_tree_line_open _node_");
                    }
                } else {
                    if (data.closed) {
                        $B.DomUtils.addClass(startNode, imgCssName + " k_tree_line_closed _node_");
                    } else {
                        $B.DomUtils.addClass(startNode, imgCssName + " k_tree_line_open _node_");
                    }
                }
                $B.DomUtils.attribute(startNode, { "event": "parentEvent" });
                let attr = { endCount: endCount }
                if (isLast) {
                    attr["islast"] = true;
                    if (!isChildEmpty && !data.closed) {
                        tmpEndCount = 0;
                        fixListCls = 'k_tree_line_vertical';
                    }
                } else {
                    tmpEndCount = 0;
                    fixListCls = 'k_tree_line_vertical';
                }
                $B.DomUtils.attribute($wrap, attr);
            } else {//如果是子节点
                if (isLast) {
                    $B.DomUtils.addClass(startNode, imgCssName + " k_tree_line_last");                  
                } else {
                    fixListCls = "k_tree_line_vertical";
                    $B.DomUtils.addClass(startNode, imgCssName + " k_tree_line_cross");
                    tmpEndCount--;
                }
            }
            while (tmpEndCount > 0 && startNode) {
                startNode = startNode.previousSibling;
                if (startNode) {
                    $B.DomUtils.addClass(startNode, imgCssName + " " + fixListCls);
                }
                tmpEndCount--;
            }
            if (startNode) {
                startNode = startNode.previousSibling;
                while (startNode) {
                    $B.DomUtils.addClass(startNode, imgCssName + " k_tree_line_vertical");
                    startNode = startNode.previousSibling;
                }
            }
        }
        fragment.appendChild($li);
        if (isParent) {
            nextEndCount = endCount;
            let isEndNode = i === list.length - 1;
            if (isEndNode) {
                nextEndCount++;               
            } else { //末节点关系被打断
                nextEndCount = 0;
            }
            hasParent = true;
            let ul = ins.childUl.cloneNode(true);
            if (data.closed) {
                ul.style.display = "none";
            }
            $B.DomUtils.append($li, ul);
            if (data.children.length > 0) {
                let loopPrs = { deep: nextDeep, endCount: nextEndCount };
                if (isEndNode) {
                    loopPrs.parentEnd = true;
                }
                if (ins.opts.showOptimize && ins.itemCounting > ins.opts.showOptimize) {
                    if (!ins.taskStack) {
                        ins.taskStack = [];
                    }
                    let stackKey = setTimeout(() => {
                        ins.optimizeCreateFn(ins, ul, loopPrs, data.children);
                        ins.taskStack.pop();
                    }, 1);
                    ins.taskStack.push(stackKey);
                } else {
                    ins.optimizeCreateFn(ins, ul, loopPrs, data.children);
                }
            }
        } else {//记录最末端节点
            if (!lastChild && $li) {
                lastChild = $li;
            }
        }
        let tools;
        if (isToolbar) {
            tools = ins.opts.toolbar.length;
        }
        if (data.toolbar) {
            if (!toolbarBtn) {
                toolbarBtn = $B.DomUtils.createEl("<div class='k_tree_button'><i class='fa'></i><span></span></div>");
                toolbarWrap = $B.DomUtils.createEl("<div class='k_tree_button_wrap'></div>");
                ins.opts.toolbar = true;
            }
            tools = data.toolbar;
        }
        if (tools) {
            let toolWrap = toolbarWrap.cloneNode(true);
            $B.DomUtils.after($txt, toolWrap);
            let evAtrr = { "event": "toolEvent" };
            if (ins.opts.toolbarHide) {
                $B.DomUtils.css(toolWrap, { display: "none" });
            }
            for (let k = 0; k < tools.length; k++) {
                let toolBtn = tools[k];
                let btn = toolbarBtn.cloneNode(true);
                $B.DomUtils.attribute(btn, evAtrr);
                let ibtn = $B.DomUtils.children(btn, "i");
                $B.DomUtils.attribute(ibtn, evAtrr);
                if (toolBtn.iconCls) {
                    $B.DomUtils.addClass(ibtn, toolBtn.iconCls);
                    if (toolBtn.color) {
                        $B.DomUtils.css(ibtn, { color: toolBtn.color });
                    }
                } else {
                    $B.DomUtils.remove(ibtn);
                }
                let itxt = $B.DomUtils.children(btn, "span");
                $B.DomUtils.attribute(itxt, evAtrr);
                if (toolBtn.showText) {
                    $B.DomUtils.text(itxt, toolBtn.text);
                } else {
                    $B.DomUtils.attribute(ibtn, { title: toolBtn.text });
                }
                $B.DomUtils.append(toolWrap, btn);
                ins.toolbarEventIndex++;
                $B.DomUtils.attribute(btn, { eventidx: ins.toolbarEventIndex });
                if (typeof toolBtn.click === "function") {
                    ins.toolbarEvents[ins.toolbarEventIndex] = toolBtn.click;
                } else {
                    let methodsObject = toolBtn.methodsObject ? toolBtn.methodsObject : ins.opts.methodsObject;
                    if (methodsObject && window[methodsObject]) {
                        ins.toolbarEvents[ins.toolbarEventIndex] = window[methodsObject][toolBtn.click];
                    } else {
                        ins.toolbarEvents[ins.toolbarEventIndex] = undefined;
                    }
                }
            }
        }
        if (ins.opts.onItemCreated) {
            ins.opts.onItemCreated.call(ins, $li, data);
        }
        if (ins.opts.clickFirst && !ins.$firstEL) {
            ins.$firstEL = $li;
        }
    }
    if (fragment.childNodes.length > 0) {
        ul.appendChild(fragment);
    }
    if (ins.opts.checkbox && !hasParent && lastChild) {
        ins.leafNodeArray.push(lastChild);//用于递归渲染父节点的勾选情况
    }
}
function _load(ins, ul, param, clickNodeBtnEl, callBack) {
    let url = ins.opts.url;
    let pel = $B.DomUtils.previous(ul);
    let method = (url.indexOf(".data") > 0 || url.indexOf(".json") > 0) ? "GET" : "POST";
    var loading = $B.getIconLoading("k_tree_deep_size k_simple_loading");
    if (pel && $B.DomUtils.hasClass(pel, "k_tree_item_wrap")) {
        let all = $B.DomUtils.children(clickNodeBtnEl);
        if (all.length > 0) {
            $B.DomUtils.hide(all);
        }
        $B.DomUtils.append(clickNodeBtnEl, loading);
    } else {
        let li = $B.DomUtils.createEl("<li/>");
        $B.DomUtils.append(li, loading);
        $B.DomUtils.css(loading, { "padding-left": "6px" });
        $B.DomUtils.append(ul, li);
        loading = li;
    }
    $B.request({
        dataType: 'json',
        url: url,
        data: param,
        type: method,
        onErrorEval: true,
        onReturn: function () {
            try {
                $B.removeLoading(loading, () => {
                    loading = undefined;
                });
                if (clickNodeBtnEl) {
                    let all = $B.DomUtils.children(clickNodeBtnEl);
                    if (all.length > 0) {
                        $B.DomUtils.show(all);
                    }
                    setTimeout(() => {
                        clickNodeBtnEl = undefined;
                    }, 10);
                }
            } catch (ex) {
                console.log(ex);
            }
        },
        ok: function (message, data) {
            if(ins.opts.onLoaded){
                setTimeout(()=>{
                    ins.opts.onLoaded(data);
                },10);
            }
            if (!data || data.length === 0) {
                if (clickNodeBtnEl) {
                    $B.toolTip(clickNodeBtnEl, ($B.config && $B.config.returnEmptyData) ? $B.config.returnEmptyData : 'the return is empty!', 2);
                } else {
                    $B.toolTip(ul, ($B.config && $B.config.returnEmptyData) ? $B.config.returnEmptyData : 'the return is empty!', 2);
                }
                return;
            }
            ins.itemCounting = 0;
            ins.leafNodeArray = [];
            var treeData = data;
            if (ins.opts.islistData) {
                treeData = ins._formatData(data);
            } else {
                if (!$B.isArrayFn(data)) {
                    treeData = [data];
                }
            }
            let deep = 1, endCount = 0;
            let isLast;
            if (pel && $B.DomUtils.hasClass(pel, "k_tree_item_wrap")) {
                deep = parseInt($B.DomUtils.attribute(pel, "deep")) + 1;
                endCount = parseInt($B.DomUtils.attribute(pel, "endCount"));
                isLast = $B.DomUtils.attr(pel, "islast");
                if (isLast) {
                    endCount++;
                } else {
                    endCount = 0;
                }
            }
            let loopPrs = { deep: deep, endCount: endCount };
            if (isLast) {
                loopPrs.parentEnd = true;
            }
            ins.optimizeCreateFn(ins, ul, loopPrs, treeData);
            callBack(treeData);
        }
    });
}
function _loopChilds(ins, params, ul) {
    if (ul) {
        let lis = $B.DomUtils.children(ul, "li");
        for (let i = 0; i < lis.length; i++) {
            let li = lis[i];
            let childs = $B.DomUtils.children(li);
            let $w = childs[0];
            let data = $B.DomUtils.getData($w, ins.dataBeanKey);
            data.checked = params.checked;
            let icon = $B.DomUtils.children($w, ".k_tree_check_box");
            if (ins.isFontIcon) {
                icon = $B.DomUtils.children(icon, "i");
            }
            if (params.checked) {
                $B.DomUtils.removeClass(icon, ins.opts.chkEmptyIcon + " " + ins.opts.chkSomeIcon);
                $B.DomUtils.addClass(icon, ins.opts.chkAllIcon);
                ins._modifyChkIconColor(1, icon);
            } else {
                $B.DomUtils.removeClass(icon, ins.opts.chkAllIcon + " " + ins.opts.chkSomeIcon);
                $B.DomUtils.addClass(icon, ins.opts.chkEmptyIcon);
                ins._modifyChkIconColor(-1, icon);
            }
            if (childs.length === 2) {
                ins.optimizeLoopChildsFn(ins, params, childs[1]);
            }
        }
    }
}
function _loopParents(ins, params, li) {
    var ul = li.parentNode;
    var parentWrap = $B.DomUtils.previous(ul);
    if (parentWrap && $B.DomUtils.hasClass(parentWrap, "k_tree_item_wrap")) {
        let checkStatu = params.checkStatu;
        let chkIcon = $B.DomUtils.children(parentWrap, ".k_tree_check_box");
        if (ins.isFontIcon) {
            chkIcon = $B.DomUtils.children(chkIcon, "i");
        }
        let data = $B.DomUtils.getData(parentWrap, ins.dataBeanKey);
        if (checkStatu === -1) {
            $B.DomUtils.removeClass(chkIcon, ins.opts.chkAllIcon + " " + ins.opts.chkSomeIcon);
            $B.DomUtils.addClass(chkIcon, ins.opts.chkEmptyIcon);
            data.checked = false;
            ins._modifyChkIconColor(-1, chkIcon);
        } else if (checkStatu === 1) {
            data.checked = true;
            $B.DomUtils.removeClass(chkIcon, ins.opts.chkEmptyIcon + " " + ins.opts.chkSomeIcon);
            $B.DomUtils.addClass(chkIcon, ins.opts.chkAllIcon);
            ins._modifyChkIconColor(1, chkIcon);
        } else {
            data.checked = false;
            $B.DomUtils.removeClass(chkIcon, ins.opts.chkAllIcon + " " + ins.opts.chkEmptyIcon);
            $B.DomUtils.addClass(chkIcon, ins.opts.chkSomeIcon);
            ins._modifyChkIconColor(0, chkIcon);
        }
        var deep = $B.DomUtils.attribute(parentWrap, "deep");
        if (deep !== "1") { //向上递归
            let liList = $B.DomUtils.children(parentWrap.parentNode.parentNode);
            let res = _getSiblingsFlag(ins, liList, ins.isFontIcon);
            checkStatu = 0;
            if (res.isCheckAll) {
                checkStatu = 1;
            } else if (res.isUnCheckAll) {
                checkStatu = -1;
            }
            ins.optimizeLoopParentsFn(ins, { checkStatu: checkStatu }, parentWrap.parentNode);
        }
    }
}
function _loopFN(ins, ul, onLoopFN) {
    let childs = ul.children;
    for (let i = 0; i < childs.length; i++) {
        let li = childs[i];
        let ret = onLoopFN(li);
        if (ret) {
            return;
        }
        if (li.firstChild.nextSibling) {
            ins.optimizeLoopTreeFn(ins, li.firstChild.nextSibling, onLoopFN);
        }
    }
}

/**获取同级元素的复选情况***/
function _getSiblingsFlag(ins, sblings, isFontIcon) {
    var checked = 0,
        unchecked = 0,
        other = 0;
    for (let i = 0; i < sblings.length; i++) {
        li = sblings[i];
        let $w = $B.DomUtils.children(li, "div");
        let chkIcon = $B.DomUtils.children($w, ".k_tree_check_box");
        if (isFontIcon) {
            chkIcon = $B.DomUtils.children(chkIcon, "i");
        }
        if ($B.DomUtils.hasClass(chkIcon, ins.opts.chkEmptyIcon)) {
            unchecked++;
        } else if ($B.DomUtils.hasClass(chkIcon, ins.opts.chkAllIcon)) {
            checked++;
        } else {
            other++;
        }
        if ((unchecked > 0 && checked > 0) || (unchecked > 0 && other > 0) || (checked > 0 && other > 0)) {
            break;
        }
    }
    return {
        isCheckAll: sblings.length === checked,
        isUnCheckAll: sblings.length === unchecked
    };
}
class Tree extends $B.BaseControl {
    constructor(elObj, opts) {
        super();
        var _this = this;
        super.setElObj(elObj);
        this._textClz = ".k_tree_text";
        this._divName = "div";
        this.dataBeanKey = "bean";
        this.itemCounting = 0;
        this.leafNodeArray = [];
        this.toolbarEventIndex = 1;
        this.toolbarEvents = {};
        this.optimize2treeFn = $B.recursionFn(_list2tree, true);
        this.optimizeCreateFn = $B.recursionFn(_createTreeItem, true);
        this.optimizeLoopChildsFn = $B.recursionFn(_loopChilds, true);
        this.optimizeLoopParentsFn = $B.recursionFn(_loopParents, true);
        this.optimizeLoopTreeFn = $B.recursionFn(_loopFN, true);
        this.optimizeTree2ListFn = $B.recursionFn(_tree2list, true);
        $B.DomUtils.addClass(this.elObj, "k_tree_ul k_tree_root k_box_size");
        this.opts = $B.extendObjectFn(true, {}, defaultOpts, opts);
        this.isFontIcon = this.opts.nodeParentIcon.indexOf("fa-") === 0;
        if (!this.opts.showLine) {
            this.opts.plainStyle = false;
        }
        this.clickedItem = undefined;
        if (this.opts.data) {
            if (this.opts.islistData) {
                this.opts.data = this._formatData(this.opts.data);
            } else {
                if (!$B.isArrayFn(this.opts.data)) {
                    this.opts.data = [this.opts.data];
                }
            }
            this.optimizeCreateFn(this, this.elObj, { deep: 1, endCount: 0 }, this.opts.data);
            this.triggerFirstClick();
        } else if (this.opts.url && this.opts.url !== "") {
            _load(this, _this.elObj, { pid: "" }, undefined, (data) => {
                this.opts.data = data;
                this.triggerFirstClick();
            });
        }
        this.liEl = undefined;
        this.deepEl = undefined;
        this.childUl = undefined;
        if (this.opts.showOptimize) {
            this._checkCreated = setInterval(() => {
                if (!this.taskStack.length) {
                    this._loopCheckedUiRender();
                    clearInterval(this._checkCreated);
                    if (this.opts.onCreated) {
                        this.opts.onCreated();
                    }
                }
            }, 50);
        } else {
            this._loopCheckedUiRender();
        }
    }
    _loopCheckedUiRender() {
        for (let i = 0; i < this.leafNodeArray.length; i++) {
            this._loopCrcParentCheck(this.leafNodeArray[i]);
        }
        this.leafNodeArray = [];
    }
    _formatData(list, pid) {
        if (!pid) {
            pid = "";
        }
        var res = [];
        this.optimize2treeFn(this, res, list, pid);
        return res;
    }
    bindEvents(node) {
        if (!this.itemEventHandler) {
            var _this = this;
            this.itemEventHandler = function (e) {
                let event = $B.DomUtils.attribute(e.target, "event");
                if (!event) {
                    event = "itemEvent";
                }
                if (_this[event]) {
                    _this[event](e);
                }
                return false;
            };
            //mosueover ，mouseout事件
            if (this.opts.toolAutoHide && this.opts.toolbar) {
                this.mouseEvents = {
                    mouseenter: (e) => {
                        let res = this.getClickItemData(e.target, true);
                        let el = res.el;
                        let tool = $B.DomUtils.children(el, ".k_tree_button_wrap");
                        $B.DomUtils.show(tool);
                    },
                    mouseleave: (e) => {
                        let res = this.getClickItemData(e.target, true);
                        let el = res.el;
                        let tool = $B.DomUtils.children(el, ".k_tree_button_wrap");
                        $B.DomUtils.hide(tool);
                    }
                };
            }
        }
        $B.DomUtils.click(node, this.itemEventHandler);
        if (this.opts.toolAutoHide && this.opts.toolbar) {
            $B.DomUtils.bind(node, this.mouseEvents);
        }
    }
    getClickItemData(el, notData) {
        var res;
        while (el) {
            if ($B.DomUtils.hasClass(el, "k_tree_item_wrap")) {
                let _data;
                if (!notData) {
                    _data = $B.DomUtils.getData(el, this.dataBeanKey);
                }
                res = {
                    el: el,
                    bean: _data,
                    data: _data
                };
                break;
            }
            el = el.parentNode;
        }
        if (!notData && this.opts.tree2list) { //将data树转换数组
            let arr = [];
            this.optimizeTree2ListFn(this, arr, res.data);
            res.data = arr;
        }
        return res;
    }
    itemEvent(e) {
        var res = this.getClickItemData(e.target);
        var el = res.el;
        if (!this.opts.canClickParent) {
            let ul = $B.DomUtils.next(el);
            if (ul) {
                return;
            }
        }
        if (this.opts.clickItemCls) {
            if (this.clickedItem) {
                $B.DomUtils.removeClass(this.clickedItem, this.opts.clickItemCls);
            }
            this.clickedItem = $B.DomUtils.addClass(el, this.opts.clickItemCls);
        }
        if (this.opts.onClick && !e._istrigger) {
            e.data = res.data;
            setTimeout(() => {
                this.opts.onClick.call(res.el, e);
            }, 1);
        }
        if (this.opts.clickCheck && this.opts.checkbox && !e._istrigger) {
            var box = $B.DomUtils.children(el, ".k_tree_check_box")[0];
            this.checkEvent({ target: box });
        }
    }
    toolEvent(e) {
        var res = this.getClickItemData(e.target);
        var eventIdx = $B.DomUtils.attribute(e.target.parentNode, "eventidx");
        e.data = res.data;
        var fn = this.toolbarEvents[eventIdx];
        if (typeof fn === "function") {
            fn.call(e.target.parentNode, e);
        } else {
            console.log("not found function,", fn);
        }
    }
    parentEvent(e) {        
        let res = this.getClickItemData(e.target);
        let el = res.el;
        let ul = el.nextSibling;
        let duration = 200;
        let _this = this;
        let oprIcon = e.target;
        let nodeBtn;
        if ($B.DomUtils.hasClass(oprIcon, "_node_")) {
            nodeBtn = oprIcon;
            oprIcon = oprIcon.nextSibling;
            if (oprIcon) {
                if (!$B.DomUtils.hasClass(oprIcon, "_parent_")) {
                    oprIcon = undefined;
                }
            }
        } else {
            nodeBtn = e.target.previousSibling;
            if (e.target.tagName === "I") {
                nodeBtn = e.target.parentNode.previousSibling;
            }
        }
        if (nodeBtn && !$B.DomUtils.hasClass(nodeBtn, "_node_")) {
            nodeBtn = undefined;
        }
        if (this.isFontIcon && oprIcon && oprIcon.tagName === "DIV") {
            oprIcon = $B.DomUtils.children(oprIcon, "i")[0];
        }
        if (ul) {
            let isLast = $B.DomUtils.attribute(el, "islast");
            let endcount = parseInt($B.DomUtils.attribute(el, "endcount"));
            if (oprIcon) {
                if (oprIcon.tagName === "I") {
                    lineNode = oprIcon.parentNode.previousSibling;
                } else {
                    lineNode = oprIcon.previousSibling;
                }
            }
            var isOpening, lineNode;
            var nodeAnimateFn = () => {
                if (isOpening) { //展开
                    if (oprIcon) {
                        $B.DomUtils.removeClass(oprIcon, _this.opts.nodeParentIcon);
                        $B.DomUtils.addClass(oprIcon, _this.opts.nodeParentOpenIcon);
                    }
                    if (isLast) {//最后一个
                        if (nodeBtn) {
                            $B.DomUtils.removeClass(nodeBtn, "k_tree_line_last_closed");
                            $B.DomUtils.addClass(nodeBtn, "k_tree_line_open");
                        }
                    } else {
                        $B.DomUtils.removeClass(nodeBtn, "k_tree_line_closed");
                        $B.DomUtils.addClass(nodeBtn, "k_tree_line_open");
                    }                    
                    if (this.opts.url && $B.DomUtils.children(ul).length === 0) {
                        let id;
                        if (this.opts.tree2list) {
                            id = res.data[0][this.opts.idField];
                        } else {
                            id = res.data.data[this.opts.idField];
                        }
                        let clickBtn = e.target;
                        if (e.target.tagName === "I") {
                            clickBtn = e.target.parentNode;
                        }
                        let deep = $B.Dom.attr(el,"deep");   
                        this.openDeep = deep + 1;                 
                        _load(this, ul, { pid: id  }, clickBtn, (retData) => {
                            res.bean.children = retData;
                            this.openDeep = undefined;
                        });
                    }
                } else {//收起
                    if (oprIcon) {
                        $B.DomUtils.removeClass(oprIcon, _this.opts.nodeParentOpenIcon);
                        $B.DomUtils.addClass(oprIcon, _this.opts.nodeParentIcon);
                    }
                    if (isLast) {//最后一个
                        if (nodeBtn) {
                            $B.DomUtils.removeClass(nodeBtn, "k_tree_line_open");
                            $B.DomUtils.addClass(nodeBtn, "k_tree_line_last_closed");
                            lineNode = nodeBtn.previousSibling;
                            while (endcount > 0) {
                                $B.DomUtils.removeClass(lineNode, "k_tree_line_vertical");
                                $B.DomUtils.addClass(lineNode, "k_tree_line_last");
                                lineNode = lineNode.previousSibling;
                                endcount--;
                            }
                        }
                    } else {
                        if (nodeBtn) {
                            $B.DomUtils.removeClass(nodeBtn, "k_tree_line_open");
                            $B.DomUtils.addClass(nodeBtn, "k_tree_line_closed");
                        }
                    }
                }
            };
            if ($B.DomUtils.css(ul, "display") === "none") {
                isOpening = true;
                $B.slideDown(ul, duration, nodeAnimateFn);
                if (nodeBtn) {
                    lineNode = nodeBtn.previousSibling;
                    while (endcount > 0) {
                        $B.DomUtils.removeClass(lineNode, "k_tree_line_last");
                        $B.DomUtils.addClass(lineNode, "k_tree_line_vertical");
                        lineNode = lineNode.previousSibling;
                        endcount--;
                    }
                }
            } else {
                $B.slideUp(ul, duration, nodeAnimateFn);
            }
        }
    }
    checkEvent(e) {
        if (this.opts.disChecked) {
            return;
        }
        var res = this.getClickItemData(e.target);
        var el = res.el;
        var data = res.data;
        if (!this.opts.canClickParent) {
            let ul = $B.DomUtils.next(el);
            if (ul) {
                return;
            }
        }
        let oldData;
        if (this.opts.onChange && !e._istrigger) {
            oldData = this.getCheckData();
        }
        var iconEl = e.target;
        if (this.isFontIcon && iconEl.tagName === "DIV") {
            iconEl = $B.DomUtils.children(iconEl, "i");
        }
        var ischecked = !$B.DomUtils.hasClass(iconEl, this.opts.chkAllIcon);
        data.checked = ischecked;
        if (ischecked) { //变全选
            $B.DomUtils.removeClass(iconEl, this.opts.chkEmptyIcon + " " + this.opts.chkSomeIcon);
            $B.DomUtils.addClass(iconEl, this.opts.chkAllIcon);
            this._modifyChkIconColor(1, iconEl);
        } else {//变不选
            $B.DomUtils.removeClass(iconEl, this.opts.chkAllIcon + " " + this.opts.chkSomeIcon);
            $B.DomUtils.addClass(iconEl, this.opts.chkEmptyIcon);
            this._modifyChkIconColor(-1, iconEl);
        }
        if (!this.opts.checkSingle) {
            this.optimizeLoopChildsFn(this, { checked: ischecked }, el.nextSibling);
            this._loopCrcParentCheck(el.parentNode);
        }
        if (this.opts.onChecked && !e._istrigger) {
            setTimeout(() => {
                this.opts.onChecked.call(el, data, ischecked);
            }, 1);
        }
        if (this.opts.onChange && !e._istrigger) {
            setTimeout(() => {
                let newData = this.getCheckData();
                this.opts.onChange(this.id, newData, oldData);
            }, 1);
        }
    }
    /**
     * -1 不选择，0部分选择，1全选
     * **/
    _modifyChkIconColor(flag, iconEl) {
        if (this.isFontIcon && this.opts.checkedColor) {
            if (flag === -1) {//不选择
                if (this.opts.fontIconColor) {
                    $B.DomUtils.css(iconEl, { color: this.opts.fontIconColor });
                } else {
                    let styleObj = $B.style2cssObj(iconEl);
                    delete styleObj["color"];
                    $B.DomUtils.attribute(iconEl, { style: $B.cssObj2string(styleObj) });
                }
            } else {
                $B.DomUtils.css(iconEl, { color: this.opts.checkedColor });
            }
        }
    }
    _loopCrcParentCheck(li) {
        //当前节点是否全选，来更新父节点上的勾选ui：1全选，0部分选，-1都不选
        var lisList = $B.DomUtils.children(li.parentNode);
        var res = _getSiblingsFlag(this, lisList, this.isFontIcon);
        var flag = 0;
        if (res.isCheckAll) {
            flag = 1;
        } else if (res.isUnCheckAll) {
            flag = -1;
        }
        var parentParam = { checkStatu: flag };
        this.optimizeLoopParentsFn(this, parentParam, li);
    }
    getCheckData(onlyId) {
        if (typeof onlyId === "undefined") {
            onlyId = !this.opts.getIdAndTxt;
        }
        var getNotEmpty = false;
        if (arguments.length === 2) {
            getNotEmpty = arguments[1];
        }
        let ret = [];
        if (!this.opts.checkbox) {
            if (this.clickedItem) {
                let id = $B.DomUtils.attribute(this.clickedItem.parentNode, "id").replace("_","");
                if (typeof onlyId === "undefined" || onlyId) {
                    ret.push(id);
                } else {
                    let $txt = $B.DomUtils.children(this.clickedItem, ".k_tree_text")[0];
                    ret.push({ id: id, text: $txt.innerText });
                }
            }
            return ret;
        }
        this.optimizeLoopTreeFn(this, this.elObj, (li) => {
            let $wap = li.firstChild;
            let $chk = $B.DomUtils.children($wap, ".k_tree_check_box")[0];
            let $i = $chk.firstChild ? $chk.firstChild : $chk;
            let isChk;
            if (getNotEmpty) {
                isChk = !$B.DomUtils.hasClass($i, this.opts.chkEmptyIcon);
            } else {
                isChk = $B.DomUtils.hasClass($i, this.opts.chkAllIcon);
            }
            if (isChk) {
                let id = $B.DomUtils.attribute(li, "id").replace("_","");
                if (id !== "") {
                    if (typeof onlyId === "undefined" || onlyId) {
                        ret.push(id);
                    } else {
                        let $txt = $chk.nextSibling;
                        ret.push({ id: id, text: $txt.innerText });
                    }
                }
            }
        });
        return ret;
    }
    reload(ul, params) {
        if (!ul) {
            ul = this.elObj;
        } else if (!$B.isElement(ul)) {
            params = ul;
            ul = this.elObj;
        }
        if ($B.Dom.hasClass(ul, "k_tree_root")) {
            this.$firstEL = undefined;
        }
        let data, clickBtn;
        if (!$B.DomUtils.hasClass(ul, "k_tree_root")) {
            let $it = ul.previousSibling;
            data = this.getClickItemData($it);
            let firstEl = $it.firstChild;
            while (firstEl) {
                if ($B.DomUtils.hasClass(firstEl, "_node_") || $B.DomUtils.hasClass(firstEl, "_parent_")) {
                    break;
                }
                firstEl = firstEl.nextSibling;
            }
            clickBtn = firstEl;
        }
        $B.DomUtils.removeChilds(ul);
        setTimeout(() => {
            _load(this, ul, params, clickBtn, (retData) => {
                if (data) {
                    data.bean.children = retData;
                } else {
                    this.opts.data = retData;
                    this.triggerFirstClick();
                }
            });
        }, 1);
    }
    triggerFirstClick() {
        if (this.opts.clickFirst && this.$firstEL) {
            this.triggerClick(this.$firstEL);
        }
    }
    triggerClick($li) {
        this.itemEvent({ target: $li.firstChild });
    }
    updateNode() {
        let $it = this.clickedItem;
        let data;
        for (let i = 0; i < arguments.length; i++) {
            let arg = arguments[i];
            if ($B.Dom.isElement(arg)) {
                $it = arg;
            } else {
                data = arg;
            }
        }
        let uiId = "_" + data[this.opts.idField];
        if (!$it && data) {
            let $li = $B.Dom.findbyId(this.elObj, data[this.opts.idField]);
            if ($li) {
                $it = $li.firstChild;
            }
        } else if ($it.parentNode.id !== uiId) {
            $it = undefined;
            let $li = $B.Dom.findbyId(this.elObj, uiId);
            if ($li) {
                $it = $li.firstChild;
            }
        }
        if ($it) {
            let $txt = $B.Dom.children($it,".k_tree_text")[0];
            let bean = $B.DomUtils.getData($it, this.dataBeanKey);            
            let text = data[this.opts.textField];
            bean.text = text;
            $txt.innerText = text;
            $B.extendObjectFn( bean.data,data); 
        }
    }
    deleteNode(id){
        let $li = $B.Dom.findbyId(this.elObj, "_"+id);
        if($li){
            let ul = $li.parentNode;
            if($B.Dom.hasClass(ul,"k_tree_root")){
                this.opts.data = undefined;
                $B.Dom.removeChilds(ul);
            }else{
                let $prtEL = ul.previousSibling;
                let bean = $B.DomUtils.getData($prtEL, this.dataBeanKey); 
                let newChilds = [];
                let childs = bean.children;
                for(let i =0 ;i < childs.length ;i++){
                    if(childs[i].id !== id){
                        newChilds.push(childs[i]);
                    }
                }
                bean.children = newChilds;
                let $fchild = $li.firstChild ;
                let nextCall ;
                if($fchild === this.clickedItem){ //删除的刚好是激活行
                    nextCall = ()=>{
                        this.clickedItem = undefined;
                        this.itemEvent({target:$prtEL});
                    };                    
                }
                $B.Dom.remove($li);
                if(nextCall){
                    nextCall();
                }
            }
        }
    }
    /**
     * 可以是数据id，或者树节点元素div
     * **/
    reloadNode(args){
        let $node =  this.clickedItem;
        if(args){
            if($B.Dom.isElement(args)){
                $node = args;
            }else if(typeof args === "string"){
                let $li = $B.Dom.findbyId(this.elObj, "_"+args);
                if ($li) {
                    $node = $li.firstChild;
                }
            }
        }
        if($node){            
            let ul = $node.nextSibling;
            if(ul){
                let ret = this.getClickItemData($node);
                let clickBtn;
                let childs = $node.children;
                for(let i =0 ; i < childs.length ;i++){
                    if($B.Dom.attr(childs[i] , "event") === "parentEvent"){
                        clickBtn = childs[i];
                        break;
                    }
                }
                let deep = $B.Dom.attr( $node,"deep");   
                this.openDeep = deep + 1; 
                $B.DomUtils.removeChilds(ul);
                _load(this, ul, { pid: ret.bean.id }, clickBtn, (retData) => {
                    this.openDeep = undefined;
                    ret.bean.children = retData;
                });
            }            
        }
    }
    /**
     *  args = [id1,id2,id3];
     * ***/
    setValue(args) {
        if ($B.isPlainObjectFn(args)) {
            if (Array.isArray(args.id)) {
                args = args.id;
            }
        }
        var idMap = {};
        if (Array.isArray(args)) {
            for (let i = 0; i < args.length; i++) {
                idMap[args[i]] = true;
            }
        } else {
            idMap[args] = true;
        }
        let triggerEls = [];
        this.optimizeLoopTreeFn(this, this.elObj, (li) => {
            let $wap = li.firstChild;
            let id = $B.DomUtils.attribute(li, "id").replace("_","");
            if (this.opts.checkbox) {
                let $chk = $B.DomUtils.children($wap, ".k_tree_check_box")[0];
                $chk = $chk.firstChild ? $chk.firstChild : $chk;
                if ($B.DomUtils.hasClass($chk, this.opts.chkSomeIcon) || $B.DomUtils.hasClass($chk, this.opts.chkAllIcon)) {
                    let _data = $B.DomUtils.getData($wap, this.dataBeanKey);
                    _data.checked = false;
                    $B.DomUtils.removeClass($chk, this.opts.chkSomeIcon + " " + this.opts.chkAllIcon);
                    $B.DomUtils.addClass($chk, this.opts.chkEmptyIcon);
                }
            }
            if (idMap[id]) {
                if (this.opts.checkbox) {
                    let $chk = $B.DomUtils.children($wap, ".k_tree_check_box")[0];
                    if ($wap.nextSibling) {
                        if ($wap.nextSibling.children.length > 0) {
                            triggerEls.push($chk);
                        }
                    } else {
                        triggerEls.push($chk);
                    }
                } else {
                    if (idMap[id]) {
                        this.itemEvent({ target: $wap, _istrigger: true });
                        return true;
                    }
                }
            }
        });
        for (let i = 0; i < triggerEls.length; i++) {
            this.checkEvent({ target: triggerEls[i], _istrigger: true });
        }
    }
    _clear() {
        this.optimizeTree2ListFn("destroy");
        this.optimize2treeFn("destroy");
        this.optimizeCreateFn("destroy");
        this.optimizeLoopChildsFn("destroy");
        this.optimizeLoopParentsFn("destroy");
        this.optimizeLoopTreeFn("destroy");
    }
    destroy(isForce) {
        this._clear();
        super.destroy(isForce);
    }
    clear() {
        this._clear();
        super.clear();
    }
}
$B["Tree"] = Tree; 
 if(!window["$B"] ) {window["$B"] = $B;} 
  return window["$B"] ; 
});

/*! BUI - v2.0.0 - 2022-03-13 09:03:17 
Copyright (c): kevin.huang  https://gitee.com/kevin-huang/bui2.0.git (qq:757723114) 
Released under MIT License*/
(function ( global, factory) {
//   "use strict";
	if ( typeof module === "object" && typeof module.exports === "object" ) {
		module.exports = global.document ?
			factory( global, true ) :
			function( w ) {
				if ( !w.document ) {
					throw new Error( "Bui requires a window with a document" );
				}
				return factory( w );
			};
	} else {
		  factory( global );
	}  
})( typeof window !== "undefined" ? window : this, function( window, noGlobal ) {
//   "use strict";
  var $B = window["$B"] ?  window["$B"] : {};


var defaultOpts = {
    style: 'plain',  //plain简约风格，normal常规风格，card卡片风格
    actItemBackColor: '#ffffff',
    actItemFontColor: '#005FFF',
    headerBackColor: undefined,
    autoScroll: true,//自动隐藏滚动条
    ifrScroll:true,
    reload: false,//点击刷新加载
    ctxmenu: true,
    openTab: false, //是否显示在浏览器新页打开
    position: "top",
    headSize: '35px',
    moreBtnSize: 14,
    tabs: undefined,
    bodyItemStyle:undefined,
    bodyStyle:undefined,
    moreBtnCss: { background: "#fff", "border-bottom": "1px solid rgb(218, 220, 224)" }
};

class Tabs extends $B.BaseControl {
    constructor(elObj, opts) {
        super();
        super.setElObj(elObj);
        $B.DomUtils.addClass(this.elObj, "k_tab_wrap k_box_size");
        $B.DomUtils.css(this.elObj, { "overflow": "hidden" });
        this.opts = $B.extendObjectFn(true, {}, defaultOpts, opts);
        this.morePaddLeft = 0;
        this.readerUI();
        var checker;
        $B.DomUtils.resize(window, (e) => {
            clearTimeout(checker);
            checker = setTimeout(() => {
                this.checkOverFlow();
            }, 300);
        });
    }
    /**
     * 根据Id激活某一个选项卡
     * **/
    activedById(id){
        let els = this.tabHeader.firstChild.children;
        for (let i = 0; i < els.length; i++) {
            if ($B.DomUtils.attribute(els[i], "id") === id) {
                this.onClickEv(els[i], () => {
                    setTimeout(() => {
                        this.movebyAct();
                    }, 1);
                });
                return true;
            }
        }
        return false;
    }
     /**
     * args = {
            fixed:true,
            id:'0001',
            text: "用户管理",
            iconCls: 'fa-user',
            content: '<p style="color:red">用户管理</p>'
        }
     * ***/
    add(args) {
        let id = args.id;
        if(this.activedById(id)){
            return;
        }
        if (this.opts.position === "top" || this.opts.position === "bottom") {
            let idx = 0;
            let nextIdx = 0;
            let $actIt = this.$activeted;
            if ($actIt) {
                idx = parseInt($B.DomUtils.attribute($actIt, "index"));
                nextIdx = idx + 1;
                let newTabs = [];
                for (let i = 0; i < this.opts.tabs.length; i++) {
                    newTabs.push(this.opts.tabs[i]);
                    if (i === idx) {
                        newTabs.push(args);
                    }
                }
                this.opts.tabs = newTabs;
            } else {
                this.opts.tabs = [args];
            }           
            let $it = this._createHorizItem(args, nextIdx, ($t, $b) => {
                let childs = this.tabHeader.firstChild.children;
                if (childs.length > 0) {
                    let el = childs[idx];
                    $B.DomUtils.after(el, $t);
                    childs = this.tabBody.children;
                    el = childs[idx];
                    $B.DomUtils.after(el, $b);
                    let nextEl = $t.nextSibling;
                    while(nextEl){
                        let newIdx = parseInt($B.DomUtils.attribute(nextEl, "index")) + 1;
                        $B.DomUtils.attribute(nextEl, {"index":newIdx});
                        nextEl = nextEl.nextSibling;
                    }
                } else {
                    $B.DomUtils.append(this.tabHeader.firstChild, $t);
                    $B.DomUtils.append(this.tabBody, $b);
                }
                setTimeout(() => {
                    this.checkOverFlow();
                }, 100);
            });           
            this.onClickEv($it, () => {
                setTimeout(() => {
                    this.movebyAct();
                }, 1);
            });
        }
    }
    movebyAct() {
        let $wap = this.$activeted.parentNode;
        let $prt = $wap.parentNode;
        let innerWidth = $B.DomUtils.width($wap);
        let prtWidth = $B.DomUtils.innerWidth($prt);
        let left = Math.abs($B.DomUtils.css($wap, "left"));
        let itWidth = $B.DomUtils.outerWidth(this.$activeted);
        let pos = 0;
        let $el = this.$activeted;
        while ($el) {
            pos = pos + $B.DomUtils.outerWidth($el);
            $el = $el.previousSibling;
        }
        let diff = pos - left;
        if (diff > prtWidth) {
            let shift = prtWidth - diff - left;
            $B.animate($wap, { left: shift }, {
                duration: 100, complete: () => {
                    if (this.$actLine) {
                        this._updateHActLinePos();
                    }
                }
            });
        } else if (diff < itWidth) {
            let shift = - (pos - itWidth);
            if (shift > 0) {
                shift = 0;
            }
            $B.animate($wap, { left: shift }, {
                duration: 100, complete: () => {
                    if (this.$actLine) {
                        this._updateHActLinePos();
                    }
                }
            });
        }
        if (this.$rbtn) {
            if (innerWidth > prtWidth) {
                if (this.$activeted.nextSibling) {
                    $B.DomUtils.removeClass(this.$rbtn, "k_tabs_more_disabled");
                    $B.DomUtils.addClass(this.$rbtn.firstChild, "enabled");
                } else {
                    $B.DomUtils.addClass(this.$rbtn, "k_tabs_more_disabled");
                    $B.DomUtils.removeClass(this.$rbtn.firstChild, "enabled");
                }
                if (this.$activeted.previousSibling) {
                    $B.DomUtils.removeClass(this.$lbtn, "k_tabs_more_disabled");
                    $B.DomUtils.addClass(this.$lbtn.firstChild, "enabled");
                } else {
                    $B.DomUtils.addClass(this.$lbtn, "k_tabs_more_disabled");
                    $B.DomUtils.removeClass(this.$lbtn.firstChild, "enabled");
                }
            } else {
                setTimeout(() => {
                    this.checkOverFlow();
                    this._updateHActLinePos()
                }, 100);
            }
        }
    }
    delete(idOrTxt) {
        if (this.opts.position === "top" || this.opts.position === "bottom") {
            let childs = this.tabHeader.firstChild.children;
            let deleteEl;
            for (let i = 0; i < childs.length; i++) {
                let el = childs[i];
                let id = $B.DomUtils.attribute(el, "id");
                let txt = el.innerText;
                if (id === idOrTxt || txt === idOrTxt) {
                    deleteEl = el;
                    break;
                }
            }
            if (deleteEl) {
                this._removeOneIt(deleteEl);
            }
        }
    }
    readerVertical() {
        var tabs = this.opts.tabs;
        var thtml = "<div style='width:100%;' class='clearfix k_tab_item_v_" + this.opts.position + " k_tab_item k_box_size'><span style='line-height:1.3em'></span></div>";
        var bhtml = "<div class='k_tab_item_body k_box_size' style='display:none;position:relative;'></div>";
        var activedIt;
        var $wrap = this.tabHeader.firstChild;
        $B.DomUtils.css(this.tabHeader, { height: '100%' });
        var srolOpt = {};
        var _this = this;
        if (this.opts.position === "right") {
            srolOpt.onScrollFn = function (scrollx, scrolly) {
                let top = parseFloat($B.DomUtils.attribute(_this.$actLine, "top")) - scrolly;
                $B.DomUtils.css(_this.$actLine, { top: top });
            };
        }
        $wrap = $B.myScrollbar($wrap, srolOpt);
        this.$iArray = [];
        for (let i = 0; i < tabs.length; i++) {
            let tab = tabs[i];
            let $t = $B.DomUtils.createEl(thtml);
            let attr = { index: i };
            if (tab.id) {
                attr["id"] = tab.id;
            }
            $B.DomUtils.attribute($t, attr);
            if (tab.text) {
                $t.firstChild.innerHTML = tab.text;
            }
            if (tab.iconCls) {
                if (this.opts.position === "left") {
                    $B.DomUtils.prepend($t, "<i style='float:left;padding-right:8px;font-size:1.2em;' class='fa " + tab.iconCls + "'></i>");
                    this.$iArray.push($t.firstChild);
                } else {
                    $B.DomUtils.css($t.firstChild, { "display": "block", "float": "left" });
                    $B.DomUtils.append($t, "<i style='float:right;padding-right:2px;font-size:1.2em;' class='fa " + tab.iconCls + "'></i>");
                    this.$iArray.push($t.lastChild);
                }
            }
            let $b = $B.DomUtils.createEl(bhtml);
            if(this.opts.bodyItemStyle){
                $B.Dom.css($b,this.opts.bodyItemStyle);
            }
            if (tab.content) {
                if ($B.DomUtils.isElement(tab.content)) {
                    if (tab.content.parentNode) {
                        $B.DomUtils.detach(tab.content);
                    }
                    $B.DomUtils.append($b, tab.content);
                } else {
                    $b.innerHTML = tab.content;
                }
            }
            let clzz = "k_tab_it_" + this.opts.position + "_" + this.opts.style;
            $B.DomUtils.addClass($t, clzz);
            $B.DomUtils.append($wrap, $t);
            if (this.opts.autoScroll && $B.myScrollbar && tab.dataType !== "iframe") {
                let $c = $B.myScrollbar($b, {});
                $B.DomUtils.addClass($b, "k_tab_diyscroll");
                $B.DomUtils.css($b, { height: "100%" });
                if(tab.height){
                    $B.Dom.css($c,{"height":tab.height});
                }
            }
            $B.DomUtils.append(this.tabBody, $b);
            if (i === 0 || tab.actived) {
                activedIt = $t;
            }
        }
        if (this.opts.style === "plain") {
            if (this.opts.position === "left") {
                $B.DomUtils.css(this.tabHeader.firstChild, { "border-right": "1px solid #DADCE0" });
                this.$actLine = $B.DomUtils.createEl("<div class='k_tabs_act_line' style='display:inline-block;position:absolute;right:0px;width:2px;top:-300px;height:50px;'></div>");
            } else {
                $B.DomUtils.css(this.tabHeader.firstChild, { "border-left": "1px solid #DADCE0" });
                this.$actLine = $B.DomUtils.createEl("<div class='k_tabs_act_line' style='display:inline-block;position:absolute;left:0px;width:2px;top:-300px;height:50px;'></div>");
            }
            $B.DomUtils.append(this.tabHeader, this.$actLine);
        } else {
            $B.DomUtils.css(this.tabHeader, { "background-color": this.opts.headerBackColor });
        }
        this.$activeted = activedIt;
    }
    _resetHeaderIndex($activeted) {
        if ($activeted) {
            this._resetIndex($activeted);
            this.checkOverFlow();
            this._updateHActLinePos();
        }
    }
    _resetIndex($activeted){   
        console.log("_resetIndex",$activeted);     
        let childNodes = $activeted.parentNode.children;
        for (let i = 0; i < childNodes.length; i++) {
            let $i = childNodes[i];
            $B.DomUtils.attribute($i, { "index": i });
        }
    }
    _removeOneIt($it) {
        let $actIt;
        if (this.$activeted && this.$activeted === $it) {
            if ($it.previousSibling) {
                $actIt = $it.previousSibling;
            } else if ($it.nextSibling) {
                $actIt = $it.nextSibling;
            } else {
                this.$activeted = undefined;
            }
            this.$actbody = undefined;
        }
        this._exeRmItem($it);
        if ($actIt) {
            this.$activeted = $actIt;
        }
        this._resetHeaderIndex(this.$activeted);
        if ($actIt) {
            this.$activeted = undefined;
            setTimeout(() => {
                this.onClickEv($actIt);
            }, 1);
        }
    }
    _exeRmItem($it){
        let idx = parseInt($B.DomUtils.attribute($it, "index"));
        let childs = this.tabBody.children;
        let actBody = childs[idx];
        $B.DomUtils.remove(actBody);
        $B.DomUtils.remove($it);
        var newOpts = [];
        for (let i = 0; i < this.opts.tabs.length; i++) {
            if (i !== idx) {
                newOpts.push(this.opts.tabs[i]);
            }
        }
        this.opts.tabs = newOpts;
        if (newOpts.length === 0) {
            this.$actLine.style.display = "none";
        }
    }
    _bindCloseable($it) {
        if (!this.closeEvents) {
            this.closeEvents = {
                click: (e) => {
                    let el = e.target;
                    this._removeOneIt(el.parentNode);
                    if(this.opts.onClosed){
                       setTimeout(()=>{
                            this.opts.onClosed(el,this.$activeted);
                       },200);
                    }
                    return false;
                }
            };
            if (this.opts.haverClose) {
                this.closeEvents.mouseenter = (e) => {
                    let el = e.target;
                    el.lastChild.style.display = "block";
                };
                this.closeEvents.mouseleave = (e) => {
                    let el = e.target;
                    el.lastChild.style.display = "none";
                };
            }
        }
        var $close = $B.DomUtils.createEl('<i style="position:absolute;top:-1px;right:-2px;font-size:13px;cursor:pointer;" class="fa fa-cancel-1"></i>');
        if (this.opts.haverClose) {
            $close.style.display = "none";
            $B.DomUtils.mouseenter($it, this.closeEvents.mouseenter);
            $B.DomUtils.mouseleave($it, this.closeEvents.mouseleave);
        }
        $B.DomUtils.append($it, $close);
        $B.DomUtils.click($close, this.closeEvents.click);
    }
    _createHorizItem(tab, i, onCreatedFn) {
        var lineHeight = parseInt(this.opts.headSize.replace("px", "")) - 1;
        var thtml = "<div class='k_tab_item_h k_tab_item k_box_size'><span style='line-height:" + lineHeight + "px;'></span></div>";
        var bhtml = "<div class='k_tab_item_body k_box_size' style='display:none;background-color:#fff;position:relative;'></div>";
        let $b = $B.DomUtils.createEl(bhtml);
        let $t = $B.DomUtils.createEl(thtml);
        if(this.opts.bodyItemStyle){
            $B.Dom.css($b,this.opts.bodyItemStyle);
        }        
        let attr = { index: i };
        if (tab.id) {
            attr["id"] = tab.id;
        }
        $B.DomUtils.attribute($t, attr);
        $B.DomUtils.attribute($b, attr);
        if (tab.text) {
            $t.firstChild.innerHTML = tab.text;
        }
        if (tab.iconCls) {
            $B.DomUtils.prepend($t, "<i style='line-height:" + lineHeight + "px' class='fa " + tab.iconCls + "'></i>");
        }       
        if (tab.content) {
            if ($B.DomUtils.isElement(tab.content)) {
                if (tab.content.parentNode) {
                    $B.DomUtils.detach(tab.content);
                }
                $B.DomUtils.append($b, tab.content);
            } else {
                $b.innerHTML = tab.content;
            }
        }
        if (this.opts.autoScroll && $B.myScrollbar && tab.dataType !== "iframe") {
            let $c = $B.myScrollbar($b, {});
            $B.DomUtils.addClass($b, "k_tab_diyscroll");
            $B.DomUtils.css($b, { height: "100%" });
            if(tab.height){
                $B.Dom.css($c,{"height":tab.height});
            }
        }
        let clzz = "k_tab_it_" + this.opts.position + "_" + this.opts.style;
        $B.DomUtils.addClass($t, clzz);
        onCreatedFn($t, $b);
        if (this.opts.style === "normal") {
            if (i !== 0) {
                $B.DomUtils.css($t, { "border-left": "none" });
            }
        }
        if (this.opts.headerBackColor) {
            $B.DomUtils.css($t, { "border-top": "none" });
        }
        if (this.opts.closeable && !tab.fixed) {
            this._bindCloseable($t);
        }
        return $t;
    }
    /**
     * 水平tab ui
     * ***/
    readerHoriz() {
        var tabs = this.opts.tabs;
        var activedIt;
        for (let i = 0; i < tabs.length; i++) {
            let tab = tabs[i];
            let $t = this._createHorizItem(tab, i, ($t, $b) => {
                $B.DomUtils.append(this.tabHeader.firstChild, $t);
                $B.DomUtils.append(this.tabBody, $b);
            });
            if (i === 0 || tab.actived) {
                activedIt = $t;
            }
        }
        if (this.opts.style === "plain") {
            //采用下划线模式
            $B.DomUtils.css(this.tabHeader.firstChild, { "border-bottom": "1px solid #DADCE0" });
            this.$actLine = $B.DomUtils.createEl("<div class='k_tabs_act_line' style='display:inline-block;position:absolute;bottom:0px;height:2px;left:-100px;width:50px;'></div>");
            $B.DomUtils.append(this.tabHeader, this.$actLine);           
        } else { //normal
            let actLinePos = "bottom";
            if (this.opts.position === "bottom") {
                actLinePos = "top";
            }
            $B.DomUtils.css(this.tabHeader.firstChild, { "border-bottom": "1px solid #DADCE0" });
            this.$actLine = $B.DomUtils.createEl("<div class='k_tabs_act_line' style='display:inline-block;position:absolute;" + actLinePos + ":0px;height:2px;left:-300px;width:50px;background-color:" + this.opts.actItemBackColor + "'></div>");
            $B.DomUtils.append(this.tabHeader, this.$actLine);
            this.opts.moreBtnCss["border-top"] = "1px solid #DADCE0";
            this.opts.moreBtnCss["border-right"] = "1px solid #DADCE0";
            this.opts.moreBtnCss["border-left"] = "1px solid #DADCE0";
        }
        this.$activeted = activedIt;
        if (this.opts.headerBackColor) {
            $B.DomUtils.css(this.tabHeader.firstChild, { "border-right": "1px solid #DADCE0" });
            $B.DomUtils.css(this.tabHeader.firstChild, { "border-top": "1px solid #DADCE0" });
            $B.DomUtils.css(this.tabHeader, { "background-color": this.opts.headerBackColor });
        }
        if(this.opts.headerStyle){
            $B.DomUtils.css(this.tabHeader.firstChild, this.opts.headerStyle);
        }
        //右键菜单功能
        $B.DomUtils.contextmenu(document, (e) => {
            let el = e.target;
            if ($B.DomUtils.hasClass(el, "k_tab_item") || $B.DomUtils.hasClass(el.parentNode, "k_tab_item")) {
                return false;
            }
        });
    }
    readerUI() {
        var position = this.opts.position;
        this.tabHeader = $B.DomUtils.createEl("<div class='k_tab_header k_box_size'><div class='k_box_size k_tabs_head_it_wrap' style='min-width:100%;height:100%;position:relative;'></div></div>");
        this.tabBody = $B.DomUtils.createEl("<div class='k_tab_body k_box_size'></div>"); 
        if(this.opts.bodyStyle){
            $B.Dom.css($b,this.opts.bodyStyle);
        }       
        var headerCss, bodyCss;
        if (position === "top") {
            headerCss = { top: 0, left: 0, width: '100%', height: this.opts.headSize, "white-space": 'nowrap', "overflow": "hidden" };
            bodyCss = { "border-top": this.opts.headSize + " solid #ffffff" };
            $B.DomUtils.css(this.tabHeader.firstChild, { "white-space": 'nowrap' });
            this.readerHoriz();
        } else if (position === "bottom") {
            headerCss = { bottom: 0, left: 0, width: '100%', height: this.opts.headSize, "white-space": 'nowrap', "overflow": "hidden" };
            bodyCss = { "border-bottom": this.opts.headSize + " solid #ffffff" };
            $B.DomUtils.css(this.tabHeader.firstChild, { "white-space": 'nowrap' });
            this.readerHoriz();
        } else if (position === "left") {
            headerCss = { top: 0, left: 0, height: '100%', width: this.opts.headSize };
            bodyCss = { "border-left": this.opts.headSize + " solid #ffffff" };
            this.readerVertical();
        } else if (position === "right") {
            headerCss = { top: 0, right: 0, width: '100%', width: this.opts.headSize };
            bodyCss = { "border-right": this.opts.headSize + " solid #ffffff" };
            this.readerVertical();
        }
        $B.DomUtils.css(this.tabHeader, headerCss);
        $B.DomUtils.css(this.tabBody, bodyCss);
        $B.DomUtils.append(this.elObj, this.tabBody);
        $B.DomUtils.append(this.elObj, this.tabHeader);
        if(this.opts.headerStyle){
            $B.DomUtils.css(this.tabHeader.firstChild, this.opts.headerStyle);
        }
        if (this.opts.ctxmenu && (position === "top" || position === "bottom")) {
            $B.DomUtils.mouseup(this.tabHeader, (e) => {
                if (e.which === 3) {
                    let el = e.target;
                    if ($B.DomUtils.hasClass(el, "k_tab_header")) {
                        return true;
                    }
                    while (el) {
                        if ($B.DomUtils.hasClass(el, "k_tab_item")) {
                            break;
                        }
                        el = el.parentNode;
                    }
                    if (!this.$ctxmenu) {
                        this.createCtxMenu();
                        $B.DomUtils.append(document.body, this.$ctxmenu);
                    }
                    this.$oprIt = el;
                    var ofs = $B.DomUtils.offset(el);
                    var h = $B.DomUtils.outerHeight(el);
                    var w = $B.DomUtils.outerWidth(el);
                    ofs.top = ofs.top + h;
                    this.$ctxmenu.style.top = ofs.top + "px";
                    this.$ctxmenu.style.left = ofs.left + "px";
                    if (w > 115) {
                        this.$ctxmenu.style.width = w + "px";
                    } else {
                        this.$ctxmenu.style.width = "115px";
                    }
                    $B.slideDown(this.$ctxmenu, 100);
                }
            });
        }
        $B.DomUtils.click(this.tabHeader, (e) => {
            let el = e.target;
            if ($B.DomUtils.hasClass(el, "k_tab_header")) {
                return true;
            }
            while (el) {
                if ($B.DomUtils.hasClass(el, "k_tab_item")) {
                    break;
                }
                el = el.parentNode;
            }
            this.onClickEv(el);
            if (this.opts.onClick) {
                let id = $B.DomUtils.attribute(el, "id");
                let text = $B.DomUtils.children(el, "span")[0].innerText;
                this.opts.onClick.call(el, id, text);
            }
        });
        var $it = this.$activeted;
        this.$activeted = undefined;
        if (this.$iArray) {
            for (let i = 0; i < this.$iArray.length; i++) {
                let h, $i = this.$iArray[i];
                let $span = $B.DomUtils.children($i.parentNode, "span")[0];
                if (position === "right") {
                    let p_width = $B.DomUtils.innerWidth($i.parentNode);
                    let maxWidth = p_width - 25;
                    $B.DomUtils.width($span, maxWidth);
                    continue;
                }
                h = $B.DomUtils.height($span);
                if (h < 22) {
                    h = 22;
                }
                this.$iArray[i].style.lineHeight = h + "px";
            }
        }
        setTimeout(() => {
            this.checkOverFlow();
            this.onClickEv($it);
        }, 100);
    }
    createCtxMenu() {
        let cfg = $B.config.tabmenu;
        this.$ctxmenu = $B.DomUtils.createEl("<div style='position:absolute;display:none;width:115px;text-align:center;' class='k_dropdown_list_wrap k_tab_ctxmenu_wrap k_box_size'></div>");
        let $flash = $B.DomUtils.createEl("<div f='x_road' class='k_dropdown_list_item k_tab_ctxmenu'><i class='fa " + cfg.reload.icon + "'></i><span>" + cfg.reload.text + "</span></div>");
        let $closeNow = $B.DomUtils.createEl("<div f='x_closen' class='k_dropdown_list_item k_tab_ctxmenu'><i class='fa " + cfg.closeNow.icon + "'></i><span>" + cfg.closeNow.text + "</span></div>");
        let $closeRight = $B.DomUtils.createEl("<div f='x_closer' class='k_dropdown_list_item k_tab_ctxmenu'><i class='fa " + cfg.closeRight.icon + "'></i><span>" + cfg.closeRight.text + "</span></div>");
        let $closeLeft = $B.DomUtils.createEl("<div f='x_closel' class='k_dropdown_list_item k_tab_ctxmenu'><i class='fa " + cfg.closeLeft.icon + "'></i><span>" + cfg.closeLeft.text + "</span></div>");
        let $closeMenu = $B.DomUtils.createEl("<div f='x_closem' class='k_dropdown_list_item k_tab_ctxmenu'><i class='fa " + cfg.closeMenu.icon + "'></i><span>" + cfg.closeMenu.text + "</span></div>");
        let $openTab;
        if(this.opts.openTab){
            $openTab = $B.DomUtils.createEl("<div f='x_open' class='k_dropdown_list_item k_tab_ctxmenu'><i class='fa " + cfg.opened.icon + "'></i><span>" + cfg.opened.text + "</span></div>");
            $B.DomUtils.append(this.$ctxmenu, $openTab);
        }       
        $B.DomUtils.append(this.$ctxmenu, $flash);
        $B.DomUtils.append(this.$ctxmenu, $closeNow);
        $B.DomUtils.append(this.$ctxmenu, $closeRight);
        $B.DomUtils.append(this.$ctxmenu, $closeLeft);
        $B.DomUtils.append(this.$ctxmenu, $closeMenu);
        $B.DomUtils.click(this.$ctxmenu, (e) => {
            let el = e.target;
            if ($B.DomUtils.hasClass(el, "k_dropdown_list_wrap")) {
                return true;
            }
            while (el) {
                if ($B.DomUtils.hasClass(el, "k_dropdown_list_item")) {
                    break;
                }
                el = el.parentNode;
            }
            let f = $B.DomUtils.attribute(el, "f");
            if (f === "x_open") {
                this.openTabEv(this.$oprIt);
            }else if (f === "x_road") {
                this.onClickEv(this.$oprIt, true);
            } else if (f === "x_closen") {
                let $c = $B.DomUtils.children(this.$oprIt, ".fa-cancel-1");
                if ($c.length > 0) {
                    $B.DomUtils.trigger($c[0], "click");
                    this.checkOverFlow();
                }
            } else if (f === "x_closer") {
                let $opr = this.$oprIt;
                this.onClickEv($opr, () => {
                    let nextEl = $opr.nextSibling;
                    while (nextEl) {                     
                        let $c = $B.DomUtils.children(nextEl, ".fa-cancel-1");
                        let nEl = nextEl.nextSibling;
                        if ($c.length > 0) {                            
                            this._exeRmItem(nextEl);
                        }    
                        nextEl = nEl;                   
                    }
                    this.checkOverFlow();                    
                });
            } else if (f === "x_closel") {
                let $opr = this.$oprIt;
                this.onClickEv($opr, () => {
                    let preEl = $opr.previousSibling;
                    while (preEl) {
                        let $c = $B.DomUtils.children(preEl, ".fa-cancel-1");
                        let _preEl = preEl.previousSibling;
                        if ($c.length > 0) {
                            this._exeRmItem(preEl);
                            this._resetIndex(this.$activeted);
                        }   
                        preEl = _preEl;                    
                    }
                    this.checkOverFlow();
                    this._updateHActLinePos();
                });
            }
            $B.slideUp(this.$ctxmenu, 100, () => {
                this.$oprIt = undefined;
            });
        });
        $B.createGlobalBodyHideEv();
    }
    _bindMoreBtnEvent() {
        if (!this.rlevents) {
            this.rlevents = (e) => {
                let btn = e.target;
                if (btn.tagName === "I") {
                    btn = btn.parentNode;
                }
                if ($B.DomUtils.hasClass(btn, "k_tabs_more_disabled")) {
                    return false;
                }
                let childrens = this.tabHeader.firstChild.children;
                let isleft = $B.DomUtils.hasClass(btn, "_more_btn_l_");
                let moveLeft = 0;
                let nowLeft = $B.DomUtils.position(this.tabHeader.firstChild).left;
                let actEl, left, w, shiftLeft;
                if (isleft) {
                    let shiftLeft = Math.abs(nowLeft);
                    let shiftVal = 0;
                    for (let i = 0; i < childrens.length; i++) {
                        left = $B.DomUtils.position(childrens[i]).left;
                        w = $B.DomUtils.outerWidth(childrens[i]);
                        if (left > shiftLeft || (left + w) > shiftLeft) {
                            actEl = childrens[i];
                            if (actEl.previousSibling) {
                                actEl = actEl.previousSibling;
                                shiftVal = shiftVal - $B.DomUtils.outerWidth(actEl);
                            }
                            break;
                        } else {
                            shiftVal = shiftVal + w;
                        }
                    }
                    if (shiftVal > 0) {
                        moveLeft = -shiftVal;
                    }
                    $B.DomUtils.removeClass(this.$rbtn, "k_tabs_more_disabled");
                    $B.DomUtils.addClass(this.$rbtn.firstChild, "enabled");
                } else {
                    shiftLeft = nowLeft - this.opts.moreBtnSize;
                    let headWidth = $B.DomUtils.innerWidth(this.tabHeader);
                    for (let i = 0; i < childrens.length; i++) {
                        left = $B.DomUtils.position(childrens[i]).left + shiftLeft;
                        w = $B.DomUtils.outerWidth(childrens[i]);
                        if (Math.floor(left + w) > headWidth || left > headWidth) {
                            actEl = childrens[i];
                            break;
                        }
                    }
                    if (!actEl) {
                        return false;
                    }
                    if (!actEl.nextSibling) {//禁用右侧按钮
                        $B.DomUtils.addClass(this.$rbtn, "k_tabs_more_disabled");
                        $B.DomUtils.removeClass(this.$rbtn.firstChild, "enabled");
                    }
                    moveLeft = headWidth - left - w;
                    moveLeft = moveLeft + shiftLeft;
                }
                if (moveLeft > 0) {
                    moveLeft = 0;
                }
                $B.animate(this.tabHeader.firstChild, { left: moveLeft }, {
                    duration: 120, complete: () => {
                        if (this.$actLine) {
                            this._updateHActLinePos();
                        }
                    }
                });
                //修改左侧按钮状态
                if (moveLeft < 0) {
                    $B.DomUtils.removeClass(this.$lbtn, "k_tabs_more_disabled");
                    $B.DomUtils.addClass(this.$lbtn.firstChild, "enabled");
                } else {
                    $B.DomUtils.addClass(this.$lbtn, "k_tabs_more_disabled");
                    $B.DomUtils.removeClass(this.$lbtn.firstChild, "enabled");
                }
                return false;
            };
        }
        $B.DomUtils.click(this.$lbtn, this.rlevents);
        $B.DomUtils.click(this.$rbtn, this.rlevents);
    }
    checkOverFlow() {
        var position = this.opts.position;
        var $warp = this.tabHeader.firstChild;
        var childs = $warp.children;
        //水平位置
        if (position === "top" || position === "bottom") {
            var headWidth = Math.ceil($B.DomUtils.width(this.tabHeader));
            let allWidth = 0;
            for (let i = 0; i < childs.length; i++) {
                let w = Math.ceil($B.DomUtils.outerWidth(childs[i]));
                allWidth = allWidth + w;
            }
            if (allWidth > headWidth) {
                $B.DomUtils.css($warp, { width: allWidth });
                if (!this.$lbtn) {
                    let h = $B.DomUtils.height(this.tabHeader);
                    let $l = $B.DomUtils.createEl("<span style='left:0;' class='k_tabs_more_btn _more_btn_l_ k_tabs_more_disabled k_box_size'><i style='line-height:" + h + "px' class='fa fa-angle-double-left'></i></span>");
                    let $r = $B.DomUtils.createEl("<span style='right:0;' class='k_tabs_more_btn _more_btn_r_ k_box_size'><i style='line-height:" + h + "px' class='fa fa-angle-double-right enabled'></i></span>");
                    let pSize = this.opts.moreBtnSize + "px";
                    $B.DomUtils.css(this.tabHeader, { "padding-left": pSize, "padding-right": pSize });
                    $B.DomUtils.append(this.tabHeader, $l);
                    $B.DomUtils.append(this.tabHeader, $r);
                    $B.DomUtils.css($l, this.opts.moreBtnCss);
                    $B.DomUtils.css($l, { "border-right": "none" });
                    $B.DomUtils.css($r, this.opts.moreBtnCss);
                    this.morePaddLeft = this.opts.moreBtnSize;
                    this.$lbtn = $l;
                    this.$rbtn = $r;
                    this._bindMoreBtnEvent();
                    if (this.$actLine) {
                        let actLeft = $B.DomUtils.position(this.$actLine).left + this.opts.moreBtnSize;
                        $B.DomUtils.css(this.$actLine, { left: actLeft });
                    }
                }
            } else {
                if (this.$lbtn) {
                    $B.DomUtils.css($warp, { width: "100%" });
                    $B.DomUtils.css(this.tabHeader, { "padding-left": "0px", "padding-right": "0px" });
                    this.morePaddLeft = 0;
                    $B.DomUtils.remove(this.$lbtn);
                    $B.DomUtils.remove(this.$rbtn);
                    this.$lbtn = undefined;
                    this.$rbtn = undefined;
                    if (this.$actLine) {
                        let actLeft = $B.DomUtils.position(this.$actLine).left - this.opts.moreBtnSize;
                        $B.DomUtils.css(this.$actLine, { left: actLeft });
                    }
                }
                if ($warp.style.left !== "0px") {
                    if ($warp.style.left !== "") {
                        let shift = Math.abs(parseFloat($warp.style.left.replace("px", "")));
                        let left = $B.DomUtils.css(this.$actLine, "left") + shift;
                        this.$actLine.style.left = left + "px";
                    }
                    $warp.style.left = "0px";
                }
            }
        } else {//垂直位置

        }
    }
    _clearSytleProp(el, propName) {
        let styleObj = $B.style2cssObj(el);
        delete styleObj[propName];
        let styleAtrr = $B.cssObj2string(styleObj);
        $B.DomUtils.attribute(el, { "style": styleAtrr });
    }
    openTabEv($it){       
        let idx = parseInt($B.DomUtils.attribute($it, "index"));
        let opt = this.opts.tabs[idx];
        if(opt.url){
            window.open(opt.url, opt.text);
        }else{
            $B.alert($B.config.urlError);
        }
    }
    onClickEv($it, completeFN) {
        var activedCls = "k_tab_" + this.opts.position + "_" + this.opts.style + "_act";
        if (this.$activeted) {
            $B.DomUtils.removeClass(this.$activeted, activedCls);
            if (this.opts.actItemBackColor) {
                this._clearSytleProp(this.$activeted, "background-color");
            }
            if (this.opts.actItemFontColor) {
                let childs = Array.from(this.$activeted.children);
                for (let i = 0; i < childs.length; i++) {
                    this._clearSytleProp(childs[i], "color");
                }
            }
        }
        this.$activeted = $it;
        $B.DomUtils.addClass(this.$activeted, activedCls);
        if (this.opts.actItemBackColor) {
            $B.DomUtils.css(this.$activeted, { "background-color": this.opts.actItemBackColor });
        }
        if (this.opts.actItemFontColor) {
            let childs = Array.from(this.$activeted.children);
            for (let i = 0; i < childs.length; i++) {
                let el = childs[i];
                let styleObj = $B.style2cssObj(el);
                styleObj["color"] = this.opts.actItemFontColor + " !important";
                let styleAtrr = $B.cssObj2string(styleObj);
                $B.DomUtils.attribute(el, { "style": styleAtrr });
            }
        }
        if (this.$actLine) {
            if (this.$actLine.style.display === "none") {
                this.$actLine.style.display = "inline-block";
            }
            let comFN = undefined;
            if (typeof completeFN === "function") {
                comFN = completeFN;
            }
            this._updateHActLinePos(comFN);
        }
        let idx = parseInt($B.DomUtils.attribute(this.$activeted, "index"));
        let opt = this.opts.tabs[idx];
        let childs = Array.from(this.tabBody.children);
        let actBody = childs[idx];
        if (!actBody) {
            console.log("索引错误！");
            return;
        }  
        let onShow = () => {
            let byEl = actBody;
            if ($B.DomUtils.hasClass(byEl, "k_tab_diyscroll")) {
                byEl = actBody.firstChild.firstChild;
            }
            if (opt.url) {
                if (this.opts.reload || !byEl.firstChild || (typeof completeFN !== "function" && completeFN)) {
                    this._load(byEl, opt);
                }
            }
        };
        if (this.$actbody === actBody) {
            onShow();
        } else {
            if (this.$actbody) {   
                if(this.opts.animate){
                    let $prtEl = this.$actbody.parentNode;
                    let prevEl = actBody.previousSibling;
                    let isPrev = false;
                    while(prevEl){
                        if(this.$actbody === prevEl){
                            isPrev = true;
                            break;
                        }
                        prevEl = prevEl.previousSibling;
                    }                  
                    $prtEl.style.overflow = "hidden"; 
                    let height =  $B.Dom.outerHeight(this.$actbody);
                    onShow();
                    if(isPrev){
                        height = -height;   
                        actBody.style.display = "";                    
                    }
                    $B.animate(this.$actbody,{top:height},{
                        duration:300, 
                        progress:()=>{
                           let top = this.$actbody.style.top; 
                           if(isPrev){
                                actBody.style.top = top; 
                           }else{
                                top = parseFloat(top.replace("px","")) - height;                                
                                actBody.style.top = top +"px"; 
                                actBody.style.display = "";
                           }                                                   
                        },
                        complete:()=>{
                            this.$actbody.style.display = "none";  
                            this.$actbody.style.top = "0px";  
                            actBody.style.top = "0px"; 
                            this.$actbody = actBody;  
                            $prtEl.style.overflow = "auto";                                        
                        }
                    });                                    
                }else{
                    this.$actbody.style.display = "none";
                    actBody.style.display = "";
                    onShow();
                    this.$actbody = actBody;
                }
            } else {
                actBody.style.display = "";
                onShow();
                this.$actbody = actBody;
            }          
        }
    }
    _updateHActLinePos(completeFN) {
        let isHoriUI = this.opts.position === "top" || this.opts.position === "bottom";
        let size;
        if (isHoriUI) {
            let w = $B.DomUtils.outerWidth(this.$activeted);
            if (this.opts.style === "normal") {
                w = w - 1;
            }
            size = w + "px";
        } else {
            let w = $B.DomUtils.outerHeight(this.$activeted);
            if (this.opts.style === "normal") {
                w = w - 1;
            }
            size = w + "px";
        }
        let preEl = this.$activeted.previousSibling;
        let shiftLeft = $B.DomUtils.position(this.tabHeader.firstChild).left;
        let pos = shiftLeft - this.morePaddLeft;
        while (preEl) {
            if (isHoriUI) {
                pos = pos + $B.DomUtils.outerWidth(preEl);
            } else {
                pos = pos + $B.DomUtils.outerHeight(preEl);
            }
            preEl = preEl.previousSibling;
        }
        let lineCss;
        if (isHoriUI) {
            pos = this.morePaddLeft + pos;
            lineCss = { width: size, left: pos + "px" };
        } else {
            $B.DomUtils.attribute(this.$actLine, { "top": pos });
            let srcolltop = $B.DomUtils.scrollTop(this.tabHeader.firstChild.firstChild);
            pos = pos - srcolltop;
            lineCss = { height: size, top: pos + "px" };
        }
        let aniOpt = { duration: 360 };
        if (completeFN) {
            aniOpt.complete = completeFN;
        }
        $B.animate(this.$actLine, lineCss, aniOpt);
    }
    _load(el, opt) {
        //console.log("onShow url " + opt.url);
        let url = opt.url;
        var dataType = opt.dataType ? opt.dataType : "html";
        var isFun = typeof this.opts.onLoaded === 'function';
        if (url.indexOf("?") > 0) {
            url = url + "&_t_=" + $B.generateMixed(5);
        } else {
            url = url + "?_t_=" + $B.generateMixed(5);
        }
        let wrap = el;
        let isNewIfr = false;
        var loading = $B.getLoadingEl();
        if (dataType === "iframe") {
            if (!this.ifrIdex) {
                this.ifrIdex = 1;
            } else {
                this.ifrIdex++;
            }
            let ifrEL = $B.DomUtils.children(wrap, "iframe");
            if (ifrEL.length === 0) {
                ifrEL = $B.getIframeEl("k_tabs_content_ifr");
                $B.DomUtils.css(el, { height: "100%", "overflow": "auto" });
                let ifrId = this.id + "_ifr_" + this.ifrIdex;
                $B.DomUtils.attribute(ifrEL, { "name": ifrId, "id": ifrId });
                $B.DomUtils.append(wrap, ifrEL);
                isNewIfr = true;
                wrap = ifrEL;
                if (!this.opts.ifrScroll) {
                    $B.DomUtils.attribute(ifrEL, { "scrolling": "no" });
                    $B.DomUtils.bind(ifrEL, {
                        mouseenter: () => {
                            $B.DomUtils.attribute(ifrEL, { "scrolling": "auto" });
                        },
                        mouseleave: () => {
                            $B.DomUtils.attribute(ifrEL, { "scrolling": "no" });
                        }
                    });
                }else{
                    $B.DomUtils.attribute(ifrEL, { "scrolling": this.opts.ifrScroll });
                }
            } else {
                wrap = ifrEL[0];
                wrap.contentDocument.body.innerHTML = "<p></p>";
            }
        }
        $B.DomUtils.prepend(el, loading);
        var _this = this;
        setTimeout(() => {
            if (dataType === "html") {
                $B.htmlLoad({
                    url: url,
                    success: function (res) {
                        wrap.innerHTML = res;
                        setTimeout(()=>{
                            if (isFun) {
                                _this.opts.onLoaded.call(wrap, opt, {});
                            }
                        },1);                        
                        $B.bindInputClear(wrap);
                    },
                    complete: function () {
                        $B.removeLoading(loading, () => {
                            loading = undefined;
                        });
                    }
                }, wrap);
            } else if (dataType === "json") {
                let method = (url.indexOf(".data") > 0 || url.indexOf(".json") > 0) ? "GET" : "POST";
                $B.request({
                    dataType: 'json',
                    url: url,
                    type: method,
                    onErrorEval: true,
                    ok: function (message, data) {
                        if (isFun) {
                            _this.opts.onLoaded.call(wrap, opt, data);
                        }
                    },
                    final: function (res) {
                        try {
                            $B.removeLoading(loading, () => {
                                loading = undefined;
                            });
                        } catch (ex) {
                        }
                    },
                    fail: function (arg1, arg2, arg3) {
                    }
                });
            } else {
                if (isNewIfr) {
                    $B.DomUtils.onload(wrap, () => {
                        var url = $B.DomUtils.attribute(wrap, "src");
                        if (url !== "") {                           
                            try {
                                loading = wrap.previousSibling;
                                $B.removeLoading(loading, () => {
                                    loading = undefined;
                                });
                            } catch (ex) {
                            }
                            let ifrBody = wrap.contentDocument.body;
                            if (isFun) {
                                _this.opts.onLoaded.call(ifrBody, opt, wrap);
                            }
                            // var ua = window.navigator.userAgent;
                            // var isFirefox = ua.indexOf("Firefox") !== -1;
                            // try {
                            //     var ifrBody = this.contentDocument.body;
                            //     $B.DomUtils.append(ifrBody, "<span id='_window_ifr_id_' style='display:none'>" + ifrId + "</span>");
                            //     if (isFirefox) {
                            //         // ifrBody.find("a").each(function () {
                            //         //     var $a = $(this);
                            //         //     if ($a.attr("href").toLowerCase().indexOf("javascript") > -1) {
                            //         //         $a.attr("href", "#");
                            //         //     }
                            //         // });
                            //     }
                            // } catch (ex) { }
                        }
                    });
                }
                wrap.src = url;
            }
        }, 1);
    }
    destroy(isForce) {
        if (this.$ctxmenu) {
            $B.DomUtils.offEvents(this.$ctxmenu);
            $B.DomUtils.remove(this.$ctxmenu);
        }
        super.destroy(isForce);
    }
}
$B["Tabs"] = Tabs; 
 if(!window["$B"] ) {window["$B"] = $B;} 
  return window["$B"] ; 
});

/*! BUI - v2.0.0 - 2022-03-13 09:03:17 
Copyright (c): kevin.huang  https://gitee.com/kevin-huang/bui2.0.git (qq:757723114) 
Released under MIT License*/
(function ( global, factory) {
//   "use strict";
	if ( typeof module === "object" && typeof module.exports === "object" ) {
		module.exports = global.document ?
			factory( global, true ) :
			function( w ) {
				if ( !w.document ) {
					throw new Error( "Bui requires a window with a document" );
				}
				return factory( w );
			};
	} else {
		  factory( global );
	}  
})( typeof window !== "undefined" ? window : this, function( window, noGlobal ) {
//   "use strict";
  var $B = window["$B"] ?  window["$B"] : {};

var defaultOpts = {
    pageSize: 15, //页大小
    pageList: [15, 20, 25, 30, 35, 40, 55, 70, 80, 90, 100], //页大小项
    page: 1, //当前页
    total: 101, //总记录
    buttons: 10, //页按钮数量
    position: 'left', // right center left
    buttonCss: undefined,
    simpleStyle: false,
    summary: true, //是否需要页大小，页总数汇总显示
    onClick: undefined
};
function _createNumsFn(ins) {
    var opt = ins.opts;
    if (opt.page > this.pageCount) {
        opt.page = 1;
    }
    var fragment = document.createDocumentFragment();
    var i = 0;
    //根据当前页，和页大小，总页数计算出开始页 
    var tmp = opt.page;
    while (tmp >= 0) {
        if ((tmp % opt.buttons) === 0 && tmp !== opt.page) {
            break;
        }
        tmp--;
    }
    var startPage = tmp + 1;
    var firstNumEl, lastNumEl;
    while (i < opt.buttons) {
        if (startPage > ins.pageCount) {
            break;
        }
        let $n = $B.DomUtils.createEl("<span class='k_pagination_num'>" + startPage + "</span>");
        if (!firstNumEl) {
            firstNumEl = $n;
        }
        lastNumEl = $n;
        if (startPage === opt.page) {
            ins.setCurrentPage($n);
        }
        if (opt.buttonCss) {
            $B.DomUtils.css($n, opt.buttonCss);
        }
        fragment.appendChild($n);
        startPage++;
        i++;
    }
    $B.DomUtils.removeChilds(ins.$NumSpan);
    $B.DomUtils.append(ins.$NumSpan, fragment);
}
function _createTools(ins) {
    let $el;
    if (ins.opts.summary && !ins.opts.simpleStyle) {
        let sumaryTxt = $B.config && $B.config.paginationSummary ? $B.config.paginationSummary : 'record:{x}，{y}pages';
        sumaryTxt = sumaryTxt.replace('{x}', '<span class="k_pg_total">' + ins.opts.total + '</span>');
        sumaryTxt = sumaryTxt.replace('{y}', '<span class="k_pg_pagesize k_dropdown_list_el"><span>' + ins.opts.pageSize + '</span><i class="fa fa-down-open-big k_dropdown_list_el"></i></span>');
        sumaryTxt = '<span class="k_pg_summary">' + sumaryTxt + '</span>';
        $el = $B.DomUtils.append(ins.$prevSpan, sumaryTxt);
        ins.$total = $B.DomUtils.findByClass($el, ".k_pg_total");
        ins.$pageSize = $B.DomUtils.findByClass($el, ".k_pg_pagesize")[0];
    }
    //首页
    if (!ins.opts.simpleStyle) {
        let firstPageTxt = $B.config && $B.config.firstPageTxt ? $B.config.firstPageTxt : 'firstPage';
        $el = $B.DomUtils.createEl("<span style='cursor:pointer' class='k_pg_first_page'>" + firstPageTxt + "</span>");
        $B.DomUtils.append(ins.$prevSpan, $el);
        ins.$firstPageBtn = $el;
    }
    //前进一个批次
    $el = $B.DomUtils.createEl("<span class='k_pg_prev_pages'><i style='padding:0 5px;' class='fa-angle-double-left'></i></span>");
    $B.DomUtils.append(ins.$prevSpan, $el);
    ins.$prevPagesBtn = $el;

    //前一页功能
    $el = $B.DomUtils.createEl("<span class='k_pg_prev_page'><i style='padding:0 5px;' class='fa-angle-left'></i></span>");
    $B.DomUtils.append(ins.$prevSpan, $el);
    ins.$prevPageBtn = $el;

    //后一页功能
    $el = $B.DomUtils.createEl("<span class='k_pg_next_page'><i style='padding:0 5px;' class='fa-angle-right'></i></span>");
    $B.DomUtils.append(ins.$nextSpan, $el);
    ins.$nextPageBtn = $el;

    //后进一个批次
    $el = $B.DomUtils.createEl("<span class='k_pg_next_pages'><i style='padding:0 5px;' class='fa-angle-double-right'></i></span>");
    $B.DomUtils.append(ins.$nextSpan, $el);
    ins.$nextPagesBtn = $el;

    if (!ins.opts.simpleStyle) {
        //最后一页 
        let lastPageTxt = $B.config && $B.config.lastPageTxt ? $B.config.lastPageTxt : 'lastPage';
        $el = $B.DomUtils.createEl("<span style='cursor:pointer' class='k_pg_last_page'>" + lastPageTxt + "</span>");
        $B.DomUtils.append(ins.$nextSpan, $el);
        ins.$lastPageBtn = $el;
    }
}
class Pagination extends $B.BaseControl {
    constructor(elObj, opts) {
        //console.log("create Pagination >>>>>>>>>>>>>>");
        super();
        this.opts = $B.extendObjectFn(true, {}, defaultOpts, opts);
        super.setElObj(elObj);
        $B.DomUtils.addClass(this.elObj, "k_pagination_wrap k_disabled_selected");
        $B.DomUtils.css(this.elObj, { "text-align": this.opts.position });
        this.$prevSpan = $B.DomUtils.append(this.elObj, "<span class='k_pg_prevspan'></span>");
        this.$NumSpan = $B.DomUtils.append(this.elObj, "<span></span>");
        this.$nextSpan = $B.DomUtils.append(this.elObj, "<span class='k_pg_nextspan'></span>");     
        _createTools(this);
        this.countPage();
        _createNumsFn(this);
        this.go2page(this.opts.page);
        this.bindEvents();
    }
    setCurrentPage(el) {
        if (this.currentBtn) {
            $B.DomUtils.removeClass(this.currentBtn, "k_pagination_actived");
        }
        $B.DomUtils.addClass(el, "k_pagination_actived");
        this.currentBtn = el;
    }
    countPage() {
        this.pageCount = Math.ceil(this.opts.total / this.opts.pageSize) || 1; //总页数 
    }
    dropdownList() {
        if (!this.pageItems) {
            this.pageItems = [];
            var isFined = false;
            for (let i = 0; i < this.opts.pageList.length; i++) {
                let it = { id:i, text: this.opts.pageList[i] };
                this.pageItems.push(it);
                if (this.opts.pageSize === this.opts.pageList[i]) {
                    isFined = true;
                    it["selected"] = true;
                }
            }
            if (!isFined) {
                this.pageItems.unshift({id:this.opts.pageSize, text: this.opts.pageSize, selected: true });
            }
            this.$dropList =  $B.createDropList(this.$pageSize, this.pageItems,
                {
                    onClick: (data, el) => {
                        this.$pageSize.firstChild.innerText = data.text;
                        this.opts.pageSize = parseInt(data.text);
                        if (this.opts.onClick) {
                            setTimeout(() => {
                                this.opts.page = 1;
                                this.opts.onClick(this.opts.page, this.opts.pageSize);
                            }, 10);
                        }
                        return true;
                    }
                });
        }
    }
    go2page(page) {
        this.opts.page = page;
        let ret;
        if (this.$prevPageBtn) {
            if (page === 1) {
                $B.DomUtils.addClass(this.$prevPageBtn, "k_pg_forbid_css");
                $B.DomUtils.addClass(this.$prevPageBtn.firstChild, "k_pg_forbid_css");
            } else {
                $B.DomUtils.removeClass(this.$prevPageBtn, "k_pg_forbid_css");
                $B.DomUtils.removeClass(this.$prevPageBtn.firstChild, "k_pg_forbid_css");
            }
        }
        if (this.$nextPageBtn) {
            if (page === this.pageCount) {
                $B.DomUtils.addClass(this.$nextPageBtn, "k_pg_forbid_css");
                $B.DomUtils.addClass(this.$nextPageBtn.firstChild, "k_pg_forbid_css");
            } else {
                $B.DomUtils.removeClass(this.$nextPageBtn, "k_pg_forbid_css");
                $B.DomUtils.removeClass(this.$nextPageBtn.firstChild, "k_pg_forbid_css");
            }
        }
        //console.log("go2page " + page);
        let num1 = parseInt(this.$NumSpan.firstChild.innerText);
        let num2 = parseInt(this.$NumSpan.lastChild.innerText);
        if (page < num1 || page > num2) {
            _createNumsFn(this);
            ret = true;
        }
        num1 = parseInt(this.$NumSpan.firstChild.innerText);
        num2 = parseInt(this.$NumSpan.lastChild.innerText);
        //前一批次，后一批次ui更新
        if (this.$prevPagesBtn) {
            if (num1 === 1) { //位于第一页批次
                $B.DomUtils.addClass(this.$prevPagesBtn, "k_pg_forbid_css");
                $B.DomUtils.addClass(this.$prevPagesBtn.firstChild, "k_pg_forbid_css");
            } else {
                $B.DomUtils.removeClass(this.$prevPagesBtn, "k_pg_forbid_css");
                $B.DomUtils.removeClass(this.$prevPagesBtn.firstChild, "k_pg_forbid_css");
            }
        }
        if (this.$nextPagesBtn) {
            if ((num1 + this.opts.buttons) >= this.pageCount) {
                $B.DomUtils.addClass(this.$nextPagesBtn, "k_pg_forbid_css");
                $B.DomUtils.addClass(this.$nextPagesBtn.firstChild, "k_pg_forbid_css");
            } else {
                $B.DomUtils.removeClass(this.$nextPagesBtn, "k_pg_forbid_css");
                $B.DomUtils.removeClass(this.$nextPagesBtn.firstChild, "k_pg_forbid_css");
            }
        }
        return ret;
    }
    prevOrNextList(el, css) {
        if ($B.DomUtils.hasClass(el, css) || $B.DomUtils.hasClass(el.parentNode, css)) {
            let newPage;
            if (css === "k_pg_prev_pages") {//前一批次
                newPage = parseInt(this.$NumSpan.firstChild.innerText) - 1;
            } else {//后一批次
                newPage = parseInt(this.$NumSpan.lastChild.innerText) + 1;
            }
            this.go2page(newPage);
            return true;
        }
    }
    prevOrNext(el, css) {
        if ($B.DomUtils.hasClass(el, css) || $B.DomUtils.hasClass(el.parentNode, css)) {
            let page = css === "k_pg_prev_page" ? this.opts.page - 1 : this.opts.page + 1;
            if (page < 1 || page > this.pageCount) {
                return true;
            }
            if (this.currentBtn) {
                $B.DomUtils.removeClass(this.currentBtn, "k_pagination_actived");
            }
            if (css === "k_pg_prev_page" && this.currentBtn.previousSibling) {
                this.currentBtn = this.currentBtn.previousSibling;
                $B.DomUtils.addClass(this.currentBtn, "k_pagination_actived");
            } else if (this.currentBtn.nextSibling) {
                this.currentBtn = this.currentBtn.nextSibling;
                $B.DomUtils.addClass(this.currentBtn, "k_pagination_actived");
            }
            this.go2page(page);
            return true;
        }
    }
    bindEvents() {
        $B.DomUtils.bind(this.elObj, {
            click: (e) => {
                let el = e.target;
                if ($B.DomUtils.hasClass(el, "k_pagination_wrap") || $B.DomUtils.hasClass(el, "k_pg_forbid_css") 
                || $B.DomUtils.hasClass(el, "k_pg_summary") || $B.DomUtils.hasClass(el, "k_pg_total") || $B.DomUtils.hasClass(el, "k_pg_pagesize")) {
                    return false;
                }
                if ($B.DomUtils.hasClass(el.parentNode, "k_pg_pagesize")) {
                    this.dropdownList();
                    return false;
                }
                if (this.opts.onClick) {
                    setTimeout(() => {
                        this.opts.onClick(this.opts.page, this.opts.pageSize);
                    }, 10);
                }
                if (this.prevOrNext(el, "k_pg_prev_page")) {
                    return false;
                }
                if (this.prevOrNext(el, "k_pg_next_page")) {
                    return false;
                }
                if (this.prevOrNextList(el, "k_pg_prev_pages")) {
                    return false;
                }
                if (this.prevOrNextList(el, "k_pg_next_pages")) {
                    return false;
                }
                if (this.currentBtn) {
                    $B.DomUtils.removeClass(this.currentBtn, "k_pagination_actived");
                }
                if ($B.DomUtils.hasClass(el, "k_pg_first_page")) {
                    if (!this.go2page(1)) {
                        this.currentBtn = this.$NumSpan.firstChild;
                        $B.DomUtils.addClass(this.currentBtn, "k_pagination_actived");
                    }
                    return false;
                }
                if ($B.DomUtils.hasClass(el, "k_pg_last_page")) {
                    if (!this.go2page(this.pageCount)) {
                        let pg = this.pageCount + '';
                        let $ns = $B.DomUtils.children(this.$NumSpan);
                        for (let i = 0; i < $ns.length; i++) {
                            if ($ns[i].innerText === pg) {
                                this.currentBtn = $ns[i];
                                $B.DomUtils.addClass(this.currentBtn, "k_pagination_actived");
                                break;
                            }
                        }
                    }
                    return false;
                }
                if ($B.DomUtils.hasClass(el, "k_pagination_num")) {
                    $B.DomUtils.addClass(el, "k_pagination_actived");
                    this.currentBtn = el;
                    this.go2page(parseInt(el.innerText));
                }
                return false;
            }
        });
    }
    update(page,pageSize,total){
        if(this.$dropList){
            this.$dropList.style.display = "none";
        }
        this.opts.page = page;
        this.opts.pageSize = pageSize;
        this.opts.total = total;
        this.countPage();
        if(page > this.pageCount){ //恢复到第一页
            this.opts.page = 1;
        }
        _createNumsFn(this);
        this.go2page(page);
        this.$total[0].innerText = total;
        this.$pageSize.firstChild.innerText = pageSize;
    }
    destroy(isForce) {
        if(this.$dropList){
            this.$dropList.clearAll();
        }
        super.destroy();
    }
}
$B["Pagination"] = Pagination; 
 if(!window["$B"] ) {window["$B"] = $B;} 
  return window["$B"] ; 
});

/*! BUI - v2.0.0 - 2022-03-13 09:03:17 
Copyright (c): kevin.huang  https://gitee.com/kevin-huang/bui2.0.git (qq:757723114) 
Released under MIT License*/
(function ( global, factory) {
//   "use strict";
	if ( typeof module === "object" && typeof module.exports === "object" ) {
		module.exports = global.document ?
			factory( global, true ) :
			function( w ) {
				if ( !w.document ) {
					throw new Error( "Bui requires a window with a document" );
				}
				return factory( w );
			};
	} else {
		  factory( global );
	}  
})( typeof window !== "undefined" ? window : this, function( window, noGlobal ) {
//   "use strict";
  var $B = window["$B"] ?  window["$B"] : {};


var defaultOpts = {
    style: 'v',  //v 垂直风格，h水平风格
    hstyleHeight: '35px',
    activedCls: 'k_nav_v_actived',  
    backgroundColor: undefined, //背景色
    fontColor: undefined,//图标字体颜色
    hoverColor: undefined, //鼠标移动上来的选项背景色
    activedColor: undefined,//激活选项的背景色
    actFontColor: undefined,//激活状态的图标字体颜色
    datas: undefined
};
function createHoverStyle(styleClzz, clazzName, backgorundColor) {
    var styleCtx = clazzName + '{background-color:' + backgorundColor + '!important;}';
    $B.createHeaderStyle(styleClzz, styleCtx);
}
class Nav extends $B.BaseControl {
    constructor(elObj, opts) {
        let wrap = $B.DomUtils.createEl("<div class='k_nav_wrap k_box_size'></div>");
        if (typeof elObj === "string") {
            elObj = $B.DomUtils.findbyId(elObj);
            $B.DomUtils.append(elObj, wrap);
            elObj = wrap;
        }
        super();
        super.setElObj(elObj);
        this.idIndex = 1;
        this.allSubMenus = [];
        this.extAllSubMenus = [];
        this.opts = $B.extendObjectFn(true, {}, defaultOpts, opts);
        if (this.opts.backgroundColor) {
            $B.DomUtils.css(elObj, { "background-color": this.opts.backgroundColor });
        }
        if (this.opts.hoverColor) {
            this.hoverClass = this.id;
            createHoverStyle(this.hoverClass, "." + this.hoverClass, this.opts.hoverColor);
        }
        if (this.opts.style === "v") {
            $B.DomUtils.css(this.elObj, { "width": "100%", "height": "100%" });
            this.$body = $B.myScrollbar(this.elObj, {});
            if(this.opts.bodyCss){
                $B.DomUtils.css(this.$body, this.opts.bodyCss);
            }            
            this.createVNav(this.$body, this.opts.datas, 0);
            this.onClickV = (e) => {
                let el = e.target;
                while (el) {
                    if ($B.DomUtils.hasClass(el, "k_nav_v_item")) {
                        break;
                    }
                    el = el.parentNode;
                }
                if(el){
                    let innerwrap = el.firstChild;
                    if (this.opts.click && !e.notfire) {
                        this._fireBtnClick(el);
                    }
                    if ($B.DomUtils.attribute(el, "isprt")) {
                        if ($B.DomUtils.isHide(el.nextSibling)) {
                            $B.animate(innerwrap.firstChild.firstChild, { "rotateZ": "0deg" }, { duration: 200 });
                            $B.slideDown(el.nextSibling, 200);
                        } else {
                            $B.animate(innerwrap.firstChild.firstChild, { "rotateZ": "270deg" }, { duration: 200 });
                            $B.slideUp(el.nextSibling, 200);
                        }
                    } else {
                        this.clearActivedStyle();
                        this.activedItem = el;
                        this.setActivedItemStyle();
                    }
                } else{
                    console.log("el is not found",e);
                }               
            };
            $B.DomUtils.click(this.$body, this.onClickV);
        } else {//水平导航
            $B.DomUtils.css(this.elObj, { "width": "100%" });
            this.$body = $B.DomUtils.createEl("<div style='height:" + this.opts.hstyleHeight + ";width:100%;overflow:hidden;' class='k_nav_h_wrap k_box_size'></div>");
            if(this.opts.bodyCss){
                $B.DomUtils.css(this.$body, this.opts.bodyCss);
            } 
            this.createHNav(this.opts.datas);
            if (this.opts.backgroundColor) {
                $B.DomUtils.css(this.$body, { "background-color": this.opts.backgroundColor });
            }
            $B.DomUtils.append(this.elObj, this.$body);
            this.autoMoreItems();
        }
        $B.DomUtils.resize(window, (e) => {
            //console.log("resize >>>>>>>>>>>>");
            this.autoMoreItems();
        });
    }
    clearActivedStyle(){
        if (this.activedItem) {
            //清理背景色
            if (this.opts.activedColor) {
                let styleAttr = $B.DomUtils.attribute(this.activedItem, "style");
                let styleObj = $B.style2cssObj(styleAttr);
                delete styleObj["background-color"];
                styleAttr = $B.cssObj2string(styleObj);
                $B.DomUtils.attribute(this.activedItem, { "style": styleAttr });
            }
            //清理字体颜色
            this.setItemColor(this.opts.fontColor, this.activedItem);
            $B.DomUtils.removeClass(this.activedItem, this.opts.activedCls);
        }
    }
    setActivedItemStyle(){
        $B.DomUtils.addClass(this.activedItem, this.opts.activedCls);
        this.setItemColor(this.opts.actFontColor, this.activedItem);
        if (this.opts.activedColor) {
            $B.DomUtils.css(this.activedItem, { "background-color": this.opts.activedColor });
        }
    }
    setActById(id){
        if (this.opts.style === "v") {
            if(!this.shortId){
                id = "nav_"+id;
            }
            let el =  $B.DomUtils.findbyId(this.elObj,id);
            if(this.activedItem && this.activedItem === el){
                return;
            }
            this.onClickV({target:el,notfire:true});
        }else{
            console.log("尚不支持水平导航！");
        }
    }
    /****
     * 设置重直ui风格的点击项目
     * ****/
    setItemColor(fColor, it) {
        var childs = $B.DomUtils.children(it.firstChild);
        for (let i = 0; i < childs.length; i++) {
            let cl = childs[i];
            if ($B.DomUtils.hasClass(cl, ".k_nav_v_more")) {
                cl = cl.firstChild;
            }
            let styleAttr = $B.DomUtils.attribute(cl, "style");
            if (styleAttr) {
                let styleObj = $B.style2cssObj(styleAttr);
                if (fColor) {
                    styleObj.color = fColor;
                } else {
                    delete styleObj.color;
                }
                styleAttr = $B.cssObj2string(styleObj);
                $B.DomUtils.attribute(cl, { "style": styleAttr });
            }
        }
    }
    /**
     * 绑定垂直风格的事件
     * ***/
    bindItemEvents(it) {
        if (this.hoverClass) {
            if (!this.itemEvents) {
                this.itemEvents = {
                    mouseenter: (e) => {
                        $B.DomUtils.addClass(e.target, this.hoverClass);
                    },
                    mouseleave: (e) => {
                        $B.DomUtils.removeClass(e.target, this.hoverClass);
                    }
                };
            }
            $B.DomUtils.bind(it, this.itemEvents);
        }
    }
    /***
     * 创建垂直风格的Dom UI
     * ****/
    createVNav(wrap, datas, padding) {
        let html = "<div  style='padding-left:" + padding + "px' class='k_nav_v_item k_box_size'><div style='padding-left:12px;'><span></span></div></div>";
        let activedIt;       
        for (let i = 0; i < datas.length; i++) {
            let data = datas[i];
            let it = $B.DomUtils.createEl(html);
            this.bindItemEvents(it);
            let subId;
            if (data.id) {
                subId = "nav_" + data.id;
            } else {
                if (!this.shortId) {
                    this.shortId = $B.getShortID();
                }
                subId = this.shortId + "_" + this.idIndex;
                this.idIndex++;
            }
            data["id"] = subId;
            let attr = { id: subId };
            let inner = it.firstChild;
            inner.firstChild.innerHTML = data.text;
            $B.DomUtils.append(wrap, it);
            it = inner;
            if (data.children) {
                attr["isprt"] = 1;
                let childWrap = $B.DomUtils.createEl("<div style=''></div>");
                let iCls = 'fa-angle-down';
                let icss = "";
                if (data.closed) {
                    childWrap.style.display = "none";
                    icss = "transform: rotateZ(270deg);";
                }
                $B.DomUtils.prepend(it, "<span class='k_nav_v_more'><i style='" + icss + "' class='fa " + iCls + "'></i></span>");
                $B.DomUtils.append(wrap, childWrap);
                this.createVNav(childWrap, data.children, padding + 18);
            } else {
                if (data.iconCls) {
                    $B.DomUtils.prepend(it, "<i class='fa " + data.iconCls + " k_nav_v_icon'></i>");
                } else if (data.data.menuIconCss) {
                    $B.DomUtils.prepend(it, "<i class='fa " + data.data.menuIconCss + " k_nav_v_icon'></i>");
                }
            }
            if (this.opts.fontColor) {
                var childs = $B.DomUtils.children(it);
                for (let j = 0; j < childs.length; j++) {
                    let cl = childs[j];
                    if ($B.DomUtils.hasClass(cl, ".k_nav_v_more")) {
                        $B.DomUtils.css(cl.firstChild, { color: this.opts.fontColor });
                    } else {
                        $B.DomUtils.css(cl, { color: this.opts.fontColor });
                    }
                }
            }
            $B.DomUtils.attribute(it.parentNode, attr);
            if (data.actived) {
                activedIt = it.parentNode;
            }
        }
        if (activedIt) {
            setTimeout(() => {
                this.onClickV({ target: activedIt });
            }, 1);
        }
    }
    /**
     * 多级菜单时候，根据id隐藏子菜单
     * ***/
    _hideSubMenus(id) {
        for (let i = 0; i < this.allSubMenus.length; i++) {
            if ($B.DomUtils.attribute(this.allSubMenus[i], "id").indexOf(id) > -1) {
                $B.DomUtils.hide(this.allSubMenus[i]);
            }
        }
    }
    /***
     * 水平一级菜单事件
     * ****/
    bindHeVENTS(el) {
        if (!this.hmouseEvents) {
            var _this = this;
            this.hmouseEvents = {
                mouseenter: (e) => {
                    let el = e.target;
                    if ($B.DomUtils.hasClass(el, "k_nav_h_item_prt")) {
                        let id = $B.DomUtils.attribute(el, "id") + "_c";
                        let it;
                        for (let i = 0; i < _this.allSubMenus.length; i++) {
                            if ($B.DomUtils.attribute(_this.allSubMenus[i], "id") === id) {
                                it = _this.allSubMenus[i];
                            } else {
                                _this.allSubMenus[i].style.display = "none";
                            }
                        }
                        clearTimeout(_this._hideSubMenusTimer);
                        if ($B.DomUtils.isHide(it)) {
                            var ofs = $B.DomUtils.offset(el);
                            var height = $B.DomUtils.outerHeight(el);
                            var width = $B.DomUtils.outerWidth(el);
                            ofs.top = ofs.top + height + 1;
                            $B.DomUtils.css(it, ofs);
                            $B.slideDown(it, 200, () => {
                                if ($B.DomUtils.outerWidth(it) < width) {
                                    $B.DomUtils.outerWidth(it, width);
                                }
                            });
                        }
                    } else {
                        _this.hideAllSubMenus(true);
                    }
                },
                mouseleave: (e) => {
                    let el = e.target;
                    if ($B.DomUtils.hasClass(el, "k_nav_h_item_prt")) {
                        let id = $B.DomUtils.attribute(el, "id") + "_c";
                        _this._hideSubMenusTimer = setTimeout(() => {
                            _this._hideSubMenus(id);
                        }, 500);
                    }
                },
                click: (e) => {
                    let el = e.target;
                    while (el) {
                        if ($B.DomUtils.hasClass(el, "k_nav_h_item")) {
                            break;
                        }
                        el = el.parentNode;
                    }
                    _this._clearAllSubAct();
                    _this._move2acitived(el);
                    this.activedItem = el;
                    _this._fireBtnClick(el);
                    this._renderExtActUI();
                }
            };
        }
        $B.DomUtils.bind(el, this.hmouseEvents);
    }
    /***移动激活的下边线***/
    _moveActviedLine(id) {
        let firstClzEl = $B.DomUtils.findbyId(this.$body, id);
        this._move2acitived(firstClzEl);
    }
    //移动激活下划线
    _move2acitived(el) {
        if (!this.$activedHelper) {
            this.$activedHelper = $B.DomUtils.createEl("<div style='position:absolute;height:2px;background:" + this.opts.activedColor + ";bottom:0;left:0;display:none;'></div>");
            $B.DomUtils.append(this.$body, this.$activedHelper);
        }
        let left = 16;
        let $txt = el.firstChild;
        let w = $B.DomUtils.outerWidth($txt);
        let prevEl = el.previousSibling;
        while (prevEl) {
            left = left + $B.DomUtils.outerWidth(prevEl);
            prevEl = prevEl.previousSibling;
        }
        if (this.$activedHelper.style.display === "none") {
            this.$activedHelper.style.left = "0px";
            this.$activedHelper.style.display = "block";
        }
        $B.animate(this.$activedHelper, { left: left + "px", "width": w + "px" }, { duration: 360 });
        this.$activeFirstMenu = el;
    }
    createHNav(datas) {
        let html = "<div class='k_nav_h_item k_box_size'><span class='_lable_'></span></div>";
        for (let i = 0; i < datas.length; i++) {
            let data = datas[i];
            let subId;
            if (data.id) {
                subId =  "nav_" + data.id ;
            } else {
                if (!this.shortId) {
                    this.shortId = $B.getShortID();
                }
                subId = this.shortId + "_" + this.idIndex;
                this.idIndex++;               
            }      
            data.id = subId;      
            let attr = { id: subId, index: i };
            let it = $B.DomUtils.createEl(html);
            $B.DomUtils.attribute(it, attr);
            it.firstChild.innerHTML = data.text;
            it.firstChild.style.lineHeight = this.opts.hstyleHeight;
            $B.DomUtils.append(this.$body, it);
            if (data.children) {
                $B.DomUtils.addClass(it, "k_nav_h_item_prt");
                $B.DomUtils.append(it, "<span  style='position:absolute;right:1px;display:inline-block;height:100%;line-height:" + this.opts.hstyleHeight + "'><i class='fa fa-angle-down'></i></span>");
                let $w = this.createChildMenu(subId, data.children);
                $B.DomUtils.addClass($w, "_first_");
            } else {
                $B.DomUtils.addClass(it, "k_nav_h_item_child");
                if (data.actived) {
                    this.activedItem = it;
                }
            }
            this.bindHeVENTS(it);
        }
        if (this.activedItem) {
            setTimeout(() => {
                $B.DomUtils.trigger(this.activedItem, "click");
            }, 100);
        }
    }
    /**
     * 水平子菜单事件
     * ***/
    bindSubMenuEv(it) {
        if (!this.subEvents) {
            this.subEvents = {
                mouseleave: (e) => {
                    this.mmHiderTimer = setTimeout(() => {
                        this.hideAllSubMenus();
                    }, 500);
                },
                mouseenter: (e) => {
                    clearTimeout(this.mmHiderTimer);
                    let el = e.target;
                    let i = $B.DomUtils.children(el, ".fa-ellipsis");
                    let btnId = $B.DomUtils.attribute(el, "id");
                    if (i.length > 0) {
                        let btnId = $B.DomUtils.attribute(el, "id");
                        let id = btnId + "_c";
                        for (let i = 0; i < this.allSubMenus.length; i++) {
                            let $w = this.allSubMenus[i];
                            let wId = $B.DomUtils.attribute($w, "id");
                            if (wId === id) {
                                if ($w.style.display === "none") {
                                    var ofs = $B.DomUtils.offset(el);
                                    var w = $B.DomUtils.outerWidth(el);
                                    var h = $B.DomUtils.outerHeight(el);
                                    ofs.top = ofs.top + h / 2;
                                    ofs.left = ofs.left + w - 2;
                                    $B.DomUtils.position($w, ofs);
                                    $B.DomUtils.detach($w);
                                    $B.DomUtils.append($B.getBody(), $w);
                                    $B.slideDown($w, 200);
                                }
                            } else if (btnId.indexOf(wId) < 0) {
                                $w.display = "none";
                            }
                        }
                    } else {
                        let tmpArr = btnId.split("_");
                        tmpArr.pop();
                        let wrpId = tmpArr.join("_");
                        for (let i = 0; i < this.allSubMenus.length; i++) {
                            let $w = this.allSubMenus[i];
                            let wId = $B.DomUtils.attribute($w, "id");
                            if (wId.indexOf(wrpId) === 0 && wId !== wrpId) {
                                $w.style.display = "none";
                            }
                        }
                    }
                },
                click: (e) => {
                    let el = e.target;
                    while (el) {
                        if (el.tagName === "BUTTON") {
                            break;
                        }
                        el = el.parentNode;
                    }
                    this._clearActItem();
                    if (this.opts.click) {
                        this._fireBtnClick(el);
                    }
                    this.activedItem = el;
                    //更多控制按钮存在index
                    let index = $B.DomUtils.attribute(el, "index");
                    if (index) {
                        index = parseInt(index);
                        let siblingsTtns = $B.DomUtils.siblings(el);
                        for (let i = 0; i < siblingsTtns.length; i++) {
                            this._clearColor(siblingsTtns[i]);
                        }
                        let children = Array.from(this.$body.children);
                        this._move2acitived(children[index]);
                    }
                    let tels = $B.DomUtils.children(this.activedItem, "._lable_");
                    tels[0].style.color = this.opts.activedColor;
                    if (tels[0].previousSibling) {
                        tels[0].previousSibling.style.color = this.opts.activedColor;
                    }

                }
            };
        }
        $B.DomUtils.bind(it, this.subEvents);
    }
    _fireBtnClick(el) {
        let id = $B.DomUtils.attribute(el, "id");
        let opt = this._getOptById(id);
        if (el.tagName === "BUTTON") {
            this._clearAllSubAct();
            this._ativedUiRender(el);
        }
        setTimeout(() => {
            this.opts.click.call(el, opt);
        }, 1);
        if (this.opts.style === "h") {
            this.hideAllSubMenus();
        }
    }
    /*****/
    _clearActItem() {
        if (this.activedItem) {
            let tels = $B.DomUtils.children(this.activedItem, "._lable_");
            if (tels.length > 0) {
                let $s = tels[0];
                this._clearColor($s);
                if ($s.previousSibling) {
                    this._clearColor($s.previousSibling);
                }
                if ($s.nextSibling) {
                    this._clearColor($s.nextSibling);
                }
            }
        }
    }
    /**清理所有激活**/
    _clearAllSubAct() {
        this._clearActItem();
        for (let i = 0; i < this.allSubMenus.length; i++) {
            let $w = this.allSubMenus[i];
            this._clearIActived(Array.from($w.children));
        }
        if (this.$extMenuContainer && this.$extMenuContainer.children) {
            this._clearIActived(Array.from(this.$extMenuContainer.children));
        }
    }
    /**
     * 循环元素进行激活ui恢复
     * **/
    _clearIActived(siblings) {
        for (let j = 0; j < siblings.length; j++) {
            let $s = siblings[j];
            let el = $s.firstChild;
            this._clearColor(el);
            el = el.nextSibling;
            while (el) {
                this._clearColor(el);
                el = el.nextSibling;
            }
        }
    }
    /***根据点击的按钮元素，向上递归激活UI
     * ***/
    _ativedUiRender(btnEl) {
        let wrap = btnEl.parentNode;
        let wrapId = $B.DomUtils.attribute(wrap, "id");
        let tmpArr = wrapId.split("_");
        tmpArr.pop();
        let parentBtnId = tmpArr.join("_");
        tmpArr = parentBtnId.split("_");
        tmpArr.pop();
        let parentWrapId = tmpArr.join("_");
        if ($B.DomUtils.hasClass(wrap, "_first_")) {
            this._moveActviedLine(parentBtnId);
            this._renderExtActUI(parentBtnId);
            return;
        }
        for (let i = 0; i < this.allSubMenus.length; i++) {
            let $w = this.allSubMenus[i];
            let wId = $B.DomUtils.attribute($w, "id");
            if (parentWrapId === wId) {
                let pBtn = $B.DomUtils.findbyId($w, parentBtnId);
                pBtn.lastChild.style.color = this.opts.activedColor;
                if (!$B.DomUtils.hasClass($w, "_first_")) {
                    this._ativedUiRender(pBtn);
                } else {
                    tmpArr = wId.split("_");
                    tmpArr.pop();
                    wId = tmpArr.join("_");
                    this._moveActviedLine(wId);
                    this._renderExtActUI(wId);
                }
                break;
            }
        }
    }
    _renderExtActUI(id) {
        if (this.$extMenuContainer && id) {
            let el = $B.DomUtils.findbyId(this.$extMenuContainer, id);
            if (el) {
                el.lastChild.style.color = this.opts.activedColor;
                this.$extMore.firstChild.style.color = this.opts.activedColor;
            } else {
                if (this.$extMore) {
                    this._clearColor(this.$extMore.firstChild);
                }
            }
        } else {
            if (this.$extMore) {
                this._clearColor(this.$extMore.firstChild);
            }
        }
    }
    _clearColor(el) {
        let cssObj = $B.style2cssObj(el);
        delete cssObj.color;
        let cssattr = $B.cssObj2string(cssObj);
        $B.DomUtils.attribute(el, { "style": cssattr });
    }
    createChildMenu(id, datas) {
        id = id + "_c";
        let $wrp = $B.DomUtils.createEl("<div style='display:none;position: absolute;z-index:" + $B.config.maxZindex + "' id='" + id + "' class='ext_mm_container k_context_menu_container k_box_shadow k_box_size'></div>");
        if (!this.subWrpEvs) {
            this.subWrpEvs = {
                mouseleave: (e) => {
                    let el = e.target;
                    let id = $B.DomUtils.attribute(el, "id");
                    this._hideSubMenusTimer = setTimeout(() => {
                        this._hideSubMenus(id);
                    }, 500);
                },
                mouseenter: (e) => {
                    clearTimeout(this._hideSubMenusTimer);
                    clearTimeout(this.detachExtTimer);
                }
            };
        }
        $B.DomUtils.bind($wrp, this.subWrpEvs);
        this._createSubBtns(datas, id, $wrp, (id, childs) => {
            this.createChildMenu(id, childs);
        });
        $B.DomUtils.append($B.getBody(), $wrp);
        this.allSubMenus.push($wrp);
        return $wrp;
    }
    _createSubBtns(datas, id, $wrp, onCreateChilds) {
        let idx = this.overIdx;
        for (let i = 0; i < datas.length; i++) {
            let it = datas[i];
            if (!it["id"]) {
                it["id"] = id + "_" + i;
            }
            let text = it.text;
            let icon = it.iconCls;
            let btn = $B.DomUtils.createEl("<button class='k_toolbar_button_plain' id='" + it.id + "' style='white-space: nowrap; border-radius: 0px; display: block; margin: 0px 0px 3px; min-width: 100%; text-align: left;'></button>");
            if (icon) {
                $B.DomUtils.append(btn, '<i class="fa ' + icon + '"></i>');
            }
            if (id === "k_nav_more_btn") {
                $B.DomUtils.attribute(btn, { index: idx });
                idx++;
            }
            $B.DomUtils.append(btn, '<span class="_lable_" style="white-space: nowrap; padding-left: 5px;">' + text + '</span>');
            if (it.children) {
                $B.DomUtils.attribute(btn, { haschild: "1" });
                $B.DomUtils.append(btn, '<i style="position:relative;font-size:10px;padding-right:-8px;padding-left:5px;top:0.5em;" class="fa fa-ellipsis"></i>');
                onCreateChilds(it.id, it.children);
            }
            $B.DomUtils.append($wrp, btn);
            this.bindSubMenuEv(btn);
            if (it.actived) {
                this.activedItem = btn;
            }
        }
    }
    autoMoreItems() {
        let width = $B.DomUtils.width(this.$body);
        let childs = this.$body.children;
        let start = 0;
        let overIdx = -1;
        for (let i = 0; i < childs.length; i++) {
            let child = childs[i];
            if ($B.DomUtils.hasClass(child, ".k_nav_h_item")) {
                start = start + $B.DomUtils.outerWidth(child);
                if (start > width) {
                    overIdx = i;
                    break;
                }
            }
        }
        this.overIdx = overIdx;
        if (overIdx > 0) {
            if (!this.$extMore) {
                this.$extMore = $B.DomUtils.createEl("<div class='k_nav_more_btn' style='display:none;position:absolute;right:-2px;bottom:-5px;'><i class='fa fa-menu'></i></div>");
                $B.DomUtils.append(this.$body, this.$extMore);
                $B.DomUtils.bind(this.$extMore, {
                    mouseenter: (e) => {
                        clearTimeout(this._hideSubMenusTimer);
                        clearTimeout(this.detachExtTimer);
                        var el = e.target;
                        var ofs = $B.DomUtils.offset(el);
                        if (!this.$extMenuContainer) {
                            let id = "k_h_nav_ext";
                            this.$extMenuContainer = $B.DomUtils.createEl("<div id='" + id + "' style='display:none;position: absolute;z-index:" + $B.config.maxZindex + "' class='k_context_menu_container k_box_shadow k_box_size'></div>");
                            $B.DomUtils.bind(this.$extMenuContainer, {
                                mouseenter: (e) => {
                                    clearTimeout(this._hideSubMenusTimer);
                                    clearTimeout(this.detachExtTimer);
                                },
                                mouseleave: (e) => {
                                    this.detachExtTimer = setTimeout((e) => {
                                        $B.DomUtils.detach(this.$extMenuContainer);
                                    }, 500);
                                }
                            });
                        } else {
                            $B.DomUtils.removeChilds(this.$extMenuContainer);
                        }
                        $B.DomUtils.css(this.$extMenuContainer, { top: "-1000px", "display": "block" });
                        let childs = this.opts.datas.slice(this.overIdx);
                        this._createSubBtns(childs, "k_nav_more_btn", this.$extMenuContainer, (id, childs) => {
                        });
                        if (this.$activeFirstMenu) {
                            setTimeout(() => {
                                let mid = $B.DomUtils.attribute(this.$activeFirstMenu, "id");
                                this._renderExtActUI(mid);
                            }, 10);
                        }
                        if (e.isTrigger) {
                            return false;
                        }
                        if (this.$extMenuContainer.parentNode) {
                            $B.DomUtils.detach(this.$extMenuContainer);
                        }
                        $B.DomUtils.append($B.getBody(), this.$extMenuContainer);
                        var info = $B.DomUtils.domInfo(this.$extMenuContainer);
                        ofs.left = ofs.left - info.width;
                        this.$extMenuContainer.style.display = "none;"
                        this.$extMenuContainer.style.top = ofs.top + 18 + "px";
                        this.$extMenuContainer.style.left = ofs.left + 14 + "px";
                        $B.slideDown(this.$extMenuContainer, 100);
                    },
                    mouseleave: (e) => {
                        if (this.$extMenuContainer) {
                            this.detachExtTimer = setTimeout((e) => {
                                $B.DomUtils.detach(this.$extMenuContainer);
                            }, 300);
                        }
                    }
                });
                $B.DomUtils.trigger(this.$extMore, "mouseenter");
            }
            this.$extMore.style.display = "block";
        } else if (this.$extMore) {
            this.$extMore.style.display = "none";
        }
    }
    hideAllSubMenus(notHideExt) {
        var childs = this.allSubMenus;
        for (let i = 0; i < childs.length; i++) {
            childs[i].style.display = "none";
        }
        if (!notHideExt && this.$extMenuContainer) {
            $B.DomUtils.detach(this.$extMenuContainer);
        }
    }
    _getOptById(id) {
        var opt = this._loopGetOptById(id, this.opts.datas);
        return opt;
    }
    _loopGetOptById(id, buttons) {
        var opt, childrens = [];
        for (let i = 0; i < buttons.length; i++) {
            if (Array.isArray(buttons[i])) {
                opt = this._loopGetOptById(id, buttons[i]);
                if (opt) {
                    break;
                }
            } else {
                if (buttons[i].id === id) {
                    opt = buttons[i];
                    break;
                }
                if (buttons[i].children) {
                    for (let j = 0; j < buttons[i].children.length; j++) {
                        childrens.push(buttons[i].children[j]);
                    }
                }
            }
        }
        if (!opt && childrens.length > 0) {
            opt = this._loopGetOptById(id, childrens);
        }
        return opt;
    }
    destroy(excuObjName) {
        if (this.$extMenuContainer) {
            $B.DomUtils.remove(this.$extMenuContainer);
        }
        $B.DomUtils.remove(this.allSubMenus);
        super.destroy(excuObjName);
    }
}
$B["Nav"] = Nav; 
 if(!window["$B"] ) {window["$B"] = $B;} 
  return window["$B"] ; 
});

/*! BUI - v2.0.0 - 2022-03-13 09:03:17 
Copyright (c): kevin.huang  https://gitee.com/kevin-huang/bui2.0.git (qq:757723114) 
Released under MIT License*/
(function ( global, factory) {
//   "use strict";
	if ( typeof module === "object" && typeof module.exports === "object" ) {
		module.exports = global.document ?
			factory( global, true ) :
			function( w ) {
				if ( !w.document ) {
					throw new Error( "Bui requires a window with a document" );
				}
				return factory( w );
			};
	} else {
		  factory( global );
	}  
})( typeof window !== "undefined" ? window : this, function( window, noGlobal ) {
//   "use strict";
  var $B = window["$B"] ?  window["$B"] : {};

var defaultOpts = {
    trigger: 'mousedown',
    setPositionFn:undefined
};
var $body;
class Ctxmenu extends $B.BaseControl {
    constructor(elObj, opts) {
        super();
        this.$doc = opts.document ? opts.document : document;
        if (!$body) {
            $body =   this.$doc.body;
        }        
        $B.DomUtils.contextmenu(this.$doc, function (e) {
            return false;
        });       
        this.id = $B.getUUID();
        this.opts = $B.extendObjectFn(true, {}, defaultOpts, opts);
        let evs = {};
        this.allSubMenus = [];
        evs[this.opts.trigger] = (e) => {
            let isMouseDown = this.opts.trigger === "mousedown";
            if (isMouseDown) {
                this.hide();
                if (e.which !== 3) {
                    return true;
                }
            }
            let pos;
            if(isMouseDown){
                pos = { left: e.pageX, top: e.pageY };
            }else{
                let el = e.target;
                pos = $B.DomUtils.offset(el);
                pos.top = $B.DomUtils.outerHeight(el) + 2;
            }
            if(this.opts.setPositionFn){
                this.opts.setPositionFn(e.target,pos);
            }
            this.elObj = this.createMenus(this.id, e, pos, this.opts.buttons);
            $B.slideDown(this.elObj, 200);
        };
        $B.DomUtils.bind(elObj, evs);
    }
    _getOptById(id) {
        var opt = this._loopGetOptById(id, this.opts.buttons);
        return opt;
    }
    _loopGetOptById(id, buttons) {
        var opt, childrens = [];
        for (let i = 0; i < buttons.length; i++) {
            if (Array.isArray(buttons[i])) {
                opt = this._loopGetOptById(id, buttons[i]);
                if (opt) {
                    break;
                }
            } else {
                if (buttons[i].id === id) {
                    opt = buttons[i];
                    break;
                }
                if (buttons[i].children) {
                    for (let j = 0; j < buttons[i].children.length; j++) {
                        childrens.push(buttons[i].children[j]);
                    }
                }
            }
        }
        if (!opt && childrens.length > 0) {
            opt = this._loopGetOptById(id, childrens);
        }
        return opt;
    }
    _getElFromEv(e) {
        let el = e.target;
        while (el.tagName !== "BUTTON") {
            el = el.parentNode;
        }
        return el;
    }
    _getBtnElAdnOpt(e) {
        let el = this._getElFromEv(e);
        let id = $B.DomUtils.attribute(el, "id");
        var opt = this._getOptById(id);
        return {
            el: el,
            opt: opt
        };
    }
    getWrapEvents(){
        if(!this.wrapEvents){
            var _this = this;
            this.wrapEvents = {
                mouseenter: function (e) {
                    clearTimeout( _this.hideTimer );
                },
                mouseleave:function(e){
                    _this.hideTimer = setTimeout(()=>{
                         _this.hide();
                    },500);
                }
            };
        }
        return this.wrapEvents;
    }
    getEvents() {
        if (!this.events) {
            let _this = this;
            this.events = {
                mousedown: function (e) {
                    let res = _this._getBtnElAdnOpt(e);
                    let opt = res.opt;
                    if (opt.click) {
                        setTimeout(() => {
                            opt.click.call(res.el, opt);
                        }, 1);
                    }
                    setTimeout(()=>{
                        _this.hide();
                    },1);
                    return false;
                },
                mouseenter: function (e) {
                    let el = _this._getElFromEv(e);
                    let id = $B.DomUtils.attribute(el, "id");
                    if (_this.showIingEl) { 
                        let wid =  $B.DomUtils.attribute(_this.showIingEl, "id"); 
                        if(wid.indexOf(id) < 0 && id.indexOf(wid) < 0){
                            _this.showIingEl.style.display = "none";
                        } 
                    }                   
                    if ($B.DomUtils.attribute(el, "haschild")) {
                        let bodyWidth = $B.DomUtils.width($body);
                        let bodyHeight = $B.DomUtils.height($body);
                        let res = _this._getBtnElAdnOpt({ target: el });
                        let opt = res.opt;
                        let items = opt.children;
                        let id = $B.DomUtils.attribute(el, "id")+"_c";
                        let ofs = $B.DomUtils.offset(el);
                        ofs.left = ofs.left + 2;
                        ofs.top = ofs.top +  $B.DomUtils.outerHeight(el) / 2;
                        let pos = { top: ofs.top, left: ofs.left };
                        pos.left = pos.left + $B.DomUtils.outerWidth(el);
                        let childEl = _this.createMenus(id, e, pos, items,true);                     
                        $B.DomUtils.show(childEl);                       
                        let elWidth = $B.DomUtils.outerWidth(childEl);
                        let elHeight = $B.DomUtils.outerHeight(childEl);
                        let diff = bodyWidth - elWidth - pos.left;
                        let reset = false;
                        if (diff < 0) {
                            pos.left = ofs.left - elWidth;
                            reset = true;
                        }
                        diff = bodyHeight - elHeight - pos.top;
                        if (diff < 0) {
                            pos.top = ofs.top - elHeight;
                            reset = true;
                        }
                        if (reset) {
                            childEl.style.top = pos.top + "px";
                            childEl.style.left = pos.left + "px";
                        }
                        _this.showIingEl = childEl;
                    }
                }
            };
        }
        return this.events;
    }
    createMenus(id, e, pos, items,isChild) {
        let elObj = $B.DomUtils.children($body, "#" + id);
        if (!elObj) {
            elObj = $B.DomUtils.createEl("<div id='" + id + "' class='k_context_menu_container k_box_shadow k_box_size' style='position:absolute;padding: 6px 8px;display:none;z-index:" + $B.config.maxZindex + "'></div>");
            if(isChild){
                this.allSubMenus.push(elObj);
            }          
            let events = this.getEvents();
            for (let i = 0; i < items.length; i++) {
                let it = items[i];
                it["id"] = id + "_" + i;
                let text = it.text;
                let icon = it.iconCls;
                let btn = $B.DomUtils.createEl("<button class='k_toolbar_button_plain' id='" + it.id + "' style='white-space: nowrap; border-radius: 0px; display: block; margin: 0px 0px 3px; min-width: 100%; text-align: left;'></button>");
                if (icon) {
                    $B.DomUtils.append(btn, '<i class="fa ' + icon + '"></i>');
                }
                $B.DomUtils.append(btn, '<span style="white-space: nowrap; padding-left: 5px;">' + text + '</span>');
                if (it.children) {
                    $B.DomUtils.attribute(btn, { haschild: "1" });
                    $B.DomUtils.append(btn, '<i style="position:relative;font-size:10px;padding-right:-8px;padding-left:5px;top:0.5em;" class="fa fa-ellipsis"></i>');
                }
                $B.DomUtils.append(elObj, btn);
                $B.DomUtils.bind(btn, events);
            }
            $B.DomUtils.bind(elObj,this.getWrapEvents());
            $B.DomUtils.append($body,elObj);
        }else{//运动到底部
            $B.DomUtils.detach(elObj);
            $B.DomUtils.append($body,elObj);
        }
        $B.DomUtils.css(elObj, pos);
        return elObj;
    }
    hide(){
        if(this.elObj){
            $B.DomUtils.hide(this.elObj);
            $B.DomUtils.hide(this.allSubMenus);
        }
    }
    destroy(excuObjName) {
        if(this.elObj){
            $B.DomUtils.remove(this.elObj);
            $B.DomUtils.remove(this.allSubMenus);
            this.elObj = undefined;
            this.allSubMenus = undefined;
        }
        this.$doc = undefined;
        super.destroy(excuObjName);
    }
}
$B["Ctxmenu"] = Ctxmenu; 
 if(!window["$B"] ) {window["$B"] = $B;} 
  return window["$B"] ; 
});

/*! BUI - v2.0.0 - 2022-03-13 09:03:17 
Copyright (c): kevin.huang  https://gitee.com/kevin-huang/bui2.0.git (qq:757723114) 
Released under MIT License*/
(function ( global, factory) {
//   "use strict";
	if ( typeof module === "object" && typeof module.exports === "object" ) {
		module.exports = global.document ?
			factory( global, true ) :
			function( w ) {
				if ( !w.document ) {
					throw new Error( "Bui requires a window with a document" );
				}
				return factory( w );
			};
	} else {
		  factory( global );
	}  
})( typeof window !== "undefined" ? window : this, function( window, noGlobal ) {
//   "use strict";
  var $B = window["$B"] ?  window["$B"] : {};

var defaultOpts = {
    data: null, //'数据'
    isTree: false,
    multiple: false, //是否多选
    readOnly: false, //是否只读
    inputImdSearch: false,//是否输入即触发搜索
    getIdAndTxt:false, 
    search: true,  //输入搜索功能，当存在url的时候，优先url请求，否则本地数据搜索
    default: { id: '', text: '请您选择'},//默认选项
    url: null, //请求数据的地址【如果有些请求参数是固定不变的，请去url中设置】
    valueModel:'parent', //parent包括父节点，显示模式 chlidId（id取最后一个但是显示包括父节点），child（id和显示都是最后一个元素）
    textField: 'text', //菜单名称字段，默认为text
    idField: 'id', //菜单id字段,默认为id
    pidField: "pid",
    onSelected: null, //function (data) { },//点击选择事件
    onloaded: null //加载完成事件function (data) { }
};
class Combox extends $B.BaseControl {
    constructor(elObj, opts) {
        super();
        this.opts = $B.extendObjectFn(true, {}, defaultOpts, opts);
        super.setElObj(elObj);
        this.$input = this.elObj;
        this.isArrayValue = this.opts.multiple || this.opts.readOnly;
        if (this.opts.readOnly) {
            $B.DomUtils.attribute(this.$input, { readonly: true });
        }
        let inputAttr = { "id": this.id + "_input", "autocomplete": "off" };
        if (this.opts.multiple) {
            inputAttr["multiple"] = true;
        }
        $B.DomUtils.attribute(this.$input, inputAttr);
        $B.DomUtils.addClass(this.elObj, "k_box_size k_combox_input");
        $B.DomUtils.css(this.elObj, { "padding-right": "18px" });
        this.srcHeight = $B.DomUtils.height(this.elObj);
        var top = (this.srcHeight - 14) / 2;
        var wrap = $B.DomUtils.createEl("<span id='" + this.id + "' class='k_combox_wrap'  outline='0' ></span>");
        
        //这里需要考虑元素被声明了位置的情况
        let posAttr = $B.DomUtils.css(this.elObj, "position");
        if (posAttr !== "static") {
            let cssinf = $B.DomUtils.domInfo(this.elObj);
            $B.DomUtils.css(wrap, { position: posAttr, top: cssinf.top, left: cssinf.left });
            this.elObj.style.position = "static";
        }
        $B.DomUtils.after(this.elObj, wrap);
        var classList = this.$input.classList;
        let wCls,wminWidth = $B.DomUtils.outerWidth(this.$input);
        if(classList){
            let rTest = /^form_input\d+$/; 
            for(let i =0 ;i < classList.length ;i++){
                let cls = classList[i];
                if(rTest.test(cls)){
                    wCls = cls;                   
                    break;
                }
            }            
        }
        $B.DomUtils.css(wrap,{"min-width":wminWidth+"px"});
        $B.DomUtils.remove(this.elObj);
        if(wCls){            
            $B.DomUtils.removeClass(this.$input,wCls);
            $B.DomUtils.css(this.$input,{width:'100%'});            
        }
        $B.DomUtils.append(wrap, this.elObj);
        this._bindInputEvent(this.elObj);
        super.setElObj(wrap);
        this.maxWidth = $B.DomUtils.innerWidth(this.elObj.parentNode) - 35;
        $B.DomUtils.attribute(this.$input, { "max_width": this.maxWidth, "src_width": $B.DomUtils.width(this.$input) });        
        if (this.isArrayValue) {
            if (!this.opts.readOnly) {
                this.maxWidth = this.maxWidth - 80;
            }
            $B.DomUtils.append(this.elObj, "<span maxwidth='" + this.maxWidth + "' style='position:absolute;top:0;left:0;display:block;max-width:" + this.maxWidth + "px;' class='k_combox_inner_wap clearfix'></span>");
        }
        this.srcWidth = $B.DomUtils.width(this.$input);
        $B.DomUtils.attribute(wrap, { "fix_width": this.srcWidth });
        if(this.opts.selectedValue){
            if(Array.isArray(this.opts.selectedValue)){
                this.$input.selectedValue = this.opts.selectedValue;
            }else{
                this.$input.selectedValue = [this.opts.selectedValue];
            }            
        }else{
            this.$input.selectedValue = [];
        }       
        this.$icon = $B.DomUtils.createEl("<i style='position:absolute;right:3px;top:" + top + "px' class='fa fa-down-open-big k_dropdown_list_el'></i>");
        $B.DomUtils.append(this.elObj, this.$icon);
        this.height = $B.DomUtils.height(this.elObj);
        let dataArray = [];
        if (this.opts.data) {
            dataArray = this.opts.data;
        } else {
            this.opts.data = dataArray;
        }
        this._createDropList(dataArray);
        if (dataArray.length === 0 && this.opts.url) {//发起请求
            this.request({}, (data) => {
                this.opts.data = data;
                let $inner = this.$dropEl.lastChild.firstChild.firstChild;
                if (this.opts.isTree) {
                    let _itlen = parseInt($B.DomUtils.attribute(this.$dropEl, "_itlen"));
                    let ret = this.dropOpts.onCreate(data, this.dropOpts, $inner, this.$input);
                    _itlen = _itlen + ret._itlen;
                    $B.DomUtils.attribute(this.$dropEl, { "_itlen": _itlen });
                } else {
                    $B._createDropListItems(data, this.dropOpts, $inner, this.$input, true);
                }
                this._renderReadonlyUI();
            });
        }
    }
    /*****
     * 树形事件
     * ******/
    _bindTreeEevents($inner) {
        var _this = this;
        if (this.hasBindedTreeEv) {
            return;
        }
        this.hasBindedTreeEv = true;
        $B.DomUtils.bind($inner, {
            click: function (e) {
                let isI = e.target.tagName === "I";
                if (isI || $B.DomUtils.hasClass(e.target, "_tree_prt")) { //父节点点击
                    let $i = e.target;
                    if (!isI) {
                        $i = $i.firstChild;
                    }
                    if ($B.DomUtils.hasClass($i, "fa-folder-empty")) {
                        $B.DomUtils.removeClass($i, "fa-folder-empty");
                        $B.DomUtils.addClass($i, "fa-folder-open-empty");
                        let ul = $i.parentNode.parentNode.parentNode.nextSibling;
                        $B.slideDown(ul, 180);
                        if (ul.children.length === 0) {
                            let pid = $B.DomUtils.attribute(ul.previousSibling, "dataid");
                            let deep = parseInt($B.DomUtils.attribute(ul.parentNode, "deep"));
                            let param = { pid: pid };
                            $B.DomUtils.removeClass($i, "fa-folder-open-empty");
                            $B.DomUtils.addClass($i, "fa-spin3 animate-spin");
                            let url = _this.opts.url;
                            let method = (url.indexOf(".data") > 0 || url.indexOf(".json") > 0) ? "GET" : "POST";
                            if (_this.opts.setParams) {
                                let pr = _this.opts.setParams(param);
                                if (pr) {
                                    param = $B.extendObjectFn(true, param, pr);
                                }
                            }
                            $B.request({
                                dataType: 'json',
                                url: url,
                                data: param,
                                type: method,
                                onErrorEval: true,
                                onReturn: function () {
                                    $B.DomUtils.addClass($i, "fa-folder-open-empty");
                                    $B.DomUtils.removeClass($i, "fa-spin3 animate-spin");
                                },
                                ok: function (message, data) {
                                    if (!data || data.length === 0) {
                                        $B.toolTip($i, ($B.config && $B.config.returnEmptyData) ? $B.config.returnEmptyData : 'the return is empty!', 2);
                                        return;
                                    }
                                    if (_this.opts.onloaded) {
                                        _this.opts.onloaded(data, param);
                                    }
                                    let $inner = _this.$dropEl.lastChild.firstChild.firstChild;
                                    let sH = $B.DomUtils.height($inner);
                                    let len = _this._createTreeUI(data, _this.dropOpts, ul, _this.$input, deep + 1); 
                                    let srlen = $B.DomUtils.attribute(_this.$dropEl,"_itlen");
                                    if(!srlen){
                                        srlen = 0;
                                    }
                                    len = len + parseInt(srlen);
                                    $B.DomUtils.attribute(_this.$dropEl,{"_itlen":len});
                                    setTimeout(() => {
                                        let eH = $B.DomUtils.height($inner);
                                        if(eH > sH){ //高度扩展
                                            let diff = eH - sH;
                                            let ofs = $B.DomUtils.offset(_this.$dropEl);
                                            let $b =  $B.getBody();
                                            let avH = $B.DomUtils.height($b) - ofs.top - sH;
                                            if(diff > avH){
                                                diff - avH - 20;
                                            }
                                            sH = sH + diff;
                                            $B.DomUtils.height(_this.$dropEl,sH);
                                        }
                                        $B.DomUtils.trigger($inner.parentNode, "myscrollabr.mouseenter");
                                    }, 200);
                                }
                            });
                        } else {
                            setTimeout(() => {
                                let $inner = _this.$dropEl.lastChild.firstChild.firstChild;
                                $B.DomUtils.trigger($inner.parentNode, "myscrollabr.mouseenter");
                            }, 200);
                        }
                    } else {
                        $B.DomUtils.addClass($i, "fa-folder-empty");
                        $B.DomUtils.removeClass($i, "fa-folder-open-empty");
                        let ul = $i.parentNode.parentNode.parentNode.nextSibling;
                        $B.slideUp(ul, 200);
                    }
                } else {
                    let el = e.target;
                    while (el) {
                        if ($B.DomUtils.hasClass(el, "k_dropdown_list_item")) {
                            break;
                        }
                        el = el.parentNode;
                    }
                    let dataid = $B.DomUtils.attribute(el, "dataid");
                    let $txt = $B.DomUtils.findByClass(el, "._combox_text")[0];
                    let data = { id: [dataid], text: [$txt.innerText] };
                    let isSelected = true;
                    if (_this.opts.multiple) { //如果是多选
                        if ($B.DomUtils.hasClass(el, "k_dropdown_item_selected")) {//执行取消
                            $B.DomUtils.removeClass(el, "k_dropdown_item_selected");
                            isSelected = false;
                        } else {
                            $B.DomUtils.addClass(el, "k_dropdown_item_selected");
                        }
                    } else {//单选   
                        isSelected = !$B.DomUtils.hasClass(el, "k_dropdown_item_selected");
                        _this._unSelectedSiblings($inner.firstChild.firstChild.firstChild.children);
                        if (isSelected) {
                            $B.DomUtils.addClass(el, "k_dropdown_item_selected");
                        }
                        _this.$dropEl.hideFn();//单选自动收起
                    }
                    if(_this.opts.valueModel !== "child"){
                        let li = el.parentNode;
                        _this._getParentDataPath(li, data);
                    }
                    if(_this.opts.valueModel === "childId"){
                        data.id = [data.id[data.id.length-1]];
                    }                   
                    if(data.id.length > 1){
                        data.id = data.id.join(";");
                    }else{
                        data.id = data.id.join("");
                    }
                    if(data.text.length > 1){
                        data.text = data.text.join("/");
                    }else{
                        data.text = data.text.join("");
                    }
                    
                    _this._onClick(data, isSelected);
                }
            }
        });
    }
    _getParentDataPath(li, data) {
        let deep = parseInt($B.DomUtils.attribute(li, "deep"));
        if (deep > 0) {
            var ul = li.parentNode;
            var $it = ul.previousSibling;
            let $txt = $B.DomUtils.findByClass($it, "._combox_text")[0];
            data.id.unshift($B.DomUtils.attribute($it, "dataid"));
            data.text.unshift($txt.innerText);
            this._getParentDataPath($it.parentNode, data);
        }
    }
    _unSelectedSiblings(listLi) {
        for (let i = 0; i < listLi.length; i++) {
            let li = listLi[i];
            $B.DomUtils.removeClass(li.firstChild, "k_dropdown_item_selected");
            let childUl = li.firstChild.nextSibling;
            if (childUl && childUl.children) {
                this._unSelectedSiblings(childUl.children);
            }
        }
    }
    /***
     * data = {id:text}
     * ***/
    _onClick(data, isSelected) {
        let _this = this;
        clearTimeout(_this.blurDefaultTimer);
        var oldValue = this.getCheckData();//_this.$input.selectedValue;
        let retVal, attr;
        if (_this.isArrayValue) { //多选或者只读
            if (isSelected) {
                if (!_this.opts.multiple) {
                    _this.$input.selectedValue = [];
                }
                _this.$input.selectedValue.push(data);
            } else {
                let newVals = [];
                let values = _this.$input.selectedValue;
                for (let i = 0; i < values.length; i++) {
                    if ((values[i].id + "") !== data.id) {
                        newVals.push(values[i]);
                    }
                }
                _this.$input.selectedValue = newVals;
            }
            attr = _this._renderReadonlyUI();
            if (!_this.opts.multiple) {
                retVal = true;
            }
        } else {
            _this.$input.value = data.text;
            _this.$input.selectedValue = [data];
            attr = _this._renderReadonlyUI();
            retVal = true;
        }
        let newData = this.getCheckData();
        this._notify(oldValue, newData);
        return retVal;
    }
    _notify(oldValue, data) {
        if (this.opts.onSelected) {
            clearTimeout(this.onSelectedTimer);
            this.onSelectedTimer = setTimeout(() => {
                this.opts.onSelected(data);
            }, 1);
        }
        if (this.opts.onChange) {
            clearTimeout(this.onChangeTimer);
            this.onChangeTimer = setTimeout(() => {
                this.opts.onChange(this.elObj.id, data, oldValue);
            }, 1);
        }
    }
    _loopTreeUl($ul, selectIdMap, selectedArr, txtPathArr, idPathArr) {
        let childs = $ul.children;
        for (let i = 0; i < childs.length; i++) {
            let childTxtArr, childIdArr;
            if (!idPathArr) {
                childIdArr = [];
            } else {
                childIdArr = Array.from(idPathArr);
            }
            if (!txtPathArr) {
                childTxtArr = [];
            } else {
                childTxtArr = Array.from(txtPathArr);
            }
            let $t = childs[i].firstChild;
            if ($B.DomUtils.hasClass($t, "k_dropdown_item_selected")) {
                $B.DomUtils.removeClass($t, "k_dropdown_item_selected");
            }
            let dataId = $B.DomUtils.attribute($t, "dataid");
            let $wap = $t.firstChild;
            let $txt = $B.DomUtils.children($wap, "._combox_text")[0];
            childTxtArr.push($txt.innerText);
            childIdArr.push(dataId);
            if (selectIdMap[dataId]) {
                selectedArr.push({ id: childIdArr.join(";"), text: childTxtArr.join("/") });
                $B.DomUtils.addClass($t, "k_dropdown_item_selected");
            }
            if ($t.nextSibling) {
                this._loopTreeUl($t.nextSibling, selectIdMap, selectedArr, childTxtArr, childIdArr);
            }
        }
    }
    setValue(args, notify){  
        if($B.isPlainObjectFn(args)){
            if(Array.isArray(args.id)){
                args = args.id;
            }
        }    
        this.setSelected(args,notify);
    }
    getCheckData(onlyId){
        if(typeof onlyId === "undefined"){
            onlyId = !this.opts.getIdAndTxt;
        }
        var values = this.$input.selectedValue ;
        var ret =[];
        for(let i =0 ; i < values.length ; i++){
            let id = values[i].id;
            let text = values[i].text;
            if(id.indexOf(";") > 0){
                id = id.split(";");
                id = id[id.length - 1];
                text = text.split("/");
                text = text[text.length - 1];
            }
            if(onlyId){
                ret.push(id);
            }else{
                ret.push({id:id,text:text});
            }
        }
        return ret;
    }
    /**
     *  args = [id1,id2,id3];
     *  notify: 是否触发通知
     * ***/
    setSelected(args, notify) {
        let idMap = {};
        let selectedArr = [];
        if (Array.isArray(args)) {
            for (let i = 0; i < args.length; i++) {
                idMap[args[i]] = true;
            }
        } else {
            idMap[args] = true;
        }
        if (this.opts.isTree) {
            this._loopTreeUl(this.$ul, idMap, selectedArr, undefined);
        } else {
            var $wap = this.$dropEl.lastChild.firstChild.firstChild;
            var childs = $wap.children;
            for (let i = 0; i < childs.length; i++) {
                let $t = childs[i];
                let dataid = $B.DomUtils.attribute($t, "dataid");
                if ($B.DomUtils.hasClass($t, "k_dropdown_item_selected")) {
                    $B.DomUtils.removeClass($t, "k_dropdown_item_selected");
                }
                if (idMap[dataid]) {
                    selectedArr.push({ id: dataid, text: $t.innerText });
                    $B.DomUtils.addClass($t, "k_dropdown_item_selected");
                }
            }
        }
        if(selectedArr.length > 0){
            let oldValue = this.getCheckData();
            this.$input.selectedValue = selectedArr;
            this._renderReadonlyUI();
            if (typeof notify === "undefined" || notify) {
                this._notify(oldValue, this.getCheckData());
            }
        }       
    }
    clearSelected(notify) {
        let oldValue = this.getCheckData();
        this.$input.selectedValue = [];
        this._renderReadonlyUI();
        if (typeof notify === "undefined" || notify) {
            this._notify(oldValue, this.getCheckData());
        }
    }
    _createDropList(data) {
        var _this = this;
        let ddOpts = {
            multiple: this.opts.multiple,
            textField: this.opts.textField,
            idField: this.opts.idField,
            motionless: true,
            onClick: function (data, target, isSelected) {
                return _this._onClick(data, isSelected);
            }
        };
        if (this.opts.isTree) {
            //拦截创建，树形列表
            function createFilterFn(items, opts, $inner, $input) { //树形列表的创建                
                let ul = $B.DomUtils.createEl("<ul class='k_combox_tree_ul'/>");
                _this.$ul = ul;
                _this.selectedLiArray = [];
                let len = _this._createTreeUI(items, opts, ul, $input, 0);
                $inner.appendChild(ul);
                _this._renderTreeSelectedUI();
                setTimeout(() => {
                    _this._bindTreeEevents($inner);
                    $B.DomUtils.trigger($inner.parentNode, "myscrollabr.mouseenter");
                }, 1);
                return { _itlen: len, defautlClick: false };
            };
            ddOpts.onCreate = createFilterFn;
        }
        this.$dropEl = $B.createDropList(this.elObj, data, ddOpts);
        this.dropOpts = ddOpts;
        this._renderReadonlyUI();
    }
    _renderTreeSelectedUI() {
        for (let i = 0; i < this.selectedLiArray.length; i++) {
            let li = this.selectedLiArray[i];
            let $it = li.firstChild;
            let id = $B.DomUtils.attribute($it, "dataid");
            let txt = $B.DomUtils.findByClass($it, "._combox_text")[0].innerText;
            let data = { id: [id], text: [txt] };
            this._getParentDataPath(li, data);
            data.id = data.id.join(";");
            data.text = data.text.join("/");
            this._onClick(data, true);
        }
        this.selectedLiArray = [];
    }
    _createTreeUI(items, opts, $ul, $input, deep) {
        let len = items.length;
        for (let i = 0; i < items.length; i++) {
            let _deep = deep;
            let it = items[i];
            let id = items[i][opts.idField]
            let li = $B.DomUtils.createEl("<li deep='" + deep + "'><div class='k_dropdown_list_item' dataid='" + id + "'><div class='k_combox_tree_item_wrap' style='display: inline-block;'></div></div></li>");
            let div = li.firstChild.firstChild;
            if (it.selected) {
                if (!this.opts.multiple) {
                    if (this.selectedLiArray.length === 0) {
                        $B.DomUtils.addClass(li.firstChild, "k_dropdown_item_selected");
                        this.selectedLiArray.push(li);
                    }
                } else {
                    $B.DomUtils.addClass(li.firstChild, "k_dropdown_item_selected");
                    this.selectedLiArray.push(li);
                }
            }
            while (_deep > 0) {
                $B.DomUtils.append(div, "<div class='_deep_helper'></div>");
                _deep--;
            }
            if (it.children) {
                let cls = "fa-folder-open-empty";
                if (it.children.length === 0) {
                    cls = "fa-folder-empty";
                }
                $B.DomUtils.append(div, "<div class='_deep_helper _tree_prt'><i class='fa " + cls + "'></i></div>");
            }
            $B.DomUtils.append(div, "<span class='_combox_text'>" + it[this.opts.textField] + "</span>");
            $B.DomUtils.append($ul, li);
            if (it.children) {
                let childUl = $B.DomUtils.createEl("<ul class='k_combox_tree_ul'></ul>");
                len = len + this._createTreeUI(it.children, opts, childUl, $input, deep + 1);
                $B.DomUtils.append(li, childUl);
            }
        }
        return len;
    }
    _renderReadonlyUI() {       
        let attr;
        let $wap = $B.DomUtils.children(this.elObj, ".k_combox_inner_wap")[0];
        let maxW = parseInt($B.DomUtils.attribute(this.$input, "max_width"));      
        let srcW = parseInt($B.DomUtils.attribute(this.$input, "src_width"));
        let maxWidth = maxW;
        if (this.isArrayValue) { //多选或者只读           
            $B.DomUtils.removeChilds($wap, ".k_combox_user_selected");
            let values = this.$input.selectedValue;
            this.$input.placeholder = "";
            this.$input.value =  "";
            if (values.length === 0) {
                this.$input.placeholder = this.opts.default.text;                
            }
            let idARR = [];
            let txtARR = [];
            let wapHeight = 0;
            let w = 0;           
            if(values.length === 0){
                this.$input.style.width = srcW + "px";
            }
            for (let i = 0; i < values.length; i++) {
                let data = values[i];                
                let it = $B.DomUtils.createEl("<span  style='' dataid='" + data.id + "' class='k_combox_user_selected k_box_size'>" + data.text + "<i class='fa fa-cancel-2'></i></span>");
                $B.DomUtils.append($wap, it);
                this._bindItemEvents(it);
                idARR.push(data.id);
                txtARR.push(data.text);
                let tmpH = $B.DomUtils.height($wap);
                if (wapHeight === 0) {
                    wapHeight = tmpH;
                }
                w = w + $B.DomUtils.outerWidth(it) + 14;
                //当高度发生变化
                if (wapHeight !== tmpH) {
                    if (w < maxWidth) {
                        $wap.style.width = w + "px";
                    }
                    wapHeight = $B.DomUtils.height($wap);
                    let wapW = $B.DomUtils.width($wap);
                    let tmp = wapW;
                    if (!this.opts.readOnly) {
                        tmp = wapW + 80;
                    }
                    if (tmp > maxW) {
                        wapW = maxW;
                    } else {
                        wapW = tmp;
                    }
                    if (wapW < srcW) {
                        wapW = srcW;
                    }
                    this.$input.style.width = wapW + "px";
                }else{
                    w =w + 5;
                    if(w > srcW){
                        if(w < maxWidth){
                            this.$input.style.width = w + "px";
                        }else{
                            this.$input.style.width = maxWidth + "px";
                        }
                    }else{
                        this.$input.style.width = srcW + "px";
                    }           
                }
            }
            attr = { "_id": idARR.join(","), "_text": txtARR.join(",") };
            $B.DomUtils.attribute(this.$input, attr);
            if (wapHeight === 0) {
                wapHeight = 28;
            }
            wapHeight = wapHeight + 4;
            let inputCss = { height: wapHeight, "line-height": wapHeight };
            if (wapHeight < 40) {
                if (!this.opts.readOnly) {
                    w = w - values.length * 4;
                    if (w < 6) {
                        w = 6;
                    }
                    inputCss["padding-left"] = w + "px";
                }
            }
            $B.DomUtils.css(this.$input, inputCss);          
            if (!this.opts.readOnly) {
                this.$input.focus();
            }
            //调整位置
            this.fitListPos();
        } else {
            attr = { "_id": "", "_text": "" };
            if (this.$input.selectedValue.length === 0) {
                this.$input.value = "";
                this.$input.placeholder = this.opts.default.text;
                this.$input.style.width = srcW + "px";
            } else {
                attr["_id"] = this.$input.selectedValue[0].id;
                attr["_text"] = this.$input.selectedValue[0].text;
                let w = $B.getCharWidth(attr["_text"]) + 20;
                this.$input.value = attr["_text"];
                if(w > srcW){
                    if(w < maxWidth){
                        this.$input.style.width = w + "px";
                    }else{
                        this.$input.style.width = maxWidth + "px";
                    }
                }else{
                    this.$input.style.width = srcW + "px";
                }     
            }
            $B.DomUtils.attribute(this.$input, attr);
        }        
        return attr;
    }
    fitListPos() {
        if(this.$dropEl && $B.DomUtils.css(this.$dropEl,"display") !== "none"){
            let $wap = $B.DomUtils.children(this.elObj, ".k_combox_inner_wap")[0];
            let wapHeight = $B.DomUtils.height($wap);
            if(wapHeight < 28){
                wapHeight = 28;
            }
            let ofs = $B.DomUtils.offset(this.elObj);
            let top = ofs.top + wapHeight + 12;
            this.$dropEl.style.top = top + "px";
        }        
    }
    /****
     * 删除选择事件
     * ******/
    _bindItemEvents($it) {
        if (!this.itemClick) {
            var _this = this;
            this.itemClick = function (e) {
                if (e.target.tagName === "I") {
                    let pt = e.target.parentNode;
                    let id = $B.DomUtils.attribute(pt, "dataid");
                    let values = _this.$input.selectedValue;
                    var newVals = [];
                    for (let i = 0; i < values.length; i++) {
                        if (values[i].id + '' !== id) {
                            if (_this.opts.isTree) {
                                let idArr = values[i].id.split(";");
                                newVals.push(idArr[idArr.length - 1]);
                            } else {
                                newVals.push(values[i].id);
                            }
                        }
                    }
                    if(newVals.length > 0){
                        _this.setSelected(newVals);
                    }else{
                        _this.clearSelected();
                    }                   
                    return false;
                }
            };
        }
        $B.DomUtils.click($it, this.itemClick);
    }
    _bindInputEvent($input) {
        var _this = this;
        this.inputEvents = {
            focus: function (e) {
                $B.DomUtils.addClass(_this.$icon, "k_combox_focus_icon");
            },
            blur: function (e) {
                $B.DomUtils.removeClass(_this.$icon, "k_combox_focus_icon");
                if (_this.isArrayValue) {
                    let $wap = $B.DomUtils.children(_this.elObj, ".k_combox_inner_wap");
                    let childs = $B.DomUtils.children($wap, ".k_combox_user_selected");
                    if (childs.length === 0) {
                        _this.blurDefaultTimer = setTimeout(() => {
                            _this.$input.selectedValue = [];
                            _this.$input.value = "";
                            _this.$input.placeholder = _this.opts.default.text;
                        }, 800);
                    }
                    if (!_this.opts.readOnly) {
                        _this.$input.value = "";
                    }
                } else {
                    if (this.value === "") {
                        _this.blurDefaultTimer = setTimeout(() => {
                            _this.$input.selectedValue = _this.opts.default;
                            _this.$input.value = "";
                            _this.$input.placeholder = _this.opts.default.text;
                        }, 800);
                    }
                }
            }
        };
        if (!this.opts.readOnly && this.opts.search) {
            this.inputEvents.input = function (e, imd) {
                if (imd) {
                    _this.userSearch($B.trimFn(this.value));
                } else if (_this.opts.inputImdSearch) {
                    clearTimeout(_this.userInputSearchTimer);
                    _this.userInputSearchTimer = setTimeout(() => {
                        _this.userSearch($B.trimFn(this.value));
                    }, 800);
                }
            };
            this.inputEvents.keydown = function (e) {
                if (e.keyCode === 13) {
                    _this.inputEvents.input.call(this, e, true);
                }
            };
        }
        $B.DomUtils.bind($input, this.inputEvents);
    }
    userSearch(text) {
        if (this.$dropEl) {
            this.$dropEl.showFn();
            text = text.toLowerCase();
            if (this.opts.url) {
                this.request({ keyword: text }, (data) => {
                    let $inner = this.$dropEl.lastChild.firstChild.firstChild;
                    if (this.opts.isTree) {
                        let _itlen = parseInt($B.DomUtils.attribute(this.$dropEl, "_itlen"));
                        let ret = this.dropOpts.onCreate(data, this.dropOpts, $inner, this.$input);
                        _itlen = _itlen + ret._itlen;
                        $B.DomUtils.attribute(this.$dropEl, { "_itlen": _itlen });
                    } else {
                        $B._createDropListItems(data, this.dropOpts, $inner, this.$input, true);
                    }
                    this._renderReadonlyUI();
                });
            } else { //本地搜索
                let $wrap = this.$dropEl.lastChild.firstChild.firstChild;
                if (this.opts.isTree) {
                    console.log("本地树搜索,待开发，，.......");
                } else {
                    for (let i = 0; i < $wrap.children.length; i++) {
                        let it = $wrap.children[i];
                        if (text === "" || it.textContent.toLowerCase().indexOf(text) >= 0) {
                            it.style.display = "block";
                        } else {
                            it.style.display = "none";
                        }
                    }
                }
            }
        }
    }
    request(param, callBack) {
        if (this.opts.setParams) {
            let pr = this.opts.setParams(param);
            if (pr) {
                param = $B.extendObjectFn(true, param, pr);
            }
        }
        let _this = this;
        if (!this.loading) {
            this.loading = $B.getLoadingEl(undefined, "#fff");
            var wrap = this.$dropEl.lastChild.firstChild.firstChild;
            wrap.innerHTML = "";
            $B.DomUtils.append(wrap, this.loading);
        }
        let url = this.opts.url;
        let method = (url.indexOf(".data") > 0 || url.indexOf(".json") > 0) ? "GET" : "POST";
        $B.request({
            dataType: 'json',
            url: url,
            data: param,
            type: method,
            onErrorEval: true,
            onReturn: function () {
                let $l = _this.loading;
                _this.loading = undefined;
                if ($l) {
                    try {
                        $B.removeLoading($l, () => {                            
                        });
                    } catch (ex) {
                        console.log(ex);
                    }
                }
            },
            ok: function (message, data) {
                if (!data || data.length === 0) {
                    data = [];
                }
                if (_this.opts.onloaded) {
                    _this.opts.onloaded(data, param);
                }
                callBack(data);
            }
        });
    }
    destroy(isForce) {
        this.$dropEl._clearAllFn();
        this.elObj.selectedValue = undefined;
        $B.DomUtils.remove(this.$dropEl);
        super.destroy();
    }
}
$B["Combox"] = Combox; 
 if(!window["$B"] ) {window["$B"] = $B;} 
  return window["$B"] ; 
});

/*! BUI - v2.0.0 - 2022-03-13 09:03:17 
Copyright (c): kevin.huang  https://gitee.com/kevin-huang/bui2.0.git (qq:757723114) 
Released under MIT License*/
(function ( global, factory) {
//   "use strict";
	if ( typeof module === "object" && typeof module.exports === "object" ) {
		module.exports = global.document ?
			factory( global, true ) :
			function( w ) {
				if ( !w.document ) {
					throw new Error( "Bui requires a window with a document" );
				}
				return factory( w );
			};
	} else {
		  factory( global );
	}  
})( typeof window !== "undefined" ? window : this, function( window, noGlobal ) {
//   "use strict";
  var $B = window["$B"] ?  window["$B"] : {};

var defaultOpts = {
    show: false,
    fixed: false,//是否固定显示
    fitWidth: false, //是否将input宽度和 控件宽度调整一致
    initValue: undefined, //可以是date对象，可以是字符串
    shadow: true,//是否需要阴影
    fmt: 'yyyy-MM-dd hh:mm:ss',
    tipTime: 2.5,
    clickHide: false,
    readonly: true,
    isSingel: false, //是否是单列模式   
    range: { min: undefined, max: undefined },  //时间日期选择范围定义,min/max = now / inputId ,'yyyyy-MM-dd hh:mm:ss' 
    onChanging: undefined, //onChanging = function(date){}
    onChange: undefined
};
var config = $B.config ? $B.config.calendar : {
    year: '年',
    month: '月',
    clear: '清空',
    now: '今天',
    close: '关闭',
    monthArray: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
    weekArray: ['日', '一', '二', '三', '四', '五', '六'],
    error: '输入是时间格式错误！',
    minTip: '输入时间不能小于[ x ]',
    maxTip: '输入时间不能大于[ x ]',
    error: '输入值不符合格式要求！'
};
var bodyEl;
class Calendar extends $B.BaseControl {
    constructor(elObj, opts) {
        if (!bodyEl) {
            bodyEl = $B.getBody();
        }
        super();
        this.config = config;
        this.opts = $B.extendObjectFn(true, {}, defaultOpts, opts);
        super.setElObj(elObj);
        $B.DomUtils.addClass(this.elObj, "k_calendar_input k_box_size");
        $B.DomUtils.attribute(this.elObj, { "autocomplete": "off" });
        if (this.opts.isSingel) { //全局单例模式            
            this.elObj._calopt = { range: this.opts.range, initValue: opts.initValue, onChanging: this.opts.onChanging,onChange:this.opts.onChange };
            this.opts.fixed = false;
            this.opts.show = false;
            var ins = window["_k_calendar_ins_"];
            if (ins) {
                ins.setTarget(this.elObj, true);
                return ins;
            } else {
                window["_k_calendar_ins_"] = this;
            }
        }
        this.currentDate = new Date();//默认初始化当前时间   
        this.isMonthStyle = this.opts.fmt === "yyyy-MM";
        this.isDayStyle = this.opts.fmt === "yyyy-MM-dd";
        this.isSecondStyle = this.opts.fmt === "yyyy-MM-dd hh:mm:ss";
        this.isTimeStyle = this.opts.fmt === "hh:mm:ss" || this.opts.fmt === "hh:mm";
        if (this.opts.fitWidth) {
            $B.DomUtils.width(this.elObj, 202);
        }
        var value = this.elObj.value;
        if (value !== "") {
            var currentDate = this._txt2Date(value);
            if (currentDate) {
                this.opts.initValue = currentDate;
            }
        }
        let _droplistid = $B.getUUID();
        $B.DomUtils.attribute(this.elObj, { "_droplistid": _droplistid });
        var i, len, _this = this;
        this._initValue(true);
        this._bindInputEvents();
        this.ddlObj = $B.DomUtils.createEl("<div id='" + _droplistid + "' _dlistgid='" + this.id + "' class='k_calendar_wrap  k_box_size clearfix' style='width:202px;z-index:" + $B.config.maxZindex + ";'></div>");
        if (this.opts.fixed) {
            $B.DomUtils.attribute(this.ddlObj, { "z-index": 1 });
            this.opts.show = true;
        }
        if (!this.opts.show) {
            $B.DomUtils.css(this.ddlObj, { "display": "none" });
        }
        if (this.isTimeStyle) {
            $B.DomUtils.css(this.ddlObj, { height: 250 });
            this.createTimeOptList();
        } else {
            /**顶部工具栏**/
            this.headerPanel = $B.DomUtils.createEl("<div class='k_calendar_pannel k_box_size clearfix' style='padding-left:2px;padding-top:2px;'><span class='_prev_year _event' style='width:24px'><i class='fa fa-angle-double-left'></i></span><span  style='width:21px' class='_prev_month _event'><i class='fa fa-angle-left'></i></span><span class='_cur_year  _event' style='width:46px;'>" + this.year + "</span><span>" + config.year + "</span><span class='_cur_month _event' style='width:28px;' >" + this.month + "</span><span>" + config.month + "</span><span style='width:25px' class='_next_month _event'><i class='fa fa-angle-right'></i></span><span class='_next_year _event' style='width:24px'><i class='fa fa-angle-double-right'></i></span></div>");
            $B.DomUtils.append(this.ddlObj, this.headerPanel);
            this.bindHeaderPanelEvents();
            /**周**/
            this.weekPanel = $B.DomUtils.createEl("<div class='k_calendar_week k_box_size' style='padding-left:2px;margin-bottom:5px;'></div>");
            for (i = 0, len = config.weekArray.length; i < len; ++i) {
                $B.DomUtils.append(this.weekPanel, "<span class='k_calendar_week_day'>" + config.weekArray[i] + "</span>");
            }
            $B.DomUtils.append(this.ddlObj, this.weekPanel);
            /**天列表**/
            this.daysPanel = $B.DomUtils.createEl("<div class='k_calendar_days k_box_size clearfix' style='padding-left:2px;'></div>");
            $B.DomUtils.append(this.ddlObj, this.daysPanel);
            this.createDays();
            /**时间***/
            this.timePanel = $B.DomUtils.createEl("<div class='k_calendar_time k_box_size' style='margin:1px 0px;padding-left:2px;text-align:center;'></div>");
            $B.DomUtils.append(this.ddlObj, this.timePanel);
            this.createTimeTools();
            /***工具栏***/
            this.toolPanel = $B.DomUtils.createEl("<div class='k_calendar_bools k_box_size clearfix' style='padding-left:1px;'></div>");
            this.createTools();
            $B.DomUtils.append(this.ddlObj, this.toolPanel);
            if (this.isDayStyle) {
                $B.DomUtils.hide(this.timePanel);
            }
            if (this.isMonthStyle) {//月模式
                $B.DomUtils.hide(this.daysPanel);
                $B.DomUtils.hide(this.timePanel);
                let $cur = $B.DomUtils.children(this.headerPanel, "._cur_month")[0];
                this.headToolClickEvent({ target: $cur });
                $B.DomUtils.css(this.toolPanel, { "margin-top": "168px" });
            }
        }
        this.setPosition();
        $B.DomUtils.append(bodyEl, this.ddlObj);
        if (!this.opts.fixed) {
            $B.DomUtils.addClass(this.ddlObj, "k_dropdown_list_wrap");
            $B.createGlobalBodyHideEv();
        } 
    }
    createTimeOptList() {
        var el1, el2, el3;
        if (this.opts.fmt === "hh:mm") {/**时分模式   makeNiceScroll***/
            el1 = $B.DomUtils.createEl("<div class='k_calendar_hours k_box_size' style='width:50%;height:100%;float:left;text-align:center;overflow:hidden;'></div>");
            $B.DomUtils.append(this.ddlObj, el1);
            el2 = $B.DomUtils.createEl("<div  class='k_calendar_minutes k_box_size' style='width:50%;height:100%;float:left;text-align:center;overflow:hidden;'></div>");
            $B.DomUtils.append(this.ddlObj, el2);
        } else {/**时分秒模式***/
            el1 = $B.DomUtils.createEl("<div class='k_calendar_hours k_box_size' style='width:33.3333%;height:100%;float:left;text-align:center;overflow:hidden;'></div>");
            $B.DomUtils.append(this.ddlObj, el1);
            el2 = $B.DomUtils.createEl("<div class='k_calendar_minutes k_box_size' style='width:33.3333%;height:100%;float:left;text-align:center;overflow:hidden;'></div>");
            $B.DomUtils.append(this.ddlObj, el2);
            el3 = $B.DomUtils.createEl("<div class='k_calendar_seconds k_box_size' style='width:33.3333%;height:100%;float:left;text-align:center;overflow:hidden;'></div>");
            $B.DomUtils.append(this.ddlObj, el3);
        }
        var _this = this;
        if (!this.opts.fixed) {
            var $btn = $B.DomUtils.createEl("<a style='position:absolute;top:1px ;right:1px;cursor:pointer;line-height:14px;'><i class='fa fa-cancel-circled'></i></a>");
            $B.DomUtils.append(this.ddlObj, $btn);
            $B.DomUtils.click($btn, function () {
                _this.hide();
                return false;
            });
        }
        var wrapHour = el1,
            wrapMinu = el2,
            wrapSec = el3;
        var envents = {
            click: function (e) {
                var $t = e.target;
                if ($B.DomUtils.hasClass($t, "k_calendar_time_opts")) {
                    $B.DomUtils.addClass($t, "actived");
                    let siblings = $B.DomUtils.siblings($t);
                    $B.DomUtils.removeClass(siblings, "actived");
                    var val = parseInt($t.innerText);
                    if ($B.DomUtils.hasClass($t, "_k_hours")) {
                        _this.hour = val;
                    } else if ($B.DomUtils.hasClass($t, "_k_minutes")) {
                        _this.minutes = val;
                    } else {
                        _this.seconds = val;
                    }
                    var newDate = _this._var2Date();
                    _this._setCurrentDate(newDate);
                    _this.update2target(_this.currentDate);
                }
                return false;
            },
            mouseenter: function (e) {
                let css = { "padding-right": "7px" };
                let childs = $B.DomUtils.children(e.target);
                for (let i = 0; i < childs.length; i++) {
                    $B.DomUtils.css(childs[i], css);
                }
                $B.DomUtils.css(e.target, { "overflow": "auto" });
            },
            mouseleave: function (e) {
                let css = { "padding-right": "15px" };
                let childs = $B.DomUtils.children(e.target);
                for (let i = 0; i < childs.length; i++) {
                    $B.DomUtils.css(childs[i], css);
                }
                $B.DomUtils.css(e.target, { "overflow": "hidden" });
            }
        };
        /***** this.hour  this.minutes this.seconds ******/
        var i = 0, txt, $el;
        while (i < 24) {
            txt = i;
            if (i < 10) {
                txt = "0" + i;
            }
            $el = $B.DomUtils.createEl("<div class='k_calendar_time_opts _k_hours k_box_size' style='padding-right:15px'>" + txt + "</div>");
            $B.DomUtils.append(wrapHour, $el);
            if (i === _this.hour) {
                $B.DomUtils.addClass($el, "actived");
            }
            i++;
        }
        $B.DomUtils.bind(wrapHour, envents);
        this._createMinuAndSecItems(wrapMinu, '_k_minutes', envents);
        if (wrapSec) {
            this._createMinuAndSecItems(wrapSec, '_k_seconds', envents);
        }
    }
    _createMinuAndSecItems(wrap, cls, envents) {
        var i = 0, txt, $el;
        while (i < 60) {
            txt = i;
            if (i < 10) {
                txt = "0" + i;
            }
            $el = $B.DomUtils.createEl("<div class='k_calendar_time_opts " + cls + " k_box_size' style='padding-right:15px'>" + txt + "</div>");
            $B.DomUtils.append(wrap, $el);
            if (cls === "_k_minutes") {
                if (this.minutes === i) {
                    $B.DomUtils.addClass($el, "actived");
                }
            } else if (this.seconds === i) {
                $B.DomUtils.addClass($el, "actived");
            }
            i++;
        }
        $B.DomUtils.bind(wrap, envents);
    }
    _time2fullDateTxt(value) {
        if (this.isTimeStyle) {
            if (/^(20|21|22|23|[0-1]\d)?:?([0-5]\d)?:?([0-5]\d)?$/.test(value)) {
                return this.currentDate.format("yyyy-MM-dd") + " " + value;
            }
        }
        return value;
    }
    /**
     * 根据输入框值 初始化currentDate  year、month变量 
     * isInit 是否是new 初始化调用
     * ***/
    _initValue(isInit) {
        var value = $B.trimFn(this.elObj.value);
        if (value !== "") {
            this.currentDate = this._txt2Date(value);
        }
        if (isInit) {
            var initVal = this.opts.initValue;
            if (this.opts.isSingel) {
                initVal = this.elObj._calopt.initValue;
            }
            //取 initValue
            if (initVal) {
                if (typeof initVal === "string") {
                    this.currentDate = this._txt2Date(initVal);
                } else {
                    this.currentDate = initVal;
                }
                this.elObj.value = this.currentDate.format(this.opts.fmt);
            }
        }
        this._setVarByDate(this.currentDate);
    }
    activedTimeUi() {
        var _this = this;
        this._activedUIopt(".k_calendar_hours", _this.hour);
        this._activedUIopt(".k_calendar_minutes", _this.minutes);
        this._activedUIopt(".k_calendar_seconds", _this.seconds);
    }
    _activedUIopt(cls, val) {
        let $el = $B.DomUtils.children(this.ddlObj, cls)[0];
        let h = $B.DomUtils.height($el.parentNode);
        h = h - h / 6;
        let optIts = $B.DomUtils.findByClass($el, ".k_calendar_time_opts");
        let top = 0;
        let add = true;
        let actItEL;
        for (let i = 0; i < optIts.length; i++) {
            var $t = optIts[i];
            if(add){
                top = top + $B.DomUtils.height($t);
            }
            if (parseInt($t.innerText) === val) {
                add = false;
                $B.DomUtils.addClass($t, "actived");
                actItEL = $t;
            } else {
                $B.DomUtils.removeClass($t, "actived");
            }
        }
        let diff = top - h;        
        if(diff > 0){
            let nextEL =actItEL.nextSibling;
            let help = 5;
            while(help > 0 && nextEL){
                diff = diff +  $B.DomUtils.height(nextEL);
                nextEL = nextEL.nextSibling;
                help--;
            }
            $B.DomUtils.scrollTop($el,diff);
        }
    }
    _setBorderColor() {
        var borderColor = $B.DomUtils.css(this.elObj, "border-color");
        $B.DomUtils.css(this.ddlObj, { "border-color": borderColor });
    }
    /**
  * 绑定顶部年/月改变事件
  * ***/
    bindHeaderPanelEvents() {
        var _this = this;
        var clickFn = function (e) {
            if (_this.hourItsPanel) {
                $B.DomUtils.hide(_this.hourItsPanel);
            }
            if (_this.mmItsPanel) {
                $B.DomUtils.hide(_this.mmItsPanel);
            }
            var $t = e.target;
            if ($t.tagName === "I") {
                $t = $t.parentNode;
            }
            var newDate = _this._var2Date();//获取当前 this.currentDate的副本，用于加减运算
            var curDay = newDate.getDate();
            var update = true;
            if ($B.DomUtils.hasClass($t, "_prev_year")) {
                newDate.setFullYear(newDate.getFullYear() - 1);
            } else if ($B.DomUtils.hasClass($t, "_prev_month")) {
                //兼容有溢出的场景 先取上一个月最大日期
                var lastMonthDate = new Date(newDate.getFullYear(), newDate.getMonth(), 0, _this.hour, _this.minutes, _this.seconds);
                if (curDay < lastMonthDate.getDate()) {
                    lastMonthDate.setDate(curDay);
                }
                newDate = lastMonthDate;
            } else if ($B.DomUtils.hasClass($t, "_cur_year")) {
                _this._hideMonthPanel();
                update = false; //
                if (_this.yearPanel && !$B.DomUtils.isHide(_this.yearPanel)) {
                    $B.hide(_this.yearPanel, 100);
                } else {
                    _this._createYearOpts(newDate.getFullYear());
                }
            } else if ($B.DomUtils.hasClass($t, "_cur_month")) {
                if (_this.yearPanel) {
                    $B.DomUtils.hide(_this.yearPanel);
                }
                update = false;
                if (!_this.monthPanel) {
                    _this._createMonthOpts();
                } else {
                    if (_this.isMonthStyle) {//月模式不可以关闭
                        if (!_this.opts.fixed) {
                            _this.slideToggle();
                        }
                    } else if ($B.DomUtils.isHide(_this.monthPanel)) {
                        $B.show(_this.monthPanel, 100);
                    } else {
                        $B.hide(_this.monthPanel, 100);
                    }
                }
            } else if ($B.DomUtils.hasClass($t, "_next_month")) {
                //需要考虑月溢出场景
                var nextMonthDate = new Date(newDate.getFullYear(), newDate.getMonth() + 2, 0, _this.hour, _this.minutes, _this.seconds);
                if (curDay < nextMonthDate.getDate()) {
                    nextMonthDate.setDate(curDay);
                }
                newDate = nextMonthDate;
            } else if ($B.DomUtils.hasClass($t, "_next_year")) {
                newDate.setFullYear(newDate.getFullYear() + 1);
            }
            if (update) {
                if (_this.yearPanel) {
                    $B.DomUtils.hide(_this.yearPanel);
                }
                _this._hideMonthPanel();
                if (_this._beforeChange(newDate)) { //先调用日期时间变更
                    _this._updateVar(newDate);
                    _this.update2target(newDate);
                    _this.updateYearAndMonthUi(newDate);
                    _this.rebuildDaysUi(newDate);
                    if (_this.isMonthStyle) {//月模式
                        var curMonth = newDate.getMonth() + 1;
                        _this._activeMonthUi(curMonth);
                    }
                }
            }
            return false;
        };
        $B.DomUtils.click(this.headerPanel, clickFn);
        this.headToolClickEvent = clickFn;
    }
    /**
     * 年选择列表
     * ***/
    _createYearOpts(year) {//创建年选项
        /**年点击事件***/
        var _this = this;
        if (!this._yearOnclick) {
            this._yearOnclick = function (e) {
                var $t, $i;
                var $t = e.target;
                var $i = $B.DomUtils.children($t);
                if ($t.tagName === "I") {
                    $i = [$t];
                }
                if ($i.length > 0) {
                    $i = $i[0];
                    $t = $i.parentNode;
                    var startYear;
                    let siblings = $B.DomUtils.siblings($t, "._year_num");
                    if ($B.DomUtils.hasClass($i, "fa-angle-double-left")) {//
                        startYear = parseInt(siblings[0].innerText) - 18;
                    } else {
                        startYear = parseInt(siblings[siblings.length - 1].innerText) + 1;
                    }
                    var startTg = siblings[0];
                    while (startTg && $B.DomUtils.hasClass(startTg, "_year_num")) {
                        startTg.innerText = startYear;
                        if (startYear === _this.year) {
                            $B.DomUtils.addClass(startTg, "actived");
                        } else {
                            $B.DomUtils.removeClass(startTg, "actived");
                        }
                        startYear++;
                        startTg = startTg.nextSibling;
                    }
                } else {
                    var year = parseInt($t.innerText);
                    _this.year = year;
                    var newDate = _this._var2Date();
                    if (_this._beforeChange(newDate)) {
                        _this._setCurrentDate(newDate);
                        _this.rebuildDaysUi();
                        _this.updateYearAndMonthUi(_this.currentDate);
                        _this.update2target(_this.currentDate);
                        $B.hide(_this.yearPanel, 200);
                    }
                }
                return false;
            };
        }
        if (this.yearPanel) {
            let childs = $B.DomUtils.children(this.yearPanel, ".k_box_size");
            $B.DomUtils.remove(childs);
            $B.DomUtils.show(this.yearPanel);
        } else {
            var _this = this;
            this.yearPanel = $B.DomUtils.createEl("<div class='k_box_size clearfix k_calendar_ym_panel k_dropdown_shadow' style='padding-left:12px;position:absolute;top:30px;left:0;z-index:2110000000;width:100%;background:#fff;'></div>");
            $B.DomUtils.append(this.ddlObj, this.yearPanel);
            var $closed = $B.DomUtils.createEl("<div style='position:absolute;top:-2px;right:5px;width:12px;height:12px;line-height:12px;text-align:center;cursor:pointer;'><i class='fa fa-cancel-2' style='font-size:14px;color:#AAAAB4'></i></div>");
            $B.DomUtils.append(this.yearPanel, $closed);
            $B.DomUtils.click($closed, function () {
                $B.DomUtils.hide(_this.yearPanel);
                return false;
            });
            $B.DomUtils.click(this.yearPanel, this._yearOnclick);
        }
        var startYear = year - 12;
        var help = 22, txt, $it;
        while (help > 0) {
            if (help === 22) {
                txt = "<i style='font-size:16px;color:#A0BFDE' class='fa fa-angle-double-left'></i>";
                $it = $B.DomUtils.createEl("<div class='k_box_size' style='float:left;line-height:20px;padding:5px 5px;cursor:pointer;width:43px;text-align:center;'>" + txt + "</div>");
                $B.DomUtils.append(this.yearPanel, $it);
            }
            txt = startYear;
            $it = $B.DomUtils.createEl("<div class='k_box_size _year_num' style='float:left;line-height:20px;padding:5px 5px;cursor:pointer;width:43px;text-align:center;'>" + txt + "</div>");
            $B.DomUtils.append(this.yearPanel, $it);
            if (txt === year) {
                $B.DomUtils.addClass($it, "actived");
            }
            if (help === 1) {
                txt = "<i style='font-size:16px;color:#A0BFDE' class='fa fa-angle-double-right'></i>";
                $it = $B.DomUtils.createEl("<div class='k_box_size' style='float:left;line-height:20px;padding:5px 5px;cursor:pointer;width:43px;text-align:center;'>" + txt + "</div>");
                $B.DomUtils.append(this.yearPanel, $it);
            }
            help--;
            startYear++;
        }
    }
    /**
     * 月模式下用
     * **/
    _activeMonthUi(month) {
        let mouths = $B.DomUtils.children(this.monthPanel);
        for (let i = 0; i < mouths.length; i++) {
            var $m = mouths[i];
            if (month === parseInt($m.innerText)) {
                $B.DomUtils.addClass($m, "actived");
            } else {
                $B.DomUtils.removeClass($m, "actived");
            }
        }
    }
    _hideMonthPanel() {
        if (!this.isMonthStyle && this.monthPanel) {
            $B.hide(this.monthPanel, 150);
        }
    }
    /***
     * 月选择列表
     * **/
    _createMonthOpts() {
        var _this = this;
        /**月点击事件**/
        if (!this._monthOnclick) {
            this._monthOnclick = function (e) {
                var $t = e.target;
                var month = parseInt($t.innerText);
                _this.month = month;
                var newDate = _this._var2Date();
                if (_this._beforeChange(newDate)) {
                    if (_this.isMonthStyle) {//月模式
                        $B.DomUtils.addClass($t, "actived");
                        let siblings = $B.DomUtils.siblings($t);
                        $B.DomUtils.removeClass(siblings, "actived");
                        if (_this.opts.clickHide) {
                            _this.hide();
                        }
                    }
                    _this._setCurrentDate(newDate);
                    _this.rebuildDaysUi();
                    _this.updateYearAndMonthUi(_this.currentDate);
                    _this.update2target(_this.currentDate);
                    _this._hideMonthPanel();
                }
                return false;
            };
        }
        if (!this.monthPanel) {
            this.monthPanel = $B.DomUtils.createEl("<div class='k_box_size clearfix k_calendar_ym_panel' style='padding-bottom:4px; padding-top:8px;padding-left: 18px; position: absolute; top: 30px; left: 0px; z-index: 2100000000; width: 100%; background: rgb(255, 255, 255);'></div>");
            $B.DomUtils.append(this.ddlObj, this.monthPanel);
            $B.DomUtils.click(this.monthPanel, this._monthOnclick);
            var i = 1, txt, $it;
            while (i < 13) {
                txt = i;
                if (i < 10) {
                    txt = "0" + i;
                }
                $it = $B.DomUtils.createEl("<div style='float:left;line-height:32px;padding:5px 5px;cursor:pointer;width:45px;text-align:center;'>" + txt + "</div>");
                $B.DomUtils.append(this.monthPanel, $it);
                if (i === this.month) {
                    $B.DomUtils.addClass($it, "actived");
                }
                i++;
            }
            if (!this.isMonthStyle) {
                $B.DomUtils.addClass(this.monthPanel, "k_dropdown_shadow");
                var $closed = $B.DomUtils.createEl("<div style='position:absolute;top:-2px;right:5px;width:12px;height:12px;line-height:12px;text-align:center;cursor:pointer;'><i class='fa fa-cancel-2' style='font-size:14px;color:#AAAAB4'></i></div>");
                $B.DomUtils.append(this.monthPanel, $closed);
                $B.DomUtils.click($closed, function () {
                    $B.DomUtils.hide(_this.monthPanel);
                    return false;
                });
            }
        } else {
            $B.DomUtils.show(_this.monthPanel);
            let childs = $B.DomUtils.children(this.monthPanel);
            for (let i = 0; i < childs.length; i++) {
                var $s = childs[i];
                if (parseInt($s.innerText) === _this.month) {
                    $B.DomUtils.addClass($s, "actived");
                } else {
                    $B.DomUtils.removeClass($s, "actived");
                }
            }
        }
    }
    /**底部工具栏按钮***/
    createTools() {
        var _this = this;
        var clickFn = function (e) {
            var $t = e.target;
            if (_this.yearPanel) {
                $B.DomUtils.hide(_this.yearPanel);
            }
            if (!_this.isMonthStyle && _this.monthPanel) {
                $B.DomUtils.hide(_this.monthPanel);
            }
            if ($B.DomUtils.hasClass($t, "002")) {
                var now = new Date();
                _this.year = now.getFullYear();
                _this.month = now.getMonth() + 1;
                _this.day = now.getDate();
                _this.hour = now.getHours();
                _this.minutes = now.getMinutes();
                _this.seconds = now.getSeconds();
                var newDate = _this._var2Date();
                if (_this._beforeChange(newDate)) {
                    _this._setCurrentDate(newDate);
                    _this.rebuildDaysUi();
                    _this.updateYearAndMonthUi(_this.currentDate);
                    _this.update2target(_this.currentDate);
                    _this._updateTimeUi();
                    if (_this.isMonthStyle) {
                        _this._activeMonthUi(_this.month);
                    }
                    if (_this.opts.clickHide) {
                        _this.hide();
                    }
                }
            } else if ($B.DomUtils.hasClass($t, "003")) {
                _this.elObj.value = "";
                let childs = $B.DomUtils.children(_this.daysPanel);
                $B.DomUtils.removeClass(childs, "activted");
            } else {
                _this.hide();
            }
            return false;
        };
        var b1 = $B.DomUtils.createEl("<div class='002'>" + this.config.now + "</div>");
        $B.DomUtils.append(this.toolPanel, b1);
        $B.DomUtils.click(b1, clickFn);
        var b2 = $B.DomUtils.createEl("<div class='003'>" + this.config.clear + "</div>");
        $B.DomUtils.append(this.toolPanel, b2);
        if(this.opts.readonly){
            $B.DomUtils.addClass(b2,"btn_disabled");
        }else{
            $B.DomUtils.click(b2, clickFn);
        }        
        if (this.opts.fixed) {
            $B.DomUtils.width(b1, 99);
            $B.DomUtils.width(b2, 99);
        } else {
            let $i = $B.DomUtils.createEl("<div class='004'>" + this.config.close + "</div>");
            $B.DomUtils.append(this.toolPanel, $i);
            $B.DomUtils.click($i, clickFn);
        }
    }
    _getStrTime() {
        var h = this.hour;
        var m = this.minutes;
        var s = this.seconds;
        if (h < 10) { h = '0' + h; }
        if (m < 10) { m = '0' + m; }
        if (s < 10) { s = '0' + s; }
        return { h: h, m: m, s: s };
    }
    /**时分秒工具栏**/
    createTimeTools() {
        var _this = this;
        var onHclick = function (e) {
            var $s = e.target;
            if ($s.tagName === "SPAN") {
                var v = $s.innerHTML;
                let id = $B.DomUtils.attribute(_this.userInput, "id");
                if (id === "_k_cal_hour") {
                    _this.hour = parseInt(v);
                } else if (id === "_k_cal_minu") {
                    _this.minutes = parseInt(v);
                } else {
                    _this.seconds = parseInt(v);
                }
                var newDate = _this._var2Date();
                if (_this._beforeChange(newDate)) {
                    _this.userInput.value = v;
                    $B.DomUtils.removeClass(_this.userInput, "actived");
                    _this._setCurrentDate(newDate);
                    _this.update2target(newDate);
                    $B.DomUtils.hide($s.parentNode);
                }
            }
            return false;
        };
        var onClickFn = function (e) {
            var $t = e.target;
            if (_this.yearPanel) {
                $B.DomUtils.hide(_this.yearPanel);
            }
            if (_this.monthPanel && !_this.isMonthStyle) {
                $B.DomUtils.hide(_this.monthPanel);
            }
            var i, len, $closed, isShow = true;
            var curVal = $t.value, spanArray;
            if ($B.DomUtils.attribute($t, "id") === "_k_cal_hour") {
                if (_this.mmItsPanel) {
                    $B.DomUtils.hide(_this.mmItsPanel);
                }
                if (!_this.hourItsPanel) {
                    _this.hourItsPanel = $B.DomUtils.createEl("<div class='k_box_size clearfix k_calendar_hours_panel k_dropdown_shadow' style='padding-bottom:2px;padding-left: 12px; position: absolute; top: 30px; left: 0px; z-index: 2112000000; width: 100%; background: rgb(255, 255, 255);'></div>");
                    for (i = 0; i < 24; i++) {
                        $B.DomUtils.append(_this.hourItsPanel, "<span>" + (i < 10 ? '0' + i : i) + "</span>");
                    }
                    $B.DomUtils.append(_this.ddlObj, _this.hourItsPanel);
                    $B.DomUtils.click(_this.hourItsPanel, onHclick);
                    $closed = $B.DomUtils.createEl("<div style='position:absolute;top:-2px;right:5px;width:12px;height:12px;line-height:12px;text-align:center;cursor:pointer;'><i class='fa fa-cancel-2' style='font-size:14px;color:#AAAAB4'></i></div>");
                    $B.DomUtils.append(_this.hourItsPanel, $closed);
                    $B.DomUtils.click($closed, function (e) {
                        $B.DomUtils.hide(_this.hourItsPanel);
                        $B.DomUtils.removeClass(_this.userInput, "actived");
                        return false;
                    });
                } else {
                    if ($B.DomUtils.isHide(_this.hourItsPanel)) {
                        $B.show(_this.hourItsPanel, 200);
                    } else {
                        $B.hide(_this.hourItsPanel, 200);
                        isShow = false;
                    }
                }
                if (isShow) {
                    spanArray = $B.DomUtils.children(_this.hourItsPanel, "span");
                }
            } else {
                if (_this.hourItsPanel) {
                    $B.hide(_this.hourItsPanel, 200);
                }
                if (!_this.mmItsPanel) {
                    _this.mmItsPanel = $B.DomUtils.createEl("<div class='k_box_size clearfix k_calendar_mm_panel k_dropdown_shadow' style='padding-top:0px;padding-left: 12px; position: absolute; top: 29px; left: 0px; z-index: 2112000000; width: 100%; background: rgb(255, 255, 255);'></div>");
                    for (i = 0; i < 60; i++) {
                        $B.DomUtils.append(_this.mmItsPanel, "<span>" + (i < 10 ? '0' + i : i) + "</span>");
                    }
                    $B.DomUtils.append(_this.ddlObj, _this.mmItsPanel);
                    $B.DomUtils.click(_this.mmItsPanel, onHclick);
                    $closed = $B.DomUtils.createEl("<div style='position:absolute;top:-2px;right:5px;width:12px;height:12px;line-height:12px;text-align:center;cursor:pointer;'><i class='fa fa-cancel-2' style='font-size:14px;color:#AAAAB4'></i></div>");
                    $B.DomUtils.append(_this.mmItsPanel, $closed);
                    $B.DomUtils.click($closed, function () {
                        $B.hide(_this.mmItsPanel, 200);
                        $B.DomUtils.removeClass(_this.userInput, "actived");
                        return false;
                    });
                } else {
                    if ($B.DomUtils.isHide(_this.mmItsPanel)) {
                        $B.show(_this.mmItsPanel, 200);
                    } else {
                        if (_this.userInput && _this.userInput === $t) {
                            $B.hide(_this.mmItsPanel, 200);
                            isShow = false;
                        }
                    }
                }
                if (isShow) {
                    spanArray = $B.DomUtils.children(_this.mmItsPanel, "span");
                }
            }
            _this.userInput = $t;
            if (isShow) {
                $B.DomUtils.addClass($t, "actived");
                let siblings = $B.DomUtils.siblings($t);
                $B.DomUtils.removeClass(siblings, "actived");
                for (let i = 0; i < spanArray.length; i++) {
                    var $s = spanArray[i];
                    if ($s.innerText === curVal) {
                        $B.DomUtils.addClass($s, "actived");
                    } else {
                        $B.DomUtils.removeClass($s, "actived");
                    }
                }
            } else {
                $B.DomUtils.removeClass($t, "actived");
            }
            return false;
        };
        var s = this._getStrTime();
        this.$hour = $B.DomUtils.createEl("<input readonly='readonly' style='width:30px;text-align:center;' id='_k_cal_hour' value='" + s.h + "'/>");
        $B.DomUtils.append(this.timePanel, this.$hour);
        $B.DomUtils.click(this.timePanel, onClickFn);
        $B.DomUtils.append(this.timePanel, "<span>：</span>");
        this.$minutes = $B.DomUtils.createEl("<input  readonly='readonly' style='width:30px;text-align:center' id='_k_cal_minu' value='" + s.m + "'/>");
        $B.DomUtils.append(this.timePanel, this.$minutes);
        $B.DomUtils.click(this.$minutes, onClickFn);
        if (this.isSecondStyle) {
            $B.DomUtils.append(this.timePanel, "<span>：</span>");
            this.$seconds = $B.DomUtils.createEl("<input  readonly='readonly' style='width:30px;text-align:center' id='_k_cal_secs' value='" + s.s + "'/>");
            $B.DomUtils.append(this.timePanel, this.$seconds);
            $B.DomUtils.click(this.$seconds, onClickFn);
        }
    }
    /***
     * this.year,this.month 转为 date对象返回
     * ***/
    _var2Date() {
        //存在bug，this.day,29,30,31可能是不存在的，需要修正
        try {
            var tmpDate = new Date(this.year, this.month, 0);
            var maxDay = tmpDate.getDate();
            if (this.day > maxDay) {
                this.day = maxDay;
            }
            var date = new Date(this.year, this.month - 1, this.day, this.hour, this.minutes, this.seconds);
            return date;
        } catch (ex) {
            $B.error(this.config.error);
            return undefined;
        }
        return date;
    }
    _updateTimeUi() {
        var s = this._getStrTime();
        if (this.$hour) {
            this.$hour.value = s.h;
        }
        if (this.$minutes) {
            this.$minutes.value = s.m;
        }
        if (this.$seconds) {
            this.$seconds.value = s.s;
        }
    }
    /**
     * 更新时分秒 下拉框
     * ***/
    _updateMinAndCec() {
        this._updateTimeUi();
    }
    /**
     * 根据newDate新时间重建列表,
     * ****/
    rebuildDaysUi(newDate) {
        if (newDate) {
            this.year = newDate.getFullYear();
            this.month = newDate.getMonth() + 1;
        }
        if (this.daysPanel) {
            var startDate = this._getStartDate();
            var day, month, tmpStr;
            var childs = $B.DomUtils.children(this.daysPanel);
            var one = childs[0];
            while (one) {
                day = startDate.getDate();
                month = startDate.getMonth() + 1;
                tmpStr = startDate.format("yyyy-MM-dd");
                if (month !== this.month) {
                    $B.DomUtils.addClass(one, "day_dull");
                    $B.DomUtils.removeClass(one, "activted");
                    $B.DomUtils.attribute(one, { "title": tmpStr });
                } else {
                    $B.DomUtils.removeClass(one, "day_dull");
                    $B.DomUtils.removeAttribute(one, "title");
                    if (day === this.day) {
                        $B.DomUtils.addClass(one, "activted");
                    } else {
                        $B.DomUtils.removeClass(one, "activted");
                    }
                }
                one.innerText = day;
                $B.DomUtils.attribute(one, { "t": tmpStr })
                startDate.setDate(startDate.getDate() + 1);
                one = one.nextSibling;
            }
        }
    }
    /**
     * 根据新时间更新年月ui
     * ***/
    updateYearAndMonthUi(newDate) {
        if (this.headerPanel) {
            var year = newDate.getFullYear();
            var monthIdx = newDate.getMonth();
            var month = this.config.monthArray[monthIdx];
            let $cur = $B.DomUtils.children(this.headerPanel, "._cur_year");
            if ($cur.length > 0) {
                $cur[0].innerText = year;
                $cur[0].nextSibling.nextSibling.innerText = month;
            }
        }
    }
    /**
     * 将date根据 config.fmt格式化后设置到target中
     * ***/
    update2target(date, notify) {
        var strValue = date.format(this.opts.fmt);
        let oldValue = this.elObj.value;
        this.elObj.value = strValue;
        var onChange = this.opts.onChange;
        if (this.opts.isSingel) {
            onChange = this.elObj._calopt.onChange;
        }    
        if(onChange){
            if(typeof notify === "undefined" || !notify){
                onChange(this.id,strValue,oldValue);
            }
        }
    }
    /***
     * 根据this.year,this.month 获取星期日开始时间
     * ***/
    _getStartDate() {
        var curMonthIdx = this.month - 1;
        var curMonthFisrtDate = new Date(this.year, curMonthIdx, 1);
        var weekDay = curMonthFisrtDate.getDay();
        var startDate = new Date(curMonthFisrtDate);
        while (weekDay > 0) {
            startDate.setDate(startDate.getDate() - 1);
            weekDay--;
        }
        return startDate;
    }
    /**
     * 创建天日期列表
     * ****/
    createDays() {
        var curMonthIdx = this.month - 1;
        var startDate = this._getStartDate();
        var count = 42, tmpStr, tmpSpan, tmpDay, tmpMonth;
        if (!this._dayOnclick) {
            var _this = this;
            this._dayOnclick = function (e) {
                var $t = e.target;
                var arr = $B.DomUtils.attribute($t, "t").split("-");
                _this.year = parseInt(arr[0]);
                _this.month = parseInt(arr[1]);
                _this.day = parseInt(arr[2]);
                var newDate = _this._var2Date();
                if (_this._beforeChange(newDate)) {
                    _this._setCurrentDate(newDate);
                    _this.updateYearAndMonthUi(_this.currentDate);
                    _this.update2target(_this.currentDate);
                    $B.DomUtils.addClass($t, "activted");
                    let siblings = $B.DomUtils.siblings($t);
                    $B.DomUtils.removeClass(siblings, "activted");
                    if (_this.opts.clickHide) {
                        _this.hide();
                    }
                }
                return false;
            };
        };
        while (count > 0) {
            tmpStr = startDate.format("yyyy-MM-dd");
            tmpDay = startDate.getDate();
            tmpMonth = startDate.getMonth();
            tmpSpan = $B.DomUtils.createEl("<span t='" + tmpStr + "'>" + tmpDay + "</span>");
            $B.DomUtils.append(this.daysPanel, tmpSpan);
            if (curMonthIdx !== tmpMonth) {
                $B.DomUtils.addClass(tmpSpan, "day_dull");
                $B.DomUtils.attribute(tmpSpan, { "title": tmpStr });
                $B.DomUtils
            } else {
                if (tmpDay === this.day) {
                    $B.DomUtils.addClass(tmpSpan, "activted");
                }
            }
            startDate.setDate(startDate.getDate() + 1);
            count--;
        }
        $B.DomUtils.click(this.daysPanel, this._dayOnclick);
    }
    _txt2Date(txtDate) {
        if (txtDate === "") {
            return undefined;
        }
        txtDate = this._time2fullDateTxt(txtDate);
        /*****解决 2019-11 格式配置错误的问题 ******/
        var arrTmp = [],
            year,
            month,
            day = 1,
            hour = 0,
            minutes = 0,
            seconds = 0;
        if (txtDate.indexOf('-') > 0) {
            arrTmp = txtDate.split('-');
        } else if (txtDate.indexOf('/') > 0) {
            arrTmp = txtDate.split('/');
        }
        if (arrTmp.length === 2) {
            year = arrTmp[0];
            month = parseInt(arrTmp[1]) - 1;
        } else {
            var m = txtDate.match(/^(\s*\d{4}\s*)(-|\/)\s*(0?[1-9]|1[0-2]\s*)(-|\/)?(\s*[012]?[0-9]|3[01]\s*)?\s*([01]?[0-9]|2[0-4]\s*)?:?(\s*[012345]?[0-9]|\s*)?:?(\s*[012345]?[0-9]\s*)?$/);
            if (m === null || !m[1] && !m[3]) {
                return undefined;
            }
            year = parseInt(m[1]),
                month = parseInt(m[3]) - 1;
            if (m[5]) {
                day = parseInt(m[5]);
            }
            if (m[6]) {
                hour = parseInt(m[6]);
            }
            if (m[7]) {
                minutes = parseInt(m[7]);
            }
            if (m[8]) {
                seconds = parseInt(m[8]);
            }
        }
        var date = new Date(year, month, day, hour, minutes, seconds);
        return date;
    }
    _time2fullDateTxt(value) {
        if (this.isTimeStyle) {
            if (/^(20|21|22|23|[0-1]\d)?:?([0-5]\d)?:?([0-5]\d)?$/.test(value)) {
                return this.currentDate.format("yyyy-MM-dd") + " " + value;
            }
        }
        return value;
    }
    //全局单列模式
    setTarget($input, isInit) {
        if (this.elObj && this.elObj === $input) {
            return;
        }
        this.elObj = $input;
        this._bindInputEvents();
        this._initValue(isInit);
        this.rebuildDaysUi();
        this.updateYearAndMonthUi(this.currentDate);
        this._updateMinAndCec();
    }
    _bindInputEvents() {
        var _this = this;
        var binding = $B.DomUtils.attribute(this.elObj, "k_calr_ins") === undefined;
        if (this.opts.readonly) {
            $B.DomUtils.attribute(this.elObj, { "readonly": true });
        } else if (binding) {//启用了用户输入                          
            var onInputEvFn = function () {
                var v = _this.elObj.value;
                var inputDate = _this._txt2Date(v);
                if (inputDate) {
                    if (_this._beforeChange(inputDate)) {
                        _this._setCurrentDate(inputDate);
                        _this._setVarByDate(inputDate);
                        _this.rebuildDaysUi();
                        _this.updateYearAndMonthUi(_this.currentDate);
                        if (_this.isMonthStyle) {//如果是月模式
                            _this._createMonthOpts();
                        } else if (_this.isTimeStyle) {
                            _this.activedTimeUi();
                        } else {
                            _this._updateMinAndCec();
                        }
                    }
                } else {
                    $B.error(_this.config.error, 2);
                    var srcVal = _this.currentDate.format(_this.opts.fmt);
                    _this.elObj.value = srcVal;
                }
            };
            this.hasInputed = false;
            $B.DomUtils.input(this.elObj, function () {
                _this.hasInputed = true;
                clearTimeout(_this.userInputTimer);
                _this.userInputTimer = setTimeout(function () {
                    _this.hasInputed = false;
                    onInputEvFn();
                }, 1000);
            });
        }
        if (binding) {
            $B.DomUtils.attribute(this.elObj, { "k_calr_ins": 1 });
            $B.DomUtils.bind(this.elObj, {
                "mousedown": function () {
                    $B._0001currentdroplistEl = _this.elObj;
                },
                "blur": function (e) {
                    _this._setBorderColor();
                    if (_this.hasInputed) {
                        _this.hasInputed = false;
                        clearTimeout(_this.userInputTimer);
                        onInputEvFn();
                    }
                },
                "click": function (e) {
                    if (_this.opts.isSingel) {
                        let isSwith = _this.elObj !== e.target;
                        if (isSwith) {
                            _this.setTarget(e.target, false);
                            _this.show();
                            _this._setBorderColor();
                            return false;
                        }
                    }
                    _this._setBorderColor();
                    if (!_this.opts.fixed) {
                        _this.slideToggle();
                    }
                    return false;
                }
            });
        }
    }
    /**
     * 将date参数设置到currentDate
     * ***/
    _setCurrentDate(date) {
        this.currentDate = date;
        /**适配验证***/
        if ($B.DomUtils.hasClass(this.elObj, "k_input_value_err")) {
            $B.DomUtils.removeClass(this.elObj, "k_input_value_err");
            let siblings = $B.DomUtils.siblings(this.elObj, ".k_validate_tip_wrap");
            $B.DomUtils.remove(siblings);
        }
    }
    _getMinMaxCfg(flag) {
        var _date, v;
        var range = this.opts.range;
        if (this.opts.isSingel) { //如果是单列模式
            range = this.elObj._calopt.range;
        }
        if (range[flag]) {
            if (range[flag].indexOf("#") > -1) { //ID
                let id = range[flag];
                let input = $B.DomUtils.findbyId(id);
                v = input.value;
                _date = this._txt2Date(v);
            } else if (range[flag] === "now") {
                _date = new Date();
            } else {
                v = range[flag];
                _date = this._txt2Date(v);
            }
        }
        return _date;
    }
    /**时间变更前回调
        * 如果不符合要求，调用this._setVarByDate(this.currentDate); 恢复变量
        * ***/
    _beforeChange(newDate) {
        var go = true;
        var minDate = this._getMinMaxCfg("min"),
            maxDate = this._getMinMaxCfg("max");
        var res = this._legalDate(newDate, minDate, maxDate);
        if (res !== "") {
            $B.toolTip(this.elObj, res, this.opts.tipTime);
            go = false;
        }
        var changeFn = this.opts.onChanging;
        if (this.opts.isSingel) {
            changeFn = this.elObj._calopt.onChanging;
        }
        if (go && typeof changeFn === "function") {
            res = changeFn(newDate, newDate.format(this.opts.fmt));
            if (typeof res !== "undefined" && res !== "") {
                $B.toolTip(this.elObj, res, this.opts.tipTime);
                go = false;
            }
        }
        if (!go) {//this.currentDate没有被修改, 恢复被修改的变量
            this._setVarByDate(this.currentDate);
        }
        return go;
    }
    /*** 
     * 将date对象提取year、month、day设置到变量this.year,this.month......
     * ***/
    _setVarByDate(date) {
        this.year = date.getFullYear();
        this.month = date.getMonth() + 1;
        this.day = date.getDate();
        this.hour = date.getHours();
        this.minutes = date.getMinutes();
        this.seconds = date.getSeconds();
    }
    /****
         * 最小值，最大值非法限制检查
         * ***/
    _legalDate(newDate, minDate, maxDate) {
        if (minDate) {
            if (newDate.getTime() < minDate.getTime()) {
                return this.config.minTip.replace("x", minDate.format(this.opts.fmt));
            }
        }
        if (maxDate) {
            if (newDate.getTime() > maxDate.getTime()) {
                return this.config.maxTip.replace("x", maxDate.format(this.opts.fmt));
            }
        }
        return "";
    }
    /***
    * 将newDate更新至 currentDate及year\month\day变量
    * ***/
    _updateVar(newDate) {
        this._setCurrentDate(newDate);
        this._setVarByDate(newDate);
    }
    /***
     * 设置时间 date = New Date / yyyy-MM-dd 
     * ***/
    setValue(date,notify) {       
        var newDate;
        if (typeof date === "string") {
            newDate = this._txt2Date(date);
        } else {
            newDate = date;
        }
        this._setCurrentDate(newDate);
        this._setVarByDate(newDate);
        this.rebuildDaysUi();
        this.updateYearAndMonthUi(this.currentDate);
        this._updateMinAndCec();
        this.update2target(this.currentDate,notify);
    }
    setPosition() {
        var ofs = $B.DomUtils.offset(this.elObj);
        var css, height;
        if (this._isTopPosition()) {
            height = this._getPanelHeight();
            css = { top: ofs.top - height, left: ofs.left };
        } else {
            height = $B.DomUtils.outerHeight(this.elObj) - 1;
            css = { top: ofs.top + height, left: ofs.left };
        }
        var bodyWidth = $B.DomUtils.width(bodyEl);
        var maxLeft = ofs.left + 202;
        var diff = maxLeft - bodyWidth;
        if (diff > 0) {
            css.left = css.left - diff;
        }
        $B.DomUtils.css(this.ddlObj, css);
    }
    slideToggle() {
        if ($B.DomUtils.css(this.ddlObj, "display") === "none") {
            this.show();
        } else {
            this.hide();
        }
    }
    _getPanelHeight() {
        var height = 276;
        if (this.isMonthStyle || this.isDayStyle || this.isTimeStyle) {
            height = 250;
        }
        return height;
    }
    _isTopPosition() {
        var bodyHeight = $B.DomUtils.height(bodyEl);
        var ofs = $B.DomUtils.offset(this.elObj);
        var h = $B.DomUtils.outerHeight(this.elObj);
        var h = ofs.top + h + this._getPanelHeight();
        if ((h - bodyHeight) > 0) {
            return true;
        }
        return false;
    }
    hide() {
        if (this.opts.fixed) {
            return;
        }
        if (this.hourItsPanel) {
            $B.DomUtils.hide(this.hourItsPanel);
        }
        if (this.mmItsPanel) {
            $B.DomUtils.hide(this.mmItsPanel);
        }
        if (this._isTopPosition()) {
            $B.DomUtils.hide(this.ddlObj);
        } else {
            $B.slideUp(this.ddlObj, 200);
        }
        if (this.yearPanel) {//恢复ui
            $B.DomUtils.hide(this.yearPanel);
        }
        this._hideMonthPanel();
    }
    show() {
        this.setPosition();
        if (this.opts.fixed) {
            return;
        }
        $B.DomUtils.detach(this.ddlObj);
        $B.DomUtils.append(bodyEl, this.ddlObj);
        if (this._isTopPosition()) {
            $B.DomUtils.show(this.ddlObj);
            this.timeScrollView();
        } else {
            $B.slideDown(this.ddlObj, 200,()=>{
                this.timeScrollView();
            });
        }
    }
    timeScrollView(){
        if(this.isTimeStyle){           
            this.activedTimeUi();
        }
    }
    destroy(isForce) {
        this.elObj._calopt = undefined;
        $B.DomUtils.remove(this.ddlObj);
        super.destroy(isForce);
    }
}
$B["Calendar"] = Calendar; 
 if(!window["$B"] ) {window["$B"] = $B;} 
  return window["$B"] ; 
});

/*! BUI - v2.0.0 - 2022-03-13 09:03:17 
Copyright (c): kevin.huang  https://gitee.com/kevin-huang/bui2.0.git (qq:757723114) 
Released under MIT License*/
(function ( global, factory) {
//   "use strict";
	if ( typeof module === "object" && typeof module.exports === "object" ) {
		module.exports = global.document ?
			factory( global, true ) :
			function( w ) {
				if ( !w.document ) {
					throw new Error( "Bui requires a window with a document" );
				}
				return factory( w );
			};
	} else {
		  factory( global );
	}  
})( typeof window !== "undefined" ? window : this, function( window, noGlobal ) {
//   "use strict";
  var $B = window["$B"] ?  window["$B"] : {};

var chkbox_w = 25;
var lineHeight = 22;
let chkBoxHtml = "<td class='k_box_size'   style='border-left:none;text-align:center;' ><div style='width:" + chkbox_w + "px' class='k_box_size k_table_cell_chkbox'><i class='fa  fa-check-empty'></i></div></td>";
let tdHtml = "<td  class='k_box_size'><div class='k_box_size k_table_cell_text'><div style='width:100%' class='k_table_text k_box_size k_mutilline_ellipsis'></div></div></td>";

/**
 *列默认配置
 ***/
var defaultColOpts = {
    title: '',
    field: '', //字段
    width: 'auto',
    rowspan: 0, // 跨行
    colspan: 0, //跨列
    align: 'left', //对齐'left'、'right'、'center'
    sortable: false, //该列是否可以排序
    resizable: false, //可以调整宽度
    formatter: null //function (cellValue, rowData) {return cellValue }//单元格格式化函数
};
/**
 *table全局配置
 ***/
var defaultOpts = {
    title: '', //标题
    data: undefined, //列表数据 存在初始化数据时，优先用初始化数据
    url: undefined, //url
    fit2height: false, //是否只能占满父元素(垂直方向)
    loadImd: true, //是否立即加载    
    oprCol: true, //是否需要行操作列  
    treeIconColor: undefined,//树形图标颜色
    oprColWidth: 'auto', //操作列最小宽度，默认自动
    rows: [], //列配置，支持多表头
    idField: 'id', //id字段名称
    checkBox: true, //是否需要复选框
    isTree: false, //是否是树形列表
    pgposition: 'bottom', //分页工具栏位置 top,bottom,both ,none none表示不需要分页栏
    pageSize: 30, //页大小
    toolbar: undefined, //顶部工具栏，参考toolbar组件
    lineClamp: 3,//省略号行数
    showLoading:true,//是否显示正在加载
    openExtendTr: false, //是否默认打开展开行
    topBtnStyle: {
        methodsObject: 'tableMethods',
        style: 'normal', //plain   plain / min  / normal /  big             
        showText: true
    },
    trBtnStyle: {
        align: 'center', //对齐方式，默认是left 、center、right
        methodsObject: 'trMethods',
        style: 'plain', //plain   plain / min  / normal /  big             
        showText: true
    },
    pgBtnNum: 10, //页按钮数量
    pageList: [15, 20, 25, 30, 35, 40, 55, 70, 80, 90, 100], //页大小选择
    pgSummary: true,//是否需要总数量，页数提示
    pgButtonCss: undefined,
    iconCls: undefined, //图标   fa-table
    setParams: undefined, //设置参数
    splitColLine: 'k_table_td_all_line', //分割线样式 k_table_td_v_line、k_table_td_h_line，k_table_td_none_line
    sortField: undefined, //默认排序字段
    sortFieldPrex: '_col_sort_',//排序字段参数前缀
    onDbClickRow: null, // function (tr,data) { },//双击一行时触发 fn(tr,data)
    onClickCell: null, // function (field, value) { },//单击一个单元格时触发fn(field, value)
    onCheck: null, //function () { },//复选事件
    onCreateCheckbox:null, //fu(td,data) //当创建checkbox回调
    onRowRender: null, // function (tr,data,rownum) { },//行渲染事件，返回true则开启行展开功能 
    onLoaded: null, //function (data) { }//加载完成回调
    onToolCreated: null, //创建工具栏完成回调 
    onParentOpen:null ,//当展开父节点，用于拦截展开，返回false则不会执行内部请求
    onCreateRowToolbar:null,//行工具栏创建回调fn(opts,data)
    onTrExtended: null, //行展开事件 fn(data, extTr,isShow)
    formatTipFn: undefined //fn (colName,rowData,colIdx,rowIdx)
};
//树转合适顺序的列表
function _tree2list(res, beans) {
    if (!Array.isArray(beans)) {
        beans = [beans];
    }
    for (let i = 0; i < beans.length; i++) {
        let bean = beans[i];
        bean.depth = res.depth;
        res.arr.push(bean);
        if (bean.children) {
            _tree2list({ arr: res.arr, depth: res.depth + 1 }, bean.children);
        }
    }
}
function _createToolbar(ins, toolOps, warp, flagClazz) {
    if (Array.isArray(toolOps.buttons) && toolOps.buttons.length > 0) {      
        let toolbarEl = $B.DomUtils.prepend(warp, "<div class='k_table_toolbar_wrap k_box_size " + flagClazz + "'></div>");        
        let toolbarIns = new $B.Toolbar(toolbarEl, toolOps);
        if (typeof ins.onToolCreated === "function") {
            setTimeout(() => {
                ins.onToolCreated(toolbarIns);
            }, 1);
        }
        ins.toolbarIns = toolbarIns;
        return toolbarEl;
    }
}
/***
 * 创建顶部工具栏，考虑工具栏动态支持功能
 * ****/
function _createTopToolbarFn(ins) {
    ins.destroyTools()
    var toolOps = $B.extendObjectFn(true, {
        buttons: ins.opts.toolbar
    }, ins.opts.topBtnStyle);
    toolOps.align = 'left';
    if ($B.config && $B.config.tableTopToolOpts) {
        toolOps = $B.extendObjectFn(true, toolOps, $B.config.tableTopToolOpts);
    }
    let flagClazz = '_top_tool_';
    let toolWrap = _createToolbar(ins, toolOps, ins.elObj, flagClazz);
    let needtopPg = ins.opts.pgposition === "top" || ins.opts.pgposition === "both";
    if (!toolWrap && needtopPg) {
        toolWrap = $B.DomUtils.prepend(ins.elObj, "<div class='k_table_toolbar_wrap k_box_size " + flagClazz + "'></div>");
    }
    ins.$topToolWrap = toolWrap; //记录起来以便_createPagination使用
}
function _createHeaderTitle(ins) {
    if (ins.opts.title !== '') {
        ins.$title = $B.DomUtils.createEl("<div class='k_table_head_title k_box_size'><h6 style='overflow:hidden;display: -webkit-box;-webkit-box-orient: vertical;-webkit-line-clamp:1' class=''><span class='k_table_h_txt'>" + ins.opts.title + "</span></h6></div>");
        ins.$H6 = ins.$title.firstChild;
        $B.DomUtils.prepend(ins.elObj, ins.$title);
        if (ins.opts.iconCls !== '') {
            $B.DomUtils.prepend(ins.$H6, '<i style="padding-right:6px;" class="fa ' + ins.opts.iconCls + '"></i>');
        }
    }
}
function getColIndexFn(element) {
    var colIndex = 0;
    var childs = element.childNodes;
    let child, colspan;
    for (let i = 0; i < childs.length; i++) {
        child = childs[i];
        colspan = $B.DomUtils.attribute(child, "colspan");
        if (colspan) {
            colIndex = colIndex + parseInt(colspan);
        } else {
            colIndex = colIndex + 1;
        }
    }
    return colIndex;
}
function fixCheckAndOpr(arr) {
    if (arr.length > 1) {
        for (let j = 0; j < arr.length; j++) {
            if (j === 0) {
                $B.DomUtils.attribute(arr[j], { rowspan: arr.length });
            } else {
                arr[j].parentNode.removeChild(arr[j]);
            }
        }
    }
}
function _cretteTableHeader(ins) {
    $B.DomUtils.removeChilds(ins.$headWrap);//先清理已经渲染的头部
    ins.$headTable = $B.DomUtils.createEl("<table></table>");
    var opts = ins.opts;
    var rowspanColMap = {},
        rows = opts.rows,
        delTdArr = [],
        lastRowIndex = rows.length - 1,
        isCheckBox = opts.checkBox,
        row,
        td,
        tr,
        colIndex,
        opt,
        tdOptMap = {},
        oprTitle = $B.config && $B.config.oprColName ? $B.config.oprColName : "action",
        isLastRow, tdAttr, tdCss, $tdTxtDiv,
        sortEl = $B.DomUtils.createEl("<div style='position:absolute;'  class='k_table_cell_sort k_box_size'><i style='bottom:14px' class='fa fa-up-dir'></i><i  style='bottom:2px' class='fa fa-down-dir'></i></div>"),
        modelTr = $B.DomUtils.createEl("<tr></tr>"),
        modelTd = $B.DomUtils.createEl("<td  class='k_box_size'><div class='k_box_size k_table_cell_text'><div style='width:100%' class='k_table_title_text k_box_size'></div></div></td>");
    /****
     * 先创建好tr td html放置于数组中，最后循环一次appendto
     * 处理colspan情况，colspan不需要处理，外部的json已经要求不能有多余的td
     * 处理rowspan情况
     * *****/
    let checkBoxArr = [];
    let oprArr = [];
    let maxColCount = 0;
    let lastColIndex = 0;
    for (let rowIdx = 0, l = rows.length; rowIdx < l; ++rowIdx) {
        row = rows[rowIdx];
        tr = modelTr.cloneNode();
        $B.DomUtils.append(ins.$headTable, tr);      
        isLastRow = rowIdx === lastRowIndex;
        if (isLastRow) {
            lastColIndex = row.length - 1; //计算记录共有多少列;
        }
        if (isCheckBox) { //复选框功能
            let chkTd = $B.DomUtils.createEl("<td class='k_box_size'  row='" + rowIdx + "' col='0'  ischk='1' style='border-left:none;' ><div style='width:" + chkbox_w + "px' class='k_box_size k_table_cell_chkbox'><i class='fa  fa-check-empty'></i></div></td>");
            $B.DomUtils.append(tr, chkTd);
            if (rowIdx === 0) {
                tdOptMap[rowIdx + "_0"] = { isCheckBox: true, width: chkbox_w };
                $B.DomUtils.css(chkTd, { "border-top": "none" });
                ins.$chkTdBtn = chkTd.firstChild;
                ins._bindHeadChkEvent(chkTd);
            }
            checkBoxArr.push(chkTd);
            chkTd._col = 0;
            chkTd._isHeader = true;
            chkTd._ischk = true;
        }
        for (let j = 0, jl = row.length; j < jl; ++j) {
            opt = isLastRow ? $B.extendObjectFn(true, {}, defaultColOpts, row[j]) : row[j];//最后一行才执行列默认配置合并
            if (!opt.width || opt.width === "") {
                opt.width = "auto";
            } else if (opt.width !== "auto") {
                opt.width = (opt.width + "").replace("px", "");
            }
            td = modelTd.cloneNode(true);
            colIndex = getColIndexFn(tr);
            tdAttr = { row: rowIdx, col: colIndex ,field:opt.field};
            if(opt.openTip){
                tdAttr["tip"] = "";
            }
            td._col = colIndex;
            if (opt.colspan) {
                let cl = parseInt(opt.colspan);
                if (cl > 1) {
                    tdAttr["colspan"] = cl;
                }
            }
            if (opt.rowspan) {
                let cl = parseInt(opt.rowspan);
                if (cl > 1) {
                    tdAttr["rowspan"] = cl;
                }
            }
            tdCss = {};
            if (rowIdx === 0) {
                tdCss["border-top"] = "none";
            }
            if (j === 0 && !isCheckBox) {
                tdCss["border-left"] = "none";
            }
            if ((j === jl - 1) && !ins.opts.oprCol) {
                tdCss["border-right"] = "none";
            }
            $tdTxtDiv = td.firstChild.firstChild;
            $B.DomUtils.html($tdTxtDiv, opt.title);
            if (opt.sortable) {
                $B.DomUtils.append(td, sortEl.cloneNode(true));
                ins.bindSortEvents(td);
            }
            //$B.DomUtils.outerWidth(td, opt.width);
            $B.DomUtils.css(td, tdCss);
            /***
             *循环已经添加到tr的td，计算当前的列索引 需要处理colspan的情况
             ****/
            //记录td归属的行、列            
            tdOptMap[rowIdx + "_" + colIndex] = opt;
            $B.DomUtils.append(tr, td);
            //处理被前面行rowspan的元素
            let rowKeys = Object.keys(rowspanColMap);
            for (let r = 0; r < rowKeys.length; r++) {
                let rNum = parseInt(rowKeys[r]);
                if (rNum < rowIdx) {
                    let rowSpanTdArr = rowspanColMap[rowKeys[r]];
                    let currentTdEndIdx = colIndex;
                    if (tdAttr.colspan) {
                        currentTdEndIdx = currentTdEndIdx + tdAttr.colspan - 1;
                    }
                    for (let k = 0; k < rowSpanTdArr.length; k++) {
                        let spTd = rowSpanTdArr[k];
                        let rowSpn = parseInt($B.DomUtils.attribute(spTd, "rowspan")) + rNum;
                        if (rowSpn > rowIdx) {
                            let startIdx = parseInt($B.DomUtils.attribute(spTd, "col"));
                            let endIdx = startIdx;
                            let colSpn = $B.DomUtils.attribute(spTd, "colspan");
                            if (colSpn) {
                                endIdx = startIdx + parseInt(colSpn) - 1;
                            }
                            //当前列号范围在合并单元格的列号范围内，说明这是一个被合并的单元格
                            //判定被合并的单元格是否存在colspan，存在则做colspan减除，否则是需要删除的
                            let isFix;
                            if (colIndex < startIdx) {
                                isFix = currentTdEndIdx >= startIdx;
                            } else {
                                isFix = startIdx <= colIndex && colIndex <= endIdx;
                            }
                            if (isFix) {
                                if (tdAttr.colspan) { //当前td是否存在colspan
                                    let diff = 1;
                                    if (colSpn) {
                                        diff = parseInt(colSpn);
                                    }
                                    tdAttr.colspan = tdAttr.colspan - diff;
                                    if (tdAttr.colspan < 1) {
                                        delTdArr.push(td);
                                    } else if (tdAttr.colspan === 1) {
                                        delete tdAttr.colspan;
                                    }
                                } else {
                                    delTdArr.push(td);
                                }
                            } else {
                                break;
                            }
                        }
                    }
                }
            }
            $B.DomUtils.attribute(td, tdAttr);
            /*判断是否是被rowspan的单元格
             *1)先将rowspan的列记录到rowspanCol,记录当前rowspan的范围
             *2)如果该列存在rowspan，则取出然后判断当前的td是否在rowspan范围内，如果在则记录起来
             * *****/
            if (tdAttr["rowspan"]) {
                if (!rowspanColMap[rowIdx]) {
                    rowspanColMap[rowIdx] = [];
                }
                rowspanColMap[rowIdx].push(td);
            }
            td._isHeader = true;
        }
        if (ins.opts.oprCol) {//操作列  
            colIndex++;
            let opTd = modelTd.cloneNode(true);
            opTd._isHeader = true;
            let opCss = { "border-right": "none" };
            // if (ins.opts.oprColWidth) {
            //     opCss.width = ins.opts.oprColWidth;
            // }
            $B.DomUtils.css(opTd, opCss);
            $B.DomUtils.attribute(opTd, { row: rowIdx, col: colIndex });
            $B.DomUtils.html(opTd.firstChild.firstChild, oprTitle);
            $B.DomUtils.append(tr, opTd);
            if (rowIdx === 0) {
                let _key = rowIdx + "_" + colIndex;
                tdOptMap[_key] = { isOpr: true, width: 'auto' };
                if (ins.opts.oprColWidth) {
                    tdOptMap[_key].width = ins.opts.oprColWidth;
                }
                $B.DomUtils.css(opTd, { "border-top": "none" });
            }
            oprArr.push(opTd);
            opTd._col = colIndex;
        }
        if (colIndex > maxColCount) {
            maxColCount = colIndex;
        }
    }
    //修正checkbox opr操作列
    fixCheckAndOpr(checkBoxArr);
    fixCheckAndOpr(oprArr);
    //删除被rowspan的td
    var waitingMegerOpt = {};
    for (let d = 0, ll = delTdArr.length; d < ll; ++d) {
        let rNumb = parseInt($B.DomUtils.attribute(delTdArr[d], "row"));
        let cNumb = $B.DomUtils.attribute(delTdArr[d], "col");
        let optKey = rNumb + "_" + cNumb;
        if (rNumb === lastRowIndex) {
            waitingMegerOpt[cNumb] = tdOptMap[optKey];
        }
        delete tdOptMap[optKey];
        delTdArr[d].parentNode.removeChild(delTdArr[d]);
    }
    //添加一个隐藏行
    ins.$hideTr = $B.DomUtils.createEl("<tr style='height:0px;'></tr>");
    let i = 0, $td, css, attr;
    let tdHtml = "<td><div></div></td>";
    ins.colOptArray = [];
    ins.autoCols = [];
    while (i <= maxColCount) {
        $td = $B.DomUtils.createEl(tdHtml);
        css = { "border-top": "none", "border-bottom": "none" };
        attr = { col: i };
        if (i === 0) {
            css["border-left"] = "none";
        } else if (i === maxColCount) {
            css["border-right"] = "none";
        }
        $B.DomUtils.css($td, css);
        $B.DomUtils.attribute($td, attr);
        $B.DomUtils.append(ins.$hideTr, $td);
        let r = rows.length;
        let key = r + "_" + i;
        let opt = tdOptMap[key];
        while (!opt) {
            if (r === 0) {
                break;
            }
            r = r - 1;
            key = r + "_" + i;
            opt = tdOptMap[key];
        }
        if (waitingMegerOpt[i]) {
            opt = $B.extendObjectFn(opt, waitingMegerOpt[i]);
        }
        ins.colOptArray.push(opt);
        //获取字符宽度，灵活设置列的最大，最小宽度
        let chartWidth = $B.getCharWidth(opt.title);
        opt.minWidth = chartWidth + 16;
        if (opt.sortable) {
            opt.minWidth = chartWidth + 20;
        }
        if ((opt.width + "") === "auto") {
            ins.autoCols.push(i);
        } else {
            $B.DomUtils.css($td.firstChild, { width: opt.width });
        }
        i++;
    }
    ins.colTdMap = {};
    //提取所有标题单元格
    for (let i = 0; i < ins.$headTable.children.length; i++) {
        let r = ins.$headTable.children[i];
        for (let j = 0; j < r.children.length; j++) {
            let _td = r.children[j];
            _td._isHeader = true;
            let colIdx = _td._col;
            ins.colTdMap[colIdx] = _td;
        }
    }
    $B.DomUtils.prepend(ins.$headTable, ins.$hideTr);
    $B.DomUtils.prepend(ins.$headWrap, ins.$headTable);
    ins.autoHeaderWidth();   
    let colKeys = Object.keys( ins.colTdMap);
    for (let t = 0; t < colKeys.length; t++) {
        ins._bindTdEvents(ins.colTdMap[colKeys[t]]);
    }
}
function _appendEmptyTr(ins, msg) {
    let w = $B.DomUtils.width(ins.$bodyTable.parentNode) - 2;
    let emptyTr = $B.DomUtils.createEl("<tr><td style='text-align:center;'><div style='width:" + w + "px'><i style='color:#F5C400;padding-right:8px;' class='fa fa-attention'></i>" + msg + "</div></td></tr>");
    if (msg === "") {
        emptyTr.firstChild.firstChild.innerHTML = "";
    }
    ins.$bodyTable.appendChild(emptyTr);
    return emptyTr;
}
function _createTr(ins, rowNum, data) {
    var isTree = ins.opts.isTree;
    let tr = document.createElement("tr");
    let idVal;
    if (isTree) {
        $B.DomUtils.attribute(tr, { "deep": data.depth });
        idVal = data.data ? data.data[ins.opts.idField] : data[ins.opts.idField];
    }else{
        idVal = data[ins.opts.idField];
    }
    
    $B.DomUtils.attribute(tr,{"data-id":idVal});
    let isHline = ins.opts.splitColLine.indexOf('td_h_line') > 0 ||  ins.opts.splitColLine.indexOf('td_none_line') > 0;
    let firstCol = true;
    for (let j = 0; j < ins.colOptArray.length; j++) {
        let opt = ins.colOptArray[j];
        let td, tdAttr = { row: rowNum, col: j };
        if (opt.isCheckBox) {//复选框列
            td = $B.DomUtils.append(tr, chkBoxHtml);
            tdAttr["ischk"] = 1;
            if (data._checked) { //复选
                let $i = td.firstChild.firstChild;
                $B.DomUtils.removeClass($i, "fa-check-empty fa-minus-squared");
                $B.DomUtils.addClass($i, "fa-check");
            }
            td._ischk = true;
            if(isHline){
                td.style.width = (chkbox_w + 1) + "px";
            }
            if(ins.opts.onCreateCheckbox){
                ins.opts.onCreateCheckbox(td,data);
            }
        } else {
            td = $B.DomUtils.append(tr, tdHtml);
            if (opt.isOpr) {//操作列 rowTools
                tdAttr["isopr"] = 1;
                let btnOpts = $B.extendObjectFn(true, {params:data}, ins.opts.trBtnStyle);
                btnOpts["buttons"] = data.toolbar;  
                if(ins.opts.onCreateRowToolbar){
                    ins.opts.onCreateRowToolbar(btnOpts,data);
                }             
                let toolIns = new $B.Toolbar(td.firstChild.firstChild, btnOpts);
                ins.rowTools.push(toolIns);
                delete data.toolbar;
            } else {//普通数据列
                let field = opt.field;                
                let val,dataObj = data;
                if (isTree && data.data) {
                    dataObj = data.data;
                } 
                if(field.indexOf(".") > 0){
                    let paths = field.split(".");
                    let jlen = paths.length - 1;
                    for(let j =0 ; j < jlen;j++){
                        dataObj = dataObj[paths[j]];
                    }
                    val = dataObj[paths[jlen]]; 
                }else{
                    val = dataObj[field]; 
                }                           
                if (opt.formatter) {
                    let tmpVal;
                    if ((typeof opt.formatter === "string") && typeof window[opt.formatter] === "function") {
                        tmpVal = window[opt.formatter].call(td,val, data);
                    } else if (typeof opt.formatter === "function") {
                        tmpVal = opt.formatter.call(td, val, data);
                    }
                    if(typeof tmpVal !== undefined && typeof tmpVal !== "boolean"){
                        val = tmpVal;
                    }else if(typeof tmpVal === "boolean" && !tmpVal){
                        val = undefined;
                    } else{
                        val = tmpVal;
                    }                  
                }
                let isTreeTitleCol = isTree && firstCol;
                if (isTreeTitleCol) {
                    firstCol = false;
                    opt.align = "left";
                    //根据deepth
                    let depth = data.depth;
                    let htmlArr = [];
                    while (depth > 0) {
                        htmlArr.push("<span class='_tree_span' style='display:inline-block;width:18px;'></span>");
                        depth--;
                    }
                    if (data.children) {
                        let iconFold = "fa-folder-open";
                        if (data.children.length === 0) {
                            iconFold = "fa-folder";
                        }
                        htmlArr.push("<span class='tree_fold_ _tree_span' style='display:inline-block;width:16px;padding-right:5px;'><i style='cursor:pointer;' class='fa " + iconFold + "'></i></span>");
                        ins._bindTreeEvents(td);
                    } else {
                        htmlArr.push("<span class='_tree_span' style='display:inline-block;width:16px;padding-right:5px'><i class='fa fa-doc'></i></span>");
                    }
                    val = htmlArr.join("") + (val === undefined ? "" : val);                   
                    $B.DomUtils.css(td.firstChild.firstChild,{"-webkit-line-clamp":1});
                }
                if(val !== undefined){
                    td.firstChild.firstChild.innerHTML = val;
                }                
                if (opt.align) {
                    $B.DomUtils.css(td, { "text-align": opt.align });
                }
                //最大省略号行数
                let maxHeight = ins.opts.lineClamp * lineHeight;
                $B.DomUtils.css(td.firstChild, { "max-height": maxHeight });
                if(!isTreeTitleCol){
                    $B.DomUtils.css(td.firstChild.firstChild, { "-webkit-line-clamp": ins.opts.lineClamp,"word-break":"break-all" });
                }                
                ins._bindTdEvents(td);
            }
        }
        //if (rowNum === 0) {
            let tdWidth = ins.colWidthArray[j];
            if (isHline) {
                tdWidth++;
            }
            $B.DomUtils.outerWidth(td.firstChild, tdWidth);
        //}
        $B.DomUtils.addClass(td, ins.opts.splitColLine);
        $B.DomUtils.attribute(td, tdAttr);
        td._col = j;
        td._row = rowNum;
    }
    let ret = false;
    if(ins.opts.onRowRender){
        ret = ins.opts.onRowRender(tr, data, rowNum);
    }
    if (typeof ret !== "undefined" && ret) {
        ins.extendTr(tr);
    }
    return tr;
}
function _makeTrByFragment(ins, datas) {
    ins.formatDataArr = [];
    ins.destroyRowTools();
    if (ins.opts.data.totalSize === 0) {
        ins._makeEmptyTrTip();
        return;
    }
    ins.$emptyTr = undefined;
    var fragment = document.createDocumentFragment();
    for (let i = 0; i < datas.length; i++) {
        let data = datas[i];
        let tr = _createTr(ins, i, data);
        fragment.appendChild(tr);
        ins.formatDataArr.push(data);
    }
    $B.DomUtils.removeChilds(ins.$bodyTable);
    ins.opts.data = undefined;
    return fragment;
}
function _cretteTableBody(ins) {
    //更新分页工具栏
    if (ins.topPgIns) {
        ins.topPgIns.update(ins.page, ins.opts.pageSize, ins.opts.data.totalSize);
    }
    if (ins.bottomPgIns) {
        ins.bottomPgIns.update(ins.page, ins.opts.pageSize, ins.opts.data.totalSize);
    }
    let fragment = _makeTrByFragment(ins, ins.opts.data.resultList);
    if(fragment){
        ins.$bodyTable.appendChild(fragment);
    }   
}
function _createPagination(ins) {
    if (ins.opts.isTree) {
        return;
    }
    if ($B["Pagination"]) {
        var $topWrap = ins.$topToolWrap;
        var total = ins.opts.data && ins.opts.data.totalSize ? ins.opts.data.totalSize : 0;
        let pgOpt = {
            total: total,
            page: ins.page,
            pageList: ins.opts.pageList, //页大小项
            buttons: ins.opts.pgBtnNum,
            pageSize: ins.opts.pageSize,
            summary: ins.opts.pgSummary,
            buttonCss: ins.opts.pgButtonCss,
            onClick: function (page, pageSize) {               
                ins.page = page;
                ins.opts.pageSize = pageSize;
                ins._load();
            }
        };
        if (ins.topPgIns) {
            ins.topPgIns.destroy();
            ins.topPgIns = undefined;
        }
        ins.destroyBottomTools();
        if (ins.opts.pgposition === "top" || ins.opts.pgposition === "both") {
            if (ins.toolbarIns) {
                let $pgWrap = $B.DomUtils.createEl("<div style='float:right'></div>");
                $topWrap.appendChild($pgWrap);
                $topWrap = $pgWrap;
            }
            ins.topPgIns = new $B.Pagination($topWrap, pgOpt);
        }
        if (ins.opts.pgposition === "bottom" || ins.opts.pgposition === "both") {
            let $pgWrap = $B.DomUtils.createEl("<div class='k_box_size' style=''></div>");
            ins.elObj.appendChild($pgWrap);
            ins.bottomPgIns = new $B.Pagination($pgWrap, pgOpt);
        }
    }
}
function _cretteTreeTableBody(ins) {
    let list = { arr: [], depth: 0 };
    let data = ins.opts.data;
    if(data.resultList){
        data = data.resultList;
    }
    _tree2list(list, data);
    ins.opts.data = list.arr;
    let fragment = _makeTrByFragment(ins, ins.opts.data);
    ins.$bodyTable.appendChild(fragment);
}
function _getTd(pNode) {
    while (pNode) {
        if (pNode.tagName === "TD") {
            break;
        }
        pNode = pNode.parentNode;
    }
    return pNode;
}
class Table extends $B.BaseControl {
    constructor(elObj, opts) {
        super();
        super.setElObj(elObj);
        this.page = 1;
        this.toolbarIns = undefined;
        this.formatDataArr = [];       
        this.rowTools = [];
        this.exttrMap = {};//扩展行map
        var tableEl = this.elObj;
        $B.DomUtils.addClass(this.elObj, "k_table_tbody");
        var tableHtml = this.elObj.outerHTML;
        this.opts = $B.extendObjectFn(true, {}, defaultOpts, opts);
        if (!$B.isArrayFn(this.opts.rows[0])) { //多维表头
            this.opts.rows = [this.opts.rows];
        }
        this.elObj = $B.DomUtils.createEl('<div style="overflow:hidden" class="k_table_main_wrap k_box_size">'
            + '<div style="overflow-x:hide;position:relative;" class="k_table_hscroll_wrap k_box_size">'
            + '<div style="overflow-x:visible;" class="k_table_head_wrap k_box_size"></div>'
            + '<div class="k_table_body_wrap k_box_size" style="width:auto;"></div>'
            + '</div></div>');
        this.$hsrollEl = $B.DomUtils.children(this.elObj, ".k_table_hscroll_wrap")[0];
        this.$headWrap = $B.DomUtils.children(this.$hsrollEl, ".k_table_head_wrap")[0];
        this.$bodyWrap = $B.DomUtils.children(this.$hsrollEl, ".k_table_body_wrap")[0];

        //这里需要考虑元素被声明了位置的情况
        let posAttr = $B.DomUtils.css(tableEl, "position");
        if (posAttr !== "static") {
            let cssinf = $B.DomUtils.domInfo(tableEl);
            $B.DomUtils.css(this.elObj, { position: posAttr, top: cssinf.top, left: cssinf.left });
            tableEl.style.position = "static";
        }
        var _this = this;
        $B.DomUtils.scroll(this.$hsrollEl, function (e) {
            _this._updateSrollIcon();
            _this._closeToolTip();
        });
        this.$bodyWrap = $B.myScrollbar(this.$bodyWrap, {
            unScroll: 'hidden',
            scrollAxis: 'y',
            isHide: true,
            onScrollFn: function () {
                _this._closeToolTip();
            },
            onMkScrollFn: (wrap) => {
                if (!this.$scrolyIcon) {
                    let els = $B.DomUtils.children(wrap, ".k_scrollbar_y");
                    if (els.length > 0) {
                        this.$scrolyIcon = els[0];
                    }
                }
                this._updateSrollIcon();
            }
        });
        $B.DomUtils.append(this.$bodyWrap, tableHtml);
        $B.DomUtils.after(tableEl, this.elObj);
        $B.DomUtils.remove(tableEl);
        this.$bodyTable = $B.DomUtils.children(this.$bodyWrap, "table")[0];
        this.colOptArray = [];
        _createTopToolbarFn(this);
        _createHeaderTitle(this);
        _cretteTableHeader(this);
        if (!this.opts.isTree) {
            this.createPagination();
        }
        this.fit2height();
        if (this.opts.splitColLine === 'k_table_td_v_line') {
            $B.DomUtils.addClass(this.$bodyTable, "k_table_tbody_even");
        }
        if (this.opts.data) {
            if (this.opts.isTree) { //树形
                _cretteTreeTableBody(this);
            } else {
                _cretteTableBody(this);
            }
            this._triggerExentdTrs();
        } else if (this.opts.url && this.opts.loadImd) {
            this._load();
        } else {
            this._makeEmptyTrTip();
        }
        var _this = this;
        var isReqFrame = false;
        var resizeEvent = function () {
            if (!isReqFrame) {
                isReqFrame = true;
                requestAnimationFrame(() => {
                    isReqFrame = false;
                    if (_this.asyncWidth) {
                        _this.autoHeaderWidth();
                        _this.asyncWidth();
                    } else {
                        $B.DomUtils.offEvents(window, resizeEvent);
                    }
                });
            }
        };
        this.resizeEvent = resizeEvent;
        $B.DomUtils.resize(window, this.resizeEvent, 100);
        this.bindTableEvents();
    }
    _triggerExentdTrs() {
        if (this.triggerExtendTrs) {
            let triggers = this.triggerExtendTrs;
            this.triggerExtendTrs = [];
            setTimeout(() => {
                for (let i = 0; i < triggers.length; i++) {
                    $B.DomUtils.trigger(triggers[i], "click");
                }
            }, 0);
        }
    }
    _executeTreeTrUi(tr, deep, isOpen) {
        let nextTr = tr.nextSibling;
        while (nextTr) {
            if ($B.DomUtils.hasClass(nextTr, "_ext_tr")) {
                if (isOpen) {
                    nextTr.style.display = "table-row";
                } else {
                    nextTr.style.display = "none";
                }
                nextTr = nextTr.nextSibling;
            }
            let nextDeep = parseInt($B.DomUtils.attribute(nextTr, "deep"));
            if (nextDeep <= deep) {
                break;
            }
            if (isOpen) {
                //当展开的时候，并不一定是所有都展开，其子元素肯定是展开的，但是子元素的下的子元素不一定是展开的
                //检查本行是不是父元素，如果是父元素需要检查是否存在子元素，如果存在，需要检查这个节点是否是收起状态
                let row = parseInt($B.DomUtils.attribute(nextTr.firstChild, "row"));
                let data = this.formatDataArr[row];
                if (data.children) {//如果是父元素
                    $B.slideDown(nextTr, 100);//直接展开即可
                    let $foldEl;
                    if (this.opts.checkBox) {
                        $foldEl = $B.DomUtils.findByClass(nextTr.firstChild.nextSibling, "tree_fold_")[0].firstChild;
                    } else {
                        $foldEl = $B.DomUtils.findByClass(nextTr.firstChild, "tree_fold_")[0].firstChild;
                    }
                    if (data.children.length > 0) {//已经存在了子元素
                        if ($B.DomUtils.hasClass($foldEl, "fa-folder")) {//是隐藏的节点                            
                            nextTr = this._executeTreeTrUi(nextTr, nextDeep, false);
                            continue;
                        }
                    } else {
                        $B.DomUtils.addClass($foldEl, "fa-folder");
                        $B.DomUtils.removeClass($foldEl, "fa-folder-open");
                    }
                } else {
                    $B.slideDown(nextTr, 100);
                }
            } else {
                $B.slideUp(nextTr, 100);//当收起时候，所有都会收起
            }
            nextTr = nextTr.nextSibling;
        }
        return nextTr;
    }
    _bindTreeEvents(td) {
        if (!this.treeClickEvent) {
            var _this = this;
            this.treeClickEvent = function (e) {
                let el = e.target;
                if (el.tagName === "SPAN") {
                    el = el.parentNode;
                }
                if (el.tagName !== "I") {
                    return false;
                }
                let tr = e.target;
                while (tr) {
                    if (tr.nodeName === "TR") {
                        break;
                    }
                    tr = tr.parentNode;
                }
                let isOpen = true;
                if ($B.DomUtils.hasClass(el, "fa-folder-open")) {
                    $B.DomUtils.removeClass(el, "fa-folder-open");
                    $B.DomUtils.addClass(el, "fa-folder");
                    isOpen = false;
                } else {
                    $B.DomUtils.addClass(el, "fa-folder-open");
                    $B.DomUtils.removeClass(el, "fa-folder");
                }
                let deep = parseInt($B.DomUtils.attribute(tr, "deep"));
                if (isOpen) {
                    let row = parseInt($B.DomUtils.attribute(tr.firstChild, "row"));
                    let data = _this.formatDataArr[row];
                    if (data.children.length === 0) {
                        let goRq = true;
                        if(_this.opts.onParentOpen){
                            let r = _this.opts.onParentOpen.call(el,data);
                            if(typeof r !== "undefined"){
                                goRq = r;
                            }
                        }
                        if(goRq){
                            $B.DomUtils.removeClass(el, "fa-folder fa-folder-open");
                            $B.DomUtils.addClass(el, "fa-spin5 animate-spin");                       
                            let prs = {pid:data.id};
                            _this._load(prs, function (isOk, childs) {
                                if (isOk) {
                                    if (!Array.isArray(childs)) {
                                        childs = [childs];
                                    }
                                    if (childs.length === 0) {
                                        $B.toolTip(el, ($B.config && $B.config.returnEmptyData) ? $B.config.returnEmptyData : 'the return is empty!', 1.6);
                                        return;
                                    }
                                    data.children = childs;
                                    _this._appendTree(tr, childs);
                                } else {
                                    $B.DomUtils.removeClass(el, "fa-spin5 animate-spin");
                                    $B.DomUtils.addClass(el, "fa-folder-open");
                                }
                            });
                        }                        
                        return false;
                    }
                }
                _this._executeTreeTrUi(tr, deep, isOpen);
                return false;
            };
        }
        setTimeout(() => {
            let $fold = $B.DomUtils.findByClass(td, "tree_fold_")[0];
            $B.DomUtils.click($fold, this.treeClickEvent);
        }, 0);
    }
    _appendTree(tr, childs) {
        let currentTr = tr;
        let deep = parseInt($B.DomUtils.attribute(tr, "deep"));
        let row = parseInt($B.DomUtils.attribute(tr.firstChild, "row")) + 1;
        let newFormateData = this.formatDataArr.slice(0, row);
        let leftFormatData;
        if (row < this.formatDataArr.length) {
            leftFormatData = this.formatDataArr.slice(row);
        }
        let list = { arr: [], depth: deep + 1 };
        _tree2list(list, childs);
        list = list.arr;
        for (let i = 0; i < list.length; i++) {
            let bean = list[i];
            let newtr = _createTr(this, row, bean);
            $B.DomUtils.after(currentTr, newtr);
            newFormateData.push(bean);
            currentTr = newtr;
            row++;
        }
        //更新后面的行号
        let nextTr = currentTr.nextSibling;
        while (nextTr) {
            let tdArr = $B.DomUtils.children(nextTr);
            for (let j = 0; j < tdArr.length; j++) {
                tdArr[j]._row = row;
                $B.DomUtils.attribute(tdArr[j], { row: row });
            }
            row++;
            nextTr = nextTr.nextSibling;
        }
        if (leftFormatData) {
            this.formatDataArr = newFormateData.concat(leftFormatData);
        } else {
            this.formatDataArr = newFormateData;
        }
    }
    _makeEmptyTrTip() {
        $B.DomUtils.removeChilds(this.$bodyTable);
        this.opts.data = undefined;
        let msg = $B.config && $B.config.returnEmptyData ? $B.config.returnEmptyData : "the return is empty!";
        this.$emptyTr = _appendEmptyTr(this, msg);
    }
    bindSortEvents(td) {
        var _this = this;
        if (!this.sortEvents) {
            this.sortEvents = {
                click: function (e) {
                    if (e.target.nodeName === "I") {
                        let td = e.target.parentNode.parentNode;
                        let col = parseInt($B.DomUtils.attribute(td, "col"));
                        let filed = _this.opts.sortFieldPrex + _this.colOptArray[col].field;
                        let params = {};
                        if ($B.DomUtils.hasClass(e.target, "fa-up-dir")) {
                            params[filed] = "asc";
                        } else {
                            params[filed] = "desc";
                        }
                        _this._load(params);
                    }
                }
            };
        }
        $B.DomUtils.bind(td, this.sortEvents);
    }
    _bindHeadChkEvent(td) {
        if (!this.chkEvents) {
            var _this = this;
            this.chkEvents = {
                click: function (e) {                    
                    var $i = e.target;
                    if ($i.tagName !== "I") {
                        $i = e.target.firstChild;
                    }
                    if ($B.DomUtils.hasClass($i, "fa-check")) {
                        $B.DomUtils.removeClass($i, "fa-check  fa-minus-squared");
                        $B.DomUtils.addClass($i, "fa-check-empty");
                        _this.checkAllData(false);
                    } else {
                        $B.DomUtils.removeClass($i, "fa-check-empty  fa-minus-squared");
                        $B.DomUtils.addClass($i, "fa-check");
                        _this.checkAllData(true);
                    }
                    return false;
                }
            };
        }
        $B.DomUtils.bind(td.firstChild, this.chkEvents);
    }
    bindTableEvents() {
        var _this = this;
        $B.DomUtils.bind(this.$bodyTable, {
            click: function (e) {
                if (_this.splitedLeftTd || _this.dragging) {
                    return false;
                }
                clearTimeout(_this.clickTimer);
                _this.clickTimer = setTimeout(() => {
                    let pNode = _getTd(e.target);
                    if ($B.DomUtils.attribute(pNode, "ischk")) {
                        _this._fireCheck(pNode);
                    } else if (!$B.DomUtils.attribute(pNode, "isopr")) {
                        let row = parseInt($B.DomUtils.attribute(pNode, "row"));
                        let col = parseInt($B.DomUtils.attribute(pNode, "col"));
                        let data = _this.formatDataArr[row];
                        if (_this.opts.onClickCell) {
                            _this.opts.onClickCell.call(pNode, _this.colOptArray[col].field, data);
                        }
                    }
                }, 300);
            },
            dblclick: function (e) {
                if (_this.splitedLeftTd || _this.dragging) {
                    return false;
                }
                clearTimeout(_this.clickTimer);
                let pNode = _getTd(e.target);
                let row = parseInt($B.DomUtils.attribute(pNode, "row"));
                let data = _this.formatDataArr[row];
                if (_this.opts.onDbClickRow) {
                    _this.opts.onDbClickRow.call(pNode, data);
                }
            }
        });
        if (this.opts.pgposition === "top" || this.opts.pgposition === "none" || this.opts.isTree) {
            $B.DomUtils.bind(_this.$hsrollEl, {
                mouseenter: function () {
                    $B.DomUtils.css(_this.$hsrollEl, { "overflow-x": "auto" });
                },
                mouseleave: function () {
                    $B.DomUtils.css(_this.$hsrollEl, { "overflow-x": "hidden" });
                }
            });
        } else {
            $B.DomUtils.css(_this.$hsrollEl, { "overflow-x": "auto" });
        }
    }
    checkAllData(ischk) {
        if (this.formatDataArr.length === 0) {
            return;
        }
        var childes = this.$bodyTable.childNodes;
        for (let i = 0; i < childes.length; i++) {
            if ($B.DomUtils.hasClass(childes[i], "_ext_tr")) {
                continue;
            }
            let $i = childes[i].firstChild.firstChild.firstChild;
            if($B.DomUtils.hasClass($i.parentNode,"k_table_chk_disabled")){
                continue;
            }
            if (ischk) {
                this.formatDataArr[i]._checked = true;
                $B.DomUtils.removeClass($i, "fa-check-empty fa-minus-squared");
                $B.DomUtils.addClass($i, "fa-check");
            } else {
                this.formatDataArr[i]._checked = false;
                $B.DomUtils.removeClass($i, "fa-check fa-minus-squared");
                $B.DomUtils.addClass($i, "fa-check-empty");
            }
        }
        if (this.opts.onCheck) {
            setTimeout(() => {
                this.opts.onCheck(this.formatDataArr);
            }, 1);
        }
    }
    _loopParentNodeUI(currentTr) {
        //向前，向后获取同级元素，同时获取父节点
        let deep = parseInt($B.DomUtils.attribute(currentTr, "deep"));
        let allSiblings = [];
        let parentNode;
        let prevEl = currentTr.previousSibling;
        while (prevEl) {
            if ($B.DomUtils.hasClass(prevEl, "_ext_tr")) {
                prevEl = prevEl.previousSibling;
                continue;
            }
            let deep1 = parseInt($B.DomUtils.attribute(prevEl, "deep"));
            if (deep1 < deep) {
                parentNode = prevEl;
                break;
            }
            if (deep1 === deep) { //同级元素
                allSiblings.push(prevEl);
            }
            prevEl = prevEl.previousSibling;
        }
        if (!parentNode) {
            return;
        }
        let nextEl = currentTr.nextSibling;
        while (nextEl) {
            if ($B.DomUtils.hasClass(nextEl, "_ext_tr")) {
                nextEl = nextEl.previousSibling;
                continue;
            }
            let deep1 = parseInt($B.DomUtils.attribute(nextEl, "deep"));
            if (deep1 < deep) {
                break;
            }
            if (deep1 === deep) { //同级元素
                allSiblings.push(nextEl);
            }
            nextEl = nextEl.nextSibling;
        }
        let all = allSiblings.length + 1;
        let checkedCount = 0;
        let row = parseInt($B.DomUtils.attribute(currentTr.firstChild, "row"));
        if (this.formatDataArr[row]._checked) {
            checkedCount = 1;
        }
        for (let i = 0; i < allSiblings.length; i++) {
            let row = parseInt($B.DomUtils.attribute(allSiblings[i].firstChild, "row"));
            if (this.formatDataArr[row]._checked) {
                checkedCount++;
            }
        }
        let $icon = parentNode.firstChild.firstChild.firstChild;
        row = parseInt($B.DomUtils.attribute(parentNode.firstChild, "row"));
        let parentBean = this.formatDataArr[row];
        parentBean._checked = false;
        if (checkedCount === all) { //全选
            $B.DomUtils.removeClass($icon, "fa-check-empty fa-minus-squared");
            $B.DomUtils.addClass($icon, "fa-check");
            parentBean._checked = true;
        } else if (checkedCount === 0) {//全不选
            $B.DomUtils.removeClass($icon, "fa-check fa-minus-squared");
            $B.DomUtils.addClass($icon, "fa-check-empty");
        } else {//部分选
            $B.DomUtils.removeClass($icon, "fa-check fa-check-empty");
            $B.DomUtils.addClass($icon, "fa-minus-squared");
        }
        this._loopParentNodeUI(parentNode);
    }
    _fireCheck(td) {
        let $i = td.firstChild.firstChild;
        if(!$B.DomUtils.hasClass($i.parentNode,"k_table_chk_disabled")){
            let row = parseInt($B.DomUtils.attribute(td, "row"));
            let bean = this.formatDataArr[row];
            if ($B.DomUtils.hasClass($i, "fa-check-empty")) {
                $B.DomUtils.removeClass($i, "fa-check-empty fa-minus-squared");
                $B.DomUtils.addClass($i, "fa-check");
                bean._checked = true;
            } else {
                $B.DomUtils.removeClass($i, "fa-check fa-minus-squared");
                $B.DomUtils.addClass($i, "fa-check-empty");
                bean._checked = false;
            }
            if (this.opts.isTree) {//如果是树形
                let tr = td.parentNode;
                let deep = parseInt($B.DomUtils.attribute(tr, "deep"));
                if (bean.children) { //如果是父节点,向下复选
                    let childTr = tr.nextSibling;
                    while (childTr) {
                        if ($B.DomUtils.hasClass(childTr, "_ext_tr")) {
                            childTr = childTr.nextSibling;
                            continue;
                        }
                        let childDeep = parseInt($B.DomUtils.attribute(childTr, "deep"));
                        if (childDeep <= deep) {
                            break;
                        }
                        let rowIdx = parseInt($B.DomUtils.attribute(childTr.firstChild, "row"));
                        this.formatDataArr[rowIdx]._checked = bean._checked;
                        //修饰子元素复选
                        let $icon = childTr.firstChild.firstChild.firstChild;
                        if (bean._checked) {
                            $B.DomUtils.removeClass($icon, "fa-check-empty fa-minus-squared");
                            $B.DomUtils.addClass($icon, "fa-check");
                        } else {
                            $B.DomUtils.removeClass($icon, "fa-check fa-minus-squared");
                            $B.DomUtils.addClass($icon, "fa-check-empty");
                        }
                        childTr = childTr.nextSibling;
                    }
                }
                //向上修饰父节点复选情况
                this._loopParentNodeUI(tr);
            }
            setTimeout(() => {
                this.renderChkUI();
                if (this.opts.onCheck) {
                    setTimeout(() => {
                        this.opts.onCheck(bean);
                    }, 1);
                }
            }, 1);
        }

    }
    renderChkUI() {
        let len = this.formatDataArr.length;
        let chkCount = 0;
        for (let i = 0; i < len; i++) {
            if (this.formatDataArr[i]._checked) {
                chkCount++;
            }
        }
        if (chkCount === 0) { //全不选
            $B.DomUtils.removeClass(this.$chkTdBtn.firstChild, "fa-check fa-minus-squared");
            $B.DomUtils.addClass(this.$chkTdBtn.firstChild, "fa-check-empty");
        } else if (chkCount === len) {//全选
            $B.DomUtils.removeClass(this.$chkTdBtn.firstChild, "fa-check-empty fa-minus-squared");
            $B.DomUtils.addClass(this.$chkTdBtn.firstChild, "fa-check");
        } else {//部分选
            $B.DomUtils.removeClass(this.$chkTdBtn.firstChild, "fa-check fa-check-empty");
            $B.DomUtils.addClass(this.$chkTdBtn.firstChild, "fa-minus-squared");
        }
    }
    _closeToolTip(closeIns) {
        if (closeIns && closeIns.close) {
            closeIns.close();
            return;
        }
        if (this.toolTipIns && this.toolTipIns.close) {
            this.toolTipIns.close();
            this.toolTipIns = undefined;
        }
    }
    _bindTdEvents(td) {
        if (!this.tdEvents) {
            var _this = this;
            var maxHeight = this.opts.lineClamp * lineHeight - 8;
            var tipMonseEvents = {
                mouseenter: function (e) {
                    if (_this.hideToolTiptimter) {
                        clearTimeout(_this.hideToolTiptimter.timer);
                        _this.hideToolTiptimter = undefined;
                    }
                    if (_this.hideToolTiptimter1) {
                        clearTimeout(_this.hideToolTiptimter1.timer);
                        _this.toolTipIns = _this.hideToolTiptimter1.tipIns;
                        _this.hideToolTiptimter1 = undefined;
                    }
                },
                mouseleave: function (e) {
                    e.target.instance.close();
                }
            };
            this.tdEvents = {
                mousedown: function (e) {
                    if (_this.splitedLeftTd) {
                        let td = _this.splitedLeftTd;
                        let col = td._col;
                        let colspan = $B.DomUtils.attribute(td, "colspan");
                        if (colspan) {
                            col = col + parseInt(colspan) - 1;
                        }
                        _this.dragingLeftcol = col;
                        _this.dragingRightcol = _this.splitedRightTd._col;
                        let left = 0;
                        for (let i = 0; i < _this.colWidthArray.length; i++) {
                            left = _this.colWidthArray[i] + left + 1;
                            if (i === col) {
                                break;
                            }
                        }
                        if (!_this.$splitLine) {
                            let splitLine = $B.DomUtils.createEl("<div style='cursor:w-resize;position:absolute;width:1px;height:100%;top:0;left:" + left + "px;margin:0;padding:0px;background:#0DF000'></div>");
                            $B.DomUtils.append(_this.$hsrollEl, splitLine);
                            _this.$splitLine = splitLine;
                            $B.draggable(splitLine, {
                                nameSpace: 'tablesplit',
                                cursor: 'w-resize',
                                axis: 'h', // v or h  水平还是垂直方向拖动 ，默认全向
                                onDragReady: function (args) {
                                    return true;
                                },
                                onStartDrag: function (args) {
                                    _this.dragging = true;
                                },
                                onDrag: function (args) {
                                },
                                onStopDrag: function (args) {
                                    splitLine.style.display = "none";
                                    setTimeout(() => {
                                        _this.dragging = false;
                                    }, 360);
                                    let leftOfs = args.state._data.leftOffset;
                                    _this.onDragColWidth(leftOfs, _this.dragingLeftcol, _this.dragingRightcol);
                                    return false;
                                },
                                onMouseUp: function (args) {
                                    splitLine.style.display = "none";
                                    setTimeout(() => {
                                        _this.dragging = false;
                                    }, 360);
                                }
                            });
                        } else {
                            _this.$splitLine.style.left = left + "px";
                            _this.$splitLine.style.display = "block";
                        }
                        $B.DomUtils.trigger(_this.$splitLine, "tablesplit.mousedown", e);
                        return false;
                    }
                },
                mouseenter: function (e) {
                    let td = e.target;
                    while (td) {
                        if (td.tagName === "TD") {
                            break;
                        }
                        td = td.parentNode;
                    }
                    let offset = $B.DomUtils.offset(td);
                    let colIdx = $B.Dom.attr(td,"col"); 
                    let headTd = _this.colTdMap[colIdx];
                    let width = _this.colWidthArray[td._col];
                    let start = offset.left;
                    let end = offset.left + width;
                    td._compareArgs = { s1: start - 3, s2: start + 3, e1: end - 3, e2: end + 3 };
                    if (td._isHeader) {
                        return;
                    }
                    if (_this.hideToolTiptimter) {
                        clearTimeout(_this.hideToolTiptimter.timer);
                        _this.hideToolTiptimter = undefined;
                    }
                    if (_this.toolTipIns) {
                        let tipIns = _this.toolTipIns;
                        _this.toolTipIns = undefined;
                        let timer = setTimeout(() => {
                            _this._closeToolTip(tipIns);
                        }, 600);
                        _this.hideToolTiptimter1 = {
                            timer: timer,
                            td: e.target,
                            tipIns: tipIns
                        };
                    }
                    let wrap = e.target.firstChild;
                    let tipAttr = $B.Dom.attr(headTd,"tip");
                    let isTip = tipAttr !== undefined;
                    if(!isTip){
                        let h = $B.DomUtils.innerHeight(wrap);
                        isTip = h > maxHeight;
                    }                    
                    if (isTip) {  
                        let tipColor,tipText = wrap.firstChild.innerText;
                        if(_this.opts.formatTipFn){                            
                            let rowIdx = parseInt( $B.Dom.attr(td,"row"));
                            let colName = $B.Dom.attr(headTd,"field"); 
                            let rowData = _this.formatDataArr[rowIdx];
                            let ret = _this.opts.formatTipFn(colName,rowData,colIdx,rowIdx);
                            if(ret){
                                if(typeof ret === "string"){
                                    if(ret.indexOf("#") === 0){
                                        tipColor = ret;
                                    }else{
                                        tipText = ret;
                                    }
                                }else{
                                    tipColor = ret.color;
                                    tipText = ret.text;
                                }
                            }
                        }                                     
                        _this.toolTipIns = $B.toolTip(wrap, tipText,undefined,tipColor);
                        $B.DomUtils.bind(_this.toolTipIns.elObj, tipMonseEvents);
                        if (_this.hideToolTiptimter1) {
                            _this.hideToolTiptimter1.tipIns = _this.toolTipIns;
                        }
                    }
                    return false;
                },
                mouseleave: function (e) {
                    e.target._compareArgs = undefined;
                    if (e.target._isHeader) {
                        return;
                    }
                    let timer = setTimeout(() => {
                        _this._closeToolTip();
                    }, 600);
                    _this.hideToolTiptimter = {
                        timer: timer,
                        td: e.target
                    };
                    _this._changeTdMouse(e.target, "default");
                    return false;
                },
                mousemove: function (e) {
                    let td = e.target;
                    while (td) {
                        if (td.tagName === "TD") {
                            break;
                        }
                        td = td.parentNode;
                    }
                    var pageX = e.pageX;
                    var args = td._compareArgs;
                    let leftTd, rightTd;
                    if (args.s1 <= pageX && pageX <= args.s2) {
                        leftTd = td.previousSibling;
                        rightTd = td;
                    } else if (args.e1 <= pageX && pageX <= args.e2) {
                        rightTd = td.nextSibling;
                        leftTd = td;
                    }
                    _this.splitedLeftTd = undefined;
                    _this.splitedRightTd = undefined;
                    if (leftTd && rightTd && !leftTd._ischk) {
                        _this._changeTdMouse(td, "w-resize");
                        _this._onSplited = true;
                        _this.splitedLeftTd = leftTd;
                        _this.splitedRightTd = rightTd;
                    } else {
                        _this._changeTdMouse(td, "default");
                    }
                }
            }
        }
        $B.DomUtils.bind(td, this.tdEvents);
    }
    onDragColWidth(shift, leftTd, rightTd) {
        let leftOpt = this.colOptArray[leftTd];
        let rightOpt = this.colOptArray[rightTd];
        let leftWidth = this.colWidthArray[leftTd];
        let rightWidth = this.colWidthArray[rightTd];
        let newLeftWidth, newRightWidth;
        if (shift < 0) { //向左边调整
            newLeftWidth = leftWidth + shift;
            if (newLeftWidth <= leftOpt.minWidth) { //限制最小宽度
                let diff = leftOpt.minWidth - newLeftWidth;
                newLeftWidth = leftOpt.minWidth;
                shift = shift + diff;
            }
            let wrapWidth = $B.DomUtils.width(this.$hsrollEl);
            let bodyWidth = $B.DomUtils.width(this.$bodyWrap.parentNode.parentNode);
            let scrollWidth = bodyWidth - wrapWidth;
            if (scrollWidth < Math.abs(shift)) {
                newRightWidth = rightWidth - shift;
            } else {
                newRightWidth = rightWidth;
            }
        } else {//向右边调整
            newRightWidth = rightWidth - shift;
            if (newRightWidth < rightOpt.minWidth) {
                newRightWidth = rightOpt.minWidth;
            }
            newLeftWidth = leftWidth + shift;
        }
        this.colWidthArray[leftTd] = newLeftWidth;
        this.colWidthArray[rightTd] = newRightWidth;
        this._updateHeaderWidth(this.colWidthArray);
        this.asyncWidth();
    }
    _changeTdMouse(el, cursor) {
        el.style.cursor = cursor;
        if (el.children) {
            for (let i = 0; i < el.children; i++) {
                this._changeTdMouse(el.children[i], cursor);
            }
        }
    }
    _updateSrollIcon() {
        if (this.$scrolyIcon) {
            let scolLeft = $B.DomUtils.scrollLeft(this.$hsrollEl);
            let scrollWidth = this.$hsrollEl.scrollWidth;
            let width = this.$hsrollEl.clientWidth;
            let right = scrollWidth - width;
            if (right < 0) {
                right = 0;
            } else {
                right = right - scolLeft;
            }
            this.$scrolyIcon.style.right = right + "px";
        }
    }
    createPagination() {
        if (this.opts.pgposition !== "none") {
            _createPagination(this);
        }
    }
    reload(params) {
        this.lastQueryParams = undefined;
        this._load(params);
    }
    refresh(){
        this._load(this.lastQueryParams);
    }
    updateTitle(title){
        let $span = $B.DomUtils.children(this.$H6,".k_table_h_txt")[0];
        $span.innerHTML = title;
    }
    _load(params, callBack) {
        var url = this.opts.url;
        let method = (url.indexOf(".data") > 0 || url.indexOf(".json") > 0) ? "GET" : "POST";
        var _this = this;
        if (this.opts.showLoading && !this.loading && !callBack) {
            this.loading = $B.DomUtils.createEl("<div class='k_box_size' style='position:absolute;top:0;left:0;overflow:hidden;height:100%;width:100%;'><div class='k_model_mask k_box_size' style='position:absolute;width:100%;height:100%;top:0;left:0;'></div></div>");
            let loadel = $B.getLoadingEl(undefined, "#F5F2FF"); 
            //当前是空表格插入一个空行占位
            if (this.$bodyTable.childNodes.length === 0) {
                this.extTr = _appendEmptyTr(this, "");
            }
            $B.DomUtils.append(this.loading, loadel);
            $B.DomUtils.append(this.$bodyWrap, this.loading);
        }
        var datas = { page: this.page, pageSize: this.opts.pageSize };
        if (params) {
            // datas.page = 1; //有参数请求恢复到第一页
            // this.page = 1;
            datas = $B.extendObjectFn(datas, params);
        }
        if (this.opts.setParams) {
            let p = this.opts.setParams(datas);
            if (p) {
                datas = $B.extendObjectFn(datas, p);
            }
        }
        if (this.opts.sortField) {
            let keys = Object.keys(this.opts.sortField);
            for (let i = 0; i < keys.length; i++) {
                let skey = this.opts.sortFieldPrex + keys[i];
                if (!datas[skey]) {
                    datas[skey] = this.opts.sortField[keys[i]];
                }
            }
        }
        let extTrKeys = Object.keys(this.exttrMap);
        for (let i = 0; i < extTrKeys.length; i++) {
            $B.DomUtils.offEvents(this.exttrMap[extTrKeys[i]]);
        }
        this.exttrMap = {};
        this.lastQueryParams = datas;       
        let reqOpt = {
            dataType: 'json',
            url: url,
            data: datas,
            type: method,
            onErrorEval: true,
            onReturn: function () {
                if (callBack) {
                    callBack(0, undefined);
                    return;
                }
                if(_this.loading){
                    let $l = _this.loading;
                    _this.loading = undefined;
                    try {
                        $B.removeLoading($l, () => {
                        });
                        if (_this.extTr) {
                            _this.extTr.parentNode.removeChild(_this.extTr);
                            _this.extTr = undefined;
                        }
                    } catch (ex) {
                        console.log(ex);
                    }
                }                
            },
            ok: function (message, data) {
                if(_this.$chkTdBtn){
                    $B.DomUtils.removeClass(_this.$chkTdBtn.firstChild, "fa-check fa-minus-squared");
                    $B.DomUtils.addClass(_this.$chkTdBtn.firstChild, "fa-check-empty");
                }                
                if (!data.resultList && !_this.opts.isTree) {
                    $B.error("the return data format must be：{resultList:[{}],totalSize:xxxx}");
                    return;
                }
                if (callBack) {
                    callBack(1, data);
                    _this._triggerExentdTrs();
                    return;
                }
                _this.opts.data = data;
                if (_this.opts.isTree) { //树形                   
                    _cretteTreeTableBody(_this);
                } else {
                    _cretteTableBody(_this);
                }
                _this._triggerExentdTrs();
            },
            final: function (res, status, xhr) {
                if (_this.opts.onLoaded) {
                    setTimeout(() => {
                        _this.opts.onLoaded(res, status);
                    }, 1);
                }
            }
        };
        if(!this.showLoading){
            reqOpt.error= function (xhr, status, errorThrown){
                console.log("error:",xhr,status);
            };
            reqOpt.fail = function (msg, res){
                console.log("fail:",msg, res);
            };
        }
        $B.request(reqOpt);
    }
    fit2height() {
        if (this.opts.fit2height) {
            let maxHeight = $B.DomUtils.innerHeight(this.elObj.parentNode);            
            let childs = this.elObj.childNodes;
            let h = 2, tmp;
            for (let i = 0; i < childs.length; i++) {
                tmp = $B.DomUtils.outerHeight(childs[i]);
                h = h + tmp;
            }
            let aviHeight = maxHeight - h;
            let $wrap = this.$bodyWrap.parentNode.parentNode;          
            $B.DomUtils.height($wrap, aviHeight);
        }
    }
    /***
     * 根据colWidthArray设置表格各列宽度
     * ***/
    asyncWidth() {
        if (this.formatDataArr.length === 0) {
            if (this.$emptyTr) {
                let w = $B.DomUtils.width(this.$bodyTable.parentNode);
                $B.DomUtils.width(this.$emptyTr.firstChild.firstChild, { width: w });
            }
            return;
        }
        var tr = this.$bodyTable.firstChild;
        while(tr){
            var childNodes = tr.childNodes;
            let isHline = this.opts.splitColLine === "k_table_td_none_line" || this.opts.splitColLine === "k_table_td_h_line";
            for (let i = 0; i < childNodes.length; i++) {
                let tdWidth = this.colWidthArray[i];
                if (isHline) {
                    tdWidth++;
                }
                $B.DomUtils.width(childNodes[i].firstChild, tdWidth);
            }
            tr = tr.nextSibling;
        }
    }
    getRowCount(){   
        return this.$bodyTable.children.length;
    }
    getCheckedData(forId) {
        var datas = [];
        for (let i = 0; i < this.formatDataArr.length; i++) {
            if (this.formatDataArr[i]._checked) {
                if (forId) {
                    if (this.opts.isTree) {
                        if (typeof this.formatDataArr[i].id !== "undefined") {
                            datas.push(this.formatDataArr[i].id);
                        } else {
                            datas.push(this.formatDataArr[i].data[this.opts.idField]);
                        }
                    } else {
                        datas.push(this.formatDataArr[i][this.opts.idField]);
                    }
                } else {
                    datas.push(this.formatDataArr[i]);
                }
            }
        }
        return datas;
    }
    openInner(tr){
        this._initExtTrEvent();
        this.extentTrEvent({target:tr});
    }
    /***
     * 扩展行展开功能
     * **/
    extendTr(tr) {
        let td = tr.firstChild;
        if (this.opts.checkBox) {
            td = td.nextSibling;
        }
        if (!$B.DomUtils.attribute(td, "exttr")) {
            $B.DomUtils.attribute(td, { "exttr": 1 });
            let wrap = td.firstChild.firstChild;
            let iconNode = $B.DomUtils.createEl("<span style='padding-left:3px;cursor:pointer;' ><i class='fa fa-right-open-1 ext_tr_closed'></i></span>");
            if (this.opts.isTree) {
                $B.DomUtils.css(iconNode, { "padding-right": "8px" });
                $B.DomUtils.after(wrap.lastElementChild, iconNode);
            } else {
                let html = "<span style='padding-left:6px;'>" + wrap.innerHTML + "</span>";
                let newNode = $B.DomUtils.createEl(html);
                wrap.innerHTML = "";
                $B.DomUtils.append(wrap, iconNode);
                $B.DomUtils.append(wrap, newNode);
            }
            this._initExtTrEvent();
            $B.DomUtils.click(iconNode, this.extentTrEvent);
            if (this.opts.openExtendTr) {//如果是需要打开展开行的
                if (!this.triggerExtendTrs) {
                    this.triggerExtendTrs = [];
                }
                this.triggerExtendTrs.push(iconNode);
            }
        }
    }
    _initExtTrEvent(){
        var _this = this;
        if (!this.extentTrEvent) {
            this.extentTrEvent = function (e) {
                let el = e.target;
                let td,data,rowNum,extId,$extTr,  isShow = true;;
                if(el.tagName === "TR"){
                    td = el.firstChild;
                    rowNum = td._row;
                    data = _this.formatDataArr[rowNum];
                    extId = "ext_tr" + rowNum;                   
                    let trId = "";
                    let nextTr = el.nextSibling;
                    if(nextTr){
                        trId = nextTr.id;
                    }                    
                    if(extId === trId){
                        $extTr = nextTr;
                        isShow = false;
                        $B.DomUtils.remove($extTr);
                    }else{
                        $extTr = $B.DomUtils.createEl("<tr class='_ext_tr' id='" + extId + "'><td style='min-height:32px;padding:3px 4px;' colspan='" + _this.colOptArray.length + "'></td></tr>");
                        $B.DomUtils.after(el, $extTr);
                    }
                }else{
                    if (el.tagName === "SPAN") {
                        el = el.firstChild;
                    }
                    td = el.parentNode;
                    while (td) {
                        if (td.tagName === "TD") {
                            break;
                        }
                        td = td.parentNode;
                    }
                    rowNum = td._row;
                    data = _this.formatDataArr[rowNum];
                    extId = "ext_tr" + rowNum;
                    if (!_this.exttrMap[extId]) {
                        _this.exttrMap[extId] = $B.DomUtils.createEl("<tr class='_ext_tr' id='" + extId + "'><td style='min-height:32px;padding:3px 4px;' colspan='" + _this.colOptArray.length + "'></td></tr>");
                    }
                    $extTr =  _this.exttrMap[extId];                   
                    if ($B.DomUtils.hasClass(el, "ext_tr_closed")) { //执行展开
                        $B.DomUtils.removeClass(el, "ext_tr_closed");
                        $B.animate(el, { "rotateZ": "90deg" }, { duration: 200 });
                        $B.DomUtils.after(td.parentNode, _this.exttrMap[extId]);
                    } else {//执行收起
                        $B.DomUtils.addClass(el, "ext_tr_closed");
                        $B.animate(el, { "rotateZ": "0deg" }, { duration: 200 });
                        $B.DomUtils.detach(_this.exttrMap[extId]);
                        isShow = false;
                    }
                }
                if (_this.opts.onTrExtended) {
                    setTimeout(() => {
                        _this.opts.onTrExtended(data, $extTr, isShow);
                    }, 1);
                }
                return false;
            };
        }
    }
    /***
     * args = {colindx:width} 4008205555  
     * ***/
    autoHeaderWidth(args) {
        let newWidth = [];
        let colTdArray = $B.DomUtils.children(this.$hideTr);
        let td, tdWidth;
        let colCount = colTdArray.length;
        for (let i = 0; i < colCount; i++) {
            td = colTdArray[i];
            tdWidth = $B.DomUtils.width(td.firstChild);
            if (args && args[i]) {
                newWidth.push(args[i]);
            } else {
                newWidth.push(tdWidth);
            }
        }
        //总宽度小于外层宽度
        let wrapWidth = $B.DomUtils.width(this.$headWrap);
        let diff = wrapWidth - $B.DomUtils.width(this.$headTable) - 1;
        if (diff > 0) {
            let avg = Math.floor(diff / this.autoCols.length);
            if (avg > 1) { //可均分到每列最少1px
                let leftVal = diff % this.autoCols.length;
                let j = 0;
                while (j < colCount) {
                    if (this.autoCols.includes(j)) {
                        newWidth[j] = newWidth[j] + avg;
                    }
                    j++;
                }
                newWidth[colCount - 1] = newWidth[colCount - 1] + leftVal;
            } else { //只将余量放在最后一列即可
                newWidth[colCount - 1] = newWidth[colCount - 1] + diff;
            }
        } else {

        }
        this._updateHeaderWidth(newWidth);
    }
    _updateHeaderWidth(newWidth) {
        let clonetr = this.$hideTr.cloneNode(true);
        var childNodes = clonetr.childNodes;
        for (let i = 0; i < childNodes.length; i++) {
            $B.DomUtils.width(childNodes[i].firstChild, newWidth[i]);
        }
        $B.DomUtils.before(this.$hideTr, clonetr);
        $B.DomUtils.remove(this.$hideTr);
        this.$hideTr = clonetr;
        this.colWidthArray = newWidth;
        let bodyWidth = $B.DomUtils.outerWidth(this.$headTable);
        let $wrap = this.$bodyWrap.parentNode.parentNode;
        $B.DomUtils.width($wrap, bodyWidth);
        //console.log("bodyWidth " + bodyWidth);
    }
    destroyTools() {
        if (this.toolbarIns) {
            this.toolbarIns.destroy();
            this.toolbarIns = undefined;
        }
        if (this.topPgIns) {
            this.topPgIns.destroy();
            this.topPgIns = undefined;
        }
    }
    destroyBottomTools() {
        if (this.bottomPgIns) {
            this.bottomPgIns.destroy();
            this.bottomPgIns = undefined;
        }
    }
    destroyRowTools() {
        for (let i = 0; i < this.rowTools.length; i++) {
            this.rowTools[i].destroy();
        }
        this.rowTools = [];
    }
    destroy(isForce) {
        this.destroyRowTools();
        this.destroyTools();
        this.destroyBottomTools();
        $B.DomUtils.offEvents(window, this.resizeEvent);
        super.destroy();
    }
}
$B["Table"] = Table; 
 if(!window["$B"] ) {window["$B"] = $B;} 
  return window["$B"] ; 
});

/*! BUI - v2.0.0 - 2022-03-13 09:03:17 
Copyright (c): kevin.huang  https://gitee.com/kevin-huang/bui2.0.git (qq:757723114) 
Released under MIT License*/
(function ( global, factory) {
//   "use strict";
	if ( typeof module === "object" && typeof module.exports === "object" ) {
		module.exports = global.document ?
			factory( global, true ) :
			function( w ) {
				if ( !w.document ) {
					throw new Error( "Bui requires a window with a document" );
				}
				return factory( w );
			};
	} else {
		  factory( global );
	}  
})( typeof window !== "undefined" ? window : this, function( window, noGlobal ) {
//   "use strict";
  var $B = window["$B"] ?  window["$B"] : {};

var defaultOpts = {
    timeout: 180, //超时时间 秒
    url: undefined,
    successColor:"#05DE5F",
    backgroundColor:'#505DF7',
    fileParamName: 'attachments',
    dislabel:false,//是否显示文本提示
    onlyOne:false, //只能选择上传一个文件
    dragUpload:false,
    multiple: true, //是否可批量选择
    immediate: true, //选择文件后是否立即自动上传，即不用用户点击提交按钮就上传
    accept: undefined,   // 可上传的文件类型 .xml,.xls,.xlsx,.png
    successAlert:true, //开启成功提示
    onDelected: undefined, //删除回调
    success: undefined,  //成功时候的回调  
    setParams: undefined,//设置参数 return {}
    error: undefined //错误回调
};
class Upload extends $B.BaseControl {
    constructor(elObj, opts) {
        super();
        super.setElObj(elObj);
        this.opts = $B.extendObjectFn(true, {}, defaultOpts, opts);
        $B.DomUtils.addClass(this.elObj, "k_mutilupload_wrap");
        this.cfg = $B.config.upload;
        this.label = this.opts.label ? this.opts.label : this.cfg.label;
        this.formData = new FormData();
        this.makeUi();
    }
    makeUi() {
        $B.DomUtils.css(this.elObj, {
            display: "inline-block"
        });
        this.$list = $B.DomUtils.createEl("<div></div>");
        var $btn = $B.DomUtils.createEl("<button style='background:"+this.opts.backgroundColor+"'><i class='fa fa-upload-cloud' style='padding-right:5px;color:#fff;'></i><span style='color:#fff;'>" + this.label + "</span></button>");
        if(this.opts.dislabel){
            $btn.lastChild.style.display = "none";
            $btn.firstChild.style.paddingRight = "0px";
            this.$list.style.display = "none";
        }
        if(this.opts.style){
            $B.Dom.css($btn,this.opts.style);
            if(this.opts.style["font-size"]){
                $btn.firstChild.style.fontSize = this.opts.style["font-size"];
            }
        }
        this.$uploadBtn = $btn;
        $B.DomUtils.append(this.elObj, $btn);
      
        $B.DomUtils.append(this.elObj, this.$list);
        this.$fileInput = $B.DomUtils.append(this.elObj, "<input style='display:none;' type='file' name='k_upload_file'/>");
        if (this.opts.accept) {
            $B.DomUtils.attribute(this.$fileInput, { accept: this.opts.accept });
        }
        if (this.opts.multiple) {
            $B.DomUtils.attribute(this.$fileInput, { multiple: this.opts.multiple });
        }
        $B.DomUtils.click($btn, (e) => {
            this.openBrowse();
        });
        this.accept = undefined;
        if(this.opts.accept){
            let tmpArr = this.opts.accept.toLowerCase().split(",");
            this.accept = {};
            for(let i =0 ; i< tmpArr.length ;i++){
                this.accept[tmpArr[i]] =1;
            }
        }
        $B.DomUtils.change(this.$fileInput, (e) => {  
            if(this.opts.immediate){
                if(this.$list.children && this.$list.children.length > 0){
                    this.formData.delete(this.opts.fileParamName);
                    $B.DomUtils.remove(Array.from( this.$list.children));
                }               
            }         
            let el = e.target;
            this._saveFiles(el);  
            this.$fileInput.value = "";          
        });
        if(this.opts.dragUpload){
            $B.DomUtils.addListener(this.$uploadBtn,"dragenter",(e)=>{
                return false;
            });
            $B.DomUtils.addListener(this.$uploadBtn,"dragover",(e)=>{
                return false;
            });
            $B.DomUtils.addListener(document.body,"drop",(e)=>{
                return false;
            });
            $B.DomUtils.addListener(this.$uploadBtn,"dragleave",(e)=>{
                return false;
            });
            $B.DomUtils.addListener(this.$uploadBtn,"drop",(e)=>{
                if(this.isUploading){
                    return false;
                }   
                if(this.opts.immediate){
                    if(this.$list.children && this.$list.children.length > 0){
                        this.formData.delete(this.opts.fileParamName);
                        $B.DomUtils.remove(Array.from( this.$list.children));
                    } 
                }
                var fileList = e._e.dataTransfer.files;
                if(fileList.length > 0 ){
                    this._saveFiles({files:fileList}); 
                }
                return false;
            });           
        }
        if(this.opts.onReader){
            this.opts.onReader(this.$uploadBtn);
        }
        //单个文件上传，检查右侧位置，如果宽度够，则采用一行显示
        this.isHorisionUi = false;
        if(!this.opts.multiple && this.opts.onlyOne){  
            setTimeout(()=>{
                let w = $B.DomUtils.innerWidth(this.elObj.parentNode);                 
                if(w > 299){ 
                    this.$list.style.display = "inline-block";
                    this.$list.style.paddingLeft = "14px";
                    this.isHorisionUi = true;
                }
            },1); 
        }
    }
    _saveFiles(el){       
        let files = [];
        for(let i =0 ; i < el.files.length ;i++){
            if(!this.accept || this.accept === ".*" || this.accept[".*"]){
                files.push(el.files[i]);
            }else{
                let _name = el.files[i].name;
                let idx = _name.lastIndexOf(".");
                if(idx > 0){
                    let subfix = _name.substring(idx).toLowerCase();
                    if(this.accept[subfix]){
                        files.push(el.files[i]);
                    }
                }                    
            }                
        }
        let fileName = [];
        for (let i = 0; i < files.length; i++) {                
            fileName.push(files[i].name);
        }
        let go = true, r;
        if (this.opts.onSelected) {
            if (files.length === 1) {
                r = this.opts.onSelected(fileName.join(""), files[0]);
            } else {
                r = this.opts.onSelected(fileName, files);
            }
        }
        if (typeof r !== "undefined") {
            go = r;
        }
        if (go) {
            for (let i = 0; i < files.length; i++) {
                this.formData.append(this.opts.fileParamName, files[i]);
                this.renderOneFile(files[i], i);
            } 
            if(this.opts.immediate){
                setTimeout(()=>{
                    this.upload();
                },500);                   
            }               
        } 
    }
    renderOneFile(file) {
        if(this.opts.onlyOne){
            this.clear();
        }
        let fileName = file.name;
        let paddTop = "padding-right:10px;padding-top:12px;";
        if(this.isHorisionUi){
            paddTop = "padding:0px;";
        }
        let $o = $B.DomUtils.append(this.$list, "<p style='"+paddTop+"'><span >" + fileName + "</span><i filename='" + fileName + "' title='" + this.cfg.delete + "' style='cursor:pointer;position:relative;left:3px;' class='fa fa-cancel-circled ifr_inner_delbtn'></i></p>");
        if (!this.deleteEv) {
            this.deleteEv = (e) => {
                let el = e.target;
                let filename = $B.DomUtils.attribute(el, "filename");
                let file;
                let files = this.formData.getAll(this.opts.fileParamName);
                let tmp = [];
                for (let i = 0; i < files.length; i++) {
                    if (filename !== files[i].name) {
                        tmp.push(files[i]);
                    }else{
                        file = files[i];
                    }
                }
                this.formData.delete(this.opts.fileParamName);                  
                for (let i = 0; i < tmp.length; i++) {
                    this.formData.append(this.opts.fileParamName, tmp[i]);
                }                   
                $B.DomUtils.remove(el.parentNode);            
                if(this.opts.onDelected){
                    this.opts.onDelected(filename,file);
                }                
                return false;
            };
        }
        $B.DomUtils.click($o.lastChild, this.deleteEv);
    }
    openBrowse() {
        let ie = navigator.appName == "Microsoft Internet Explorer" ? true : false;
        if (ie) {
            this.$fileInput.click();
        } else {
            let a = document.createEvent("MouseEvents");
            a.initEvent("click", true, true);
            this.$fileInput.dispatchEvent(a);
        }
    }
    upload() {
        if(this.isUploading){
            return;
        }        
        let files = this.formData.getAll(this.opts.fileParamName);
        clearTimeout(this._tempTimer);
        let $label = this.$uploadBtn.lastChild;
        if (!files || files.length === 0) {
            $label.innerText = this.cfg.emptyWarning;
            $B.DomUtils.addClass($label, "k_font_blink");
            this._tempTimer = setTimeout(() => {
                $label.innerText = this.label;
                $B.DomUtils.removeClass($label, "k_font_blink");
            }, 1100);
            return;
        }
        if (this.opts.setParams) {
            let prs = this.opts.setParams();
            if (prs) {
                for (let key in prs) {
                    this.formData.delete(key);
                    this.formData.append(key, prs[key]);
                }
            }
        }
        this.isUploading = true;
        let childs = this.$list.children;
        let color = $B.DomUtils.css(childs[0].firstChild,"color");        
        for(let i = 0; i < childs.length ;i++){
           $B.DomUtils.removeClass(childs[i].lastChild,"fa-cancel-circled fa-ok-1");
           $B.DomUtils.addClass(childs[i].lastChild,"fa-spin5 animate-spin");           
           $B.DomUtils.css(childs[i].lastChild,{color:color});
           $B.DomUtils.attribute(childs[i].lastChild,{title:this.cfg.uploading});
        }
        $label.innerText = this.cfg.uploading + "0%";
        $B.request({
            timeout:this.opts.timeout,
            url: this.opts.url,
            type: "POST",
            processData: false,
            contentType: false,
            data: this.formData,
            fail: (msg, res) => {
                this.isUploading = false;                               
                for(let i = 0; i < childs.length ;i++){
                    $B.DomUtils.addClass(childs[i].lastChild,"fa-cancel-circled");
                    $B.DomUtils.removeClass(childs[i].lastChild,"fa-spin5 animate-spin");
                    $B.DomUtils.attribute(childs[i].lastChild,{title:this.cfg.delete});
                 }                                 
                 let errMsg = msg !== null &&  msg !== '' ? msg :this.cfg.errMsg;
                 if($B.error && $B.Panel){
                    $B.error(errMsg);
                    $label.innerText = this.label ; 
                 }else{
                    $label.innerText = errMsg ; 
                    $B.DomUtils.addClass($label, "k_font_blink");
                    this._tempTimer = setTimeout(() => {
                        $label.innerText = this.label;
                        $B.DomUtils.removeClass($label, "k_font_blink");
                    }, 2500);
                 }
                 if(this.opts.error){
                    this.opts.error(msg, res);
                 } 
            },
            ok: (res, data) => {
                this.isUploading = false; 
                this.formData.delete(this.opts.fileParamName);
                for(let i = 0; i < childs.length ;i++){
                    $B.DomUtils.addClass(childs[i].lastChild,"fa-ok-1");
                    $B.DomUtils.removeClass(childs[i].lastChild,"fa-spin5 animate-spin");
                    $B.DomUtils.attribute(childs[i].lastChild,{title:this.cfg.delete + "("+this.cfg.success +")"});
                    $B.DomUtils.css(childs[i].lastChild,{color:this.opts.successColor});
                 }
                 let successMsg = res !== null &&  res !== '' ? res :this.cfg.success;
                 if(this.opts.successAlert){
                    if($B.success && $B.Panel){
                        $B.success(successMsg,1.5);
                        $label.innerText = this.label;
                    }else{
                        $label.innerText = successMsg ; 
                        $B.DomUtils.addClass($label, "k_font_blink");
                        this._tempTimer = setTimeout(() => {
                            $label.innerText = this.label;
                            $B.DomUtils.removeClass($label, "k_font_blink");
                        }, 2500);
                    }
                 }else{
                    $label.innerText = this.label ;
                 }
                 if(this.opts.success){
                    this.opts.success(res, data);
                 }
            },
            onProgress: (rate) => {    
                if(rate === 100){
                    rate = 99;
                }            
                $label.innerText = this.cfg.uploading + rate + "%";
            }
        });
    }
    hasFiles(){
        let files = this.formData.getAll(this.opts.fileParamName);
        return files.length > 0;
    }
    clear(neetTip){
        if(this.$list.children && this.$list.children.length > 0){
            this.formData.delete(this.opts.fileParamName);
            $B.DomUtils.remove(Array.from( this.$list.children));
        } 
        if(neetTip){
            if($B.message && $B.Panel){
                $B.message(this.cfg.clearLabel,1.2);
            }    
        }           
    }
}
$B["Upload"] = Upload;


/***
    * 下载封装
    * el : 提示下载的容器
    * args = {    *  
    *  url: 下载地址，
    *  message:'提示语',
    *  ivtTime:'检测间距，毫秒',
    *  params：{},附加的参数
    *  onSuccess:function(res){}
    * }
    * ***/
$B["Download"] = function(el,args){
    if(typeof el === "string"){
        el = document.getElementById(el);
    }else{
        $B.DomUtils.removeChilds(el);
    }
    var inteVal, ivtTime = args.ivtTime ? args.ivtTime :1500;
    var finish_down_key = $B.getUUID();
    var ifrId = "k_" + $B.getShortID();
    var ifr = $B.DomUtils.createEl('<iframe name="' + ifrId + '" id="' + ifrId + '" style="display: none"></iframe>');
    $B.DomUtils.append(el,ifr);
    var message = $B.config ? $B.config.exporting : "正在下载......";
    var _url = args.url;
    if (_url.indexOf("?") > 0) {
        _url = _url + "&isifr=1&_diff=" + $B.generateMixed(4);
    } else {
        _url = _url + "?isifr=1&_diff=" + $B.generateMixed(4);
    }
    var $msg = $B.DomUtils.createEl("<h3 id='k_file_export_xls_msg_'  style='height:20px;line-height:20px;text-align:center;padding-top:30px;'><div class='loading' style='width:100%;margin:0px auto;'><i class='fa-spin3 animate-spin'></i><span style='padding-left:12px;'>" + message + "</span></div></h3>");
    var $form = $B.DomUtils.createEl('<form action="' + _url + '" target="' + ifrId + '" method="post" ></form>');

    if ($B.isPlainObjectFn(args.params)) {
        Object.keys(args.params).forEach(function (p) {
            var v = args.params[p];
            $B.DomUtils.append($form,'<input type="hidden" id="' + p + '" name="' + p + '" value="' + encodeURIComponent(v) + '"/>');
        });
    }
    $B.DomUtils.append($form,'<input type="hidden" id="_down_key_" name="_down_key_" value="' + finish_down_key + '"/>');

    var callReturn = typeof args.onSuccess === 'function';
    $B.DomUtils.append(el,$msg);
    $B.DomUtils.append(el,$form);
    var onCompleteFn = ()=>{
        clearInterval(inteVal);
        try {            
            var _$body = window.frames[ifrId].document.body;
            var res = _$body.innerText;
            if (res && res !== '') {
                var json = eval("(" + res + ")");
                $msg.innerHTML = "<h2>" + json.message + "</h2>";
                if (json.code === 0) {
                    if (callReturn) {
                        args.onSuccess(json);
                    }
                }
                return;
            }
        } catch (e) {
            $msg.innerHTML =  "<h2>" +  $B.config ? $B.config.exportException : 'sorry，exception happen！'+ "</h2>";
            console.log(e);
            return;
        }
    };
    var regex = /(\/\w+)/g;
    var match, lastChar;
    do {
        match = regex.exec(args.url);
        if (match !== null) {
            lastChar = match[0];
        }
    } while (match !== null);
    var url = args.url.replace(lastChar, "/checkresponse");
    if (url.indexOf("?") > 0) {
        url = url + '&k_finish_down_key_=' + finish_down_key;
        url = url.replace("cmd=","_a=");
    } else {
        url = url + '?k_finish_down_key_=' + finish_down_key;
    }
    inteVal = setInterval(()=>{
        $B.request({
            url: url + "&_t=" + $B.generateMixed(5),
            ok: function (msg, data, res) {
                if (msg !== 'null' && msg !="") {
                    clearInterval(inteVal);
                    $msg.innerHTML = "<h2>" + msg + "</h2>";
                    if (callReturn) {
                        setTimeout(function () {
                            args.onSuccess(res);
                        }, 1500);
                    }
                }
            },
            fail:function(){},
            error:function(){}
        });
    },1000);
    $B.DomUtils.onload(ifr,onCompleteFn);
    $form.submit();
}; 
 if(!window["$B"] ) {window["$B"] = $B;} 
  return window["$B"] ; 
});

/*! BUI - v2.0.0 - 2022-03-13 09:03:17 
Copyright (c): kevin.huang  https://gitee.com/kevin-huang/bui2.0.git (qq:757723114) 
Released under MIT License*/
(function ( global, factory) {
//   "use strict";
	if ( typeof module === "object" && typeof module.exports === "object" ) {
		module.exports = global.document ?
			factory( global, true ) :
			function( w ) {
				if ( !w.document ) {
					throw new Error( "Bui requires a window with a document" );
				}
				return factory( w );
			};
	} else {
		  factory( global );
	}  
})( typeof window !== "undefined" ? window : this, function( window, noGlobal ) {
//   "use strict";
  var $B = window["$B"] ?  window["$B"] : {};

function Layout(opts) {
    var elObj, id, topEl, leftEl, bottomEl, rightEl, centerEL, readerEls = [];
    if (typeof opts.el === "string") {
        elObj = document.getElementById(opts.el);
        id = opts.el;
    } else {
        elObj = opts.el;
        d = $B.DomUtils.attribute(elObj, "id");
        if (!id) {
            id = $B.getShortID();
        }
    }
    $B.DomUtils.css(elObj, { "padding": "0 0 0 0", "position": "relative", "overflow": "hidden" });
    $B.DomUtils.addClass(elObj, "k_box_size k_layout_el");
    centerEL = $B.DomUtils.createEl("<div style='width:100%;height:100%;position:absolute;top:0;left:0;overflow:auto;' id='" + id + "_center' class='k_box_size'></div>");
    if (opts.center) {
        if (opts.center.css) {
            $B.DomUtils.css(centerEL, opts.center.css);
        }
        if (opts.center.overflow) {
            if (opts.center.overflow === "diy") {
                let tmp = $B.DomUtils.createEl("<div style='width:100%;height:100%'></div>");
                $B.DomUtils.append(centerEL, tmp);
                let $c = $B.myScrollbar(tmp, {style:{color:opts.center.diyScrollColor}});
                readerEls.push({ flag: "center", el: $c });
            } else {
                $B.DomUtils.css(centerEL, { overflow: opts.center.overflow });
                readerEls.push({ flag: "center", el: centerEL });
            }
        } else {
            readerEls.push({ flag: "center", el: centerEL });
        }
    }
    if (opts.top) {
        topEl = $B.DomUtils.createEl("<div style='width:100%;height:" + opts.top.size + "px;position:absolute;top:0;left:0;overflow:auto;' id='" + id + "_top' class='k_box_size'></div>");
        $B.DomUtils.css(centerEL, { "border-top": opts.top.size + "px solid #fff" });
        if (opts.top.css) {
            $B.DomUtils.css(topEl, opts.top.css);
        }
        if (opts.top.overflow) {
            if (opts.top.overflow === "diy") {
                let tmp = $B.DomUtils.createEl("<div style='width:100%;height:100%'></div>");
                $B.DomUtils.append(topEl, tmp);
                let $c = $B.myScrollbar(tmp, {});
                readerEls.push({ flag: "top", el: $c });
            } else {
                $B.DomUtils.css(topEl, { overflow: opts.top.overflow });
                readerEls.push({ flag: "top", el: topEl });
            }
        } else {
            readerEls.push({ flag: "top", el: topEl });
        }
    }
    let leftResizeFN;
    if (opts.left) {
        leftEl = $B.DomUtils.createEl("<div style='height:100%;width:" + opts.left.size + "px;position:absolute;top:0;left:0;overflow:auto;' id='" + id + "_left' class='k_box_size'></div>");
        if (opts.top) {
            $B.DomUtils.css(leftEl, { "border-top": opts.top.size + "px solid #fff" });
        }
        if (opts.bottom) {
            $B.DomUtils.css(leftEl, { "border-bottom": opts.bottom.size + "px solid #fff" });
        }
        $B.DomUtils.css(centerEL, { "border-left": opts.left.size + "px solid #fff" });
        if (opts.left.css) {
            $B.DomUtils.css(leftEl, opts.left.css);
        }
        if (opts.left.resizebar) {
            leftResizeFN = (e) => {     
                $B.DomUtils.attribute(leftEl, {"called":1});           
                let el = $B.DomUtils.children(leftEl, ".k_layout_resize")[0];
                let $i = el.lastChild;
                let siblings = $B.DomUtils.siblings(el);
                if (!$B.DomUtils.attribute(leftEl, "closed")) {
                    let w = $B.DomUtils.width(leftEl);
                    $B.DomUtils.attribute(leftEl, { "closed": true,"_w":w });
                    $B.hide(siblings);
                    el.style.width = "10px";
                    $B.animate($i, { "rotateZ": "180deg" }, { duration: 200 });
                    $B.animate(leftEl, { width: "10px" }, { duration: 200 });
                    $B.animate(centerEL, { "border-left-width": "10px" }, { duration: 200,complete:()=>{
                        try{
                            $B.DomUtils.trigger(window,"resize")
                        }catch(e){}                      
                    } });
                    if(opts.left.resizebar.backgroundColorAct){
                        $B.DomUtils.css(el,{"background-color":opts.left.resizebar.backgroundColorAct});
                    }
                    if(opts.left.resizebar.iconColorAct){
                        $B.DomUtils.css($i,{"color":opts.left.resizebar.iconColorAct});
                    }
                } else {      
                    let w = parseFloat($B.DomUtils.attribute(leftEl,"_w"));                               
                    $B.animate(leftEl, { width: w + "px" }, { duration: 200 });
                    $B.animate(centerEL, { "border-left-width":w + "px" }, { duration: 200 ,complete:()=>{
                        $B.animate($i, { "rotateZ": "0deg" }, { duration: 200 });
                        $B.show(siblings);
                        $B.DomUtils.removeAttribute(leftEl, "closed");  
                        if(opts.left.resizebar.backgroundColorAct){
                            $B.DomUtils.css(el,{"background-color":opts.left.resizebar.backgroundColor});
                        }
                        if(opts.left.resizebar.iconColorAct){
                            $B.DomUtils.css($i,{"color":opts.left.resizebar.iconColor});
                        } 
                        try{
                            $B.DomUtils.trigger(window,"resize")
                        }catch(e){}   
                        el.style.display = "none";   
                        el.style.width = "8px";                     
                    }});
                }
                setTimeout(()=>{
                    $B.DomUtils.removeAttribute(leftEl, "called");
                },500);             
            };
            $B.DomUtils.bind(leftEl, {
                mousemove: (e) => {
                    let el = $B.DomUtils.children(leftEl, ".k_layout_resize")[0];
                    if (!$B.DomUtils.attribute(leftEl, "closed")) {
                        let ofs = $B.DomUtils.offset(leftEl);
                        let w = $B.DomUtils.width(leftEl);
                        let left = ofs.left + w - 15;
                        let mouseX = e.pageX;
                        if (mouseX > left) {
                            let h = $B.DomUtils.height(leftEl) / 2 - 50;
                            if (h < 10) {
                                h = 10;
                            }
                            el.lastChild.style.top = h + "px";
                            el.style.display = "";
                        } else {
                            el.style.display = "none";
                        }                        
                    }
                },
                mouseleave: (e) => {
                    let el = $B.DomUtils.children(e.target, ".k_layout_resize")[0];
                    if (!$B.DomUtils.attribute(leftEl, "closed")) {
                        el.style.display = "none";
                    }
                }
            });
        }
        let notDiy = true;
        if (opts.left.overflow) {
            if (opts.left.overflow === "diy") {
                notDiy = false;
                let tmp = $B.DomUtils.createEl("<div style='width:100%;height:100%'></div>");
                $B.DomUtils.append(leftEl, tmp);
                let $c = $B.myScrollbar(tmp,  {style:{color:opts.left.diyScrollColor}});
                readerEls.push({ flag: "left", el: $c });
                if (leftResizeFN) {
                    let $rs = $B.createEl("<div class='k_layout_resize' style='cursor:pointer;position:absolute;top:0;right:0;width:8px;height:100%;background:"+opts.left.resizebar.backgroundColor+";padding:0;margin:0;display:none;'><i style='width:100%;text-align:center;font-size:12px;color:"+opts.left.resizebar.iconColor+";position:absolute;top:100px;' class='fa fa-angle-double-left'></i></div>");
                    $B.DomUtils.append(leftEl, $rs);
                    $B.DomUtils.click($rs, leftResizeFN);
                    bindDrag($rs,leftEl,centerEL);
                }
            } else {
                $B.DomUtils.css(leftEl, { overflow: opts.left.overflow });
            }
        }
        if (notDiy) {
            let $contentEL = leftEl;
            if (leftResizeFN) {
                $B.DomUtils.css(leftEl, { overflow: "hidden" });
                $contentEL = $B.createEl("<div style='height:100%;width:100%;overflow:auto'></div>");
                if (opts.left.overflow) {
                    $B.DomUtils.css($contentEL, { overflow: opts.left.overflow });
                }               
                $B.DomUtils.append(leftEl,$contentEL);
                let $rs = $B.createEl("<div class='k_layout_resize' style='cursor:pointer;position:absolute;top:0;right:0;width:8px;height:100%;background:"+opts.left.resizebar.backgroundColor+";padding:0;margin:0;display:none;'><i style='width:100%;text-align:center;font-size:12px;color:"+opts.left.resizebar.iconColor+";position:absolute;top:100px;' class='fa fa-angle-double-left'></i></div>");
                $B.DomUtils.append(leftEl, $rs);
                $B.DomUtils.click($rs, leftResizeFN);
                bindDrag($rs,leftEl,centerEL);
            }
            readerEls.push({ flag: "left", el: $contentEL });
        }
    }
    if (opts.bottom) {
        bottomEl = $B.DomUtils.createEl("<div style='width:100%;height:" + opts.bottom.size + "px;position:absolute;bottom:0;left:0;overflow:auto;' id='" + id + "_bottom' class='k_box_size'></div>");
        $B.DomUtils.css(centerEL, { "border-bottom": opts.bottom.size + "px solid #fff" });
        if (opts.bottom.css) {
            $B.DomUtils.css(bottomEl, opts.bottom.css);
        }
        if (opts.bottom.overflow) {
            if (opts.bottom.overflow === "diy") {
                let tmp = $B.DomUtils.createEl("<div style='width:100%;height:100%'></div>");
                $B.DomUtils.append(bottomEl, tmp);
                let $c = $B.myScrollbar(tmp, {});
                readerEls.push({ flag: "bottom", el: $c });
            } else {
                $B.DomUtils.css(bottomEl, { overflow: opts.bottom.overflow });
                readerEls.push({ flag: "bottom", el: bottomEl });
            }
        } else {
            readerEls.push({ flag: "bottom", el: bottomEl });
        }
    }
    let rightResizeFN;
    if (opts.right) {
        rightEl = $B.DomUtils.createEl("<div style='height:100%;width:" + opts.right.size + "px;position:absolute;bottom:0;right:0;overflow:auto;' id='" + id + "_right' class='k_box_size'></div>");
        $B.DomUtils.css(centerEL, { "border-right": opts.right.size + "px solid #fff" });
        if (opts.top) {
            $B.DomUtils.css(rightEl, { "border-top": opts.top.size + "px solid #fff" });
        }
        if (opts.bottom) {
            $B.DomUtils.css(rightEl, { "border-bottom": opts.bottom.size + "px solid #fff" });
        }
        if (opts.right.css) {
            $B.DomUtils.css(rightEl, opts.right.css);
        }
        if (opts.right.resizebar) {
            rightResizeFN = (e)=>{
                $B.DomUtils.attribute(rightEl, {"called":1});           
                let el = $B.DomUtils.children(rightEl, ".k_layout_resize")[0];
                let $i = el.lastChild;
                let siblings = $B.DomUtils.siblings(el);
                if (!$B.DomUtils.attribute(rightEl, "closed")) {
                    let w = $B.DomUtils.width(rightEl);
                    $B.DomUtils.attribute(rightEl, { "closed": true,"_w":w });
                    $B.hide(siblings);
                    el.style.width = "10px";
                    $B.animate($i, { "rotateZ": "180deg" }, { duration: 200 });
                    $B.animate(rightEl, { width: "10px" }, { duration: 200 });
                    $B.animate(centerEL, { "border-right-width": "10px" }, { duration: 200,complete:()=>{
                        try{
                            $B.DomUtils.trigger(window,"resize")
                        }catch(e){}                      
                    } });
                    if(opts.right.resizebar.backgroundColorAct){
                        $B.DomUtils.css(el,{"background-color":opts.right.resizebar.backgroundColorAct});
                    }
                    if(opts.right.resizebar.iconColorAct){
                        $B.DomUtils.css($i,{"color":opts.right.resizebar.iconColorAct});
                    }
                } else {      
                    let w = parseFloat($B.DomUtils.attribute(rightEl,"_w"));                               
                    $B.animate(rightEl, { width: w + "px" }, { duration: 200 });
                    $B.animate(centerEL, { "border-right-width":w + "px" }, { duration: 200 ,complete:()=>{
                        $B.animate($i, { "rotateZ": "0deg" }, { duration: 200 });
                        $B.show(siblings);
                        setTimeout(()=>{
                            $B.DomUtils.removeAttribute(rightEl, "closed"); 
                        },300);                         
                        if(opts.right.resizebar.backgroundColorAct){
                            $B.DomUtils.css(el,{"background-color":opts.right.resizebar.backgroundColor});
                        }
                        if(opts.right.resizebar.iconColorAct){
                            $B.DomUtils.css($i,{"color":opts.right.resizebar.iconColor});
                        } 
                        try{
                            $B.DomUtils.trigger(window,"resize")
                        }catch(e){}  
                        el.style.display = "none";   
                        el.style.width = "8px";                                       
                    }});
                }
                setTimeout(()=>{
                    $B.DomUtils.removeAttribute(rightEl, "called");
                },500);                 
            };
            $B.DomUtils.bind(rightEl, {
                mousemove: (e) => {
                    let el = $B.DomUtils.children(rightEl, ".k_layout_resize")[0];
                    if (!$B.DomUtils.attribute(rightEl, "closed")) {
                        let ofs = $B.DomUtils.offset(rightEl);
                        let left = ofs.left +  20;
                        let mouseX = e.pageX;
                        if (mouseX < left) {
                            let h = $B.DomUtils.height(rightEl) / 2 - 50;
                            if (h < 10) {
                                h = 10;
                            }
                            el.lastChild.style.top = h + "px";
                            el.style.display = "";
                            console.log("mousemove top "+h);
                        } else {
                            el.style.display = "none";
                        }                        
                    }
                },
                mouseleave: (e) => {
                    let el = $B.DomUtils.children(e.target, ".k_layout_resize")[0];
                    if (!$B.DomUtils.attribute(rightEl, "closed")) {
                        el.style.display = "none";
                    }
                }
            });
        }
        let notDiy = true;
        if (opts.right.overflow) {
            if (opts.right.overflow === "diy") {
                notDiy = false;
                let tmp = $B.DomUtils.createEl("<div style='width:100%;height:100%'></div>");
                $B.DomUtils.append(rightEl, tmp);
                let $c = $B.myScrollbar(tmp,  {style:{color:opts.right.diyScrollColor}});
                readerEls.push({ flag: "right", el: $c });
                if (rightResizeFN) {
                    let $rs = $B.createEl("<div class='k_layout_resize' style='display:none;cursor:pointer;position:absolute;top:0;left:0;width:8px;height:100%;background:"+opts.right.resizebar.backgroundColor+";padding:0;margin:0;'><i style='width:100%;text-align:center;font-size:12px;color:"+opts.right.resizebar.iconColor+";position:absolute;top:100px;' class='fa fa-angle-double-right'></i></div>");
                    $B.DomUtils.append(rightEl, $rs);
                    $B.DomUtils.click($rs, rightResizeFN);
                    bindDrag($rs,rightEl,centerEL,true);
                }
            } else {
                $B.DomUtils.css(rightEl, { overflow: opts.right.overflow });               
            }
        } 
        if(notDiy) {
            let $contentEL = rightEl;
            if (rightResizeFN) {
                $B.DomUtils.css(rightEl, { overflow: "hidden" });
                $contentEL = $B.createEl("<div style='height:100%;width:100%;overflow:auto'></div>");
                if (opts.right.overflow) {
                    $B.DomUtils.css($contentEL, { overflow: opts.right.overflow });
                }               
                $B.DomUtils.append(rightEl,$contentEL);
                let $rs = $B.createEl("<div class='k_layout_resize' style='cursor:pointer;position:absolute;top:0;left:0;width:8px;height:100%;background:"+opts.right.resizebar.backgroundColor+";padding:0;margin:0;display:none;'><i style='width:100%;text-align:center;font-size:12px;color:"+opts.right.resizebar.iconColor+";position:absolute;top:100px;' class='fa fa-angle-double-right'></i></div>");
                $B.DomUtils.append(rightEl, $rs);
                $B.DomUtils.click($rs, rightResizeFN);
                bindDrag($rs,rightEl,centerEL,true);
            }
            readerEls.push({ flag: "right", el: $contentEL });
        }
    }
    $B.DomUtils.append(elObj, centerEL);
    if (leftEl) {
        $B.DomUtils.append(elObj, leftEl);
    }
    if (rightEl) {
        $B.DomUtils.append(elObj, rightEl);
    }
    if (topEl) {
        $B.DomUtils.append(elObj, topEl);
    }
    if (bottomEl) {
        $B.DomUtils.append(elObj, bottomEl);
    }
    if (opts.onRender) {
        setTimeout(() => {
            for (let i = 0; i < readerEls.length; i++) {
                opts.onRender(readerEls[i].flag, readerEls[i].el);
            }
        }, 1);
    }
    return id;
}
function bindDrag($rs,panelEl,centerEL,isRight){
    $B.draggable($rs,{
        holdTime:350,
        isProxy: true, //是否产生一个空代理进行拖动
        axis:'h',
        onTimeIsUpFn:()=>{
            if( $B.DomUtils.attribute(panelEl,"called")){
                return true;
            }
            if($B.DomUtils.attribute(panelEl,"closed")){
                return true;
            }
        },
        onStopDrag:(e)=>{
            let dv = e.state._data;
            let leftOffset = dv.leftOffset;
            let newWidth,borderAttr ;
            if(isRight){
                newWidth = $B.DomUtils.width(panelEl) - leftOffset;
                borderAttr = "border-right-width";
            }else{
                newWidth = $B.DomUtils.width(panelEl) + leftOffset;
                borderAttr = "border-left-width";
            }           
            if(newWidth < 10){
                newWidth = 10;
            }
            let cssObj ={};
            cssObj[borderAttr] = newWidth +"px";
            $B.animate(panelEl, { width: newWidth + "px" }, { duration: 200 });
            $B.animate(centerEL, cssObj, { duration: 200,complete:()=>{
                try{
                    $B.DomUtils.trigger(window,"resize")
                }catch(e){}                      
            } 
            });                           
        },
        onProxyEnd:(e)=>{
            return false;
        }
    });
}
$B["Layout"] = Layout; 
 if(!window["$B"] ) {window["$B"] = $B;} 
  return window["$B"] ; 
});

/*! BUI - v2.0.0 - 2022-03-13 09:03:17 
Copyright (c): kevin.huang  https://gitee.com/kevin-huang/bui2.0.git (qq:757723114) 
Released under MIT License*/
(function ( global, factory) {
//   "use strict";
	if ( typeof module === "object" && typeof module.exports === "object" ) {
		module.exports = global.document ?
			factory( global, true ) :
			function( w ) {
				if ( !w.document ) {
					throw new Error( "Bui requires a window with a document" );
				}
				return factory( w );
			};
	} else {
		  factory( global );
	}  
})( typeof window !== "undefined" ? window : this, function( window, noGlobal ) {
//   "use strict";
  var $B = window["$B"] ?  window["$B"] : {};

function CURD(tableObj, opts) {
    this.gdOpts = $B.extendObjectFn({}, $B.config.curd.table, opts);
    if(opts.methodsObject){
        this.gdOpts.topBtnStyle.methodsObject = opts.methodsObject;
        this.gdOpts.trBtnStyle.methodsObject = opts.methodsObject;
    }
    this.actions = $B.config.curd.actions;
    this.id = this.gdOpts.id;
    this.curWindow = null; //当前打开的window
    this.idField = this.gdOpts.idField ? this.gdOpts.idField : "id";
    var httpHost = $B.getHttpHost();
    if (this.gdOpts.url.indexOf(httpHost) >= 0) {
        this.url = this.gdOpts.url;
    } else {
        this.url = httpHost + this.gdOpts.url;
    }
    var _url = this.url;
    if (this.url && !/gridid=\w+/.test(this.url)) {
        if (this.url.indexOf("?") > 0) {
            _url = this.url + "&gridid=" + this.id;
        } else {
            _url = this.url + "?gridid=" + this.id;
        }
    }
    this.gdOpts.url = _url;
    if (this.gdOpts.fit2height && this.gdOpts.pageArea) { //自适应全屏大小           
        let $page = this.gdOpts.pageArea;
        if (typeof this.gdOpts.pageArea === "string") {
            $page = document.getElementById(this.gdOpts.pageArea.replace("#", ""));
        }
        $B.DomUtils.addClass($page, "k_box_size");
        $page.style.height = "100%";
        let $f = $B.DomUtils.children($page, ".k_form_condition")[0];
        let $g = $B.DomUtils.children($page, ".k_form_grid")[0];
        let need2move = true;
        if ($f.previousSibling && $f.previousSibling === $g) {
            need2move = false;
        }
        $B.DomUtils.addClass($f, "k_box_size");
        $B.DomUtils.addClass($g, "k_box_size");
        let formHeight = $B.DomUtils.outerHeight($f);
        if (need2move) {
            $B.DomUtils.detach($g);
        }
        let css = {
            "position": 'absolute',
            "top": 0,
            "left": 0,
            "height": "100%",
            "width": "100%",
            "border-top": formHeight + "px solid rgba(255,255,255,0)"
        };
        $B.DomUtils.css($g, css);
        $B.DomUtils.css($f, { "position": 'absolute', "top": 0, "left": 0, "width": "100%" });
        if (need2move) {
            $B.DomUtils.prepend($page, $g);
        }
    }
    this.dg = new $B.Table(tableObj, this.gdOpts);
}
CURD.prototype = {
    constructor: CURD,
    setVueForm: function (form) {
        this.vueForm = form;
    },
    /**获取datagrid对象**/
    getDataGrid: function () {
        return this.dg;
    },
    updateTitle: function (title) {
        this.dg.updateTitle(title);
    },
    /** 打开行内嵌页面
    tr:表格行对象
    ****/
    openInner: function (tr) {
        this.dg.openInner(tr);
    },
    /**打开一个操作窗口 一般用于弹出新增、修改窗口
     * args={
        full:false,//是否满屏，当为true时候，高宽无效  
        autoHeight:true,//自动根据内容设置高度
        params:{} ,//参数
        size: { width: 'auto', height: 'auto' },         
        title: '', //标题
        isTop: false,
        iconCls: null, //图标class，font-awesome字体图标
        iconColor: undefined,//图标颜色
        headerColor: undefined,//头部颜色
        toolbar: null, //工具栏对象参考工具栏组件配置说明，可以是创建函数
        toolbarStyle: undefined,//参考工具栏样式定义
        shadow: true, //是否需要阴影
        radius: undefined, //圆角px定义
        header: true, //是否显示头部
        zIndex: 2147483647,//层级
        content: null, //静态内容
        url: '',//请求地址
        dataType: 'html', //当为url请求时，html/json/iframe
        draggable: false, //是否可以拖动
        moveProxy: false, //是否代理移动方式
        draggableHandler: 'header', //拖动触发焦点
        closeable: false, //是否关闭
        closeType: 'hide', //关闭类型 hide(隐藏，可重新show)/ destroy 直接从dom中删除
        expandable: false, //可左右收缩
        maxminable: false, //可变化小大
        collapseable: false, //上下收缩
        resizeable: false,//右下角拖拉大小
        onResized: null, //function (pr) { },//大小变化事件
        onLoaded: null, //function () { },//加载后
        onClose: null, //关闭前
        onClosed: null, //function () { },//关闭后
        onExpanded: null, // function (pr) { },//左右收缩后
        onCollapsed: null, // function (pr) { }//上下收缩后
        onCreated: null //function($content,$header){} panel创建完成事件
    }   
    isUpdated 是否是打开修改窗口
     ***/
    window: function (args, isUpdated) {
        var id,strPrs;
        if ($B.isPlainObjectFn(args.params)) {
            var tmp = [];
            var keys = Object.keys(args.params);
            for (var i = 0, len = keys.length; i < len; ++i) {
                tmp.push(keys[i] + "=" + decodeURIComponent(args.params[keys[i]]));
                if(keys[i] === this.idField){
                    id = args.params[keys[i]];
                }
            }
            strPrs = tmp.join("&");
        }
        if (isUpdated) {
            if (!id) {
                var chkdatas = this.dg.getCheckedData(true);
                if (chkdatas.length === 0) {
                    var msg = typeof args.content === 'undefined' ? $B.config.curd.checkforUpdate : args.content;
                    $B.alert(msg, 1.5);
                    return;
                }
                if (chkdatas.length > 1) {
                    $B.alert($B.config.onlyCheckOneData);
                    return;
                }
                id = chkdatas[0];
                if(strPrs !== ""){
                    strPrs =  strPrs + '&id=' + id;
                }else{
                    strPrs =  'id=' + id;
                }
            }           
            if (!args["iconCls"]) {
                args["iconCls"] = "fa-edit";
            }
        }
        var opts = $B.extendObjectFn({}, $B.config.curd.window, args);
        var pageName = (opts.pageName && opts.pageName !== "") ? opts.pageName : "form";
        opts.url = this.url.replace(/\?\S+/, '').replace(/list/, $B.config.curd.actions.page) + "/" + pageName;
        if (strPrs && strPrs !== '') {
            opts.url = opts.url + "?" + strPrs;
        }
        delete opts.pageName; 
        let onCloseFn = opts.onClosed;
        opts.onClosed = () => {
            if (this.vueForm && this.vueForm.toDestroy) {
                this.vueForm.toDestroy();
                console.log("colsed destroy vue form!");
            }
            this.vueForm = undefined;
            if (onCloseFn) {
                onCloseFn();
            }
        };
        this.curWindow = $B.window(opts);
        return this.curWindow;
    },
    /**
     * 用于$B.window()与curd.window()共用页面时候
     * 便于内部curdObj.add/update能够自动关闭窗口
     * **/
    setOpenWindow: function (win) {
        this.curWindow = win;
    },
    /*** 关闭当前打开的窗口***/
    close: function () {
        if (this.curWindow !== null && this.curWindow.close) {
            this.curWindow.close();
        }
        this.curWindow = null;
    },
    closeWindow: function () {
        this.close();
    },
    /**新增args=
     {
        data:{}, //提交的参数
        ok:function(data,message){}//成功处理后的回调,
        final:function(res){} //无论成功，失败都回调的事件
    }/
     args = {f1:xx,f2:xx...};
     onReturnFn:回调或者按钮
     ***/
    add: function () {
        console.log(arguments);
        var args, onReturnFn,btn;
        for(let i = 0 ;i < arguments.length ;i++){
            let arg = arguments[i];
            if(typeof arg === "function"){
                onReturnFn = arg;
            }else if($B.Dom.isElement(arg)){
                btn = arg;
            }else{
                args = arg;
            }
        }
        let actions = $B.config.curd.actions;
        var url = this.url.replace(/\?\S+/, '').replace(/list/, actions.add);
        this._save(args, url, onReturnFn,btn);
    },
    /**
     * 更新 args={
        data:{},
        ok:function(data,message){},
        final:function(res){}
    }/
     args = {f1:xx,f2:xx...};
     onReturnFn:回调或者按钮
     * **/
    update: function () {
        var args, onReturnFn,btn;
        for(let i = 0 ;i < arguments.length ;i++){
            let arg = arguments[i];
            if(typeof arg === "function"){
                onReturnFn = arg;
            }else if($B.Dom.isElement(arg)){
                btn = arg;
            }else{
                args = arg;
            }
        }
        let actions = $B.config.curd.actions;
        var url = this.url.replace(/\?\S+/, '').replace(/list/, actions.update);
        this._save(args, url, onReturnFn,btn);
    },
    /**add / update 的内部调用函数**/
    _save: function (args, url, onReturnFn,btn) {        
        var _this = this;
        var opts;
        if (args.data && (typeof args.url !== "undefined" || typeof args.fail === "function" || typeof args.ok === "function" || typeof args.final === "final")) {
            opts = args;
        } else {
            opts = {
                waiting: true,
                data: args,
                ok: function (message, data, res) {
                    _this.reload({
                        page: 1
                    });
                    setTimeout(()=>{
                        _this.close();
                    },1);                     
                    if (typeof onReturnFn === "function") {
                        onReturnFn(res);
                    }                                      
                },
                fail: function (message, res) {
                    $B.error(message, 1.5);
                    if (typeof onReturnFn === "function") {
                        onReturnFn(res);
                    }
                }
            };
        }
        opts.url = url;       
        $B.request(opts, btn);
    },
    /**删除数据 idArray = 需要删除的id数组
     * args:其他附加的参数
     * ***/
    deleteData: function (idArray, args) {
        return this._del({}, idArray, args);
    },
    /**删除复选的数据 args={
        content:'提示信息',
        params:{},
        ok:function(data,message){},
        final:function(res){},
        fail:function(){}
        }
     * **/
    delChecked: function (args) {
        var chkdatas = this.dg.getCheckedData(true);
        return this._del(args, chkdatas);
    },
    _del: function (args, idArray, prs) {
        if (idArray.length === 0) {
            var msg = (!args || typeof args.content === 'undefined') ? $B.config.curd.checkforDelete : args.content;
            $B.alert(msg,1.5);
            return;
        }
        var _this = this;
        return $B.confirm({
            content: $B.config.curd.confirmDelete,
            okFn: function () {
                var deleteAll = _this.dg.getRowCount() === idArray.length;
                var url = _this.url.replace(/\?\S+/, '').replace(/list/, $B.config.curd.actions.del);
                var params = { idList: idArray.join(",") };
                if (prs) {
                    $B.extendObjectFn(params, prs);
                }
                if (args && args.params) {
                    $B.extendObjectFn(params, args.params);
                }
                var opts = {
                    waiting: true,
                    url: url,
                    data: params,
                    ok: function (message, data) {
                        _this.close();
                        if (deleteAll) {
                            _this.reload({
                                page: 1
                            });
                        } else {
                            _this.refresh();
                        }
                    }
                };
                $B.request(opts);
            }
        });
    },
    /**
     * 获取复选的数据
     * ***/
    getCheckedData: function () {
        return this.dg.getCheckedData();
    },
    /**
     * 获取复选的数据id
     * ***/
    getCheckedId: function () {
        return this.dg.getCheckedData(true);
    },
    /**根据参数查询多条数据
     args={
        params:{},
        ok:function(data,message){}
    }
     ****/
    query: function (args) {
    },
    /**根据id查询单条数据
     args={
        id:id
        ok:function(data,message){}
        }
     ***/
    get: function (args) { },
    /**
     * 重新加载
     * args={} 查询参数
     * ****/
    reload: function (args) {
        this.dg.reload(args);
    },
    /**
     * 采用上一次查询的参数刷新当前页
     * **/
    refresh: function () {
        this.dg.refresh();
    }
};
$B["CURD"] = CURD; 
 if(!window["$B"] ) {window["$B"] = $B;} 
  return window["$B"] ; 
});

/*! BUI - v2.0.0 - 2022-03-13 09:03:17 
Copyright (c): kevin.huang  https://gitee.com/kevin-huang/bui2.0.git (qq:757723114) 
Released under MIT License*/
(function ( global, factory) {
//   "use strict";
	if ( typeof module === "object" && typeof module.exports === "object" ) {
		module.exports = global.document ?
			factory( global, true ) :
			function( w ) {
				if ( !w.document ) {
					throw new Error( "Bui requires a window with a document" );
				}
				return factory( w );
			};
	} else {
		  factory( global );
	}  
})( typeof window !== "undefined" ? window : this, function( window, noGlobal ) {
//   "use strict";
  var $B = window["$B"] ?  window["$B"] : {};

var defaultOpts = {
    control: 'hue',
    defaultValue: '#191933',
    record: 6,
    mouseleaveHide:false,
    recodrOnHide: true,
    onChange: undefined,
    onStartFn: null,
    onEndFn: null,
    onHideFn: null,
    buttons: ['transparent', "#ffffff",
        "#000000", "#5C5C5C", "#7B7B7B", "#C4C4C4",
        "#CC0000", "#DB5050", "#E99899", "#FFC8B8",
        "#FFB700", "#F7C946", "#F7DB8B", "#FFF2CC",
        "#558303", "#89C915", "#ACDB7E", "#D9EAD3",
        "#0079E3", "#36A0FA", "#81D3E5", "#DFF8FF",
        "#0324B7", "#1F48FA", "#8B95FA", "#E3E5FC",
        "#9803C9", "#CE43FA", "#DE81FA", "#F2D6FA",
        "#0AD8DB", "#3EDB0A", "#DB0AD6", "#EAB905", "#FFEA00", "#0034FF"
    ]
};
function keepWithin(value, min, max) {
    if (value < min) {
        value = min;
    }
    if (value > max) {
        value = max;
    }
    return value;
}

function parseHex(string, expand) {
    string = string.replace(/[^A-F0-9]/ig, '');
    if (string === undefined || string === "") {
        return "";
    }
    var len = string.length;
    var diff = 6 - len,
        lastChar;
    if (diff) {
        lastChar = string[len - 1];
    }
    while (diff > 0) {
        string += lastChar;
        diff--;
    }
    return '#' + string;
}
function convertCase(string, letterCase) {
    return letterCase === 'uppercase' ? string.toUpperCase() : string.toLowerCase();
}
function getCoords(picker, container) {
    let pos = $B.DomUtils.position(picker);
    return { x: pos.left, y: pos.top };
}
function getElE(ins) {
    var minicolors = ins.elObj;
    if (!ins.ctlMap) {
        ins.ctlMap = {};
        ins.ctlMap["grid"] = $B.DomUtils.findByClass(minicolors, '.k_minicolors_grid')[0];
        ins.ctlMap["slider"] = $B.DomUtils.findByClass(minicolors, '.k_minicolors_slider')[0];
        ins.ctlMap["opacitySlider"] = $B.DomUtils.findByClass(minicolors, '.k_minicolors_opacity_slider')[0];
        ins.ctlMap["gridPicker"] = $B.DomUtils.findByClass(ins.ctlMap["grid"], '.k_minicolors_picker')[0];
        ins.ctlMap["sliderPicker"] = $B.DomUtils.findByClass(ins.ctlMap["slider"], '.k_minicolors_picker')[0];
        ins.ctlMap["opacityPicker"] = $B.DomUtils.findByClass(ins.ctlMap["opacitySlider"], '.k_minicolors_picker')[0];
        ins.ctlMap["gWidth"] = $B.DomUtils.outerWidth(ins.ctlMap["grid"]);
        ins.ctlMap["gHeight"] = $B.DomUtils.outerHeight(ins.ctlMap["grid"]);
        ins.ctlMap["sWidth"] = $B.DomUtils.outerHeight(ins.ctlMap["slider"]);
        ins.ctlMap["sHeight"] = $B.DomUtils.outerHeight(ins.ctlMap["slider"]);
        ins.ctlMap["$inners"] = $B.DomUtils.findByClass(ins.ctlMap["grid"], ".k_minicolors_grid_inner")[0];
    }
    return ins.ctlMap;
}
/***
 * css =  {hex:,opacity:}
 * **/
function updateFromInput(css) {
    var ctlMap = getElE(this);
    var hex = css["hex"],
        hsb, els,
        opacity = css["opacity"],
        x, y, r, phi,
        settings = this.opts,
        minicolors = this.elObj,
        // swatch = ctlMap.swatch,
        grid = ctlMap.grid,
        slider = ctlMap.slider,
        gridPicker = ctlMap.gridPicker,
        sliderPicker = ctlMap.sliderPicker,
        opacityPicker = ctlMap.opacityPicker;
    if (!hex) {
        hex = convertCase(parseHex(settings.defaultValue, true), "lowercase");
    }
    hsb = $B.hex2Hsb(hex);

    var _pos, bkColor,
        gH = ctlMap.gHeight,
        gW = ctlMap.gWidth,
        sH = ctlMap.sHeight - 4,
        sW = ctlMap.sWidth;
    switch (settings.control) {
        case 'wheel':
            r = keepWithin(Math.ceil(hsb.s * 0.75), 0, gH / 2);
            phi = hsb.h * Math.PI / 180;
            x = keepWithin(75 - Math.cos(phi) * r, 0, gW);
            y = keepWithin(75 - Math.sin(phi) * r, 0, gH);
            _pos = {
                top: y + 'px',
                left: x + 'px'
            };
            y = 150 - (hsb.b / (100 / gH));
            if (hex === '') {
                y = 0;
            }
            sliderPicker.style.top = y + 'px';
            bkColor = $B.hsb2Hex({
                h: hsb.h,
                s: hsb.s,
                b: 100
            });
            $B.DomUtils.css(slider, { "background-color": bkColor });
            break;

        case 'saturation':
            x = keepWithin((5 * hsb.h) / 12, 0, 150);
            y = keepWithin(gH - Math.ceil(hsb.b / (100 / gH)), 0, gH);
            _pos = {
                top: y + 'px',
                left: x + 'px'
            };
            y = keepWithin(sH - (hsb.s * (sH / 100)), 0, sH);
            sliderPicker.style.top = y + 'px';
            bkColor = $B.hsb2Hex({
                h: hsb.h,
                s: 100,
                b: hsb.b
            });
            $B.DomUtils.css(slider, { "background-color": bkColor });
            els = $B.DomUtils.find(minicolors, '.minicolors-grid-inner');
            $B.DomUtils.css(els, { 'opacity': hsb.s / 100 });
            break;

        case 'brightness':
            x = keepWithin((5 * hsb.h) / 12, 0, 150);
            y = keepWithin(gH - Math.ceil(hsb.s / (100 / gH)), 0, gH);
            _pos = {
                top: y + 'px',
                left: x + 'px'
            };
            y = keepWithin(sH - (hsb.b * (sH / 100)), 0, sH);
            sliderPicker.style.top = y + 'px';
            bkColor = $B.hsb2Hex({
                h: hsb.h,
                s: hsb.s,
                b: 100
            });
            els = $B.DomUtils.find(minicolors, '.minicolors-grid-inner');
            $B.DomUtils.css(els, { 'opacity': 1 - (hsb.b / 100) });
            break;
        default:
            x = keepWithin(Math.ceil(hsb.s / (100 / gW)), 0, gW);
            y = keepWithin(gH - Math.ceil(hsb.b / (100 / gH)), 0, gH);
            _pos = {
                top: y + 'px',
                left: x + 'px'
            };
            y = keepWithin(sH - (hsb.h / (360 / sH)), 0, sH);
            sliderPicker.style.top = y + 'px';
            bkColor = $B.hsb2Hex({
                h: hsb.h,
                s: 100,
                b: 100
            });
            $B.DomUtils.css(grid, { "background-color": bkColor });
            y = sH * (1 - opacity);
            opacityPicker.style.top = y + "px";
            break;
    }
    gridPicker.style.top = _pos.top;
    gridPicker.style.left = _pos.left;
}

function updateControl(target) {
    var ctlMap = getElE(this);
    var hue, saturation, brightness, x, y, r, phi,
        hex,
        opacity = 1,
        settings = this.opts,
        grid = ctlMap.grid,
        slider = ctlMap.slider,
        opacitySlider = ctlMap.opacitySlider,
        gridPicker = ctlMap.gridPicker,
        sliderPicker = ctlMap.sliderPicker,
        opacityPicker = ctlMap.opacityPicker;

    var gridPos = getCoords(gridPicker, grid),
        sliderPos = getCoords(sliderPicker, slider),
        opacityPos = getCoords(opacityPicker, opacitySlider);
    let go = $B.DomUtils.hasClass(target, "k_minicolors_grid")
        || $B.DomUtils.hasClass(target, "k_minicolors_slider")
        || $B.DomUtils.hasClass(target, "k_minicolors_opacity_slider");
    if (go) {
        let bkColor;
        let gWidth = ctlMap.gWidth;
        let gHeight = ctlMap.gHeight;
        let sHeight = ctlMap.sHeight;
        let $inners = ctlMap["$inners"];
        switch (settings.control) {
            case 'wheel':
                x = (gWidth / 2) - gridPos.x;
                y = (gHeight / 2) - gridPos.y;
                r = Math.sqrt(x * x + y * y);
                phi = Math.atan2(y, x);
                if (phi < 0) {
                    phi += Math.PI * 2;
                }
                if (r > 75) {
                    r = 75;
                    gridPos.x = 69 - (75 * Math.cos(phi));
                    gridPos.y = 69 - (75 * Math.sin(phi));
                }
                saturation = keepWithin(r / 0.75, 0, 100);
                hue = keepWithin(phi * 180 / Math.PI, 0, 360);
                brightness = keepWithin(100 - Math.floor(sliderPos.y * (100 / sHeight)), 0, 100);
                hex = $B.hsb2Hex({
                    h: hue,
                    s: saturation,
                    b: brightness
                });
                bkColor = $B.hsb2Hex({
                    h: hue,
                    s: saturation,
                    b: 100
                });
                $B.DomUtils.css(slider, { "backgorund-color": bkColor });
                break;
            case 'saturation':
                hue = keepWithin(parseInt(gridPos.x * (360 / gWidth), 10), 0, 360);
                saturation = keepWithin(100 - Math.floor(sliderPos.y * (100 / sHeight)), 0, 100);
                brightness = keepWithin(100 - Math.floor(gridPos.y * (100 / gHeight)), 0, 100);
                hex = $B.hsb2Hex({
                    h: hue,
                    s: saturation,
                    b: brightness
                });
                bkColor = $B.hsb2Hex({
                    h: hue,
                    s: 100,
                    b: brightness
                });
                slider.style.backgroundColor = bkColor;
                $B.DomUtils.css($inners, { "opacity": saturation / 100 });
                break;
            case 'brightness':
                hue = keepWithin(parseInt(gridPos.x * (360 / gWidth), 10), 0, 360);
                saturation = keepWithin(100 - Math.floor(gridPos.y * (100 / gHeight)), 0, 100);
                brightness = keepWithin(100 - Math.floor(sliderPos.y * (100 / sHeight)), 0, 100);
                hex = $B.hsb2Hex({
                    h: hue,
                    s: saturation,
                    b: brightness
                });
                bkColor = $B.hsb2Hex({
                    h: hue,
                    s: saturation,
                    b: 100
                });
                slider.style.backgroundColor = bkColor;
                $B.DomUtils.css($inners, { "opacity": 1 - (brightness / 100) });
                //minicolors.find('.k_minicolors_grid_inner').css('opacity', 1 - (brightness / 100));
                break;
            default:
                hue = keepWithin(360 - parseInt(sliderPos.y * (360 / sHeight), 10), 0, 360);
                saturation = keepWithin(Math.floor(gridPos.x * (100 / gWidth)), 0, 100);
                brightness = keepWithin(100 - Math.floor(gridPos.y * (100 / gHeight)), 0, 100);
                hex = $B.hsb2Hex({
                    h: hue,
                    s: saturation,
                    b: brightness
                });
                bkColor = $B.hsb2Hex({
                    h: hue,
                    s: 100,
                    b: 100
                });
                grid.style.backgroundColor = bkColor;
                break;
        }
        opacity = parseFloat(1 - (opacityPos.y / sHeight)).toFixed(2);
        doChange.call(this, hex, opacity);
        this._callChange();
    }
}
function doChange(hex, opacity) {
    if (hex.indexOf("rgb") > -1) {
        hex = $B.rgb2Hex(hex);
    }
    if (typeof opacity !== "undefined") {
        opacity = parseFloat(opacity);
    }
    if (opacity === NaN) {
        opacity = 1.0;
    }
    var lastChange = this['minicolors-lastChange'];
    if (!lastChange || lastChange.hex !== hex || lastChange.opacity !== opacity) {
        this['minicolors-lastChange'] = {
            hex: hex,
            opacity: opacity
        };
        this._updateInputs();
        if (!this.opts.recodrOnHide) {
            this.saveHisColor();
        }
    }
}
function move(moveEl, e) {
    var event = e._e;
    if (!this.target) {
        console.log("not target color skip");
        return;
    }
    let ofs = $B.DomUtils.offset(moveEl);
    var _this = this,
        picker = $B.DomUtils.findByClass(moveEl, "k_minicolors_picker")[0],
        offsetX = ofs.left,
        offsetY = ofs.top,
        x = Math.round(event.pageX - offsetX),
        y = Math.round(event.pageY - offsetY),
        wx, wy, r, phi;

    if (event.changedTouches) {
        x = event.changedTouches[0].pageX - offsetX;
        y = event.changedTouches[0].pageY - offsetY;
    }
    if (x < 0) {
        x = 0;
    }
    if (y < 0) {
        y = 0;
    }
    let tWidth = $B.DomUtils.width(moveEl);
    let tHeight = $B.DomUtils.height(moveEl);
    if (x > tWidth) {
        x = tWidth;
    }
    if (y > tHeight) {
        y = tHeight;
    }
    let go1 = $B.DomUtils.hasClass(moveEl.parentNode, '.k_minicolors_slider_wheel');
    let go2 = $B.DomUtils.hasClass(picker.parentNode, '.k_minicolors_grid');
    if (go1 && go2) {
        wx = 75 - x;
        wy = 75 - y;
        r = Math.sqrt(wx * wx + wy * wy);
        phi = Math.atan2(wy, wx);
        if (phi < 0) {
            phi += Math.PI * 2;
        }
        if (r > 75) {
            r = 75;
            x = 75 - (75 * Math.cos(phi));
            y = 75 - (75 * Math.sin(phi));
        }
        x = Math.round(x);
        y = Math.round(y);
    }
    if ($B.DomUtils.hasClass(moveEl, '.k_minicolors_grid')) {
        picker.style.top = y + 'px';
        picker.style.left = x + 'px';
        updateControl.call(_this, moveEl);
    } else {
        picker.style.top = y + 'px';
        updateControl.call(_this, moveEl);
    }
    return false;
}

class Color extends $B.BaseControl {
    constructor(tgObj, opts) {
        super();
        this.opts = $B.extendObjectFn(true, {}, defaultOpts, opts);
        this.$doc = opts.document ? opts.document : document;
        this.lastColors = [];//最近使用的颜色
        this.colorCookieName = "colorpr_";
        this.namespace = "kcolor" + $B.getIdIdx();
        var seft = this;
        this['minicolors-lastChange'] = {
            "hex": this.opts.defaultValue,
            "opacity": 1
        };
        this.$actor = $B.DomUtils.createEl("<div style='height: 8px;width:100%;top:-10px;padding-left:2px;border-bottom:2px solid #39393E;position:absolute;left:0px;'><i style='font-size:16px;color:#39393E;line-height:12px;position:relative;top:-5px' class='fa fa-up-dir'></i></div>");
        this.elObj = $B.DomUtils.createEl('<div tabindex="0" class="k_minicolors k_box_shadow k_minicolors_theme_default k_box_size" style="display:none;"/>');
        super.setElObj(this.elObj);
        $B.DomUtils.append(this.elObj,
            '<div tabindex="0" class="k_minicolors_panel  k_minicolors_slider_' + this.opts.control + '">' +
            '<div tabindex="0" class="k_minicolors_slider"><div class="k_minicolors_picker mv_picker"></div>' +
            '</div><div tabindex="0"  class="k_minicolors_opacity_slider"><div  tabindex="0"  class="k_minicolors_picker  mv_picker"></div>' +
            '</div><div  tabindex="0" class="k_minicolors_grid"><div  tabindex="0"  class="k_minicolors_grid_inner"></div>' +
            '<div tabindex="0" class="k_minicolors_picker  mv_picker"><div  tabindex="0" class="inner_mv_picker"></div></div></div></div>');
        $B.DomUtils.prepend(this.elObj, this.$actor);
        if (tgObj) {
            this.setTarget(tgObj);
        }
        this.isBodyClick = true;
        let $panel = $B.DomUtils.children(this.elObj, '.k_minicolors_panel')[0];
        var _this = this;       
        $B.DomUtils.mousedown($panel, (e) => {
            let evEl = e.target;
            let isFinded = false;
            while (evEl) {
                if ($B.DomUtils.hasClass(evEl, "k_minicolors_grid") || $B.DomUtils.hasClass(evEl, "k_minicolors_slider") || $B.DomUtils.hasClass(evEl, "k_minicolors_opacity_slider")) {
                    isFinded = true;
                    break;
                }
                evEl = evEl.parentNode;
            }
            if (isFinded) {
                $B.DomUtils.addListener(this.$doc, this.namespace + ".mousedown", (ev) => {
                    let tgEl = ev.target;
                    while (tgEl) {
                        if ($B.DomUtils.hasClass(tgEl, "k_minicolors_grid")
                            || $B.DomUtils.hasClass(tgEl, "k_minicolors_slider")
                            || $B.DomUtils.hasClass(tgEl, "k_minicolors_opacity_slider")) {
                            break;
                        }
                        tgEl = tgEl.parentNode;
                    }
                    _this.movingEl = tgEl;
                    _this.starting = true;
                });
                $B.DomUtils.addListener(this.$doc, this.namespace + ".mousemove", (ev) => {
                    if (_this.starting) {
                        _this.starting = false;
                        if (_this.opts.onStartFn) {
                            _this.opts.onStartFn();
                        }
                    }
                    let tgEl = _this.movingEl;
                    move.call(_this, tgEl, ev);
                });
                $B.DomUtils.addListener(this.$doc, this.namespace + ".mouseup", (ev) => {
                    if (!_this.starting) {
                        if (_this.opts.onEndFn) {
                            _this.opts.onEndFn();
                        }
                    }
                    _this.movingEl = undefined;
                    _this.starting = undefined;
                    $B.DomUtils.removeListener(ev.catpurer, this.namespace + ".*");
                });
            }
        });
        $B.DomUtils.mouseup($panel, (e) => {
            _this.movingEl = undefined;
            $B.DomUtils.removeListener(this.$doc, "k_color.*");
        });
        this.colorsBtnWrap = $B.DomUtils.createEl("<div  tabindex='0'  class='k_minicolors_color_buttons'></div>");
        $B.DomUtils.append(this.elObj, this.colorsBtnWrap);
        $B.DomUtils.click(this.colorsBtnWrap, (e) => {
            let el = e.target;
            while (el) {
                if ($B.DomUtils.hasClass(el, "k_color_btn")) {
                    break;
                }
                el = el.parentNode;
            }
            let color = $B.DomUtils.css(el, "background-color");
            seft.setValue(color);
            seft._callChange();
        });
        for (var i = 0, len = this.opts.buttons.length; i < len; ++i) {
            let $btn = $B.DomUtils.createEl("<div class='k_color_btn'  tabindex='0' style='background-color:" + this.opts.buttons[i] + "'></div>");
            $B.DomUtils.append(this.colorsBtnWrap, $btn);
            if (i === 0) {
                $B.DomUtils.addClass($btn, "k_mincolors_btn_nonecolor");
            }
        }       
        if (this.opts.mouseenter) {
            $B.DomUtils.mouseenter(this.elObj, this.opts.mouseenter);
        }
        this.rebuildFromCookie();
        $B.DomUtils.append(this.$doc.body, this.elObj);        
        //输入框
        let $inps = $B.DomUtils.createEl("<div tabindex='0' class='k_color_input_wap' style='width:100%;height:40px;'></div>");
        let $inpwap = $B.DomUtils.append(this.elObj, "<div tabindex='0' class='k_color_input_wap' style='width:100%;height:40px;position:absolute;left:0;bottom:0'></div>");
        let style = [];
        style.push("p{padding-left:1px;}p span{display:inline-block;width:32px;margin-left:2px;}");
        style.push("p span input{padding:0 !important;width:100%;height:18px;font-size:13px !important;line-height:18px;text-align:center;}");
        style.push(".k_label{text-align:center;height:18px;font-size:13px;line-height:18px;padding-top: 3px;}");
        $B.createTextIfr($inpwap,$inps,style);  
        
        $B.DomUtils.append($inps, "<p><span class='k_label' style='width:53px;'>Hex</span><span class='k_label'>R</span><span class='k_label'>G</span><span class='k_label'>B</span><span class='k_label'>A</span></p>");
        $B.DomUtils.append($inps, "<p><span style='width:53px;'><input spellcheck='false' id='k_color_hex' type='text'/></span><span><input id='k_color_r' type='text' /></span><span><input id='k_color_g' type='text' /></span><span><input id='k_color_b' type='text' /></span><span><input id='k_color_a' type='text' /></span></p>");
        
        this.$hex = $B.DomUtils.findbyId($inps, "k_color_hex");
        this.$r = $B.DomUtils.findbyId($inps, "k_color_r");
        this.$g = $B.DomUtils.findbyId($inps, "k_color_g");
        this.$b = $B.DomUtils.findbyId($inps, "k_color_b");
        this.$a = $B.DomUtils.findbyId($inps, "k_color_a");
     
        this._bindInputEvents($inps);
        updateFromInput.call(this, this['minicolors-lastChange']);
        this._updateInputs();
        $B.DomUtils.addListener(this.$doc, this.namespace + "g.click", (ev) => {
            if (!this._skiphide) {
                let e = ev.target;
                let helper = 8;
                let go2hide = true;
                if (e.nodeName !== "BODY") {
                    while (helper > 0 && e) {
                        if (e.nodeName === "BODY") {
                            break;
                        }
                        if ($B.DomUtils.hasClass(e, "k_minicolors") || $B.DomUtils.hasClass(e, "k_color_input_wap")) {
                            go2hide = false;
                            break;
                        }
                        e = e.parentNode;
                        helper--;
                    }
                }
                if (go2hide) {
                    this.hide();
                }
            }
        });
        if(this.opts.mouseleaveHide){
            $B.DomUtils.mouseleave(this.elObj,(e)=>{                
                this.hide();
            });
        }
    }
    rebuildFromCookie(){
        if(this.target){
            //从cookie恢复最近使用的5个颜色
            let ckey = this.colorCookieName +  this.target.id;
            var tmp = $B.getCookie(ckey);                   
            if (tmp !== "") {
                var colors = tmp.split(",");
                this.lastColors = colors;
                this._setLastColors();
            }
        }
    }
    _callChange() {
        if (this.opts.onChange) {
            let v = this['minicolors-lastChange'];
            this.opts.onChange.call(this.target, v.hex, v.opacity);
        }
    }
    _bindInputEvents(el) {
        if (!this.inputEvents) {
            this.inputEvents = {
                focus: (e) => {
                    console.log("focus", e.target);
                },
                input: (e) => {                   
                    clearTimeout(this.inputingTimer);
                    this.inputingTimer = setTimeout(() => {
                        let $in = e.target;
                        $B.DomUtils.removeClass(this.$hex, "k_input_value_err");
                        $B.DomUtils.removeClass(this.$r, "k_input_value_err");
                        $B.DomUtils.removeClass(this.$g, "k_input_value_err");
                        $B.DomUtils.removeClass(this.$b, "k_input_value_err");
                        $B.DomUtils.removeClass(this.$a, "k_input_value_err");
                        let r = parseInt(this.$r.value);
                        let g = parseInt(this.$g.value);
                        let b = parseInt(this.$b.value);
                        let a = parseFloat(this.$a.value);
                        let isErr = false;
                        if (r === NaN || r < 0 || r > 255) {
                            $B.DomUtils.addClass(this.$r, "k_input_value_err");
                            isErr = true;
                        }
                        if (g === NaN || g < 0 || g > 255) {
                            $B.DomUtils.addClass(this.$g, "k_input_value_err");
                            isErr = true;
                        }
                        if (b === NaN || b < 0 || b > 255) {
                            $B.DomUtils.addClass(this.$b, "k_input_value_err");
                            isErr = true;
                        }
                        if (a === NaN || a < 0 || a > 1) {
                            $B.DomUtils.addClass(this.$a, "k_input_value_err");
                            isErr = true;
                        }
                        if (!isErr) {
                            if ($in.id === "k_color_hex") {
                                if ($in.value.length === 6) {
                                    try {
                                        let v = "#" + $in.value;
                                        this.setValue(v, this['minicolors-lastChange'].opacity);
                                        this._callChange();
                                    } catch (x) {
                                        $B.DomUtils.addClass(this.$hex, "k_input_value_err");
                                        console.log(x);
                                    }
                                } else {
                                    $B.DomUtils.addClass(this.$hex, "k_input_value_err");
                                }
                            } else if ($in.id === "k_color_a") {
                                if ($in.value !== "") {
                                    this.setValue(this['minicolors-lastChange'].hex, $in.value);
                                    this._callChange();
                                }
                            } else {
                                try {
                                    let rgb = "rgb(" + r + "," + g + "," + b + ")";
                                    let hexVal = $B.rgb2Hex(rgb);
                                    this.setValue(hexVal, this['minicolors-lastChange'].opacity);
                                    this._callChange();
                                } catch (e1) {
                                    console.log(e1);
                                }
                            }
                        }
                    }, 500);
                }
            };
        }
        $B.DomUtils.bind(el, this.inputEvents);
    }
    _updateInputs() {
        var val = this['minicolors-lastChange'];
        this.$hex.value = val.hex.replace("#", "").toUpperCase();
        var rgbObj = $B.hex2RgbObj(val.hex);
        this.$r.value = rgbObj.r;
        this.$g.value = rgbObj.g;
        this.$b.value = rgbObj.b;
        this.$a.value = val.opacity;
    }
    setTarget(elObj) {
        if (typeof elObj === "string") {
            elObj = this.$doc.getElementById(elObj);
        }
        if (this.target && this.target !== elObj) {
            this._setLastColors();
            this.unbindTarget();            
        }
        this.target = elObj;
        this.rebuildFromCookie();
        if (!$B.DomUtils.hasClass(this.target, "k_color_picker_cls")) {
            $B.DomUtils.css(this.target, "background-image", "none");
            $B.DomUtils.addClass(this.target, "k_color_picker_cls");
            if(!$B.DomUtils.attribute(this.target,"unbindcolorev")){
                $B.DomUtils.addListener(this.target, this.namespace + ".click", (e) => {
                    if (this.elObj.style.display === "none") {
                        this.show();
                    } else {
                        this.hide();
                    }
                    return false;
                });
            }           
        }
        return this;
    }
    unbindTarget() {
        if (this.target) {
            $B.DomUtils.offEvents(this.target, this.namespace + ".click");
            $B.DomUtils.removeClass(this.target, "k_color_picker_cls");
            this.target = undefined;
        }
        return this;
    }
    setPosition(ofs) {
        let tH = $B.DomUtils.outerHeight(this.target) + 6;
        ofs.top = ofs.top + tH;
        let bodyW = $B.DomUtils.width(this.$doc.body);
        let bodyH = $B.DomUtils.height(this.$doc.body);
        if ((ofs.top + 250) > bodyH) {
            ofs.top = ofs.top - 250 - tH + 2;
            this.$actor.style.top = "231px";
            $B.DomUtils.css(this.$actor.firstChild, { top: "1px", transform: "rotateZ(180deg)" });
        } else {
            this.$actor.style.top = "-10px";
            $B.DomUtils.css(this.$actor.firstChild, { top: "-5px", transform: "rotateZ(0deg)" });
        }
        let w = ofs.left + 198;
        if (w > bodyW) {
            let diff = w - bodyW;
            ofs.left = ofs.left - diff;
            this.$actor.firstChild.style.left = diff + "px";
        } else {
            this.$actor.firstChild.style.left = "0px";
        }
        this.elObj.style.top = ofs.top + "px";
        this.elObj.style.left = ofs.left + "px";
        this.elObj.style.display = "block";
        return this;
    }    
    hide(isUnbind) {
        var _this = this;
        clearTimeout(this["hidetimer"]);
        if (isUnbind) {
            _this.unbindTarget();
        }
        if (_this.opts.onHideFn) {
            setTimeout(() => {
                _this.opts.onHideFn(_this);
            }, 1);
        }
        _this.elObj.style.display = "none";
        this._setLastColors();       
        return this;
    }
    saveHisColor() {
        clearTimeout(this.hisRecoderTimer);
        this.hisRecoderTimer = setTimeout(() => {
            this._setLastColors();
        }, 1500);
    }
    /**设置最近使用颜色**/
    _setLastColors() {
        var currentColor = this["minicolors-lastChange"].hex;
        this.lastColors.unshift(currentColor);
        this.lastColors = Array.from(new Set(this.lastColors));
        if (this.lastColors.length > this.opts.record) {
            this.lastColors = this.lastColors.slice(0 ,this.opts.record );
        }        
        var lastBtn = this.colorsBtnWrap.lastChild;
        for (let i = 0, len = this.lastColors.length; i < len; ++i) {
            $B.DomUtils.css(lastBtn, { "background-color": this.lastColors[i] });
            lastBtn = lastBtn.previousSibling;
        }
        var lastColorstr = this.lastColors.join(",");
        let ckey = this.colorCookieName ;
        if(this.target){
            ckey = ckey + this.target.id;
        }
        $B.writeCookie(ckey, lastColorstr, 3);
    }
    show(target,shift) {       
        if (this.target && target && this.target === target) {
            if(this.isShow()){
                return;
            }           
        }
        var _this = this;
        if (target) {
            _this.setTarget(target);
        }
        var ofs = $B.DomUtils.offset(_this.target);
        if(shift){
            if(shift.top){
                ofs.top = ofs.top + shift.top;
            }
            if(shift.left){
                ofs.left = ofs.left + shift.left;
            }
        }
        this._skiphide = true;
        _this.setPosition(ofs);
        setTimeout(() => {
            this._skiphide = undefined;
        }, 1);
        return this;
    }
    isShow() {
        var dis = $B.DomUtils.css(this.elObj, "display");
        return dis === "block";
    }
    setValue(value, opacity) {
        this._setLastColors();
        try {
            if (value.toLowerCase().indexOf("rgb") > -1) {
                value = $B.rgb2Hex(value);
            } else {
                $B.hex2RgbObj(value);
            }
            if (typeof opacity !== "undefined") {
                opacity = parseFloat(opacity);
            }
            if (opacity === undefined || opacity === NaN) {
                opacity = this['minicolors-lastChange'].opacity;
            }
        } catch (e) {
            console.log(e);
            return;
        }
        doChange.call(this, value, opacity);
        value = {
            "hex": value,
            "opacity": opacity
        };
        updateFromInput.call(this, value);
        return this;
    }
    getValue() {
        return this['minicolors-lastChange'];
    }
    destroy() {
        $B.DomUtils.offEvents(this.$doc, this.namespace + "g.click");
        this.$doc = undefined;
        this.unbindTarget();
        $B.DomUtils.remove(this.elObj);
        super.destroy();
    }
}
$B["Color"] = Color; 
 if(!window["$B"] ) {window["$B"] = $B;} 
  return window["$B"] ; 
});

