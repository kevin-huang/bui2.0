var toolbars = [{
    params: {
        pr1: 12345678
    }, 
    disabled: false, //是否禁用
    text: '按钮3', //文本
    click: function (pr) { //点击事件，如果存在click，则字符串形式的handler不可用
        alert(JSON.stringify(pr));
    }
},
{
    params: {
        pr1: 123,
        p1: 456
    },
    disabled: false, //是否禁用
    text: '按钮4', //文本
    click: 'lineDetailFn', //window.methodsObject对象中 test
    methodsObject: 'toolMethods'
}
];
var treeData1 = [{
    "id": "2cc7b22456736e8001567377314b0000",
    "text": "我的项目",
    "toolbars": toolbars,
    "data": {
        "controller": "/action/form",
        "createTime": "2016-08-10 16:00:06",
        "depth": 0,
        "id": "2cc7b22456736e8001567377314b0000",
        "isEnable": true,
        "isLeaf": true,
        "isVisible": true,
        "menuIconCss": "menu_mainpage",
        "menuText": "我的项目",
        "orderIndex": 0,
        "pageName": "index",
        "pid": "0"
    },
    "pid": "0"
},
{
    "id": "2cc7b22456736e80015673a67e570002",
    "text": "项目管理",
    "toolbars": toolbars,
    "data": {
        "controller": "/action/formmgr",
        "createTime": "2016-08-10 16:51:46",
        "depth": 0,
        "id": "2cc7b22456736e80015673a67e570002",
        "isEnable": true,
        "isLeaf": true,
        "isVisible": true,
        "menuIconCss": "menu_control",
        "menuText": "项目管理",
        "orderIndex": 4,
        "pageName": "index",
        "params": "gridid=form",
        "pid": "0"
    },
    "pid": "0"
},
{
    "id": "2cc7b22456736e80015673a72a0c0003",
    "text": "页面管理",
    "toolbars": toolbars,
    "data": {
        "controller": "/action/template",
        "createTime": "2016-08-10 16:52:30",
        "depth": 0,
        "id": "2cc7b22456736e80015673a72a0c0003",
        "isEnable": true,
        "isLeaf": true,
        "isVisible": true,
        "menuIconCss": "menu_work",
        "menuText": "页面管理",
        "orderIndex": 6,
        "pageName": "index",
        "pid": "0"
    },
    "pid": "0"
},
{
    "id": "ff80808156e6944e0156fb3cadc10089",
    "text": "控件管理",
    "toolbars": toolbars,
    "data": {
        "controller": "/action/",
        "depth": 0,
        "id": "ff80808156e6944e0156fb3cadc10089",
        "isEnable": true,
        "isLeaf": true,
        "isVisible": true,
        "menuIconCss": "menu_attr",
        "menuText": "控件管理",
        "orderIndex": 7,
        "pid": "0"
    },
    "children": [{
            "id": "2cc7b2f05caf1733015caf1aa1a10000",
            "text": "控件数据管理",
            "toolbars": toolbars,
            "data": {
                "controller": "/action/component",
                "createTime": "2017-06-16 12:12:57",
                "id": "2cc7b2f05caf1733015caf1aa1a10000",
                "isEnable": true,
                "isLeaf": true,
                "isVisible": true,
                "menuText": "控件数据管理",
                "orderIndex": 1,
                "pageName": "index",
                "params": "gridid=component",
                "pid": "ff80808156e6944e0156fb3cadc10089"
            },
            "pid": "ff80808156e6944e0156fb3cadc10089"
        },
        {
            "id": "2cc7b2f05d830e36015d8315045f0000",
            "text": "控件分类管理",
            "toolbars": toolbars,
            "data": {
                "controller": "/action/componentgroup",
                "createTime": "2017-07-27 16:06:18",
                "id": "2cc7b2f05d830e36015d8315045f0000",
                "isEnable": true,
                "isLeaf": true,
                "isVisible": true,
                "menuText": "控件分类管理",
                "orderIndex": 2,
                "params": "gridid=componentgroup",
                "pid": "ff80808156e6944e0156fb3cadc10089"
            },
            "pid": "ff80808156e6944e0156fb3cadc10089"
        },
        {
            "id": "2cc7b2f05caf1733015caf1b2b320002",
            "text": "资源管理",
            "toolbars": toolbars,
            "closed": false,
            "data": {
                "controller": "/action/resource",
                "createTime": "2017-06-16 12:13:32",
                "id": "2cc7b2f05caf1733015caf1b2b320002",
                "isEnable": true,
                "isLeaf": true,
                "isVisible": true,
                "menuText": "资源管理",
                "orderIndex": 4,
                "params": "gridid=resource",
                "pid": "ff80808156e6944e0156fb3cadc10089"
            },
            "pid": "ff80808156e6944e0156fb3cadc10089",
            "children": [{
                "id": "2cc7b22456736e80015673a89a210004",
                "text": "系统管理",
                "toolbars": toolbars,
                "closed": true,
                "data": {
                    "createTime": "2016-08-10 16:54:04",
                    "depth": 0,
                    "id": "2cc7b22456736e80015673a89a210004",
                    "isEnable": true,
                    "isLeaf": false,
                    "isVisible": true,
                    "menuIconCss": "menu_opts",
                    "menuText": "系统管理",
                    "orderIndex": 8,
                    "pid": "0"
                },
                "children": [{
                        "id": "1",
                        "text": "权限管理",
                        "toolbars": toolbars,
                        "closed": true,
                        "data": {
                            "createTime": "2016-09-12 18:15:20",
                            "depth": 0,
                            "id": "1",
                            "isEnable": true,
                            "isLeaf": false,
                            "isVisible": true,
                            "menuText": "权限管理",
                            "orderIndex": 9,
                            "pid": "2cc7b22456736e80015673a89a210004"
                        },
                        "children": [{
                                "id": "23",
                                "text": "用户管理",
                                "toolbars": toolbars,
                                "data": {
                                    "controller": "/base/user",
                                    "createTime": "2016-06-27 20:16:30",
                                    "depth": 1,
                                    "id": "23",
                                    "isEnable": true,
                                    "isLeaf": true,
                                    "isVisible": true,
                                    "menuIconCss": "menu_users",
                                    "menuText": "用户管理",
                                    "orderIndex": 1,
                                    "params": "gridid=user",
                                    "pid": "1"
                                },
                                "pid": "1"
                            },
                            {
                                "id": "24",
                                "text": "角色管理",
                                "toolbars": toolbars,
                                "data": {
                                    "controller": "/base/role",
                                    "createTime": "2016-06-27 20:16:30",
                                    "depth": 1,
                                    "id": "24",
                                    "isEnable": true,
                                    "isLeaf": true,
                                    "isVisible": true,
                                    "menuIconCss": "menu_users",
                                    "menuText": "角色管理",
                                    "orderIndex": 2,
                                    "params": "gridid=role",
                                    "pid": "1"
                                },
                                "pid": "1"
                            },
                            {
                                "id": "25",
                                "text": "功能管理",
                                "toolbars": toolbars,
                                "data": {
                                    "controller": "/base/func",
                                    "createTime": "2016-06-27 20:16:30",
                                    "depth": 1,
                                    "id": "25",
                                    "isEnable": true,
                                    "isLeaf": true,
                                    "isVisible": true,
                                    "menuIconCss": "menu_controller",
                                    "menuText": "功能管理",
                                    "orderIndex": 3,
                                    "params": "gridid=func",
                                    "pid": "1"
                                },
                                "pid": "1"
                            },
                            {
                                "id": "10",
                                "text": "菜单管理",
                                "toolbars": toolbars,
                                "data": {
                                    "controller": "/base/menu",
                                    "createTime": "2016-06-27 20:16:30",
                                    "depth": 1,
                                    "id": "10",
                                    "isEnable": true,
                                    "isLeaf": true,
                                    "isVisible": true,
                                    "menuIconCss": "menu_settings",
                                    "menuText": "菜单管理",
                                    "orderIndex": 4,
                                    "params": "gridid=menu",
                                    "pid": "1"
                                },
                                "pid": "1"
                            }
                        ],
                        "pid": "2cc7b22456736e80015673a89a210004"
                    },
                    {
                        "id": "2cc7b224555216e801555218afdd0000",
                        "text": "系统字典管理",
                        "toolbars": toolbars,
                        "data": {
                            "controller": "/base/dict",
                            "createTime": "2016-06-27 20:16:30",
                            "depth": 1,
                            "id": "2cc7b224555216e801555218afdd0000",
                            "isEnable": true,
                            "isLeaf": true,
                            "isVisible": true,
                            "menuText": "系统字典管理",
                            "orderIndex": 10,
                            "params": "gridid=dict",
                            "pid": "2cc7b22456736e80015673a89a210004"
                        },
                        "pid": "2cc7b22456736e80015673a89a210004"
                    }
                ],
                "pid": "0"
            }]
        },
        {
            "id": "2cc7b2f05caf1733015caf1ae6f80001",
            "text": "属性管理",
            "toolbars": toolbars,
            "data": {
                "controller": "/action/attribute",
                "createTime": "2017-06-16 12:13:14",
                "id": "2cc7b2f05caf1733015caf1ae6f80001",
                "isEnable": true,
                "isLeaf": true,
                "isVisible": true,
                "menuText": "属性管理",
                "orderIndex": 3,
                "params": "gridid=action-attribute",
                "pid": "ff80808156e6944e0156fb3cadc10089"
            },
            "pid": "ff80808156e6944e0156fb3cadc10089"
        },
        {
            "id": "2cc7b2f05cf2189b015cf219fac70000",
            "text": "值内容管理",
            "toolbars": toolbars,
            "data": {
                "controller": "/action/meta",
                "createTime": "2017-06-29 12:26:47",
                "id": "2cc7b2f05cf2189b015cf219fac70000",
                "isEnable": true,
                "isLeaf": true,
                "isVisible": true,
                "menuText": "值内容管理",
                "orderIndex": 5,
                "params": "gridid=meta",
                "pid": "ff80808156e6944e0156fb3cadc10089"
            },
            "children": [{
                "id": "2cc7b2f05d9ca329015d9ce70caf0000",
                "text": "数据绑定测试",
                "toolbars": toolbars,
                "data": {
                    "controller": "/action/form",
                    "createTime": "2017-08-01 16:26:13",
                    "id": "2cc7b2f05d9ca329015d9ce70caf0000",
                    "isEnable": true,
                    "isLeaf": true,
                    "isVisible": true,
                    "menuText": "数据绑定测试",
                    "orderIndex": 6,
                    "pageName": "databind",
                    "params": "page=databind",
                    "pid": "2cc7b2f05cf2189b015cf219fac70000"
                },
                "pid": "2cc7b2f05cf2189b015cf219fac70000"
            }],
            "pid": "ff80808156e6944e0156fb3cadc10089"
        },
        {
            "id": "2cc7b2f05d9ca329015d9ce84faf0001",
            "text": "数据绑定测试",
            "toolbars": toolbars,
            "data": {
                "controller": "/action/databind",
                "createTime": "2017-08-01 16:27:36",
                "id": "2cc7b2f05d9ca329015d9ce84faf0001",
                "isEnable": true,
                "isLeaf": true,
                "isVisible": true,
                "menuText": "数据绑定测试",
                "orderIndex": 6,
                "pid": "ff80808156e6944e0156fb3cadc10089"
            },
            "pid": "ff80808156e6944e0156fb3cadc10089"
        }
    ],
    "pid": "0"
},
{
    "id": "2cc7b22456736e80015673a89a210004",
    "text": "系统管理",
    "toolbars": toolbars,
    "data": {
        "createTime": "2016-08-10 16:54:04",
        "depth": 0,
        "id": "2cc7b22456736e80015673a89a210004",
        "isEnable": true,
        "isLeaf": false,
        "isVisible": true,
        "menuIconCss": "menu_opts",
        "menuText": "系统管理",
        "orderIndex": 8,
        "pid": "0"
    },
    "children": [{
            "id": "1",
            "text": "权限管理",
            "toolbars": toolbars,
            "data": {
                "createTime": "2016-09-12 18:15:20",
                "depth": 0,
                "id": "1",
                "isEnable": true,
                "isLeaf": false,
                "isVisible": true,
                "menuText": "权限管理",
                "orderIndex": 9,
                "pid": "2cc7b22456736e80015673a89a210004"
            },
            "children": [{
                    "id": "23",
                    "text": "用户管理",
                    "toolbars": toolbars,
                    "data": {
                        "controller": "/base/user",
                        "createTime": "2016-06-27 20:16:30",
                        "depth": 1,
                        "id": "23",
                        "isEnable": true,
                        "isLeaf": true,
                        "isVisible": true,
                        "menuIconCss": "menu_users",
                        "menuText": "用户管理",
                        "orderIndex": 1,
                        "params": "gridid=user",
                        "pid": "1"
                    },
                    "pid": "1"
                },
                {
                    "id": "24",
                    "text": "角色管理",
                    "toolbars": toolbars,
                    "data": {
                        "controller": "/base/role",
                        "createTime": "2016-06-27 20:16:30",
                        "depth": 1,
                        "id": "24",
                        "isEnable": true,
                        "isLeaf": true,
                        "isVisible": true,
                        "menuIconCss": "menu_users",
                        "menuText": "角色管理",
                        "orderIndex": 2,
                        "params": "gridid=role",
                        "pid": "1"
                    },
                    "pid": "1"
                },
                {
                    "id": "25",
                    "text": "功能管理",
                    "toolbars": toolbars,
                    "data": {
                        "controller": "/base/func",
                        "createTime": "2016-06-27 20:16:30",
                        "depth": 1,
                        "id": "25",
                        "isEnable": true,
                        "isLeaf": true,
                        "isVisible": true,
                        "menuIconCss": "menu_controller",
                        "menuText": "功能管理",
                        "orderIndex": 3,
                        "params": "gridid=func",
                        "pid": "1"
                    },
                    "pid": "1"
                },
                {
                    "id": "10",
                    "text": "菜单管理",
                    "toolbars": toolbars,
                    "data": {
                        "controller": "/base/menu",
                        "createTime": "2016-06-27 20:16:30",
                        "depth": 1,
                        "id": "10",
                        "isEnable": true,
                        "isLeaf": true,
                        "isVisible": true,
                        "menuIconCss": "menu_settings",
                        "menuText": "菜单管理",
                        "orderIndex": 4,
                        "params": "gridid=menu",
                        "pid": "1"
                    },
                    "pid": "1"
                }
            ],
            "closed": true,
            "pid": "2cc7b22456736e80015673a89a210004"
        },
        {
            "id": "2cc7b224555216e801555218afdd0000",
            "text": "系统字典管理",
            "toolbars": toolbars,
            "data": {
                "controller": "/base/dict",
                "createTime": "2016-06-27 20:16:30",
                "depth": 1,
                "id": "2cc7b224555216e801555218afdd0000",
                "isEnable": true,
                "isLeaf": true,
                "isVisible": true,
                "menuText": "系统字典管理",
                "orderIndex": 10,
                "params": "gridid=dict",
                "pid": "2cc7b22456736e80015673a89a210004"
            },
            "pid": "2cc7b22456736e80015673a89a210004"
        }
    ],
    "closed": true,
    "pid": "0"
}
];

var data = {
"resultList": [{
        "toolbar": [{
                "id": "1",
                "iconCls": "fa-send-o",
                "disabled": false,
                "text": "测试",
                "methodsObject": "toolMethods",
                "click": "lineTestFn"
            },
            {
                "id": "2",
                "iconCls": "fa-trash",
                "disabled": true,
                "text": "删除",
                "methodsObject": "toolMethods",
                "click": "lineDleteFn"
            },
            {
                "id": "line_btn_0",
                "cmd": "list",
                "text": "详情",
                "methodsObject": "toolMethods",
                "params": [{
                        "cmd": "list"
                    },
                    {
                        "privilage": "1"
                    }
                ],
                "visualable": true,
                "disabled": false,
                "iconCls": "fa-doc-text",
                "click": "lineDetailFn"
            }
        ],
        "createTime": "2017-06-09 16:26:58",
        "status": 1,
        "actionName": "uploadTestData",
        "content": "编辑流程",
        "id": "ff8080815c8aa869015c8bf6ad1a4a4c",
        "operateip": "172.86.50.5",
        "cmd": "update",
        "createUser": "超级管理员",
        "extFeildsMap": {
            "ROWNUM_": 31
        },
        "controller": "/idp/proflow",
        "menuName": "项目流程管理"
    },
    {
        "toolbar": [{
                "id": "1",
                "iconCls": "fa-send-o",
                "disabled": false,
                "text": "测试",
                "methodsObject": "toolMethods",
                "click": "lineTestFn"
            },
            {
                "id": "2",
                "iconCls": "fa-trash",
                "disabled": true,
                "text": "删除",
                "methodsObject": "toolMethods",
                "click": "lineDleteFn"
            },
            {
                "id": "line_btn_0",
                "cmd": "list",
                "text": "详情",
                "methodsObject": "toolMethods",
                "params": [{
                        "cmd": "list"
                    },
                    {
                        "privilage": "1"
                    }
                ],
                "visualable": true,
                "disabled": false,
                "iconCls": "fa-doc-text",
                "click": "lineDetailFn"
            }
        ],
        "createTime": "2017-06-09 16:26:58",
        "status": 1,
        "actionName": "uploadTestData",
        "content": "编辑流程",
        "id": "ff8080815c8aa869015c8bf6ad1a4a4c",
        "operateip": "172.86.50.5",
        "cmd": "update",
        "createUser": "超级管理员",
        "extFeildsMap": {
            "ROWNUM_": 31
        },
        "controller": "/idp/proflow",
        "menuName": "项目流程管理"
    },
    {
        "toolbar": [{
                "id": "1",
                "iconCls": "fa-send-o",
                "disabled": false,
                "text": "测试",
                "methodsObject": "toolMethods",
                "click": "lineTestFn"
            },
            {
                "id": "2",
                "iconCls": "fa-trash",
                "disabled": true,
                "text": "删除",
                "methodsObject": "toolMethods",
                "click": "lineDleteFn"
            },
            {
                "id": "line_btn_0",
                "cmd": "list",
                "text": "详情",
                "methodsObject": "toolMethods",
                "params": [{
                        "cmd": "list"
                    },
                    {
                        "privilage": "1"
                    }
                ],
                "visualable": true,
                "disabled": false,
                "iconCls": "fa-doc-text",
                "click": "lineDetailFn"
            }
        ],
        "createTime": "2017-06-09 16:26:58",
        "status": 1,
        "actionName": "uploadTestData",
        "content": "编辑流程",
        "id": "ff8080815c8aa869015c8bf6ad1a4a4c",
        "operateip": "172.86.50.5",
        "cmd": "update",
        "createUser": "超级管理员",
        "extFeildsMap": {
            "ROWNUM_": 31
        },
        "controller": "/idp/proflow",
        "menuName": "项目流程管理"
    },
    {
        "toolbar": [{
                "id": "1",
                "iconCls": "fa-send-o",
                "disabled": false,
                "text": "测试",
                "methodsObject": "toolMethods",
                "click": "lineTestFn"
            },
            {
                "id": "2",
                "iconCls": "fa-trash",
                "disabled": true,
                "text": "删除",
                "methodsObject": "toolMethods",
                "click": "lineDleteFn"
            },
            {
                "id": "line_btn_0",
                "cmd": "list",
                "text": "详情",
                "methodsObject": "toolMethods",
                "params": [{
                        "cmd": "list"
                    },
                    {
                        "privilage": "1"
                    }
                ],
                "visualable": true,
                "disabled": false,
                "iconCls": "fa-doc-text",
                "click": "lineDetailFn"
            }
        ],
        "createTime": "2017-06-09 16:26:58",
        "status": 1,
        "actionName": "uploadTestData",
        "content": "编辑流程",
        "id": "ff8080815c8aa869015c8bf6ad1a4a4c",
        "operateip": "172.86.50.5",
        "cmd": "update",
        "createUser": "超级管理员",
        "extFeildsMap": {
            "ROWNUM_": 31
        },
        "controller": "/idp/proflow",
        "menuName": "项目流程管理"
    },
    {
        "toolbar": [{
                "id": "1",
                "iconCls": "fa-send-o",
                "disabled": false,
                "text": "测试",
                "methodsObject": "toolMethods",
                "click": "lineTestFn"
            },
            {
                "id": "2",
                "iconCls": "fa-trash",
                "disabled": true,
                "text": "删除",
                "methodsObject": "toolMethods",
                "click": "lineDleteFn"
            },
            {
                "id": "line_btn_0",
                "cmd": "list",
                "text": "详情",
                "methodsObject": "toolMethods",
                "params": [{
                        "cmd": "list"
                    },
                    {
                        "privilage": "1"
                    }
                ],
                "visualable": true,
                "disabled": false,
                "iconCls": "fa-doc-text",
                "click": "lineDetailFn"
            }
        ],
        "createTime": "2017-06-09 16:26:58",
        "status": 1,
        "actionName": "uploadTestData",
        "content": "编辑流程",
        "id": "ff8080815c8aa869015c8bf6ad1a4a4c",
        "operateip": "172.86.50.5",
        "cmd": "update",
        "createUser": "超级管理员",
        "extFeildsMap": {
            "ROWNUM_": 31
        },
        "controller": "/idp/proflow",
        "menuName": "项目流程管理"
    },
    {
        "toolbar": [{
                "id": "1",
                "iconCls": "fa-send-o",
                "disabled": false,
                "text": "测试",
                "methodsObject": "toolMethods",
                "click": "lineTestFn"
            },
            {
                "id": "2",
                "iconCls": "fa-trash",
                "disabled": true,
                "text": "删除",
                "methodsObject": "toolMethods",
                "click": "lineDleteFn"
            },
            {
                "id": "line_btn_0",
                "cmd": "list",
                "text": "详情",
                "methodsObject": "toolMethods",
                "params": [{
                        "cmd": "list"
                    },
                    {
                        "privilage": "1"
                    }
                ],
                "visualable": true,
                "disabled": false,
                "iconCls": "fa-doc-text",
                "click": "lineDetailFn"
            }
        ],
        "createTime": "2017-06-09 16:26:58",
        "status": 1,
        "actionName": "uploadTestData",
        "content": "编辑流程",
        "id": "ff8080815c8aa869015c8bf6ad1a4a4c",
        "operateip": "172.86.50.5",
        "cmd": "update",
        "createUser": "超级管理员",
        "extFeildsMap": {
            "ROWNUM_": 31
        },
        "controller": "/idp/proflow",
        "menuName": "项目流程管理"
    },
    {
        "toolbar": [{
                "id": "1",
                "iconCls": "fa-send-o",
                "disabled": false,
                "text": "测试",
                "methodsObject": "toolMethods",
                "click": "lineTestFn"
            },
            {
                "id": "2",
                "iconCls": "fa-trash",
                "disabled": true,
                "text": "删除",
                "methodsObject": "toolMethods",
                "click": "lineDleteFn"
            },
            {
                "id": "line_btn_0",
                "cmd": "list",
                "text": "详情",
                "methodsObject": "toolMethods",
                "params": [{
                        "cmd": "list"
                    },
                    {
                        "privilage": "1"
                    }
                ],
                "visualable": true,
                "disabled": false,
                "iconCls": "fa-doc-text",
                "click": "lineDetailFn"
            }
        ],
        "createTime": "2017-06-09 16:26:58",
        "status": 1,
        "actionName": "uploadTestData",
        "content": "编辑流程",
        "id": "ff8080815c8aa869015c8bf6ad1a4a4c",
        "operateip": "172.86.50.5",
        "cmd": "update",
        "createUser": "超级管理员",
        "extFeildsMap": {
            "ROWNUM_": 31
        },
        "controller": "/idp/proflow",
        "menuName": "项目流程管理"
    },
    {
        "toolbar": [{
                "id": "1",
                "iconCls": "fa-send-o",
                "disabled": false,
                "text": "测试",
                "methodsObject": "toolMethods",
                "click": "lineTestFn"
            },
            {
                "id": "2",
                "iconCls": "fa-trash",
                "disabled": true,
                "text": "删除",
                "methodsObject": "toolMethods",
                "click": "lineDleteFn"
            },
            {
                "id": "line_btn_0",
                "cmd": "list",
                "text": "详情",
                "methodsObject": "toolMethods",
                "params": [{
                        "cmd": "list"
                    },
                    {
                        "privilage": "1"
                    }
                ],
                "visualable": true,
                "disabled": false,
                "iconCls": "fa-doc-text",
                "click": "lineDetailFn"
            }
        ],
        "createTime": "2017-06-09 16:26:58",
        "status": 1,
        "actionName": "uploadTestData",
        "content": "编辑流程",
        "id": "ff8080815c8aa869015c8bf6ad1a4a4c",
        "operateip": "172.86.50.5",
        "cmd": "update",
        "createUser": "超级管理员",
        "extFeildsMap": {
            "ROWNUM_": 31
        },
        "controller": "/idp/proflow",
        "menuName": "项目流程管理"
    },
    {
        "toolbar": [{
                "id": "1",
                "iconCls": "fa-send-o",
                "disabled": false,
                "text": "测试",
                "methodsObject": "toolMethods",
                "click": "lineTestFn"
            },
            {
                "id": "2",
                "iconCls": "fa-trash",
                "disabled": true,
                "text": "删除",
                "methodsObject": "toolMethods",
                "click": "lineDleteFn"
            },
            {
                "id": "line_btn_0",
                "cmd": "list",
                "text": "详情",
                "methodsObject": "toolMethods",
                "params": [{
                        "cmd": "list"
                    },
                    {
                        "privilage": "1"
                    }
                ],
                "visualable": true,
                "disabled": false,
                "iconCls": "fa-doc-text",
                "click": "lineDetailFn"
            }
        ],
        "createTime": "2017-06-09 16:26:58",
        "status": 1,
        "actionName": "uploadTestData",
        "content": "编辑流程",
        "id": "ff8080815c8aa869015c8bf6ad1a4a4c",
        "operateip": "172.86.50.5",
        "cmd": "update",
        "createUser": "超级管理员",
        "extFeildsMap": {
            "ROWNUM_": 31
        },
        "controller": "/idp/proflow",
        "menuName": "项目流程管理"
    },
    {
        "toolbar": [{
                "id": "1",
                "iconCls": "fa-send-o",
                "disabled": false,
                "text": "测试",
                "methodsObject": "toolMethods",
                "click": "lineTestFn"
            },
            {
                "id": "2",
                "iconCls": "fa-trash",
                "disabled": true,
                "text": "删除",
                "methodsObject": "toolMethods",
                "click": "lineDleteFn"
            },
            {
                "id": "line_btn_0",
                "cmd": "list",
                "text": "详情",
                "methodsObject": "toolMethods",
                "params": [{
                        "cmd": "list"
                    },
                    {
                        "privilage": "1"
                    }
                ],
                "visualable": true,
                "disabled": false,
                "iconCls": "fa-doc-text",
                "click": "lineDetailFn"
            }
        ],
        "createTime": "2017-06-09 16:26:58",
        "status": 1,
        "actionName": "uploadTestData",
        "content": "编辑流程",
        "id": "ff8080815c8aa869015c8bf6ad1a4a4c",
        "operateip": "172.86.50.5",
        "cmd": "update",
        "createUser": "超级管理员",
        "extFeildsMap": {
            "ROWNUM_": 31
        },
        "controller": "/idp/proflow",
        "menuName": "项目流程管理"
    },
    {
        "toolbar": [{
                "id": "1",
                "iconCls": "fa-send-o",
                "disabled": false,
                "text": "测试",
                "methodsObject": "toolMethods",
                "click": "lineTestFn"
            },
            {
                "id": "2",
                "iconCls": "fa-trash",
                "disabled": true,
                "text": "删除",
                "methodsObject": "toolMethods",
                "click": "lineDleteFn"
            },
            {
                "id": "line_btn_0",
                "cmd": "list",
                "text": "详情",
                "methodsObject": "toolMethods",
                "params": [{
                        "cmd": "list"
                    },
                    {
                        "privilage": "1"
                    }
                ],
                "visualable": true,
                "disabled": false,
                "iconCls": "fa-doc-text",
                "click": "lineDetailFn"
            }
        ],
        "createTime": "2017-06-09 16:26:58",
        "status": 1,
        "actionName": "uploadTestData",
        "content": "编辑流程",
        "id": "ff8080815c8aa869015c8bf6ad1a4a4c",
        "operateip": "172.86.50.5",
        "cmd": "update",
        "createUser": "超级管理员",
        "extFeildsMap": {
            "ROWNUM_": 31
        },
        "controller": "/idp/proflow",
        "menuName": "项目流程管理"
    }
],
"currentPage": 1,
"totalSize": 100
};



function getTreeData(rootLoopCount, maxDeep) {
var treeData = [];
var nodeCount = 0;
var limitDeep = 5;

    function loopMakeChilds(childs, deep,pid) {
        var nextChids = [];
        if (deep < limitDeep) {
            var i = 0;
            if (deep % 2 === 0) {
                while (i < 2) {
                    nodeCount++;
                    var id =  "deep" + deep + "_" + i;
                    var text = "节点" + id;
                    //if(i >4){
                    if (i === 0 || i === 1) {
                        var gg =  {
                            id: $B.getUUID() + id,
                            text: text,
                            checked: true,
                            data: {
                                "f1": 'abc' + deep,
                                "f2": '字段' + id
                            },
                            toolbar: toolbars
                        };
                        if(i === 0 && rootLoopCount === 1){
                            gg.children = [];
                            nextChids.push({
                                children:gg.children,
                                deep:deep + 1
                            });
                        }
                        childs.push(gg);
                    } else {
                        var data = {
                            id: $B.getUUID() + id,
                            text: text,
                            data: {
                                "f1": 'abc' + deep + i,
                                "f2": '字段' + id
                            },
                            children: [],
                            toolbar: toolbars
                        };
                        if (deep > 1) {
                            data["closed"] = true;
                        }
                        if (text === "节点deep2_3") {
                            data["closed"] = false;
                        }
                        childs.push(data);
                        //loopMakeChilds(data.children, deep + 1);
                        nextChids.push({
                            children:data.children,
                            deep:deep + 1
                        });
                    }
                    i++;
                }
            } else {
                while (i < 3) {
                    nodeCount++;
                    var id = "deep" + deep + "_" + i;
                    var text = "节点" + id;
                    //if( i === 4){
                    if (i === 1 || i === 3) {
                        var data = {
                            disChecked: false,
                            id: $B.getUUID() + id,
                            text: text,
                            data: {
                                "f1": 'kevine' + deep + i,
                                "f2": '字段' + id
                            },
                            children: [],
                            toolbar: toolbars
                        };
                        if (deep > 1) {
                            data["closed"] = true;
                        }
                        childs.push(data);
                        //loopMakeChilds(data.children, deep + 1);
                        nextChids.push({
                            children:data.children,
                            deep:deep + 1
                        });
                    } else {
                        if (i % 2 === 0) {
                            childs.push({
                                id: $B.getUUID() + id,
                                text: text,
                                checked: true,
                                data: {
                                    "f1": 'khuang' + deep + i,
                                    "f2": '字段' + id
                                },
                                toolbar: toolbars
                            });
                        } else {
                            childs.push({
                                id: $B.getUUID() + id,
                                text: text,
                                data: {
                                    "f1": 'khuang' + deep + i,
                                    "f2": '字段' + id
                                },
                                toolbar: toolbars
                            });
                        }
                    }
                    i++;
                }
            }
        }

        var f = (function(nexts){
            return function(){
                for(var i = 0 ; i < nexts.length ; i++){
                    loopMakeChilds(nexts[i].children, nexts[i].deep);
                };
            }
        })(nextChids);
        return f();

    }
    
    function makeRoot(rootLoopCount, maxDeep) {
        limitDeep = maxDeep;
        var i = 0;
        var deep = 1;
        var nextChids = [];
        while (i < rootLoopCount) {
            nodeCount++;
            var id =  deep + "_" + i;
            var text = "节点" + id;
            if (i > 1  && i < 6) {
                if (rootLoopCount > 3) {
                    if (i > 1) {
                        treeData.push({
                            id: $B.getUUID() + id,
                            text: text,
                            data: {
                                "f1": 'name' + deep + i,
                                "f2": '字段' + id
                            }
                        });
                    }
                } else {
                    var tddd = {
                        id: $B.getUUID() + id,
                        text: text,
                        data: {
                            "f1": 'name' + deep + i,
                            "f2": '字段' + id
                        }
                    };
                    if(rootLoopCount ===1 ){
                        tddd.children = [];
                        nextChids.push({
                            children:tddd.children,
                            deep:deep + 1
                        });
                    }  
                    treeData.push(tddd);                      
                }
            } else {
                var data = {
                    id: $B.getUUID() + id,
                    text: text,
                    data: {
                        "f1": 'name' + deep + i,
                        "f2": '字段' + id
                    },
                    children: []
                };
                treeData.push(data);
                nextChids.push({
                    children:data.children,
                    deep:deep + 1
                });
                //loopMakeChilds(data.children, deep + 1);
            }
            i++;
        }
        var f = (function(nexts){
            return function(){
                for(var i = 0 ; i < nexts.length ; i++){
                    loopMakeChilds(nexts[i].children, nexts[i].deep);
                };
            }
        })(nextChids);
        return f();

    }  
makeRoot(rootLoopCount, maxDeep);
return treeData;
}

var comBoxJson = [{
"id": "55f26940fb3411e69507889ffaf248ae",
"text": "首页",
"data": {
    "controller": "/admin/board",
    "createTime": "2017-02-25 00:00:00",
    "createUserId": "0",
    "depth": 0,
    "id": "55f26940fb3411e69507889ffaf248ae",
    "isEnable": 1,
    "isLeaf": 1,
    "isVisible": 1,
    "menuText": "首页",
    "orderIndex": 0,
    "pageName": "board",
    "pid": "0"
},
"pid": "0"
}, {
"id": "1",
"text": "基础管理基础管理基",
"data": {
    "controller": "",
    "createTime": "2016-04-08 00:00:00",
    "createUserId": "0",
    "depth": 0,
    "id": "1",
    "isEnable": 1,
    "isLeaf": 0,
    "isVisible": 1,
    "menuIconCss": "",
    "menuText": "基础管理",
    "orderIndex": 1,
    "pageName": "",
    "params": "",
    "pid": "0"
},
"closed": true,
"children": [{
    "id": "10",
    "text": "菜单管理",
    "data": {
        "controller": "/base/menu",
        "createTime": "2016-04-08 00:00:00",
        "createUserId": "0",
        "depth": 1,
        "id": "10",
        "isEnable": 1,
        "isLeaf": 1,
        "isVisible": 1,
        "menuIconCss": "",
        "menuText": "菜单管理",
        "orderIndex": 1,
        "pageName": "",
        "params": "gridid=menu",
        "pid": "1"
    },
    "pid": "1"
}, {
    "id": "23",
    "text": "用户管理",
    "data": {
        "controller": "/base/user",
        "createTime": "2016-04-08 00:00:00",
        "createUserId": "0",
        "depth": 1,
        "id": "23",
        "isEnable": 1,
        "isLeaf": 1,
        "isVisible": 1,
        "menuIconCss": "",
        "menuText": "用户管理",
        "orderIndex": 1,
        "pageName": "",
        "params": "gridid=user",
        "pid": "1"
    },
    "pid": "1"
}, {
    "id": "15",
    "text": "机构管理",
    "data": {
        "controller": "/base/org",
        "createTime": "2016-04-08 00:00:00",
        "createUserId": "0",
        "depth": 1,
        "id": "15",
        "isEnable": 1,
        "isLeaf": 1,
        "isVisible": 1,
        "menuIconCss": "",
        "menuText": "机构管理",
        "orderIndex": 2,
        "pageName": "",
        "params": "gridid=org",
        "pid": "1"
    },
    "pid": "1"
}, {
    "id": "24",
    "text": "角色管理",
    "data": {
        "controller": "/base/role",
        "createTime": "2016-04-08 00:00:00",
        "createUserId": "0",
        "depth": 1,
        "id": "24",
        "isEnable": 1,
        "isLeaf": 1,
        "isVisible": 1,
        "menuIconCss": "",
        "menuText": "角色管理",
        "orderIndex": 2,
        "pageName": "",
        "params": "gridid=role",
        "pid": "1"
    },
    "pid": "1"
}, {
    "id": "25",
    "text": "功能管理",
    "data": {
        "controller": "/base/func",
        "createTime": "2016-04-08 00:00:00",
        "createUserId": "0",
        "depth": 1,
        "id": "25",
        "isEnable": 1,
        "isLeaf": 1,
        "isVisible": 1,
        "menuIconCss": "",
        "menuText": "功能管理",
        "orderIndex": 3,
        "pageName": "",
        "params": "gridid=func",
        "pid": "1"
    },
    "pid": "1"
}, {
    "id": "1558f6e0001311e7b135889ffaf248ae",
    "text": "系统日志",
    "data": {
        "controller": "/base/log",
        "createTime": "2017-03-03 00:00:00",
        "createUserId": "0",
        "depth": 1,
        "id": "1558f6e0001311e7b135889ffaf248ae",
        "isEnable": 1,
        "isLeaf": 1,
        "isVisible": 1,
        "menuIconCss": "",
        "menuText": "系统日志",
        "orderIndex": 100,
        "pageName": "",
        "params": "gridid=log",
        "pid": "1"
    },
    "pid": "1"
}],
"pid": "0"
}, {
"id": "7f8e5600fb0d11e68025843a4b602ed9",
"text": "门户管理",
"data": {
    "createTime": "2017-02-25 00:00:00",
    "createUserId": "0",
    "depth": 0,
    "id": "7f8e5600fb0d11e68025843a4b602ed9",
    "isEnable": 1,
    "isLeaf": 0,
    "isVisible": 1,
    "menuText": "门户管理",
    "orderIndex": 1,
    "pid": "0"
},
"closed": true,
"children": [{
    "id": "c7f66860fb0d11e68025843a4b602ed9",
    "text": "中心动态",
    "data": {
        "controller": "/admin/news",
        "createTime": "2017-02-25 00:00:00",
        "createUserId": "0",
        "depth": 1,
        "id": "c7f66860fb0d11e68025843a4b602ed9",
        "isEnable": 1,
        "isLeaf": 1,
        "isVisible": 1,
        "menuIconCss": "",
        "menuText": "中心动态",
        "orderIndex": 0,
        "pageName": "",
        "params": "type=0&gridid=homenews",
        "pid": "7f8e5600fb0d11e68025843a4b602ed9"
    },
    "pid": "7f8e5600fb0d11e68025843a4b602ed9"
}, {
    "id": "cc65be80fb0f11e6bcc9843a4b602ed9",
    "text": "通知通报",
    "data": {
        "controller": "/admin/news",
        "createTime": "2017-02-25 00:00:00",
        "createUserId": "0",
        "depth": 1,
        "id": "cc65be80fb0f11e6bcc9843a4b602ed9",
        "isEnable": 1,
        "isLeaf": 1,
        "isVisible": 1,
        "menuIconCss": "",
        "menuText": "通知通报",
        "orderIndex": 1,
        "pageName": "",
        "params": "type=1&gridid=homenews",
        "pid": "7f8e5600fb0d11e68025843a4b602ed9"
    },
    "pid": "7f8e5600fb0d11e68025843a4b602ed9"
}, {
    "id": "08ef7c60fb1011e6bcc9843a4b602ed9",
    "text": "动态管控",
    "data": {
        "controller": "/admin/news",
        "createTime": "2017-02-25 00:00:00",
        "createUserId": "0",
        "depth": 1,
        "id": "08ef7c60fb1011e6bcc9843a4b602ed9",
        "isEnable": 1,
        "isLeaf": 1,
        "isVisible": 1,
        "menuIconCss": "",
        "menuText": "动态管控",
        "orderIndex": 2,
        "pageName": "",
        "params": "type=2&gridid=homenews",
        "pid": "7f8e5600fb0d11e68025843a4b602ed9"
    },
    "pid": "7f8e5600fb0d11e68025843a4b602ed9"
}, {
    "id": "40b597b0fb1011e6bcc9843a4b602ed9",
    "text": "预警通报",
    "data": {
        "controller": "/admin/news",
        "createTime": "2017-02-25 00:00:00",
        "createUserId": "0",
        "depth": 1,
        "id": "40b597b0fb1011e6bcc9843a4b602ed9",
        "isEnable": 1,
        "isLeaf": 1,
        "isVisible": 1,
        "menuIconCss": "",
        "menuText": "预警通报",
        "orderIndex": 3,
        "pageName": "",
        "params": "type=3&gridid=homenews",
        "pid": "7f8e5600fb0d11e68025843a4b602ed9"
    },
    "pid": "7f8e5600fb0d11e68025843a4b602ed9"
}, {
    "id": "5d2fa1b0fb1011e6bcc9843a4b602ed9",
    "text": "案件研判",
    "data": {
        "controller": "/admin/news",
        "createTime": "2017-02-25 00:00:00",
        "createUserId": "0",
        "depth": 1,
        "id": "5d2fa1b0fb1011e6bcc9843a4b602ed9",
        "isEnable": 1,
        "isLeaf": 1,
        "isVisible": 1,
        "menuIconCss": "",
        "menuText": "案件研判",
        "orderIndex": 4,
        "pageName": "",
        "params": "type=4&gridid=homenews",
        "pid": "7f8e5600fb0d11e68025843a4b602ed9"
    },
    "pid": "7f8e5600fb0d11e68025843a4b602ed9"
}, {
    "id": "81e8e070fb1011e6bcc9843a4b602ed9",
    "text": "个案通报",
    "data": {
        "controller": "/admin/news",
        "createTime": "2017-02-25 00:00:00",
        "createUserId": "0",
        "depth": 1,
        "id": "81e8e070fb1011e6bcc9843a4b602ed9",
        "isEnable": 1,
        "isLeaf": 1,
        "isVisible": 1,
        "menuIconCss": "",
        "menuText": "个案通报",
        "orderIndex": 5,
        "pageName": "",
        "params": "type=5&gridid=homenews",
        "pid": "7f8e5600fb0d11e68025843a4b602ed9"
    },
    "pid": "7f8e5600fb0d11e68025843a4b602ed9"
}, {
    "id": "a92f59c0fb1011e6bcc9843a4b602ed9",
    "text": "视频串并",
    "data": {
        "controller": "/admin/news",
        "createTime": "2017-02-25 00:00:00",
        "createUserId": "0",
        "depth": 1,
        "id": "a92f59c0fb1011e6bcc9843a4b602ed9",
        "isEnable": 1,
        "isLeaf": 1,
        "isVisible": 1,
        "menuIconCss": "",
        "menuText": "视频串并",
        "orderIndex": 6,
        "pageName": "",
        "params": "type=6&gridid=homenews",
        "pid": "7f8e5600fb0d11e68025843a4b602ed9"
    },
    "pid": "7f8e5600fb0d11e68025843a4b602ed9"
}, {
    "id": "c40306c0fb1011e6bcc9843a4b602ed9",
    "text": "工作简报",
    "data": {
        "controller": "/admin/news",
        "createTime": "2017-02-25 00:00:00",
        "createUserId": "0",
        "depth": 1,
        "id": "c40306c0fb1011e6bcc9843a4b602ed9",
        "isEnable": 1,
        "isLeaf": 1,
        "isVisible": 1,
        "menuIconCss": "",
        "menuText": "工作简报",
        "orderIndex": 7,
        "pageName": "",
        "params": "type=7&gridid=homenews",
        "pid": "7f8e5600fb0d11e68025843a4b602ed9"
    },
    "pid": "7f8e5600fb0d11e68025843a4b602ed9"
}, {
    "id": "eb0e72e0fb1011e6bcc9843a4b602ed9",
    "text": "经验交流",
    "data": {
        "controller": "/admin/news",
        "createTime": "2017-02-25 00:00:00",
        "createUserId": "0",
        "depth": 1,
        "id": "eb0e72e0fb1011e6bcc9843a4b602ed9",
        "isEnable": 1,
        "isLeaf": 1,
        "isVisible": 1,
        "menuIconCss": "",
        "menuText": "经验交流",
        "orderIndex": 9,
        "pageName": "",
        "params": "type=8&gridid=homenews",
        "pid": "7f8e5600fb0d11e68025843a4b602ed9"
    },
    "pid": "7f8e5600fb0d11e68025843a4b602ed9"
}, {
    "id": "0157c7b0fb0f11e6bcc9843a4b602ed9",
    "text": "友情链接",
    "data": {
        "controller": "/admin/siteLink",
        "createTime": "2017-02-25 00:00:00",
        "createUserId": "0",
        "depth": 1,
        "id": "0157c7b0fb0f11e6bcc9843a4b602ed9",
        "isEnable": 1,
        "isLeaf": 1,
        "isVisible": 1,
        "menuIconCss": "",
        "menuText": "友情链接",
        "orderIndex": 10,
        "pageName": "",
        "params": "gridid=sitelink",
        "pid": "7f8e5600fb0d11e68025843a4b602ed9"
    },
    "pid": "7f8e5600fb0d11e68025843a4b602ed9"
}],
"pid": "0"
}, {
"id": "a2968770fcd011e6866c3c970e51b949",
"text": "本地数据",
"data": {
    "createTime": "2017-02-27 00:00:00",
    "createUserId": "0",
    "depth": 0,
    "id": "a2968770fcd011e6866c3c970e51b949",
    "isEnable": 1,
    "isLeaf": 0,
    "isVisible": 1,
    "menuText": "本地数据",
    "orderIndex": 3,
    "pid": "0"
},
"closed": true,
"children": [{
    "id": "d42bdba0fcd011e6866c3c970e51b949",
    "text": "停车场管理",
    "data": {
        "controller": "/admin/park",
        "createTime": "2017-02-27 00:00:00",
        "createUserId": "0",
        "depth": 1,
        "id": "d42bdba0fcd011e6866c3c970e51b949",
        "isEnable": 1,
        "isLeaf": 1,
        "isVisible": 1,
        "menuIconCss": "",
        "menuText": "停车场管理",
        "orderIndex": 0,
        "pageName": "",
        "params": "type=0&gridid=park",
        "pid": "a2968770fcd011e6866c3c970e51b949"
    },
    "pid": "a2968770fcd011e6866c3c970e51b949"
}, {
    "id": "9b8d7f80fdd211e690e73c970e51b949",
    "text": "停车场车辆管理",
    "data": {
        "controller": "/admin/parkCar",
        "createTime": "2017-03-01 00:00:00",
        "createUserId": "0",
        "depth": 1,
        "id": "9b8d7f80fdd211e690e73c970e51b949",
        "isEnable": 1,
        "isLeaf": 1,
        "isVisible": 1,
        "menuIconCss": "",
        "menuText": "停车场车辆管理",
        "orderIndex": 1,
        "pageName": "",
        "params": "type=0&gridid=parkcar",
        "pid": "a2968770fcd011e6866c3c970e51b949"
    },
    "pid": "a2968770fcd011e6866c3c970e51b949"
}, {
    "id": "bb3b4130034c11e7a6c43c970e51b949",
    "text": "前科信息",
    "data": {
        "controller": "/admin/criminalRecord",
        "createTime": "2017-03-07 00:00:00",
        "createUserId": "0",
        "depth": 1,
        "id": "bb3b4130034c11e7a6c43c970e51b949",
        "isEnable": 1,
        "isLeaf": 1,
        "isVisible": 1,
        "menuIconCss": "",
        "menuText": "前科信息",
        "orderIndex": 3,
        "pageName": "",
        "params": "gridid=criminalrecord",
        "pid": "a2968770fcd011e6866c3c970e51b949"
    },
    "pid": "a2968770fcd011e6866c3c970e51b949"
}, {
    "id": "8ed0b5d0034c11e7a6c43c970e51b949",
    "text": "刑嫌信息",
    "data": {
        "controller": "/admin/criminalSuspect",
        "createTime": "2017-03-07 00:00:00",
        "createUserId": "0",
        "depth": 1,
        "id": "8ed0b5d0034c11e7a6c43c970e51b949",
        "isEnable": 1,
        "isLeaf": 1,
        "isVisible": 1,
        "menuIconCss": "",
        "menuText": "刑嫌信息",
        "orderIndex": 4,
        "pageName": "",
        "params": "gridid=criminalsuspect",
        "pid": "a2968770fcd011e6866c3c970e51b949"
    },
    "pid": "a2968770fcd011e6866c3c970e51b949"
}],
"pid": "0"
}, {
"id": "30a9f680fe2c11e6bfcf843a4b602ed9",
"text": "辅助决策",
"data": {
    "createTime": "2017-03-01 00:00:00",
    "createUserId": "0",
    "depth": 0,
    "id": "30a9f680fe2c11e6bfcf843a4b602ed9",
    "isEnable": 1,
    "isLeaf": 0,
    "isVisible": 1,
    "menuText": "辅助决策",
    "orderIndex": 4,
    "pid": "0"
},
"children": [{
    "id": "573efa20fe2c11e6bfcf843a4b602ed9",
    "text": "警情数据",
    "data": {
        "controller": "/admin/alarmInfo",
        "createTime": "2017-03-01 00:00:00",
        "createUserId": "0",
        "depth": 1,
        "id": "573efa20fe2c11e6bfcf843a4b602ed9",
        "isEnable": 1,
        "isLeaf": 1,
        "isVisible": 1,
        "menuIconCss": "",
        "menuText": "警情数据",
        "orderIndex": 0,
        "pageName": "",
        "params": "gridid=alarminfo",
        "pid": "30a9f680fe2c11e6bfcf843a4b602ed9"
    },
    "pid": "30a9f680fe2c11e6bfcf843a4b602ed9"
}, {
    "id": "6784ee80fe2c11e6bfcf843a4b602ed9",
    "text": "各色人员数据",
    "data": {
        "controller": "/admin/fourPerson",
        "createTime": "2017-03-01 00:00:00",
        "createUserId": "0",
        "depth": 1,
        "id": "6784ee80fe2c11e6bfcf843a4b602ed9",
        "isEnable": 1,
        "isLeaf": 1,
        "isVisible": 1,
        "menuIconCss": "",
        "menuText": "各色人员数据",
        "orderIndex": 0,
        "pageName": "",
        "params": "gridid=fourperson",
        "pid": "30a9f680fe2c11e6bfcf843a4b602ed9"
    },
    "children": [{
        "id": "10",
        "text": "菜单管理",
        "data": {
            "controller": "/base/menu",
            "createTime": "2016-04-08 00:00:00",
            "createUserId": "0",
            "depth": 1,
            "id": "10",
            "isEnable": 1,
            "isLeaf": 1,
            "isVisible": 1,
            "menuIconCss": "",
            "menuText": "菜单管理",
            "orderIndex": 1,
            "pageName": "",
            "params": "gridid=menu",
            "pid": "6784ee80fe2c11e6bfcf843a4b602ed9"
        },
        "pid": "6784ee80fe2c11e6bfcf843a4b602ed9"
    }, {
        "id": "23",
        "text": "用户管理",
        "data": {
            "controller": "/base/user",
            "createTime": "2016-04-08 00:00:00",
            "createUserId": "0",
            "depth": 1,
            "id": "23",
            "isEnable": 1,
            "isLeaf": 1,
            "isVisible": 1,
            "menuIconCss": "",
            "menuText": "用户管理",
            "orderIndex": 1,
            "pageName": "",
            "params": "gridid=user",
            "pid": "6784ee80fe2c11e6bfcf843a4b602ed9"
        },
        "pid": "6784ee80fe2c11e6bfcf843a4b602ed9"
    }, {
        "id": "15",
        "text": "机构管理",
        "data": {
            "controller": "/base/org",
            "createTime": "2016-04-08 00:00:00",
            "createUserId": "0",
            "depth": 1,
            "id": "15",
            "isEnable": 1,
            "isLeaf": 1,
            "isVisible": 1,
            "menuIconCss": "",
            "menuText": "机构管理",
            "orderIndex": 2,
            "pageName": "",
            "params": "gridid=org",
            "pid": "6784ee80fe2c11e6bfcf843a4b602ed9"
        },
        "pid": "6784ee80fe2c11e6bfcf843a4b602ed9"
    }, {
        "id": "24",
        "text": "角色管理",
        "data": {
            "controller": "/base/role",
            "createTime": "2016-04-08 00:00:00",
            "createUserId": "0",
            "depth": 1,
            "id": "24",
            "isEnable": 1,
            "isLeaf": 1,
            "isVisible": 1,
            "menuIconCss": "",
            "menuText": "角色管理",
            "orderIndex": 2,
            "pageName": "",
            "params": "gridid=role",
            "pid": "6784ee80fe2c11e6bfcf843a4b602ed9"
        },
        "pid": "6784ee80fe2c11e6bfcf843a4b602ed9"
    }, {
        "id": "25",
        "text": "功能管理",
        "data": {
            "controller": "/base/func",
            "createTime": "2016-04-08 00:00:00",
            "createUserId": "0",
            "depth": 1,
            "id": "25",
            "isEnable": 1,
            "isLeaf": 1,
            "isVisible": 1,
            "menuIconCss": "",
            "menuText": "功能管理",
            "orderIndex": 3,
            "pageName": "",
            "params": "gridid=func",
            "pid": "6784ee80fe2c11e6bfcf843a4b602ed9"
        },
        "pid": "6784ee80fe2c11e6bfcf843a4b602ed9"
    }, {
        "id": "1558f6e0001311e7b135889ffaf248ae",
        "text": "系统日志",
        "data": {
            "controller": "/base/log",
            "createTime": "2017-03-03 00:00:00",
            "createUserId": "0",
            "depth": 1,
            "id": "1558f6e0001311e7b135889ffaf248ae",
            "isEnable": 1,
            "isLeaf": 1,
            "isVisible": 1,
            "menuIconCss": "",
            "menuText": "系统日志",
            "orderIndex": 100,
            "pageName": "",
            "params": "gridid=log",
            "pid": "6784ee80fe2c11e6bfcf843a4b602ed9"
        },
        "pid": "6784ee80fe2c11e6bfcf843a4b602ed9"
    }],
    "pid": "30a9f680fe2c11e6bfcf843a4b602ed9"
}],
"pid": "0"
}];