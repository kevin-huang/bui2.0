function tcoFn(fn){  
  var active = false;
  var accumulated = [];  
  return function executeFn() {
    accumulated.push(arguments);//每次将参数传入. 例如, 1 100000
    if (!active) {
        active = true; 
        while (accumulated.length) {
          let args = accumulated.pop();          
          fn.apply(this,args);
        }
        active = false;
    }
  };
}
var txtPre = "text_";
var dataName = "data";
var attrName = "attr";
var counted = 0;
function fmaker(obj,deep,count){
  counted++;
  obj["id"] = deep;
  obj["text"] = txtPre+deep;
  var dataAttr = {};
  for(var i = 0; i < count; i++){
      let p = attrName+i;
      dataAttr[p] = deep+"-"+count;
  }
  obj[dataName] = dataAttr;    
  if(deep > 0){ 
      obj["children"] = [];
      let childCount = 1;  
      if(deep % 3 === 0){
        childCount = 3;
        if( counted > 5){
          obj["closed"] = true;
        }      
      }else if(deep % 2 === 0){
        childCount = 2;
      }    
      if(deep % 7 === 0){
        childCount = 0;
        var tmp = {
          id:deep+"999",
          text:'子元素',
          data:{},
          children:[
              {
                id:90,
                text:'abcccc',
                data:{}
              },{
                id:100,
                text:'bbbdsfk',
                data:{}
              }
          ]
        }
        obj["children"].push(tmp);
      }
      let nextDeep = deep - 1;
      while(childCount > 0){
        if(deep % 3 === 0){
          let temp = {
            id : deep+"_"+childCount+"000",
            text:'child_'+childCount,
            checked:true,
            data:{

            }
          };
          obj.children.push(temp); 
        }      
          let child = {};
          obj.children.push(child);       
          exeFn(child,nextDeep,count);
          child.id = child.id+"_"+childCount;         
          childCount--;
          if(deep % 2 === 0){
            let temp = {
              id : deep+"_"+childCount+"1111",
              text:'child_'+childCount,
              checked:true,
              data:{
  
              }
            };
            obj.children.push(temp); 
          }          
      }
  }
};
var exeFn = tcoFn(fmaker);
function getTreeData(deep,child){
  var data = {};
  counted = 0;
  console.log("starting......");
  exeFn(data,deep,child);
  console.log("counted = "+counted);
  var arr = [];
  arr.push(data);
  arr.push({
    id:999999,
    text:'中国结',
    data:{},
    children:[
        {
          id:'sdfjdkl',
          text:'圣诞节福利看',
          data:{"mmmmm":'sdfljdfjk'},
          children:[]
        },{
          id:'09998',
          text:'水电费来',
          data:{"aaaa":'sdfljdfjk'},
          children:[{
              id:'123123',
              text:'df水电费进口量',
              data:{"test11":'sdfljdfjk'},
              children:[
                {
                  id:999999,
                  text:'中国结',
                  closed:true,
                  data:{"test":'sdfljdfjk'},
                  children:[
                      {
                        id:'sdfjdkl',
                        text:'圣诞节福利看',
                        data:{"bbdsf":'sdfljdfjk'}
                      },{
                        id:'09998',
                        text:'水电费来',
                        data:{},
                        children:[{
                            id:'123123',
                            text:'df水电费进口量',
                            data:{"aaaa":'sdfljdfjk'}
                          }
                        ]
                      }
                  ]
                }
              ]
            }
          ]
        }
    ]
  });
  return arr;
}







