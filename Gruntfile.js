module.exports = function (grunt) {
	//var taskArray = ['copy:main', 'concat', 'uglify', 'cssmin', 'copy:toweb', 'watch'];
	var taskArray = ['concat','jshint','copy:tolib','watch'];
	//var taskArray = ['concat','jshint','copy:tolib','cssmin','watch'];
	var pkgObj = grunt.file.readJSON('package.json');
	grunt.initConfig({
		pkg: pkgObj,
		meta: {
			banner: '/*! <%= pkg.name %>-v<%= pkg.version %> - ' +
			'<%= grunt.template.today("yyyy-mm-dd") %> \nCopyright (c): kevin.huang <%= pkg.site %> \nReleased under MIT License*/'
		},
		jshint: {
			options: {
				jshintrc: '.jshintrc'
			},
			build: [				
				'build/javascript/^(?!basic).*$'
			]
		},
		copy:{
			tolib: {
				files: [	
					{src:'src/js/editor/prpForm.js', dest: 'build/javascript/prpForm-<%= pkg.version %>.js'},				
					{ expand: true, cwd: 'src/js/lib', src: ['**'], dest: 'build/lib' },
					{ expand: true, cwd: 'src/theme', src: ['**'], dest: 'build/theme' },
					{ expand: true, cwd: 'src/js/', src: ['lang_config.js'], dest: 'build/test' },
				]
			}
		},
		uglify: {
			options: {
				mangle: true, //不混淆变量名
				stripBanners: true,
				banner: '/*! <%= pkg.name %> - v<%= pkg.version %> - ' +
				'<%= grunt.template.today("yyyy-mm-dd") %> \nCopyright (c): kevin.huang <%= pkg.site %> \nReleased under MIT License*/'
			},
			buildall: {//压缩所有js
				files: [{
					expand: true,
					cwd: 'build/javascript',//js目录下
					src: '**/*.js',//所有js文件
					dest: 'build/javascript-min'//输出到此目录下
				}]
			}
		},
		cssmin: {
			options: {
				mergeIntoShorthands: false,
				roundingPrecision: -1
			},
			target: {
				files: {
					'build/theme/font/bui-fonts.min.css': ['build/theme/font/bui-fonts.css'],
					'build/theme/default/default-<%= pkg.version %>.min.css': ['build/theme/default/default-<%= pkg.version %>.css']
				}
			}
		},
		concat: {
			options: {
				process: function(src, filepath) {
					if(filepath.indexOf("head.js") > 0){
						src = src.replace("});","");
					}else if(filepath.indexOf("main.js") > 0){
						src = src+' \n if(!window["$B"] ) {window["$B"] = $B;} \n  return window["$B"] ; \n});';
					}
					return src+"\n";
				},
				separator: '',
				stripBanners: true,
				banner: '/*! <%= pkg.name %> - v<%= pkg.version %> - ' +
					'<%= grunt.template.today("yyyy-mm-dd hh:mm:ss") %> \nCopyright (c): kevin.huang  <%= pkg.site %> \nReleased under MIT License*/\n'
			},
			css:{
				src:[
					'src/theme/default/base.css',
					'src/theme/default/toolbar.css',
					'src/theme/default/window.css',
					'src/theme/default/panel.css',
					'src/theme/default/tree.css',
					'src/theme/default/pagination.css',
					'src/theme/default/nav.css',
					'src/theme/default/tabs.css',
					'src/theme/default/combox.css',
					'src/theme/default/ctxmenu.css',
					'src/theme/default/calendar.css',
					'src/theme/default/table.css',
					'src/theme/default/accordion.css',
					'src/theme/default/color.css'
				],
				dest: 'build/theme/default/default-<%= pkg.version %>.css'
			},
			baseCss:{
				src:[
					'src/theme/default/base.css',
					'src/theme/default/toolbar.css',
					'src/theme/default/window.css',
					'src/theme/default/panel.css',
					'src/theme/default/color.css'					
				],
				dest: 'build/theme/default/base-<%= pkg.version %>.css'
			},
			basic:{
				src: [	
					'src/js/head.js',
					'src/js/basic/dom.js',
					'src/js/basic/request.js',	
					'src/js/basic/window.js',						
					'src/js/basic/main.js',							
				],
				dest: 'build/javascript/self-basic-<%= pkg.version %>.js'
			},
			extplugs:{
				src: [	
					'src/lib/velocity.min.js',					
					'build/javascript/self-basic-<%= pkg.version %>.js',
				],
				dest: 'build/javascript/basic-<%= pkg.version %>.js'
			},
			layout:{
				src: [		
					'src/js/head.js',	
					'src/js/layout/main.js'		
				],
				dest: 'build/javascript/layout-<%= pkg.version %>.js'
			},
			plugins:{
				src:[
					'src/js/head.js',
					'src/js/plugins/draggable.js',
					'src/js/plugins/scrollbar.js',					
					'src/js/plugins/main.js'
				],
				dest: 'build/javascript/plugins-<%= pkg.version %>.js'
			},
			resize:{
				src:[
					'src/js/head.js',					
					'src/js/resize/main.js'
				],
				dest: 'build/javascript/resize-<%= pkg.version %>.js'
			},
			toolbar:{
				src:[
					'src/js/head.js',					
					'src/js/toolbar/main.js'
				],
				dest: 'build/javascript/toolbar-<%= pkg.version %>.js'
			},
			panel:{
				src:[
					'src/js/head.js',					
					'src/js/panel/main.js'
				],
				dest: 'build/javascript/panel-<%= pkg.version %>.js'
			},
			pagination:{
				src:[
					'src/js/head.js',					
					'src/js/pagination/main.js'
				],
				dest: 'build/javascript/pagination-<%= pkg.version %>.js'
			},
			table:{
				src:[
					'src/js/head.js',					
					'src/js/table/main.js'
				],
				dest: 'build/javascript/table-<%= pkg.version %>.js'
			},
			tree:{
				src:[
					'src/js/head.js',					
					'src/js/tree/main.js'
				],
				dest: 'build/javascript/tree-<%= pkg.version %>.js'
			},
			calendar:{
				src:[
					'src/js/head.js',					
					'src/js/calendar/main.js'
				],
				dest: 'build/javascript/calendar-<%= pkg.version %>.js'
			},
			combox:{
				src:[
					'src/js/head.js',					
					'src/js/combox/main.js'
				],
				dest: 'build/javascript/combox-<%= pkg.version %>.js'
			},
			ctxmenu:{
				src:[
					'src/js/head.js',					
					'src/js/ctxmenu/main.js'
				],
				dest: 'build/javascript/ctxmenu-<%= pkg.version %>.js'
			},
			nav:{
				src:[
					'src/js/head.js',					
					'src/js/nav/main.js'
				],
				dest: 'build/javascript/nav-<%= pkg.version %>.js'
			},
			tabs:{
				src:[
					'src/js/head.js',					
					'src/js/tabs/main.js'
				],
				dest: 'build/javascript/tabs-<%= pkg.version %>.js'
			},
			accordion:{
				src:[
					'src/js/head.js',					
					'src/js/accordion/main.js'
				],
				dest: 'build/javascript/accordion-<%= pkg.version %>.js'
			},
			upload:{
				src:[
					'src/js/head.js',					
					'src/js/upload/main.js'
				],
				dest: 'build/javascript/upload-<%= pkg.version %>.js'
			},
			curd:{
				src:[
					'src/js/head.js',					
					'src/js/curd/main.js'
				],
				dest: 'build/javascript/curd-<%= pkg.version %>.js'
			},
			color:{
				src:[
					'src/js/head.js',					
					'src/js/color/main.js'
				],
				dest: 'build/javascript/color-<%= pkg.version %>.js'
			},
			editor:{
				src:[
					'src/js/head.js',
					'src/js/editor/utils.js',
					'src/js/editor/stack.js',	
					'src/js/editor/config.js',				
					'src/js/editor/index.js',
					'src/js/editor/core.js',
					'src/js/editor/table.js',
					'src/js/editor/api.js',									
					'src/js/editor/toolbar.js',
					'src/js/editor/urdo.js',
					'src/js/editor/main.js'
				],
				dest: 'build/javascript/editor-<%= pkg.version %>.js'
			},
			mindep:{
				src:[
					'build/javascript/basic-<%= pkg.version %>.js',					
					'build/javascript/panel-<%= pkg.version %>.js',
					'build/javascript/plugins-<%= pkg.version %>.js',
					'build/javascript/resize-<%= pkg.version %>.js',
					'build/javascript/toolbar-<%= pkg.version %>.js',					
					'build/javascript/upload-<%= pkg.version %>.js',
					'build/javascript/color-<%= pkg.version %>.js'					
					],
				dest:'build/javascript/mindep-<%= pkg.version %>.js'
			},			
			all:{
				src: [					
					'build/javascript/basic-<%= pkg.version %>.js',
					'build/javascript/accordion-<%= pkg.version %>.js',
					'build/javascript/panel-<%= pkg.version %>.js',
					'build/javascript/plugins-<%= pkg.version %>.js',
					'build/javascript/resize-<%= pkg.version %>.js',
					'build/javascript/toolbar-<%= pkg.version %>.js',
					'build/javascript/tree-<%= pkg.version %>.js',
					'build/javascript/tabs-<%= pkg.version %>.js',
					'build/javascript/pagination-<%= pkg.version %>.js',
					'build/javascript/nav-<%= pkg.version %>.js',
					'build/javascript/ctxmenu-<%= pkg.version %>.js',
					'build/javascript/combox-<%= pkg.version %>.js',
					'build/javascript/calendar-<%= pkg.version %>.js',
					'build/javascript/table-<%= pkg.version %>.js',
					'build/javascript/upload-<%= pkg.version %>.js',
					'build/javascript/layout-<%= pkg.version %>.js',
					'build/javascript/curd-<%= pkg.version %>.js',
					'build/javascript/color-<%= pkg.version %>.js'
				],
				dest: 'build/javascript/<%= pkg.name %>-<%= pkg.version %>.js'
			}
		},
		watch: {
			build: {
				files: ['src/js/**/*.js', 
						'src/js/*.js', 
						'src/theme/*.css'],
				tasks: taskArray,
				options: { spawn: false }
			}
		}
	});
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-uglify-es');	
	grunt.registerTask('default', taskArray);
}