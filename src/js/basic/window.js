
_extendObjectFn($B, {
    /**
     *打开一个窗口
    *arg={
            full:false,//是否满屏，当为true时候，高宽无效  
            autoHeight:true,//自动根据内容设置高度
            size: { width: 'auto', height: 'auto' },         
            title: '', //标题
            isTop: false,
            iconCls: null, //图标class，font-awesome字体图标
            iconColor: undefined,//图标颜色
            headerColor: undefined,//头部颜色
            toolbar: null, //工具栏对象参考工具栏组件配置说明，可以是创建函数
            toolbarStyle: undefined,//参考工具栏样式定义
            shadow: true, //是否需要阴影
            radius: undefined, //圆角px定义
            header: true, //是否显示头部
            zIndex: 200000000,//层级
            content: null, //静态内容
            url: '',//请求地址
            dataType: 'html', //当为url请求时，html/json/iframe
            draggable: false, //是否可以拖动
            moveProxy: false, //是否代理移动方式
            draggableHandler: 'header', //拖动触发焦点
            closeable: false, //是否关闭
            closeType: 'hide', //关闭类型 hide(隐藏，可重新show)/ destroy 直接从dom中删除
            expandable: false, //可左右收缩
            maxminable: false, //可变化小大
            collapseable: false, //上下收缩
            resizeable: false,//右下角拖拉大小
            onResized: null, //function (pr) { },//大小变化事件
            onLoaded: null, //function () { },//加载后
            onClose: null, //关闭前
            onClosed: null, //function () { },//关闭后
            onExpanded: null, // function (pr) { },//左右收缩后
            onCollapsed: null, // function (pr) { }//上下收缩后
            onCreated: null //function($content,$header){} panel创建完成事件
        }
    返回一个具有close(timeout) api的对象
    ***/
    window: function (args) {
        if (!$B.Panel) {
            alert("Panel is not find!");
            return;
        }
        var _$body, mask = true;
        if (args.isTop) {
            _$body = $B.DomUtils.css(window.top.document.body, { "position": "relative" });
        } else {
            _$body = $B.getBody();
        }
        var _bodyw = parseInt($B.DomUtils.outerWidth(_$body)),
            _bodyh = parseInt($B.DomUtils.outerHeight(_$body));
        if (typeof args.mask !== 'undefined') {
            mask = args.mask;
        }
        if (args.full) {
            mask = false;
            args.size = { width: _bodyw , height: _bodyh };
        } else if (!args.size) {
            args.size = { width: 600, height: 500 };
        }
        if(args.width){
            args.size.width = args.width;
        }
        if(args.height){
            args.size.height = args.height;
        }
        if(typeof args.size.width === "string" ){
            if(args.size.width.indexOf("%")){
                args.size.width = parseFloat(args.size.width.replace("%","")) / 100 * _bodyw;
            }else{
                args.size.width = parseFloat(args.size.width);
            }
        }
        if(typeof args.size.height === "string" ){
            if(args.size.height.indexOf("%")){
                args.size.height = parseFloat(args.size.height.replace("%","")) / 100 * _bodyh;
            }else{
                args.size.height = parseFloat(args.size.height);
            }
        }
        if (args.fixed || args.full) {
            args.draggable = false;
            args.collapseable = false;
            args.maxminable = false;
            args.resizeable = false;
        }
        if(!args.zIndex){
            args.zIndex = 2000000000;
        }
        let avH = _bodyh / 2;
        //自动内容高度
        var auto = typeof args.autoHeight !== "undefined" ? args.autoHeight : true;
        if (auto && args.content) {
            let cc = typeof args.content === "string" ? args.content : args.content.outerHTML;
            let visualWidth = args.size.width - 10;
            let visual = $B.DomUtils.createEl("<div style='position:absolute;top:-999999px;z-index:0;width:" + visualWidth + "px'>" + cc + "</div>");
            $B.DomUtils.append(_$body, visual);
            let h = visual.clientHeight + 2;
            if (h < 35) {
                h = 35;
            }
            if (typeof args.header === "undefined" || args.header) {
                h = h + 35;
            }
            let maxH = _bodyh - 100;
            if (h > maxH) {
                h = maxH;
            }
            $B.DomUtils.remove(visual);
            if (args.toolbar) {
                h = h + 35;
            }
            args.size.height = Math.floor( h);
        }
        var _l = (_bodyw - args.size.width) / 2;
        var _t = (_bodyh - args.size.height) / 2 ;
        avH = _bodyh / 5;
        if(_t > avH){
            _t = avH;
        }
        _t = _t + (window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0);
        var $mask = $B.DomUtils.getChildrenById(_$body, "k_window_mask_bg");
        var uuid = this.generateMixed(6);
        if (mask) {
            var mskCss = {};
            var maskWidth = parseInt(_$body.scrollWidth - 1);
            var maskHeight = parseInt(_$body.scrollHeight - 1);
            if (!$mask) {
                $mask = $B.DomUtils.createEl("<div style='z-index:" +  (args.zIndex - 2) + ";position:abosulte;width:" + maskWidth + "px;height:" + maskHeight + "px' for='" + uuid + "' id='k_window_mask_bg'></div>");
                $B.DomUtils.append(_$body, $mask);
            } else {
                mskCss = { width: maskWidth, height: maskHeight };
            }
            if (args.opacity) {
                mskCss["opacity"] = args.opacity;
            }
            if ($B.DomUtils.isHide($mask)) {
                mskCss["top"] = 0;
                $B.DomUtils.css($mask, mskCss);
                $B.DomUtils.show($mask);
                $B.DomUtils.attribute($mask, { "for": uuid });
            } else {
                $B.DomUtils.css($mask, mskCss);
            }
        }
        var $win = $B.DomUtils.createEl("<div  id='" + uuid + "' style='position:absolute;z-index:" + args.zIndex + ";' class='k_window_main_wrap'></div>");
        var posIsPlaintObj, bodyOverflow, velocity2pos;
        if (args.position) {
            posIsPlaintObj = $B.isPlainObjectFn(args.position);
            if (posIsPlaintObj) {
                $B.DomUtils.css($mask, args.position);
                $B.DomUtils.append(_$body, $win);
            } else {
                let winCss, hidepos = -888888;
                velocity2pos = args.position;
                if (args.position === "bottom") {
                    bodyOverflow = $B.DomUtils.css(_$body, "overflow");
                    $B.DomUtils.css(_$body, { "overflow": "hidden" });
                    winCss = {
                        bottom: hidepos,
                        right: 0
                    };
                } else {
                    winCss = {
                        top: hidepos,
                        left: _l
                    };
                }
                $B.DomUtils.css($win, winCss);
                $B.DomUtils.append(_$body, $win);
            }
        } else {
            if (_t < 1) {
                _t = 0;
            }
            if (_l < 1) {
                _l = 0;
            }
            $B.DomUtils.css($win, {
                top: _t,
                left: _l
            });
            $B.DomUtils.append(_$body, $win);
        }
        var panel, closeTimer,
            defOpt = $B.config && $B.config.winDefOpts ? $B.config.winDefOpts : {
                iconCls: "fa-popup", //图标class，font-awesome字体图标
                iconColor: "#6A50FC",//图标颜色
                shadow: true, //是否需要阴影
                radius: "2px", //圆角px定义
                header: true, //是否显示头部
                zIndex: 2147483647,//层级
                dataType: 'html', //当为url请求时，html/json/iframe
                triggerHide:true, //触发全局隐藏
                draggable: true, //是否可以拖动
                moveProxy: false, //是否代理移动方式
                draggableHandler: 'header', //拖动触发焦点
                closeable: true, //是否关闭
                closeType: 'destroy', //关闭类型 hide(隐藏，可重新show)/ destroy 直接从dom中删除
                expandable: false, //可左右收缩
                maxminable: true, //可变化小大
                collapseable: true, //上下收缩
                resizeable: true//右下角拖拉大小 
            };
        var opts = $B.extendObjectFn({}, defOpt, args);
        opts.onClosed = function () { //关闭后
            clearTimeout(closeTimer);
            if ($mask) {
                $B.DomUtils.hide($mask);
            }
            if (typeof args.onClosed === 'function') {
                setTimeout(() => {
                    args.onClosed();
                }, 1);
            }
        };
        if (args.timeout && args.timeout < 5) {
            opts.closeable = false;
        }
        if (args.isTop) {
            $B.DomUtils.css($win, { "top": -888888 });
            if (window.top.$B) {
                $B.DomUtils.attribute($win, { "istop": 1 });
                panel = new window.top.$B.Panel($win, opts);
                $B.velocity($win, { top: 1 }, { duration: 500 });
            }
        }
        if (!panel) {
            panel = new $B.Panel($win, opts);
        }
        if (args.timeout) {
            closeTimer = setTimeout(() => {
                if (opts.closeType === "destroy") {
                    panel.destroy();
                } else {
                    panel.close(true);
                }
            }, args.timeout * 1000);
        }
        if (velocity2pos) {
            let vcss, existEls, fix = 0;
            if (velocity2pos === "bottom") {
                existEls = $B.DomUtils.children(_$body, ".k_window_bottom_");
                vcss = { bottom: 0, right: 2 };
                $B.DomUtils.addClass(panel.elObj, "k_window_bottom_");
            } else {
                vcss = { top: 0 };
                existEls = $B.DomUtils.children(_$body, ".k_window_top_");
                $B.DomUtils.addClass(panel.elObj, "k_window_top_");
            }
            for (let i = 0; i < existEls.length; i++) {
                if ($B.DomUtils.css(existEls[i], "display") !== "none") {
                    fix = fix + existEls[i].clientHeight + 3;
                }
            }
            if (typeof vcss.bottom !== "undefined") {
                vcss.bottom = vcss.bottom + fix;
            } else {
                vcss.top = vcss.top + fix;
            }
            $B.velocity(panel.elObj, vcss, { duration: 360 });
        }
        return panel;
    },
    /***
     *{
            mask:false,
            title:'',
            position:'top',
            content:'提示信息！',
            closeable:false,
            width:宽度,
            onClosed:fn
     }
     *****/
    busyTip: function (args) {
        var title;
        let isStr = typeof args === "string";
        if (args) {
            title = isStr ? args : args.title;
        }
        if (!title) {
            title = $B.config ? $B.config.busyTipTitle : 'please waiting.......';
        }
        let content = "<div><p class='k_processing_top_tip' style='text-align:center;'><i class='fa fa-spin5 animate-spin'></i><span style='padding-left:10px'>" + title + "</span></p></div>";
        var ops = {
            header: false,
            mask: false,
            position: 'top',
            closeable: false, //是否关闭
            draggable: false, //是否可以拖动
            expandable: false, //可左右收缩
            maxminable: false, //可变化小大
            collapseable: false, //上下收缩
            resizeable: false,//右下角拖拉大小
            size: { width: 230 },
            content: content
        };
        if (args && !isStr) {
            ops = $B.extendObjectFn(ops, args);
        }
        return this.window(ops);
    },
    _getToolTipos: function (el, message, fixPos) {
        let shiftPos;
        if (fixPos && typeof fixPos !== "string") {
            shiftPos = fixPos;
            fixPos = undefined;
        }
        if(typeof el["fixpos"] === "object"){
            shiftPos = el["fixpos"];
            el["fixpos"] = undefined;
        }
        let w = 8, h = 8, pos;
        var _$body = $B.getBody();
        if ($B.DomUtils.isNode(el)) {
            w = w + $B.DomUtils.outerWidth(el);
            h = h + $B.DomUtils.outerHeight(el);
            pos = $B.DomUtils.offset(el);
        }
        let scrTop = $B.DomUtils.scrollTop(document);
        let scrLeft = $B.DomUtils.scrollLeft(document);
        let elLeft = pos.left;
        let elTop = pos.top;
        let bodyWidth = _$body.offsetWidth + scrLeft;
        let bodyHeight = _$body.offsetHeight + scrTop;
        let r = 0.95;
        if(bodyWidth > 800){
            r = 0.85;
        }else if(bodyWidth > 1000){
            r = 0.8;
        }else if(bodyWidth > 1300){
            r = 0.7;
        }else if(bodyWidth > 1500){
            r = 0.6;
        }
        var maxWidth =  bodyWidth * r  ;
        let visual = $B.DomUtils.createEl("<span class='k_tool_tip_helper k_box_size' style='max-width:" + maxWidth + "px;position:absolute;top:-10000px;left:-10000px;z-index:0;padding:5px'><p style=''><i style='' class='fa fa-info-1'></i><span style='padding-left:8px'>" + message + "</span></p></span>");
        $B.DomUtils.append(_$body, visual);
        let charWidth = Math.ceil($B.DomUtils.outerWidth(visual));
        let charHeight = Math.ceil($B.DomUtils.outerHeight(visual));
        $B.DomUtils.remove(visual);
        //检测放置合适的位置 transform: rotate(90deg)
        // 1，先检测右侧是否够空间，不够放下方，下方不够放上方，上方不够放右侧
        let isOk = false, iconCss;
        let avaiWidth = bodyWidth - pos.left - w;
        if ((!fixPos && avaiWidth > charWidth) || (fixPos === "right")) {
            isOk = true;
            pos.left = pos.left + w;
            let aviHeight = bodyHeight - pos.top;
            let diff = 0;
            if (aviHeight < charHeight) {
                diff = charHeight - aviHeight + 1;
                pos.top = pos.top - diff;
            }
            iconCss = {
                transform: "rotate(180deg)",
                top: diff + 'px',
                left: '-8px'
            };
        }
        if (!isOk) { //放下方           
            pos.left = bodyWidth - charWidth;
            if (pos.left > elLeft) {
                pos.left = elLeft;
            }
            pos.top = pos.top + h;
            let aviHeight = bodyHeight - pos.top;
            let diff = elLeft - pos.left + 2;
            if (diff < 2) {
                diff = 2;
            }
            iconCss = {
                transform: "rotate(270deg)",
                top: '-12px',
                left: diff + "px"
            };
            let isfixBottom = fixPos === "bottom";
            if (!isfixBottom) {
                //检测底部是否够空间
                if ((aviHeight < charHeight && !fixPos) || (fixPos === "top")) {
                    pos.top = elTop - charHeight - 6;
                    delete iconCss.top;
                    iconCss.bottom = "-12px";
                    iconCss.transform = "rotate(90deg)";
                }
            }
        }   
        if(shiftPos){
            if(shiftPos.top ){
                pos.top = pos.top + shiftPos.top;
            }
            if(shiftPos.left ){
                pos.left = pos.left + shiftPos.left;
            }
        }     
        return {
            pos: pos,
            iconCss: iconCss,
            maxWidth: maxWidth + 20,
            charWidth: charWidth
        };
    },
    /*****
     * pos:元素，或者位置
     * msgFn:提示信息/或者返回信息的函数
     * timeout:关闭时间 或者 up 、right 、bottom 位置信息
     * color:背景色
     * *******/
    toolTip: function (pos, msgFn, timeout,color) {
        let id = $B.getUUID();
        var _$body = $B.getBody();
        let targetEl = pos;
        if ($B.DomUtils.isNode(pos)) {
            let tipId = $B.DomUtils.attribute(pos, 'tooltip_id');
            if (tipId) {
                let el = $B.DomUtils.findbyId(_$body, tipId);
                if (el) {
                    $B.DomUtils.remove(el);
                }
            }
            $B.DomUtils.attribute(pos, { "tooltip_id": id });
        }
        let message = msgFn;
        if(typeof msgFn === "function"){
            message = msgFn();
        }
        let res = this._getToolTipos(pos, message, timeout);
        let charWidth = res.charWidth;
        let maxWidth = res.maxWidth;
        pos = res.pos;
        let widthcss;
        if (charWidth < maxWidth) {
            widthcss = "width:" + charWidth;
        } else {
            widthcss = "max-width:" + maxWidth;
        }
        let iconCss = res.iconCss;
        let txtHtml;
        if(message.indexOf("</") < 0){
            txtHtml = "<p style='text-align:left;padding:0;margin:0;'><i class='fa fa-info-1'></i><span style='padding-left:6px'>" + message + "</span></p>";
        }else{
            txtHtml = "<div>" + message + "</div>";
        }
        let el = $B.DomUtils.createEl("<div  id='" + id + "'  style='" + widthcss + "px;display:none;position:absolute;top:" + pos.top + "px;left:" + pos.left + "px;z-index:" 
        + $B.config.maxZindex + "' class='k_window_tooltip_wrap k_box_size'>"+txtHtml+"<div style='position:absolute;' class='k_tooltip_attrow'><i class='fa fa-play'></i></div></div>");
        let icon = $B.DomUtils.children(el, ".k_tooltip_attrow");
        $B.DomUtils.css(icon, iconCss);
        if(typeof color === "string"){
            el.style.backgroundColor = color;
            icon[0].firstChild.style.color = color;
        }
        $B.DomUtils.append(_$body, el);
        var closeTimer;
        $B.fadeIn(el, {
            duration: 200
        });
        var isTimeout = $B.isNumericFn(timeout);
        let ret = {
            fixPos: isTimeout ? undefined : timeout,
            elObj: el,
            targetEl: targetEl,
            message: msgFn,
            show: function (el, fixPos) {
                let fix =  this.fixPos;
                if (fixPos) {
                    fix = fixPos;
                }
                let msg = this.message;
                if(typeof  this.message === "function"){
                    msg = this.message();
                }
                let res = $B._getToolTipos(el, msg, fix);
                res.pos["max-width"] = res.maxWidth;
                $B.DomUtils.css(this.elObj, res.pos);
                let $icon = $B.DomUtils.children(this.elObj, ".k_tooltip_attrow");
                $B.DomUtils.css($icon, res.iconCss);
                this.elObj.firstChild.lastChild.innerHTML = msg;
                $B.fadeIn(this.elObj, {
                    duration: 200
                });
            },
            hide: function () {
                $B.fadeOut(this.elObj, {
                    duration: 200
                });
            },
            close: function (imdly) {
                let fn =()=>{
                    clearTimeout(closeTimer);
                    this.elObj.instance = undefined;
                    $B.DomUtils.remove(this.elObj);
                    this.message = undefined;
                    this.elObj = undefined;
                    this.close = undefined;
                    this.fixPos = undefined;
                    this.show = undefined;
                    this.hide = undefined;
                    this.targetEl = undefined;
                };
                if(imdly){
                    fn();
                }else{
                    $B.fadeOut(this.elObj, {
                        duration: 200,
                        complete: fn
                    });
                }                
            }
        };
        if (isTimeout) {
            closeTimer = setTimeout(() => {
                ret.close();
            }, timeout * 1000);
        }
        el.instance = ret;
        return ret;
    },
    /**
     * 元素绑定鼠标提示
     * el:元素
     * message:提示信息/或者返回信息的函数
     * fixPos :up 、right 、bottom 位置信息
     * color:背景色
     * **/
    mouseTip: function (el, message, fixPos,color) {
        var flag = $B.DomUtils.attribute(el, 'has_tooltip');
        if (!flag) {
            $B.DomUtils.attribute(el, { 'has_tooltip': 1 });
            $B.DomUtils.bind(el, {
                mouseenter: function (e) { 
                    var ins = $B.DomUtils.getData(this, "tooltipInd");
                    if (ins && ins.show) {
                        ins.show(this, fixPos);
                    } else {
                        ins = $B.toolTip(this, message, fixPos,color);
                        $B.DomUtils.setData(this, "tooltipInd", ins);
                    }
                },
                mouseleave: function (e) {
                    var ins = $B.DomUtils.getData(this, "tooltipInd");
                    if (ins && ins.hide) {
                        ins.hide();
                    }
                }
            });
        }
    },
    /**
     *成功信息
    *arg={
            title:'请您确认',
            mask:false,
            position:'top',
            iconCls:'图标样式',
            iconColor:
            content:'提示信息！',
            toolbar:[],//工具栏，如果传入，则不生成默认的按钮
            contentIcon:'内容区域的图标',
            width:宽度
    }
    ***/
    success: function (args) {
        var opts = this._getWinOpt.apply(this, arguments);
        var title = typeof args !== "undefined" ? args.title : undefined;
        if (!title) {
            title = $B.config ? $B.config.successTitle : 'success message';
        }
        opts = $B.extendObjectFn({}, { title: title, iconCls: 'fa-check', iconColor: '#08E358', contentIcon: 'fa-ok-circled' }, opts);
        var win = this._window(opts, "#08E358");
        return win;
    },
    /**
     * message 警告信息对话框
     * opts = {
            title:'请您确认',
            mask:false,
            position:'top',
            iconCls:'图标样式',
            iconColor:
            content:'提示信息！',
            toolbar:[],//工具栏，如果传入，则不生成默认的按钮
            contentIcon:'内容区域的图标',
            width:宽度
    }
    * ***/
    alert: function (args) {
        var opts = this._getWinOpt.apply(this, arguments);
        var title = typeof args !== "undefined" ? args.title : undefined;
        if (!title) {
            title = $B.config ? $B.config.alertTitle : 'alert message';
        }
        opts = $B.extendObjectFn({}, { title: title, iconCls: 'fa-attention-1', iconColor: '#EDA536', contentIcon: 'fa-attention-alt' }, opts);
        var win = this._window(opts, "#EDA536");
        return win;
    },
    /**
     * message 错误信息对话框
     * opts = {
            title:'请您确认',
            mask:false,
            position:'top',
            iconCls:'图标样式',
            iconColor:
            content:'提示信息！',
            toolbar:[],//工具栏，如果传入，则不生成默认的按钮
            contentIcon:'内容区域的图标',
            width:宽度
    }
    * ***/
    error: function (args) {
        var opts = this._getWinOpt.apply(this, arguments);
        var title = typeof args !== "undefined" ? args.title : undefined;
        if (!title) {
            title = $B.config ? $B.config.errorTitle : 'error message';
        }
        opts = $B.extendObjectFn({}, { title: title, iconCls: 'fa-cancel-circled-1', iconColor: '#F72EA7', contentIcon: 'fa-emo-unhappy' }, opts);
        var win = this._window(opts, "#F72EA7");
        return win;
    },
    /**
     *信息提示框
    *args={
            title:'请您确认',
            mask:false,
            position:'top',
            iconCls:'图标样式',
            iconColor:
            content:'提示信息！',
            toolbar:[],//工具栏，如果传入，则不生成默认的按钮
            contentIcon:'内容区域的图标',
            width:宽度
    }
    ***/
    message: function (args) {
        var opts = this._getWinOpt.apply(this, arguments);
        var title = typeof args !== "undefined" ? args.title : undefined;
        if (!title) {
            title = $B.config ? $B.config.messageTitle : 'message';
        }
        opts = $B.extendObjectFn({}, { title: title, iconCls: 'fa-comment', iconColor: '#3BA7EA', contentIcon: 'fa-comment-1' }, opts);
        var win = this._window(opts, "#3BA7EA");
        return win;
    },
    _getWinOpt: function (args) {
        var opts = {
            width: args.width ? args.width : 400,
            title: undefined, //标题
            shadow: typeof args.shadow !== "undefined" ? args.shadow : true, //是否需要阴影
            mask: typeof args.mask !== "undefined" ? args.mask : true, //是否需要遮罩层
            position: typeof args.position !== "undefined" ? args.position : undefined,
            timeout: args.timeout,
            onClosed:typeof args.onClosed === "function" ? args.onClosed : undefined,
            collapseable: false,
            maxminable: false,
            resizeable: false,
            closeable: typeof args.closeable !== "undefined" ? args.closeable : true
        };
        if (args.iconCls) {
            opts.iconCls = args.iconCls;
        }
        if (args.iconColor) {
            opts.iconColor = args.iconColor;
        }
        if (args.contentIcon) {
            opts.contentIcon = args.contentIcon;
        }
        var content;
        if (typeof args === "string") {
            content = args;
            if (arguments.length >= 2) {
                if ($B["isNumericFn"](arguments[1])) {
                    opts.timeout = arguments[1];
                } else {
                    opts.position = arguments[1];
                }
            }
        } else if (args.content) {
            content = args.content;
        }
        opts.content = content;
        if (typeof args.toolbar !== "undefined") {
            opts["toolbar"] = args.toolbar;
        }
        return opts;
    },
    /**
     * 确认提示框
     * args={
            title:'请您确认',
            iconCls:'图标样式',
            content:'提示信息！',
            toolbar:[],//工具栏，如果传入，则不生成默认的按钮
            contentIcon:'内容区域的图标',
            width:width ,//宽度        
            okIcon :"fa-ok-circled",
            noIcon : "fa-ok-circled",
            okText : "确认",           
            noText : "取消",
            okFn:fn, //确认回调
            noFn:fn, //否定回调
    }
    * ***/
    confirm: function (args) {        
        let title = args.title;
        if (!title) {
            title = $B.config ? $B.config.confirmTitle : 'please confirm';
        }
        var opts = {
            size: { width: args.width ? args.width : 400 },
            title: title, //标题
            iconCls: args.iconCls ? args.iconCls : 'fa-question', //图标cls，对应icon.css里的class
            shadow: typeof args.shadow !== "undefined" ? args.shadow : true, //是否需要阴影
            mask: typeof args.mask !== "undefined" ? args.mask : true, //是否需要遮罩层
            iconColor: typeof args.iconColor !== "undefined" ? args.iconColor : undefined,
            position: typeof args.position !== "undefined" ? args.position : undefined,
            collapseable: false,
            maxminable: false,
            resizeable: false,
            contentIcon: args.contentIcon ? args.contentIcon : "fa-help-1"
        };
        var okFn = args.okFn,
            noFn = args.noFn;
        var okIcon = args.okIcon ? args.okIcon : "fa-ok-circled",
            okText = args.okText ? args.okText : $B.config ? $B.config.buttonOkText : 'submit',
            noIcon = args.noIcon ? args.noIcon : "fa-reply-all",
            noText = args.noText ? args.noText : $B.config ? $B.config.buttonCancleText : 'cancel';
        var content, win;
        if (typeof args === "string") {
            content = args;
            if (arguments.length >= 2) {
                if (typeof arguments[1] === "function") {
                    okFn = arguments[1];
                }
            }
            if (arguments.length === 3) {
                if (typeof arguments[2] === "function") {
                    noFn = arguments[2];
                }
            }
            if (arguments.length === 4) {
                if (typeof arguments[3] === "function") {
                    opts.onClosed = arguments[3];
                }
            }
        } else if (args.content) {
            content = args.content;
        }
        opts.content = content;
        if (typeof args.toolbar === "function") {
            opts["toolbar"] = args.toolbar;
        } else {
            opts["toolbar"] = function () {
                var wrap = this;
                setTimeout(() => { $B.DomUtils.css(wrap, { "padding-bottom": 0 }); }, 1);
                var tool = $B.DomUtils.append(wrap, "<div class='k_confirm_buttons_wrap'></div>");
                var ybtn = $B.DomUtils.append(tool, "<button class='yes'><i   class='fa " + okIcon + "'>\u200B</i>" + okText + "</button>");
                $B.DomUtils.click(ybtn, function () {
                    var goClose = true;
                    if (typeof okFn === "function") {
                        var res = okFn();
                        if (typeof res !== "undefined") {
                            goClose = res;
                        }
                    }
                    if (goClose) {
                        win.close(true);
                    }
                });
                var nbtn = $B.DomUtils.append(tool, "<button class='no'><i  class='fa " + noIcon + "'>\u200B</i>" + noText + "</button>");
                $B.DomUtils.click(nbtn, function () {
                    var goClose = true;
                    if (typeof noFn === "function") {
                        var res = noFn();
                        if (typeof res !== "undefined") {
                            goClose = res;
                        }
                    }
                    if (goClose) {
                        win.close(true);
                    }
                });
                setTimeout(() => {
                    let bg = $B.DomUtils.css(ybtn, "background-color");
                    if ($B.isDeepColor(bg)) {
                        ybtn.style.color = "#ffffff";
                    } else {
                        ybtn.style.color = "#1A242C";
                    }
                    bg = $B.DomUtils.css(nbtn, "background-color");
                    if ($B.isDeepColor(bg)) {
                        nbtn.style.color = "#ffffff";
                    } else {
                        nbtn.style.color = "#1A242C";
                    }
                }, 0);
                return tool;
            };
        }
        if(args.onClosed){
            opts.onClosed = args.onClosed;
        }
        win = this._window(opts, '#B2894B');
        return win;
    },
    _window: function (args, iconColor) {
        let title = args.title;
        if (!title) {
            title = $B.config ? $B.config.confirmTitle : 'please set windwo title';
        }
        var opts = {
            size: { width: args.width ? args.width : 400 },
            title: title, //标题
            iconCls: args.iconCls ? args.iconCls : 'fa-question', //图标cls，对应icon.css里的class
            shadow: typeof args.shadow !== "undefined" ? args.shadow : true, //是否需要阴影
            mask: typeof args.mask !== "undefined" ? args.mask : true, //是否需要遮罩层
            timeout: typeof args.timeout !== "undefined" ? args.timeout : undefined,
            iconColor: typeof args.iconColor !== "undefined" ? args.iconColor : undefined,
            position: typeof args.position !== "undefined" ? args.position : undefined,
            collapseable: false,
            maxminable: false,
            resizeable: false,
            closeable: args.closeable,
            toolbar: args.toolbar
        };
        var win,
            contentIcon = args.contentIcon ? args.contentIcon : "fa-help-1";
        var content;
        if (typeof args === "string") {
            content = args;
        } else if (args.content) {
            content = args.content;
        }
        let id = "c" + $B.generateMixed(8);
        if (typeof ccontent === "string" || !content) {
            content = '<div style="position:relative;min-height:35px"><div id="' + id + '" class="k_box_size" style="border-left:50px solid #fff;"><p>' + content + '</p></div><div  class="k_box_size" style="position:absolute;top:2px;left:1px; height:100%;padding:0px 10px;"><i style="line-height:35px;font-size:32px;color:' + iconColor + '" class="fa ' + contentIcon + '"></i></div></div>';
        } else {
            let el = content;
            content = $B.DomUtils.createEl('<div style="position:relative;min-height:35px"><div id="' + id + '" class="k_box_size" style="border-left:50px solid #fff;"><p></p></div><div  class="k_box_size" style="position:absolute;top:2px;left:1px; height:100%;padding:0px 10px;"><i style="line-height:35px;font-size:32px;color:' + iconColor + '" class="fa ' + contentIcon + '"></i></div></div>');
            let pel = $B.DomUtils.findByTagName(content, "p");
            $B.DomUtils.append(pel, el);
        }
        opts.content = content;
        opts.onCreated = function ($body) {
            let sp = $B.DomUtils.findbyId($body, id);
            let $p = $B.DomUtils.children(sp, "p")[0];
            if ($p.clientHeight < 30) {
                $B.DomUtils.css($p, { "line-height": "36px" });
            }
        };
        opts.onClosed = args.onClosed;
        win = this.window(opts);
        return win;
    }
});