/*! BUI - v2.0.0 - 2022-03-13 09:03:17 
Copyright (c): kevin.huang  https://gitee.com/kevin-huang/bui2.0.git (qq:757723114) 
Released under MIT License*/
(function ( global, factory) {
//   "use strict";
	if ( typeof module === "object" && typeof module.exports === "object" ) {
		module.exports = global.document ?
			factory( global, true ) :
			function( w ) {
				if ( !w.document ) {
					throw new Error( "Bui requires a window with a document" );
				}
				return factory( w );
			};
	} else {
		  factory( global );
	}  
})( typeof window !== "undefined" ? window : this, function( window, noGlobal ) {
//   "use strict";
  var $B = window["$B"] ?  window["$B"] : {};


var defaultOpts = {
    style: 'v',  //v 垂直风格，h水平风格
    hstyleHeight: '35px',
    activedCls: 'k_nav_v_actived',  
    backgroundColor: undefined, //背景色
    fontColor: undefined,//图标字体颜色
    hoverColor: undefined, //鼠标移动上来的选项背景色
    activedColor: undefined,//激活选项的背景色
    actFontColor: undefined,//激活状态的图标字体颜色
    datas: undefined
};
function createHoverStyle(styleClzz, clazzName, backgorundColor) {
    var styleCtx = clazzName + '{background-color:' + backgorundColor + '!important;}';
    $B.createHeaderStyle(styleClzz, styleCtx);
}
class Nav extends $B.BaseControl {
    constructor(elObj, opts) {
        let wrap = $B.DomUtils.createEl("<div class='k_nav_wrap k_box_size'></div>");
        if (typeof elObj === "string") {
            elObj = $B.DomUtils.findbyId(elObj);
            $B.DomUtils.append(elObj, wrap);
            elObj = wrap;
        }
        super();
        super.setElObj(elObj);
        this.idIndex = 1;
        this.allSubMenus = [];
        this.extAllSubMenus = [];
        this.opts = $B.extendObjectFn(true, {}, defaultOpts, opts);
        if (this.opts.backgroundColor) {
            $B.DomUtils.css(elObj, { "background-color": this.opts.backgroundColor });
        }
        if (this.opts.hoverColor) {
            this.hoverClass = this.id;
            createHoverStyle(this.hoverClass, "." + this.hoverClass, this.opts.hoverColor);
        }
        if (this.opts.style === "v") {
            $B.DomUtils.css(this.elObj, { "width": "100%", "height": "100%" });
            this.$body = $B.myScrollbar(this.elObj, {});
            if(this.opts.bodyCss){
                $B.DomUtils.css(this.$body, this.opts.bodyCss);
            }            
            this.createVNav(this.$body, this.opts.datas, 0);
            this.onClickV = (e) => {
                let el = e.target;
                while (el) {
                    if ($B.DomUtils.hasClass(el, "k_nav_v_item")) {
                        break;
                    }
                    el = el.parentNode;
                }
                if(el){
                    let innerwrap = el.firstChild;
                    if (this.opts.click && !e.notfire) {
                        this._fireBtnClick(el);
                    }
                    if ($B.DomUtils.attribute(el, "isprt")) {
                        if ($B.DomUtils.isHide(el.nextSibling)) {
                            $B.animate(innerwrap.firstChild.firstChild, { "rotateZ": "0deg" }, { duration: 200 });
                            $B.slideDown(el.nextSibling, 200);
                        } else {
                            $B.animate(innerwrap.firstChild.firstChild, { "rotateZ": "270deg" }, { duration: 200 });
                            $B.slideUp(el.nextSibling, 200);
                        }
                    } else {
                        this.clearActivedStyle();
                        this.activedItem = el;
                        this.setActivedItemStyle();
                    }
                } else{
                    console.log("el is not found",e);
                }               
            };
            $B.DomUtils.click(this.$body, this.onClickV);
        } else {//水平导航
            $B.DomUtils.css(this.elObj, { "width": "100%" });
            this.$body = $B.DomUtils.createEl("<div style='height:" + this.opts.hstyleHeight + ";width:100%;overflow:hidden;' class='k_nav_h_wrap k_box_size'></div>");
            if(this.opts.bodyCss){
                $B.DomUtils.css(this.$body, this.opts.bodyCss);
            } 
            this.createHNav(this.opts.datas);
            if (this.opts.backgroundColor) {
                $B.DomUtils.css(this.$body, { "background-color": this.opts.backgroundColor });
            }
            $B.DomUtils.append(this.elObj, this.$body);
            this.autoMoreItems();
        }
        $B.DomUtils.resize(window, (e) => {
            //console.log("resize >>>>>>>>>>>>");
            this.autoMoreItems();
        });
    }
    clearActivedStyle(){
        if (this.activedItem) {
            //清理背景色
            if (this.opts.activedColor) {
                let styleAttr = $B.DomUtils.attribute(this.activedItem, "style");
                let styleObj = $B.style2cssObj(styleAttr);
                delete styleObj["background-color"];
                styleAttr = $B.cssObj2string(styleObj);
                $B.DomUtils.attribute(this.activedItem, { "style": styleAttr });
            }
            //清理字体颜色
            this.setItemColor(this.opts.fontColor, this.activedItem);
            $B.DomUtils.removeClass(this.activedItem, this.opts.activedCls);
        }
    }
    setActivedItemStyle(){
        $B.DomUtils.addClass(this.activedItem, this.opts.activedCls);
        this.setItemColor(this.opts.actFontColor, this.activedItem);
        if (this.opts.activedColor) {
            $B.DomUtils.css(this.activedItem, { "background-color": this.opts.activedColor });
        }
    }
    setActById(id){
        if (this.opts.style === "v") {
            if(!this.shortId){
                id = "nav_"+id;
            }
            let el =  $B.DomUtils.findbyId(this.elObj,id);
            if(this.activedItem && this.activedItem === el){
                return;
            }
            this.onClickV({target:el,notfire:true});
        }else{
            console.log("尚不支持水平导航！");
        }
    }
    /****
     * 设置重直ui风格的点击项目
     * ****/
    setItemColor(fColor, it) {
        var childs = $B.DomUtils.children(it.firstChild);
        for (let i = 0; i < childs.length; i++) {
            let cl = childs[i];
            if ($B.DomUtils.hasClass(cl, ".k_nav_v_more")) {
                cl = cl.firstChild;
            }
            let styleAttr = $B.DomUtils.attribute(cl, "style");
            if (styleAttr) {
                let styleObj = $B.style2cssObj(styleAttr);
                if (fColor) {
                    styleObj.color = fColor;
                } else {
                    delete styleObj.color;
                }
                styleAttr = $B.cssObj2string(styleObj);
                $B.DomUtils.attribute(cl, { "style": styleAttr });
            }
        }
    }
    /**
     * 绑定垂直风格的事件
     * ***/
    bindItemEvents(it) {
        if (this.hoverClass) {
            if (!this.itemEvents) {
                this.itemEvents = {
                    mouseenter: (e) => {
                        $B.DomUtils.addClass(e.target, this.hoverClass);
                    },
                    mouseleave: (e) => {
                        $B.DomUtils.removeClass(e.target, this.hoverClass);
                    }
                };
            }
            $B.DomUtils.bind(it, this.itemEvents);
        }
    }
    /***
     * 创建垂直风格的Dom UI
     * ****/
    createVNav(wrap, datas, padding) {
        let html = "<div  style='padding-left:" + padding + "px' class='k_nav_v_item k_box_size'><div style='padding-left:12px;'><span></span></div></div>";
        let activedIt;       
        for (let i = 0; i < datas.length; i++) {
            let data = datas[i];
            let it = $B.DomUtils.createEl(html);
            this.bindItemEvents(it);
            let subId;
            if (data.id) {
                subId = "nav_" + data.id;
            } else {
                if (!this.shortId) {
                    this.shortId = $B.getShortID();
                }
                subId = this.shortId + "_" + this.idIndex;
                this.idIndex++;
            }
            data["id"] = subId;
            let attr = { id: subId };
            let inner = it.firstChild;
            inner.firstChild.innerHTML = data.text;
            $B.DomUtils.append(wrap, it);
            it = inner;
            if (data.children) {
                attr["isprt"] = 1;
                let childWrap = $B.DomUtils.createEl("<div style=''></div>");
                let iCls = 'fa-angle-down';
                let icss = "";
                if (data.closed) {
                    childWrap.style.display = "none";
                    icss = "transform: rotateZ(270deg);";
                }
                $B.DomUtils.prepend(it, "<span class='k_nav_v_more'><i style='" + icss + "' class='fa " + iCls + "'></i></span>");
                $B.DomUtils.append(wrap, childWrap);
                this.createVNav(childWrap, data.children, padding + 18);
            } else {
                if (data.iconCls) {
                    $B.DomUtils.prepend(it, "<i class='fa " + data.iconCls + " k_nav_v_icon'></i>");
                } else if (data.data.menuIconCss) {
                    $B.DomUtils.prepend(it, "<i class='fa " + data.data.menuIconCss + " k_nav_v_icon'></i>");
                }
            }
            if (this.opts.fontColor) {
                var childs = $B.DomUtils.children(it);
                for (let j = 0; j < childs.length; j++) {
                    let cl = childs[j];
                    if ($B.DomUtils.hasClass(cl, ".k_nav_v_more")) {
                        $B.DomUtils.css(cl.firstChild, { color: this.opts.fontColor });
                    } else {
                        $B.DomUtils.css(cl, { color: this.opts.fontColor });
                    }
                }
            }
            $B.DomUtils.attribute(it.parentNode, attr);
            if (data.actived) {
                activedIt = it.parentNode;
            }
        }
        if (activedIt) {
            setTimeout(() => {
                this.onClickV({ target: activedIt });
            }, 1);
        }
    }
    /**
     * 多级菜单时候，根据id隐藏子菜单
     * ***/
    _hideSubMenus(id) {
        for (let i = 0; i < this.allSubMenus.length; i++) {
            if ($B.DomUtils.attribute(this.allSubMenus[i], "id").indexOf(id) > -1) {
                $B.DomUtils.hide(this.allSubMenus[i]);
            }
        }
    }
    /***
     * 水平一级菜单事件
     * ****/
    bindHeVENTS(el) {
        if (!this.hmouseEvents) {
            var _this = this;
            this.hmouseEvents = {
                mouseenter: (e) => {
                    let el = e.target;
                    if ($B.DomUtils.hasClass(el, "k_nav_h_item_prt")) {
                        let id = $B.DomUtils.attribute(el, "id") + "_c";
                        let it;
                        for (let i = 0; i < _this.allSubMenus.length; i++) {
                            if ($B.DomUtils.attribute(_this.allSubMenus[i], "id") === id) {
                                it = _this.allSubMenus[i];
                            } else {
                                _this.allSubMenus[i].style.display = "none";
                            }
                        }
                        clearTimeout(_this._hideSubMenusTimer);
                        if ($B.DomUtils.isHide(it)) {
                            var ofs = $B.DomUtils.offset(el);
                            var height = $B.DomUtils.outerHeight(el);
                            var width = $B.DomUtils.outerWidth(el);
                            ofs.top = ofs.top + height + 1;
                            $B.DomUtils.css(it, ofs);
                            $B.slideDown(it, 200, () => {
                                if ($B.DomUtils.outerWidth(it) < width) {
                                    $B.DomUtils.outerWidth(it, width);
                                }
                            });
                        }
                    } else {
                        _this.hideAllSubMenus(true);
                    }
                },
                mouseleave: (e) => {
                    let el = e.target;
                    if ($B.DomUtils.hasClass(el, "k_nav_h_item_prt")) {
                        let id = $B.DomUtils.attribute(el, "id") + "_c";
                        _this._hideSubMenusTimer = setTimeout(() => {
                            _this._hideSubMenus(id);
                        }, 500);
                    }
                },
                click: (e) => {
                    let el = e.target;
                    while (el) {
                        if ($B.DomUtils.hasClass(el, "k_nav_h_item")) {
                            break;
                        }
                        el = el.parentNode;
                    }
                    _this._clearAllSubAct();
                    _this._move2acitived(el);
                    this.activedItem = el;
                    _this._fireBtnClick(el);
                    this._renderExtActUI();
                }
            };
        }
        $B.DomUtils.bind(el, this.hmouseEvents);
    }
    /***移动激活的下边线***/
    _moveActviedLine(id) {
        let firstClzEl = $B.DomUtils.findbyId(this.$body, id);
        this._move2acitived(firstClzEl);
    }
    //移动激活下划线
    _move2acitived(el) {
        if (!this.$activedHelper) {
            this.$activedHelper = $B.DomUtils.createEl("<div style='position:absolute;height:2px;background:" + this.opts.activedColor + ";bottom:0;left:0;display:none;'></div>");
            $B.DomUtils.append(this.$body, this.$activedHelper);
        }
        let left = 16;
        let $txt = el.firstChild;
        let w = $B.DomUtils.outerWidth($txt);
        let prevEl = el.previousSibling;
        while (prevEl) {
            left = left + $B.DomUtils.outerWidth(prevEl);
            prevEl = prevEl.previousSibling;
        }
        if (this.$activedHelper.style.display === "none") {
            this.$activedHelper.style.left = "0px";
            this.$activedHelper.style.display = "block";
        }
        $B.animate(this.$activedHelper, { left: left + "px", "width": w + "px" }, { duration: 360 });
        this.$activeFirstMenu = el;
    }
    createHNav(datas) {
        let html = "<div class='k_nav_h_item k_box_size'><span class='_lable_'></span></div>";
        for (let i = 0; i < datas.length; i++) {
            let data = datas[i];
            let subId;
            if (data.id) {
                subId =  "nav_" + data.id ;
            } else {
                if (!this.shortId) {
                    this.shortId = $B.getShortID();
                }
                subId = this.shortId + "_" + this.idIndex;
                this.idIndex++;               
            }      
            data.id = subId;      
            let attr = { id: subId, index: i };
            let it = $B.DomUtils.createEl(html);
            $B.DomUtils.attribute(it, attr);
            it.firstChild.innerHTML = data.text;
            it.firstChild.style.lineHeight = this.opts.hstyleHeight;
            $B.DomUtils.append(this.$body, it);
            if (data.children) {
                $B.DomUtils.addClass(it, "k_nav_h_item_prt");
                $B.DomUtils.append(it, "<span  style='position:absolute;right:1px;display:inline-block;height:100%;line-height:" + this.opts.hstyleHeight + "'><i class='fa fa-angle-down'></i></span>");
                let $w = this.createChildMenu(subId, data.children);
                $B.DomUtils.addClass($w, "_first_");
            } else {
                $B.DomUtils.addClass(it, "k_nav_h_item_child");
                if (data.actived) {
                    this.activedItem = it;
                }
            }
            this.bindHeVENTS(it);
        }
        if (this.activedItem) {
            setTimeout(() => {
                $B.DomUtils.trigger(this.activedItem, "click");
            }, 100);
        }
    }
    /**
     * 水平子菜单事件
     * ***/
    bindSubMenuEv(it) {
        if (!this.subEvents) {
            this.subEvents = {
                mouseleave: (e) => {
                    this.mmHiderTimer = setTimeout(() => {
                        this.hideAllSubMenus();
                    }, 500);
                },
                mouseenter: (e) => {
                    clearTimeout(this.mmHiderTimer);
                    let el = e.target;
                    let i = $B.DomUtils.children(el, ".fa-ellipsis");
                    let btnId = $B.DomUtils.attribute(el, "id");
                    if (i.length > 0) {
                        let btnId = $B.DomUtils.attribute(el, "id");
                        let id = btnId + "_c";
                        for (let i = 0; i < this.allSubMenus.length; i++) {
                            let $w = this.allSubMenus[i];
                            let wId = $B.DomUtils.attribute($w, "id");
                            if (wId === id) {
                                if ($w.style.display === "none") {
                                    var ofs = $B.DomUtils.offset(el);
                                    var w = $B.DomUtils.outerWidth(el);
                                    var h = $B.DomUtils.outerHeight(el);
                                    ofs.top = ofs.top + h / 2;
                                    ofs.left = ofs.left + w - 2;
                                    $B.DomUtils.position($w, ofs);
                                    $B.DomUtils.detach($w);
                                    $B.DomUtils.append($B.getBody(), $w);
                                    $B.slideDown($w, 200);
                                }
                            } else if (btnId.indexOf(wId) < 0) {
                                $w.display = "none";
                            }
                        }
                    } else {
                        let tmpArr = btnId.split("_");
                        tmpArr.pop();
                        let wrpId = tmpArr.join("_");
                        for (let i = 0; i < this.allSubMenus.length; i++) {
                            let $w = this.allSubMenus[i];
                            let wId = $B.DomUtils.attribute($w, "id");
                            if (wId.indexOf(wrpId) === 0 && wId !== wrpId) {
                                $w.style.display = "none";
                            }
                        }
                    }
                },
                click: (e) => {
                    let el = e.target;
                    while (el) {
                        if (el.tagName === "BUTTON") {
                            break;
                        }
                        el = el.parentNode;
                    }
                    this._clearActItem();
                    if (this.opts.click) {
                        this._fireBtnClick(el);
                    }
                    this.activedItem = el;
                    //更多控制按钮存在index
                    let index = $B.DomUtils.attribute(el, "index");
                    if (index) {
                        index = parseInt(index);
                        let siblingsTtns = $B.DomUtils.siblings(el);
                        for (let i = 0; i < siblingsTtns.length; i++) {
                            this._clearColor(siblingsTtns[i]);
                        }
                        let children = Array.from(this.$body.children);
                        this._move2acitived(children[index]);
                    }
                    let tels = $B.DomUtils.children(this.activedItem, "._lable_");
                    tels[0].style.color = this.opts.activedColor;
                    if (tels[0].previousSibling) {
                        tels[0].previousSibling.style.color = this.opts.activedColor;
                    }

                }
            };
        }
        $B.DomUtils.bind(it, this.subEvents);
    }
    _fireBtnClick(el) {
        let id = $B.DomUtils.attribute(el, "id");
        let opt = this._getOptById(id);
        if (el.tagName === "BUTTON") {
            this._clearAllSubAct();
            this._ativedUiRender(el);
        }
        setTimeout(() => {
            this.opts.click.call(el, opt);
        }, 1);
        if (this.opts.style === "h") {
            this.hideAllSubMenus();
        }
    }
    /*****/
    _clearActItem() {
        if (this.activedItem) {
            let tels = $B.DomUtils.children(this.activedItem, "._lable_");
            if (tels.length > 0) {
                let $s = tels[0];
                this._clearColor($s);
                if ($s.previousSibling) {
                    this._clearColor($s.previousSibling);
                }
                if ($s.nextSibling) {
                    this._clearColor($s.nextSibling);
                }
            }
        }
    }
    /**清理所有激活**/
    _clearAllSubAct() {
        this._clearActItem();
        for (let i = 0; i < this.allSubMenus.length; i++) {
            let $w = this.allSubMenus[i];
            this._clearIActived(Array.from($w.children));
        }
        if (this.$extMenuContainer && this.$extMenuContainer.children) {
            this._clearIActived(Array.from(this.$extMenuContainer.children));
        }
    }
    /**
     * 循环元素进行激活ui恢复
     * **/
    _clearIActived(siblings) {
        for (let j = 0; j < siblings.length; j++) {
            let $s = siblings[j];
            let el = $s.firstChild;
            this._clearColor(el);
            el = el.nextSibling;
            while (el) {
                this._clearColor(el);
                el = el.nextSibling;
            }
        }
    }
    /***根据点击的按钮元素，向上递归激活UI
     * ***/
    _ativedUiRender(btnEl) {
        let wrap = btnEl.parentNode;
        let wrapId = $B.DomUtils.attribute(wrap, "id");
        let tmpArr = wrapId.split("_");
        tmpArr.pop();
        let parentBtnId = tmpArr.join("_");
        tmpArr = parentBtnId.split("_");
        tmpArr.pop();
        let parentWrapId = tmpArr.join("_");
        if ($B.DomUtils.hasClass(wrap, "_first_")) {
            this._moveActviedLine(parentBtnId);
            this._renderExtActUI(parentBtnId);
            return;
        }
        for (let i = 0; i < this.allSubMenus.length; i++) {
            let $w = this.allSubMenus[i];
            let wId = $B.DomUtils.attribute($w, "id");
            if (parentWrapId === wId) {
                let pBtn = $B.DomUtils.findbyId($w, parentBtnId);
                pBtn.lastChild.style.color = this.opts.activedColor;
                if (!$B.DomUtils.hasClass($w, "_first_")) {
                    this._ativedUiRender(pBtn);
                } else {
                    tmpArr = wId.split("_");
                    tmpArr.pop();
                    wId = tmpArr.join("_");
                    this._moveActviedLine(wId);
                    this._renderExtActUI(wId);
                }
                break;
            }
        }
    }
    _renderExtActUI(id) {
        if (this.$extMenuContainer && id) {
            let el = $B.DomUtils.findbyId(this.$extMenuContainer, id);
            if (el) {
                el.lastChild.style.color = this.opts.activedColor;
                this.$extMore.firstChild.style.color = this.opts.activedColor;
            } else {
                if (this.$extMore) {
                    this._clearColor(this.$extMore.firstChild);
                }
            }
        } else {
            if (this.$extMore) {
                this._clearColor(this.$extMore.firstChild);
            }
        }
    }
    _clearColor(el) {
        let cssObj = $B.style2cssObj(el);
        delete cssObj.color;
        let cssattr = $B.cssObj2string(cssObj);
        $B.DomUtils.attribute(el, { "style": cssattr });
    }
    createChildMenu(id, datas) {
        id = id + "_c";
        let $wrp = $B.DomUtils.createEl("<div style='display:none;position: absolute;z-index:" + $B.config.maxZindex + "' id='" + id + "' class='ext_mm_container k_context_menu_container k_box_shadow k_box_size'></div>");
        if (!this.subWrpEvs) {
            this.subWrpEvs = {
                mouseleave: (e) => {
                    let el = e.target;
                    let id = $B.DomUtils.attribute(el, "id");
                    this._hideSubMenusTimer = setTimeout(() => {
                        this._hideSubMenus(id);
                    }, 500);
                },
                mouseenter: (e) => {
                    clearTimeout(this._hideSubMenusTimer);
                    clearTimeout(this.detachExtTimer);
                }
            };
        }
        $B.DomUtils.bind($wrp, this.subWrpEvs);
        this._createSubBtns(datas, id, $wrp, (id, childs) => {
            this.createChildMenu(id, childs);
        });
        $B.DomUtils.append($B.getBody(), $wrp);
        this.allSubMenus.push($wrp);
        return $wrp;
    }
    _createSubBtns(datas, id, $wrp, onCreateChilds) {
        let idx = this.overIdx;
        for (let i = 0; i < datas.length; i++) {
            let it = datas[i];
            if (!it["id"]) {
                it["id"] = id + "_" + i;
            }
            let text = it.text;
            let icon = it.iconCls;
            let btn = $B.DomUtils.createEl("<button class='k_toolbar_button_plain' id='" + it.id + "' style='white-space: nowrap; border-radius: 0px; display: block; margin: 0px 0px 3px; min-width: 100%; text-align: left;'></button>");
            if (icon) {
                $B.DomUtils.append(btn, '<i class="fa ' + icon + '"></i>');
            }
            if (id === "k_nav_more_btn") {
                $B.DomUtils.attribute(btn, { index: idx });
                idx++;
            }
            $B.DomUtils.append(btn, '<span class="_lable_" style="white-space: nowrap; padding-left: 5px;">' + text + '</span>');
            if (it.children) {
                $B.DomUtils.attribute(btn, { haschild: "1" });
                $B.DomUtils.append(btn, '<i style="position:relative;font-size:10px;padding-right:-8px;padding-left:5px;top:0.5em;" class="fa fa-ellipsis"></i>');
                onCreateChilds(it.id, it.children);
            }
            $B.DomUtils.append($wrp, btn);
            this.bindSubMenuEv(btn);
            if (it.actived) {
                this.activedItem = btn;
            }
        }
    }
    autoMoreItems() {
        let width = $B.DomUtils.width(this.$body);
        let childs = this.$body.children;
        let start = 0;
        let overIdx = -1;
        for (let i = 0; i < childs.length; i++) {
            let child = childs[i];
            if ($B.DomUtils.hasClass(child, ".k_nav_h_item")) {
                start = start + $B.DomUtils.outerWidth(child);
                if (start > width) {
                    overIdx = i;
                    break;
                }
            }
        }
        this.overIdx = overIdx;
        if (overIdx > 0) {
            if (!this.$extMore) {
                this.$extMore = $B.DomUtils.createEl("<div class='k_nav_more_btn' style='display:none;position:absolute;right:-2px;bottom:-5px;'><i class='fa fa-menu'></i></div>");
                $B.DomUtils.append(this.$body, this.$extMore);
                $B.DomUtils.bind(this.$extMore, {
                    mouseenter: (e) => {
                        clearTimeout(this._hideSubMenusTimer);
                        clearTimeout(this.detachExtTimer);
                        var el = e.target;
                        var ofs = $B.DomUtils.offset(el);
                        if (!this.$extMenuContainer) {
                            let id = "k_h_nav_ext";
                            this.$extMenuContainer = $B.DomUtils.createEl("<div id='" + id + "' style='display:none;position: absolute;z-index:" + $B.config.maxZindex + "' class='k_context_menu_container k_box_shadow k_box_size'></div>");
                            $B.DomUtils.bind(this.$extMenuContainer, {
                                mouseenter: (e) => {
                                    clearTimeout(this._hideSubMenusTimer);
                                    clearTimeout(this.detachExtTimer);
                                },
                                mouseleave: (e) => {
                                    this.detachExtTimer = setTimeout((e) => {
                                        $B.DomUtils.detach(this.$extMenuContainer);
                                    }, 500);
                                }
                            });
                        } else {
                            $B.DomUtils.removeChilds(this.$extMenuContainer);
                        }
                        $B.DomUtils.css(this.$extMenuContainer, { top: "-1000px", "display": "block" });
                        let childs = this.opts.datas.slice(this.overIdx);
                        this._createSubBtns(childs, "k_nav_more_btn", this.$extMenuContainer, (id, childs) => {
                        });
                        if (this.$activeFirstMenu) {
                            setTimeout(() => {
                                let mid = $B.DomUtils.attribute(this.$activeFirstMenu, "id");
                                this._renderExtActUI(mid);
                            }, 10);
                        }
                        if (e.isTrigger) {
                            return false;
                        }
                        if (this.$extMenuContainer.parentNode) {
                            $B.DomUtils.detach(this.$extMenuContainer);
                        }
                        $B.DomUtils.append($B.getBody(), this.$extMenuContainer);
                        var info = $B.DomUtils.domInfo(this.$extMenuContainer);
                        ofs.left = ofs.left - info.width;
                        this.$extMenuContainer.style.display = "none;"
                        this.$extMenuContainer.style.top = ofs.top + 18 + "px";
                        this.$extMenuContainer.style.left = ofs.left + 14 + "px";
                        $B.slideDown(this.$extMenuContainer, 100);
                    },
                    mouseleave: (e) => {
                        if (this.$extMenuContainer) {
                            this.detachExtTimer = setTimeout((e) => {
                                $B.DomUtils.detach(this.$extMenuContainer);
                            }, 300);
                        }
                    }
                });
                $B.DomUtils.trigger(this.$extMore, "mouseenter");
            }
            this.$extMore.style.display = "block";
        } else if (this.$extMore) {
            this.$extMore.style.display = "none";
        }
    }
    hideAllSubMenus(notHideExt) {
        var childs = this.allSubMenus;
        for (let i = 0; i < childs.length; i++) {
            childs[i].style.display = "none";
        }
        if (!notHideExt && this.$extMenuContainer) {
            $B.DomUtils.detach(this.$extMenuContainer);
        }
    }
    _getOptById(id) {
        var opt = this._loopGetOptById(id, this.opts.datas);
        return opt;
    }
    _loopGetOptById(id, buttons) {
        var opt, childrens = [];
        for (let i = 0; i < buttons.length; i++) {
            if (Array.isArray(buttons[i])) {
                opt = this._loopGetOptById(id, buttons[i]);
                if (opt) {
                    break;
                }
            } else {
                if (buttons[i].id === id) {
                    opt = buttons[i];
                    break;
                }
                if (buttons[i].children) {
                    for (let j = 0; j < buttons[i].children.length; j++) {
                        childrens.push(buttons[i].children[j]);
                    }
                }
            }
        }
        if (!opt && childrens.length > 0) {
            opt = this._loopGetOptById(id, childrens);
        }
        return opt;
    }
    destroy(excuObjName) {
        if (this.$extMenuContainer) {
            $B.DomUtils.remove(this.$extMenuContainer);
        }
        $B.DomUtils.remove(this.allSubMenus);
        super.destroy(excuObjName);
    }
}
$B["Nav"] = Nav; 
 if(!window["$B"] ) {window["$B"] = $B;} 
  return window["$B"] ; 
});
