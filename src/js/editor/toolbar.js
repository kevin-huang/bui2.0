/***
 * 工具栏功能封装，当工具栏事件调用时候，优先执行工具栏自身注册的事件(Maker),
 * 当工具栏没有注册事件时候则调用富文本实例的处理函数
 * by kevin.huang
 * ***/
class KeditTools extends $B.BaseControl {
    constructor(editIns) {
        super();
        this.BUICTLS = [];
        this.$panels = [];
        this.cssMap = {};
        this.editIns = editIns;
        this.$wrap = $B.DomUtils.createEl("<div tabindex='0' style='z-index:1' class='k_box_size k_edit_tools_wrap'></div>");
        this.$tip = $B.DomUtils.createEl("<div style='width:auto;max-width:150px;position:absolute;display:none;' class='k_edit_tools_tip_wrapper' disabled='disabled'><div class='k_edit_tools_tip_caret'><i style='height:5px;' class='fa fa-up-dir'></i></div><div class='k_edit_tools_tip_text'></div></div>");
        this.regionMap = editCfg.regionMap ? editCfg.regionMap : {};
        this.createUi();
        $B.DomUtils.append(editIns.elObj, this.$wrap);
        this.bindEvents();
        $B.DomUtils.append(document.body, this.$tip);
        var _this = this;
        this.colorIns = editIns.getColorIns("toolbar",function(hex, opacity){
               if(this.tagName === "DIV"){
                    $B.DomUtils.css(this.lastChild, { "background-color": hex, opacity: opacity });
                }else{
                    $B.DomUtils.css(this, { "background-color": hex, opacity: opacity });
                }
                if(this.id === "_border_color"){ //段落边框颜色
                    _this.exeBorderCssFn();
                    return;
                }else if(this.id === "k_img_color"){//图片边框颜色
                    _this.exeImgBorderCssFn();
                    return;
                }            
                _this.editIns.execute(_this.editAPI, { color: hex, opacity: opacity }, this.lastChild);
        });
        setTimeout(() => {
            this.bindFocusEv(this.$wrap);
        }, 10);
    }
    /**
     * 回车键事件
     * **/
    onEditorMouseDown(){
        this.hidePanel();
    }
    /**
     * 绑定鼠标focus事件
     * **/
    bindFocusEv($el) {
        this.editIns.bindFocusEv($el);
    }
    /**
     * 绑定工具栏点击监听
     */
    bindEvents() {
        $B.DomUtils.click(this.$wrap, (e) => {
            this.editIns.callOffBrush();
            let el = e.target;
            let btn;
            while (el) {
                if ($B.DomUtils.hasClass(el, "k_edit_tools_wrap")) {
                    break;
                }
                if ($B.DomUtils.hasClass(el, "k_edit_tools_item")) {
                    btn = el;
                    break;
                }
                el = el.parentNode;
            }
            if (btn) {
                if(!$B.Dom.hasClass(btn,"k_edit_tools_disabled")){
                    this.callFn(btn);
                }                
            }
        });
    }
    /**
     * 调用点击处理函数，优先调用工具栏自身实现的处理函数，
     * 再次检查是否是一个Maker处理，否则调用编辑器的处理函数
     * **/
    callFn(btn) {
        let fn = $B.DomUtils.attribute(btn, "fn");
        let val = $B.DomUtils.attribute(btn, "val");
        this.editIns.hideAll();
        //优先执行工具栏自身的响应
        if (typeof this[fn] === "function") {
            this[fn](btn, val);
        } else {
            //需要工具栏自身提供响应的api
            let makeName = fn + "Maker";
            if (typeof this[makeName] === "function") {
                this.editAPI = fn;
                this[makeName](fn, btn);
            } else {
                this.editIns.execute(fn, val, btn);
            }
        }
    }
    pictureFnMaker(fn, btn){
        //先检测是否有可插入的位置
        let go = true;
        try{
            let r = this.editIns._getDomRange();
            go =  r.collapsed &&  r.startContainer.nodeType === 3 ;
        }catch(e){
            console.log(e);
            go = false;
        }
        if(!go){
            $B.alert(editCfg.label.availableInsert,1.6);
            return;
        }        
        let elkey = "$" + fn;
        let bodyKey = elkey + "_body";
        let $fb = this[bodyKey];
        let opts;
        if(!$fb){
            opts = {
                title: editCfg.label.insertImg,
                css:undefined,
                onCreated:($wap) => {
                    let $form = $B.DomUtils.createEl(editCfg.imageForm);
                    $fb = $form;
                    let css = "p{line-height:30px;padding-top:12px;} span.label{text-align:left;margin-left:10px;} p span{display:inline-block;}"
                            +".k_edit_img_pos{background:url("+editCfg.imgPos+") no-repeat;display:block;float:left;cursor:pointer;margin-left:12px;opacity:0.4;}"
                            +".pos_actived{opacity:1;}.k_edit_img_pos_right{height:17px;width:19px;background-position:0 0;margin-left:17px;}"
                            +".k_edit_img_pos_left{height:17px;width:19px;background-position:0 -17px;}.k_edit_img_pos_innser{height:17px;width:19px;background-position:0 -51px;}"
                            +".k_edit_img_pos_center{height:17px;width:19px;background-position:0 -34px;}#_submit_upload_{padding:3px 5px;}";

                    $B.createTextIfr($wap, $form, css, { height: '300px', width: '500px' });                    
                    this[bodyKey] = $form; 

                    let $top = $B.Dom.findbyId($fb,"_top_k_padding");
                    let $bottom = $B.Dom.findbyId($fb,"_bottom_k_padding"); 
                    let $left = $B.Dom.findbyId($fb,"_left_k_padding");
                    let $right = $B.Dom.findbyId($fb,"_right_k_padding");
                    let $width = $B.Dom.findbyId($fb,"_img_width");
                    let $height = $B.Dom.findbyId($fb,"_img_height");
                    let $href = $B.Dom.findbyId($fb,"_href_");
                    let $pos = $B.Dom.findbyId($fb,"_img_pos");

                    setTimeout(() => {
                        $B.DomUtils.attribute(this[elkey],{"hold":1});
                    }, 10);                  
                    let $upload = $B.Dom.findbyId($fb,"_file_upload_wrap");
                    let opt = {
                        timeout: 300, //超时时间 秒
                        onlyOne: true,
                        multiple: false, //是否可批量选择
                        immediate: false, //选择文件后是否立即自动上传，即不用用户点击提交按钮就上传
                        accept: '.jpg,.jpeg,.gif,.bmp,.png',   // 可上传的文件类型 .xml,.xls,.xlsx,.png                       
                        error: function (res,err) { //错误回调                           
                            $B.error(editCfg.label.uploadError,1.6);
                            console.log(err);
                        } 
                    };                   
                    let onScussFn = this.editIns.opts.imgUpload.success;
                    $B.extendObjectFn(opt, this.editIns.opts.imgUpload);
                  
                    opt.success = (mesage, data, res)=>{
                        if(typeof data === "string"){
                            this.returnImg = data;
                        }else{
                            this.returnImg = data.url;
                            if(data.width){
                                $width.value = data.width;
                            }
                            if(data.height){
                                $height.value = data.height;
                            }
                        } 
                        if(onScussFn){
                            onScussFn(mesage, data, res);
                        }                        
                    };
                   
                    let uploadIns = new $B.Upload($upload ,opt); 
                    this.BUICTLS.push(uploadIns);
                    this.uploadIns = uploadIns;
                    let $localBtn = $B.Dom.findbyId($fb,"_local_upload_");  
                    let $remoteBtn = $B.Dom.findbyId($fb,"_remote_upload_"); 
                    let localForm = $B.Dom.findbyId($fb,"_local_upload_form");  
                    let rmoteForm = $B.Dom.findbyId($fb,"_remote_upload_form");  
                    let isLocal = true;            
                    let swdithFN = (e)=>{
                        $width.value = "";
                        $height.value = "";
                        let el = e.target;
                        if(el.id === "_local_upload_"){
                            localForm.style.display = "";
                            rmoteForm.style.display = "none";
                            $localBtn.style.background = "#666666";
                            $localBtn.style.color = "#fff";
                            $remoteBtn.style.background = "#fff";
                            $remoteBtn.style.color = "#666666";
                            isLocal = true;
                        }else{
                            localForm.style.display = "none";
                            rmoteForm.style.display = "";
                            $localBtn.style.background = "#fff";
                            $localBtn.style.color = "#666666";
                            $remoteBtn.style.background = "#666666";
                            $remoteBtn.style.color = "#fff";
                            isLocal = false;
                        }
                    };
                    $B.Dom.click($localBtn,swdithFN);
                    $B.Dom.click($remoteBtn,swdithFN);                    

                    let $submit = $B.Dom.findbyId($fb,"_submit_upload_");
                    $B.Dom.click($submit,(e)=>{  
                        let params = {
                            top: $B.getInputVal($top,0),
                            bottom: $B.getInputVal($bottom,0),
                            right: $B.getInputVal($right,0),
                            left: $B.getInputVal($left,0),
                            width: $B.getInputVal($width,0),
                            height: $B.getInputVal($height,0),
                            pos:$pos.value
                        };
                        if(isLocal){
                            params["url"] = this.returnImg;
                        }else{
                            params["url"] = $B.getInputVal($href);
                        }                  
                        this.hidePanel();                       
                        this.editIns.execute(fn, params);
                    });

                    $B.Dom.click($B.Dom.findbyId($fb,"k_edit_img_layout"),(e)=>{
                        let el = e.target;                       
                        if(el.nodeName === "A"){                         
                            $B.Dom.addClass(el,"pos_actived");
                            let p = $B.Dom.attr(el,"v");
                            $pos.value = p;
                            $B.Dom.forSiblings(el,(node)=>{
                                if(node.nodeName === "A"){
                                    $B.Dom.removeClass(node,"pos_actived");
                                }
                            });                            
                        }
                    });
                },
                onPanalClick: (e) => {
                    // console.log(e);
                }
            };            
        }
        this.createCommPanel(btn,elkey,opts); 
        this.uploadIns.clear(); 
        this.returnImg = "";
        return $fb;  
    }
    editImgMaker(fn, btn){
        let elkey = "$" + fn;
        let bodyKey = elkey + "_body";
        let $fb = this[bodyKey];
        let opts;
        if(!$fb){
            opts = {
                title: editCfg.label.editImg,
                css:undefined,
                onCreated:($wap) => {
                    let $form = $B.DomUtils.createEl(editCfg.imgEditForm);
                    $fb = $form;
                    let css = "p{line-height:30px;padding-top:12px;} span.label{text-align:left;margin-left:10px;} p span{display:inline-block;}"                          
                             +"#_submit_upload_{padding:3px 5px;}";

                    $B.createTextIfr($wap, $form, css, { height: '120px', width: '450px' });                    
                    this[bodyKey] = $form; 

                    let $top = $B.Dom.findbyId($fb,"_top_k_padding");
                    let $bottom = $B.Dom.findbyId($fb,"_bottom_k_padding"); 
                    let $left = $B.Dom.findbyId($fb,"_left_k_padding");
                    let $right = $B.Dom.findbyId($fb,"_right_k_padding");
                    let $width = $B.Dom.findbyId($fb,"_img_width");
                    let $height = $B.Dom.findbyId($fb,"_img_height");

                    setTimeout(() => {
                        $B.DomUtils.attribute(this[elkey],{"hold":1});
                    }, 10);         

                    let $submit = $B.Dom.findbyId($fb,"_submit_upload_");
                    $B.Dom.click($submit,(e)=>{  
                        let params = {
                            top: $B.getInputVal($top,0),
                            bottom: $B.getInputVal($bottom,0),
                            right: $B.getInputVal($right,0),
                            left: $B.getInputVal($left,0),
                            width: $B.getInputVal($width,0),
                            height: $B.getInputVal($height,0)
                        };                                    
                        setTimeout(()=>{
                            this.hidePanel();
                        },10);                       
                        this.editIns.execute(fn, params);
                    });
                },
                onPanalClick: (e) => {
                    // console.log(e);
                }
            };            
        }
        this.createCommPanel(btn,elkey,opts);        
        return $fb;  
    }
    borderImgMaker(fn, btn){
        let elkey = "$" + fn;
        let bodyKey = elkey + "_body";
        let $fb = this[bodyKey];
        let opts;
        if(!$fb){
            opts = {
                title: editCfg.label.borderImg,
                css:undefined,
                onCreated:($wap) => {
                    let $form = $B.DomUtils.createEl(editCfg.borderForm);
                    $fb = $form;                                    
                    this[bodyKey] = $form; 
                    $B.Dom.append($wap,$form); 
                    setTimeout(() => {
                        $B.DomUtils.attribute(this[elkey],{"hold":1});
                    }, 10);
                    let $submit = $B.Dom.findbyId($fb,"_cls_btn");   
                    let $pos = $B.Dom.findbyId($fb,"k_border_pos");
                    let $tyle = $B.Dom.findbyId($fb,"k_border_style");
                    let $size = $B.Dom.findbyId($fb,"k_border_size");
                    let $color = $B.Dom.findbyId($fb,"k_img_color");
                    let exeCuteFN = ()=>{
                        let prs = {
                            "border-top":"none",
                            "border-right":"none",
                            "border-bottom":"none",
                            "border-left":"none"
                        };
                        let bgColor = $B.DomUtils.css($color, "background-color");
                        let bstyle = $tyle.value;
                        let bsize = $size.value;
                        let childs = $pos.children;
                        for(let i =0 ;i < childs.length ;i++){
                            if($B.Dom.hasClass(childs[i],"activted")){
                                let key = "border-"+$B.Dom.attr(childs[i],"v");
                                prs[key] = bsize + " "+bstyle + " "+bgColor;
                            }
                        }
                        this.editIns.execute("imgBorderFn", prs);
                    };
                    this.exeImgBorderCssFn = exeCuteFN;
                    $B.Dom.click($submit,(e)=>{           
                        setTimeout(()=>{
                            this.hidePanel();
                        },10);   
                    });
                    $B.Dom.click($pos,(E)=>{
                        let el = E.target;                        
                        if($B.Dom.hasClass(el,"_border_pos")){
                            if($B.Dom.hasClass(el,"activted")){
                                el.style.backgroundColor = "#FFF";
                                $B.Dom.removeClass(el,"activted");
                            }else{
                                el.style.backgroundColor = "#93F5FA";
                                $B.Dom.addClass(el,"activted");
                            }  
                            exeCuteFN();                          
                        }                        
                    });                    
                    let onChangeFN = (e)=>{
                        exeCuteFN(); 
                    };
                    $B.Dom.change($tyle,onChangeFN);
                    $B.Dom.change($size,onChangeFN);                    
                    $B.Dom.click($color,(e)=>{
                        let btn = e.target;
                        if(this.colorIns.isShow() && btn === this.colorIns.target){
                            this.colorIns.hide();
                            return false;
                        }
                        let bgColor = $B.DomUtils.css(btn, "background-color");
                        let opacity = $B.DomUtils.css(btn, "opacity");
                        this.colorIns._callFnKey = "toolbar";
                        this.colorIns.setValue(bgColor, opacity);
                        this.colorIns.show(btn);  
                        setTimeout(()=>{
                            this.colorIns.target = btn;
                        },300);  
                        return false;
                    });
                },
                onPanalClick: (e) => {
                    // console.log(e);
                }
            };            
        }
        this.createCommPanel(btn,elkey,opts);  
        if(this.editIns.curImgTag){
            let borders =  UTILS.getBorder(this.editIns.curImgTag);            
            //联动当前img边框样式
            let $pos = $B.Dom.findbyId($fb,"k_border_pos");           
            let childs = $pos.children;
            let size = "1px",style = "solid",color = "#717171";
            let isDef = true;
            for(let i =0 ;i < childs.length ;i++){
                let k = "border-"+$B.Dom.attr(childs[i],"v");
                if(borders[k]){
                    $B.Dom.addClass(childs[i],"activted");
                    childs[i].style.backgroundColor = "#93F5FA";
                    if(isDef){
                       isDef = false;
                       let obj = UTILS.border2obj(borders[k],true);
                       size = obj.size;
                       style = obj.style;
                       color = obj.color;
                    }
                }else{
                    $B.Dom.removeClass(childs[i],"activted");
                    childs[i].style.backgroundColor = "#fff";
                }
            }
            let $tyle = $B.Dom.findbyId($fb,"k_border_style");
            let $size = $B.Dom.findbyId($fb,"k_border_size");
            let $color = $B.Dom.findbyId($fb,"k_img_color"); 
            $B.setSelectValue($size,size);
            $B.setSelectValue($tyle,style);
            $color.style.backgroundColor = color;
        }     
        return $fb; 
    }
    linkFnMaker(fn, btn){       
        let elkey = "$" + fn;
        let bodyKey = elkey + "_body";
        let $fb = this[bodyKey];
        let opts;
        if(!$fb){
            opts = {
                title: editCfg.label.insertLink,
                css:undefined,
                onCreated:($wap) => {
                    let $form = $B.DomUtils.createEl(editCfg.linkForm);
                    $fb = $form;
                    $B.createTextIfr($wap, $form, "p{line-height:30px;padding-top:12px;} span.label{text-align:left;margin-left:10px;} p span{display:inline-block;}", { height: '160px', width: '350px' });                    
                    this[bodyKey] = $form; 
                    let $btn = $B.Dom.findbyId($form,"okbtn");
                    $B.Dom.click($btn,(e)=>{
                        let $target = $B.Dom.findbyId($form,"_target_");
                        let $addr = $B.Dom.findbyId($form,"_addr_url");
                        let $name =  $B.Dom.findbyId($form,"_link_name");
                        let isBlank = $B.Dom.attr($target,"checked") === true;
                        let addr = $B.trimFn( $addr.value);
                        let name = $B.trimFn($name.value);                    
                        this.hidePanel();
                        if(addr !== "" && name !== ""){
                            let params = {name:name,addr:addr,isBlank:isBlank};                           
                            this.editIns.linkFn(params);
                        }
                    });
                },
                onPanalClick: (e) => {
                    // console.log(e);
                }
            };            
        }
        this.createCommPanel(btn,elkey,opts);  
        this.editIns.onOpenLinkPanel($fb);
        return $fb;     
    } 
    uploadFnMaker(fn, btn){
                //先检测是否有可插入的位置
                let go = true;
                try{
                    let r = this.editIns._getDomRange();
                    go =  r.collapsed &&  r.startContainer.nodeType === 3 ;
                }catch(e){
                    console.log(e);
                    go = false;
                }
                if(!go){
                    $B.alert(editCfg.label.availableInsert,1.6);
                    return;
                }        
                let elkey = "$" + fn;
                let bodyKey = elkey + "_body";
                let $fb = this[bodyKey];
                let opts;
                if(!$fb){
                    opts = {
                        title: editCfg.label.insertFile,
                        css:undefined,
                        onCreated:($wap) => {
                            let $form = $B.DomUtils.createEl(editCfg.fileForm);
                            $fb = $form;
                            let css = "p{line-height:30px;padding-top:12px;} span.label{text-align:left;margin-left:10px;} p span{display:inline-block;}"
                                    +".k_edit_img_pos{background:url("+editCfg.imgPos+") no-repeat;display:block;float:left;cursor:pointer;margin-left:12px;opacity:0.4;}"
                                    +".pos_actived{opacity:1;}.k_edit_img_pos_right{height:17px;width:19px;background-position:0 0;margin-left:17px;}"
                                    +".k_edit_img_pos_left{height:17px;width:19px;background-position:0 -17px;}.k_edit_img_pos_innser{height:17px;width:19px;background-position:0 -51px;}"
                                    +".k_edit_img_pos_center{height:17px;width:19px;background-position:0 -34px;}#_submit_upload_{padding:3px 5px;}";
        
                            $B.createTextIfr($wap, $form, css, { height: '160px', width: '500px' });                    
                            this[bodyKey] = $form; 
        
                            let $url = $B.Dom.findbyId($fb,"_addr_url");                         
                            let $name = $B.Dom.findbyId($fb,"file_name");                          
        
                            setTimeout(() => {
                                $B.DomUtils.attribute(this[elkey],{"hold":1});
                            }, 10);                  
                            let $upload = $B.Dom.findbyId($fb,"_file_upload_wrap");
                            let opt = {
                                timeout: 300, //超时时间 秒
                                onlyOne: true,
                                multiple: false, //是否可批量选择
                                immediate: false, //选择文件后是否立即自动上传，即不用用户点击提交按钮就上传
                                error: function (res,err) { //错误回调                           
                                    $B.error(editCfg.label.uploadError,1.6);
                                    console.log(err);
                                } 
                            };                   
                            let onScussFn = this.editIns.opts.fileUpload.success;
                            $B.extendObjectFn(opt, this.editIns.opts.fileUpload);
                          
                            opt.success = (mesage, data, res)=>{
                                let fileName;
                                if(typeof data === "string"){
                                    this.returnFile = data;
                                }else{
                                    this.returnFile = data.url;
                                    if(data.labelName){
                                        fileName = data.labelName;
                                    }
                                }
                                if(!fileName){
                                    let tmps = this.returnFile.split("/");
                                    fileName = tmps[tmps.length - 1];
                                }     
                                $name.value = fileName;                           
                                if(onScussFn){
                                    onScussFn(mesage, data, res);
                                }                        
                            };
                           
                            let uploadIns = new $B.Upload($upload ,opt); 
                            this.BUICTLS.push(uploadIns);
                            this.uploadFileIns = uploadIns;
                            let $localBtn = $B.Dom.findbyId($fb,"_local_upload_");  
                            let $remoteBtn = $B.Dom.findbyId($fb,"_remote_upload_"); 
                            let localForm = $B.Dom.findbyId($fb,"_local_upload_form");  
                            let rmoteForm = $B.Dom.findbyId($fb,"_remote_upload_form");  
                            let isLocal = true;            
                            let swdithFN = (e)=>{
                                let el = e.target;
                                if(el.id === "_local_upload_"){
                                    localForm.style.display = "";
                                    rmoteForm.style.display = "none";
                                    $localBtn.style.background = "#666666";
                                    $localBtn.style.color = "#fff";
                                    $remoteBtn.style.background = "#fff";
                                    $remoteBtn.style.color = "#666666";
                                    isLocal = true;
                                }else{
                                    localForm.style.display = "none";
                                    rmoteForm.style.display = "";
                                    $localBtn.style.background = "#fff";
                                    $localBtn.style.color = "#666666";
                                    $remoteBtn.style.background = "#666666";
                                    $remoteBtn.style.color = "#fff";
                                    isLocal = false;
                                }
                            };
                            $B.Dom.click($localBtn,swdithFN);
                            $B.Dom.click($remoteBtn,swdithFN);                    
        
                            let $submit = $B.Dom.findbyId($fb,"_submit_upload_");
                            $B.Dom.click($submit,(e)=>{  
                                let params = {
                                    name : $B.getInputVal($name),                               
                                };
                                if(isLocal){
                                    params["url"] = this.returnFile;
                                }else{
                                    params["url"] = $B.getInputVal($url);
                                }                  
                                this.hidePanel();                       
                                this.editIns.execute(fn, params);
                            });                            
                        },
                        onPanalClick: (e) => {
                            // console.log(e);
                        }
                    };            
                }
                this.createCommPanel(btn,elkey,opts); 
                this.uploadFileIns.clear(); 
                this.returnFile = "";
                return $fb;  
    }    
    emotionFnMaker(fn, btn){

    }
    insertIconFnMaker(fn, btn){

    }
    /**
     * 插入浮动文本窗口创建
     * **/
    insertFloatFnMaker(fn, btn) {
    }
    /**
     * 插入表格窗口窗口
     * **/
    insertTableFnMaker(fn, btn) {
        let elkey = "$" + fn;
        let bodyKey = elkey + "_body";
        let $fb = this[bodyKey];
        let opts;
        if(!$fb){
            opts = {
                title: editCfg.label.insertTable,
                css:undefined,
                onCreated:($wap) => {
                    let $form = $B.DomUtils.createEl("<table style='width:166px;' class='k_edit_table_set'></table>");
                    let tdArray = [];
                    for (let i = 0, len = 15; i < len; ++i) {
                        var $tr = $B.DomUtils.createEl("<tr row='" + i + "'></tr>");
                        for (var j = 0, jlen = 15; j < jlen; ++j) {                            
                            let $td = $B.DomUtils.append($tr,"<td col='" + j + "'></td>");
                            tdArray.push($td);
                        }
                        $B.DomUtils.append($form,$tr);
                    }
                    $B.DomUtils.append($wap,$form);
                    let tip = editCfg.label.insertTableTip.replace("x","0").replace("y","0");
                    let $tip = $B.DomUtils.createEl("<p style='text-align:left'>"+tip+"</p>");
                    $B.DomUtils.append($wap,$tip);
                    this[bodyKey] = $form;
                    $B.DomUtils.mousemove($form,(e)=>{
                        $B.DomUtils.removeClass(tdArray,"selectd");
                        let td = e.target;
                        $B.DomUtils.addClass(td,"selectd");
                        let col = parseInt( $B.DomUtils.attribute(td,"col"));                       
                        if(col !== NaN){
                            let row = parseInt( $B.DomUtils.attribute(td.parentNode,"row"));
                            let preEl = td.previousSibling;
                            while(preEl){
                                $B.DomUtils.addClass(preEl,"selectd");
                                preEl = preEl.previousSibling;
                            }
                            let tr = td.parentNode.previousSibling;
                            while(tr){
                                let childs = tr.children;
                                for(let i =0 ; i < childs.length ;i++){
                                    $B.DomUtils.addClass(childs[i],"selectd");
                                    if(i === col){
                                        break;
                                    }
                                }
                                tr = tr.previousSibling;
                            }
                            tip = editCfg.label.insertTableTip.replace("x",row+1).replace("y",col+1);
                            $tip.innerText = tip;
                        }
                    });
                    $B.DomUtils.click($form,(e)=>{
                        //console.log("exe insert table......");
                        $B.DomUtils.removeClass(tdArray,"selectd");
                        var td = e.target;
                        let col = parseInt( $B.DomUtils.attribute(td,"col")) + 1;
                        let row = parseInt( $B.DomUtils.attribute(td.parentNode,"row")) + 1;
                        this.hidePanel();
                        this.editIns.execute("insertTableFn", {row:row,col:col});          
                    });
                },
                onPanalClick: (e) => {
                    // console.log(e);
                }
            };            
        }
        this.createCommPanel(btn,elkey,opts);
        let trs = this[bodyKey].children;
        for(let i = 0 ; i < trs.length ;i++){
            let tds = Array.from(trs[i].children);
            $B.DomUtils.removeClass(tds,"selectd");
        }
    }
    /**
     * 段落Panel窗口创建
     * **/
    paragraphFnMaker(fn, btn) {     
        //console.log(fn,btn);    
        let elkey = "$" + fn;
        let bodyKey = elkey + "_body";
        let $fb = this[bodyKey];
        let opts;
        var _this = this;
        if(!$fb){
            opts = {
                title: editCfg.label.paragraph,
                css:undefined,
                onCreated:($wap) => {
                    let $form = $B.DomUtils.createEl(editCfg.paragraphForm);
                    $B.createTextIfr($wap, $form, ".borderAct{background:#05F5FF !important;}", { height: '240px', width: '450px' });
                    this[bodyKey] = $form;  
                    setTimeout(()=>{
                        $B.DomUtils.attribute(this[elkey],{"hold":1});
                    },1);                      
                    let inuptFn = function(e){
                        let elid = e.target.id;
                        let val = e.target.value;
                        if(!/^\d+$/.test(val)){
                            return;
                        }                       
                        let css;
                        if(elid === "_indent_"){
                            _this.editIns.execute("pindentFn", {indent: parseInt(val) });                         
                        }else if(elid === "txt_line_h"){
                            _this.editIns.execute("plineHeight", {"line-height": val });
                        }else if(elid === "_pdding-top_"){
                            css = {"padding-top":val+"px"};
                        }else if(elid === "_pdding-bottom_"){
                            css = {"padding-bottom":val+"px"};
                        }else if(elid === "_padding-left_"){
                            css = {"padding-left":val+"px"};
                        }else if(elid === "_padding-right_"){
                            css = {"padding-right":val+"px"};
                        }
                        if(css){
                            _this.editIns.execute("ppadding", css);
                        }
                    };
                    let $borderStyle,$borderSize,$borderColor,$borderPos;

                    let  exeBorderCssFn = function(){                        
                        let params = {};
                        let el = $borderPos.firstChild;
                        let bstyle = $borderStyle.value;
                        let bsize = $borderSize.value;
                        let bcolor = $B.DomUtils.css($borderColor,"background-color");
                        let v =  bsize+" "+ bstyle+" " + bcolor;
                        if($B.DomUtils.hasClass(el,"borderAct")){
                            params["border-left"] = v;
                        }
                        el = el.nextSibling;
                        if($B.DomUtils.hasClass(el,"borderAct")){
                            params["border-top"] = v;
                        }
                        el = el.nextSibling;
                        if($B.DomUtils.hasClass(el,"borderAct")){
                            params["border-right"] = v;
                        }
                        el = el.nextSibling;
                        if($B.DomUtils.hasClass(el,"borderAct")){
                            params["border-bottom"] = v;
                        }
                        _this.editIns.execute("pborderFn", params);
                    };
                    _this.exeBorderCssFn = exeBorderCssFn;//拾色器触发
                    let $txtLineH = $B.DomUtils.findbyId($form,"#txt_line_h");
                    let selectedFn =function(e){
                        let elid = e.target.id;                       
                        if(elid === "_line-height_"){
                            if(e.target.value !== ""){
                                let val = parseFloat( e.target.value);
                                _this.editIns.execute("plineHeight", {"lineR": val },$txtLineH);
                            }
                        }else{//边框修饰
                            exeBorderCssFn();
                        }
                    };
                    let $indent = $B.DomUtils.findbyId($form,"#_indent_");  
                    $B.DomUtils.input($indent,inuptFn);
                   
                    $borderStyle = $B.DomUtils.findbyId($form,"#_border_style");
                    $B.DomUtils.change($borderStyle,selectedFn);
                    $borderSize = $B.DomUtils.findbyId($form,"#_border_size");
                    $B.DomUtils.change($borderSize,selectedFn);
                   
                    let $paddingTop = $B.DomUtils.findbyId($form,"#_pdding-top_"); 
                    $B.DomUtils.input($paddingTop,inuptFn);
                    let $paddingBottom = $B.DomUtils.findbyId($form,"#_pdding-bottom_");
                    $B.DomUtils.input($paddingBottom,inuptFn);
                    let $paddingLeft = $B.DomUtils.findbyId($form,"#_padding-left_");
                    $B.DomUtils.input($paddingLeft,inuptFn);
                    let $paddingRight = $B.DomUtils.findbyId($form,"#_padding-right_");
                    $B.DomUtils.input($paddingRight,inuptFn);
                    let $lineHOpt = $B.DomUtils.findbyId($form,"#_line-height_"); 
                    $B.DomUtils.change($lineHOpt,selectedFn);
                  
                    $B.DomUtils.input($txtLineH,inuptFn);

                    let colorFn = function(e){
                        let el = e.target;
                        while(el){
                            if(el.tagName === "SPAN"){
                                break;
                            }
                            el = el.parentNode;
                        }
                        let id = el.id;
                        let top = 35 , left =0 ;                       
                        if(id === "_border_color"){//边框颜色
                            //边框颜色需要组合边框其他参数
                            //this.editAPI = "pcolorFn";
                        }else if(id === "_font_color"){
                            _this.editAPI = "pcolorFn";
                        }else if(id === "_bg_color"){
                            _this.editAPI = "pbackground";
                        }        
                        let $grp = _this["$paragraphFn"];                
                        let ofs = $B.DomUtils.offset($grp);
                        ofs.top = ofs.top + top;
                        ofs.left = ofs.left + left;                      
                        _this.showColorPicker(el,ofs);
                        return false;
                    };

                    $borderColor = $B.DomUtils.findbyId($form,"#_border_color");
                    let $fontcolor =  $B.DomUtils.findbyId($form,"#_font_color");
                    let $bgColor = $B.DomUtils.findbyId($form,"#_bg_color");
                    $B.DomUtils.click($borderColor,colorFn);
                    $B.DomUtils.click($fontcolor,colorFn);
                    $B.DomUtils.click($bgColor,colorFn);

                    $borderPos = $B.DomUtils.findbyId($form,"#_border_pos");
                    $B.DomUtils.click( $borderPos ,(e)=>{     
                        if(e.target.id !== "_border_pos"){
                            if($B.DomUtils.hasClass(e.target,"borderAct")){
                                $B.DomUtils.removeClass(e.target,"borderAct");
                            }else{
                                $B.DomUtils.addClass(e.target,"borderAct");
                            }
                            exeBorderCssFn();
                        } 
                    });
                },
                onPanalClick: (e) => {
                   // console.log(e);
                }
            };            
        }
        let $panel = this.createCommPanel(btn,elkey,opts); 
        $panel.id = "paragraph_panel"; 
        $fb = this[bodyKey];
        let cssMap = this.cssMap;
        if (cssMap["letter-spacing"]) { 
          this.rectiveGraphUI($fb,cssMap);
        }       
        if(btn && btn.openFlag){
            if(btn.openFlag === "table"){
                this.editIns.tableIns.activedLinePELS();//高亮当前选择的段落
            }
        }else{
            this.editIns.activedLinePELS();//高亮当前选择的段落
        }    
        return $fb;    
    }
    rectiveGraphUI($fb,cssMap){
        let $borderPos = $B.DomUtils.findbyId($fb,"#_border_pos");
        let $borderStyle = $B.DomUtils.findbyId($fb,"#_border_style");
        let $borderSize = $B.DomUtils.findbyId($fb,"#_border_size");
        let $borderColor = $B.DomUtils.findbyId($fb,"#_border_color");  
        let $bleft =  $borderPos.firstChild;
        let bmap = {
            size:undefined,
            style:undefined,
            color:undefined
        };
        if(cssMap["border-left"]){
           $B.DomUtils.addClass($bleft,"borderAct");
           let obj = UTILS.border2obj(cssMap["border-left"]); 
           UTILS.setBorderMap(bmap, obj );             
        }else{
           $B.DomUtils.removeClass($bleft,"borderAct");
        }
        if(cssMap["border-top"]){
            $B.DomUtils.addClass($bleft.nextSibling,"borderAct");
            let obj = UTILS.border2obj(cssMap["border-left"]);
            UTILS.setBorderMap(bmap, obj );
        }else{
            $B.DomUtils.removeClass($bleft.nextSibling,"borderAct");
        }
        if(cssMap["border-right"]){
            $B.DomUtils.addClass($bleft.nextSibling.nextSibling,"borderAct");
            let obj = UTILS.border2obj(cssMap["border-right"]);
            UTILS.setBorderMap(bmap, obj );
        }else{
            $B.DomUtils.removeClass($bleft.nextSibling.nextSibling,"borderAct");
        }
        if(cssMap["border-bottom"]){
            $B.DomUtils.addClass($bleft.nextSibling.nextSibling.nextSibling,"borderAct");
            let obj = UTILS.border2obj(cssMap["border-bottom"]);
            UTILS.setBorderMap(bmap, obj );
        }else{
            $B.DomUtils.removeClass($bleft.nextSibling.nextSibling.nextSibling,"borderAct");
        }
        if(bmap.size){
            bmap.size = bmap.size+"px";
            let childs = $borderSize.children;
            for(let i = 0 ;i < childs.length ;i++){
                if(childs[i].value === bmap.size){
                    $B.DomUtils.attribute(childs[i],{"selected":true});
                }else{
                    $B.DomUtils.attribute(childs[i],{"selected":false});
                }                    
            }
        }          
        if(bmap.style){
            let childs = $borderStyle.children;
            for(let i = 0 ;i < childs.length ;i++){
                if(childs[i].value === bmap.style){
                    $B.DomUtils.attribute(childs[i],{"selected":true});
                }else{
                    $B.DomUtils.attribute(childs[i],{"selected":false});
                }                    
            }
        }
        if(bmap.color){
            $borderColor.style.backgroundColor = bmap.color;
        }
        //段落背景色
        let $bgcolor = $B.DomUtils.findbyId($fb,"#_bg_color");          
        if(cssMap.pbackgorund){
            $bgcolor.style.backgroundColor = cssMap.pbackgorund;
        }
        //字体颜色
        let $fontcolor =  $B.DomUtils.findbyId($fb,"#_font_color");   
        $fontcolor.style.backgroundColor = cssMap.color;

        //行高，边距，缩进
        let $indent = $B.DomUtils.findbyId($fb,"#_indent_");              
        let $paddingTop = $B.DomUtils.findbyId($fb,"#_pdding-top_"); 
        let $paddingBottom = $B.DomUtils.findbyId($fb,"#_pdding-bottom_");
        let $paddingLeft = $B.DomUtils.findbyId($fb,"#_padding-left_");
        let $paddingRight = $B.DomUtils.findbyId($fb,"#_padding-right_");
        let $lineHOpt = $B.DomUtils.findbyId($fb,"#_line-height_"); 
        let $txtLineH = $B.DomUtils.findbyId($fb,"#txt_line_h");
       
        $indent.value = cssMap.indent;
        $paddingTop.value = cssMap["padding-top"];
        $paddingBottom.value = cssMap["padding-bottom"];
        $paddingLeft.value = cssMap["padding-left"];
        $paddingRight.value = cssMap["padding-right"];
        $txtLineH.value = cssMap["line-height"];
        let childs = $lineHOpt.children;
        let r = cssMap["lhrate"] + "";
        let emptyOpt,hasFiled;
        for(let i = 0 ;i < childs.length ;i++){
            if(childs[i].value === r){
                $B.DomUtils.attribute(childs[i],{"selected":true});
                hasFiled = true;
            }else{
                $B.DomUtils.attribute(childs[i],{"selected":false});
            }
            if(childs[i].value === ""){
                emptyOpt = childs[i];
            }
        }
        if(!hasFiled){
            $B.DomUtils.attribute(emptyOpt,{"selected":true});
        } 
    }
    /**
     * 文字间距窗口创建
     * **/
    fontSpaceFnMaker(fn, btn) {
        let elkey = "$" + fn;
        let bodyKey = elkey + "_body";
        let $fb = this[bodyKey];
        let opts;
        if(!$fb){
            opts = {
                title: editCfg.label.wordspace,
                css:undefined,
                onCreated:($wap) => {
                    let $form = $B.DomUtils.createEl(editCfg.fontSpaceForm);
                    $B.createTextIfr($wap, $form, "p{line-height:30px;padding-top:12px;} span.label{text-align:left;margin-left:10px;} p span{display:inline-block;}", { height: '115px', width: '170px' });
                    let clickTags = $B.DomUtils.findByClass($form, "ev");
                    $B.DomUtils.click(clickTags, (e) => {
                        let el = e.target;
                        if (el.tagName === "svg") {
                            el = el.parentNode;
                        }
                        let $p =  el.parentNode;
                        let $forP = $B.DomUtils.findbyId($p.previousSibling,"for_p");
                        let $in = $p.nextSibling.firstChild.nextSibling.firstChild;
                        if(/^\d+$/.test($in.value)){
                            let v = parseInt($in.value);
                            if ($B.DomUtils.hasClass(el, "reduce")) {
                                v--;
                            } else if ($B.DomUtils.hasClass(el, "addplus")) {
                                v++;
                            } else {
                                v = 0;
                            }
                            $in.value = v;
                            let isforp =  $forP.checked ? true : false;
                            this.editIns.execute(fn,{"letter-spacing":v+"px","forp":isforp});
                        }
                    });
                    let btn = $B.DomUtils.findbyId($form, "surebtn");
                    $B.DomUtils.click(btn, (e) => {
                        let $forP = $B.DomUtils.findbyId(e.target.parentNode.parentNode.previousSibling.previousSibling,"for_p");
                        let isforp =  $forP.checked ? true : false;
                        let v = e.target.parentNode.previousSibling.firstChild.value;
                        if(/^\d+$/.test(v)){
                            this.editIns.execute(fn,{"letter-spacing":v+"px","forp":isforp});
                        }                
                    });
                    this[bodyKey] = $form;
                },
                onPanalClick: (e) => {
                    // console.log(e);
                }
            };            
        }
        this.createCommPanel(btn,elkey,opts);
        $fb = this[bodyKey];
        if (this.cssMap["letter-spacing"]) {                      
            let $input = $B.DomUtils.findbyId($fb, "txtinput");
            let $pradio = $B.DomUtils.findbyId($fb,"for_p");
            let $sradio = $B.DomUtils.findbyId($fb,"for_s");
            $input.value = this.cssMap["letter-spacing"];
            if(this.editIns.hasRegion()){
                $B.DomUtils.attribute($pradio,{checked:false});
                $B.DomUtils.attribute($sradio,{checked:true});
            }else{
                $B.DomUtils.attribute($pradio,{checked:true});
                $B.DomUtils.attribute($sradio,{checked:false});
            }
        }
    }
    /**
     * 背景色窗口窗口
     * **/
    backgroundColorFnMaker(fn, btn) {
        this.showColorPicker(btn.lastChild);  
    }
    /**
     * 字体颜色窗口创建
     * **/
    fontColorFnMaker(fn, btn) {
        this.showColorPicker(btn.lastChild); 
    }
    /**
     * 显示拾色器
     * **/
    showColorPicker(btn,ofs){
        if(this.colorIns.isShow() && btn === this.colorIns.target){
            this.colorIns.hide();
            return ;
        }
        var bgColor = $B.DomUtils.css(btn, "background-color");
        var opacity = $B.DomUtils.css(btn, "opacity");
        this.colorIns._callFnKey = "toolbar";
        this.colorIns.setValue(bgColor, opacity);
        this.colorIns.show(btn,ofs);  
        setTimeout(()=>{
            this.colorIns.target = btn;
        },300);            
    }
    /**
     * 拾色器按钮ui渲染
     * **/
    _appendColorUI(btn, fn) {
        let $color = $B.DomUtils.createEl('<span class="k_box_size" style="border:1px solid #A0A0A0; display:inline-block;height: 5px;width: 100%;position: absolute;background: red;bottom: -3px;left: 0;"></span>');
        $B.DomUtils.append(btn, $color);
        if (fn === "fontColorFn") {
            $color.style.backgroundColor = "#000000";
        } else if (fn === "backgroundColorFn") {
            $color.style.backgroundColor = "#ffffff";
        }
        return $color;
    }
    /**
     * 获取工具栏高度
     * **/
    getHeight() {
        var h = $B.DomUtils.outerHeight(this.$wrap);
        return h;
    }
    /**
     * 当面板被隐藏后的回调
     * **/
    onPanelClosed(el){     
        if(el.id === "paragraph_panel"){
            this.editIns.unactivedLinePELS();
            this.editIns.tableIns.unactivedLinePELS();
        }
    }
    /** 
     * 绑定面板事件，点击，鼠标移出监听
     * 当传值clickFn时候注册点击监听
     * **/
    bindPanelEvents(el, clickFn) {
        if (!this.dlListEvents) {
            this.dlListEvents = {
                mouseleave: (e) => {
                    if($B.DomUtils.attribute(e.target,"hold")){
                        return;
                    }                   
                    this.hidePanel();
                }
            };
            if (clickFn) {
                this.dlListEvents["click"] = clickFn;
            }
        }
        $B.DomUtils.bind(el, this.dlListEvents);
    }
    /** 
     * 获取面板元素对象
     * **/
    getPanelEl() {
        return UTILS.getPanelEl();
    }
    /**
     * 创建工具栏UI
     * **/
    createUi() {
        let events = {
            mouseenter: (e) => {
                let title = $B.DomUtils.attribute(e.target, "_title");
                let ofs = $B.DomUtils.offset(e.target);
                let h = $B.DomUtils.outerHeight(e.target);
                ofs.top = ofs.top + h;
                this.$tip.lastChild.innerText = title;
                this.$tip.style.top = ofs.top + "px";
                this.$tip.style.left = ofs.left + "px";
                this.$tip.style.display = "";
                this.hidePanel();
            },
            mouseleave: (e) => {
                this.$tip.style.display = "none";
            }
        };
        /**单击列表事件**/
        let clickFN = (e) => {
            this.editIns.callOffBrush();
            let el = e.target;
            while (el) {
                if ($B.DomUtils.hasClass(el, "k_edit_tools_item")) {
                    break;
                }
                el = el.parentNode;
            }
            let _key = $B.DomUtils.attribute(el, "fn");
            let elkey = "$" + _key;
            if (!this[elkey]) {
                this[elkey] = this.getPanelEl();
                let $wap = this[elkey].lastChild;
                let arr = e.opts;
                let fus = $B.Dom.attr(el,"focus");
                if(fus){
                    fus = 'focus="1"';
                }
                for (let i = 0; i < arr.length; i++) {
                    let lbl = arr[i].label;
                    if(arr[i].value.indexOf("fa-") === 0){
                        lbl = "<i class='fa "+arr[i].value+"' style='padding-right:4px;'></i>"+lbl;
                    }
                    let $it = $B.DomUtils.append($wap, '<div '+fus+' fn="' + _key + '" val="' + arr[i].value + '" tabindex="0" style="" class="k_edit_tools_drop_item item" _focus="true">' + lbl + '</div>');
                    let css = {};
                    if (_key === "fontFamilyFn") {
                        css["font-family"] = arr[i].value;
                    } else if (_key === "fontSizeFn") {
                        css["font-size"] = arr[i].value;
                    } else if (_key === "fontTitleFn") {
                        css["font-size"] = arr[i].value;
                    }
                    $B.DomUtils.css($it, css);
                }
                $B.DomUtils.append(document.body, this[elkey]);
                this.$panels.push(this[elkey]);
                this.bindPanelEvents(this[elkey], (e) => {
                    let node = e.target;
                    let tg;
                    while (node) {
                        if ($B.DomUtils.hasClass(node, "k_edit_tools_drop_item_wrap")) {
                            break;
                        }
                        if ($B.DomUtils.hasClass(node, "k_edit_tools_drop_item")) {
                            tg = node;
                            break;
                        }
                        node = node.parentNode;
                    }
                    if (tg) {
                        let val = tg.innerText;
                        let fn = $B.Dom.attr(tg,"fn");
                        if(fn !== "orderListFn" && fn !== "unorderListFn"){
                            this.$curListFireBtn.firstChild.innerText = val;
                        }
                        this.callFn(tg);
                    }
                });
                this.bindFocusEv(this[elkey]);
            }
            if (!e._init) {
                this.showPanel(elkey, el);
            }
            return false;
        };
        var tools = editCfg.tools;        
        this.btnElMap = {};
        for (let i = 0; i < tools.length; i++) {
            let it = tools[i];
            if (it.region) {
                this.regionMap[it.fn] = true;
            }
            let btn = $B.DomUtils.createEl("<div fn='" + it.fn + "' unbindcolorev='1' style='padding-bottom:2px;cursor:pointer;position:relative;' tabindex='0' _title='" + it.label + "' class='k_box_size k_edit_tools_item'><i class='fa " + it.icon + " _e_btn_'></i></div>");
            if(it.fn === "undoFn" || it.fn === "redoFn"){
                $B.Dom.addClass(btn,"k_edit_tools_disabled");
            }
            this.btnElMap[it.fn] = btn;
            $B.DomUtils.append(this.$wrap, btn);
            $B.DomUtils.bind(btn, events);
            if(it.focus){
                $B.Dom.attr(btn,{"focus":1});
            }
            if (it.opts) { //存在下拉列表
                //console.log("下拉列表");
                if(it.fn !== "orderListFn" && it.fn !== "unorderListFn"){
                    $B.DomUtils.prepend(btn, "<span label='" + it.label + "'>" + it.label + "</span>");
                }
                $B.DomUtils.click(btn, clickFN);
                clickFN({ target: btn, _init: true, opts: it.opts });
            } else if (it.fn.indexOf("ColorFn") > 0) {
                this._appendColorUI(btn, it.fn);
            }
        }
    }
    /**
     * 显示panel
     * elKey: key
     * el: 可以是触发显示的元素，可以是位置对象
     * **/
    showPanel(elkey, el) {
        let ofs;
        if($B.DomUtils.isElement(el)){
            ofs = $B.DomUtils.offset(el);
            let h = $B.DomUtils.outerHeight(el);
            ofs.top = ofs.top + h + 6;
            this.$curListFireBtn = el;
        }else{
            ofs = el;
        }  
        this[elkey].style.display = "block";
        this[elkey].style.top = "-2000px";
        let w = $B.DomUtils.width(this[elkey]) + 3;
        let bw = $B.DomUtils.width(document.body);
        let diff = w + ofs.left - bw;
        if(diff > 0){
            ofs.left = ofs.left - diff;
            this[elkey].firstChild.firstChild.style.paddingLeft = diff+"px";
        }else{
            this[elkey].firstChild.firstChild.style.paddingLeft = "0px";
        }
        this[elkey].style.top = ofs.top + "px";
        this[elkey].style.left = ofs.left + "px";        
        this.$curOpenPanel = this[elkey];        
    }
    /**
     * 隐藏当前打开的panel
     * **/
    hidePanel(){
        if(this.$curOpenPanel){
            this.$curOpenPanel.style.display = "none";
            this.onPanelClosed(this.$curOpenPanel);            
            this.$curOpenPanel = undefined;
        }
    }
    /**
     * 创建通用panel
     * ***/
    createCommPanel(btn,elkey, opts) {        
        if (!this[elkey]) {
            if(!opts){
                opts = {};
            }
            opts.onClosed = ($el)=>{
                this.$curOpenPanel = undefined;
                this.$curListFireBtn = undefined;
                this.onPanelClosed($el);
            };
            this[elkey] = UTILS.createCommPanel(opts);
            this.$panels.push(this[elkey]);
            this.bindPanelEvents(this[elkey], opts.onPanalClick);
            this.bindFocusEv(this[elkey]);
        }
        this.showPanel(elkey, btn);
        return this[elkey];
    }
    /**
     * 判定按钮对应的处理函数是否需要选区
     * **/
    needRegion(fn) {
        return this.regionMap[fn];
    }
    /**联动工具栏**/
    reactiveUI(cssMap) {
        this.cssMap = cssMap;
        if (cssMap["letter-spacing"] === "normal" || !cssMap["letter-spacing"]) {
            cssMap["letter-spacing"] = "0";
        } else if(cssMap["letter-spacing"]) {
            cssMap["letter-spacing"] = cssMap["letter-spacing"].replaceAll("px", "");
        }      
        let $bold = this.btnElMap["boldFn"];
        if (cssMap["font-weight"] === "400") {
            $B.DomUtils.removeClass($bold, "k_edit_tools_hlight");
        } else {
            $B.DomUtils.addClass($bold, "k_edit_tools_hlight");
        }
        let $italic = this.btnElMap["italicFn"];
        if (cssMap["font-style"] === "normal") {
            $B.DomUtils.removeClass($italic, "k_edit_tools_hlight");
        } else {
            $B.DomUtils.addClass($italic, "k_edit_tools_hlight");
        }
        let $underline = this.btnElMap["underlineFn"];
        let txtDecriVal = cssMap["text-decoration"];
        if (txtDecriVal && txtDecriVal.indexOf("underline") >= 0) {
            $B.DomUtils.addClass($underline, "k_edit_tools_hlight");
        } else {
            $B.DomUtils.removeClass($underline, "k_edit_tools_hlight");
        }
        let $strikethrough = this.btnElMap["strikethroughFn"];
        if (txtDecriVal && txtDecriVal.indexOf("line-through") >= 0) {
            $B.DomUtils.addClass($strikethrough, "k_edit_tools_hlight");
        } else {
            $B.DomUtils.removeClass($strikethrough, "k_edit_tools_hlight");
        }
        let $superscript = this.btnElMap["superscriptFn"];
        if (cssMap["vertical-align"] === "super") {
            $B.DomUtils.addClass($superscript, "k_edit_tools_hlight");
        } else {
            $B.DomUtils.removeClass($superscript, "k_edit_tools_hlight");
        }
        let $subscript = this.btnElMap["subscriptFn"];
        if (cssMap["vertical-align"] === "sub") {
            $B.DomUtils.addClass($subscript, "k_edit_tools_hlight");
        } else {
            $B.DomUtils.removeClass($subscript, "k_edit_tools_hlight");
        }
        let $fontFamily = this.btnElMap["fontFamilyFn"];
        if (cssMap["font-family"]) {
            let v = cssMap["font-family"].replaceAll("\"", "");
            let comparev = v.replaceAll(/,\s+/g, ",");
            comparev = comparev.replaceAll(/\s+/g, "").toLowerCase();
            let childs = this["$fontFamilyFn"].lastChild.children;
            for (let i = 0; i < childs.length; i++) {
                let cfv = $B.DomUtils.attribute(childs[i], "val").replaceAll(/,\s+/g, ",").replaceAll(/\s+/g, "").toLowerCase();
                if (cfv.indexOf(comparev) >= 0) {
                    v = childs[i].innerText;
                    break;
                }
            }
            $fontFamily.firstChild.innerText = v;
        } else {
            $fontFamily.firstChild.innerText = $B.DomUtils.attribute($fontFamily.firstChild, "label");
        }

        let $fontSize = this.btnElMap["fontSizeFn"];
        if (cssMap["font-size"]) {
            $fontSize.firstChild.innerText = cssMap["font-size"];
        } else {
            $fontSize.firstChild.innerText = $B.DomUtils.attribute($fontSize.firstChild, "label");
        }
        let $fontTitle = this.btnElMap["fontTitleFn"];
        let txt = $B.DomUtils.attribute($fontTitle.firstChild, "label");
        if (cssMap["font-weight"] !== "400") {
            //轮询列表取值
            let childs = this["$fontTitleFn"].lastChild.children;
            for (let i = 0; i < childs.length; i++) {
                if (cssMap["font-size"] === $B.DomUtils.attribute(childs[i], "val")) {
                    txt = childs[i].innerText;
                    break;
                }
            }
        }
        $fontTitle.firstChild.innerText = txt;

        let $fontColor = this.btnElMap["fontColorFn"];
        $fontColor.lastChild.style.backgroundColor = cssMap["color"];

        let $backgroundColor = this.btnElMap["backgroundColorFn"];
        $backgroundColor.lastChild.style.backgroundColor = cssMap["background-color"];


        let $alignCenter = this.btnElMap["alignCenterFn"];
        if (cssMap["text-align"] === "center") {
            $B.DomUtils.addClass($alignCenter, "k_edit_tools_hlight");
        } else {
            $B.DomUtils.removeClass($alignCenter, "k_edit_tools_hlight");
        }

        let $alignLeft = this.btnElMap["alignLeftFn"];
        if (cssMap["text-align"] === "left") {
            $B.DomUtils.addClass($alignLeft, "k_edit_tools_hlight");
        } else {
            $B.DomUtils.removeClass($alignLeft, "k_edit_tools_hlight");
        }

        let $alignRight = this.btnElMap["alignRightFn"];
        if (cssMap["text-align"] === "right") {
            $B.DomUtils.addClass($alignRight, "k_edit_tools_hlight");
        } else {
            $B.DomUtils.removeClass($alignRight, "k_edit_tools_hlight");
        }
    }
    destroy() {
        this.colorIns = undefined;
        this.editIns = undefined;
        $B.DomUtils.remove(this.$tip);
        for (let i = 0; len = this.$panels.length; i++) {
            $B.DomUtils.remove(this.$panels[i]);
        }
        for(let i =0 ; i < this.BUICTLS.length ;i++){
            this.BUICTLS[i].destroy();
        }
        super.destroy();
    }
}